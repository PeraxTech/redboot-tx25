#!/bin/bash
options="cnqrRp:d:"
release=false
tools_dir="$PWD/tools/bin"
ecosconfig="$tools_dir/tools/configtool/standalone/common/ecosconfig"
src_dir="$PWD/packages"
quiet=false
clean=false
rebuild=false
doit=true
date="$(date -I)"
deploy_dir=""
make_opts=
pattern="*"

error() {
    rc=$?
    if [ -n "${target}" ];then
	echo "${target} build aborted"
    fi
    return $rc
}

build_host_tools() {
    echo "Building host tools in $tools_dir"
    # Debian packages tcl-dev and tk-dev are required for this build
    local wd="$PWD"
    local tcldirs="/usr/lib/tcl /usr/local/lib/tcl"
    local config_opts=""
    for d in $tcldirs;do
	if [ -d "$d" ];then
	    config_opts="$config_opts --with-tcl=${d%lib/tcl}"
	    break
	fi
    done
    if [ -z "$config_opts" ];then
	for d in /usr/lib/tcl*;do
	    [ -d "$d" ] || continue
	    config_opts="$config_opts --with-tcl-version=${d##*tcl}"
	done
    fi
    if [ -z "$config_opts" ];then
	echo "No Tcl installation found"
	exit 1
    fi

    export TCL_INC_DIR="$(. /usr/lib/tclConfig.sh; echo $TCL_INCLUDE_SPEC | sed 's/^-I//')"
    mkdir -p "$tools_dir"
    cd "$tools_dir"
    sh ../src/configure $config_opts
    make
    cd "$wd"
}

while getopts "$options" opt;do
    case $opt in
	c)
	    clean=true
	    ;;
	n)
	    doit=false
	    make_opts="${make_opts} -n"
	    ;;
	p)
	    pattern="$OPTARG"
	    ;;
	q)
	    quiet=true
	    ;;
	r)
	    rebuild=true
	    ;;
	R)
	    release=true
	    ;;
	d)
	    deploy_dir="$OPTARG"
	    ;;
	'?')
	    exit 1
	    ;;
	*)
	    echo "Unsupported option '$opt'"
	    exit 1
    esac
done
shift $(($OPTIND - 1))

if [ $# -gt 0 ];then
    targets="$@"
else
    targets=$(cd config;ls $pattern.ecc)
fi

set -e
trap error 0
conf_dir="$PWD/config"
[ -d build ] || mkdir -p build
cd build
wd=$PWD
if [ ! -x "${ecosconfig}" ];then
    build_host_tools
fi
for target in ${targets};do
    target="${target%.ecc}"
    if [ ! -d "${target}" ];then
	echo "Creating build dir ${target}"
	mkdir -p "${target}"
    fi

    cd "${target}"
    echo "Building ${target}"

    build_dir="$PWD/${target}_build"
    inst_dir="$PWD/${target}_install"

    $quiet || echo "Checking configuration ${target}"
    cp -p "${conf_dir}/${target}.ecc" "${conf_dir}/${target}.ecc.bak"
    echo ecosconfig --srcdir="$src_dir" --config="${conf_dir}/${target}.ecc" check
    if $doit;then
	stty -isig # prevent CTRL-C from trashing the config file
	set +e
	"${ecosconfig}" --srcdir="$src_dir" --config="${conf_dir}/${target}.ecc" check
	if [ $? != 0 ];then
	    mv "${conf_dir}/${target}.ecc.bak" "${conf_dir}/${target}.ecc"
	    exit 1
	fi
	set -e
	stty isig
    fi

    if $rebuild;then
	echo "Removing build dir ${build_dir} and ${inst_dir}"
	$doit && rm -rf "${build_dir}" "${inst_dir}"
    fi
    if [ ! -d "${build_dir}" ];then
	$quiet || echo "Creating build tree for ${target}"
	echo mkdir "${build_dir}"
	$doit && mkdir "${build_dir}"
    fi
    $doit && cd "${build_dir}"
    echo ecosconfig --srcdir="$src_dir" --prefix="${inst_dir}" \
	--config="${conf_dir}/${target}.ecc" tree
    if $doit;then
	stty -isig
	"${ecosconfig}" --srcdir="$src_dir" --prefix="${inst_dir}" \
	    --config="${conf_dir}/${target}.ecc" tree
	stty isig
	rm -f "${target}" ../../current && ln -snvf "${target}" ../../current
	rm -f ../install && ln -snvf "${target}_install" ../install
	rm -f ../build && ln -snvf "${target}_build" ../build
    fi

    if $clean;then
	$quiet || echo "Cleaning up build tree for ${target}"
	make ${make_opts} clean
    fi

    $quiet || echo "Compiling ${target}"
    [ -d "${build_dir}" ]
    make -C "${build_dir}" ${make_opts}

    cd $wd
    if $doit && [ -s "${inst_dir}/bin/redboot.elf" ];then
	bootstrap_addr="$(${cmd_prefix}nm "${inst_dir}/bin/redboot.elf" \
	    | sed '/Now_in_SDRAM/!d;s/ .*$//')"
	if [ -n "$bootstrap_addr" ] && ! echo "$bootstrap_addr" | grep -i '^[0-9a-f]\{4\}0[0-7]';then
	    echo "ERROR: Bootstrap does not fit into first NAND page!"
	    echo $bootstrap_addr
	    exit 1
	fi
    fi
    if $doit && [ -s "${inst_dir}/bin/redboot.bin" ] && \
	    [ -n "$deploy_dir" ];then
	echo "Deploying $deploy_dir/${target}${date:+-$date}.bin"
	cp -avu "${inst_dir}/bin/redboot.bin" "$deploy_dir/${target}${date:+-$date}.bin" 
	ln -snvf "${target}${date:+-$date}.bin" "$deploy_dir/${target}-latest.bin" 
    fi
    echo "${target} build finished"

done
trap - 0
