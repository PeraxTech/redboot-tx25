#ifndef CYGONCE_HAL_PLATFORM_SETUP_H
#define CYGONCE_HAL_PLATFORM_SETUP_H

//=============================================================================
//
//	hal_platform_setup.h
//
//	Platform specific support for HAL (assembly code)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <pkgconf/system.h>			// System-wide configuration info
#include CYGBLD_HAL_VARIANT_H			// Variant specific configuration
#include CYGBLD_HAL_PLATFORM_H			// Platform specific configuration
#include <cyg/hal/hal_soc.h>			// Variant specific hardware definitions
#include <cyg/hal/hal_mmu.h>			// MMU definitions
#include CYGBLD_HAL_PLF_DEFS_H			// Platform specific hardware definitions
#include CYGHWR_MEMORY_LAYOUT_H

#define CPU_CLK				CYGNUM_HAL_ARM_TX51_CPU_CLK
#define SDRAM_CLK			CYGNUM_HAL_ARM_TX51_SDRAM_CLK

#if defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1 _platform_setup1
#define CYGHWR_HAL_ARM_HAS_MMU

#ifdef CYG_HAL_STARTUP_ROMRAM
#define CYGSEM_HAL_ROM_RESET_USES_JUMP
#endif

#define TX51_NAND_PAGE_SIZE		2048
#define TX51_NAND_BLKS_PER_PAGE		64

#define DEBUG_LED_BIT			10
#define LED_GPIO_BASE			GPIO4_BASE_ADDR
#define LED_MUX_OFFSET			0x1d0
#define LED_MUX_MODE			0x13

#define LED_ON	LED_CTRL #1
#define LED_OFF	LED_CTRL #0

#ifndef CYGOPT_HAL_ARM_TX51_DEBUG
	.macro	LED_CTRL,val
	.endm
	.macro	LED_BLINK,val
	.endm
	.macro  DELAY,ms
	.endm
#else
#define CYGHWR_LED_MACRO	LED_BLINK #\x
	.macro	DELAY,ms
	ldr	r10, =\ms
111:
	subs	r10, r10, #1
	bmi	113f
	ldr	r9, =(3600 / 10)
112:
	subs	r9, r9, #1
	bne	112b
	b	111b
	.ltorg
113:
	.endm

	.macro	LED_CTRL,val
	// switch user LED (GPIO4_10) on STK5
	ldr	r10, =LED_GPIO_BASE
	// GPIO_DR
	mov	r9, \val
	cmp	r9, #0
	ldr	r9, [r10, #GPIO_DR]
	orrne	r9, #(1 << DEBUG_LED_BIT)	@ LED ON
	biceq	r9, #(1 << DEBUG_LED_BIT)	@ LED OFF
	str	r9, [r10, #GPIO_DR]
	.endm

	.macro	LED_BLINK,val
	mov	r8, \val
211:
	subs	r8, r8, #1
	bmi	212f
	LED_CTRL #1
	DELAY	200
	LED_CTRL #0
	DELAY	300
	b	211b
212:
	DELAY	1000
	.endm
#endif

	.macro	LED_INIT
	// initialize GPIO4_10 (PAD CSI2_D13) for LED on STK5
	ldr	r10, =LED_GPIO_BASE
	// GPIO_DR
	ldr	r9, [r10, #GPIO_DR]
	bic	r9, #(1 << DEBUG_LED_BIT)
	str	r9, [r10, #GPIO_DR]
	// GPIO_GDIR
	ldr	r9, [r10, #GPIO_GDIR]
	orr	r9, r9, #(1 << DEBUG_LED_BIT)
	str	r9, [r10, #GPIO_GDIR]
	// iomux
	ldr	r10, =IOMUXC_BASE_ADDR
	mov	r9, #LED_MUX_MODE
	str	r9, [r10, #LED_MUX_OFFSET]
	.endm

#define DCDGEN(type, addr, data)  .long	type, addr, data

#define PLATFORM_PREAMBLE flash_header

// This macro represents the initial startup code for the platform
	.macro	_platform_setup1
KARO_TX51_SETUP_START:
	mrs	r0, CPSR
	mov	r0, #0x1f
	orr	r0, r0, #128
	orr	r0, r0, #64
	msr	CPSR_xc, r0

	ldr	r1, =ROM_BASE_ADDR
	ldr	r11, [r1, #ROM_SI_REV_OFFSET]

	setup_sdram

	ldr	r0, =GPC_BASE_ADDR
	cmp	r11, #0x10	  // r11 contains the silicon rev
	ldrls	r1, =0x1FC00000
	ldrhi	r1, =0x1A800000
	str	r1, [r0, #4]

	// Explicitly disable L2 cache
	mrc	15, 0, r0, c1, c0, 1
	bic	r0, r0, #0x2
	mcr	15, 0, r0, c1, c0, 1

	// reconfigure L2 cache aux control reg
	mov	r0, #0xC0		// tag RAM
	add	r0, r0, #0x4		// data RAM
	orr	r0, r0, #(1 << 24)	// disable write allocate delay
	orr	r0, r0, #(1 << 23)	// disable write allocate combine
	orr	r0, r0, #(1 << 22)	// disable write allocate

	@ cc is still set from "cmp r11, #0x10" above
	orrls	r0, r0, #(1 << 25)	@ disable write combine for TO 2 and lower revs

	mcr	15, 1, r0, c9, c0, 2

init_aips_start:
	init_aips
	LED_INIT
init_m4if_start:
	init_m4if

#ifdef CYG_HAL_STARTUP_ROMRAM     /* enable running from RAM */
	LED_BLINK #1
#endif /* CYG_HAL_STARTUP_ROMRAM */
init_clock_start:
	init_clock

Normal_Boot_Continue:
/*
 * Note:
 *	IOMUX/PBC setup is done in C function plf_hardware_init() for simplicity
 */

STACK_Setup:
	@ Set up a stack [for calling C code]
	ldr	r1, =__startup_stack
	ldr	r2, =RAM_BANK0_BASE
	orr	sp, r1, r2

	@ Create MMU tables
	bl	hal_mmu_init
	LED_BLINK #2

	/* Workaround for arm erratum #709718 */
	@ Setup PRRR so device is always mapped to non-shared
	mrc	MMU_CP, 0, r1, c10, c2, 0 // Read Primary Region Remap Register
	bic	r1, #(3 << 16)
	mcr	MMU_CP, 0, r1, c10, c2, 0 // Write Primary Region Remap Register

	@ Enable MMU
	ldr	r2, =10f
	mrc	MMU_CP, 0, r1, MMU_Control, c0
	orr	r1, r1, #7			@ enable MMU bit
	orr	r1, r1, #0x800			@ enable z bit
	orr	r1, r1, #(1 << 28)		@ Enable TEX remap, workaround for L1 cache issue
	mcr	MMU_CP, 0, r1, MMU_Control, c0

	/* Workaround for arm errata #621766 */
	mrc	MMU_CP, 0, r1, MMU_Control, c0, 1
	orr	r1, r1, #(1 << 5)		@ enable L1NEON bit
	mcr	MMU_CP, 0, r1, MMU_Control, c0, 1

	mov	pc, r2				@ Change address spaces
	.align	5
10:
	LED_BLINK #3
	.endm	@ _platform_setup1

	/* AIPS setup - Only setup MPROTx registers. The PACR default values are good.*/
	.macro	init_aips
	/*
	* Set all MPROTx to be non-bufferable, trusted for R/W,
	* not forced to user-mode.
	*/
	ldr	r0, =AIPS1_CTRL_BASE_ADDR
	ldr	r1, AIPS1_PARAM
	str	r1, [r0, #0x00]
	str	r1, [r0, #0x04]
	ldr	r0, =AIPS2_CTRL_BASE_ADDR
	str	r1, [r0, #0x00]
	str	r1, [r0, #0x04]
	.endm	/* init_aips */

	.macro	WDOG_RESET
	ldr	r0, =WDOG_BASE_ADDR
	mov	r1, #0
1:
	strh	r1, [r0]
	b	1b
	.endm

	.macro	init_clock
	ldr	r0, =CCM_BASE_ADDR
	ldr	r1, [r0, #CLKCTL_CCR]
	tst	r1, #(1 << 12)
	bne	osc_ok

	orr	r1, r1, #(1 << 12)
	str	r1, [r0, #CLKCTL_CCR]

	ldr	r1, [r0, #CLKCTL_CCSR]
	bic	r1, #(1 << 9)	/* switch lp_apm to OSC */
	str	r1, [r0, #CLKCTL_CCSR]
osc_ok:
	/* Gate off clocks to the peripherals first */
	ldr	r1, =0x3FFFFFFF
	str	r1, [r0, #CLKCTL_CCGR0]
	ldr	r1, =0x0
	str	r1, [r0, #CLKCTL_CCGR1]
	str	r1, [r0, #CLKCTL_CCGR2]
	str	r1, [r0, #CLKCTL_CCGR3]

	ldr	r1, =0x00030000
	str	r1, [r0, #CLKCTL_CCGR4]
	ldr	r1, =0x00FFF030
	str	r1, [r0, #CLKCTL_CCGR5]
	ldr	r1, =0x00000300
	str	r1, [r0, #CLKCTL_CCGR6]

	/* Disable IPU and HSC dividers */
	mov	r1, #0x60000
	str	r1, [r0, #CLKCTL_CCDR]

	/* Make sure to switch the DDR away from PLL 1 */
	ldr	r1, CCM_CBCDR_VAL1
	str	r1, [r0, #CLKCTL_CBCDR]
	/* make sure divider effective */
1:
	ldr	r1, [r0, #CLKCTL_CDHIPR]
	cmp	r1, #0x0
	bne	1b

	/* Switch ARM to step clock */
	ldr	r1, [r0, #CLKCTL_CCSR]
	mov	r1, #0x4
	str	r1, [r0, #CLKCTL_CCSR]

#if CPU_CLK == 800
	setup_pll PLL1, 800
#elif CPU_CLK == 600
	setup_pll PLL1, 600
#else
#error Bad CPU clock
#endif
	setup_pll PLL3, 665

	/* Switch peripheral to PLL 3 */
	ldr	r1, CCM_CBCMR_VAL1
	str	r1, [r0, #CLKCTL_CBCMR]

	ldr	r1, CCM_CBCDR_VAL2
	str	r1, [r0, #CLKCTL_CBCDR]

	setup_pll PLL2, 665

	/* Switch peripheral to PLL 2 */
	ldr	r1, CCM_CBCDR_VAL1
	str	r1, [r0, #CLKCTL_CBCDR]
	/* Use lp_apm (24MHz) source for perclk */
	ldr	r1, CCM_CBCMR_VAL2
	str	r1, [r0, #CLKCTL_CBCMR]

	setup_pll PLL3, 216

	/* Set the platform clock dividers */
	ldr	r2, =PLATFORM_BASE_ADDR
	ldr	r1, PLATFORM_CLOCK_DIV
	str	r1, [r2, #PLATFORM_ICGC]

	/* Run TO 3.0 at Full speed, for other TO's wait till we increase VDDGP */
	cmp	r11, #0x10
	movls	r1, #1
	movhi	r1, #0
	str	r1, [r0, #CLKCTL_CACRR]

	/* Switch ARM back to PLL 1. */
	mov	r1, #0
	str	r1, [r0, #CLKCTL_CCSR]

	/* setup the rest */
	@ ddr clock from PLL 1, all perclk dividers are 1 since using 24MHz
	ldr	r1, CCM_CBCDR_VAL3
	str	r1, [r0, #CLKCTL_CBCDR]

	/* Restore the default values in the Gate registers */
	ldr	r1, =0xFFFFFFFF
	str	r1, [r0, #CLKCTL_CCGR0]
	str	r1, [r0, #CLKCTL_CCGR1]
	str	r1, [r0, #CLKCTL_CCGR2]
	str	r1, [r0, #CLKCTL_CCGR3]
	str	r1, [r0, #CLKCTL_CCGR4]
	str	r1, [r0, #CLKCTL_CCGR5]
	str	r1, [r0, #CLKCTL_CCGR6]

	/* Use PLL 2 for UART's, get 66.5MHz from it */
	ldr	r1, CCM_CSCMR1_VAL
	str	r1, [r0, #CLKCTL_CSCMR1]
	ldr	r1, CCM_CSCDR1_VAL
	str	r1, [r0, #CLKCTL_CSCDR1]

	/* make sure divider is in effect */
1:
	ldr	r1, [r0, #CLKCTL_CDHIPR]
	cmp	r1, #0x0
	bne	1b

	mov	r1, #0x00000
	str	r1, [r0, #CLKCTL_CCDR]

	ldr	r1, [r0, #CLKCTL_CCR]
	bic	r1, #(1 << 8)		/* switch off FPM */
	str	r1, [r0, #CLKCTL_CCR]
	.endm	@ init_clock

	.macro	setup_pll pll_nr, mhz
	ldr	r2, BASE_ADDR_\pll_nr
	.ifge	\mhz - 800
	/* implement workaround for ENGcm12051 */
	mov	r1, #0				@ Disable auto-restart (AREN)
	str	r1, [r2, #PLL_DP_CONFIG]

	ldr	r1, =DP_OP_864
	str	r1, [r2, #PLL_DP_OP]
	str	r1, [r2, #PLL_DP_HFS_OP]

	ldr	r1, =DP_MFD_864
	str	r1, [r2, #PLL_DP_MFD]
	str	r1, [r2, #PLL_DP_HFS_MFD]

	ldr	r1, =DP_MFN_864
	str	r1, [r2, #PLL_DP_MFN]
	str	r1, [r2, #PLL_DP_HFS_MFN]

	ldr	r1, =((1 << 2) | 0x1232)	@ Set DPLL ON; UPEN=1 BRMO=1 PLM=1
	str	r1, [r2, #PLL_DP_CTL]
100:
	ldr	r1, [r2, #PLL_DP_CTL]		@ Poll LRF
	tst	r1, #(1 << 0)
	beq	100b

	ldr	r1, =60
	str	r1, [r2, #PLL_DP_MFN]
	str	r1, [r2, #PLL_DP_HFS_MFN]

	mov	r1, #(1 << 0)
	str	r1, [r2, #PLL_DP_CONFIG]	@ Assert LDREQ
101:
	ldr	r1, [r2, #PLL_DP_CONFIG]	@ Poll LDREQ
	tst	r1, #(1 << 0)
	bne	101b

	mov	r1, #4
	/*
	 * delay for 4 µs
	 * since cache is disabled, this loop is more than enough
	 */
102:
	subs	r1, r1, #1
	bne	102b
	.else
	ldr	r1, =0x1232			@ Set DPLL ON (set UPEN bit); BRMO=1
	str	r1, [r2, #PLL_DP_CTL]

	mov	r1, #(1 << 1)			@ Enable auto-restart (AREN)
	str	r1, [r2, #PLL_DP_CONFIG]

	ldr	r1, W_DP_OP_\mhz
	str	r1, [r2, #PLL_DP_OP]
	str	r1, [r2, #PLL_DP_HFS_OP]

	ldr	r1, W_DP_MFD_\mhz
	str	r1, [r2, #PLL_DP_MFD]
	str	r1, [r2, #PLL_DP_HFS_MFD]

	ldr	r1, W_DP_MFN_\mhz
	str	r1, [r2, #PLL_DP_MFN]
	str	r1, [r2, #PLL_DP_HFS_MFN]

	mov	r1, #((1 << 0) | (1 << 1))
	str	r1, [r2, #PLL_DP_CONFIG]	@ Assert LDREQ + AREN

	@ Now restart PLL
	ldr	r1, =0x1232
	str	r1, [r2, #PLL_DP_CTL]
103:
	ldr	r1, [r2, #PLL_DP_CTL]
	ands	r1, r1, #0x1
	beq	103b
	.endif
	.endm

	/* M4IF setup */
	.macro	init_m4if
	ldr	r1, =M4IF_BASE_ADDR
	ldr	r0, M4IF_M4IF4_VAL
	str	r0, [r1, #M4IF_MIF4]

	/* Configure M4IF registers, VPU and IPU given higher priority (=0x4) */
	ldr	r0, M4IF_FBPM0_VAL
	str	r0, [r1, #M4IF_FBPM0]

	ldr	r0, M4IF_FPWC_VAL
	str	r0, [r1, #M4IF_FPWC]
	.endm	/* init_m4if */

	.macro	setup_sdram
#if 0
	cmp	r11, #0x10    // r11 contains the silicon rev
	bls	skip_setup
	/* Decrease the DRAM SDCLK pads to HIGH Drive strength */
	ldr	r0, =IOMUXC_BASE_ADDR
	ldr	r1, =0x000000e5
	str	r1, [r0, #0x4b8]
	/* Change the delay line configuration */
	ldr	r0, =ESDCTL_BASE_ADDR
	ldr	r1, =0x00f49400
	str	r1, [r0, #ESDCTL_ESDCDLY1]
	ldr	r1, =0x00f49a00
	str	r1, [r0, #ESDCTL_ESDCDLY2]
	ldr	r1, =0x00f49100
	str	r1, [r0, #ESDCTL_ESDCDLY3]
	ldr	r1, =0x00f48900
	str	r1, [r0, #ESDCTL_ESDCDLY4]
	ldr	r1, =0x00f49400
	str	r1, [r0, #ESDCTL_ESDCDLY5]
#endif
skip_setup:
	.endm
#else // defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1
#endif

#define PLATFORM_VECTORS	 _platform_vectors
	.macro	_platform_vectors
	.globl	_KARO_MAGIC
_KARO_MAGIC:
	.ascii	"KARO_CE6"
	.globl	_KARO_STRUCT_SIZE
_KARO_STRUCT_SIZE:
	.word	0	// reserve space structure length

	.globl	_KARO_CECFG_START
_KARO_CECFG_START:
	.rept	1024/4
	.word	0	// reserve space for CE configuration
	.endr

	.globl	_KARO_CECFG_END
_KARO_CECFG_END:
	.endm

	.align	5
	.ascii	"KARO TX51 " __DATE__ " " __TIME__
	.align

/* SDRAM timing setup */
#define RALAT		1
#define LHD		0

#if SDRAM_SIZE <= SZ_128M
#define RA_BITS		(13 - 11)	/* row addr bits - 11 */
#else
#define RA_BITS		(14 - 11)	/* row addr bits - 11 */
#endif

#define CA_BITS		(10 - 8)	/* 0-2: col addr bits - 8 3: rsrvd */
#define DSIZ		2	/* 0: D[31..16] 1: D[15..D0] 2: D[31..0] 3: rsrvd */
#define SREFR		3	/* 0: disabled 1-5: 2^n rows/clock *: rsrvd */
#define SRT		0	/* 0: disabled *: 1: self refr. ... */
#define PWDT		0	/* 0: disabled 1: precharge pwdn
				   2: pwdn after 64 clocks 3: pwdn after 128 clocks */
#define ESDCTL_VAL	(0x80000000 | (SREFR << 28) | (RA_BITS << 24) | (CA_BITS << 20) | \
			 (DSIZ << 16) | (SRT << 14) | (PWDT << 12))

#define NS_TO_CK(ns)	(((ns) * SDRAM_CLK + 999) / 1000)

	.macro		CK_VAL,	name, clks, offs
	.iflt		\clks - \offs
	.set		\name, 0
	.else
	.set		\name, \clks - \offs
	.endif
	.endm

	.macro		NS_VAL,	name, ns, offs
	.iflt		\ns - \offs
	.set		\name, 0
	.else
	CK_VAL		\name, NS_TO_CK(\ns), \offs
	.endif
	.endm

#if SDRAM_CLK < 200
/* MT46H32M32LF-6 */
NS_VAL	tRFC, 125, 10	/* clks - 10 (0..15) */
NS_VAL	tXSR, 138, 25	/* clks - 25 (0..15) */
NS_VAL	tXP,   25,  1	/* clks - 1 (0..7)  */
CK_VAL	tWTR,   1,  1	/* clks - 1 (0..1)  */
NS_VAL	tRP,   18,  2	/* clks - 2 (0..3)  */
CK_VAL	tMRD,   2,  1	/* clks - 1 (0..3)  */
NS_VAL	tWR,   15,  2	/* clks - 2 (0..1)  */
NS_VAL	tRAS,  42,  1	/* clks - 1 (0..15) */
NS_VAL	tRRD,  12,  1	/* clks - 1 (0..3)  */
NS_VAL	tRCD,  18,  1	/* clks - 1 (0..7) */
NS_VAL	tRC,   60,  1	/* 0: 20 *: clks - 1 (0..15) */
#else
/* MT46H64M32LF-5 or -6 */
NS_VAL	tRFC,  72, 10	/* clks - 10 (0..15) */
NS_VAL	tXSR, 113, 25	/* clks - 25 (0..15) */
CK_VAL	tXP,    2,  1	/* clks - 1 (0..7)  */
CK_VAL	tWTR,   2,  1	/* clks - 1 (0..1)  */
NS_VAL	tRP,   18,  2	/* clks - 2 (0..3)  */
CK_VAL	tMRD,   2,  1	/* clks - 1 (0..3)  */
NS_VAL	tWR,   15,  2	/* clks - 2 (0..1)  */
NS_VAL	tRAS,  42,  1	/* clks - 1 (0..15) */
NS_VAL	tRRD,  12,  1	/* clks - 1 (0..3)  */
NS_VAL	tRCD,  18,  1	/* clks - 1 (0..7) */
NS_VAL	tRC,   60,  1	/* 0: 20 *: clks - 1 (0..15) */
#endif

#define ESDCFG_VAL	((tRFC << 28) | (tXSR << 24) | (tXP << 21) | \
			(tWTR << 20) | (tRP << 18) | (tMRD << 16) | \
			(tRAS << 12) | (tRRD << 10) | (tWR << 7) | \
			(tRCD << 4) | (tRC << 0))
/*
	0x70655427
*/
#define ESDMISC_RALAT(n)	(((n) & 0x3) << 7)
#define ESDMISC_DDR2_EN(n)	(((n) & 0x1) << 4)
#define ESDMISC_DDR_EN(n)	(((n) & 0x1) << 3)
#define ESDMISC_AP(n)		(((n) & 0xf) << 16)
#define ESDMISC_VAL		(ESDMISC_AP(10) | ESDMISC_RALAT(RALAT) | \
				(LHD << 5) | ESDMISC_DDR2_EN(0) | ESDMISC_DDR_EN(0))

	.macro	flash_header
	b	reset_vector
	.org	0x400
app_start_addr:
	.long reset_vector
app_code_barker:
	.long	0xB1
app_code_csf:
	.long	0 // 0x97f40000 - 0x1000
dcd_ptr_ptr:
	.long	dcd_ptr
super_root_key:
	.long	0 // hab_super_root_key
dcd_ptr:
	.long	dcd_data
app_dest_ptr:
#ifndef RAM_BANK1_SIZE
	.long	RAM_BANK0_BASE + SDRAM_SIZE - REDBOOT_OFFSET
#else
	.long	RAM_BANK1_BASE + RAM_BANK1_SIZE - REDBOOT_OFFSET
#endif
dcd_data:
	.long	0xB17219E9   // Fixed. can't change.
dcd_len:
	.long	dcd_end - dcd_start
dcd_start:
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDCTL0, 0x80000000)
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDSCR, 0x04008008)
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDSCR, 0x00008010)
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDSCR, 0x00008010)
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDSCR, 0x00338018)
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDCTL0, ESDCTL_VAL)
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDCFG0, ESDCFG_VAL)
#ifdef RAM_BANK1_SIZE
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDCTL1, ESDCTL_VAL)
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDCFG1, ESDCFG_VAL)
#endif
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDGPR, 0x00020000)
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDMISC, ESDMISC_VAL)
	DCDGEN(4, ESDCTL_BASE_ADDR + ESDCTL_ESDSCR, 0x00000000)

	DCDGEN(4, IOMUXC_BASE_ADDR + 0x508, 0x000020e0)	@ EIM_SDBA2
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x50c, 0x000020e1)	@ EIM_SDODT1
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x510, 0x000020e1)	@ EIM_SDODT0
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x820, 0x00000040)	@ (Bit6 PUE) GRP_DDRPKS
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x82c, 0x00000000)	@ (Bit[1..2] DSE D[24..31]) GRP_DRAM_B4 DFT: 0x4
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x830, 0x00000000)	@ (Bit9 DDR_INPUT A[0..14] CAS CS[0..1] RAS SDCKE[0..1]
						       	@  SDWE SDBA[0..1]) GRP_INDDR
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x838, 0x00000080)	@ (Bit7 PKE D[0..31]) GRP_PKEDDR
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x83c, 0x00000000)	@ (Bit[1..2] DSE A[0..7]) GRP_DDR_A0 DFT: 0x4
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x848, 0x00000000)	@ (Bit[1..2] DSE A[8..14] SDBA[0..2]) GRP_DDR_A1
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x84c, 0x00000020)	@ (Bit[4..5] PUS A[0..14] CAS RAS SDBA[0..1]) GRP_DDRAPUS
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x85c, 0x00000000)	@ (Bit8 HYS D[0..7]) GRP_HYSDDR0
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x864, 0x00000000)	@ (Bit8 HYS D[8..15]) GRP_HYSDDR1
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x86c, 0x00000000)	@ (Bit8 HYS D[16..23]) GRP_HYSDDR2
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x870, 0x00002000)	@ (Bit13 A[0..14] CAS CS[0..1] D[0..31] DQM[0..3] RAS
						       	@  SDCKE[0..1] SDCLK SDQS[0..3] SDWE SDBA[0..2]
						       	@  SDODT[0..1]) GRP_HVDDR
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x874, 0x00000000)	@ (Bit8 HYS D[24..31]) GRP_HYSDDR3
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x878, 0x00000001)	@ (Bit0 SRE D[0..7]) GRP_SR_B0
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x87c, 0x00000040)	@ (Bit6 PUE A[0..14] CAS RAS SDBA[0..1]) GRP_DDRAPKS
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x880, 0x00000001)	@ (Bit0 SRE D[8..15]) GRP_SR_B1
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x884, 0x00000020)	@ (Bit[4..5] PUS D[0..31]) GRP_DDRPUS
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x88c, 0x00000001)	@ (Bit0 SRE D[16..23]) GRP_SR_B2
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x890, 0x00000080)	@ (Bit7 PKE A[0..14] CAS RAS SDBA[0..1]) GRP_PKEADDR
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x89c, 0x00000001)	@ (Bit0 SRE D[24..31]) GRP_SR_B4
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x8a0, 0x00000000)	@ (Bit9 DDR_INPUT D[0..31] DQM[0..3]) GRP_INMODE1
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x8a4, 0x00000000)	@ (Bit[1..2] DSE D[0..7]) GRP_DRAM_B0 DFT: 0x4
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x8ac, 0x00000000)	@ (Bit[1..2] DSE D[8..15]) GRP_DRAM_B1 DFT: 0x4
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x8b0, 0x00000001)	@ (Bit0 SRE A[0..7]) GRP_SR_A0
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x8b8, 0x00000000)	@ (Bit[1..2] DSE D[16..23]) GRP_DRAM_B2 DFT: 0x4
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x8bc, 0x00000001)	@ (Bit0 SRE A[8..14] SDBA[0..2]) GRP_SR_A1

	DCDGEN(4, IOMUXC_BASE_ADDR + 0x4e4, 0x2000)	@ NANDF_WE_B
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x4e8, 0x2000)	@ NANDF_RE_B
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x4ec, 0x2000)	@ NANDF_ALE
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x4f0, 0x2000)	@ NANDF_CLE
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x4f4, 0x2000)	@ NANDF_WP_B
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x4f8, 0x2000)	@ NANDF_RB0

	DCDGEN(4, IOMUXC_BASE_ADDR + 0x518, 0x0084)	@ NANDF_CS0

	DCDGEN(4, IOMUXC_BASE_ADDR + 0x538, 0x20e0)	@ NANDF_RDY_INT

	DCDGEN(4, IOMUXC_BASE_ADDR + 0x55c, 0x20a4)	@ NANDF_D7
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x560, 0x20a4)	@ NANDF_D6
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x564, 0x20a4)	@ NANDF_D5
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x568, 0x20a4)	@ NANDF_D4
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x56c, 0x20a4)	@ NANDF_D3
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x570, 0x20a4)	@ NANDF_D2
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x574, 0x20a4)	@ NANDF_D1
	DCDGEN(4, IOMUXC_BASE_ADDR + 0x578, 0x20a4)	@ NANDF_D0
dcd_end:
	.ifgt	dcd_end - dcd_start - 60 * 12
	.error	"DCD too large!"
	.endif
image_len:
	.long	REDBOOT_IMAGE_SIZE
	.endm

SRC_BASE_ADDR_W:	.long	SRC_BASE_ADDR
WDOG_BASE_ADDR_W:	.long	WDOG_BASE_ADDR
AIPS1_PARAM:		.word	0x77777777
M4IF_FBPM0_VAL:		.word	0x00000103
M4IF_M4IF4_VAL:		.word	0x00230185
M4IF_FPWC_VAL:		.word	0x00240126
MXC_REDBOOT_ROM_START:	.long	SDRAM_BASE_ADDR + SDRAM_SIZE - REDBOOT_OFFSET
CCM_CBCDR_VAL1:		.word	0x19235145
CCM_CBCDR_VAL2:		.word	0x13235145
#if (CPU_CLK % SDRAM_CLK == 0)
CCM_CBCDR_VAL3:		.word	(((CPU_CLK + SDRAM_CLK - 1) / SDRAM_CLK - 1) << 27) | (1 << 30) | 0x01e35100
#else
CCM_CBCDR_VAL3:		.word	0x01e35100
#endif
#if 0

#if SDRAM_CLK == 200
#if CPU_CLK == 800
CCM_CBCDR_VAL3:		.word	0x59E35100
#elif CPU_CLK == 600
CCM_CBCDR_VAL3:		.word	0x51E35100
#else
#error Bad CPU_CLK
#endif
#elif SDRAM_CLK == 166
#if CPU_CLK == 800
CCM_CBCDR_VAL3:		.word	0x01E35100
#elif CPU_CLK == 600
CCM_CBCDR_VAL3:		.word	0x01E35100
#else
#error Bad CPU_CLK
#endif
#else
#error Bad SDRAM_CLK
#endif

#endif
CCM_CBCMR_VAL1:		.word	0x000010C0
CCM_CBCMR_VAL2:		.word	0x000020C0
CCM_CSCMR1_VAL:		.word	0xA5A2A020
CCM_CSCDR1_VAL:		.word	0x00C30321
BASE_ADDR_PLL1:		.long	PLL1_BASE_ADDR
BASE_ADDR_PLL2:		.long	PLL2_BASE_ADDR
BASE_ADDR_PLL3:		.long	PLL3_BASE_ADDR
W_DP_OP_800:		.word	DP_OP_800
W_DP_MFD_800:		.word	DP_MFD_800
W_DP_MFN_800:		.word	DP_MFN_800
W_DP_OP_700:		.word	DP_OP_700
W_DP_MFD_700:		.word	DP_MFD_700
W_DP_MFN_700:		.word	DP_MFN_700
W_DP_OP_600:		.word	DP_OP_600
W_DP_MFD_600:		.word	DP_MFD_600
W_DP_MFN_600:		.word	DP_MFN_600
W_DP_OP_400:		.word	DP_OP_400
W_DP_MFD_400:		.word	DP_MFD_400
W_DP_MFN_400:		.word	DP_MFN_400
W_DP_OP_532:		.word	DP_OP_532
W_DP_MFD_532:		.word	DP_MFD_532
W_DP_MFN_532:		.word	DP_MFN_532
W_DP_OP_665:		.word	DP_OP_665
W_DP_MFD_665:		.word	DP_MFD_665
W_DP_MFN_665:		.word	DP_MFN_665
W_DP_OP_216:		.word	DP_OP_216
W_DP_MFD_216:		.word	DP_MFD_216
W_DP_MFN_216:		.word	DP_MFN_216
PLATFORM_CLOCK_DIV:	.word	0x00000124

/*----------------------------------------------------------------------*/
/* end of hal_platform_setup.h						*/
#endif /* CYGONCE_HAL_PLATFORM_SETUP_H */
