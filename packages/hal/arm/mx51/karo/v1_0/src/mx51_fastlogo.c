//==========================================================================
//
//      mx51_fastlogo.c
//
//      MX51 Fast Logo Implementation
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================

#include <cyg/io/ipu_common.h>

// DI counter definitions
#define DI_COUNTER_BASECLK		0
#define DI_COUNTER_IHSYNC		1
#define DI_COUNTER_OHSYNC		2
#define DI_COUNTER_OVSYNC		3
#define DI_COUNTER_ALINE		4
#define DI_COUNTER_ACLOCK		5

//extern display_buffer_info_t display_buffer;
static display_buffer_info_t display_buffer;

void fastlogo_init(display_buffer_info_t *di)
{
	display_buffer = *di;
}

void fastlogo_dma(void)
{
	ipu_channel_parameter_t ipu_channel_params;

	ipu_idmac_channel_enable(display_buffer.channel, 0);

	ipu_idmac_params_init(&ipu_channel_params);
	ipu_channel_params.channel = display_buffer.channel;
	ipu_channel_params.eba0 = display_buffer.startAddr / 8;
	ipu_channel_params.fw = display_buffer.width - 1;   /* frame width */
	ipu_channel_params.fh = display_buffer.height - 1;  /* frame height */
	ipu_channel_params.sl = (display_buffer.width * display_buffer.bpp) / 8 - 1;
	ipu_channel_params.npb = 31;	/* 16 pixels per burst */
	ipu_channel_params.pfs = 7;		/* 1->4:2:2 non-interleaved, 7->rgb */

	switch (display_buffer.bpp) {
	case 32:
		ipu_channel_params.bpp = 0;
		break;
	case 24:
		ipu_channel_params.bpp = 1;
		break;
	case 18:
		ipu_channel_params.bpp = 2;
		break;
	case 16:
		ipu_channel_params.bpp = 3;
		break;
	default:
		diag_printf("%s: unsupported bpp value: %d\n", __FUNCTION__,
					display_buffer.bpp);
		return;
	}

	switch (display_buffer.dataFormat) {
	case RGB565:
		ipu_channel_params.wid0 = 5 - 1;
		ipu_channel_params.wid1 = 6 - 1;
		ipu_channel_params.wid2 = 5 - 1;
		ipu_channel_params.wid3 = 0;
		ipu_channel_params.ofs0 = 0;
		ipu_channel_params.ofs1 = 5;
		ipu_channel_params.ofs2 = 11;
		ipu_channel_params.ofs3 = 16;
		break;

	case RGB666:
		ipu_channel_params.wid0 = 6 - 1;
		ipu_channel_params.wid1 = 6 - 1;
		ipu_channel_params.wid2 = 6 - 1;
		ipu_channel_params.wid3 = 0;
		ipu_channel_params.ofs0 = 0;
		ipu_channel_params.ofs1 = 6;
		ipu_channel_params.ofs2 = 12;
		ipu_channel_params.ofs3 = 18;
		break;

	case RGB888:
		ipu_channel_params.wid0 = 8 - 1;
		ipu_channel_params.wid1 = 8 - 1;
		ipu_channel_params.wid2 = 8 - 1;
		ipu_channel_params.wid3 = 0;
		ipu_channel_params.ofs0 = 0;
		ipu_channel_params.ofs1 = 8;
		ipu_channel_params.ofs2 = 16;
		ipu_channel_params.ofs3 = 24;
		break;

	case RGBA8888:
		ipu_channel_params.wid0 = 8 - 1;
		ipu_channel_params.wid1 = 8 - 1;
		ipu_channel_params.wid2 = 8 - 1;
		ipu_channel_params.wid3 = 8 - 1;
		ipu_channel_params.ofs0 = 0;
		ipu_channel_params.ofs1 = 8;
		ipu_channel_params.ofs2 = 16;
		ipu_channel_params.ofs3 = 24;
		break;

	default:
		diag_printf("%s: unsupported data format: %d\n", __FUNCTION__,
					display_buffer.dataFormat);
		return;
	}
	ipu_channel_params.bm = 0;
	ipu_channel_params.hf = 0;
	ipu_channel_params.vf = 0;
	ipu_channel_params.id = 0;
	ipu_idmac_interleaved_channel_config(ipu_channel_params);

	ipu_idmac_channel_mode_sel(display_buffer.channel, 0);
	ipu_idmac_channel_enable(display_buffer.channel, 1);
}

void fastlogo_dmfc(void)
{
	ipu_dmfc_fifo_allocate(display_buffer.channel, 1, 0, 4);
}

void fastlogo_dc(void)
{
	const int display_port = 0;

	//***************************************************/
	//DI CONFIGURATION
	//****************************************************/
	/* MICROCODE */
	dc_microcode_t microcode;
	microcode.addr = 4;
	microcode.stop = 1;
	microcode.opcode = "WROD";
	microcode.lf = 0;
	microcode.af = 0;
	microcode.operand = 0;
	microcode.mapping = 2;
	microcode.waveform = 1;
	microcode.gluelogic = 0;
	microcode.sync = 5;
	ipu_dc_microcode_config(microcode);

	ipu_dc_microcode_event(1, "NEW_DATA", 1, 4);

	/* WRITE_CHAN */
	ipu_dc_write_channel_config(display_buffer.channel, display_port, 0, 0);

	/* DISP_CONF */
	ipu_dc_display_config(display_port, 2 /* paralell */, 0,
						display_buffer.width);

	/* output data pixel format */
	ipu_dc_map(1, RGB888);
}

void fastlogo_di(void)
{
	di_sync_wave_gen_t syncWaveformGen = { 0 };
	int clkUp, clkDown;
	int hSyncStartWidth = 36;
	int hSyncWidth = 96;
	int hSyncEndWidth = 76;
	int delayH2V = display_buffer.width;
	int hDisp = display_buffer.width;
	int vSyncStartWidth = 11;
	int vSyncWidth = 2;
	int vSyncEndWidth = 32;
	int vDisp = display_buffer.height;
	int ipuClk = 133000000;		// ipu clk is 133M
	int typPixClk = 24000000;//25175000;   // typical value of pixel clock
	int div = (int)((float)ipuClk / (float)typPixClk + 0.5);	// get the nearest value of typical pixel clock

	/* DI0_SCR, set the screen height */
	ipu_di_screen_set(0, vDisp + vSyncStartWidth + vSyncWidth + vSyncEndWidth - 1);

	/* set DI_PIN15 to be waveform according to DI data wave set 3 */
	ipu_di_pointer_config(0, 0, div - 1, div - 1, 0, 0, 0, 0, 0, 2, 0, 0);

	/* set the up & down of data wave set 3. */
	ipu_di_waveform_config(0, 0, 2, 0, div * 2);	// one bit for fraction part

	/* set clk for DI0, generate the base clock of DI0. */
	clkUp = div - 2;
	clkDown = clkUp * 2;
	ipu_di_bsclk_gen(0, div << 4, clkUp, clkDown);

	/*
	  DI0 configuration:
	  hsync    -   DI0 pin 3
	  vsync    -   DI0 pin 2
	  data_en  -   DI0 pin 15
	  clk      -   DI0 disp clk
	  COUNTER 2  for VSYNC
	  COUNTER 3  for HSYNC
	*/
	/* internal HSYNC */
	syncWaveformGen.runValue = hDisp + hSyncStartWidth + hSyncWidth + hSyncEndWidth - 1;
	syncWaveformGen.runResolution = DI_COUNTER_BASECLK + 1;
	syncWaveformGen.offsetValue = 0;
	syncWaveformGen.offsetResolution = 0;
	syncWaveformGen.cntAutoReload = 1;
	syncWaveformGen.stepRepeat = 0;
	syncWaveformGen.cntClrSel = 0;
	syncWaveformGen.cntPolarityGenEn = 0;
	syncWaveformGen.cntPolarityTrigSel = 0;
	syncWaveformGen.cntPolarityClrSel = 0;
	syncWaveformGen.cntUp = 0;
	syncWaveformGen.cntDown = 1;
	ipu_di_sync_config(0, DI_COUNTER_IHSYNC, syncWaveformGen);

	/* OUTPUT HSYNC */
	syncWaveformGen.runValue = hDisp + hSyncStartWidth + hSyncWidth + hSyncEndWidth - 1;
	syncWaveformGen.runResolution = DI_COUNTER_BASECLK + 1;
	syncWaveformGen.offsetValue = delayH2V;
	syncWaveformGen.offsetResolution = DI_COUNTER_BASECLK + 1;
	syncWaveformGen.cntAutoReload = 1;
	syncWaveformGen.stepRepeat = 0;
	syncWaveformGen.cntClrSel = 0;
	syncWaveformGen.cntPolarityGenEn = 0;
	syncWaveformGen.cntPolarityTrigSel = 0;
	syncWaveformGen.cntPolarityClrSel = 0;
	syncWaveformGen.cntUp = 0;
	syncWaveformGen.cntDown = div * hSyncWidth;
	ipu_di_sync_config(0, DI_COUNTER_OHSYNC, syncWaveformGen);

	/* Output Vsync */
	syncWaveformGen.runValue = vDisp + vSyncStartWidth + vSyncWidth + vSyncEndWidth - 1;
	syncWaveformGen.runResolution = DI_COUNTER_IHSYNC + 1;
	syncWaveformGen.offsetValue = 0;
	syncWaveformGen.offsetResolution = 0;
	syncWaveformGen.cntAutoReload = 1;
	syncWaveformGen.stepRepeat = 0;
	syncWaveformGen.cntClrSel = 0;
	syncWaveformGen.cntPolarityGenEn = 1;
	syncWaveformGen.cntPolarityTrigSel = 2;
	syncWaveformGen.cntPolarityClrSel = 0;
	syncWaveformGen.cntUp = 0;
	syncWaveformGen.cntDown = vSyncWidth;
	ipu_di_sync_config(0, DI_COUNTER_OVSYNC, syncWaveformGen);

	/* Active Lines start points */
	syncWaveformGen.runValue = 0;
	syncWaveformGen.runResolution = DI_COUNTER_OHSYNC + 1;
	syncWaveformGen.offsetValue = vSyncWidth;
	syncWaveformGen.offsetResolution = DI_COUNTER_OHSYNC + 1;
	syncWaveformGen.cntAutoReload = 0;
	syncWaveformGen.stepRepeat = vDisp;
	syncWaveformGen.cntClrSel = DI_COUNTER_OVSYNC + 1;
	syncWaveformGen.cntPolarityGenEn = 0;
	syncWaveformGen.cntPolarityTrigSel = 0;
	syncWaveformGen.cntPolarityClrSel = 0;
	syncWaveformGen.cntUp = 0;
	syncWaveformGen.cntDown = 0;
	ipu_di_sync_config(0, DI_COUNTER_ALINE, syncWaveformGen);

	/* Active clock start points */
	syncWaveformGen.runValue = 0;
	syncWaveformGen.runResolution = DI_COUNTER_BASECLK + 1;
	syncWaveformGen.offsetValue = hSyncWidth;
	syncWaveformGen.offsetResolution = DI_COUNTER_BASECLK + 1;
	syncWaveformGen.cntAutoReload = 0;
	syncWaveformGen.stepRepeat = hDisp;
	syncWaveformGen.cntClrSel = DI_COUNTER_ALINE + 1;
	syncWaveformGen.cntPolarityGenEn = 0;
	syncWaveformGen.cntPolarityTrigSel = 0;
	syncWaveformGen.cntPolarityClrSel = 0;
	syncWaveformGen.cntUp = 0;
	syncWaveformGen.cntDown = 0;
	ipu_di_sync_config(0, DI_COUNTER_ACLOCK, syncWaveformGen);

	ipu_di_general_set(0, 1, 2, 1, 0);
}
