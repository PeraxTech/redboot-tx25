//==========================================================================
//
//	tx51_misc.c
//
//	HAL misc board support code for the tx51
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <stdlib.h>
#include <redboot.h>
#include <string.h>
#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>		// base types
#include <cyg/infra/cyg_trac.h>		// tracing macros
#include <cyg/infra/cyg_ass.h>		// assertion macros

#include <cyg/hal/hal_io.h>			// IO macros
#include <cyg/hal/hal_arch.h>		// Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>		// Interrupt names
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_soc.h>		// Hardware definitions
#include <cyg/hal/mx51_iomux.h>
#include CYGBLD_HAL_PLF_DEFS_H		// Platform specifics

#include <cyg/infra/diag.h>			// diag_printf

// All the MM table layout is here:
#include <cyg/hal/hal_mm.h>


/* MMU table definitions */
#define SD_P0		(RAM_BANK0_BASE >> 20)			/* physical RAM bank 0 address */
#define SD_C0		SD_P0							/* virtual address for cached 1:1 mapping */
#define SD_S0		(RAM_BANK0_SIZE >> 20)			/* RAM bank 0 size */
#define SD_U0		(UNCACHED_RAM_BASE_VIRT >> 20)
#ifdef RAM_BANK1_SIZE
#define SD_P1		(RAM_BANK1_BASE >> 20)			/* physical RAM bank 1 address */
#define SD_C1		(SD_P0 + SD_S0)
#define SD_S1		(RAM_BANK1_SIZE >> 20)			/* RAM bank 1 size */
#define SD_U1		(SD_U0 + SD_S0)
#define SD_HI		(SD_P1 + (SD_S1 - 1))
#endif

static unsigned long ttb_base = RAM_BANK0_BASE + 0x4000;

void hal_mmu_init(void)
{
	unsigned long i;

	/*
	 * Set the TTB register
	 */
	asm volatile ("mcr  p15,0,%0,c2,c0,0" : : "r"(ttb_base));

	/*
	 * Set the Domain Access Control Register
	 */
	i = ARM_ACCESS_DACR_DEFAULT;
	asm volatile ("mcr  p15,0,%0,c3,c0,0" : : "r"(i) /*:*/);

	/*
	 * First clear all TT entries - ie Set them to Faulting
	 */
	memset((void *)ttb_base, 0, ARM_FIRST_LEVEL_PAGE_TABLE_SIZE);

	/*		   Physical	 Virtual	 Size			Attributes				  access permissions		Function */
	/*		   Base	 Base	  MB			 cached?	   buffered?											 */
	/*		   xxx00000	 xxx00000										 */
	X_ARM_MMU_SECTION(0x000, 0x200,	0x001, ARM_CACHEABLE,	ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* Boot Rom */
	X_ARM_MMU_SECTION(0x1FF, 0x1FF, 0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* IRAM */
	X_ARM_MMU_SECTION(0x400, 0x400, 0x200, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* IPUv3D */
	X_ARM_MMU_SECTION(0x600, 0x600,	0x300, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* Internal Registers */
	X_ARM_MMU_SECTION(SD_P0, 0x000,	SD_S0, ARM_CACHEABLE,	ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(SD_P0, SD_C0,	SD_S0, ARM_CACHEABLE,	ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(SD_P0, SD_U0,	SD_S0, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* SDRAM (uncached) */
#ifdef RAM_BANK1_SIZE
	X_ARM_MMU_SECTION(SD_P1, SD_S0, SD_S1, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(SD_P1, SD_C1, SD_S1, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(SD_P1, SD_U1, SD_S1, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	/* make sure the last MiB in the upper bank of SDRAM (where RedBoot resides)
	 * has a unity mapping (required when switching MMU on) */
	X_ARM_MMU_SECTION(SD_HI, SD_HI, 0x001, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RO_RO); /* SDRAM bank1 identity mapping */
#endif
	X_ARM_MMU_SECTION(0xB80, 0xB80,	0x010, ARM_UNCACHEABLE,	ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* CS1 EIM control */
	X_ARM_MMU_SECTION(0xCC0, 0xCC0,	0x040, ARM_UNCACHEABLE,	ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* CS4/5/NAND Flash buffer */
}

static inline void set_reg(unsigned long addr, CYG_WORD32 set, CYG_WORD32 clr)
{
	CYG_WORD32 val;

	HAL_READ_UINT32(addr, val);
	val = (val & ~clr) | set;
	HAL_WRITE_UINT32(addr, val);
}

#define GPIO_BASE(grp)		(GPIO1_BASE_ADDR + (((grp) - 1) << 14))
static inline void setup_gpio(int grp, int bit)
{
	set_reg(GPIO_BASE(grp) + GPIO_DR, 0, 1 << bit);
	set_reg(GPIO_BASE(grp) + GPIO_GDIR, 1 << bit, 0);
}

//
// Platform specific initialization
//
static void uart_gpio_init(void)
{
	writel(0, IOMUXC_SW_MUX_CTL_PAD_UART1_TXD);
	writel(0, IOMUXC_SW_MUX_CTL_PAD_UART1_RXD);
	/* pad_ctl: PKE | PUE | DSE_HIGH | SRE */
	writel(0x0c5, IOMUXC_SW_PAD_CTL_PAD_UART1_TXD);
	/* pad_ctl: HYS | PKE | PUE | DSE_HIGH | SRE */
	writel(0x1c5, IOMUXC_SW_PAD_CTL_PAD_UART1_RXD);
	/* pad_ctl: HYS | PKE | PUE | DSE_HIGH */
	writel(0x1c4, IOMUXC_SW_PAD_CTL_PAD_UART1_RTS);
	writel(0x1c4, IOMUXC_SW_PAD_CTL_PAD_UART1_CTS);
}

/* GPIOs to set up for TX51/Starterkit-5:
   Function  FCT  GPIO        Pad        IOMUXC SW_PAD  SW_PAD mode
                                         OFFSET  CTRL    MUX
FEC_MDC       2   GPIO3_19 NANDF_CS3     0x13c   0x524
FEC_MDIO      3   GPIO2_22 EIM_EB2       0x0d4   0x468  0x954  0
FEC_RX_CLK    1   GPIO3_11 NANDF_RB3     0x128   0x504  0x968  0
FEC_RX_DV     2   GPIO3_29 NANDF_D11     0x164   0x54c  0x96c  0
FEC_RXD0      2   GPIO3_31 NANDF_D9      0x16c   0x554  0x958  0
FEC_RXD1      3   GPIO2_23 EIM_EB3       0x0d8   0x64c  0x95c  0
FEC_RXD2      3   GPIO2_27 EIM_CS2       0x0e8   0x47c  0x960  0
FEC_RXD3      3   GPIO2_28 EIM_CS3       0x0ec   0x480  0x964  0
FEC_RX_ER     3   GPIO2_29 EIM_CS4       0x0f0   0x484  0x970  0
FEC_TX_CLK    1   GPIO3_24 NANDF_RDY_INT 0x150   0x538  0x974  0
FEC_TX_EN     1   GPIO3_23 NANDF_CS7     0x14c   0x534
FEC_TXD0      2   GPIO4_0  NANDF_D8      0x170   0x558
FEC_TXD1      2   GPIO3_20 NANDF_CS4     0x140   0x528
FEC_TXD2      2   GPIO3_21 NANDF_CS5     0x144   0x52c
FEC_TXD3      2   GPIO3_22 NANDF_CS6     0x148   0x530
FEC_COL       1   GPIO3_10 NANDF_RB2     0x124   0x500  0x94c  0
FEC_CRS       3   GPIO2_30 EIM_CS5       0x0f4   0x488  0x950  0
FEC_TX_ER     2   GPIO3_18 NANDF_CS2     0x138   0x520            (INT)

FEC_RESET#    1   GPIO2_14 EIM_A20       0x0ac   0x440
FEC_ENABLE    0   GPIO1_3  GPIO1_3       0x3d0   0x7d8
---
OSC26M_ENABLE LP3972 GPIO2
*/
static void fec_gpio_init(void)
{
	/* setup GPIO data register to 0 and DDIR output for FEC PHY pins */
	setup_gpio(3, 19);
	setup_gpio(2, 22);
	setup_gpio(3, 11);
	setup_gpio(3, 29);
	setup_gpio(3, 31);
	setup_gpio(2, 23);
	setup_gpio(2, 27);
	setup_gpio(2, 28);
	setup_gpio(2, 29);
	setup_gpio(3, 24);
	setup_gpio(3, 23);
	setup_gpio(4, 0);
	setup_gpio(3, 20);
	setup_gpio(3, 21);
	setup_gpio(3, 22);
	setup_gpio(3, 10);
	setup_gpio(2, 30);
	setup_gpio(3, 18);

	setup_gpio(2, 14);
	setup_gpio(1, 3);

	/* setup input mux for FEC pins */
	HAL_WRITE_UINT32(IOMUXC_FEC_FEC_MDI_SELECT_INPUT, 0);
	HAL_WRITE_UINT32(IOMUXC_FEC_FEC_RX_CLK_SELECT_INPUT, 0);
	HAL_WRITE_UINT32(IOMUXC_FEC_FEC_RX_DV_SELECT_INPUT, 0);
	HAL_WRITE_UINT32(IOMUXC_FEC_FEC_RDATA_0_SELECT_INPUT, 0);
	HAL_WRITE_UINT32(IOMUXC_FEC_FEC_RDATA_1_SELECT_INPUT, 0);
	HAL_WRITE_UINT32(IOMUXC_FEC_FEC_RDATA_2_SELECT_INPUT, 0);
	HAL_WRITE_UINT32(IOMUXC_FEC_FEC_RDATA_3_SELECT_INPUT, 0);
	HAL_WRITE_UINT32(IOMUXC_FEC_FEC_RX_ER_SELECT_INPUT, 0);
	HAL_WRITE_UINT32(IOMUXC_FEC_FEC_TX_CLK_SELECT_INPUT, 0);
	HAL_WRITE_UINT32(IOMUXC_FEC_FEC_COL_SELECT_INPUT, 0);
	HAL_WRITE_UINT32(IOMUXC_FEC_FEC_CRS_SELECT_INPUT, 0);

	/* setup FEC PHY pins for GPIO function (with SION set) */
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_CS3, 2 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_EIM_EB2, 3 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_RB3, 1 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_D11, 2 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_D9, 2 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_EIM_EB3, 3 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_EIM_CS2, 3 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_EIM_CS3, 3 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_EIM_CS4, 3 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_RDY_INT, 1 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_CS7, 1 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_D8, 2 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_CS4, 2 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_CS5, 2 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_CS6, 2 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_RB2, 1 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_EIM_CS5, 3 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_NANDF_CS2, 3 | 0x10);

	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_EIM_A20, 1 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_SW_MUX_CTL_PAD_GPIO1_3, 0 | 0x10);
}

#ifdef CYGHWR_MX51_LCD_LOGO
void mxc_ipu_iomux_config(void)
{
	// configure display data0~17 for LCD panel
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT0, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT0, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT1, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT1, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT2, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT2,0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT3, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT3, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT4, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT4, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT5, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT5, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT6, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT6, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT7, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT7, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT8, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT8, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT9, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT9, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT10, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT10, 0x5);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT11, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT12, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT12, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT13, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT13, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT14, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT14, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT15, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT15, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT16, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT16, 0x5);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DISP1_DAT17, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DISP1_DAT17, 0x5);

	// DI1_PIN2 and DI1_PIN3, configured to be HSYNC and VSYNC of LCD
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DI1_PIN2, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DI1_PIN3, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);

	// PCLK - DISP_CLK
	// No IOMUX configuration required, as there is no IOMUXing for this pin

	// DRDY - PIN15
	// No IOMUX configuration required, as there is no IOMUXing for this pin

	// configure this pin to be the SER_DISP_CS

	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DI1_D1_CS, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT4);
	CONFIG_PIN(IOMUXC_SW_PAD_CTL_PAD_DI1_D1_CS, 0x85);

	// configure to be DISPB1_SER_RS
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DI_GP1, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DI_GP1, 0x85);
	// configure to be SER_DISP1_CLK
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DI_GP2, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DI_GP2, 0x85);
	// configure to be DISPB1_SER_DIO
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DI_GP3, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DI_GP3, 0xC5);
	// configure to be DISPB1_SER_DIN
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DI_GP4, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DI_GP4, 0xC4);
	//CS0
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DI1_D0_CS, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT1);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DI1_D0_CS, 0x85);
	// WR
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DI1_PIN11, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT1);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DI1_PIN11, 0x85);
	// RD
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DI1_PIN12, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT1);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DI1_PIN12, 0x85);
	// RS
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_DI1_PIN13, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT1);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_DI1_PIN13, 0x85);

	/* LCD Power Enable GPIO4_14 (active High) */
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_CSI2_HSYNC, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT3);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_CSI2_HSYNC, 0x04);
	gpio_set_bit(4, 14);

	/* LCD Reset GPIO4_13 (active Low) */
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_CSI2_VSYNC, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT3);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_CSI2_VSYNC, 0x04);
	gpio_set_bit(4, 13);

	/* LCD Backlight GPIO1_2 (PWM 0: full brightness 1: off) */
	CONFIG_PIN(IOMUXC_SW_MUX_CTL_PAD_GPIO1_2, IOMUX_PIN_SION_REGULAR | IOMUX_SW_MUX_CTL_ALT0);
	CONFIG_PAD(IOMUXC_SW_PAD_CTL_PAD_GPIO1_2, 0x04);
	gpio_clr_bit(1, 2);
}
RedBoot_init(mxc_ipu_iomux_config, RedBoot_INIT_SECOND);
#endif

//
// Platform specific initialization
//

void plf_hardware_init(void)
{
#ifdef RAM_BANK1_SIZE
	/* destroy mapping for high area in SDRAM */
	X_ARM_MMU_SECTION(SD_HI, 0, 0, 0, 0, ARM_ACCESS_PERM_NONE_NONE);
#endif
	uart_gpio_init();
	fec_gpio_init();

	/* NFC setup */
	writel(readl(NFC_FLASH_CONFIG3_REG) |
			(1 << 15) | /* assert RBB_MODE  (see Errata: ENGcm09970) */
			(1 << 20) | /* assert NO_SDMA */
			(1 << 3), /* set bus width to 8bit */
			NFC_FLASH_CONFIG3_REG);

	/* configure MIPI-HSC legacy mode required to get data on DISP1_DAT[0..5] */
	writel(0xf00, MIPI_HSC_BASE_ADDR);
	/* bypass MDT and enable legacy CSI interface */
	writel((3 << 16) | (2 << 2) | (2 << 0), MIPI_HSC_BASE_ADDR + 0x800);
}

static int lp3972_reg_read(cyg_uint8 reg)
{
	int ret;
	ret = -ENOSYS;
	return ret;
}

static int lp3972_reg_write(cyg_uint8 reg, cyg_uint8 val)
{
	int ret;
	ret = -ENOSYS;
	return ret;
}

int tx51_mac_addr_program(unsigned char mac_addr[ETHER_ADDR_LEN])
{
	int ret = 0;
	int i;

	for (i = 0; i < ETHER_ADDR_LEN; i++) {
		unsigned char fuse = readl(SOC_FEC_MAC_BASE +
								ETHER_ADDR_LEN * 4 - ((i + 1) << 2));

		if ((fuse | mac_addr[i]) != mac_addr[i]) {
			diag_printf("MAC address fuse cannot be programmed: fuse[%d]=0x%02x -> 0x%02x\n",
						i, fuse, mac_addr[i]);
			return -1;
		}
		if (fuse != mac_addr[i]) {
			ret = 1;
		}
	}
	if (ret == 0) {
		return ret;
	}
	ret = lp3972_reg_write(0x39, 0xf0);
	if (ret < 0 && ret != -ENOSYS) {
		diag_printf("Failed to switch fuse programming voltage: %d\n", ret);
		return ret;
	}
	ret = lp3972_reg_read(0x39);
	if (ret != -ENOSYS && ret != 0xf0) {
		diag_printf("Failed to switch fuse programming voltage: %d\n", ret);
		return ret;
	}
	for (i = 0; i < ETHER_ADDR_LEN; i++) {
		unsigned char fuse = readl(SOC_FEC_MAC_BASE +
								ETHER_ADDR_LEN * 4 - ((i + 1) << 2));
		int row = SOC_MAC_ADDR_FUSE + ETHER_ADDR_LEN - 1 - i;

		if (fuse == mac_addr[i]) {
			continue;
		}
		fuse_blow_row(SOC_MAC_ADDR_FUSE_BANK, row, mac_addr[i]);
		ret = sense_fuse(SOC_MAC_ADDR_FUSE_BANK, row, 0);
		if (ret != mac_addr[i]) {
			diag_printf("Failed to verify fuse bank %d row %d; expected %02x got %02x\n",
						SOC_MAC_ADDR_FUSE_BANK, row, mac_addr[i], ret);
			goto out;
		}
	}
#ifdef SOC_MAC_ADDR_LOCK_BIT
	fuse_blow_row(SOC_MAC_ADDR_FUSE_BANK, SOC_MAC_ADDR_LOCK_FUSE,
				(1 << SOC_MAC_ADDR_LOCK_BIT));
#endif
out:
	lp3972_reg_write(0x39, 0);
	return ret;
}

#include CYGHWR_MEMORY_LAYOUT_H

typedef void code_fun(void);

void tx51_program_new_stack(void *func)
{
	register CYG_ADDRESS stack_ptr asm("sp");
	register CYG_ADDRESS old_stack asm("r4");
	register code_fun *new_func asm("r0");
	old_stack = stack_ptr;
	stack_ptr = CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE - sizeof(CYG_ADDRESS);
	new_func = (code_fun*)func;
	new_func();
	stack_ptr = old_stack;
}

void increase_core_voltage(bool i)
{
	int ret;

	ret = lp3972_reg_read(0x23);
	if (ret < 0) {
		diag_printf("Failed to read core voltage: %d\n", ret);
	}

	if (i) {
		/* Set core voltage to 1.175V */
		ret = 0x12;
	} else {
		/* Set core voltage to 1.05V */
		ret = 0x0d;
	}

	ret = lp3972_reg_write(0x23, ret);
	if (ret < 0) {
		diag_printf("Failed to write core voltage: %d\n", ret);
	}
}

static void display_board_type(void)
{
	diag_printf("\nBoard Type: Ka-Ro TX51-8xx%d\n",
		SDRAM_SIZE > SZ_128M);
}

static void display_board_info(void)
{
	display_board_type();
}
RedBoot_init(display_board_info, RedBoot_INIT_LAST);

void mxc_i2c_init(unsigned int module_base)
{
	switch (module_base) {
	case I2C_BASE_ADDR:
		writel(0x11, IOMUXC_BASE_ADDR + 0x210);
		writel(0x1ad, IOMUXC_BASE_ADDR + 0x600);
		writel(0x1, IOMUXC_BASE_ADDR + 0x9B4);

		writel(0x11, IOMUXC_BASE_ADDR + 0x224);
		writel(0x1ad, IOMUXC_BASE_ADDR + 0x614);
		writel(0x1, IOMUXC_BASE_ADDR + 0x9B0);
		break;
	case I2C2_BASE_ADDR:
		writel(0x12, IOMUXC_BASE_ADDR + 0x3CC);  // i2c SCL
		writel(0x3, IOMUXC_BASE_ADDR + 0x9B8);
		writel(0x1ed, IOMUXC_BASE_ADDR + 0x7D4);

		writel(0x12, IOMUXC_BASE_ADDR + 0x3D0); // i2c SDA
		writel(0x3, IOMUXC_BASE_ADDR + 0x9BC);
		writel(0x1ed, IOMUXC_BASE_ADDR + 0x7D8);
		break;
	default:
		diag_printf("Invalid I2C base: 0x%x\n", module_base);
		return;
	}
}
