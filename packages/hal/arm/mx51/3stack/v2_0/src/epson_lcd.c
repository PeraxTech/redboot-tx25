//==========================================================================
//
//      epson_lcd.c
//
//      LCD Display Implementation
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================

#include <cyg/io/ipu_common.h>
#include <cyg/io/mxc_i2c.h>

#define MX51_ATLAS_PMIC_ADDRESS 0x8

static int pmic_read_reg(unsigned int reg_addr, unsigned char *data, unsigned int count)
{
    struct mxc_i2c_request rq;

    rq.dev_addr = MX51_ATLAS_PMIC_ADDRESS;  // dev_addr of Atlas PMIC
    rq.reg_addr = reg_addr;     // addr of LEC Control0 Reg
    rq.reg_addr_sz = 1;
    rq.buffer_sz = count;           // send 3 data in a series
    rq.buffer = data;
    i2c_xfer(1, &rq, 1);

    return 1;
}

static int pmic_write_reg(unsigned int reg_addr, unsigned char *data, unsigned int count)
{
    struct mxc_i2c_request rq;

    rq.dev_addr = MX51_ATLAS_PMIC_ADDRESS;  // dev_addr of Atlas PMIC
    rq.reg_addr = reg_addr;     // addr of LEC Control0 Reg
    rq.reg_addr_sz = 1;
    rq.buffer_sz = count;           // send 3 data in a series
    rq.buffer = data;
    i2c_xfer(1, &rq, 0);

    return 1;
}

/*this function use common pins of IPU to simulate a cspi interface*/
static void epson_lcd_spi_simulate(void)
{
    dc_microcode_t microcode = { 0 };
    microcode.addr = 0x24;
    microcode.stop = 1;
    microcode.opcode = "WROD";
    microcode.lf = 0;
    microcode.af = 0;
    microcode.operand = 0;
    microcode.mapping = 5;
    microcode.waveform = 7;
    microcode.gluelogic = 0;
    microcode.sync = 0;
    ipu_dc_microcode_config(microcode);

    ipu_write_field(IPU_DC_RL3_CH_8__COD_NEW_DATA_START_CHAN_W_8_1, 0x24);  //address of second region
    ipu_write_field(IPU_DC_RL3_CH_8__COD_NEW_DATA_START_CHAN_W_8_0, 0x24);  //address of first region
    ipu_write_field(IPU_DC_RL3_CH_8__COD_NEW_DATA_PRIORITY_CHAN_8, 1);  //MEDIUM PRIORITY FOR DATA

    /*  Data Mapping of 24-bit new data
       |23..16|15..8|7..0| ==>> bit[15..0]&(0x1FF), just keep the last 17bit for LCD configuration */
    ipu_write_field(IPU_DC_MAP_CONF_2__MAPPING_PNTR_BYTE2_4, 3);
    ipu_write_field(IPU_DC_MAP_CONF_2__MAPPING_PNTR_BYTE1_4, 4);
    ipu_write_field(IPU_DC_MAP_CONF_2__MAPPING_PNTR_BYTE0_4, 5);
    ipu_write_field(IPU_DC_MAP_CONF_16__MD_OFFSET_3, 0);
    ipu_write_field(IPU_DC_MAP_CONF_16__MD_MASK_3, 0x00);
    ipu_write_field(IPU_DC_MAP_CONF_17__MD_OFFSET_4, 15);
    ipu_write_field(IPU_DC_MAP_CONF_17__MD_MASK_4, 0x01);
    ipu_write_field(IPU_DC_MAP_CONF_17__MD_OFFSET_5, 7);
    ipu_write_field(IPU_DC_MAP_CONF_17__MD_MASK_5, 0xFF);

    /*set clock and cs signal for command.
       sclk should be more than 90ns interval, derived from base clock. */
    ipu_di_waveform_config(0, 6, 0, 0, 11);
    ipu_di_waveform_config(0, 6, 1, 1, 2);
    ipu_di_waveform_config(0, 6, 2, 0, 0);
    ipu_write_field(IPU_DI0_DW_GEN_6__DI0_SERIAL_PERIOD_6, 3);  //base clock, 133MHz/div
    ipu_write_field(IPU_DI0_DW_GEN_6__DI0_START_PERIOD_6, 0);   //start immediatly
    ipu_write_field(IPU_DI0_DW_GEN_6__DI0_CST_6, 0);    //pointer for CS
    ipu_write_field(IPU_DI0_DW_GEN_6__DI0_SERIAL_VALID_BITS_6, 8);  //8+1 bit, should be more than or equal with 9
    ipu_write_field(IPU_DI0_DW_GEN_6__DI0_SERIAL_RS_6, 2);  //RS=0
    ipu_write_field(IPU_DI0_DW_GEN_6__DI0_SERIAL_CLK_6, 1); //SCLK for command, should be less than 11MHz
    ipu_write_field(IPU_DI0_SER_CONF__DI0_SER_CLK_POLARITY, 1);
    ipu_write_field(IPU_IPU_DISP_GEN__MCU_DI_ID_8, 0);  //MCU accesses DC's channel #8 via DI0.

    /* T VALUE, seperate into two parts */
    ipu_write_field(IPU_IPU_DISP_GEN__MCU_T, T_VALUE);  //diffrenciate

    ipu_write_field(IPU_DC_WR_CH_CONF1_8__MCU_DISP_ID_8, 1);    //display 1
    ipu_write_field(IPU_DC_WR_CH_CONF1_8__W_SIZE_8, 3); //32 bits are used (RGB)

    ipu_write_field(IPU_DC_DISP_CONF1_1__DISP_TYP_1, 0);    //serial display

    ipu_write_field(IPU_IPU_CONF__DI0_EN, 1);
    ipu_write_field(IPU_IPU_CONF__DP_EN, 0);
    ipu_write_field(IPU_IPU_CONF__DC_EN, 1);
    ipu_write_field(IPU_IPU_CONF__DMFC_EN, 1);
}

static void epson_lcd_rst(void)
{
    ipu_write_field(IPU_DI1_GENERAL__DI1_POLARITY_4, 1);
    hal_delay_us(1000);
    ipu_write_field(IPU_DI1_GENERAL__DI1_POLARITY_4, 0);
}

void lcd_backlit_on(void)
{
    struct mxc_i2c_request rq;
    unsigned char data[3];
    unsigned char dataCheck[3];
    int timeout = 0;
    int ret = 0xFF;
    /*duty cycle = (mainDispDutyCycle % 32) / 32; */
    unsigned char mainDispDutyCycle = 0x20;
    /*current = mainDispCurrentSet * 3 * 2^mainDispHiCurMode
       current should be no more than 15 */
    unsigned char mainDispCurrentSet = 3;
    unsigned char mainDispHiCurMode = 0;

#ifndef CYGPKG_REDBOOT
    mxc_i2c2_clock_gate(1);
#endif
    if (i2c_init(I2C2_BASE_ADDR, 170000) == 0) {
        while (timeout < 5) {
            data[0] = 0x0;
            data[1] = ((mainDispCurrentSet << 1) & 0xE) | ((mainDispDutyCycle >> 5) & 0x1);
            data[2] = ((mainDispHiCurMode << 1) & 0x2) | ((mainDispDutyCycle << 3) & 0xF8);
            pmic_write_reg(0x33, data, 3);
            dataCheck[0] = 0x0;
            dataCheck[1] = 0x0;
            dataCheck[2] = 0x0;
            pmic_read_reg(0x33, dataCheck, 3);

            if ((dataCheck[0] == data[0]) && (dataCheck[1] == data[1]) && (dataCheck[2] == data[2])) {
                break;
            }
            timeout++;
            hal_delay_us(20);
        }
    } else {
        diag_printf("ERROR:I2C initialization failed\n");
    }
#ifndef CYGPKG_REDBOOT
    mxc_i2c2_clock_gate(0);
#endif
}

void lcd_config(void)
{
    /* set these regs to conpensate color. */
    writel((readl(MIPI_HSC_BASE_ADDR + 0x800) | (1<<16)), MIPI_HSC_BASE_ADDR + 0x800);
    writel((readl(MIPI_HSC_BASE_ADDR + 0x0) | (1<<10)), MIPI_HSC_BASE_ADDR + 0x0);

    /* simulate spi interface to access LCD regs */
    epson_lcd_spi_simulate();
    epson_lcd_rst();

    /* enable chip select */
    gpio_dir_config(GPIO_PORT3, 4, GPIO_GDIR_OUTPUT);
    gpio_write_data(GPIO_PORT3, 4, 0);
    hal_delay_us(300);

    writel(MADCTL, IPU_CTRL_BASE_ADDR);
    writel(0x0100, IPU_CTRL_BASE_ADDR);

    writel(GAMSET, IPU_CTRL_BASE_ADDR);
    writel(0x0101, IPU_CTRL_BASE_ADDR);

    writel(COLMOD, IPU_CTRL_BASE_ADDR);
    writel(0x0160, IPU_CTRL_BASE_ADDR);

    writel(SLPOUT, IPU_CTRL_BASE_ADDR); // SLEEP OUT
    hal_delay_us(300);

    writel(DISON, IPU_CTRL_BASE_ADDR);  // Display ON
    hal_delay_us(300);

    /* disable chip select */
    gpio_dir_config(GPIO_PORT3, 4, GPIO_GDIR_OUTPUT);
    gpio_write_data(GPIO_PORT3, 4, 1);

    ipu_write_field(IPU_IPU_CONF__DI0_EN, 0);
    ipu_write_field(IPU_IPU_CONF__DP_EN, 0);
    ipu_write_field(IPU_IPU_CONF__DC_EN, 0);
    ipu_write_field(IPU_IPU_CONF__DMFC_EN, 0);
    hal_delay_us(300);
}
