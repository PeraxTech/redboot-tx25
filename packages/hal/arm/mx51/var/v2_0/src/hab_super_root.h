//==========================================================================
//
//      hab_super_root.h
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
#ifndef HAB_SUPER_ROOT_H
#define HAB_SUPER_ROOT_H

#ifdef __cplusplus
extern "C" {
#endif

/*==================================================================================================

     Header Name: hab_super_root.h

     General Description: This module contains the HAB Super Root public keys.

====================================================================================================*/

/* Generic type definitions */
typedef signed char		   INT8;
typedef unsigned char	   UINT8;
typedef short int		   INT16;
typedef unsigned short int UINT16;
typedef int				   INT32;
typedef unsigned int	   UINT32;
typedef unsigned char	   BOOLEAN;


/* HAB specific type definitions */
typedef UINT8			   *hab_bytestring;
typedef UINT16			   hab_algorithm;
typedef UINT8			   hab_index;
typedef UINT32			   hab_address;
typedef UINT8			   hab_certificate;
typedef UINT32			   hab_data_length;
typedef UINT16			   hab_int_length;
typedef UINT8			   hab_error;

#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/* HAB specific definitions */
#define HAB_MAX_EXP_SIZE   ((hab_int_length)4) /* Maximum size of RSA
                                                * public key exponent
                                                * - in bytes
                                                */

/*==================================================================================================
                                            MACROS
==================================================================================================*/

/*==================================================================================================
                                             ENUMS
==================================================================================================*/

/*==================================================================================================
                                 STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/* RSA public key structure */
typedef struct
{
	UINT8			rsa_exponent[HAB_MAX_EXP_SIZE]; /* RSA public exponent */
	const UINT8		*rsa_modulus;					/* RSA modulus pointer */
	hab_int_length	exponent_size;					/* Exponent size in bytes */
	hab_int_length	modulus_size;					/* Modulus size in bytes */
	BOOLEAN			init_flag;						/* Indicates if key initialised */
} hab_rsa_public_key;

/*==================================================================================================
                                 GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/
/* Super Root keys */
extern const hab_rsa_public_key hab_super_root_key[];

#ifdef __cplusplus
}
#endif

#endif  /* HAB_SUPER_ROOT_H */
