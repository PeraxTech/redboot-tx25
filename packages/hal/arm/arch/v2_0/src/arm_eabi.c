#include <unwind.h>
#include <cyg/infra/diag.h>

_Unwind_Reason_Code __aeabi_unwind_cpp_pr0(_Unwind_State state,
                                           _Unwind_Control_Block *ucbp,
                                           _Unwind_Context *context)
{
	diag_printf("EXCEPTION CAUGHT\n");
	while (1);
	return _URC_FAILURE;
}

