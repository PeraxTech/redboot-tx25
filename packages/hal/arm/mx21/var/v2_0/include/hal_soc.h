//==========================================================================
//
//      hal_soc.h
//
//      HAL misc board support definitions
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
// Copyright (C) 2002 Gary Thomas
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#ifndef __HAL_SOC_H__
#define __HAL_SOC_H__

#ifdef __ASSEMBLER__

#define REG8_VAL(a)  (a)
#define REG16_VAL(a) (a)
#define REG32_VAL(a) (a)

#define REG8_PTR(a)  (a)
#define REG16_PTR(a) (a)
#define REG32_PTR(a) (a)

#else /* __ASSEMBLER__ */

extern char HAL_PLATFORM_EXTRA[];
#define REG8_VAL(a)         ((unsigned char)(a))
#define REG16_VAL(a)        ((unsigned short)(a))
#define REG32_VAL(a)        ((unsigned int)(a))

#define REG8_PTR(a)         ((volatile unsigned char *)(a))
#define REG16_PTR(a)        ((volatile unsigned short *)(a))
#define REG32_PTR(a)         ((volatile unsigned int *)(a))
#define readb(a)             (*(volatile unsigned char *)(a))
#define readw(a)             (*(volatile unsigned short *)(a))
#define readl(a)             (*(volatile unsigned int *)(a))
#define writeb(v,a)          (*(volatile unsigned char *)(a) = (v))
#define writew(v,a)          (*(volatile unsigned short *)(a) = (v))
#define writel(v,a)          (*(volatile unsigned int *)(a) = (v))

#endif /* __ASSEMBLER__ */

/*
 * Default Memory Layout Definitions
 */

/*
  * Function Multiplexing Control Register (FMCR)
  */
#define MX21_FMCR                 0x10027814
/*
 * MX21 IRQ Controller Register Definitions.
 */
#define MX21_AITC_BASE              0x10040000
#define MX21_AITC_NIMASK            0x10040004
#define MX21_AITC_INTTYPEH          0x10040018
#define MX21_AITC_INTTYPEL          0x10040014
#define MX21_AITC_NIPRIORITY3       0x10040030
#define MX21_AITC_NIPRIORITY2       0x10040034
#define MX21_AITC_NIPRIORITY1       0x10040038
#define MX21_AITC_NIPRIORITY0       0x1004003C

/*
 * MX21 UART Base Addresses
 */
#define MX21_UART1_BASE             0x1000A000
#define MX21_UART2_BASE             0x1000B000
#define MX21_UART3_BASE             0x1000C000
#define MX21_UART4_BASE             0x1000D000

#define MX21_AIPI1_BASE             0x10000000 /*  AIPI 1    */
#define MX21_AIPI2_BASE             0x10020000 /*  AIPI 2    */
#define MX21_CRM_SysCtrl_BASE       0x10027800 /*  system control   */
#define CRM_SysCtrl_PCSR_Offset     0x50

#define MX21_MAX_BASE               0x1003F000 /*  MAX       */
#define MAX_Port0_OFFSET            0x0
#define MAX_Port1_OFFSET            0x100
#define MAX_Port2_OFFSET            0x200
#define MAX_Port3_OFFSET            0x300
#define MAX_Port4_OFFSET            0x400
#define MAX_Port5_OFFSET            0x500

#define MAX_Slave_MPR_Offset        0x0         /* Master Priority register */
#define MAX_Slave_AMPR_Offset       0x4         /* Alternate Master Priority register */
#define MAX_Slave_SGPCR_Offset      0x10        /* Slave General Purpose Control register */
#define MAX_Slave_ASGPCR_Offset     0x14        /* Alternate Slave General Purpose control register */
#define MAX_Master_MGPCR_Offset     0x800       /* Master General Purpose Control Register */
#define MX21_CRM_BASE               0x10027000  /*  RST/CLK   */
#define MX21_CRM_CSCR               0x00        /*  clock source control reg */
#define MX21_CRM_MPCTL0             0x04        /*  MCU & System PLL control register 0 */
#define MX21_CRM_MPCTL1             0x08        /*  MCU & System PLL control register 1 */
#define MX21_CRM_SPCTL0             0x0C        /*  serial peripheral PLL control register 0 */
#define MX21_CRM_SPCTL1             0x10        /*  serial peripheral PLL control register 1 */
#define MX21_CRM_PCDR0              0x18        /*  peripheral clock divider register 0 */
#define MX21_CRM_PCDR1              0x1C        /*  peripheral clock divider register 1 */
#define MX21_CRM_PCCR0              0x20        /*  peripheral clock control register 0 */
#define MX21_CRM_PCCR1              0x24        /*  peripheral clock control register 1 */
#define MX21_CRM_CCSR               0x28        /*  Clock Control Status Register */
#define CRM_CSCR_USBDIV             (0x5<<26)
#define CRM_CSCR_SD_CNT             (0x3<<24)
#define CRM_CSCR_SPLL_Restart       (0x1<<22)
#define CRM_CSCR_MPLL_Restart       (0x1<<21)
#define CRM_CSCR_Prescaler          0
#define CRM_CSCR_BCLKDIV            (0x1<<10)
#define CRM_CSCR_IPDIV              0
#define CRM_CSCR_FPM_EN             (0x1<<2)
#define CRM_CSCR_SPEN               (0x1<<1)
#define CRM_CSCR_MPEN               (0x1<<0)
    /* for MPCTL0 */
#define CRM_MPCTL0_PD               (0<<26)
#define CRM_MPCTL0_MFD              (123<<16)
#define CRM_MPCTL0_MFI              (7<<10)
#define CRM_MPCTL0_MFN              115
    /* SPCTL0 */
#define CRM_SPCTL0_PD               (0<<26)
#define CRM_SPCTL0_MFD              (944<<16)
#define CRM_SPCTL0_MFI              (8<<10)
#define CRM_SPCTL0_MFN              551

#define FREQ_26MHZ                  26000000
#define FREQ_32768HZ                (32768 * 512)
#define FREQ_32000HZ                (32000 * 512)

#define PLL_REF_CLK                 FREQ_32768HZ
//#define PLL_REF_CLK  FREQ_26MHZ
//#define PLL_REF_CLK  FREQ_32000HZ

/*
 * MX21 GPIO Register Definitions
 */
#define MX21_GPIOA_BASE             0x10015000
#define MX21_GPIOB_BASE             0x10015100
#define MX21_GPIOC_BASE             0x10015200
#define MX21_GPIOD_BASE             0x10015300
#define MX21_GPIOE_BASE             0x10015400
#define MX21_GPIOF_BASE             0x10015500
#define MX21_GPIO_PMASK             0x10015600
#define KHwGpioDDIR                 0x000         /* Data direction reg */
#define KHwGpioOCR1                 0x004         /* Output config reg 1 */
#define KHwGpioOCR2                 0x008         /* Output config reg 2 */
#define KHwGpioICONFA1              0x00C         /* Input config reg A1 */
#define KHwGpioICONFA2              0x010         /* Input config reg A2 */
#define KHwGpioICONFB1              0x014         /* Input config reg B1 */
#define KHwGpioICONFB2              0x018         /* Input config reg B2 */
#define KHwGpioDR                   0x01C         /* Data reg */
#define KHwGpioGIUS                 0x020         /* GPIO in use reg */
#define KHwGpioSSR                  0x024         /* Sample startus reg */
#define KHwGpioICR1                 0x028         /* Int config reg 1 */
#define KHwGpioICR2                 0x02C         /* Int config reg 2 */
#define KHwGpioIMR                  0x030         /* Int mask reg */
#define KHwGpioISR                  0x034         /* Int status reg */
#define KHwGpioGPR                  0x038         /* Gen purpose reg */
#define KHwGpioSWR                  0x03C         /* Software reset reg */
#define KHwGpioPUEN                 0x040         /* Pull-up enable reg */

#define EGpioOCR_A                  0            /* External input a_IN */
#define EGpioOCR_B                  1            /* External input b_IN */
#define EGpioOCR_C                  2            /* External input c_IN */
#define EGpioOCR_DR                 3            /* Data register */
#define EGpioICONF_In               0            /* GPIO-in */
#define EGpioICONF_Isr              1            /* Interrupt status register */
#define EGpioICONF_0                2            /* 0 */
#define EGpioICONF_1                3            /* 1 */
#define EGpioICR_PosEdge            0            /* Positive edge */
#define EGpioICR_NegEdge            1            /* Negative edge */
#define EGpioICR_PosLvl             2            /* Positive level */
#define EGpioICR_NegLvl             3            /* Negative level */
#define EGpioSWR_SWR                1              /* Software reset */

/*
 * MX2 timer defines
 */
#define HAL_DELAY_TIMER_MX2         MX21_Timer2_BASE  // use timer2 for hal_delay_us()

#define MX21_Timer1_BASE            0x10003000
#define MX21_Timer2_BASE            0x10004000
#define MX21_Timer3_BASE            0x10005000
#define KHwTimerTCL                 0x00
#define KHwTimerTPRER               0x04
#define KHwTimerTCMP                0x08
#define KHwTimerTCR                 0x0C
#define KHwTimerTCN                 0x10
#define KHwTimerTSTAT               0x14
#define MX_STARTUP_DELAY            (1000000/10)  // 0.1s delay to get around the ethernet reset failure problem

#define MX21_PERCLK1                44333342    /* Peripheral Clock 1 */
#define DelayTimerPresVal           3
#define MX21_SI_ID_REG              0x1002780C
//CS_IN0 is connected to the BoardID chip on MX21 ADS board.
#define MX21_CS_IN0_BASE            0xCC400000
#define MX21_SILICONID_Rev2_x       0x001D101D
#define MX21_SILICONID_Rev3_0       0x101D101D
#define MX21_SILICONID_Rev3_1       0x201D101D
#define CHIP_REV_1_x                1
#define CHIP_REV_2_x                2
#define CHIP_REV_3_0                3
#define CHIP_REV_3_1                4
#define CHIP_REV_unknown            0x100

#define MX21_DSCR_BASE              0x10027820
#define DSCR2                       0x04
#define DSCR3                       0x08
#define DSCR4                       0x0C
#define DSCR5                       0x10
#define DSCR6                       0x14
#define DSCR7                       0x18
#define DSCR8                       0x1C
#define DSCR9                       0x20
#define DSCR10                      0x24
#define DSCR11                      0x28

#define WDOG_BASE_ADDR              0x10002000

#define ESDCTL_BASE                 0xDF000000
#define NFC_BASE                    0xDF003000
#define MX21_EIM_BASE               0xDF001000  /*  EIM       */
#define MX21_CS1_LOCTRL             0x0C        /* CS1 lower control register */
#define MX21_CS1_UPCTRL             0x08        /* CS1 upper control register */

// External I/O
#define MX21_CS1_BASE               0xCC000000

// Memories
#define CSD0_BASE_ADDR                  0xC0000000
#define CS0_BASE_ADDR                   0xC8000000
#define NAND_REG_BASE                   (NFC_BASE + 0xE00)
#define NFC_BUFSIZE_REG_OFF             (0 + 0x00)
#define RAM_BUFFER_ADDRESS_REG_OFF      (0 + 0x04)
#define NAND_FLASH_ADD_REG_OFF          (0 + 0x06)
#define NAND_FLASH_CMD_REG_OFF          (0 + 0x08)
#define NFC_CONFIGURATION_REG_OFF       (0 + 0x0A)
#define ECC_STATUS_RESULT_REG_OFF       (0 + 0x0C)
#define ECC_RSLT_MAIN_AREA_REG_OFF      (0 + 0x0E)
#define ECC_RSLT_SPARE_AREA_REG_OFF     (0 + 0x10)
#define NF_WR_PROT_REG_OFF              (0 + 0x12)
#define UNLOCK_START_BLK_ADD_REG_OFF    (0 + 0x14)
#define UNLOCK_END_BLK_ADD_REG_OFF      (0 + 0x16)
#define NAND_FLASH_WR_PR_ST_REG_OFF     (0 + 0x18)
#define NAND_FLASH_CONFIG1_REG_OFF      (0 + 0x1A)
#define NAND_FLASH_CONFIG2_REG_OFF      (0 + 0x1C)
#define RAM_BUFFER_ADDRESS_RBA_3        0x3
#define NFC_BUFSIZE_1KB                 0x0
#define NFC_BUFSIZE_2KB                 0x1
#define NFC_CONFIGURATION_UNLOCKED      0x2
#define ECC_STATUS_RESULT_NO_ERR        0x0
#define ECC_STATUS_RESULT_1BIT_ERR      0x1
#define ECC_STATUS_RESULT_2BIT_ERR      0x2
#define NF_WR_PROT_UNLOCK               0x4
#define NAND_FLASH_CONFIG1_FORCE_CE     (1 << 7)
#define NAND_FLASH_CONFIG1_RST          (1 << 6)
#define NAND_FLASH_CONFIG1_BIG          (1 << 5)
#define NAND_FLASH_CONFIG1_INT_MSK      (1 << 4)
#define NAND_FLASH_CONFIG1_ECC_EN       (1 << 3)
#define NAND_FLASH_CONFIG1_SP_EN        (1 << 2)
#define NAND_FLASH_CONFIG2_INT_DONE     (1 << 15)
#define NAND_FLASH_CONFIG2_FDO_PAGE     (0 << 3)
#define NAND_FLASH_CONFIG2_FDO_ID       (2 << 3)
#define NAND_FLASH_CONFIG2_FDO_STATUS   (4 << 3)
#define NAND_FLASH_CONFIG2_FDI_EN       (1 << 2)
#define NAND_FLASH_CONFIG2_FADD_EN      (1 << 1)
#define NAND_FLASH_CONFIG2_FCMD_EN      (1 << 0)
#define FDO_PAGE_SPARE_VAL              0x8

#define NOR_FLASH_BOOT                     0
#define NAND_FLASH_BOOT                  0x10
#define SDRAM_NON_FLASH_BOOT        0x20
#define MMC_BOOT                                 0x40
#define MXCBOOT_FLAG_REG                (MX21_AITC_BASE + 0x20)

#define MXCFIS_NOTHING                  0x00000000
#define MXCFIS_NAND                     0x10000000
#define MXCFIS_NOR                      0x20000000
#define MXCFIS_MMC                      0x40000000
#define MXCFIS_FLAG_REG                 (MX21_AITC_BASE + 0x24)

#define IS_BOOTING_FROM_NAND()          (readl(MXCBOOT_FLAG_REG) == NAND_FLASH_BOOT)
#define IS_BOOTING_FROM_NOR()           (readl(MXCBOOT_FLAG_REG) == NOR_FLASH_BOOT)
#define IS_BOOTING_FROM_SDRAM()         (readl(MXCBOOT_FLAG_REG) == SDRAM_NON_FLASH_BOOT)
#define IS_BOOTING_FROM_MMC()          (readl(MXCBOOT_FLAG_REG) == MMC_BOOT)

#ifndef MXCFLASH_SELECT_NAND
#define IS_FIS_FROM_NAND()              0
#else
#define IS_FIS_FROM_NAND()              (readl(MXCFIS_FLAG_REG) == MXCFIS_NAND)
#endif

#ifndef MXCFLASH_SELECT_NOR
#define IS_FIS_FROM_NOR()               0
#else
#define IS_FIS_FROM_NOR()                (readl(MXCFIS_FLAG_REG) == MXCFIS_NOR)
#endif

#ifndef MXCFLASH_SELECT_MMC
#define IS_FIS_FROM_MMC()               0
#else
#define IS_FIS_FROM_MMC()              (readl(MXCFIS_FLAG_REG) == MXCFIS_MMC)
#endif

#define MXC_ASSERT_NOR_BOOT()           writel(MXCFIS_NOR, MXCFIS_FLAG_REG)
#define MXC_ASSERT_NAND_BOOT()         writel(MXCFIS_NAND, MXCFIS_FLAG_REG)
#define MXC_ASSERT_MMC_BOOT()           writel(MXCFIS_MMC, MXCFIS_FLAG_REG)

#define SERIAL_DOWNLOAD_MAGIC           0x000000AA
#define SERIAL_DOWNLOAD_MAGIC_REG       MX21_AITC_NIPRIORITY3
#define SERIAL_DOWNLOAD_SRC_REG         MX21_AITC_NIPRIORITY2
#define SERIAL_DOWNLOAD_TGT_REG         MX21_AITC_NIPRIORITY1
#define SERIAL_DOWNLOAD_SZ_REG          MX21_AITC_NIPRIORITY0

#if !defined(__ASSEMBLER__)
void cyg_hal_plf_serial_init(void);
void cyg_hal_plf_serial_stop(void);
void hal_delay_us(unsigned int usecs);
#define HAL_DELAY_US(n)     hal_delay_us(n)

enum plls {
    MCU_PLL = MX21_CRM_BASE + MX21_CRM_MPCTL0,
    SER_PLL = MX21_CRM_BASE + MX21_CRM_SPCTL0,
};

enum main_clocks {
    CPU_CLK,
    AHB_CLK,
    IPG_CLK,
    NFC_CLK,
    USB_CLK,
};

enum peri_clocks {
    PER_CLK1,
    PER_CLK2,
    PER_CLK3,
    PER_CLK4,
    FIRI_BAUD,
    SSI1_BAUD,
    SSI2_BAUD,
};

typedef unsigned int nfc_setup_func_t(unsigned int, unsigned int, unsigned int, unsigned int);

#endif //#if !defined(__ASSEMBLER__)

#define HAL_MMU_OFF()                                               \
CYG_MACRO_START                                                     \
    asm volatile (                                                  \
        "1: "                                                       \
        "mrc p15, 0, r15, c7, c14, 3;"   /*test clean and inval*/   \
        "bne 1b;"                                                   \
        "mov r0, #0;"                                               \
        "mcr p15,0,r0,c7,c10,4;"   /*drain write buffer*/           \
        "mcr p15,0,r0,c7,c5,0;" /* invalidate I cache */            \
        "mrc p15,0,r0,c1,c0,0;" /* read c1 */                       \
        "bic r0,r0,#0x7;" /* disable DCache and MMU */              \
        "bic r0,r0,#0x1000;" /* disable ICache */                   \
        "mcr p15,0,r0,c1,c0,0;" /*  */                              \
        "nop;" /* flush i+d-TLBs */                                 \
        "nop;" /* flush i+d-TLBs */                                 \
        "nop;" /* flush i+d-TLBs */                                 \
        :                                                           \
        :                                                           \
        : "r0","memory" /* clobber list */);                        \
CYG_MACRO_END

#endif /* __HAL_SOC_H__ */
