//==========================================================================
//
//      cmds.c
//
//      Platform specific RedBoot commands
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
#include <redboot.h>
#include <cyg/hal/hal_intr.h>
#include <cyg/hal/plf_mmap.h>
#include <cyg/hal/hal_soc.h>         // Hardware definitions
#include <cyg/hal/hal_cache.h>

typedef unsigned long long  u64;
typedef unsigned int        u32;
typedef unsigned short      u16;
typedef unsigned char       u8;

#define SZ_DEC_1M       1000000
#define PLL_PD_MAX      16      //actual pd+1
#define PLL_MFI_MAX     15
#define PLL_MFI_MIN     5
#define PLL_MFD_MAX     1024    //actual mfd+1
#define PLL_MFN_MAX     1022
#define PRESC_MAX       8
#define IPG_DIV_MAX     2
#define AHB_DIV_MAX     16

//#define CPLM_SETUP      (1 << 31)
#define CPLM_SETUP      0

#if (PLL_REF_CLK == FREQ_32768HZ) || (PLL_REF_CLK == FREQ_32000HZ)
#define PLL_MFD_FIXED   1024
#endif
#if (PLL_REF_CLK == FREQ_26MHZ)
#define PLL_MFD_FIXED   (26 * 16)       // =416
#endif

#define PLL_FREQ_MAX    (2 * PLL_REF_CLK * PLL_MFI_MAX)
#define PLL_FREQ_MIN    ((2 * PLL_REF_CLK * PLL_MFI_MIN) / PLL_PD_MAX)
#define AHB_CLK_MAX     133333333
#define IPG_CLK_MAX     (AHB_CLK_MAX / 2)
#define NFC_CLK_MAX     33333333

#define ERR_WRONG_CLK   -1
#define ERR_NO_MFI      -2
#define ERR_NO_MFN      -3
#define ERR_NO_PD       -4
#define ERR_NO_PRESC    -5

u32 pll_clock(enum plls pll);
u32 get_main_clock(enum main_clocks clk);
u32 get_peri_clock(enum peri_clocks clk);

static void clock_setup(int argc, char *argv[]);
static void clko(int argc, char *argv[]);

RedBoot_cmd("clock",
            "Setup/Display clock (max AHB=133MHz, max IPG=66.5MHz)\nSyntax:",
            "[<core clock in MHz> [:<AHB-to-core divider>[:<IPG-to-AHB divider>]]] \n\n\
If a divider is zero or no divider is specified, the optimal divider values \n\
will be chosen. Examples:\n\
   [clock]         -> Show various clocks\n\
   [clock 266]     -> Core=266  AHB=133           IPG=66.5\n\
   [clock 350]     -> Core=350  AHB=117           IPG=58.5\n\
   [clock 266:4]   -> Core=266  AHB=66.5(Core/4)  IPG=66.5\n\
   [clock 266:4:2] -> Core=266  AHB=66.5(Core/4)  IPG=33.25(AHB/2)\n",
            clock_setup
           );

/*!
 * This is to calculate various parameters based on reference clock and 
 * targeted clock based on the equation:
 *      t_clk = 2*ref_freq*(mfi + mfn/(mfd+1))/(pd+1)
 * This calculation is based on a fixed MFD value for simplicity.
 *
 * @param ref       reference clock freq
 * @param target    targeted clock in HZ
 * @param p_pd      calculated pd value (pd value from register + 1) upon return
 * @param p_mfi     calculated actual mfi value upon return
 * @param p_mfn     calculated actual mfn value upon return
 * @param p_mfd     fixed mfd value (mfd value from register + 1) upon return
 *
 * @return          0 if successful; non-zero otherwise.
 */
int calc_pll_params(u32 ref, u32 target, u32 *p_pd, 
                    u32 *p_mfi, u32 *p_mfn, u32 *p_mfd)
{
    u64 pd, mfi, mfn, n_target = (u64)target, n_ref = (u64)ref;

    // Make sure targeted freq is in the valid range. Otherwise the 
    // following calculation might be wrong!!!
    if (target < PLL_FREQ_MIN || target > PLL_FREQ_MAX) {
        return ERR_WRONG_CLK;
    }
    // Use n_target and n_ref to avoid overflow
    for (pd = 1; pd <= PLL_PD_MAX; pd++) {
        mfi = (n_target * pd) / (2 * n_ref);
        if (mfi > PLL_MFI_MAX) {
            return ERR_NO_MFI;
        } else if (mfi < 5) {
            continue;
        }
        break;
    }
    // Now got pd and mfi already
    mfn = (((n_target * pd) / 2 - n_ref * mfi) * PLL_MFD_FIXED) / n_ref;
    // Check mfn within limit and mfn < denominator
    if (mfn > PLL_MFN_MAX || mfn >= PLL_MFD_FIXED) {
        return ERR_NO_MFN;
    }

    if (pd > PLL_PD_MAX) {
        return ERR_NO_PD;
    }
    *p_pd = (u32)pd;
    *p_mfi = (u32)mfi;
    *p_mfn = (u32)mfn;
    *p_mfd = PLL_MFD_FIXED;
    return 0;
}

/*!
 * This function assumes the expected core clock has to be changed by
 * modifying the PLL. This is NOT true always but for most of the times,
 * it is. So it assumes the PLL output freq is the same as the expected 
 * core clock (presc=1) unless the core clock is less than PLL_FREQ_MIN.
 * In the latter case, it will try to increase the presc value until 
 * (presc*core_clk) is greater than PLL_FREQ_MIN. It then makes call to
 * calc_pll_params() and obtains the values of PD, MFI,MFN, MFD based
 * on the targeted PLL and reference input clock to the PLL. Lastly, 
 * it sets the register based on these values along with the dividers.
 * Note 1) There is no value checking for the passed-in divider values
 *         so the caller has to make sure those values are sensible.
 *      2) Also adjust the NFC divider such that the NFC clock doesn't
 *         exceed NFC_CLK_MAX (which is 33MHz now).
 * 
 * @param ref       pll input reference clock (32KHz or 26MHz)
 * @param core_clk  core clock in Hz
 * @param ahb_div   ahb divider to divide the core clock to get ahb clock 
 *                  (ahb_div - 1) needs to be set in the register
 * @param ipg_div   ipg divider to divide the ahb clock to get ipg clock
 *                  (ipg_div - 1) needs to be set in the register
 # @return          0 if successful; non-zero otherwise
 */
int configure_clock(u32 ref, u32 core_clk, u32 ahb_div, u32 ipg_div)
{
    u32 pll, presc = 1, pd, mfi, mfn, mfd, brmo = 1, cscr, mpctl0;
    u32 pcdr0, nfc_div;
    int ret, i;

    // assume pll default to core clock first
    pll = core_clk;
    // when core_clk >= PLL_FREQ_MIN, the presc can be 1.
    // Otherwise, need to calculate presc value below and adjust the targeted pll
    if (core_clk < PLL_FREQ_MIN) {
        for (presc = 1; presc <= PRESC_MAX; presc++) {
            if ((core_clk * presc) > PLL_FREQ_MIN) {
                break;
            }
        }
        if (presc == (PRESC_MAX + 1)) {
            diag_printf("can't make presc=%d\n", presc);
            return ERR_NO_PRESC;
        }
        pll = core_clk * presc;
    }
    // pll is now the targeted pll output. Use it along with ref input clock
    // to get pd, mfi, mfn, mfd
    if ((ret = calc_pll_params(ref, pll, &pd, &mfi, &mfn, &mfd)) != 0) {
        diag_printf("can't find pll parameters: %d\n", ret);
        return ret;
    }

    // blindly increase divider first to avoid too fast ahbclk and ipgclk
    // in case the core clock increases too much
    cscr = readl(MX21_CRM_BASE + MX21_CRM_CSCR);
    cscr &= ~0xE0003E00;
    // increase the dividers. should work even when core clock is 780MHz
    // which is unlikely true.
    cscr |= (3 << 29) | (5 << 10) | (1 << 9);
    writel(cscr, MX21_CRM_BASE + MX21_CRM_CSCR);

    // update PLL register
    if ((mfd < (10 * mfn)) && ((10 * mfn) < (9 * mfd)))
        brmo = 0;
    if (brmo != 0)
        writel(1 << 6, MX21_CRM_BASE + MX21_CRM_MPCTL1);

    mpctl0 = readl(MX21_CRM_BASE + MX21_CRM_MPCTL0);
    mpctl0 = (mpctl0 & 0xC000C000)  |
             CPLM_SETUP             |
             ((pd - 1) << 26)       | 
             ((mfd - 1) << 16)      |
             (mfi << 10)            | 
             mfn;
    writel(mpctl0, MX21_CRM_BASE + MX21_CRM_MPCTL0);
    // restart mpll
    writel((cscr | (1 << 21)), MX21_CRM_BASE + MX21_CRM_CSCR);
    // check the LF bit to insure lock
    while ((readl(MX21_CRM_BASE + MX21_CRM_MPCTL1) & (1 << 15)) == 0);
    // have to add some delay for new values to take effect
    for (i = 0; i < 10000; i++);

    // PLL locked already so use the new divider values
    cscr = readl(MX21_CRM_BASE + MX21_CRM_CSCR);
    cscr &= ~0xE0003E00;
    cscr |= ((presc - 1) << 29) | ((ahb_div - 1) << 10) | ((ipg_div - 1) << 9);
    writel(cscr, MX21_CRM_BASE + MX21_CRM_CSCR);

    // Make sure optimal NFC clock but less than NFC_CLK_MAX
    for (nfc_div = 1; nfc_div <= 16; nfc_div++) {
        if ((core_clk / nfc_div) <= NFC_CLK_MAX) {
            break;
        }
    }
    pcdr0 = readl(MX21_CRM_BASE + MX21_CRM_PCDR0);
    writel(((pcdr0 & 0xFFFF0FFF) | ((nfc_div - 1) << 12)), 
           MX21_CRM_BASE + MX21_CRM_PCDR0);

    return 0;
}

static void clock_setup(int argc,char *argv[])
{
    u32 i, core_clk, ipg_div, data[3],
    ahb_div, ahb_clk, ipg_clk;
    int ret;

    if (argc == 1)
        goto print_clock;
    for (i = 0;  i < 3;  i++) {
        unsigned long temp;
        if (!parse_num(*(&argv[1]), &temp, &argv[1], ":")) {
            diag_printf("Error: Invalid parameter\n");
            return;
        }
        data[i] = temp;
    }

    core_clk = data[0] * SZ_DEC_1M;
    ahb_div = data[1];  // actual register field + 1
    ipg_div = data[2];  // actual register field + 1

    if (core_clk < (PLL_FREQ_MIN / PRESC_MAX) || core_clk > PLL_FREQ_MAX) {
        diag_printf("Targeted core clock should be within [%d - %d]\n", 
                    PLL_FREQ_MIN / PRESC_MAX, PLL_FREQ_MAX);
        return;
    }

    // find the ahb divider  
    if (ahb_div > AHB_DIV_MAX) {
        diag_printf("Invalid AHB divider: %d. Maximum value is %d\n",
                    ahb_div, AHB_DIV_MAX);
        return;
    }
    if (ahb_div == 0) {
        // no AHBCLK divider specified
        for (ahb_div = 1; ; ahb_div++) {
            if ((core_clk / ahb_div) <= AHB_CLK_MAX) {
                break;
            }
        }
    }
    if (ahb_div > AHB_DIV_MAX || (core_clk / ahb_div) > AHB_CLK_MAX) {
        diag_printf("Can't make AHB=%d since max=%d\n", 
                    core_clk / ahb_div, AHB_CLK_MAX);
        return;
    }

    // find the ipg divider
    ahb_clk = core_clk / ahb_div;
    if (ipg_div > IPG_DIV_MAX) {
        diag_printf("Invalid IPG divider: %d. Maximum value is %d\n", 
                    ipg_div, IPG_DIV_MAX);
        return;
    }
    if (ipg_div == 0) {
        ipg_div++;          // At least =1
        if (ahb_clk > IPG_CLK_MAX)
            ipg_div++;      // Make it =2
    }
    if (ipg_div > IPG_DIV_MAX || (ahb_clk / ipg_div) > IPG_CLK_MAX) {
        diag_printf("Can't make IPG=%d since max=%d\n", 
                    (ahb_clk / ipg_div), IPG_CLK_MAX);
        return;
    }
    ipg_clk = ahb_clk / ipg_div;

    diag_printf("Trying to set core=%d ahb=%d ipg=%d...\n", 
                core_clk, ahb_clk, ipg_clk);
    
    // stop the serial to be ready to adjust the clock
    hal_delay_us(100000);
    cyg_hal_plf_serial_stop();
    // adjust the clock
    ret = configure_clock(PLL_REF_CLK, core_clk, ahb_div, ipg_div);
    // restart the serial driver
    cyg_hal_plf_serial_init();
    hal_delay_us(100000);

    if (ret != 0) {
        diag_printf("Failed to setup clock: %d\n", ret);
        return;
    }
    diag_printf("\n<<<New clock setting>>>\n");

    // Now printing clocks
print_clock:
    diag_printf("\nMPLL\t\tSPLL\n");
    diag_printf("=========================\n");
    diag_printf("%-16d%-16d\n\n", pll_clock(MCU_PLL), pll_clock(SER_PLL));
    diag_printf("CPU\t\tAHB\t\tIPG\t\tNFC\t\tUSB\n");
    diag_printf("========================================================================\n");
    diag_printf("%-16d%-16d%-16d%-16d%-16d\n\n",
                get_main_clock(CPU_CLK),
                get_main_clock(AHB_CLK),
                get_main_clock(IPG_CLK),
                get_main_clock(NFC_CLK),
                get_main_clock(USB_CLK));

    diag_printf("PER1\t\tPER2\t\tPER3\t\tPER4\n");
    diag_printf("===========================================");
    diag_printf("=============\n");

    diag_printf("%-16d%-16d%-16d%-16d\n\n", 
                get_peri_clock(PER_CLK1),
                get_peri_clock(PER_CLK2),
                get_peri_clock(PER_CLK3),
                get_peri_clock(PER_CLK4));

    diag_printf("FIRI\t\tSSI1\t\tSSI2\n");
    diag_printf("========================================\n");
    diag_printf("%-16d%-16d%-16d\n\n", 
                get_peri_clock(FIRI_BAUD),
                get_peri_clock(SSI1_BAUD),
                get_peri_clock(SSI2_BAUD));
    diag_printf("PERCLK: 1-<UART|GPT|PWM> 2-<SDHC|CSPI> 3-<LCDC> 4-<CSI>\n");
}

/*!
 * This function returns the PLL output value in Hz based on pll.
 */
u32 pll_clock(enum plls pll)
{
    u64 mfi, mfn, mfd, pdf, ref_clk, pll_out;
    u64 reg = readl(pll);

    pdf = (reg >> 26) & 0xF;
    mfd = (reg >> 16) & 0x3FF;
    mfi = (reg >> 10) & 0xF;
    mfi = (mfi <= 5) ? 5: mfi;
    mfn = reg & 0x3FF;

    ref_clk = PLL_REF_CLK;

    pll_out = (2 * ref_clk * mfi + ((2 * ref_clk * mfn) / (mfd + 1))) /
              (pdf + 1);

    return (u32)pll_out;
}

/*!
 * This function returns the main clock value in Hz.
 */
u32 get_main_clock(enum main_clocks clk)
{
    u32 presc, ahb_div, ipg_pdf, nfc_div;
    u32 ret_val = 0, usb_div;
    u32 cscr = readl(MX21_CRM_BASE + MX21_CRM_CSCR);
    u32 pcdr0 = readl(MX21_CRM_BASE + MX21_CRM_PCDR0);

    presc = (cscr >> 29) + 1;

    switch (clk) {
    case CPU_CLK:
        ret_val = pll_clock(MCU_PLL) / presc;
        break;
    case AHB_CLK:
        ahb_div = ((cscr >> 10) & 0xF) + 1;
        ret_val = pll_clock(MCU_PLL) / (presc * ahb_div);
        break;
    case IPG_CLK:
        ahb_div = ((cscr >> 10) & 0xF) + 1;
        ipg_pdf = ((cscr >> 9) & 0x1) + 1;
        ret_val = pll_clock(MCU_PLL) / (presc * ahb_div * ipg_pdf);
        break;
    case NFC_CLK:
        nfc_div = ((pcdr0 >> 12) & 0xF) + 1;
        /* AHB/nfc_div */
        ret_val = pll_clock(MCU_PLL) / (presc * nfc_div);
        break;
    case USB_CLK:
        usb_div = ((cscr >> 26) & 0x7) + 1;
        ret_val = pll_clock(SER_PLL) / usb_div;
        break;
    default:
        diag_printf("Unknown clock: %d\n", clk);
        break;
    }

    return ret_val;
}

/*!
 * This function returns the peripheral clock value in Hz.
 */
u32 get_peri_clock(enum peri_clocks clk)
{
    u32 ret_val = 0, div;
    u32 pcdr0 = readl(MX21_CRM_BASE + MX21_CRM_PCDR0);
    u32 pcdr1 = readl(MX21_CRM_BASE + MX21_CRM_PCDR1);
    u32 cscr = readl(MX21_CRM_BASE + MX21_CRM_CSCR);

    switch (clk) {
    case PER_CLK1:
        div = (pcdr1 & 0x3F) + 1;
        ret_val = pll_clock(MCU_PLL) / div;
        break;
    case PER_CLK2:
        div = ((pcdr1 >> 8) & 0x3F) + 1;
        ret_val = pll_clock(MCU_PLL) / div;
        break;
    case PER_CLK3:
        div = ((pcdr1 >> 16) & 0x3F) + 1;
        ret_val = pll_clock(MCU_PLL) / div;
        break;
    case PER_CLK4:
        div = ((pcdr1 >> 24) & 0x3F) + 1;
        ret_val = pll_clock(MCU_PLL) / div;
        break;
    case SSI1_BAUD:
        div = (pcdr0 >> 16) & 0x3F;
        if (div < 2)
            div = 62 * 2;
        if ((cscr & (1 << 19)) != 0) {
            // This takes care of 0.5*SSIDIV[0] by x2
            ret_val = (2 * pll_clock(MCU_PLL)) / div;
        } else {
            ret_val = (2 * pll_clock(SER_PLL)) / div;
        }
        break;
    case SSI2_BAUD:
        div = (pcdr0 >> 26) & 0x3F;
        if (div < 2)
            div = 62 * 2;
        if ((cscr & (1 << 20)) != 0) {
            ret_val = (2 * pll_clock(MCU_PLL)) / div;
        } else {
            ret_val = (2 * pll_clock(SER_PLL)) / div;
        }
        break;
    case FIRI_BAUD:
        div = (pcdr0 & 0x1F) + 1;
        if ((cscr & (1 << 18)) == 0) {
            ret_val = pll_clock(MCU_PLL) / div;
        } else {
            ret_val = (2 * pll_clock(SER_PLL)) / div;
        }
        break;
    default:
        diag_printf("%s(): This clock: %d not supported yet \n",
                    __FUNCTION__, clk);
        break;
    }

    return ret_val;
}

RedBoot_cmd("clko",
            "Select clock source for CLKO (TP1 on the CPU daughter card)",
            " The output clock is the actual clock source freq. Default is FCLK\n\
         Note that the module clock will be turned on for reading!\n\
          <0> - display current clko selection \n\
          <1> - CLK32 \n\
          <2> - PREMCLK \n\
          <3> - CLK26M (may see nothing if 26MHz Crystal is not connected) \n\
          <4> - MPLL Reference CLK \n\
          <5> - SPLL Reference CLK \n\
          <6> - MPLL CLK \n\
          <7> - SPLL CLK \n\
          <8> - FCLK \n\
          <9> - AHBCLK \n\
          <10> - IPG_CLK (PERCLK) \n\
          <11> - PERCLK1 \n\
          <12> - PERCLK2 \n\
          <13> - PERCLK3 \n\
          <14> - PERCLK4 \n\
          <15> - SSI 1 Baud \n\
          <16> - SSI 2 Baud \n\
          <17> - NFC \n\
          <18> - FIRI Baud \n\
          <19> - CLK48M Always \n\
          <20> - CLK32K Always \n\
          <21> - CLK48M \n\
          <22> - CLK48DIV_CLKO",
            clko
           );

static u8* clko_name[] ={
    "NULL",
    "CLK32",
    "PREMCLK",
    "CLK26M (may see nothing if 26MHz Crystal is not connected)",
    "MPLL Reference CLK",
    "SPLL Reference CLK",
    "MPLL CLK",
    "SPLL CLK",
    "FCLK",
    "AHBCLK",
    "IPG_CLK (PERCLK)",
    "PERCLK1",
    "PERCLK2",
    "PERCLK3",
    "PERCLK4",
    "SSI 1 Baud",
    "SSI 2 Baud",
    "NFC",
    "FIRI Baud",
    "CLK48M Always",
    "CLK32K Always",
    "CLK48M",
    "CLK48DIV_CLKO",
};

// This has to agree with the above table
enum clko_src {
    PERCLK3     = 13,
    PERCLK4     = 14,
    SSI_1_Baud  = 15,
    SSI_2_Baud  = 16,
    NFC_Baud    = 17,
    FIRI_Baud   = 18,
};

#define CLKO_MAX_INDEX          (sizeof(clko_name) / sizeof(u8*))

static void clko(int argc,char *argv[])
{
    u32 action = 0, ccsr;

    if (!scan_opts(argc, argv, 1, 0, 0, (void*) &action,
                   OPTION_ARG_TYPE_NUM, "action"))
        return;

    if (action >= CLKO_MAX_INDEX) {
        diag_printf("%d is not supported\n\n", action);
        return;
    }

    ccsr = readl(MX21_CRM_BASE + MX21_CRM_CCSR);

    if (action != 0) {
        // turn on these clocks
        switch (action) {
        case PERCLK3:
            writel(readl(MX21_CRM_BASE + MX21_CRM_PCCR0) | (1 << 18),
                   MX21_CRM_BASE + MX21_CRM_PCCR0);
            break;
        case PERCLK4:
            writel(readl(MX21_CRM_BASE + MX21_CRM_PCCR0) | (1 << 22),
                   MX21_CRM_BASE + MX21_CRM_PCCR0);
            break;
        case SSI_1_Baud:
            writel(readl(MX21_CRM_BASE + MX21_CRM_PCCR0) | (1 << 17),
                   MX21_CRM_BASE + MX21_CRM_PCCR0);
            break;
        case SSI_2_Baud:
            writel(readl(MX21_CRM_BASE + MX21_CRM_PCCR0) | (1 << 16),
                   MX21_CRM_BASE + MX21_CRM_PCCR0);
            break;
        case NFC_Baud:
            writel(readl(MX21_CRM_BASE + MX21_CRM_PCCR0) | (1 << 19),
                   MX21_CRM_BASE + MX21_CRM_PCCR0);
            break;
        case FIRI_Baud:
            writel(readl(MX21_CRM_BASE + MX21_CRM_PCCR0) | (1 << 20),
                   MX21_CRM_BASE + MX21_CRM_PCCR0);
            break;
        }
        ccsr = (ccsr & (~0x1F)) + action - 1;
        writel(ccsr, MX21_CRM_BASE + MX21_CRM_CCSR);
        diag_printf("Set clko to ");
    }

    ccsr = readl(MX21_CRM_BASE + MX21_CRM_CCSR);
    diag_printf("%s\n", clko_name[(ccsr & 0x1F) + 1]);
    diag_printf("CCSR register[0x%x] = 0x%x\n", 
                (MX21_CRM_BASE + MX21_CRM_CCSR), ccsr);
}

extern int flash_program(void *_addr, void *_data, int len, void **err_addr);
extern int flash_erase(void *addr, int len, void **err_addr);

void auto_flash_start(void)
{
    void *err_addr;
	int stat;
    int nor_update = 1; //todo: need to support NAND
    u32 src = readl(SERIAL_DOWNLOAD_SRC_REG);
    u32 dst = readl(SERIAL_DOWNLOAD_TGT_REG);
    u32 sz = readl(SERIAL_DOWNLOAD_SZ_REG);

    if (readl(SERIAL_DOWNLOAD_MAGIC_REG) != SERIAL_DOWNLOAD_MAGIC) {
        return;
    }

    if (nor_update) {
        // Erase area to be programmed
        if ((stat = flash_erase((void *)dst, sz, (void **)&err_addr)) != 0) {
            diag_printf("BEADDEAD\n");
    	return;
        }
        diag_printf("BEADBEEF\n");
        // Now program it
        if ((stat = flash_program((void *)dst, (void *)src, sz, 
                                  (void **)&err_addr)) != 0) {
            diag_printf("BEADFEEF\n");
        }
    }
    diag_printf("BEADCEEF\n");
}

RedBoot_init(auto_flash_start, RedBoot_INIT_LAST);
