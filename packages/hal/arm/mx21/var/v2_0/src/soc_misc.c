//==========================================================================
//
//      soc_misc.c
//
//      HAL misc board support code
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <redboot.h>
#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>         // base types
#include <cyg/infra/cyg_trac.h>         // tracing macros
#include <cyg/infra/cyg_ass.h>          // assertion macros

#include <cyg/hal/hal_misc.h>           // Size constants
#include <cyg/hal/hal_io.h>             // IO macros
#include <cyg/hal/hal_arch.h>           // Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>           // Interrupt names
#include <cyg/hal/hal_cache.h>          // Cache control
#include <cyg/hal/hal_soc.h>         // Hardware definitions
#include <cyg/hal/hal_mm.h>             // MMap table definitions

#include <cyg/infra/diag.h>             // diag_printf

// Most initialization has already been done before we get here.
// All we do here is set up the interrupt environment.

unsigned int system_rev;
externC void plf_hardware_init(void);

char HAL_PLATFORM_EXTRA[20] = "PASS x.x [x32 SDR]";

void hal_hardware_init(void)
{
    volatile unsigned int temp;
    volatile unsigned int esdctl0 = readl(ESDCTL_BASE);
    
    system_rev = readl(MX21_SI_ID_REG);

    if (system_rev == MX21_SILICONID_Rev2_x) {
        HAL_PLATFORM_EXTRA[5] = '2';
        HAL_PLATFORM_EXTRA[7] = 'x';
    } else if (system_rev == MX21_SILICONID_Rev3_0) {
        HAL_PLATFORM_EXTRA[5] = '3';
        HAL_PLATFORM_EXTRA[7] = '0';
    } else if (system_rev == MX21_SILICONID_Rev3_1) {
        HAL_PLATFORM_EXTRA[5] = '3';
        HAL_PLATFORM_EXTRA[7] = '1';
    }

    if ((esdctl0 & 0x20000) == 0x0) {
            HAL_PLATFORM_EXTRA[11] = '1';
            HAL_PLATFORM_EXTRA[12] = '6';
    }

    // Mask all interrupts
    writel(0xFFFFFFFF, MX21_AITC_NIMASK);

    // Make all interrupts do IRQ and not FIQ
    // FIXME: Change this if you use FIQs.
    writel(0, MX21_AITC_INTTYPEH);
    writel(0, MX21_AITC_INTTYPEL);

    // Disable all GPIO interrupt sources
    
    // Enable caches
    HAL_ICACHE_ENABLE();
    HAL_DCACHE_ENABLE();

    if ((readw(WDOG_BASE_ADDR) & 4) != 0) {
        // increase the WDOG timeout value to the max
        writew(readw(WDOG_BASE_ADDR) | 0xFF00, WDOG_BASE_ADDR);
    }

    // Perform any platform specific initializations
    plf_hardware_init();

    // Set up eCos/ROM interfaces
    hal_if_init();

    //init timer2 and start it -- assuming that per clock 1 runs at 44MHz)


    writel(0x4, MX21_Timer2_BASE + KHwTimerTCL); // counter reset when timer is disabled
    writel(0x0, MX21_Timer2_BASE + KHwTimerTCL); // disable timer
	
    writel(0x00008000, MX21_Timer2_BASE + KHwTimerTCL); // reset timer
    while((readl(MX21_Timer2_BASE + KHwTimerTCL) & 0x8000) != 0); // make sure reset complete
    
    writel(0x0, MX21_Timer2_BASE + KHwTimerTCL); // disable timer
    writel(DelayTimerPresVal, MX21_Timer2_BASE + KHwTimerTPRER);
    temp = readl(MX21_Timer2_BASE + KHwTimerTCL);
    writel(temp & (~0x00000100), MX21_Timer2_BASE + KHwTimerTCL); // restart mode
    temp = readl(MX21_Timer2_BASE + KHwTimerTCL);
    writel(temp | 0x00000002, MX21_Timer2_BASE + KHwTimerTCL); // PERCLK1 to prescaler
    temp = readl(MX21_Timer2_BASE + KHwTimerTCL);
    writel(temp | 0x00000001, MX21_Timer2_BASE + KHwTimerTCL); //enable timer

    hal_delay_us(MX_STARTUP_DELAY);

    //Reset ethernet here
    BitSetEIO(EIO_RESET_BASE);
    hal_delay_us(10000);
    BitClearEIO(EIO_RESET_BASE);
    hal_delay_us(100000);
    readb(BOARD_CS_UART_BASE + 1);
    hal_delay_us(100000);

    if (system_rev == MX21_SILICONID_Rev2_x) {
   	    writel(0x7FFF7FFF, MX21_DSCR_BASE + DSCR2);
   	    writel(0x7FFF7FFF, MX21_DSCR_BASE + DSCR3);
   	    writel(0x7FFF7FFF, MX21_DSCR_BASE + DSCR4);
   	    writel(0x7FFF7FFF, MX21_DSCR_BASE + DSCR5);
   	    writel(0x7FFF7FFF, MX21_DSCR_BASE + DSCR6);
   	    writel(0x7FFF7FFF, MX21_DSCR_BASE + DSCR7);
   	    writel(0x7FFF7FFF, MX21_DSCR_BASE + DSCR8);
   	    writel(0x7FFF7FFF, MX21_DSCR_BASE + DSCR10);
   	    writel(0x7FFF7FFF, MX21_DSCR_BASE + DSCR11);
   	    writel(0x7FC07FF8, MX21_DSCR_BASE + DSCR9);
   	}
}

// -------------------------------------------------------------------------
void hal_clock_initialize(cyg_uint32 period)
{
}

// This routine is called during a clock interrupt.

// Define this if you want to ensure that the clock is perfect (i.e. does
// not drift).  One reason to leave it turned off is that it costs some
// us per system clock interrupt for this maintenance.
#undef COMPENSATE_FOR_CLOCK_DRIFT

void hal_clock_reset(cyg_uint32 vector, cyg_uint32 period)
{
}

// Read the current value of the clock, returning the number of hardware
// "ticks" that have occurred (i.e. how far away the current value is from
// the start)

// Note: The "contract" for this function is that the value is the number
// of hardware clocks that have happened since the last interrupt (i.e.
// when it was reset).  This value is used to measure interrupt latencies.
// However, since the hardware counter runs freely, this routine computes
// the difference between the current clock period and the number of hardware
// ticks left before the next timer interrupt.
void hal_clock_read(cyg_uint32 *pvalue)
{
}

// This is to cope with the test read used by tm_basic with
// CYGVAR_KERNEL_COUNTERS_CLOCK_LATENCY defined; we read the count ASAP
// in the ISR, *before* resetting the clock.  Which returns 1tick +
// latency if we just use plain hal_clock_read().
void hal_clock_latency(cyg_uint32 *pvalue)
{
}

unsigned int hal_timer_count(void)
{
    return readl(HAL_DELAY_TIMER_MX2 + KHwTimerTCN);
}

#define WDT_MAGIC_1             0x5555
#define WDT_MAGIC_2             0xAAAA
#define MXC_WDT_WSR             0x2

static unsigned int led_on = 0;
//
// Delay for some number of micro-seconds
//
void hal_delay_us(unsigned int usecs)
{
    volatile unsigned long timerCount, timerCompare, tmp;
    const unsigned long TIMERMAXCOUNT = 0xFFFFFFFF;
    unsigned long delayCount = usecs * ((MX21_PERCLK1/(DelayTimerPresVal+1))/1000000);
 //diag_printf("entering mx2 hal_delay_us: %d, delaycount = %d, system_rev = %d\n\n", usecs, delayCount, system_rev);

    if (delayCount == 0) {
        return;
    }

    // issue the service sequence instructions
    if ((readw(WDOG_BASE_ADDR) & 4) != 0) {
        writew(WDT_MAGIC_1, WDOG_BASE_ADDR + MXC_WDT_WSR);
        writew(WDT_MAGIC_2, WDOG_BASE_ADDR + MXC_WDT_WSR);
    }

    writel(0x03, HAL_DELAY_TIMER_MX2 + KHwTimerTSTAT); // clear the compare status bit
    timerCount = readl(HAL_DELAY_TIMER_MX2 + KHwTimerTCN);
   
    
    tmp = TIMERMAXCOUNT - timerCount;

    if (tmp < delayCount) {
        timerCompare = delayCount - tmp;
    } else {
        timerCompare = delayCount + timerCount;
    }
    writel(timerCompare, HAL_DELAY_TIMER_MX2 + KHwTimerTCMP);  // setup compare reg

    while ( (0x1 & readl(HAL_DELAY_TIMER_MX2 + KHwTimerTSTAT)) == 0 ); // return until compare bit is set
    writel(0x03, HAL_DELAY_TIMER_MX2 + KHwTimerTSTAT); // clear the compare status bit

    if ((++led_on % 2000) == 0)
        BOARD_DEBUG_LED(0);
}

// -------------------------------------------------------------------------

// This routine is called to respond to a hardware interrupt (IRQ).  It
// should interrogate the hardware and return the IRQ vector number.
int hal_IRQ_handler(void)
{
#ifdef HAL_EXTENDED_IRQ_HANDLER
    cyg_uint32 index;

    // Use platform specific IRQ handler, if defined
    // Note: this macro should do a 'return' with the appropriate
    // interrupt number if such an extended interrupt exists.  The
    // assumption is that the line after the macro starts 'normal' processing.
    HAL_EXTENDED_IRQ_HANDLER(index);
#endif

    return CYGNUM_HAL_INTERRUPT_NONE; // This shouldn't happen!
}

//
// Interrupt control

void hal_interrupt_mask(int vector)
{
//    diag_printf("6hal_interrupt_mask(vector=%d) \n", vector);
#ifdef HAL_EXTENDED_INTERRUPT_MASK
    // Use platform specific handling, if defined
    // Note: this macro should do a 'return' for "extended" values of 'vector'
    // Normal vectors are handled by code subsequent to the macro call.
    HAL_EXTENDED_INTERRUPT_MASK(vector);
#endif
}

void hal_interrupt_unmask(int vector)
{
//    diag_printf("7hal_interrupt_unmask(vector=%d) \n", vector);

#ifdef HAL_EXTENDED_INTERRUPT_UNMASK
    // Use platform specific handling, if defined
    // Note: this macro should do a 'return' for "extended" values of 'vector'
    // Normal vectors are handled by code subsequent to the macro call.
    HAL_EXTENDED_INTERRUPT_UNMASK(vector);
#endif
}

void hal_interrupt_acknowledge(int vector)
{

//    diag_printf("8hal_interrupt_acknowledge(vector=%d) \n", vector);
#ifdef HAL_EXTENDED_INTERRUPT_UNMASK
    // Use platform specific handling, if defined
    // Note: this macro should do a 'return' for "extended" values of 'vector'
    // Normal vectors are handled by code subsequent to the macro call.
    HAL_EXTENDED_INTERRUPT_ACKNOWLEDGE(vector);
#endif
}

void hal_interrupt_configure(int vector, int level, int up)
{

#ifdef HAL_EXTENDED_INTERRUPT_CONFIGURE
    // Use platform specific handling, if defined
    // Note: this macro should do a 'return' for "extended" values of 'vector'
    // Normal vectors are handled by code subsequent to the macro call.
    HAL_EXTENDED_INTERRUPT_CONFIGURE(vector, level, up);
#endif
}

void hal_interrupt_set_level(int vector, int level)
{

#ifdef HAL_EXTENDED_INTERRUPT_SET_LEVEL
    // Use platform specific handling, if defined
    // Note: this macro should do a 'return' for "extended" values of 'vector'
    // Normal vectors are handled by code subsequent to the macro call.
    HAL_EXTENDED_INTERRUPT_SET_LEVEL(vector, level);
#endif

    // Interrupt priorities are not configurable.
}

/*------------------------------------------------------------------------*/

