//==========================================================================
//
//      board_misc.c
//
//      HAL misc board support code for the board
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>         // base types
#include <cyg/infra/cyg_trac.h>         // tracing macros
#include <cyg/infra/cyg_ass.h>          // assertion macros

#include <cyg/hal/hal_io.h>             // IO macros
#include <cyg/hal/hal_arch.h>           // Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>           // Interrupt names
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_soc.h>            // Hardware definitions
#include <cyg/hal/fsl_board.h>          // Platform specifics

#include <cyg/infra/diag.h>             // diag_printf

// All the MM table layout is here:
#include <cyg/hal/hal_mm.h>

externC void* memset(void *, int, size_t);

void hal_mmu_init(void)
{
    unsigned long ttb_base = RAM_BANK0_BASE + 0x4000;
    unsigned long i;

    /*
     * Set the TTB register
     */
    asm volatile ("mcr  p15,0,%0,c2,c0,0" : : "r"(ttb_base) /*:*/);

    /*
     * Set the Domain Access Control Register
     */
    i = ARM_ACCESS_DACR_DEFAULT;
    asm volatile ("mcr  p15,0,%0,c3,c0,0" : : "r"(i) /*:*/);

    /*
     * First clear all TT entries - ie Set them to Faulting
     */
    memset((void *)ttb_base, 0, ARM_FIRST_LEVEL_PAGE_TABLE_SIZE);

    /*               Actual  Virtual  Size   Attributes                                                    Function  */
    /*		     	  Base     Base     MB      cached?           buffered?        access permissions                 */
    /*             xxx00000  xxx00000                                                                                */
    X_ARM_MMU_SECTION(0x000,  0x500,     1,  ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* Boot Rom */
    X_ARM_MMU_SECTION(0x100,  0x100,     1,  ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* Internal Regsisters */
    X_ARM_MMU_SECTION(0xC00,  0x000,    64,  ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
    X_ARM_MMU_SECTION(0xC00,  0xC00,    64,  ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
    X_ARM_MMU_SECTION(0xC80,  0xC80,    32,  ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* Flash */
    X_ARM_MMU_SECTION(0xCC0,  0xCC0,    832,  ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* External I/O */
}

//
// Platform specific initialization
//

void plf_hardware_init(void)
{
    /* Init port mask */
    *REG32_PTR(MX21_GPIO_PMASK) = 0x0000003F;

    /* Init pull-up enable */
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioPUEN) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioPUEN) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioPUEN) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioPUEN) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioPUEN) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioPUEN) = 0xFFFFFFFF;

    /* Init data direction */
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioDDIR) = 0x00000000;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioDDIR) = 0x40000000;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioDDIR) = 0x00000000;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioDDIR) = 0x00000000;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioDDIR) = 0x00000000;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioDDIR) = 0x00000000;

    /* Init output configuration */
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioOCR1) = 0x00000000;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioOCR1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioOCR1) = 0x00000000;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioOCR1) = 0x00000000;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioOCR1) = 0x00000000;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioOCR1) = 0x00000000;
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioOCR2) = 0x00000000;
    /* Set PortB pin30 to C_IN to get UART4 to work. */
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioOCR2) = 0xEFFFFFFF;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioOCR2) = 0x00000000;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioOCR2) = 0x00000000;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioOCR2) = 0x00000000;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioOCR2) = 0x00000000;

    /* Init input configuration */
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioICONFA1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioICONFA1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioICONFA1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioICONFA1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioICONFA1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioICONFA1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioICONFA2) = 0xFFFFFFFF;
    /* Set PortB pin 29, pin 31 to A_OUT to get UART4 to work. */
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioICONFA2) = 0x33FFFFFF;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioICONFA2) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioICONFA2) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioICONFA2) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioICONFA2) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioICONFB1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioICONFB1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioICONFB1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioICONFB1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioICONFB1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioICONFB1) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioICONFB2) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioICONFB2) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioICONFB2) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioICONFB2) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioICONFB2) = 0xFFFFFFFF;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioICONFB2) = 0xFFFFFFFF;

    /* Init data */
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioDR) = 0x00000000;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioDR) = 0x00000000;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioDR) = 0x00000000;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioDR) = 0x00000000;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioDR) = 0x00000000;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioDR) = 0x00000000;

    /* Init interrupt configuration */
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioICR1) = 0x00000000;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioICR1) = 0x00000000;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioICR1) = 0x00000000;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioICR1) = 0x00000000;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioICR1) = 0x00000000;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioICR1) = 0x00000000;
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioICR2) = 0x00000000;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioICR2) = 0x00000000;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioICR2) = 0x00000000;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioICR2) = 0x00000000;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioICR2) = 0x00000000;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioICR2) = 0x00000000;

    /* Init interrupt mask */
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioIMR) = 0x00000000;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioIMR) = 0x00000000;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioIMR) = 0x00000000;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioIMR) = 0x00000000;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioIMR) = 0x00000000;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioIMR) = 0x00000000;

    /* Init general purpose. */
    /* For using UART4: Set PortB bit 28 for selecting alternate function*/
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioGPR) = 0x00000000;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioGPR) = 0x10000000;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioGPR) = 0x00000000;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioGPR) = 0x00000000;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioGPR) = 0x00000000;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioGPR) = 0x00000000;

    /* Init GPIO in use. */
    /* For using UART4:  */
    /* Set PortB pin 28 as alternate, pin 29, 30 and 31 as GPIO */
    *REG32_PTR(MX21_GPIOA_BASE+KHwGpioGIUS) = 0xFFFFFFE0;
    *REG32_PTR(MX21_GPIOB_BASE+KHwGpioGIUS) = 0xEF3FFFF0;
    *REG32_PTR(MX21_GPIOC_BASE+KHwGpioGIUS) = 0xFFFFFFE0;
    *REG32_PTR(MX21_GPIOD_BASE+KHwGpioGIUS) = 0xFFFE0000;
    *REG32_PTR(MX21_GPIOE_BASE+KHwGpioGIUS) = 0x00FC0F20;
    *REG32_PTR(MX21_GPIOF_BASE+KHwGpioGIUS) = 0x00000000;

    /* Set the FMCR register, to enable UART4 RXD functionality. */
    *REG32_PTR(MX21_FMCR) = 0xFCFFFFCB;

}

#include CYGHWR_MEMORY_LAYOUT_H

typedef void code_fun(void);

void board_program_new_stack(void *func)
{
    register CYG_ADDRESS stack_ptr asm("sp");
    register CYG_ADDRESS old_stack asm("r4");
    register code_fun *new_func asm("r0");
    old_stack = stack_ptr;
    stack_ptr = CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE - sizeof(CYG_ADDRESS);
    new_func = (code_fun*)func;
    new_func();
    stack_ptr = old_stack;
}

// ------------------------------------------------------------------------
