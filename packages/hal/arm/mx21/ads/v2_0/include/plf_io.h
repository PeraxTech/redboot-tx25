#ifndef CYGONCE_HAL_ARM_BOARD_PLF_IO_H
#define CYGONCE_HAL_ARM_BOARD_PLF_IO_H

//=============================================================================
//
//      plf_io.h
//
//      Platform specific support (register layout, etc)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//=============================================================================

#include <cyg/hal/fsl_board.h>
#include <cyg/hal/hal_soc.h>

/* Board Specific Registers */

#define BRD_ID_REG_ADDR		(MX21_CS1_BASE + 0x00400000)
#define EXP_IO_REG_ADDR		(MX21_CS1_BASE + 0x00800000)

#define CPU_BRD_ID()		((*(volatile unsigned short *)(BRD_ID_REG_ADDR)) & 0x00FF)
#define BASE_BRD_ID()		((*(volatile unsigned short *)(BRD_ID_REG_ADDR)) >> 8)

#define EIO()				(*(volatile unsigned short *)(EXP_IO_REG_ADDR))
#define SetEIO(a)			*(volatile unsigned short *)(EXP_IO_REG_ADDR)=(a)
#define BitSetEIO(a)	  	*(volatile unsigned short *)(EXP_IO_REG_ADDR)|=(a)
#define BitClearEIO(a)		*(volatile unsigned short *)(EXP_IO_REG_ADDR)&=(~(a))
#define ModifyEIO(a,b)		*(volatile unsigned short *)(EXP_IO_REG_ADDR)=((*(volatile TUint16 *)(EXP_IO_REG_ADDR))&(~(a)))|(b)

#define EIO_SD_WP			0x0001
#define EIO_SW_SEL 			0x0002
#define EIO_RESET_UART 		0x0004
#define EIO_RESET_BASE		0x0008
#define EIO_CSI_CTL2		0x0010
#define EIO_CSI_CTL1		0x0020
#define EIO_CSI_CTL0		0x0040
#define EIO_UART1_EN		0x0080
#define EIO_UART2_EN		0x0000
#define EIO_UART3_EN		0x0000
#define EIO_UART4_EN		0x0100
#define EIO_LCDON			0x0200
#define EIO_IRDA_EN			0x0400
#define EIO_IRDA_FIR_SEL	0x0800
#define EIO_IRDA_MD0		0x1000
#define EIO_IRDA_MD1		0x2000
#define EIO_LED4			0x4000
#define EIO_LED3			0x8000

#endif // CYGONCE_HAL_ARM_BOARD_PLF_IO_H
