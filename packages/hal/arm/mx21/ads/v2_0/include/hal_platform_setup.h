#ifndef CYGONCE_HAL_PLATFORM_SETUP_H
#define CYGONCE_HAL_PLATFORM_SETUP_H

//=============================================================================
//
//      hal_platform_setup.h
//
//      Platform specific support for HAL (assembly code)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <pkgconf/system.h>             // System-wide configuration info
#include CYGBLD_HAL_VARIANT_H           // Variant specific configuration
#include CYGBLD_HAL_PLATFORM_H          // Platform specific configuration
#include <cyg/hal/hal_soc.h>            // Variant specific hardware definitions
#include <cyg/hal/hal_mmu.h>            // MMU definitions
#include <cyg/hal/fsl_board.h>          // Platform specific hardware definitions

#if defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1 _platform_setup1
#define CYGHWR_HAL_ARM_HAS_MMU

#ifdef CYG_HAL_STARTUP_ROMRAM
#define CYGSEM_HAL_ROM_RESET_USES_JUMP
#endif

#define CYGHWR_HAL_ROM_VADDR 0x0

// This macro represents the initial startup code for the platform
// r11 is reserved to contain chip rev info in this file
    .macro  _platform_setup1
FSL_BOARD_SETUP_START:
/*
 * invalidate I/D cache/TLB and drain write buffer
 */
    mov r0, #0
    mcr 15, 0, r0, c7, c7, 0    /* invalidate I cache and D cache */
    mcr 15, 0, r0, c8, c7, 0    /* invalidate TLBs */
    mcr 15, 0, r0, c7, c10, 4    /* Drain the write buffer */
Find_silicon_id:
    ldr r0, MX21_SI_ID_REG_W
    mov r11, #CHIP_REV_unknown
    ldr r1, [r0]
    ldr r2, MX21_SILICONID_Rev2_x_W
    cmp r1, r2
    moveq r11, #CHIP_REV_2_x
    ldr r2, =MX21_SILICONID_Rev3_0_W
    cmp r1, r2
    moveq r11, #CHIP_REV_3_0
    ldr r2, =MX21_SILICONID_Rev3_1_W
    cmp r1, r2
    moveq r11, #CHIP_REV_3_1

/*
 * Step2: setup AIPI1 and AIPI2
 */
    ldr r0, MX21_AIPI1_BASE_W
    /*
     * Note: set I2C to be 32bit access because current I2C driver
     * is written that way
     */
    mov r1, #0x00000304
    str r1, [r0]  /* PSR0 */
    mov r2, #0xFFFFFCFB
    str r2, [r0, #4]  /* PSR1 */

    ldr r0, MX21_AIPI2_BASE_W
    mov r1, #0x3F000000
    add r1, r1, #0x00FC0000
    str r1, [r0]  /* PSR0 */
    mov r2, #0xFFFFFFFF
    str r2, [r0, #4]  /* PSR1 */

    mov r0, #SDRAM_NON_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1] // for checking boot source from nand, nor or sdram

/*
 * Step3: setup System Controls
 */
    /* PCSR - priority control and select */
    ldr r0, MX21_CRM_SysCtrl_BASE_W
    mov r1, #0x03
    str r1, [r0, #CRM_SysCtrl_PCSR_Offset]
    mov r1, #0xFFFFFFC9
    str r1, [r0, #0x14]

init_max_start:
    setup_max           //setup MAX for other versions of chips

init_clock_start:
    setup_clock

    mov r0, pc
    cmp r0, #(CS0_BASE_ADDR)
    /* if we are in SDRAM, it must have been setup - skip the SDRAM setup */
    blo HWInitialise_skip_SDRAM_setup

    /* Now we must boot from Flash */

    mov r0, #NOR_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]

/*
 * Step 6: SDRAM setup
 */
    mov r1, #ESDCTL_BASE
    ldr r2, SDRAM_0x92120300
    str r2, [r1]         /* set precharge command */
    ldr r3, SDRAM_0xC0200000
    ldr r2, [r3]        /* issue precharge all command */

    /* set AutoRefresh command */
    ldr r2, SDRAM_0xA2120300
    str r2, [r1]

    /* Issue AutoRefresh command */
    mov r3, #(CSD0_BASE_ADDR)
    ldr r2, [r3]
    ldr r2, [r3]
    ldr r2, [r3]
    ldr r2, [r3]
    ldr r2, [r3]
    ldr r2, [r3]
    ldr r2, [r3]
    ldr r2, [r3]

    /* set Mode Register  */
    ldr r2, SDRAM_0xB2120300
    str r2, [r1]

    /* Issue Mode Register command. Burst Length=8 */
    ldr r3, SDRAM_0xC0119800
    ldr r2, [r3]

    /* set to Normal Mode */
/*
    ldr    r2, =(0x8212C304)
*/

    ldr r2, SDRAM_0x8212C300

    str r2, [r1]
    /* end of CSD0 config */
/*
 * End of SDRAM setup
 */

HWInitialise_skip_SDRAM_setup:

    ldr r0, NFC_BASE_W
    add r2, r0, #0x800      // 2K window
    cmp pc, r0
    blo Normal_Boot_Continue
    cmp pc, r2
    bhi Normal_Boot_Continue
NAND_Boot_Start:
    /* Copy image from flash to SDRAM first */
    ldr r1, MXC_REDBOOT_ROM_START

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    blo 1b

    /* Jump to SDRAM */
    ldr r1, CONST_0xFFF
    and r0, pc, r1     /* offset of pc */
    ldr r1, MXC_REDBOOT_ROM_START
    add r1, r1, #0x10
    add pc, r0, r1
    nop
    nop
    nop
    nop

    mov r0, #NAND_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]
    mov r0, #MXCFIS_NAND
    ldr r1, AVIC_VECTOR1_ADDR_W
    str r0, [r1]
NAND_Copy_Main:
    ldr r0, NFC_BASE_W   //r0: nfc base. Reloaded after each page copying
    mov r1, #0x800       //r1: starting flash addr to be copied. Updated constantly
    add r2, r0, #0x200   //r2: end of 1st RAM buf. Doesn't change
    add r12, r0, #0xE00  //r12: NFC register base. Doesn't change
    ldr r14, MXC_REDBOOT_ROM_START
    add r13, r14, #REDBOOT_IMAGE_SIZE //r13: end of SDRAM address for copying. Doesn't change
    add r14, r14, r1     //r14: starting SDRAM address for copying. Updated constantly

    //unlock internal buffer
    mov r3, #0x2
    strh r3, [r12, #0xA]

Nfc_Read_Page:
//  writew(FLASH_Read_Mode1, NAND_FLASH_CMD_REG);
    mov r3, #0x0;
    strh r3, [r12, #NAND_FLASH_CMD_REG_OFF]
    mov r3, #NAND_FLASH_CONFIG2_FCMD_EN;
    strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
    do_wait_op_done

//    start_nfc_addr_ops(ADDRESS_INPUT_READ_PAGE, addr, nflash_dev_info->base_mask);
    mov r3, r1
    do_addr_input       //1st addr cycle
    mov r3, r1, lsr #9
    do_addr_input       //2nd addr cycle
    mov r3, r1, lsr #17
    do_addr_input       //3rd addr cycle
    mov r3, r1, lsr #25
    do_addr_input       //4th addr cycle

//    NFC_DATA_OUTPUT(buf, FDO_PAGE_SPARE_VAL);
//        writew(NAND_FLASH_CONFIG1_ECC_EN, NAND_FLASH_CONFIG1_REG);
//    mov r3, #(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN)
    mov r3, #(NAND_FLASH_CONFIG1_ECC_EN)
    strh r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]

//        writew(buf_no, RAM_BUFFER_ADDRESS_REG);
    mov r3, #0
    strh r3, [r12, #RAM_BUFFER_ADDRESS_REG_OFF]
//        writew(FDO_PAGE_SPARE_VAL & 0xFF, NAND_FLASH_CONFIG2_REG);
    mov r3, #FDO_PAGE_SPARE_VAL
    strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
//        wait_op_done();
    do_wait_op_done

    // check for bad block
    mov r3, r1, lsl #(32-5-9)
    cmp r3, #(512 << (32-5-9))
    bhi Copy_Good_Blk
    add r4, r0, #0x800  //r3 -> spare area buf 0
    ldrh r4, [r4, #0x4]
    and r4, r4, #0xFF00
    cmp r4, #0xFF00
    beq Copy_Good_Blk
    // really sucks. Bad block!!!!
    cmp r3, #0x0
    beq Skip_bad_block
    // even suckier since we already read the first page!
    sub r14, r14, #512  //rewind 1 page for the sdram pointer
    sub r1, r1, #512    //rewind 1 page for the flash pointer
Skip_bad_block:
    add r1, r1, #(32*512)
    b Nfc_Read_Page
Copy_Good_Blk:
    //copying page
1:  ldmia r0!, {r3-r10}
    stmia r14!, {r3-r10}
    cmp r0, r2
    blo 1b
    cmp r14, r13
    bge NAND_Copy_Main_done
    add r1, r1, #0x200
    ldr r0, NFC_BASE_W
    b Nfc_Read_Page

NAND_Copy_Main_done:

Normal_Boot_Continue:

#ifdef CYG_HAL_STARTUP_ROMRAM     /* enable running from RAM */
    /* Copy image from flash to SDRAM first */
    ldr r0, =0xFFFFF000
    and r0, r0, pc
    ldr r1, MXC_REDBOOT_ROM_START
    cmp r0, r1
    beq HWInitialise_skip_SDRAM_copy

    add r2, r0, #REDBOOT_IMAGE_SIZE

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    ble 1b
    /* Jump to SDRAM */
    ldr r1, =0xFFFF
    and r0, pc, r1         /* offset of pc */
    ldr r1, =(SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000 + 0x8)
    add pc, r0, r1
    nop
    nop
    nop
    nop
#endif /* CYG_HAL_STARTUP_ROMRAM */

HWInitialise_skip_SDRAM_copy:

CS0_Setup: /* for burst flash */
    ldr r1, =(MX21_EIM_BASE)
    ldr r2, =(0x00000800)     /* 8 wait states */
    str r2, [r1]              /* CS0 UPCTRL */
    ldr r2, =(0x00000E01)     /* 32bit data port size */
    str r2, [r1, #4]           /* CS0 LOCTRL */

CS1_Setup: /* ADS board expanded IOs */
    /* CS1 is setup as 16bit port on D[15:0]. May need to configure the rest later */
    ldr r1, =(MX21_EIM_BASE)
    ldr r2, =0x2000
    str r2, [r1, #MX21_CS1_UPCTRL]
    ldr r2, =0x11118501
    str r2, [r1, #MX21_CS1_LOCTRL]

NAND_ClockSetup:
    ldr r1, =(MX21_CRM_BASE)
    ldr r2, [r1, #MX21_CRM_PCDR0]
    and r2, r2, #0xFFFF0FFF
    orr r2, r2, #0x7000
    str r2, [r1, #MX21_CRM_PCDR0]

/* end of NAND clock divider setup */

    // Set up a stack [for calling C code]
    ldr r1, =__startup_stack
    ldr r2, =RAM_BANK0_BASE
    orr sp, r1, r2

    // Create MMU tables
    bl hal_mmu_init

    // Enable MMU
    ldr r2, =10f
    mrc    MMU_CP, 0, r1, MMU_Control, c0      // get c1 value to r1 first
    orr r1, r1, #7                          // enable MMU bit
    mcr    MMU_CP, 0, r1, MMU_Control, c0
    mov    pc,r2    /* Change address spaces */
    nop
    nop
    nop
10:

    // Save shadow copy of BCR, also hardware configuration
    ldr r1, =_board_BCR
    str r2,[r1]
    ldr r1, =_board_CFG
    str r9,[r1]                // Saved far above...

    .endm                       // _platform_setup1

#else // defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1
#endif

    /* Setup the clocks. After this setup, the clock values are:
     *               TO2(MHz)
     *   FCLK        266(~270)
     *   HCLK        133
     *   PerClk1     44.3
     *   PerClk2     33.25
     *   NFCClk      66.5
     */
    .macro setup_clock
        ldr r0, MX21_CRM_BASE_W

        /* Configure MPCTL0 */
        ldr r1, MX21_CRM_MPCTL0_W
//        ldr r1, =0x83FF29B9
        str r1, [r0, #MX21_CRM_MPCTL0]

        /* Configure MPCTL1 */
        ldr r1, =0x00000040
        str r1, [r0, #MX21_CRM_MPCTL1]

        /* Configure SPCTL0 */
        ldr r1, MX21_CRM_SPCTL0_W
        str r1, [r0, #MX21_CRM_SPCTL0]

        /* issue SPLL restart and MPLL restart */
        ldr r1, [r0, #MX21_CRM_CSCR]
        orr r1, r1, #0x00600000
        str r1, [r0, #MX21_CRM_CSCR]
    SPLL_Not_Locked:
        ldr r1, [r0, #MX21_CRM_SPCTL1]
        ands r1, r1, #0x00008000
        beq SPLL_Not_Locked
    MPLL_Not_Locked:
        ldr r1, [r0, #MX21_CRM_MPCTL1]
        ands r1, r1, #0x00008000
        beq MPLL_Not_Locked      /* reach here means MPLL is locked okay */

        // add some delay here
        mov r1, #0x100
    1:  subs r1, r1, #0x1
        bne 1b

        /*BLKDIV =1 for 266Mhz FCLK and HCLK = 133MHz, change the PRESC to 0 for this, else its 1 for 133MHZ FCLK */
        ldr r2, MX21_CRM_CSCR_W
        str r2, [r0, #MX21_CRM_CSCR]

        /* Configure PCDR */
        /* Configure PCDR0 for TO2 and TO3*/
        ldr r1, MX21_CRM_PCDR0_W    /*  set SSI2DIV=0x29, SSI1DIV=0x19 NFCDIV=7, FIRIDIV = 7 */
        str r1, [r0, #MX21_CRM_PCDR0]

        /* Configure PCDR1 */
        ldr r1, MX21_CRM_PCDR1_W
        str r1, [r0, #MX21_CRM_PCDR1]

        /*
         * Configure PCCR0 and PCCR1
         * Only enable peripheral clocks for: SAHARA, BROM, USBOTG, NFC, GPIO,
         * UART1,2,3%4 and timer1&2, WDOG
         */
        ldr r1, MX21_CRM_PCCR0_W
        str r1, [r0, #MX21_CRM_PCCR0]
        ldr r1, =0x07000000
        str r1, [r0, #MX21_CRM_PCCR1]
        // make default CLKO to be FCLK
        ldr r1, [r0, #MX21_CRM_CCSR]
        and r1, r1, #0xFFFFFFE0
        orr r1, r1, #0x7
        str r1, [r0, #MX21_CRM_CCSR]
    .endm //setup_clock

    .macro setup_max
        ldr r0, MX21_MAX_BASE_W
        add r1, r0, #MAX_Port1_OFFSET
        add r2, r0, #MAX_Port2_OFFSET
        add r3, r0, #MAX_Port3_OFFSET
        add r4, r0, #MAX_Port4_OFFSET
        add r5, r0, #MAX_Port5_OFFSET
        add r0, r0, #MAX_Port0_OFFSET

        /* MPR and AMPR */
        ldr r6, MX21_MAX_0x00123045         /* Priority LCD>EMMA>DMA>USB>DAHB>IAHB */
        str r6, [r0, #MAX_Slave_MPR_Offset]   /* same for all slave ports */
        str r6, [r0, #MAX_Slave_AMPR_Offset]
        str r6, [r1, #MAX_Slave_MPR_Offset]
        str r6, [r1, #MAX_Slave_AMPR_Offset]
        str r6, [r2, #MAX_Slave_MPR_Offset]
        str r6, [r2, #MAX_Slave_AMPR_Offset]
        str r6, [r3, #MAX_Slave_MPR_Offset]
        str r6, [r3, #MAX_Slave_AMPR_Offset]
        /* ASGPCR */
        /*
         HPE bits removed in TO2, ARB added.
         ARB - Arbitration mode (0 - fixed priority,
                                 01 - round robin priority)
         */
        ldr r6, =0x00000001             /* HLP=0, ARB=0, park ARM */
        str r6, [r0, #MAX_Slave_ASGPCR_Offset] /* for slave port 0 */
        ldr r6, =0x00000001             /* HLP=0, ARB=0, park ARM */
        str r6, [r1, #MAX_Slave_ASGPCR_Offset] /* for slave port 1 */
        ldr r6, =0x00000004             /* HLP=0, ARB=0, part LCD */
        str r6, [r2, #MAX_Slave_ASGPCR_Offset] /* for slave port 2 */
        ldr r6, =0x00000005             /* HLP=0, ARB=0, park EMMA */
        str r6, [r3, #MAX_Slave_ASGPCR_Offset] /* for slave port 3 */

        /* SGPCR - this has to be the last since RO bit is set here */
        ldr r6, =0x00000001             /* HLP=0, ARB=0, park ARM */
        str r6, [r0, #MAX_Slave_SGPCR_Offset] /* for slave port 0 */
        ldr r6, =0x00000001             /* HLP=0, ARB=0, park ARM */
        str r6, [r1, #MAX_Slave_SGPCR_Offset] /* for slave port 1 */
        ldr r6, =0x00000004             /* HLP=0, ARB=0, part LCD */
        str r6, [r2, #MAX_Slave_SGPCR_Offset] /* for slave port 2 */
        ldr r6, =0x00000005             /* HLP=0, ARB=0, park EMMA */
        str r6, [r3, #MAX_Slave_SGPCR_Offset] /* for slave port 3 */
        /* MGPCR */
        mov r6, #0x0    /* leave as default */
        str r6, [r0, #MAX_Master_MGPCR_Offset]
        str r6, [r1, #MAX_Master_MGPCR_Offset]
        str r6, [r2, #MAX_Master_MGPCR_Offset]
        str r6, [r3, #MAX_Master_MGPCR_Offset]
        str r6, [r4, #MAX_Master_MGPCR_Offset]
        str r6, [r5, #MAX_Master_MGPCR_Offset]
    .endm //setup_max


    .macro do_wait_op_done
    1:
        ldrh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        ands r3, r3, #NAND_FLASH_CONFIG2_INT_DONE
        beq 1b
        mov r3, #0x0
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
    .endm   // do_wait_op_done

    .macro do_addr_input
        and r3, r3, #0xFF
        strh r3, [r12, #NAND_FLASH_ADD_REG_OFF]
        mov r3, #NAND_FLASH_CONFIG2_FADD_EN
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        do_wait_op_done
    .endm   // do_addr_input

#define PLATFORM_VECTORS         _platform_vectors
    .macro  _platform_vectors
        .globl  _board_BCR, _board_CFG
_board_BCR:   .long   0       // Board Control register shadow
_board_CFG:   .long   0       // Board Configuration (read at RESET)
    .endm

MXC_REDBOOT_ROM_START:      .word   SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000
CONST_0xFFF:                .word   0xFFF
AVIC_VECTOR0_ADDR_W:        .word   MXCBOOT_FLAG_REG
AVIC_VECTOR1_ADDR_W:        .word   MXCFIS_FLAG_REG
MX21_SI_ID_REG_W:           .word   MX21_SI_ID_REG
MX21_SILICONID_Rev2_x_W:    .word   MX21_SILICONID_Rev2_x
MX21_SILICONID_Rev3_0_W:    .word   MX21_SILICONID_Rev3_0
MX21_SILICONID_Rev3_1_W:    .word   MX21_SILICONID_Rev3_1
MX21_AIPI1_BASE_W:          .word   MX21_AIPI1_BASE
MX21_AIPI2_BASE_W:          .word   MX21_AIPI2_BASE
MX21_CRM_SysCtrl_BASE_W:    .word   MX21_CRM_SysCtrl_BASE
MX21_MAX_BASE_W:            .word   MX21_MAX_BASE
MX21_MAX_0x00123045:        .word   0x00123045
MX21_CRM_BASE_W:            .word   MX21_CRM_BASE
MX21_CRM_MPCTL0_W:          .word   CRM_MPCTL0_PD+CRM_MPCTL0_MFI+CRM_MPCTL0_MFD+CRM_MPCTL0_MFN
MX21_CRM_SPCTL0_W:          .word   CRM_SPCTL0_PD+CRM_SPCTL0_MFI+CRM_SPCTL0_MFD+CRM_SPCTL0_MFN
MX21_CRM_CSCR_W:            .word   0x17000607
MX21_CRM_PCDR0_W:           .word   0x64197007
MX21_CRM_PCDR1_W:           .word   0x02070705
MX21_CRM_PCCR0_W:           .word   0x3108480F
NFC_BASE_W:                 .word   NFC_BASE
SDRAM_0x92120300:           .word   0x92120300
SDRAM_0xC0200000:           .word   0xC0200000
SDRAM_0xA2120300:           .word   0xA2120300
SDRAM_0xB2120300:           .word   0xB2120300
SDRAM_0xC0119800:           .word   0xC0119800
SDRAM_0x8212C300:           .word   0x8212C300

/*---------------------------------------------------------------------------*/
/* end of hal_platform_setup.h                                               */
#endif /* CYGONCE_HAL_PLATFORM_SETUP_H */
