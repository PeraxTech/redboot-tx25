#ifndef CYGONCE_HAL_PLATFORM_SETUP_H
#define CYGONCE_HAL_PLATFORM_SETUP_H

//=============================================================================
//
//      hal_platform_setup.h
//
//      Platform specific support for HAL (assembly code)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <pkgconf/system.h>             // System-wide configuration info
#include CYGBLD_HAL_VARIANT_H           // Variant specific configuration
#include CYGBLD_HAL_PLATFORM_H          // Platform specific configuration
#include <cyg/hal/hal_soc.h>            // Variant specific hardware definitions
#include <cyg/hal/hal_mmu.h>            // MMU definitions
#include <cyg/hal/fsl_board.h>          // Platform specific hardware definitions

#if defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1 _platform_setup1
#define CYGHWR_HAL_ARM_HAS_MMU

#ifdef CYG_HAL_STARTUP_ROMRAM
#define CYGSEM_HAL_ROM_RESET_USES_JUMP
#endif

#define CYGHWR_HAL_ROM_VADDR    0x0

// This macro represents the initial startup code for the platform
    .macro  _platform_setup1
FSL_BOARD_SETUP_START:
/*
 *       ARM1136 init
 *       - invalidate I/D cache/TLB and drain write buffer;
 *       - invalidate L2 cache
 *       - unaligned access
 *       - branch predictions
 */
#ifdef TURN_OFF_IMPRECISE_ABORT
    mrs r0, cpsr
    bic r0, r0, #0x100
    msr cpsr, r0
#endif
    mov r0, #0
    mcr 15, 0, r0, c7, c7, 0        /* invalidate I cache and D cache */
    mcr 15, 0, r0, c8, c7, 0        /* invalidate TLBs */
    mcr 15, 0, r0, c7, c10, 4       /* Drain the write buffer */

    /* Also setup the Peripheral Port Remap register inside the core */
    ldr r0, ARM_PPMRR        /* start from AIPS 2GB region */
    mcr p15, 0, r0, c15, c2, 4

    /*** L2 Cache setup/invalidation/disable ***/
    /* Disable L2 cache first */
    mov r0, #L2CC_BASE_ADDR
    ldr r2, [r0, #L2_CACHE_CTL_REG]
    bic r2, r2, #0x1
    str r2, [r0, #L2_CACHE_CTL_REG]
    /*
     * Configure L2 Cache:
     * - 128k size(16k way)
     * - 8-way associativity
     * - 0 ws TAG/VALID/DIRTY
     * - 4 ws DATA R/W
     */
    ldr r1, [r0, #L2_CACHE_AUX_CTL_REG]
    and r1, r1, #0xFE000000
    ldr r2, L2CACHE_PARAM
    orr r1, r1, r2
    str r1, [r0, #L2_CACHE_AUX_CTL_REG]

    /* Invalidate L2 */
    mov r1, #0x000000FF
    str r1, [r0, #L2_CACHE_INV_WAY_REG]
L2_loop:
    /* Poll Invalidate By Way register */
    ldr r2, [r0, #L2_CACHE_INV_WAY_REG]
    cmp r2, #0
    bne L2_loop
    /*** End of L2 operations ***/

    mov r0, #SDRAM_NON_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1] // for checking boot source from nand, nor or sdram
/*
 * End of ARM1136 init
 */
init_spba_start:
    init_spba
init_aips_start:
    init_aips
init_max_start:
    init_max
init_m3if_start:
    init_m3if

    init_drive_strength
//    init_cs0_sync
init_cs0_start:
    init_cs0_async

    /* If SDRAM has been setup, bypass clock/WEIM setup */
    cmp pc, #SDRAM_BASE_ADDR
    blo init_clock_start
    cmp pc, #(SDRAM_BASE_ADDR + SDRAM_SIZE)
    blo HWInitialise_skip_SDRAM_setup

    mov r0, #NOR_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]

init_clock_start:
    init_clock

init_sdram_start:
    init_ddr_sdram

HWInitialise_skip_SDRAM_setup:

    mov r0, #NFC_BASE
    add r2, r0, #0x800      // 2K window
    cmp pc, r0
    blo Normal_Boot_Continue
    cmp pc, r2
    bhi Normal_Boot_Continue
NAND_Boot_Start:
    /* Copy image from flash to SDRAM first */
    ldr r1, MXC_REDBOOT_ROM_START

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    blo 1b
    /* Jump to SDRAM */
    ldr r1, CONST_0x0FFF
    and r0, pc, r1     /* offset of pc */
    ldr r1, MXC_REDBOOT_ROM_START
    add r1, r1, #0x10
    add pc, r0, r1
    nop
    nop
    nop
    nop
NAND_Copy_Main:
    mov r0, #NAND_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]
    mov r0, #MXCFIS_NAND
    ldr r1, AVIC_VECTOR1_ADDR_W
    str r0, [r1]

    mov r0, #NFC_BASE;   //r0: nfc base. Reloaded after each page copying
    mov r1, #0x800       //r1: starting flash addr to be copied. Updated constantly
    add r2, r0, #0x200   //r2: end of 1st RAM buf. Doesn't change
    add r12, r0, #0xE00  //r12: NFC register base. Doesn't change
    ldr r14, MXC_REDBOOT_ROM_START
    add r13, r14, #REDBOOT_IMAGE_SIZE //r13: end of SDRAM address for copying. Doesn't change
    add r14, r14, r1     //r14: starting SDRAM address for copying. Updated constantly

    //unlock internal buffer
    mov r3, #0x2
    strh r3, [r12, #0xA]

Nfc_Read_Page:
//  writew(FLASH_Read_Mode1, NAND_FLASH_CMD_REG);
    mov r3, #0x0;
    strh r3, [r12, #NAND_FLASH_CMD_REG_OFF]
    mov r3, #NAND_FLASH_CONFIG2_FCMD_EN;
    strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
    do_wait_op_done

//    start_nfc_addr_ops(ADDRESS_INPUT_READ_PAGE, addr, nflash_dev_info->base_mask);
    mov r3, r1
    do_addr_input       //1st addr cycle
    mov r3, r1, lsr #9
    do_addr_input       //2nd addr cycle
    mov r3, r1, lsr #17
    do_addr_input       //3rd addr cycle
    mov r3, r1, lsr #25
    do_addr_input       //4th addr cycle

//    NFC_DATA_OUTPUT(buf, FDO_PAGE_SPARE_VAL);
//        writew(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN,
//               NAND_FLASH_CONFIG1_REG);
    mov r3, #(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN)
    strh r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]

//        writew(buf_no, RAM_BUFFER_ADDRESS_REG);
    mov r3, #0
    strh r3, [r12, #RAM_BUFFER_ADDRESS_REG_OFF]
//        writew(FDO_PAGE_SPARE_VAL & 0xFF, NAND_FLASH_CONFIG2_REG);
    mov r3, #FDO_PAGE_SPARE_VAL
    strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
//        wait_op_done();
    do_wait_op_done

    // check for bad block
    mov r3, r1, lsl #(32-5-9)
    cmp r3, #(512 << (32-5-9))
    bhi Copy_Good_Blk
    add r4, r0, #0x800  //r3 -> spare area buf 0
    ldrh r4, [r4, #0x4]
    and r4, r4, #0xFF00
    cmp r4, #0xFF00
    beq Copy_Good_Blk
    // really sucks. Bad block!!!!
    cmp r3, #0x0
    beq Skip_bad_block
    // even suckier since we already read the first page!
    sub r14, r14, #512  //rewind 1 page for the sdram pointer
    sub r1, r1, #512    //rewind 1 page for the flash pointer
Skip_bad_block:
    add r1, r1, #(32*512)
    b Nfc_Read_Page
Copy_Good_Blk:
    //copying page
1:  ldmia r0!, {r3-r10}
    stmia r14!, {r3-r10}
    cmp r0, r2
    blo 1b
    cmp r14, r13
    bge NAND_Copy_Main_done
    add r1, r1, #0x200
    mov r0, #NFC_BASE
    b Nfc_Read_Page

NAND_Copy_Main_done:

Normal_Boot_Continue:

#ifdef CYG_HAL_STARTUP_ROMRAM     /* enable running from RAM */
    /* Copy image from flash to SDRAM first */
    ldr r0, =0xFFFFF000
    and r0, r0, pc
    ldr r1, MXC_REDBOOT_ROM_START
    cmp r0, r1
    beq HWInitialise_skip_SDRAM_copy

    add r2, r0, #REDBOOT_IMAGE_SIZE

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    ble 1b
    /* Jump to SDRAM */
    ldr r1, =0xFFFF
    and r0, pc, r1         /* offset of pc */
    ldr r1, =(SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000 + 0x8)
    add pc, r0, r1
    nop
    nop
    nop
    nop
#endif /* CYG_HAL_STARTUP_ROMRAM */

HWInitialise_skip_SDRAM_copy:
    init_dsp
init_cs4_start:
    init_cs4

NAND_ClockSetup:

/*
 * Note:
 *     IOMUX/PBC setup is done in C function plf_hardware_init() for simplicity
 */

STACK_Setup:
    // Set up a stack [for calling C code]
    ldr r1, =__startup_stack
    ldr r2, =RAM_BANK0_BASE
    orr sp, r1, r2

    // Create MMU tables
    bl hal_mmu_init

    // Enable MMU
    ldr r2, =10f
    mrc MMU_CP, 0, r1, MMU_Control, c0      // get c1 value to r1 first
    orr r1, r1, #7                          // enable MMU bit
    mcr MMU_CP, 0, r1, MMU_Control, c0
    mov pc,r2    /* Change address spaces */
    nop
    nop
    nop
10:

    // Save shadow copy of BCR, also hardware configuration
    ldr r1, =_board_BCR
    str r2, [r1]
    ldr r1, =_board_CFG
    str r9, [r1]                // Saved far above...

    .endm                       // _platform_setup1

#else // defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1
#endif

    /* Allow all 3 masters to have access to these shared peripherals */
    .macro  init_spba
        ldr r0, SPBA_CTRL_BASE_ADDR_W
        add r4, r0, #0x38
        ldr r1, =0x7            /* allow all 3 masters access */
        ldr r2, SPBA_LOCK_VAL
spba_continue:
        str r1, [r0]
spba_check_loop:
        ldr r3, [r0]
        cmp r2, r3
        bne spba_check_loop
        add r0, r0, #4
        cmp r0, r4
        ble spba_continue
    .endm  /* init_spba */

    /* AIPS setup - Only setup MPROTx registers. The PACR default values are good.*/
    .macro init_aips
        /*
         * Set all MPROTx to be non-bufferable, trusted for R/W,
         * not forced to user-mode.
         */
        ldr r0, AIPS1_CTRL_BASE_ADDR_W
        ldr r1, AIPS1_PARAM_W
        str r1, [r0, #0x00]
        str r1, [r0, #0x04]
        ldr r0, AIPS2_CTRL_BASE_ADDR_W
        str r1, [r0, #0x00]
        str r1, [r0, #0x04]

        /*
         * Clear the on and off peripheral modules Supervisor Protect bit
         * for SDMA to access them. Did not change the AIPS control registers
         * (offset 0x20) access type
         */
        ldr r0, AIPS1_CTRL_BASE_ADDR_W
        ldr r1, =0x0
        str r1, [r0, #0x40]
        str r1, [r0, #0x44]
        str r1, [r0, #0x48]
        str r1, [r0, #0x4C]
        ldr r1, [r0, #0x50]
        and r1, r1, #0x00FFFFFF
        str r1, [r0, #0x50]

        ldr r0, AIPS2_CTRL_BASE_ADDR_W
        ldr r1, =0x0
        str r1, [r0, #0x40]
        str r1, [r0, #0x44]
        str r1, [r0, #0x48]
        str r1, [r0, #0x4C]
        ldr r1, [r0, #0x50]
        and r1, r1, #0x00FFFFFF
        str r1, [r0, #0x50]
    .endm /* init_aips */

    /* MAX (Multi-Layer AHB Crossbar Switch) setup */
    .macro init_max
        ldr r0, MAX_BASE_ADDR_W
        /* MPR - priority is M4 > M2 > M3 > M5 > M0 > M1 */
        ldr r1, MAX_PARAM1
        str r1, [r0, #0x000]        /* for S0 */
        str r1, [r0, #0x100]        /* for S1 */
        str r1, [r0, #0x200]        /* for S2 */
        str r1, [r0, #0x300]        /* for S3 */
        str r1, [r0, #0x400]        /* for S4 */
        /* SGPCR - always park on last master */
        ldr r1, =0x10
        str r1, [r0, #0x010]        /* for S0 */
        str r1, [r0, #0x110]        /* for S1 */
        str r1, [r0, #0x210]        /* for S2 */
        str r1, [r0, #0x310]        /* for S3 */
        str r1, [r0, #0x410]        /* for S4 */
        /* MGPCR - restore default values */
        ldr r1, =0x0
        str r1, [r0, #0x800]        /* for M0 */
        str r1, [r0, #0x900]        /* for M1 */
        str r1, [r0, #0xA00]        /* for M2 */
        str r1, [r0, #0xB00]        /* for M3 */
        str r1, [r0, #0xC00]        /* for M4 */
        str r1, [r0, #0xD00]        /* for M5 */
    .endm /* init_max */

    /* Clock setup */
    .macro init_clock
        /* RVAL/WVAL for L2 cache memory */
        ldr r0, RVAL_WVAL_W
        ldr r1, CLKCTL_BASE_ADDR_W
        str r0, [r1, #0x10]

        ldr r0, CRM_MCU_BASE_ADDR_W
#ifdef CYGPKG_HAL_ARM_MXC91331_CHIP
        ldr r1, CRM_MCR_0x18FF2902
        str r1, [r0, #CLKCTL_MCR]
upll_lock:
        ldr r1, [r0, #CLKCTL_MCR]
        ands r1, r1, #0x80
        beq upll_lock
#endif
#ifdef CYGPKG_HAL_ARM_MXC91321_CHIP
        // enable MPLL, UPLL, TurboPLL
        ldr r1, CRM_MCR_0x18FF2952
        str r1, [r0, #CLKCTL_MCR]
check_pll_lock:
        ldr r1, [r0, #CLKCTL_MCR]
        and r1, r1, #0x8C
        cmp r1, #0x8C
        bne check_pll_lock
#endif
        /*
         * J10 (CPU card) - CKO1=MCU_PLL div by 8
         * J9 (CPU card) - CKO2=IPG_CLK_ARM div by 8
         */
        ldr r1, CRM_COSR_0x00036C58
        str r1, [r0, #CLKCTL_COSR]

        b 1f
        .ALIGN 4
1:
        ldr r1, PDR0_399_100_50_W
        str r1, [r0, #CLKCTL_PDR0]
        ldr r1, MPCTL_PARAM_399_W
        str r1, [r0, #CLKCTL_MPCTL]

        /* Set to default values */
        ldr r1, PDR1_0x2910AC56_W
        str r1, [r0, #CLKCTL_PDR1]
        /* Set UPLL=288MHz */
        ldr r1, UPCTL_PARAM_288_W
        str r1, [r0, #CLKCTL_UPCTL]
    .endm /* init_clock */

    /* M3IF setup */
    .macro init_m3if
        /* Configure M3IF registers */
        ldr r1, M3IF_BASE_W
        /*
        * M3IF Control Register (M3IFCTL)
        * MRRP[0] = TMAX not on priority list (0 << 0)        = 0x00000000
        * MRRP[1] = SMIF not on priority list (0 << 0)        = 0x00000000
        * MRRP[2] = MAX0 not on priority list (0 << 0)        = 0x00000000
        * MRRP[3] = MAX1 not on priority list (0 << 0)        = 0x00000000
        * MRRP[4] = SDMA not on priority list (0 << 0)        = 0x00000000
        * MRRP[5] = MPEG4 not on priority list (0 << 0)       = 0x00000000
        * MRRP[6] = IPU on priority list (1 << 6)             = 0x00000040
        * MRRP[7] = SMIF-L2CC not on priority list (0 << 0)   = 0x00000000
        *                                                       ------------
        *                                                       0x00000040
        */
        ldr r0, =0x00000040
        str r0, [r1]  /* M3IF control reg */
    .endm /* init_m3if */
    /* CS0 sync mode setup */

    .macro init_cs0_sync
        ldr r0, WEIM_BASE_ADDR_W
        ldr r1, CS0_CSCRU_0x23D29000
        str r1, [r0, #CSCRU]
        ldr r1, CS0_CSCRL_0x60000D01
        str r1, [r0, #CSCRL]
        mov r1, #0x00000080
        str r1, [r0, #CSCRA]
        mov r0, #CS0_BASE_ADDR
        add r0, r0, #0x00004300
        mov r1, #0x60
        strh r1, [r0, #0x9E]
        mov r1, #0x03
        strh r1, [r0, #0x9E]
    .endm /* init_cs0_sync */

    /* CS0 async mode setup */
    .macro init_cs0_async
        /* Async flash mode */
        /* CS0 setup: Configuring the CS0 in Asynchronous mode */
        ldr r0, WEIM_BASE_ADDR_W         /* 0xB8002000 */

        ldr r1, CS0_CSCRU_0x20428F00             /* no sync/burst. 8 wait states */
        str r1, [r0, #CSCRU]            /* +0x00 */
        ldr r1, CS0_CSCRL_0x60000D01             /* 16-bit port, CS enabled */
        str r1, [r0, #CSCRL]            /* +0x04 */
        mov r1, #0x00000080             /* decrease Write Wait State enabled */
        str r1, [r0, #CSCRA]            /* +0x08 */
    .endm /* init_cs0_async */

    /* CS4 setup */
    .macro init_cs4
        ldr r0, =WEIM_CTRL_CS4
        ldr r1, =0x0000DCF6
        str r1, [r0, #CSCRU]
        ldr r1, =0x444A4531
        str r1, [r0, #CSCRL]
        ldr r1, =0x44443302
        str r1, [r0, #CSCRA]
    .endm /* init_cs4 */

// DDR SDRAM setup
    .macro  init_ddr_sdram
        ldr r0, ESDCTL_BASE_W
        mov r2, #SDRAM_BASE_ADDR
        mov r1, #0x2                    /* reset */
        str r1, [r0, #ESDCTL_ESDMISC]
        ldr r1, SDRAM_0x00395728
        str r1, [r0, #ESDCTL_ESDCFG1]
        ldr r1, =0x14                   /* DDR */
        str r1, [r0, #ESDCTL_ESDMISC]

        // Hold for more than 200ns
        ldr r1, =0x10000
     1:
        subs r1, r1, #0x1
        bne 1b

        ldr r1, SDRAM_0x92116080
        str r1, [r0, #ESDCTL_ESDCTL1]
        ldr r1, =0x0
        mov r12, #0x90000000
        add r12, r12, #0x00000400
        strh r1, [r12]
        ldr r1, SDRAM_0xA2116080
        str r1, [r0, #ESDCTL_ESDCTL1]

        ldr r1, =0x0
        strh r1, [r2]
        strh r1, [r2]

        ldr r1, SDRAM_0xB2116080
        str r1, [r0, #ESDCTL_ESDCTL1]

        ldr r1, =0x0
        strb r1, [r2, #0x33]
        mov r12, #0x91000000
        strb r1, [r12]
        ldr r3, SDRAM_0x82116080     /* 16 bit memory */
        str r3, [r0, #ESDCTL_ESDCTL1]
        ldr r1, =0x0
        strh r1, [r2]
    .endm

    .macro do_wait_op_done
        mov r3, #0x1000
    1:
        subs r3, r3, #0x1
        bne 1b
    1:
        ldrh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        ands r3, r3, #NAND_FLASH_CONFIG2_INT_DONE
        beq 1b
        mov r3, #0x0
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
    .endm   // do_wait_op_done

    .macro do_addr_input
        and r3, r3, #0xFF
        strh r3, [r12, #NAND_FLASH_ADD_REG_OFF]
        mov r3, #NAND_FLASH_CONFIG2_FADD_EN
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        do_wait_op_done
    .endm   // do_addr_input

    /* To support 133MHz SDR */
    .macro  init_drive_strength
        // max drive strength for all pads except SDQS0/1

        ldr r0, IOMUXC_BASE_ADDR_W
        add r0, r0, #0x300

        //============= set_drive_strength_ctl_data ========================
        /* SDRAM SD0-SD15 data lines sw_pad_ctl_sd0_sd1_sd2 */
        ldr r1, [r0, #0xF8]
        ldr r2, =0x00700000
        orr r1, r1, r2
        str r1, [r0, #0xF8]

        //=========== set_drive_strenght_ctl_signals =======================
        /* OE line sw_pad_ctl_dqm3_oe_b_cs0_b */
        /* Set max drive strength for OE line by sw_pad_ctl_oe_b */
        ldr r1, [r0, #0xC4]
        ldr r2, =0x00001C00
        add r2, r2, #0x07
        orr r1, r1, r2
        str r1, [r0, #0xC4]

    .endm /* init_drive_strength */

    .macro init_dsp
/*
 * Deal with DSP reset
 */
        /* Set DSP to LE */
        ldr r0, =0x5001C808
        ldr r1, [r0]
        cmp r1, #0x0
        beq skip_dsp_reset
        ldr r1, =0x0
        str r1, [r0]

        ldr r0, =0x43F84024
        /* Put DSP in reset */
        ldr r1, =0x00000010
        str r1, [r0]

        /* Hold for some time */
        ldr r2, =0x80000
dsp_reset_delay:
        subs r2, r2, #0x1
        bne dsp_reset_delay

        /* Put DSP out of reset */
        ldr r1, =0x0
        str r1, [r0]
skip_dsp_reset:
    .endm /* init_dsp */
#define PLATFORM_VECTORS         _platform_vectors
    .macro  _platform_vectors
        .globl  _board_BCR, _board_CFG
_board_BCR:   .long   0       // Board Control register shadow
_board_CFG:   .long   0       // Board Configuration (read at RESET)
    .endm

#define PLATFORM_PREAMBLE _switch_to_le

    .macro  _switch_to_le
        .word 0xEE110F10        // mrc 15, 0, r0, c1, c0, 0
        .word 0xE3C00080        // bic r0, r0, #0x80
        .word 0xEE010F10        // mcr 15, 0, r0, c1, c0, 0

        .word 0x0F10EE11        // mrc 15, 0, r0, c1, c0, 0
        .word 0x0080E3C0        // bic r0, r0, #0x80
        .word 0x0F10EE01        // mcr 15, 0, r0, c1, c0, 0
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
    .endm
#if 0 /// good for SDRAM since 32bit
        .word 0xEE110F10        // mrc 15, 0, r0, c1, c0, 0
        .word 0xE3C00080        // bic r0, r0, #0x80
        .word 0xEE010F10        // mcr 15, 0, r0, c1, c0, 0
#endif

ARM_PPMRR:              .word   0x40000015
L2CACHE_PARAM:          .word   0x00030024
IIM_SREV_REG_VAL:       .word   IIM_BASE_ADDR + IIM_SREV_OFF
AIPS1_CTRL_BASE_ADDR_W: .word   AIPS1_CTRL_BASE_ADDR
AIPS2_CTRL_BASE_ADDR_W: .word   AIPS2_CTRL_BASE_ADDR
AIPS1_PARAM_W:          .word   0x77777777
MAX_BASE_ADDR_W:        .word   MAX_BASE_ADDR
MAX_PARAM1:             .word   0x00302154
RVAL_WVAL_W:            .word   0x515
CLKCTL_BASE_ADDR_W:     .word   CLKCTL_BASE_ADDR
CRM_MCR_0x18FF2902:     .word   0x18FF2902
CRM_MCR_0x18FF2952:     .word   0x18FF2952
CRM_COSR_0x00036C58:    .word   0x00036C58
PDR0_399_100_50_W:      .word   PDR0_399_100_50
PDR0_399_133_66_W:      .word   PDR0_399_133_66
PDR0_399_66_66_W:       .word   PDR0_399_66_66
PDR1_0x2910AC56_W:      .word   0x2910AC56
MPCTL_PARAM_399_W:      .word   MPCTL_PARAM_399
UPCTL_PARAM_288_W:      .word   UPCTL_PARAM_288
TPCTL_PARAM_500_W:      .word   TPCTL_PARAM_500
TPCTL_PARAM_532_W:      .word   TPCTL_PARAM_532
SPBA_CTRL_BASE_ADDR_W:  .word   SPBA_CTRL_BASE_ADDR
SPBA_LOCK_VAL:          .word   0xC0010007
ESDCTL_BASE_W:          .word   ESDCTL_BASE
M3IF_BASE_W:            .word   M3IF_BASE
SDRAM_0x82116080:       .word   0x82116080
SDRAM_0x92116080:       .word   0x92116080
SDRAM_0xA2116080:       .word   0xA2116080
SDRAM_0xB2116080:       .word   0xB2116080
SDRAM_0x00395728:       .word   0x00395728
WEIM_BASE_ADDR_W:       .word   WEIM_BASE_ADDR
CS0_CSCRU_0x20428F00:   .word   0x20428F00
CS0_CSCRL_0x60000D01:   .word   0x60000D01
CS0_CSCRU_0x23D29000:   .word   0x23D29000
DS_0x12449D24:          .word   0x12449D24
IOMUXC_BASE_ADDR_W:     .word   IOMUXC_BASE_ADDR
CRM_MCU_BASE_ADDR_W:    .word   CRM_MCU_BASE_ADDR
MXC_REDBOOT_ROM_START:  .word   SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000
CONST_0x0FFF:           .word   0x0FFF
AVIC_VECTOR0_ADDR_W:    .word   MXCBOOT_FLAG_REG
AVIC_VECTOR1_ADDR_W:    .word   MXCFIS_FLAG_REG

/*---------------------------------------------------------------------------*/
/* end of hal_platform_setup.h                                               */
#endif /* CYGONCE_HAL_PLATFORM_SETUP_H */
