//==========================================================================
//
//      cmds.c
//
//      SoC [platform] specific RedBoot commands
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
#include <redboot.h>
#include <cyg/hal/hal_intr.h>
#include <cyg/hal/plf_mmap.h>
#include <cyg/hal/hal_soc.h>         // Hardware definitions
#include <cyg/hal/hal_cache.h>

typedef unsigned long long  u64;
typedef unsigned int        u32;
typedef unsigned short      u16;
typedef unsigned char       u8;

#define SZ_DEC_1M       1000000
#define PLL_PD_MAX      16      //actual pd+1
#define PLL_MFI_MAX     15
#define PLL_MFI_MIN     5
#define PLL_MFD_MAX     1024    //actual mfd+1
#define PLL_MFN_MAX     511
#define IPG_DIV_MAX     4
#define AHB_DIV_MAX     8
#define HSP_PODF_MAX    8
#define NFC_PODF_MAX    8

#if (PLL_REF_CLK == FREQ_32768HZ) || (PLL_REF_CLK == FREQ_32000HZ)
#define PLL_MFD_FIXED   1024
#endif
#if (PLL_REF_CLK == FREQ_26MHZ)
#define PLL_MFD_FIXED   (26 * 16)       // =416
#endif

#define PLL_FREQ_MAX    (2 * PLL_REF_CLK * PLL_MFI_MAX)
#define TPLL_FREQ_MAX   534000000
#define PLL_FREQ_MIN    ((2 * PLL_REF_CLK * (PLL_MFI_MIN - 1)) / PLL_PD_MAX)
#define AHB_CLK_MAX     133333333
#define IPG_CLK_MAX     (AHB_CLK_MAX / 2)
#define NFC_CLK_MAX     25000000
// IPU-HSP clock is independent of the HCLK and can go up to 177MHz but requires
// higher voltage support. For simplicity, limit it to 133MHz
#define HSP_CLK_MAX     133333333

#define ERR_WRONG_CLK   -1
#define ERR_NO_MFI      -2
#define ERR_NO_MFN      -3
#define ERR_NO_PD       -4
#define ERR_NO_AHB_DIV  -6

u32 pll_clock(enum plls pll);
u32 get_main_clock(enum main_clocks clk);
u32 get_peri_clock(enum peri_clocks clk);
int poll_fuse_set(void);
int gcd(int m, int n);

static void clock_setup(int argc, char *argv[]);
static void ckol(int argc, char *argv[]);
static void ckoh(int argc, char *argv[]);

RedBoot_cmd("clock",
            "Setup/Display clock (max AHB=133MHz, max IPG=66.5MHz)\nSyntax:",
            "[<core clock in MHz> [:<AHB-to-core divider>[:<IPG-to-AHB divider>]]] \n\n\
If a divider is zero or no divider is specified, the optimal divider values \n\
will be chosen. It does NOT do integer freq scaling so no brmm value changes.\n\
Instead, it always adjusts the PLL settings. \n\
Examples:\n\
   [clock]         -> Show various clocks\n\
   [clock 399]     -> Core=399  AHB=133           IPG=66.5\n\
   [clock 200]     -> Core=200  AHB=100           IPG=50\n\
   [clock 399:6]   -> Core=399  AHB=66.5(Core/6)  IPG=66.5\n\
   [clock 399:6:2] -> Core=399  AHB=66.5(Core/6)  IPG=33.25(AHB/2)\n",
            clock_setup
           );

/*!
 * This is to calculate various parameters based on reference clock and 
 * targeted clock based on the equation:
 *      t_clk = 2*ref_freq*(mfi + mfn/(mfd+1))/(pd+1)
 * This calculation is based on a fixed MFD value for simplicity.
 *
 * @param ref       reference clock freq
 * @param target    targeted clock in HZ
 * @param p_pd      calculated pd value (pd value from register + 1) upon return
 * @param p_mfi     calculated actual mfi value upon return
 * @param p_mfn     calculated actual mfn value upon return
 * @param p_mfd     fixed mfd value (mfd value from register + 1) upon return
 *
 * @return          0 if successful; non-zero otherwise.
 */
int calc_pll_params(u32 ref, u32 target, u32 *p_pd, 
                    u32 *p_mfi, u32 *p_mfn, u32 *p_mfd)
{
    u64 pd, mfi, mfn, n_target = (u64)target, n_ref = (u64)ref;

    // Make sure targeted freq is in the valid range. Otherwise the 
    // following calculation might be wrong!!!
    if (target < PLL_FREQ_MIN || target > PLL_FREQ_MAX) {
        return ERR_WRONG_CLK;
    }
    // Use n_target and n_ref to avoid overflow
    for (pd = 1; pd <= PLL_PD_MAX; pd++) {
        mfi = (n_target * pd) / (2 * n_ref);
        if (mfi > PLL_MFI_MAX) {
            return ERR_NO_MFI;
        } else if (mfi < 5) {
            continue;
        }
        break;
    }
    // Now got pd and mfi already
    mfn = (((n_target * pd) / 2 - n_ref * mfi) * PLL_MFD_FIXED) / n_ref;
    // Check mfn within limit and mfn < denominator
    if (mfn > PLL_MFN_MAX || mfn >= PLL_MFD_FIXED) {
        return ERR_NO_MFN;
    }

    if (pd > PLL_PD_MAX) {
        return ERR_NO_PD;
    }
    *p_pd = (u32)pd;
    *p_mfi = (u32)mfi;
    *p_mfn = (u32)mfn;
    *p_mfd = PLL_MFD_FIXED;
    return 0;
}

/*!
 * This function assumes the expected core clock has to be changed by
 * modifying the PLL. This is NOT true always but for most of the times,
 * it is. So it assumes the PLL output freq is the same as the expected 
 * core clock (presc=1) unless the core clock is less than PLL_FREQ_MIN.
 * In the latter case, it will try to increase the presc value until 
 * (presc*core_clk) is greater than PLL_FREQ_MIN. It then makes call to
 * calc_pll_params() and obtains the values of PD, MFI,MFN, MFD based
 * on the targeted PLL and reference input clock to the PLL. Lastly, 
 * it sets the register based on these values along with the dividers.
 * Note 1) There is no value checking for the passed-in divider values
 *         so the caller has to make sure those values are sensible.
 *      2) Also adjust the NFC divider such that the NFC clock doesn't
 *         exceed NFC_CLK_MAX.
 *      3) IPU HSP clock is independent of AHB clock. Even it can go up to
 *         177MHz for higher voltage, this function fixes the max to 133MHz.
 *      4) This function should not have allowed diag_printf() calls since
 *         the serial driver has been stoped. But leave then here to allow
 *         easy debugging by NOT calling the cyg_hal_plf_serial_stop().
 * 
 * @param ref       pll input reference clock (32KHz or 26MHz)
 * @param core_clk  core clock in Hz
 * @param ahb_div   ahb divider to divide the core clock to get ahb clock 
 *                  (ahb_div - 1) needs to be set in the register
 * @param ipg_div   ipg divider to divide the ahb clock to get ipg clock
 *                  (ipg_div - 1) needs to be set in the register
 # @return          0 if successful; non-zero otherwise
 */
int configure_clock(u32 ref, u32 core_clk, u32 ahb_div, u32 ipg_div)
{
    u32 pll, pd, mfi, mfn, mfd, brmo = 0, pctl0;
    u32 pdr0, nfc_div, ahb_clk = core_clk / ahb_div;
    int ret, i;

    // assume pll default to core clock first
    pll = core_clk;
    // when core_clk >= PLL_FREQ_MIN, the presc can be 1.
    // Otherwise, need to calculate presc value below and adjust the targeted pll
    if (core_clk < PLL_FREQ_MIN) {
        diag_printf("can't make core_clk=%d\n", core_clk);
        return ERR_WRONG_CLK;
    }
    
    // get nfc_div - make sure optimal NFC clock but less than NFC_CLK_MAX
    for (nfc_div = 1; nfc_div <= NFC_PODF_MAX; nfc_div++) {
        if ((pll / (ahb_div * nfc_div)) <= NFC_CLK_MAX) {
            break;
        }
    }

    // pll is now the targeted pll output. Use it along with ref input clock
    // to get pd, mfi, mfn, mfd
    if ((ret = calc_pll_params(ref, pll, &pd, &mfi, &mfn, &mfd)) != 0) {
        diag_printf("can't find pll parameters: %d\n", ret);
        return ret;
    }
#ifdef CMD_CLOCK_DEBUG
    diag_printf("ref=%d, pll=%d, pd=%d, mfi=%d,mfn=%d, mfd=%d\n", 
                ref, pll, pd, mfi, mfn, mfd);
#endif

    // blindly increase divider first to avoid too fast ahbclk and ipgclk
    // in case the core clock increases too much
    pdr0 = readl(CRM_MCU_BASE_ADDR + CLKCTL_PDR0);
    pdr0 &= ~0x000007F8;
    // increase the dividers. should work even when core clock is 832 (26*2*16)MHz
    // which is unlikely true.
    pdr0 |= (1 << 6) | (6 << 3);
    writel(pdr0, CRM_MCU_BASE_ADDR + CLKCTL_PDR0);
    // calculate new pdr0. Also clear the brmm bits
    pdr0 &= ~0x000007FF;
    pdr0 |= ((nfc_div - 1) << 8) | ((ipg_div - 1) << 6) | ((ahb_div - 1) << 3);

    // update PLL register
    if ((mfd >= (10 * mfn)) || ((10 * mfn) >= (9 * mfd)))
        brmo = 1;

    pctl0 = readl(CRM_MCU_BASE_ADDR + CLKCTL_MPCTL);
    pctl0 = (pctl0 & 0x40008000)  |
            (brmo << 31)           |
            ((pd - 1) << 26)       |
            ((mfd - 1) << 16)      |
            (mfi << 11)            |
            mfn;
    writel(pctl0, CRM_MCU_BASE_ADDR + CLKCTL_MPCTL);
    writel(pdr0, CRM_MCU_BASE_ADDR + CLKCTL_PDR0);
    // add some delay for new values to take effect
    for (i = 0; i < 10000; i++);

    // --------------- now adjust for TPLL ---------------------------
    pll = (TPLL_FREQ_MAX / ahb_clk) * ahb_clk;
    if ((ret = calc_pll_params(ref, pll, &pd, &mfi, &mfn, &mfd)) != 0) {
        diag_printf("can't find tpll parameters: %d\n", ret);
        return ret;
    }
#ifdef CMD_CLOCK_DEBUG
    diag_printf("ref=%d, pll=%d, pd=%d, mfi=%d,mfn=%d, mfd=%d\n", 
                ref, pll, pd, mfi, mfn, mfd);
#endif

    // update PLL register
    if ((mfd >= (10 * mfn)) || ((10 * mfn) >= (9 * mfd)))
        brmo = 1;

    pctl0 = readl(CRM_MCU_BASE_ADDR + CLKCTL_TPCTL);
    pctl0 = (pctl0 & 0x40008000)  |
            (brmo << 31)           |
            ((pd - 1) << 26)       |
            ((mfd - 1) << 16)      |
            (mfi << 11)            |
            mfn;
    writel(pctl0, CRM_MCU_BASE_ADDR + CLKCTL_TPCTL);

    return 0;
}

static void clock_setup(int argc,char *argv[])
{
    u32 i, core_clk, ipg_div, data[3],
    ahb_div, ahb_clk, ipg_clk;
    int ret;

    if (argc == 1)
        goto print_clock;
    for (i = 0;  i < 3;  i++) {
        unsigned long temp;
        if (!parse_num(*(&argv[1]), &temp, &argv[1], ":")) {
            diag_printf("Error: Invalid parameter\n");
            return;
        }
        data[i] = temp;
    }

    core_clk = data[0] * SZ_DEC_1M;
    ahb_div = data[1];  // actual register field + 1
    ipg_div = data[2];  // actual register field + 1

    if (core_clk < PLL_FREQ_MIN || core_clk > PLL_FREQ_MAX) {
        diag_printf("Targeted core clock should be within [%d - %d]\n", 
                    PLL_FREQ_MIN, PLL_FREQ_MAX);
        return;
    }

    // find the ahb divider  
    if (ahb_div > AHB_DIV_MAX) {
        diag_printf("Invalid AHB divider: %d. Maximum value is %d\n",
                    ahb_div, AHB_DIV_MAX);
        return;
    }
    if (ahb_div == 0) {
        // no HCLK divider specified
        for (ahb_div = 1; ; ahb_div++) {
            if ((core_clk / ahb_div) <= AHB_CLK_MAX) {
                break;
            }
        }
    }
    if (ahb_div > AHB_DIV_MAX || (core_clk / ahb_div) > AHB_CLK_MAX) {
        diag_printf("Can't make AHB=%d since max=%d\n", 
                    core_clk / ahb_div, AHB_CLK_MAX);
        return;
    }

    // find the ipg divider
    ahb_clk = core_clk / ahb_div;
    if (ipg_div > IPG_DIV_MAX) {
        diag_printf("Invalid IPG divider: %d. Maximum value is %d\n", 
                    ipg_div, IPG_DIV_MAX);
        return;
    }
    if (ipg_div == 0) {
        ipg_div++;          // At least =1
        if (ahb_clk > IPG_CLK_MAX)
            ipg_div++;      // Make it =2
    }
    if (ipg_div > IPG_DIV_MAX || (ahb_clk / ipg_div) > IPG_CLK_MAX) {
        diag_printf("Can't make IPG=%d since max=%d\n", 
                    (ahb_clk / ipg_div), IPG_CLK_MAX);
        return;
    }
    ipg_clk = ahb_clk / ipg_div;

    diag_printf("Trying to set core=%d ahb=%d ipg=%d...\n", 
                core_clk, ahb_clk, ipg_clk);

    // stop the serial to be ready to adjust the clock
    hal_delay_us(100000);
    cyg_hal_plf_serial_stop();
    // adjust the clock
    ret = configure_clock(PLL_REF_CLK, core_clk, ahb_div, ipg_div);
    // restart the serial driver
    cyg_hal_plf_serial_init();
    hal_delay_us(100000);

    if (ret != 0) {
        diag_printf("Failed to setup clock: %d\n", ret);
        return;
    }
    diag_printf("\n<<<New clock setting>>>\n");

    // Now printing clocks
print_clock:
#ifdef CYGPKG_HAL_ARM_MXC91321_CHIP
    diag_printf("\nMPLL\t\tUPLL\t\tTPLL\n");
    diag_printf("================================================\n");
    diag_printf("%-16d%-16d%-16d\n\n", 
                pll_clock(MCU_PLL), pll_clock(USB_PLL), pll_clock(TUR_PLL));
#endif
 
    diag_printf("CPU\t\tAHB\t\tIPG\t\tNFC\t\tUSB\n");
    diag_printf("===========================================");
    diag_printf("=============================\n");
    diag_printf("%-16d%-16d%-16d%-16d%-16d\n\n",
                get_main_clock(CPU_CLK),
                get_main_clock(AHB_CLK),
                get_main_clock(IPG_CLK),
                get_main_clock(NFC_CLK),
                get_main_clock(USB_CLK));

    diag_printf("UART1/2/3/4\tSSI1\t\tSSI2\t\tCSI\t\tFIRI\n");
    diag_printf("===========================================");
    diag_printf("=============================\n");

    diag_printf("%-16d%-16d%-16d%-16d%-16d\n\n", 
                get_peri_clock(UART1_BAUD),
                get_peri_clock(SSI1_BAUD),
                get_peri_clock(SSI2_BAUD),
                get_peri_clock(CSI_BAUD),
                get_peri_clock(FIRI_BAUD));
}

/*!
 * This function returns the PLL output value in Hz based on pll.
 */
u32 pll_clock(enum plls pll)
{
    u64 mfi, mfn, mfd, pdf, ref_clk, pll_out, sign;
    u64 reg = readl(pll);

    pdf = (reg >> 26) & 0xF;
    mfd = (reg >> 16) & 0x3FF;
    if (pll == MCU_PLL || pll == TUR_PLL) {
        mfi = (reg >> 11) & 0xF;
        mfi = (mfi <= 5) ? 5: mfi;
        mfn = reg & 0x7FF;
        sign = (mfn < 1024) ? 0: 1;
        mfn = (mfn <= 0x400) ? mfn: (0x800 - mfn);
    } else {
        sign = 0;
        mfi = (reg >> 10) & 0xF;
        mfi = (mfi <= 5) ? 5: mfi;
        mfn = reg & 0x3FF;
    }

    /* Scale down to avoid overflow */
    ref_clk = PLL_REF_CLK;
    if (ref_clk == 0) {
        diag_printf("Error: fix input clock first for %s() to work\n", 
                    __FUNCTION__);
        return 0;
    }

    if (sign == 0) {
        pll_out = (2 * ref_clk * mfi + ((2 * ref_clk * mfn) / (mfd + 1))) /
                  (pdf + 1);
    } else {
        pll_out = (2 * ref_clk * mfi - ((2 * ref_clk * mfn) / (mfd + 1))) /
                  (pdf + 1);
    }

    return (u32)pll_out;
}

#define NORMALIZE_FACTOR    10

void clock_spi_enable(unsigned int spi_clk)
{
    if (spi_clk == SPI1_CLK) {
        // do nothing now as it is already enabled by default
    } else if (spi_clk == SPI2_CLK) {
        // do nothing now as it is already enabled by default
    }
}

/*!
 * This function returns the main clock value in Hz.
 */
u32 get_main_clock(enum main_clocks clk)
{
    u32 brmm, max_pdf, ipg_pdf, nfc_pdf, csi_pdf;
    u32 pll, ret_val = 0, hclk, usb_pdf, div;
    enum plls CORE_PLL_SEL = MCU_PLL;

    volatile u32 reg = readl(CRM_MCU_BASE_ADDR + CLKCTL_PDR0);
    volatile u32 reg1 = readl(CRM_MCU_BASE_ADDR + CLKCTL_PDR1);
    brmm = reg & 0x7;
    max_pdf = (reg >> 3) & 0x7;
    ipg_pdf = (reg >> 6) & 0x3;
    nfc_pdf = (reg >> 8) & 0x7;
    csi_pdf = reg >> 23;
    usb_pdf = (reg1 >> 27) & 0x7;

#ifdef CYGPKG_HAL_ARM_MXC91321_CHIP
    if ((readl(CRM_MCU_BASE_ADDR + CLKCTL_PDR0) & (1 << 11)) != 0) {
        CORE_PLL_SEL = TUR_PLL;
    }
#endif

    switch (clk) {
    case CPU_CLK:
        pll = pll_clock(CORE_PLL_SEL);
        if (brmm >= 5) {
            diag_printf("Wrong BRMM value in the CRM_AP, MPDR0 reg \n");
            return 0;
        }
        hclk = pll / (max_pdf + 1);
        div = (pll * NORMALIZE_FACTOR) / hclk;
        switch (brmm) {
        case 0:
            ret_val = pll;
            break;
        case 1:
            // new period = (2*MCU_period + 1*AHB_period)/3
            // => new freq = (3*pll*hclk)/(2*hclk+pll)
            // => new frq = (3*pll)/(2+pll/hclk). Also normalize it.
            ret_val = (3* pll * NORMALIZE_FACTOR) / 
                ((2 * NORMALIZE_FACTOR) + ((pll * NORMALIZE_FACTOR) / hclk));
            break;
        case 2:
            // new period = (1*MCU_period + 1*AHB_period)/2
            // => new freq = (2*pll*hclk)/(hclk+pll)
            // => new frq = (2*pll)/(1+pll/hclk). Also normalize it.
            ret_val = (2* pll * NORMALIZE_FACTOR) / 
                ((1 * NORMALIZE_FACTOR) + ((pll * NORMALIZE_FACTOR) / hclk));
            break;
        case 3:
            // new period = (1*MCU_period + 2*AHB_period)/3
            // => new freq = (3*pll*hclk)/(hclk+2*pll)
            // => new frq = (3*pll)/(1+(2*pll)/hclk). Also normalize it.
            ret_val = (3* pll * NORMALIZE_FACTOR) / 
                ((1 * NORMALIZE_FACTOR) + ((2 * pll * NORMALIZE_FACTOR) / hclk));
            break;
        case 4:
            ret_val = hclk;
            break;
        default:
            break;
        }
        break;
    case AHB_CLK:
        pll = pll_clock(CORE_PLL_SEL);
        ret_val = pll / (max_pdf + 1);
        break;
    case IPG_CLK:
        pll = pll_clock(CORE_PLL_SEL);
        ret_val = pll / ((max_pdf + 1) * (ipg_pdf + 1));
        break;
    case NFC_CLK:
        pll = pll_clock(CORE_PLL_SEL);
        ret_val = pll / ((max_pdf + 1) * (nfc_pdf + 1));
        break;
    case USB_CLK:
        pll = pll_clock(USB_PLL);
        ret_val = pll / (usb_pdf + 1);
        break;
    default:
        diag_printf("%s(): This clock: %d not supported yet \n",
                    __FUNCTION__, clk);
        break;
    }

    return ret_val;
}

#ifdef CYGPKG_HAL_ARM_MXC91321_CHIP
static u32 csi_sdhc_clock_src(u32 clksrc)
{
    u32 val = 0;

    switch (clksrc) {
    case 0:
        val = pll_clock(USB_PLL);
        break;
    case 1:
        val = pll_clock(MCU_PLL);
        break;
    case 2:
        val = pll_clock(TUR_PLL);
        break;
    case 3:
        val = FREQ_26MHZ;
        break;
    }

    return val;
}
#endif
/*!
 * This function returns the peripheral clock value in Hz.
 */
u32 get_peri_clock(enum peri_clocks clk)
{
    volatile u32 mcr = readl(CRM_MCU_BASE_ADDR + CLKCTL_MCR);
    volatile u32 mpdr0 = readl(CRM_MCU_BASE_ADDR + CLKCTL_PDR0);
    volatile u32 mpdr1 = readl(CRM_MCU_BASE_ADDR + CLKCTL_PDR1);
    u32 clk_sel, pre_pdf, pdf, ref_clk, ret_val = 0; 

    switch (clk) {
    case UART1_BAUD:
    case UART2_BAUD:
    case UART3_BAUD:
    case UART4_BAUD:
        return get_main_clock(IPG_CLK);
        break;
    case SSI1_BAUD:
        pre_pdf = (mpdr1 >> 6) & 0x7;
        pdf = (mpdr1 >> 1) & 0x1F;
        clk_sel = mcr & (1 << 28);
        ref_clk = (clk_sel != 0) ? pll_clock(USB_PLL) : pll_clock(MCU_PLL);
        ret_val = ref_clk / ((pre_pdf + 1) * (pdf + 1));
        break;
    case SSI2_BAUD:
        pre_pdf = (mpdr1 >> 15) & 0x7;
        pdf = (mpdr1 >> 10) & 0x1F;
        clk_sel = mcr & (1 << 27);
        ref_clk = (clk_sel != 0) ? pll_clock(USB_PLL) : pll_clock(MCU_PLL);
        ret_val = ref_clk / ((pre_pdf + 1) * (pdf + 1));
        break;

#ifdef CYGPKG_HAL_ARM_MXC91321_CHIP
        clk_sel = (mcr >> 25) & 0x3;
        pdf = ((mpdr0 >> 23) & 0x1FF) + 1;
        pdf = (2 * pdf) + (mpdr0 & (1 << 22)); //multiplied by 2
        pdf *= (1 + (mpdr0 & (1 << 21)));
        
        ret_val = (2 * csi_sdhc_clock_src(clk_sel)) / pdf;
#endif
        break;
    case FIRI_BAUD:
        pre_pdf = (mpdr1 >> 24) & 0x7;
        pdf = (mpdr1 >> 19) & 0x1F;
        clk_sel = mcr & (1 << 11);
        ref_clk = (clk_sel != 0) ? pll_clock(USB_PLL) : pll_clock(MCU_PLL);
        ret_val = ref_clk / ((pre_pdf + 1) * (pdf + 1));
        break;
    case SPI1_CLK:
    case SPI2_CLK:
        ret_val = get_main_clock(IPG_CLK);
        break;
    default:
        diag_printf("%s(): This clock: %d not supported yet \n",
                    __FUNCTION__, clk);
        break;
    }

    return ret_val;
}

RedBoot_cmd("ckol",
            "Select clock source for CKO1 (AKA CKO) (J10 on the EVB CPU daughter card)",
            " The output is 1/8 of actual clock. Default is MCU_PLL\n\
          <0> - display current cko selection\n\
          <1> - MCU_PLL \n\
          <2> - CKIH \n\
          <3> - USB_PLL \n\
          <4} - DSP_PLL \n\
          <5> - WB_PAT_REF \n\
          <6> - RESERVED \n\
          <7> - RESERVED \n\
          <8> - MB_PAT_REF \n",
            ckol
           );

static u8* cko_name[] = {
    "NULL",
    "MCU_PLL",
    "CKIH",
    "USB_PLL",
    "DSP_PLL",
    "WB_PAT_REF",
    "RESERVED",
    "RESERVED",
    "MB_PAT_REF",
};

#define CKO_MAX_INDEX           (sizeof(cko_name) / sizeof(u8*))
#define CKO_DIV                 3  // default divide by 8
#define CKOH_DIV                3  // default divide by 8

static void ckol(int argc,char *argv[])
{
    u32 action = 0, cosr;

    if (!scan_opts(argc, argv, 1, 0, 0, (void*) &action,
                   OPTION_ARG_TYPE_NUM, "action"))
        return;

    if (action >= CKO_MAX_INDEX) {
        diag_printf("%d is not supported\n\n", action);
        return;
    }

    cosr = readl(CRM_MCU_BASE_ADDR + CLKCTL_COSR);

    if (action != 0) {
        cosr = (cosr & (~0x7F)) + (1 << 6) + (CKO_DIV << 3) + action - 1;
        writel(cosr, CRM_MCU_BASE_ADDR + CLKCTL_COSR);
        diag_printf("Set clko to ");
    }

    cosr = readl(CRM_MCU_BASE_ADDR + CLKCTL_COSR);
    diag_printf("%s\n", cko_name[(cosr & 0x7) + 1]);
    diag_printf("COSR register[0x%x] = 0x%x\n", 
                (CRM_MCU_BASE_ADDR + CLKCTL_COSR), cosr);
}

RedBoot_cmd("ckoh",
            "Select clock source for CKO2 (J9 on the EVB CPU daughter card)",
            " The default is 1/8 of IPG_CLK_ARM (core clock)\n\
          <0> - display current cko selection\n\
          <1> - MCU_PLL \n\
          <2> - REC_64KHZ \n\
          <3> - USB_PLL \n\
          <4} - DSP_PLL \n\
          <5> - WB_PLL \n\
          <6> - RESERVED \n\
          <7> - RESERVED \n\
          <8> - WCSI_RX \n\
          <9> - NFC_CLK \n\
          <10> - MCU_AHB_CLK \n\
          <11> - IPG_CLK_S \n\
          <12> - IPG_CLK \n\
          <13> - DSP_AHB_CLK \n\
          <14> - IPG_CLK_ARM (Core) \n\
          <15> - PAT_REF_CLK_SYNC \n\
          <16> - WB_PAT_REF_CLK_SYNC \n\
          <17> - TURBO_PLL (MXC91321 only)\n\
          <18> - AFC_PLL (MXC91321 only) \n",
            ckoh
           );

static u8* div_str[] = {
    "original ",
    "1/2 of ",
    "1/4 of ",
    "1/8 of ",
    "1/16 of ",
    "unknown of ",
    "unknown of ",
    "unknown of ",
};

static u8* ckoh_name[] ={
    "NULL",
    "MCU_PLL",
    "REC_64KHZ",
    "USB_PLL",
    "DSP_PLL",
    "WB_PLL",
    "RESERVED",
    "RESERVED",
    "WCSI_RX",
    "NFC_CLK",
    "MCU_AHB_CLK",
    "IPG_CLK_S",
    "IPG_CLK",
    "DSP_AHB_CLK",
    "IPG_CLK_ARM (Core)",
    "PAT_REF_CLK_SYNC",
    "WB_PAT_REF_CLK_SYNC",
#ifdef CYGPKG_HAL_ARM_MXC91321_CHIP
    "TURBO_PLL",
    "AFC_PLL",
#endif
};

#define CKOH_MAX_INDEX           (sizeof(ckoh_name) / sizeof(u8*))

static void ckoh(int argc,char *argv[])
{
    u32 action = 0, cosr, div = 0, i, j;

    if (!scan_opts(argc, argv, 1, 0, 0, (void*) &action,
                   OPTION_ARG_TYPE_NUM, "action"))
        return;

    if (action >= CKOH_MAX_INDEX) {
        diag_printf("%d is not supported\n\n", action);
        return;
    }

    cosr = readl(CRM_MCU_BASE_ADDR + CLKCTL_COSR);

    if (action != 0) {
        if (action == 1 || action == 3 || action == 4 || action == 5 || action == 14 || action == 17)
            div = CKOH_DIV;
        cosr = (cosr & (~0x0007FC00)) + (div << 10) + (1 << 13) + 
               ((action - 1) << 14);
        writel(cosr, CRM_MCU_BASE_ADDR + CLKCTL_COSR);
        diag_printf("Set clko to ");
    }

    cosr = readl(CRM_MCU_BASE_ADDR + CLKCTL_COSR);
    i = (cosr >> 10) & 0x7;
    j = (cosr >> 14) & 0x1F;
    diag_printf("%s%s\n", div_str[i], ckoh_name[j + 1]);
    diag_printf("COSR register[0x%x] = 0x%x\n", 
                (CRM_MCU_BASE_ADDR + CLKCTL_COSR), cosr);
}

#ifdef L2CC_ENABLED
/*
 * This command is added for some simple testing only. It turns on/off
 * L2 cache regardless of L1 cache state. The side effect of this is
 * when doing any flash operations such as "fis init", the L2
 * will be turned back on along with L1 caches even though it is off
 * by using this command.
 */
RedBoot_cmd("L2",
            "L2 cache",
            "[ON | OFF]",
            do_L2_caches
           );

void do_L2_caches(int argc, char *argv[])
{
    u32 oldints;
    int L2cache_on=0;

    if (argc == 2) {
        if (strcasecmp(argv[1], "on") == 0) {
            HAL_DISABLE_INTERRUPTS(oldints);
            HAL_ENABLE_L2();
            HAL_RESTORE_INTERRUPTS(oldints);
        } else if (strcasecmp(argv[1], "off") == 0) {
            HAL_DISABLE_INTERRUPTS(oldints);
            HAL_CLEAN_INVALIDATE_L2();
            HAL_DISABLE_L2();
            HAL_RESTORE_INTERRUPTS(oldints);
        } else {
            diag_printf("Invalid L2 cache mode: %s\n", argv[1]);
        }
    } else {
        HAL_L2CACHE_IS_ENABLED(L2cache_on);
        diag_printf("L2 cache: %s\n", L2cache_on?"On":"Off");
    }
}
#endif //L2CC_ENABLED

#define IIM_ERR_SHIFT       8
#define POLL_FUSE_PRGD      (IIM_STAT_PRGD | (IIM_ERR_PRGE << IIM_ERR_SHIFT))
#define POLL_FUSE_SNSD      (IIM_STAT_SNSD | (IIM_ERR_SNSE << IIM_ERR_SHIFT))

static void fuse_op_start(void)
{
    /* Do not generate interrupt */
    writel(0, IIM_BASE_ADDR + IIM_STATM_OFF);
    // clear the status bits and error bits
    writel(0x3, IIM_BASE_ADDR + IIM_STAT_OFF);
    writel(0xFE, IIM_BASE_ADDR + IIM_ERR_OFF);
}

/*
 * The action should be either:
 *          POLL_FUSE_PRGD 
 * or:
 *          POLL_FUSE_SNSD
 */
static int poll_fuse_op_done(int action)
{

    u32 status, error;

    if (action != POLL_FUSE_PRGD && action != POLL_FUSE_SNSD) {
        diag_printf("%s(%d) invalid operation\n", __FUNCTION__, action);
        return -1;
    }

    /* Poll busy bit till it is NOT set */
    while ((readl(IIM_BASE_ADDR + IIM_STAT_OFF) & IIM_STAT_BUSY) != 0 ) {
    }

    /* Test for successful write */
    status = readl(IIM_BASE_ADDR + IIM_STAT_OFF);
    error = readl(IIM_BASE_ADDR + IIM_ERR_OFF);

    if ((status & action) != 0 && (error & (action >> IIM_ERR_SHIFT)) == 0) {
        if (error) {
            diag_printf("Even though the operation seems successful...\n");
            diag_printf("There are some error(s) at addr=0x%x: 0x%x\n",
                        (IIM_BASE_ADDR + IIM_ERR_OFF), error);
        }
        return 0;
    }
    diag_printf("%s(%d) failed\n", __FUNCTION__, action);
    diag_printf("status address=0x%x, value=0x%x\n",
                (IIM_BASE_ADDR + IIM_STAT_OFF), status);
    diag_printf("There are some error(s) at addr=0x%x: 0x%x\n",
                (IIM_BASE_ADDR + IIM_ERR_OFF), error);
    return -1;
}

static void sense_fuse(int bank, int row, int bit)
{
    int addr, addr_l, addr_h, reg_addr;

    fuse_op_start();
    
    addr = ((bank << 11) | (row << 3) | (bit & 0x7));
    /* Set IIM Program Upper Address */
    addr_h = (addr >> 8) & 0x000000FF;
    /* Set IIM Program Lower Address */
    addr_l = (addr & 0x000000FF);

#ifdef IIM_FUSE_DEBUG
    diag_printf("%s: addr_h=0x%x, addr_l=0x%x\n",
                __FUNCTION__, addr_h, addr_l);
#endif
    writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
    writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
    /* Start sensing */
    writel(0x8, IIM_BASE_ADDR + IIM_FCTL_OFF);
    if (poll_fuse_op_done(POLL_FUSE_SNSD) != 0) {
        diag_printf("%s(bank: %d, row: %d, bit: %d failed\n",
                    __FUNCTION__, bank, row, bit);
    }
    reg_addr = IIM_BASE_ADDR + IIM_SDAT_OFF;
    diag_printf("fuses at (bank:%d, row:%d) = 0x%x\n", bank, row, readl(reg_addr));
}

void do_fuse_read(int argc, char *argv[])
{
    int bank, row;

    if (argc == 1) {
        diag_printf("Useage: fuse_read <bank> <row>\n");
        return;
    } else if (argc == 3) {
        if (!parse_num(*(&argv[1]), (unsigned long *)&bank, &argv[1], " ")) {
                diag_printf("Error: Invalid parameter\n");
            return;
        }
        if (!parse_num(*(&argv[2]), (unsigned long *)&row, &argv[2], " ")) {
                diag_printf("Error: Invalid parameter\n");
                return;
            }

        diag_printf("Read fuse at bank:%d row:%d\n", bank, row);
        sense_fuse(bank, row, 0);

    } else {
        diag_printf("Passing in wrong arguments: %d\n", argc);
        diag_printf("Useage: fuse_read <bank> <row>\n");
    }
}

/* Blow fuses based on the bank, row and bit positions (all 0-based)
*/
static int fuse_blow(int bank,int row,int bit)
{
    int addr, addr_l, addr_h, ret = -1;

    fuse_op_start();

    /* Disable IIM Program Protect */
    writel(0xAA, IIM_BASE_ADDR + IIM_PREG_P_OFF);

    addr = ((bank << 11) | (row << 3) | (bit & 0x7));
    /* Set IIM Program Upper Address */
    addr_h = (addr >> 8) & 0x000000FF;
    /* Set IIM Program Lower Address */
    addr_l = (addr & 0x000000FF);

#ifdef IIM_FUSE_DEBUG
    diag_printf("blowing addr_h=0x%x, addr_l=0x%x\n", addr_h, addr_l);
#endif

    writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
    writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
    /* Start Programming */
    writel(0x31, IIM_BASE_ADDR + IIM_FCTL_OFF);
    if (poll_fuse_op_done(POLL_FUSE_PRGD) == 0) {
        ret = 0;
    }

    /* Enable IIM Program Protect */
    writel(0x0, IIM_BASE_ADDR + IIM_PREG_P_OFF);
    return ret;
}

/*
 * This command is added for burning IIM fuses
 */
RedBoot_cmd("fuse_read",
            "read some fuses",
            "<bank> <row>",
            do_fuse_read
           );

RedBoot_cmd("fuse_blow",
            "blow some fuses",
            "<bank> <row> <value>",
            do_fuse_blow
           );

#define         INIT_STRING              "12345678"
static char ready_to_blow[] = INIT_STRING;

void quick_itoa(u32 num, char *a) 
{
    int i, j, k;        
    for (i = 0; i <= 7; i++) {
        j = (num >> (4 * i)) & 0xF;
        k = (j < 10) ? '0' : ('a' - 0xa);
        a[i] = j + k;
    }
}

void do_fuse_blow(int argc, char *argv[])
{
    int bank, row, value, i;

    if (argc == 1) {
        diag_printf("It is too dangeous for you to use this command.\n");
        return;
    } else if (argc == 2) {
        if (strcasecmp(argv[1], "nandboot") == 0) {
            quick_itoa(readl(EPIT_BASE_ADDR + EPITCNR), ready_to_blow);
            diag_printf("%s\n", ready_to_blow);
        }
        return;
    } else if (argc == 3) {
        if (strcasecmp(argv[1], "nandboot") == 0 && 
            strcasecmp(argv[2], ready_to_blow) == 0) {
#if defined(CYGPKG_HAL_ARM_MX21) || defined(CYGPKG_HAL_ARM_MX27) || defined(CYGPKG_HAL_ARM_MX31)
            diag_printf("No need to blow any fuses for NAND boot on this platform\n\n");
#else
            diag_printf("Ready to burn NAND boot fuses\n");
            if (fuse_blow(0, 16, 1) != 0 || fuse_blow(0, 16, 7) != 0) {
                diag_printf("NAND BOOT fuse blown failed miserably ...\n");
            } else {
                diag_printf("NAND BOOT fuse blown successfully ...\n");
            }
        } else {
            diag_printf("Not ready: %s, %s\n", argv[1], argv[2]);
#endif
        }
    } else if (argc == 4) {
        if (!parse_num(*(&argv[1]), (unsigned long *)&bank, &argv[1], " ")) {
                diag_printf("Error: Invalid parameter\n");
            return;
        }
        if (!parse_num(*(&argv[2]), (unsigned long *)&row, &argv[2], " ")) {
                diag_printf("Error: Invalid parameter\n");
                return;
        }
        if (!parse_num(*(&argv[3]), (unsigned long *)&value, &argv[3], " ")) {
                diag_printf("Error: Invalid parameter\n");
                return;
        }

        diag_printf("Blowing fuse at bank:%d row:%d value:%d\n",
                    bank, row, value);
        for (i = 0; i < 8; i++) {
            if (((value >> i) & 0x1) == 0) {
                continue;
            }
            if (fuse_blow(bank, row, i) != 0) {
                diag_printf("fuse_blow(bank: %d, row: %d, bit: %d failed\n",
                            bank, row, i);
            } else {
                diag_printf("fuse_blow(bank: %d, row: %d, bit: %d successful\n",
                            bank, row, i);
            }
        }
        sense_fuse(bank, row, 0);

    } else {
        diag_printf("Passing in wrong arguments: %d\n", argc);
    }
    /* Reset to default string */
    strcpy(ready_to_blow, INIT_STRING);;
}

/* precondition: m>0 and n>0.  Let g=gcd(m,n). */
int gcd(int m, int n)
{
    int t;
    while(m > 0) {
        if(n > m) {t = m; m = n; n = t;} /* swap */
        m -= n;
    }
    return n;
 }
