#ifndef CYGONCE_HAL_PLATFORM_SETUP_H
#define CYGONCE_HAL_PLATFORM_SETUP_H

//=============================================================================
//
//      hal_platform_setup.h
//
//      Platform specific support for HAL (assembly code)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <pkgconf/system.h>             // System-wide configuration info
#include CYGBLD_HAL_VARIANT_H           // Variant specific configuration
#include CYGBLD_HAL_PLATFORM_H          // Platform specific configuration
#include <cyg/hal/hal_soc.h>            // Variant specific hardware definitions
#include <cyg/hal/hal_mmu.h>            // MMU definitions
#include <cyg/hal/fsl_board.h>          // Platform specific hardware definitions

#if defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1 _platform_setup1
#define CYGHWR_HAL_ARM_HAS_MMU

#ifdef CYG_HAL_STARTUP_ROMRAM
#define CYGSEM_HAL_ROM_RESET_USES_JUMP
#endif


#define CYGHWR_HAL_ROM_VADDR    0x0

#if 0
#define UNALIGNED_ACCESS_ENABLE
#define SET_T_BIT_DISABLE
#define BRANCH_PREDICTION_ENABLE
#endif

//#define TURN_OFF_IMPRECISE_ABORT

// This macro represents the initial startup code for the platform
// r11 is reserved to contain chip rev info in this file
    .macro  _platform_setup1
FSL_BOARD_SETUP_START:
/*
 *       ARM1136 init
 *       - invalidate I/D cache/TLB and drain write buffer;
 *       - invalidate L2 cache
 *       - unaligned access
 *       - branch predictions
 */
#ifdef TURN_OFF_IMPRECISE_ABORT
    mrs r0, cpsr
    bic r0, r0, #0x100
    msr cpsr, r0
#endif

    mov r0, #0
    mcr 15, 0, r0, c7, c7, 0        /* invalidate I cache and D cache */
    mcr 15, 0, r0, c8, c7, 0        /* invalidate TLBs */
    mcr 15, 0, r0, c7, c10, 4       /* Drain the write buffer */

    mov r0, #SDRAM_NON_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1] // for checking boot source from nand, nor or sdram

init_spba_start:
    init_spba
init_aips_start:
    init_aips
init_max_start:
    init_max
init_m3if_start:
    init_m3if

    mov r11, #CHIP_REV_1_0
    ldr r0, IIM_SREV_REG_VAL
    ldr r1, [r0, #0x0]
    cmp r1, #0x0
    movne r11, #CHIP_REV_2_0
init_cs0_async_start:
    init_cs0_async

    /* If SDRAM has been setup, bypass clock/WEIM setup */
    cmp pc, #SDRAM_BASE_ADDR
    blo init_clock_start
    cmp pc, #(SDRAM_BASE_ADDR + SDRAM_SIZE)
    blo HWInitialise_skip_SDRAM_setup

    mov r0, #NOR_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]

init_clock_start:
    init_clock

init_sdram_start:

    /* Assuming DDR memory first */
    //init_drive_strength_ddr
    init_ddr_sdram
    /* Testing if it is truly DDR */
    ldr r1, SDRAM_COMPARE_CONST1
    mov r0, #SDRAM_BASE_ADDR
    str r1, [r0]
    ldr r2, SDRAM_COMPARE_CONST2
    str r2, [r0, #0x4]
    ldr r2, [r0]
    cmp r1, r2
    beq HWInitialise_skip_SDRAM_setup

    /* Reach here ONLY when SDR */
    init_drive_strength_sdr
    init_sdr_sdram
    /* Test to make sure SDR */
    ldr r1, SDRAM_COMPARE_CONST1
    mov r0, #SDRAM_BASE_ADDR
    str r1, [r0]
    ldr r2, SDRAM_COMPARE_CONST2
    str r2, [r0, #0x4]
    ldr r2, [r0]
    cmp r1, r2
    beq HWInitialise_skip_SDRAM_setup

    /* Reach hear means memory setup problem. Try to
     * increase the HCLK divider */
    ldr r0, CRM_AP_BASE_ADDR_W
    ldr r1, [r0, #CRM_AP_ACDR]
    and r2, r1, #0x70
    cmp r2, #0x70
    beq loop_forever
    add r1, r1, #0x12
    str r1, [r0, #CRM_AP_ACDR]
    b init_sdram_start

loop_forever:
    b loop_forever  /* shouldn't get here */

HWInitialise_skip_SDRAM_setup:

    mov r0, #NFC_BASE
    add r2, r0, #0x800      // 2K window
    cmp pc, r0
    blo Normal_Boot_Continue
    cmp pc, r2
    bhi Normal_Boot_Continue
NAND_Boot_Start:
    /* Copy image from flash to SDRAM first */
    ldr r1, MXC_REDBOOT_ROM_START

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    blo 1b
    /* Jump to SDRAM */
    ldr r1, CONST_0x0FFF
    and r0, pc, r1     /* offset of pc */
    ldr r1, MXC_REDBOOT_ROM_START
    add r1, r1, #0x10
    add pc, r0, r1
    nop
    nop
    nop
    nop
NAND_Copy_Main:
    mov r0, #NAND_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]
    mov r0, #MXCFIS_NAND
    ldr r1, AVIC_VECTOR1_ADDR_W
    str r0, [r1]

    mov r0, #NFC_BASE;   //r0: nfc base. Reloaded after each page copying
    mov r1, #0x800       //r1: starting flash addr to be copied. Updated constantly
    add r2, r0, #0x200   //r2: end of 1st RAM buf. Doesn't change
    add r12, r0, #0xE00  //r12: NFC register base. Doesn't change
    ldr r14, MXC_REDBOOT_ROM_START
    add r13, r14, #REDBOOT_IMAGE_SIZE //r13: end of SDRAM address for copying. Doesn't change
    add r14, r14, r1     //r14: starting SDRAM address for copying. Updated constantly

    //unlock internal buffer
    mov r3, #0x2
    strh r3, [r12, #0xA]

Nfc_Read_Page:
//  writew(FLASH_Read_Mode1, NAND_FLASH_CMD_REG);
    mov r3, #0x0;
    strh r3, [r12, #NAND_FLASH_CMD_REG_OFF]
    mov r3, #NAND_FLASH_CONFIG2_FCMD_EN;
    strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
    do_wait_op_done

//    start_nfc_addr_ops(ADDRESS_INPUT_READ_PAGE, addr, nflash_dev_info->base_mask);
    mov r3, r1
    do_addr_input       //1st addr cycle
    mov r3, r1, lsr #9
    do_addr_input       //2nd addr cycle
    mov r3, r1, lsr #17
    do_addr_input       //3rd addr cycle

//    NFC_DATA_OUTPUT(buf, FDO_PAGE_SPARE_VAL);
//        writew(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN,
//               NAND_FLASH_CONFIG1_REG);
    mov r3, #(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN)
    strh r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]

//        writew(buf_no, RAM_BUFFER_ADDRESS_REG);
    mov r3, #0
    strh r3, [r12, #RAM_BUFFER_ADDRESS_REG_OFF]
//        writew(FDO_PAGE_SPARE_VAL & 0xFF, NAND_FLASH_CONFIG2_REG);
    mov r3, #FDO_PAGE_SPARE_VAL
    strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
//        wait_op_done();
    do_wait_op_done

    // check for bad block
    mov r3, r1, lsl #(32-5-9)
    cmp r3, #(512 << (32-5-9))
    bhi Copy_Good_Blk
    add r4, r0, #0x800  //r3 -> spare area buf 0
    ldrh r4, [r4, #0x4]
    and r4, r4, #0xFF00
    cmp r4, #0xFF00
    beq Copy_Good_Blk
    // really sucks. Bad block!!!!
    cmp r3, #0x0
    beq Skip_bad_block
    // even suckier since we already read the first page!
    sub r14, r14, #512  //rewind 1 page for the sdram pointer
    sub r1, r1, #512    //rewind 1 page for the flash pointer
Skip_bad_block:
    add r1, r1, #(32*512)
    b Nfc_Read_Page
Copy_Good_Blk:
    //copying page
1:  ldmia r0!, {r3-r10}
    stmia r14!, {r3-r10}
    cmp r0, r2
    blo 1b
    cmp r14, r13
    bge NAND_Copy_Main_done
    add r1, r1, #0x200
    mov r0, #NFC_BASE
    b Nfc_Read_Page

NAND_Copy_Main_done:

Normal_Boot_Continue:

init_cs4_start:
    init_cs4

#ifdef CYG_HAL_STARTUP_ROMRAM     /* enable running from RAM */
    /* Copy image from flash to SDRAM first */
    ldr r0, =0xFFFFF000
    and r0, r0, pc
    ldr r1, MXC_REDBOOT_ROM_START
    cmp r0, r1
    beq HWInitialise_skip_SDRAM_copy

    add r2, r0, #REDBOOT_IMAGE_SIZE

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    ble 1b
    /* Jump to SDRAM */
    ldr r1, =0xFFFF
    and r0, pc, r1         /* offset of pc */
    ldr r1, =(SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000 + 0x8)
    add pc, r0, r1
    nop
    nop
    nop
    nop
#endif /* CYG_HAL_STARTUP_ROMRAM */
init_cs0_sync_start:
    init_cs0_sync

HWInitialise_skip_SDRAM_copy:

/*
 * Note:
 *     IOMUX/PBC setup is done in C function plf_hardware_init() for simplicity
 */

STACK_Setup:
    // Set up a stack [for calling C code]
    ldr r1, =__startup_stack
    ldr r2, =RAM_BANK0_BASE
    orr sp, r1, r2

    // Create MMU tables
    bl hal_mmu_init

    // Enable MMU
    ldr r2, =10f
    mrc MMU_CP, 0, r1, MMU_Control, c0      // get c1 value to r1 first
    orr r1, r1, #7                          // enable MMU bit
    mcr MMU_CP, 0, r1, MMU_Control, c0
    mov pc,r2    /* Change address spaces */
    nop
    nop
    nop
10:

    // Save shadow copy of BCR, also hardware configuration
    ldr r1, =_board_BCR
    str r2, [r1]
    ldr r1, =_board_CFG
    str r9, [r1]                // Saved far above...

    .endm                       // _platform_setup1

#else // defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1
#endif

    /* Allow all 3 masters to have access to these shared peripherals */
    .macro  init_spba
        ldr r0, SPBA_CTRL_BASE_ADDR_W
        mov r1, #0x7            /* allow all 3 masters access */
        str r1, [r0, #0x04]
        str r1, [r0, #0x0C]
        str r1, [r0, #0x10]
        str r1, [r0, #0x14]
        str r1, [r0, #0x18]
        str r1, [r0, #0x1C]
        str r1, [r0, #0x24]
        str r1, [r0, #0x28]
        str r1, [r0, #0x40]
        str r1, [r0, #0x44]
        str r1, [r0, #0x48]
        str r1, [r0, #0x4C]
        str r1, [r0, #0x50]
        str r1, [r0, #0x54]
        str r1, [r0, #0x58]
        str r1, [r0, #0x5C]
    .endm  /* init_spba */

    /* AIPS setup - Only setup MPROTx registers. The PACR default values are good.*/
    .macro init_aips
        /*
         * Set all MPROTx to be non-bufferable, trusted for R/W,
         * not forced to user-mode.
         */
        ldr r0, AIPS1_CTRL_BASE_ADDR_W
        ldr r1, AIPS1_PARAM_W
        str r1, [r0, #0x00]
        str r1, [r0, #0x04]
        ldr r0, AIPS2_CTRL_BASE_ADDR_W
        str r1, [r0, #0x00]
        str r1, [r0, #0x04]

        /*
         * Clear the on and off peripheral modules Supervisor Protect bit
         * for SDMA to access them. Did not change the AIPS control registers
         * (offset 0x20) access type
         */
        ldr r0, AIPS1_CTRL_BASE_ADDR_W
        ldr r1, =0x0
        str r1, [r0, #0x40]
        str r1, [r0, #0x44]
        str r1, [r0, #0x48]
        str r1, [r0, #0x4C]
        ldr r1, [r0, #0x50]
        and r1, r1, #0x00FFFFFF
        str r1, [r0, #0x50]

        ldr r0, AIPS2_CTRL_BASE_ADDR_W
        ldr r1, =0x0
        str r1, [r0, #0x40]
        str r1, [r0, #0x44]
        str r1, [r0, #0x48]
        str r1, [r0, #0x4C]
        ldr r1, [r0, #0x50]
        and r1, r1, #0x00FFFFFF
        str r1, [r0, #0x50]
    .endm /* init_aips */

    /* MAX (Multi-Layer AHB Crossbar Switch) setup */
    .macro init_max
        ldr r0, MAX_BASE_ADDR_W
        /* MPR - priority is M3 > M0 > M1 > M2 */
        ldr r1, MAX_PARAM1
        str r1, [r0, #0x000]        /* for S0 */
        str r1, [r0, #0x100]        /* for S1 */
        str r1, [r0, #0x200]        /* for S2 */
        str r1, [r0, #0x300]        /* for S3 */
        str r1, [r0, #0x400]        /* for S4 */
        /* SGPCR - always park on last master */
        ldr r1, =0x10
        str r1, [r0, #0x010]        /* for S0 */
        str r1, [r0, #0x110]        /* for S1 */
        str r1, [r0, #0x210]        /* for S2 */
        str r1, [r0, #0x310]        /* for S3 */
        str r1, [r0, #0x410]        /* for S4 */
        /* MGPCR - restore default values */
        ldr r1, =0x0
        str r1, [r0, #0x800]        /* for M0 */
        str r1, [r0, #0x900]        /* for M1 */
        str r1, [r0, #0xA00]        /* for M2 */
        str r1, [r0, #0xB00]        /* for M3 */
    .endm /* init_max */

    /* Clock setup */
    .macro init_clock
        /*
         * Clock setup
         * After this step, AP domain is running out of PLL0 with:

           Module           Freq (MHz)   Note
           =========================================================================
           ARM core         266          ap_clk
           AHB              133          known as "hclk" for ap_hclk and xxx_ahb_clk's
           IP               66.5         ap_pclk and ap_com_pclk
           EMI              133          =hclk

         * All other clocks can be figured out based on this.
         */
        /*
        * Step 1: Make CKOH controlled by CKOH.
        * Switch to patref
        */
        ldr r0, IOMUX_COM_BASE_ADDR_W
        add r0, r0, #0x14
        ldr r1, =0x0
        strh r1, [r0, #0x2]
        ldr r0, CRM_AP_BASE_ADDR_W
        str r1, [r0, #CRM_AP_ACSR]
        /*
        * Step 2: Setup PLL0 - ADPLL for AP domain.
        */
        ldr r0, PLL0_BASE_ADDR_W
        ldr r1, =0x2
        str r1, [r0, #PLL_DP_CONFIG]  /* Enable auto-restart AREN bit */
        ldr r1, =0x12
        str r1, [r0, #PLL_DP_CTL]     /* Set DPLL ON; BRMO=1 */
        ldr r1, =0x0
        str r1, [r0, #PLL_DP_CTL]

        /*
        * Set PLL0 to be 399MHz.
        * MFI=5, PDF=0, MFD=25, MFN=3 ->
        * PLL0=2*26MHzInput*(5+3/(25+1))/(0+1)=266 MHz
        */
        ldr r1, =0x50
        str r1, [r0, #PLL_DP_OP]
        ldr r1, =0x19
        str r1, [r0, #PLL_DP_MFD]
        ldr r1, =0x3
        str r1, [r0, #PLL_DP_MFN]

        ldr r1, =0x50
        str r1, [r0, #PLL_DP_HFS_OP]
        ldr r1, =0x19
        str r1, [r0, #PLL_DP_HFS_MFD]
        ldr r1, =0x3
        str r1, [r0, #PLL_DP_HFS_MFN]

        /* Now restart DPLL */
        ldr r1, =0x22
        str r1, [r0, #PLL_DP_CTL]
wait_pll0_lock:
        ldr r1, [r0, #PLL_DP_CTL]
        ands r1, r1, #0x1
        beq wait_pll0_lock

        /* End of PLL0 setup with PLL0 being locked at 399MHz */

        /*
        * Set PLL2 to 240MHz
        */
        ldr r0, PLL2_BASE_ADDR_W

        ldr r1, =0x22
        str r1, [r0, #PLL_DP_CTL]     /* Set DPLL ON (set UPEN bit); BRMO=1 */
        ldr r1, =0x2
        str r1, [r0, #PLL_DP_CONFIG]  /* Enable auto-restart AREN bit */

        /*
        * MFI=9, PDF=1, MFD=12, MFN=3 ->
        * PLL2 = 2*26*(9+3/(12+1))/(1+1)=240 MHz
        */
        ldr r1, =0x91
        str r1, [r0, #PLL_DP_OP]
        ldr r1, =0xC
        str r1, [r0, #PLL_DP_MFD]
        ldr r1, =0x3
        str r1, [r0, #PLL_DP_MFN]

        ldr r1, =0x91
        str r1, [r0, #PLL_DP_HFS_OP]
        ldr r1, =0xC
        str r1, [r0, #PLL_DP_HFS_MFD]
        ldr r1, =0x3
        str r1, [r0, #PLL_DP_HFS_MFN]

        /* Now restart DPLL */
        ldr r1, =0x32
        str r1, [r0, #PLL_DP_CTL]

wait_pll2_lock:
        ldr r1, [r0, #PLL_DP_CTL]
        ands r1, r1, #0x1
        beq wait_pll2_lock
        /* End of PLL2 setup with PLL2 being locked at 48MHz */

        /*
        * Step 3: switching to DPLL for AP domain and restore default register values.
        */
        // AP CKO/CKOH selected
        ldr r0, SRC_BASE_ADDR_W
        ldr r1, [r0, #SRC_SSCR]
        orr r1, r1, #0x60000
        str r1, [r0, #SRC_SSCR]
        ldr r0, CRM_AP_BASE_ADDR_W
        /* Default CKOH as AP_CLK with div by 10 */
        //orr r1, r1, #0x5600 /* HCLK */
        ldr r1, [r0, #CRM_AP_ACR]
        bic r1, r1, #0xFF00
        bic r1, r1, #0x00FF
        orr r1, r1, #0x02C0
        str r1, [r0, #CRM_AP_ACR]

        /*Dividers setup */
        mov r1, #0x030
        str r1, [r0, #CRM_AP_ACDR]      /* ARM core=266MHz, AHB=66.5MHz, IP=66.5MHz */
        mov r1, #0xB
        str r1, [r0, #CRM_AP_ASCSR]     /* Select USB PLL for CS CLK */
        ldr r1, =0x1
        str r1, [r0, #CRM_AP_ACSR]      /* select DPLL for AP domain at new freq */
        ldr r1, CRM_AP_ACDER_W
        str r1, [r0, #CRM_AP_ACDER]     /* set nfc_div=5 (5+1 actual divider)  */
        mov r1, #0x6
        str r1, [r0, #CRM_AP_ACGCR]     /* restore default */
        mov r1, #0x5
        str r1, [r0, #CRM_AP_ARCGR]    /* restore default */

        /*
        * Only when NOT directly booting from SDRAM:
        * ARM core=266MHz, AHB=133MHz, IP=66.5MHz.
        * This is to work around the booting problem with RVD on EVB only.
        * Brassboard is fine.
        */
        ldr r0, CRM_AP_BASE_ADDR_W
        mov r1, #0x011
        str r1, [r0, #CRM_AP_ACDR]

        // SD clock input select - usb_clk. divider changed to 1
        mov r1, #0x00000140
        add r1, r1, #0x00000002
        str r1, [r0, #CRM_AP_APR]
    .endm /* init_clock */

    /* M3IF setup */
    .macro init_m3if
        /* Configure M3IF registers */
        ldr r1, M3IF_BASE_W
        /*
        * M3IF Control Register (M3IFCTL)
        * MRRP[0] = TMAX not on priority list (0 << 0)        = 0x00000000
        * MRRP[1] = SMIF not on priority list (0 << 0)        = 0x00000000
        * MRRP[2] = MAX0 not on priority list (0 << 0)        = 0x00000000
        * MRRP[3] = MAX1 not on priority list (0 << 0)        = 0x00000000
        * MRRP[4] = SDMA not on priority list (0 << 0)        = 0x00000000
        * MRRP[5] = MPEG4 not on priority list (0 << 0)       = 0x00000000
        * MRRP[6] = IPU on priority list (1 << 6)             = 0x00000040
        * MRRP[7] = SMIF-L2CC not on priority list (0 << 0)   = 0x00000000
        *                                                       ------------
        *                                                       0x00000040
        */
        ldr r0, =0x00000040
        str r0, [r1]  /* M3IF control reg */
    .endm /* init_m3if */

    /* CS0 sync mode setup */
    .macro init_cs0_sync
        /*
         * Sync mode (AHB Clk = 133MHz ; BCLK = 44.3MHz):
         */
        /* Flash reset command */
        ldr     r0, =CS0_BASE_ADDR
        ldr     r1, =0xF0F0
        strh    r1, [r0]
        /* 1st command */
        ldr     r2, =0xAAA
        add     r2, r2, r0
        ldr     r1, =0xAAAA
        strh    r1, [r2]
        /* 2nd command */
        ldr     r2, =0x554
        add     r2, r2, r0
        ldr     r1, =0x5555
        strh    r1, [r2]
        /* 3rd command */
        ldr     r2, =0xAAA
        add     r2, r2, r0
        ldr     r1, =0xD0D0
        strh    r1, [r2]
        /* Write flash config register */
        ldr     r1, =0x56CA
        strh    r1, [r2]
        /* Flash reset command */
        ldr     r1, =0xF0F0
        strh    r1, [r0]

        ldr r0, =WEIM_BASE_ADDR
        ldr r1, =0x23524E80
        str r1, [r0, #CSCRU]
        ldr r1, =0x10000D03
        str r1, [r0, #CSCRL]
        ldr r1, =0x00720900
        str r1, [r0, #CSCRA]
    .endm /* init_cs0_sync */

    /* CS0 async mode setup */
    .macro init_cs0_async
        /* Async flash mode */
        ldr r0, =WEIM_CTRL_CS0
        ldr r1, WEIM_CSCRU_0x0000CC03
        str r1, [r0, #CSCRU]
        ldr r1, WEIM_CSCRL_0xA0330D01
        str r1, [r0, #CSCRL]
        ldr r1, WEIM_CSCRA_0x00220800
        str r1, [r0, #CSCRA]
    .endm /* init_cs0_async */

    /* CPLD on CS4 setup */
    .macro init_cs4
        ldr r0, =WEIM_CTRL_CS4
        ldr r1, =0x0000D743
        str r1, [r0, #CSCRU]
        ldr r1, =0x42001521
        str r1, [r0, #CSCRL]
        ldr r1, =0x00430A00
        str r1, [r0, #CSCRA]

        ldr r0, CS4_BASE_ADDR_W
        ldrh r1, [r0, #0x0]
        and r0, r1, #0xFF00
        cmp r0, #0x2000
        bge done_cs4_setup

        ldr r0, =WEIM_CTRL_CS4
        ldr r1, =0x0000DCF6
        str r1, [r0, #CSCRU]
        ldr r1, =0x444A4541
        str r1, [r0, #CSCRL]
        ldr r1, =0x44443302
        str r1, [r0, #CSCRA]

done_cs4_setup:

    .endm /* init_cs4 */

// DDR SDRAM setup
    .macro  init_ddr_sdram
        ldr r0, ESDCTL_BASE_W
        /*
         * Configure Enhanced SDRAM Miscellaneous Register (ESDMISC) Register
         * Set to DDR Mode (Not SDR), Do a delay line reset
         * as the EMI AHB clock was changed
         */
        mov r1, #0xC            // set ESDMISC reg
        str r1, [r0, #0x10]
        // Hold for more than 200ns
        mov r1, #0x10000
    1:
        subs r1, r1, #0x1
        bne 1b

        /*
         * Configure Enhanced SDRAM Configuration Register 0
         *  (ESDCFG0) Register. XP=2 MRD=2 RAS=6 CAS=3 Clocks
         */
        ldr r1, SDRAM_0x00395728
        str r1, [r0, #0x4]

        /*
         * Precharge all rows (ROW/COL Muxing NOT used outside
         * NORMAL Mode); MODE=PRECHARGE ALL.
         */
        ldr r1, SDRAM_0x92210080
        str r1, [r0]

        /* PRECHARGE ALL (A10=1). */
        ldr r1, =0x0
        mov r12, #0x80000000
        add r12, r12, #0x00000400
        str r1, [r12]
        /* Run two refresh cycles; MODE=AUTO REFRESH */
        ldr r1, SDRAM_0xA2210080
        str r1, [r0]

        /* AUTO REFRESH 2 times */
        ldr r1, =0x0
        mov r12, #0x80000000
        str r1, [r12]
        str r1, [r12]

        /*
         * Configure DDRAM Operating mode to Load Mode
         * Register Command; MODE=LOAD MODE REGISTER.
         */
        ldr r1, SDRAM_0xB2210080
        str r1, [r0]

        /* LOAD MODE */
        ldr r1, =0x0
        mov r12, #0x80000000
        add r12, r12, #0x00000033
        strb r1, [r12]

        /* Load Extended Mode register */
        mov r12, #0x81000000
        strb r1, [r12]

        /*
         * Put controller in Normal mode. SDRAM now ready for
         * accesses; Configure CSD0 ESDCTL0 and go into Normal
         * Read/Write Mode; 16-bit[D0..D15];  BL=8; row=13;
         * col=10; MODE=Normal
         */
        ldr r1, SDRAM_0x82216080
        str r1, [r0]

        /* Dummy Write */
        ldr r1, =0x0
        mov r12, #0x80000000
        strb r1, [r12]

        /* Dummy write into DDRAM */
        ldr r1, SDRAM_0xC001C001
        mov r12, #0x80000000
        strb r1, [r12]

        /* Dummy Read From DDRAM */
        ldr r1, SDRAM_BASE_ADDR_W

        /* DISABLE CSD1 */
        ldr r1, =0x0
        str r1, [r0, #0x8]
    .endm

// SDR SDRAM setup
    /* r3 = value for ESDCTL0
     * r4 = burst mode vs full-page mode */
    .macro  init_sdr_sdram
        ldr r0, ESDCTL_BASE_W
        mov r2, #SDRAM_BASE_ADDR
        ldr r1, SDRAM_0x0075E73A
        str r1, [r0, #0x4]
        ldr r1, =0x2            // reset
        str r1, [r0, #0x10]
        ldr r1, =0x0            // sdr
        str r1, [r0, #0x10]

        // Hold for more than 200ns
        ldr r1, =0x10000
1:
        subs r1, r1, #0x1
        bne 1b

        ldr r1, SDRAM_0x92126080
        str r1, [r0]
        ldr r1, =0x0
        mov r12, #0x80000000
        add r12, r12, #0x00000400
        str r1, [r12]
        ldr r1, SDRAM_0xA2126080
        str r1, [r0]

        ldr r1, =0x0
        str r1, [r2]
        str r1, [r2]

        ldr r1, SDRAM_0xB2126180
        str r1, [r0]

        ldr r1, =0x0
        strb r1, [r2, r4]
        mov r12, #0x81000000
        str r1, [r12]
        str r3, [r0]
        ldr r1, =0x0
        str r1, [r2]
    .endm

    .macro do_wait_op_done
    1:
        ldrh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        ands r3, r3, #NAND_FLASH_CONFIG2_INT_DONE
        beq 1b
        mov r3, #0x0
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
    .endm   // do_wait_op_done

    .macro do_addr_input
        and r3, r3, #0xFF
        strh r3, [r12, #NAND_FLASH_ADD_REG_OFF]
        mov r3, #NAND_FLASH_CONFIG2_FADD_EN
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        do_wait_op_done
    .endm   // do_addr_input

    /* Required for MXC91221 PASS 2 for 133MHz SDR */
    .macro  init_drive_strength_sdr
        ldr r0, IOMUX_COM_BASE_ADDR_W
        add r0, r0, #0x200
        ldr r1, =0x0082
        strh r1, [r0, #0x0]
        ldr r1, =0x0002
        strh r1, [r0, #0x2]
        ldr r1, =0x0108
        strh r1, [r0, #0x4]
        ldr r1, =0x0103
        strh r1, [r0, #0x8]
        ldr r1, =0x01C3
        strh r1, [r0, #0xA]
        ldr r1, =0x0183
        strh r1, [r0, #0xC]
    .endm

    /* Required for DDR */
    .macro  init_drive_strength_ddr
        ldr r0, IOMUX_COM_BASE_ADDR_W
        add r0, r0, #0x200
        ldr r1, =0x0082
        strh r1, [r0, #0x0]
        ldr r1, =0x0012
        strh r1, [r0, #0x2]
        ldr r1, =0x0112
        strh r1, [r0, #0x4]
        ldr r1, =0x012
        strh r1, [r0, #0x6]
        ldr r1, =0x0103
        strh r1, [r0, #0x8]
        ldr r1, =0x0587
        strh r1, [r0, #0xA]
        ldr r1, =0x0187
        strh r1, [r0, #0xC]
        ldr r1, =0x0107
        strh r1, [r0, #0xE]
    .endm

#define PLATFORM_VECTORS         _platform_vectors
    .macro  _platform_vectors
        .globl  _board_BCR, _board_CFG
_board_BCR:   .long   0       // Board Control register shadow
_board_CFG:   .long   0       // Board Configuration (read at RESET)
    .endm

L2CACHE_PARAM:          .word   0x00030024
IIM_SREV_REG_VAL:       .word   IIM_BASE_ADDR + IIM_SREV_OFF
AIPS1_CTRL_BASE_ADDR_W: .word   AIPS1_CTRL_BASE_ADDR
AIPS2_CTRL_BASE_ADDR_W: .word   AIPS2_CTRL_BASE_ADDR
CS4_BASE_ADDR_W:        .word   CS4_BASE_ADDR
AIPS1_PARAM_W:          .word   0x77777777
MAX_BASE_ADDR_W:        .word   MAX_BASE_ADDR
MAX_PARAM1:             .word   0x00000321
RVAL_WVAL_W:            .word   0x515
CLKCTL_BASE_ADDR_W:     .word   CLKCTL_BASE_ADDR
PLL0_BASE_ADDR_W:       .word   PLL0_BASE_ADDR
CRM_AP_BASE_ADDR_W:     .word   CRM_AP_BASE_ADDR
SRC_BASE_ADDR_W:        .word   SRC_BASE_ADDR
IOMUX_COM_BASE_ADDR_W:  .word   IOMUX_COM_BASE_ADDR
PLL2_BASE_ADDR_W:       .word   PLL2_BASE_ADDR
SPBA_CTRL_BASE_ADDR_W:  .word   SPBA_CTRL_BASE_ADDR
WEIM_CSCRU_0x0000CC03:  .word   0x0000CC03
WEIM_CSCRL_0xA0330D01:  .word   0xA0330D01
WEIM_CSCRA_0x00220800:  .word   0x00220800
ESDCTL_BASE_W:          .word   ESDCTL_BASE
SDRAM_BASE_ADDR_W:      .word   SDRAM_BASE_ADDR
M3IF_BASE_W:            .word   M3IF_BASE
CRM_AP_ACDER_W:         .word   0x59155912
SDRAM_0x92126080:       .word   0x92126080
SDRAM_0xA2126080:       .word   0xA2126080
SDRAM_0xB2126180:       .word   0xB2126180
SDRAM_0x0075E73A:       .word   0x0075E73A
SDRAM_0x00395728:       .word   0x00395728
SDRAM_0x92210080:       .word   0x92210080
SDRAM_0xA2210080:       .word   0xA2210080
SDRAM_0xB2210080:       .word   0xB2210080
SDRAM_0x82216080:       .word   0x82216080
SDRAM_0xC001C001:       .word   0xC001C001
SDRAM_COMPARE_CONST1:   .word   0x55555555
SDRAM_COMPARE_CONST2:   .word   0xAAAAAAAA
MXC_REDBOOT_ROM_START:  .word   SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000
CONST_0x0FFF:           .word   0x0FFF
AVIC_VECTOR0_ADDR_W:    .word   MXCBOOT_FLAG_REG
AVIC_VECTOR1_ADDR_W:    .word   MXCFIS_FLAG_REG

/*---------------------------------------------------------------------------*/
/* end of hal_platform_setup.h                                               */
#endif /* CYGONCE_HAL_PLATFORM_SETUP_H */
