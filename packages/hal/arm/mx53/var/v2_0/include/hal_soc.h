//==========================================================================
//
//      hal_soc.h
//
//      SoC chip definitions
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
// Copyright (C) 2002 Gary Thomas
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#ifndef __HAL_SOC_H__
#define __HAL_SOC_H__

#ifdef __ASSEMBLER__
#define UL(a)				(a)
#define VA(a)				(a)
#define REG8(a)				(a)
#define REG16(a)			(a)
#define REG32(a)			(a)

#else /* __ASSEMBLER__ */
#define UL(a)				(a##UL)
#define VA(a)				((void *)(a))

extern char HAL_PLATFORM_EXTRA[40];
externC void plf_hardware_init(void);

extern int adjust_core_voltage(unsigned int);

#define REG8(a)				(*(volatile unsigned char *)(a))
#define REG16(a)			(*(volatile unsigned short *)(a))
#define REG32(a)			(*(volatile unsigned int *)(a))

#define readb(a)			(*(volatile unsigned char *)(a))
#define readw(a)			(*(volatile unsigned short *)(a))
#define readl(a)			(*(volatile unsigned int *)(a))
#define writeb(v,a)			(*(volatile unsigned char *)(a) = (v))
#define writew(v,a)			(*(volatile unsigned short *)(a) = (v))
#define writel(v,a)			(*(volatile unsigned int *)(a) = (v))

#endif /* __ASSEMBLER__ */

#include <cyg/hal/mx53_iomux.h>

/*
 * Default Memory Layout Definitions
 */

#define MXC_NAND_BASE_DUMMY 0

/*
 * UART Chip level Configuration that a user may not have to edit. These
 * configuration vary depending on how the UART module is integrated with
 * the ARM core
 */
#define MXC_UART_NR 3
/*!
 * This option is used to set or clear the RXDMUXSEL bit in control reg 3.
 * Certain platforms need this bit to be set in order to receive Irda data.
 */
#define MXC_UART_IR_RXDMUX			0x0004
/*!
 * This option is used to set or clear the RXDMUXSEL bit in control reg 3.
 * Certain platforms need this bit to be set in order to receive UART data.
 */
#define MXC_UART_RXDMUX				0x0004

/*
   * ROM address
   */
#define ROM_BASE_ADDR				UL(0x00000000)
//#define ROM_BASE_ADDR_VIRT			VA(0x20000000)

//#define ROM_SI_REV_OFFSET			0x48

#define PLATFORM_BASE_ADDR			ARM_ELBOW_BASE_ADDR
#define PLATFORM_ICGC				0x14

/*
 * GPU control registers
 */
#define IPU_CTRL_BASE_ADDR			UL(0x18000000)
#define GPU_2D_BASE_ADDR			UL(0x20000000)
#define GPU_3D_BASE_ADDR			UL(0x30000000)

#define TZIC_BASE_ADDR				UL(0x0FFFC000)

#define DEBUG_BASE_ADDR				UL(0x40000000)
#define DEBUG_ROM_ADDR				(DEBUG_BASE_ADDR + 0x0)
#define ETB_BASE_ADDR				(DEBUG_BASE_ADDR + 0x00001000)
#define ETM_BASE_ADDR				(DEBUG_BASE_ADDR + 0x00002000)
#define TPIU_BASE_ADDR				(DEBUG_BASE_ADDR + 0x00003000)
#define CTI0_BASE_ADDR				(DEBUG_BASE_ADDR + 0x00004000)
#define CTI1_BASE_ADDR				(DEBUG_BASE_ADDR + 0x00005000)
#define CTI2_BASE_ADDR				(DEBUG_BASE_ADDR + 0x00006000)
#define CTI3_BASE_ADDR				(DEBUG_BASE_ADDR + 0x00007000)
#define CORTEX_DBG_BASE_ADDR		(DEBUG_BASE_ADDR + 0x00008000)

/*
 * SPBA global module enabled #0
 */
#define SPBA0_BASE_ADDR				UL(0x50000000)

#define MMC_SDHC1_BASE_ADDR			(SPBA0_BASE_ADDR + 0x00004000)
#define ESDHC1_REG_BASE				MMC_SDHC1_BASE_ADDR
#define MMC_SDHC2_BASE_ADDR			(SPBA0_BASE_ADDR + 0x00008000)
#define UART3_BASE_ADDR				(SPBA0_BASE_ADDR + 0x0000C000)
//eCSPI1
#define CSPI1_BASE_ADDR				(SPBA0_BASE_ADDR + 0x00010000)
#define SSI2_BASE_ADDR				(SPBA0_BASE_ADDR + 0x00014000)
#define ESAI1_BASE_ADDR				(SPBA0_BASE_ADDR + 0x00018000)
#define MMC_SDHC3_BASE_ADDR			(SPBA0_BASE_ADDR + 0x00020000)
#define MMC_SDHC4_BASE_ADDR			(SPBA0_BASE_ADDR + 0x00024000)
#define SPDIF_BASE_ADDR				(SPBA0_BASE_ADDR + 0x00028000)
#define ASRC_BASE_ADDR				(SPBA0_BASE_ADDR + 0x0002C000)
#define ATA_DMA_BASE_ADDR			(SPBA0_BASE_ADDR + 0x00030000)
#define SPBA_CTRL_BASE_ADDR			(SPBA0_BASE_ADDR + 0x0003C000)

/*
 * AIPS 1
 */
#define AIPS1_BASE_ADDR				UL(0x53F00000)
#define AIPS1_CTRL_BASE_ADDR		AIPS1_BASE_ADDR
#define USBOH3_BASE_ADDR			(AIPS1_BASE_ADDR + 0x00080000)
#define GPIO1_BASE_ADDR				(AIPS1_BASE_ADDR + 0x00084000)
#define GPIO2_BASE_ADDR				(AIPS1_BASE_ADDR + 0x00088000)
#define GPIO3_BASE_ADDR				(AIPS1_BASE_ADDR + 0x0008C000)
#define GPIO4_BASE_ADDR				(AIPS1_BASE_ADDR + 0x00090000)
#define KPP_BASE_ADDR				(AIPS1_BASE_ADDR + 0x00094000)
#define WDOG1_BASE_ADDR				(AIPS1_BASE_ADDR + 0x00098000)
#define WDOG_BASE_ADDR				WDOG1_BASE_ADDR
#define WDOG2_BASE_ADDR				(AIPS1_BASE_ADDR + 0x0009C000)
#define GPT_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000A0000)
#define SRTC_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000A4000)
#define IOMUXC_BASE_ADDR			(AIPS1_BASE_ADDR + 0x000A8000)
#define EPIT1_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000AC000)
#define EPIT2_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000B0000)
#define PWM1_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000B4000)
#define PWM2_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000B8000)
#define UART1_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000BC000)
#define UART2_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000C0000)

#define SRC_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000D0000)
#define CCM_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000D4000)
#define GPC_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000D8000)
#define GPIO5_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000DC000)
#define GPIO6_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000E0000)
#define GPIO7_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000E4000)
#define PATA_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000E8000)
#define I2C3_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000EC000)

#define UART4_BASE_ADDR				(AIPS1_BASE_ADDR + 0x000F0000)

/*
 * AIPS 2
 */
#define AIPS2_BASE_ADDR				UL(0x63F00000)
#define AIPS2_CTRL_BASE_ADDR		AIPS2_BASE_ADDR
#define PLL1_BASE_ADDR				(AIPS2_BASE_ADDR + 0x00080000)
#define PLL2_BASE_ADDR				(AIPS2_BASE_ADDR + 0x00084000)
#define PLL3_BASE_ADDR				(AIPS2_BASE_ADDR + 0x00088000)
#define PLL4_BASE_ADDR				(AIPS2_BASE_ADDR + 0x0008C000)
#define UART5_BASE_ADDR				(AIPS2_BASE_ADDR + 0x00090000)
#define AHBMAX_BASE_ADDR			(AIPS2_BASE_ADDR + 0x00094000)
#define MAX_BASE_ADDR				AHBMAX_BASE_ADDR
#define IIM_BASE_ADDR				(AIPS2_BASE_ADDR + 0x00098000)
#define CSU_BASE_ADDR				(AIPS2_BASE_ADDR + 0x0009C000)
#define ARM_ELBOW_BASE_ADDR			(AIPS2_BASE_ADDR + 0x000A0000)
#define OWIRE_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000A4000)
#define FIRI_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000A8000)
// eCSPI2
#define CSPI2_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000AC000)
#define SDMA_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000B0000)
#define SCC_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000B4000)
#define ROMCP_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000B8000)
#define RTIC_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000BC000)
// actually cspi1
#define CSPI3_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000C0000)
#define I2C2_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000C4000)
#define I2C1_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000C8000)
#define I2C_BASE_ADDR				I2C1_BASE_ADDR
#define SSI1_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000CC000)
#define AUDMUX_BASE_ADDR			(AIPS2_BASE_ADDR + 0x000D0000)

#define M4IF_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000D8000)
#define ESDCTL_BASE_ADDR			(AIPS2_BASE_ADDR + 0x000D9000)
#define WEIM_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000DA000)
#define NFC_IP_BASE					(AIPS2_BASE_ADDR + 0x000DB000)
#define EMI_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000DBF00)
#define SSI3_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000E8000)
#define FEC_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000EC000)
#define SOC_FEC_BASE				FEC_BASE_ADDR
#define TVE_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000F0000)
#define VPU_BASE_ADDR				(AIPS2_BASE_ADDR + 0x000F4000)
#define SAHARA_BASE_ADDR			(AIPS2_BASE_ADDR + 0x000F8000)

/*
 * Memory regions and CS
 */
#define CSD0_BASE_ADDR				UL(0x70000000)
#define CSD1_BASE_ADDR				UL(0xB0000000)
#define CS0_BASE_ADDR				UL(0xF0000000)
#define CS1_BASE_ADDR				UL(0xF4000000)

/*
 * NFC internal RAM
 */
#define NFC_BASE_ADDR_AXI			UL(0xF7FF0000)
#define NFC_BASE					NFC_BASE_ADDR_AXI

/*
 * IRAM
 */
#define IRAM_BASE_ADDR				UL(0xF8000000)	/* 128K internal ram */

/*
 * Graphics Memory of GPU
 */
#define GPU_MEM_BASE_ADDR			UL(0xF8020000)

/*
 * Number of GPIO ports as defined in the IC Spec
 */
#define GPIO_PORT_NUM				7
/*
 * Number of GPIO pins per port
 */
#define GPIO_NUM_PIN				32

/* CCM */
#define CLKCTL_CCR					0x00
#define CLKCTL_CCDR					0x04
#define CLKCTL_CSR					0x08
#define CLKCTL_CCSR					0x0C
#define CLKCTL_CACRR				0x10
#define CLKCTL_CBCDR				0x14
#define CLKCTL_CBCMR				0x18
#define CLKCTL_CSCMR1				0x1C
#define CLKCTL_CSCMR2				0x20
#define CLKCTL_CSCDR1				0x24
#define CLKCTL_CS1CDR				0x28
#define CLKCTL_CS2CDR				0x2C
#define CLKCTL_CDCDR				0x30
#define CLKCTL_CHSCCDR				0x34
#define CLKCTL_CSCDR2				0x38
#define CLKCTL_CSCDR3				0x3C
#define CLKCTL_CSCDR4				0x40
#define CLKCTL_CWDR					0x44
#define CLKCTL_CDHIPR				0x48
#define CLKCTL_CDCR					0x4C
#define CLKCTL_CTOR					0x50
#define CLKCTL_CLPCR				0x54
#define CLKCTL_CISR					0x58
#define CLKCTL_CIMR					0x5C
#define CLKCTL_CCOSR				0x60
#define CLKCTL_CGPR					0x64
#define CLKCTL_CCGR0				0x68
#define CLKCTL_CCGR1				0x6C
#define CLKCTL_CCGR2				0x70
#define CLKCTL_CCGR3				0x74
#define CLKCTL_CCGR4				0x78
#define CLKCTL_CCGR5				0x7C
#define CLKCTL_CCGR6				0x80
#define CLKCTL_CCGR7				0x84
#define CLKCTL_CMEOR				0x88

#define FREQ_24MHZ					24000000
#define FREQ_32768HZ				(32768 * 1024)
#define FREQ_38400HZ				(38400 * 1024)
#define FREQ_32000HZ				(32000 * 1024)
#define PLL_REF_CLK					FREQ_24MHZ
#define CKIH						22579200
//#define PLL_REF_CLK  FREQ_32768HZ
//#define PLL_REF_CLK  FREQ_32000HZ

/* WEIM registers */
#define CSGCR1						0x00
#define CSGCR2						0x04
#define CSRCR1						0x08
#define CSRCR2						0x0C
#define CSWCR1						0x10

/* M4IF */
#define M4IF_FBPM0					0x40
#define M4IF_FBPM1					0x44
#define M4IF_FIDBP					0x48
#define M4IF_MIF4					0x48
#define M4IF_FPWC					0x9C

/* ESDCTL */
#define ESDCTL_ESDCTL0				0x00
#define ESDCTL_ESDCFG0				0x04
#define ESDCTL_ESDCTL1				0x08
#define ESDCTL_ESDCFG1				0x0C
#define ESDCTL_ESDMISC				0x18
#define ESDCTL_ESDSCR				0x1c
#define ESDCTL_ESDMRR				0x34
#define ESDCTL_WLGCR				0x48
#define ESDCTL_RDDLHWCTL			0xa0
#define ESDCTL_WRDLHWCTL			0xa4

/* DPLL */
#define PLL_DP_CTL					0x00
#define PLL_DP_CONFIG				0x04
#define PLL_DP_OP					0x08
#define PLL_DP_MFD					0x0C
#define PLL_DP_MFN					0x10
#define PLL_DP_MFNMINUS				0x14
#define PLL_DP_MFNPLUS				0x18
#define PLL_DP_HFS_OP				0x1C
#define PLL_DP_HFS_MFD				0x20
#define PLL_DP_HFS_MFN				0x24
#define PLL_DP_TOGC					0x28
#define PLL_DP_DESTAT				0x2C

#define CHIP_REV_1_0				0x0		 /* PASS 1.0 */
#define CHIP_REV_1_1				0x1		 /* PASS 1.1 */
#define CHIP_REV_2_0				0x2		 /* PASS 2.0 */
#define CHIP_LATEST					CHIP_REV_1_1

#define IIM_STAT_OFF				0x00
#define IIM_STAT_BUSY				(1 << 7)
#define IIM_STAT_PRGD				(1 << 1)
#define IIM_STAT_SNSD				(1 << 0)
#define IIM_STATM_OFF				0x04
#define IIM_ERR_OFF					0x08
#define IIM_ERR_PRGE				(1 << 7)
#define IIM_ERR_WPE					(1 << 6)
#define IIM_ERR_OPE					(1 << 5)
#define IIM_ERR_RPE					(1 << 4)
#define IIM_ERR_WLRE				(1 << 3)
#define IIM_ERR_SNSE				(1 << 2)
#define IIM_ERR_PARITYE				(1 << 1)
#define IIM_EMASK_OFF				0x0C
#define IIM_FCTL_OFF				0x10
#define IIM_UA_OFF					0x14
#define IIM_LA_OFF					0x18
#define IIM_SDAT_OFF				0x1C
#define IIM_PREV_OFF				0x20
#define IIM_SREV_OFF				0x24
#define IIM_PREG_P_OFF				0x28
#define IIM_SCS0_OFF				0x2C
#define IIM_SCS1_P_OFF				0x30
#define IIM_SCS2_OFF				0x34
#define IIM_SCS3_P_OFF				0x38

#define IIM_PROD_REV_SH				3
#define IIM_PROD_REV_LEN			5
#define IIM_SREV_REV_SH				4
#define IIM_SREV_REV_LEN			4
#define PROD_SIGNATURE_MX53			0x1

#define EPIT_BASE_ADDR				EPIT1_BASE_ADDR
#define EPITCR						0x00
#define EPITSR						0x04
#define EPITLR						0x08
#define EPITCMPR					0x0C
#define EPITCNR						0x10

#define GPTCR						0x00
#define GPTPR						0x04
#define GPTSR						0x08
#define GPTIR						0x0C
#define GPTOCR1						0x10
#define GPTOCR2						0x14
#define GPTOCR3						0x18
#define GPTICR1						0x1C
#define GPTICR2						0x20
#define GPTCNT						0x24

/* Assuming 24MHz input clock with doubler ON */
/*									  MFI		  PDF */
#define DP_OP_1000					((10 << 4) + ((1 - 1) << 0))
#define DP_MFD_1000					(12 - 1)
#define DP_MFN_1000					5

#define DP_OP_850					((8 << 4) + ((1 - 1) << 0))
#define DP_MFD_850					(48 - 1)
#define DP_MFN_850					41

#define DP_OP_800					((8 << 4) + ((1 - 1) << 0))
#define DP_MFD_800					(3 - 1)
#define DP_MFN_800					1

#define DP_OP_700					((7 << 4) + ((1 - 1) << 0))
#define DP_MFD_700					(24 - 1)
#define DP_MFN_700					7

#define DP_OP_400					((8 << 4) + ((2 - 1) << 0))
#define DP_MFD_400					(3 - 1)
#define DP_MFN_400					1

#define DP_OP_532					((5 << 4) + ((1 - 1) << 0))
#define DP_MFD_532					(24 - 1)
#define DP_MFN_532					13

#define DP_OP_665					((6 << 4) + ((1 - 1) << 0))
#define DP_MFD_665					(96 - 1)
#define DP_MFN_665					89

#define DP_OP_666					((6 << 4) + ((1 - 1) << 0))
#define DP_MFD_666					(16 - 1)
#define DP_MFN_666					15

#define DP_OP_333					((6 << 4) + ((2 - 1) << 0))
#define DP_MFD_333					(16 - 1)
#define DP_MFN_333					15

#define DP_OP_266					((5 << 4) + ((2 - 1) << 0))
#define DP_MFD_266					(24 - 1)
#define DP_MFN_266					13

#define DP_OP_216					((9 << 4) + ((4 - 1) << 0))
#define DP_MFD_216					(1 - 1)
#define DP_MFN_216					0

#define PROD_SIGNATURE_SUPPORTED  PROD_SIGNATURE_MX51

#define CHIP_VERSION_NONE			0xFFFFFFFF		// invalid product ID
#define CHIP_VERSION_UNKNOWN		0xDEADBEEF		// invalid chip rev

#define PART_NUMBER_OFFSET			12
#define MAJOR_NUMBER_OFFSET			4
#define MINOR_NUMBER_OFFSET			0

//#define BARKER_CODE_SWAP_LOC		0x404
#define BARKER_CODE_VAL				0xB1
#define NFC_V3_0

// This defines the register base for the NAND AXI registers
#define NAND_REG_BASE				(NFC_BASE_ADDR_AXI + 0x1E00)

#define NAND_CMD_REG				(NAND_REG_BASE + 0x00)
#define NAND_ADD0_REG				(NAND_REG_BASE + 0x04)
#define NAND_ADD1_REG				(NAND_REG_BASE + 0x08)
#define NAND_ADD2_REG				(NAND_REG_BASE + 0x0C)
#define NAND_ADD3_REG				(NAND_REG_BASE + 0x10)
#define NAND_ADD4_REG				(NAND_REG_BASE + 0x14)
#define NAND_ADD5_REG				(NAND_REG_BASE + 0x18)
#define NAND_ADD6_REG				(NAND_REG_BASE + 0x1C)
#define NAND_ADD7_REG				(NAND_REG_BASE + 0x20)
#define NAND_ADD8_REG				(NAND_REG_BASE + 0x24)
#define NAND_ADD9_REG				(NAND_REG_BASE + 0x28)
#define NAND_ADD10_REG				(NAND_REG_BASE + 0x2C)
#define NAND_ADD11_REG				(NAND_REG_BASE + 0x30)

#define NAND_CONFIGURATION1_REG		(NAND_REG_BASE + 0x34)
#define NAND_CONFIGURATION1_NFC_RST	(1 << 2)
#define NAND_CONFIGURATION1_NF_CE	(1 << 1)
#define NAND_CONFIGURATION1_SP_EN	(1 << 0)

#define NAND_ECC_STATUS_RESULT_REG	(NAND_REG_BASE + 0x38)

#define NAND_STATUS_SUM_REG			(NAND_REG_BASE + 0x3C)

#define NAND_LAUNCH_REG				(NAND_REG_BASE + 0x40)
#define NAND_LAUNCH_FCMD			(1 << 0)
#define NAND_LAUNCH_FADD			(1 << 1)
#define NAND_LAUNCH_FDI				(1 << 2)
#define NAND_LAUNCH_AUTO_PROG		(1 << 6)
#define NAND_LAUNCH_AUTO_READ		(1 << 7)
#define NAND_LAUNCH_AUTO_READ_CONT	(1 << 8)
#define NAND_LAUNCH_AUTO_ERASE		(1 << 9)
#define NAND_LAUNCH_COPY_BACK0		(1 << 10)
#define NAND_LAUNCH_COPY_BACK1		(1 << 11)
#define NAND_LAUNCH_AUTO_STAT		(1 << 12)

#define NFC_WR_PROT_REG				(NFC_IP_BASE + 0x00)
#define UNLOCK_BLK_ADD0_REG			(NFC_IP_BASE + 0x04)
#define UNLOCK_BLK_ADD1_REG			(NFC_IP_BASE + 0x08)
#define UNLOCK_BLK_ADD2_REG			(NFC_IP_BASE + 0x0C)
#define UNLOCK_BLK_ADD3_REG			(NFC_IP_BASE + 0x10)
#define UNLOCK_BLK_ADD4_REG			(NFC_IP_BASE + 0x14)
#define UNLOCK_BLK_ADD5_REG			(NFC_IP_BASE + 0x18)
#define UNLOCK_BLK_ADD6_REG			(NFC_IP_BASE + 0x1C)
#define UNLOCK_BLK_ADD7_REG			(NFC_IP_BASE + 0x20)

#define NFC_FLASH_CONFIG2_REG		(NFC_IP_BASE + 0x24)
#define NFC_FLASH_CONFIG2_ECC_EN	(1 << 3)

#define NFC_FLASH_CONFIG3_REG		(NFC_IP_BASE + 0x28)

#define NFC_IPC_REG					(NFC_IP_BASE + 0x2C)
#define NFC_IPC_INT					(1 << 31)
#define NFC_IPC_AUTO_DONE			(1 << 30)
#define NFC_IPC_LPS					(1 << 29)
#define NFC_IPC_RB_B				(1 << 28)
#define NFC_IPC_CACK				(1 << 1)
#define NFC_IPC_CREQ				(1 << 0)
#define NFC_AXI_ERR_ADD_REG			(NFC_IP_BASE + 0x30)

#define MXC_MMC_BASE_DUMMY			0x00000000

#define NAND_FLASH_BOOT				(1 << 28)
#define FROM_NAND_FLASH				NAND_FLASH_BOOT

#define SDRAM_NON_FLASH_BOOT		(1 << 29)

#define MMC_FLASH_BOOT				(1 << 30)
#define FROM_MMC_FLASH				MMC_FLASH_BOOT

#define SPI_NOR_FLASH_BOOT			0x80000000
#define FROM_SPI_NOR_FLASH			SPI_NOR_FLASH_BOOT

#define IS_BOOTING_FROM_NAND()		1
#define IS_BOOTING_FROM_SPI_NOR()	0
#define IS_BOOTING_FROM_NOR()		0
#define IS_BOOTING_FROM_SDRAM()		0
#define IS_BOOTING_FROM_MMC()		0

#define IS_FIS_FROM_NAND()			1
#define IS_FIS_FROM_NOR()			0

#define SOC_MAC_ADDR_FUSE_BANK		1
#define SOC_MAC_ADDR_FUSE			9
#define SOC_MAC_ADDR_LOCK_FUSE		0
#define SOC_MAC_ADDR_LOCK_BIT		4

/*
 * This macro is used to get certain bit field from a number
 */
#define MXC_GET_FIELD(val, len, sh)	((val >> sh) & ((1 << len) - 1))

/*
 * This macro is used to set certain bit field inside a number
 */
#define MXC_SET_FIELD(val, len, sh, nval)	((val & ~(((1 << len) - 1) << sh)) | (nval << sh))

#define L2CC_ENABLED
#define UART_WIDTH_32		  /* internal UART is 32bit access only */

#if !defined(__ASSEMBLER__)
extern void fuse_blow_row(int bank, int row, int value);
extern unsigned int sense_fuse(int bank, int row, int bit);

void cyg_hal_plf_serial_init(void);
void cyg_hal_plf_serial_stop(void);
void hal_delay_us(unsigned int usecs);
#define HAL_DELAY_US(n)		hal_delay_us(n)
extern int _mxc_fis;
extern unsigned int system_rev;

enum plls {
	PLL1,
	PLL2,
	PLL3,
	PLL4,
};

enum main_clocks {
	CPU_CLK,
	AHB_CLK,
	IPG_CLK,
	IPG_PER_CLK,
	DDR_CLK,
	NFC_CLK,
	USB_CLK,
	AXI_A_CLK,
	AXI_B_CLK,
	EMI_SLOW_CLK,
};

enum peri_clocks {
	UART1_BAUD,
	UART2_BAUD,
	UART3_BAUD,
	SSI1_BAUD,
	SSI2_BAUD,
	CSI_BAUD,
	MSTICK1_CLK,
	MSTICK2_CLK,
	SPI1_CLK = CSPI1_BASE_ADDR,
	SPI2_CLK = CSPI2_BASE_ADDR,
};

extern unsigned int pll_clock(enum plls pll);

extern unsigned int get_main_clock(enum main_clocks clk);

extern unsigned int get_peri_clock(enum peri_clocks clk);

typedef unsigned int nfc_setup_func_t(unsigned int, unsigned int, unsigned int, unsigned int);

#endif //#if !defined(__ASSEMBLER__)

#endif /* __HAL_SOC_H__ */
