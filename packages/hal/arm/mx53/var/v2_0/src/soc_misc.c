//==========================================================================
//
//      soc_misc.c
//
//      HAL misc board support code
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <redboot.h>
#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>			// base types
#include <cyg/infra/cyg_trac.h>			// tracing macros
#include <cyg/infra/cyg_ass.h>			// assertion macros

#include <cyg/hal/hal_misc.h>			// Size constants
#include <cyg/hal/hal_io.h>				// IO macros
#include <cyg/hal/hal_arch.h>			// Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>			// Interrupt names
#include <cyg/hal/hal_cache.h>			// Cache control
#include <cyg/hal/hal_soc.h>			// Hardware definitions
#include <cyg/hal/hal_mm.h>				// MMap table definitions
#include <cyg/infra/diag.h>				// diag_printf
#ifdef MXCFLASH_SELECT_NAND
#include <cyg/io/imx_nfc.h>
#endif

// Most initialization has already been done before we get here.
// All we do here is set up the interrupt environment.
// FIXME: some of the stuff in hal_platform_setup could be moved here.

/*
 * System_rev will have the following format
 * 31-12 = part # (0x31, 0x32, 0x27, 0x91131, 0x91321, etc)
 * 11-8 = unused
 * 7-4 = major (1.y)
 * 3-0 = minor (x.0)
 */
unsigned int system_rev = CHIP_REV_1_0;
static int find_correct_chip;

#define SBMR_BOOT_CFG1_SHIFT		0
#define SBMR_BOOT_CFG1_MASK			(0xff << SBMR_BOOT_CFG1_SHIFT)
#define SBMR_BOOT_CFG1(r)			(((r) & SBMR_BOOT_CFG1_MASK) >> SBMR_BOOT_CFG1_SHIFT)

#define SBMR_BOOT_CFG2_SHIFT		8
#define SBMR_BOOT_CFG2_MASK			(0xff << SBMR_BOOT_CFG2_SHIFT)
#define SBMR_BOOT_CFG2(r)			(((r) & SBMR_BOOT_CFG2_MASK) >> SBMR_BOOT_CFG2_SHIFT)

#define SBMR_BOOT_CFG3_SHIFT		16
#define SBMR_BOOT_CFG3_MASK			(0xff << SBMR_BOOT_CFG3_SHIFT)
#define SBMR_BOOT_CFG3(r)			(((r) & SBMR_BOOT_CFG3_MASK) >> SBMR_BOOT_CFG3_SHIFT)

#define SBMR_BMOD_SHIFT				24
#define SBMR_BMOD_MASK				(0x3 << SBMR_BMOD_SHIFT)
#define SBMR_BMOD(r)				(((r) & SBMR_BMOD_MASK) >> SBMR_BMOD_SHIFT)

#define SBMR_BT_FUSE_SEL_SHIFT		26
#define SBMR_BT_FUSE_SEL_MASK		(3 << SBMR_BT_FUSE_SEL_SHIFT)
#define SBMR_BT_FUSE_SEL(r)			(((r) & SBMR_BT_FUSE_SEL_MASK) >> SBMR_BT_FUSE_SEL_SHIFT)

/*
 * This functions reads the IIM module and returns the system revision number.
 * It returns the IIM silicon revision reg value if valid product rev is found.
 . Otherwise, it returns -1.
 */
static int read_system_rev(void)
{
	int prev, srev;

	prev = readl(IIM_BASE_ADDR + IIM_PREV_OFF) >> 3;
	srev = readl(IIM_BASE_ADDR + IIM_SREV_OFF);

	system_rev = 0x53 << PART_NUMBER_OFFSET; /* For MX53 Platform*/

	/* Now try to retrieve the silicon rev from IIM's SREV register */
	return srev;
}

#ifdef MXCFLASH_SELECT_NAND
unsigned int mxc_nfc_soc_setup(unsigned int pg_sz, unsigned int io_sz,
							unsigned int is_mlc, unsigned int num_of_chips);
extern nfc_setup_func_t *nfc_setup;
#endif

#ifdef MXCFLASH_SELECT_MMC
//extern mxc_mmc_check_sdhc_boot_slot *check_sdhc_slot;
#endif

int mxc_check_sdhc_boot_slot(unsigned int port, unsigned int *sdhc_addr);

void hal_hardware_init(void)
{
	int ver;

	ver = read_system_rev();

	find_correct_chip = ver;

	if (ver != CHIP_VERSION_NONE) {
		/* Valid product revision found. Check actual silicon rev from the ROM code. */
		if (ver == 0x1) {
			HAL_PLATFORM_EXTRA[5] = '1';
			HAL_PLATFORM_EXTRA[7] = '0';
			system_rev |= 1 << MAJOR_NUMBER_OFFSET;
			system_rev |= 0 << MINOR_NUMBER_OFFSET;
		} else if (ver == 0x2) {
			HAL_PLATFORM_EXTRA[5] = '1';
			HAL_PLATFORM_EXTRA[7] = '1';
			system_rev |= 1 << MAJOR_NUMBER_OFFSET;
			system_rev |= 1 << MINOR_NUMBER_OFFSET;
		} else if (ver == 0x10) {
			HAL_PLATFORM_EXTRA[5] = '2';
			HAL_PLATFORM_EXTRA[7] = '0';
			system_rev |= 2 << MAJOR_NUMBER_OFFSET;
			system_rev |= 0 << MINOR_NUMBER_OFFSET;
		} else if (ver == 0x3) {
			HAL_PLATFORM_EXTRA[5] = '2';
			HAL_PLATFORM_EXTRA[7] = '1';
			system_rev |= 2 << MAJOR_NUMBER_OFFSET;
			system_rev |= 1 << MINOR_NUMBER_OFFSET;
		} else if (ver == 0x4) {
			HAL_PLATFORM_EXTRA[5] = '3';
			HAL_PLATFORM_EXTRA[7] = '0';
			system_rev |= 3 << MAJOR_NUMBER_OFFSET;
			system_rev |= 0 << MINOR_NUMBER_OFFSET;
		} else {
			HAL_PLATFORM_EXTRA[5] = 'x';
			HAL_PLATFORM_EXTRA[7] = 'x';
			system_rev |= 3 << MAJOR_NUMBER_OFFSET;
			system_rev |= 0 << MINOR_NUMBER_OFFSET;
			find_correct_chip = CHIP_VERSION_UNKNOWN;
		}

	}
	// Enable caches
#ifdef CYGSEM_HAL_ENABLE_ICACHE_ON_STARTUP
	HAL_ICACHE_ENABLE();
#endif
#ifdef CYGSEM_HAL_ENABLE_DCACHE_ON_STARTUP
	HAL_DCACHE_ENABLE();
#endif
	// enable EPIT and start it with 32KHz input clock
	writel(0x00010000, EPIT_BASE_ADDR + EPITCR);

	// make sure reset is complete
	while ((readl(EPIT_BASE_ADDR + EPITCR) & 0x10000) != 0) {
		static int retries = 10000;
		if (retries--)
			break;
	}

	writel(0x030E0002, EPIT_BASE_ADDR + EPITCR);
	writel(0x030E0003, EPIT_BASE_ADDR + EPITCR);

	writel(0, EPIT_BASE_ADDR + EPITCMPR);  // always compare with 0

	if ((readw(WDOG_BASE_ADDR) & 4) != 0) {
		// increase the WDOG timeout value to the max
		writew(readw(WDOG_BASE_ADDR) | 0xFF00, WDOG_BASE_ADDR);
	}
	// Perform any platform specific initializations
	plf_hardware_init();

	// Set up eCos/ROM interfaces
	hal_if_init();

#ifdef MXCFLASH_SELECT_NAND
	nfc_setup = mxc_nfc_soc_setup;
#endif
}

// -------------------------------------------------------------------------
void hal_clock_initialize(cyg_uint32 period)
{
}

// This routine is called during a clock interrupt.

// Define this if you want to ensure that the clock is perfect (i.e. does
// not drift).  One reason to leave it turned off is that it costs some
// us per system clock interrupt for this maintenance.
#undef COMPENSATE_FOR_CLOCK_DRIFT

void hal_clock_reset(cyg_uint32 vector, cyg_uint32 period)
{
}

// Read the current value of the clock, returning the number of hardware
// "ticks" that have occurred (i.e. how far away the current value is from
// the start)

// Note: The "contract" for this function is that the value is the number
// of hardware clocks that have happened since the last interrupt (i.e.
// when it was reset).  This value is used to measure interrupt latencies.
// However, since the hardware counter runs freely, this routine computes
// the difference between the current clock period and the number of hardware
// ticks left before the next timer interrupt.
void hal_clock_read(cyg_uint32 *pvalue)
{
}

// This is to cope with the test read used by tm_basic with
// CYGVAR_KERNEL_COUNTERS_CLOCK_LATENCY defined; we read the count ASAP
// in the ISR, *before* resetting the clock.  Which returns 1tick +
// latency if we just use plain hal_clock_read().
void hal_clock_latency(cyg_uint32 *pvalue)
{
}

unsigned int hal_timer_count(void)
{
	return (0xFFFFFFFF - readl(EPIT_BASE_ADDR + EPITCNR));
}

#define WDT_MAGIC_1				0x5555
#define WDT_MAGIC_2				0xAAAA
#define MXC_WDT_WSR				0x2

unsigned int i2c_base_addr[] = {
	I2C_BASE_ADDR,
	I2C2_BASE_ADDR,
};
unsigned int i2c_num = 2;

static unsigned int led_on = 0;
//
// Delay for some number of micro-seconds
//
void hal_delay_us(unsigned int usecs)
{
	/*
	 * This causes overflow.
	 * unsigned int delayCount = (usecs * 32768) / 1000000;
	 * So use the following one instead
	 */
	unsigned int delayCount = (usecs * 512) / 15625;

	// issue the service sequence instructions
	if ((readw(WDOG_BASE_ADDR) & 4) != 0) {
		writew(WDT_MAGIC_1, WDOG_BASE_ADDR + MXC_WDT_WSR);
		writew(WDT_MAGIC_2, WDOG_BASE_ADDR + MXC_WDT_WSR);
	}

	if (delayCount == 0) {
		return;
	}

	writel(0x01, EPIT_BASE_ADDR + EPITSR); // clear the compare status bit

	writel(delayCount, EPIT_BASE_ADDR + EPITLR);

	while ((0x1 & readl(EPIT_BASE_ADDR + EPITSR)) == 0); // return until compare bit is set
	if ((++led_on % 3000) == 0)
		BOARD_DEBUG_LED(0);
}

// -------------------------------------------------------------------------

// This routine is called to respond to a hardware interrupt (IRQ).  It
// should interrogate the hardware and return the IRQ vector number.
int hal_IRQ_handler(void)
{
#ifdef HAL_EXTENDED_IRQ_HANDLER
	cyg_uint32 index;

	// Use platform specific IRQ handler, if defined
	// Note: this macro should do a 'return' with the appropriate
	// interrupt number if such an extended interrupt exists.  The
	// assumption is that the line after the macro starts 'normal' processing.
	HAL_EXTENDED_IRQ_HANDLER(index);
#endif

	return CYGNUM_HAL_INTERRUPT_NONE; // This shouldn't happen!
}

//
// Interrupt control
//

void hal_interrupt_mask(int vector)
{
//    diag_printf("6hal_interrupt_mask(vector=%d) \n", vector);
#ifdef HAL_EXTENDED_INTERRUPT_MASK
	// Use platform specific handling, if defined
	// Note: this macro should do a 'return' for "extended" values of 'vector'
	// Normal vectors are handled by code subsequent to the macro call.
	HAL_EXTENDED_INTERRUPT_MASK(vector);
#endif
}

void hal_interrupt_unmask(int vector)
{
//    diag_printf("7hal_interrupt_unmask(vector=%d) \n", vector);

#ifdef HAL_EXTENDED_INTERRUPT_UNMASK
	// Use platform specific handling, if defined
	// Note: this macro should do a 'return' for "extended" values of 'vector'
	// Normal vectors are handled by code subsequent to the macro call.
	HAL_EXTENDED_INTERRUPT_UNMASK(vector);
#endif
}

void hal_interrupt_acknowledge(int vector)
{

//    diag_printf("8hal_interrupt_acknowledge(vector=%d) \n", vector);
#ifdef HAL_EXTENDED_INTERRUPT_UNMASK
	// Use platform specific handling, if defined
	// Note: this macro should do a 'return' for "extended" values of 'vector'
	// Normal vectors are handled by code subsequent to the macro call.
	HAL_EXTENDED_INTERRUPT_ACKNOWLEDGE(vector);
#endif
}

void hal_interrupt_configure(int vector, int level, int up)
{

#ifdef HAL_EXTENDED_INTERRUPT_CONFIGURE
	// Use platform specific handling, if defined
	// Note: this macro should do a 'return' for "extended" values of 'vector'
	// Normal vectors are handled by code subsequent to the macro call.
	HAL_EXTENDED_INTERRUPT_CONFIGURE(vector, level, up);
#endif
}

void hal_interrupt_set_level(int vector, int level)
{

#ifdef HAL_EXTENDED_INTERRUPT_SET_LEVEL
	// Use platform specific handling, if defined
	// Note: this macro should do a 'return' for "extended" values of 'vector'
	// Normal vectors are handled by code subsequent to the macro call.
	HAL_EXTENDED_INTERRUPT_SET_LEVEL(vector, level);
#endif

	// Interrupt priorities are not configurable.
}

#ifdef MXCFLASH_SELECT_NAND
unsigned int mxc_nfc_soc_setup(unsigned int pg_sz, unsigned int io_sz,
							unsigned int is_mlc, unsigned int num_of_chips)
{
	unsigned int tmp;

	tmp = readl(NFC_FLASH_CONFIG2_REG);

	/* Set the ST_CMD to be 0x70 for all NAND devices */
	tmp &= ~(0xFF << 24);
	tmp |= 0x70 << 24;
	/* spare size = 64 byte */
	tmp = (tmp & ~(0xff << 16)) | ((64 / 2) << 16);

	/* Set the Page Size */
	tmp &= ~0x3;
	switch (pg_sz) {
	case 512:
		tmp |= 0x0;
		break;

	case 2048:
		tmp |= 0x1;
		break;

	case 4096:
	default:
		tmp |= 0x2;
		break;
	}

	/* set ECC_MODE to 4bit ECC */
	tmp = (tmp & ~(3 << 6)) | (0 << 6);
	/* set pages/block to 64 */
	tmp = (tmp & ~(3 << 8)) | (1 << 8);

	/* Set the number of addr phases & ECC mode to default value */
	tmp &= ~(0x3 << 12);
	tmp |= (0x2 << 12) | 0x038;
	writel(tmp, NFC_FLASH_CONFIG2_REG);

	tmp = readl(NFC_FLASH_CONFIG3_REG);

	/* Set the No SDMA bit */
	tmp |= 0x1 << 20;

	/* Set the Status Busy Bit to 0x6 (default) */
	tmp &= ~(0x7 << 8);
	tmp |= 0x6 << 8;

	/* Set the Flash Width */
	if (io_sz == MXC_NAND_16_BIT) {
		tmp &= ~(1 << 3);
	} else {
		tmp |= 1 << 3;
	}

	/* Set the Number of Nand Chips */
	tmp &= ~(0x7 << 12);
	tmp |= (num_of_chips - 1) << 12;
	if (num_of_chips > 1)
		tmp |= 0x1;

#if 0
	/* STATUS_SAMP_SEL */
	tmp |= 1 << 19;
#endif
	writel(tmp, NFC_FLASH_CONFIG3_REG);

	return MXC_NFC_V3;
}
#endif

int mx53_iomux_setup(iomux_v3_cfg_t pad)
{
#if 0
	diag_printf("pad=%016llx MUX_OFS=%03x PAD_OFS=%03x INP_SEL_OFS=%03x MUX_MODE=%02x PAD_CTL=%04x INP_SEL=%x PAD_CTL_VALID=%d\n",
				pad, IOMUX_MUX_CTRL_OFS(pad), IOMUX_PAD_CTRL_OFS(pad),
				IOMUX_INP_SEL_OFS(pad), IOMUX_MUX_MODE(pad), IOMUX_PAD_CTRL(pad),
				IOMUX_INP_SEL(pad), IOMUX_PAD_CTRL_VALID(pad));
	if (readl(IOMUXC_BASE_ADDR + IOMUX_MUX_CTRL_OFS(pad)) !=
		IOMUX_MUX_MODE(pad)) {
		diag_printf("Changing IOMUX[%03x] from %02x to %02x\n",
					IOMUX_MUX_CTRL_OFS(pad),
					readl(IOMUXC_BASE_ADDR + IOMUX_MUX_CTRL_OFS(pad)),
					IOMUX_MUX_MODE(pad));
	}
#endif
	writel(IOMUX_MUX_MODE(pad),
		IOMUXC_BASE_ADDR + IOMUX_MUX_CTRL_OFS(pad));

	if (IOMUX_PAD_CTRL_VALID(pad)) {
#if 0
		if (readl(IOMUXC_BASE_ADDR + IOMUX_PAD_CTRL_OFS(pad)) !=
			IOMUX_PAD_CTRL(pad)) {
			diag_printf("Changing PAD_CTRL[%03x] from %04x to %04x\n",
						IOMUX_PAD_CTRL_OFS(pad),
						readl(IOMUXC_BASE_ADDR + IOMUX_PAD_CTRL_OFS(pad)),
						IOMUX_PAD_CTRL(pad));
		}
#endif
		writel(IOMUX_PAD_CTRL(pad), IOMUXC_BASE_ADDR + IOMUX_PAD_CTRL_OFS(pad));
	}
	if (IOMUX_INP_SEL_OFS(pad)) {
#if 0
		if (readl(IOMUXC_BASE_ADDR + IOMUX_INP_SEL_OFS(pad)) !=
			IOMUX_INP_SEL(pad)) {
			diag_printf("Changing INP_SEL[%03x] from %x to %x\n",
						IOMUX_INP_SEL_OFS(pad),
						readl(IOMUXC_BASE_ADDR + IOMUX_INP_SEL_OFS(pad)),
						IOMUX_INP_SEL(pad));
		}
#endif
		writel(IOMUX_INP_SEL(pad), IOMUXC_BASE_ADDR + IOMUX_INP_SEL_OFS(pad));
	}
	return 0;
}

int mx53_iomux_setup_pads(iomux_v3_cfg_t *pad, int num_pads)
{
	int ret = 0;
	int i;

	for (i = 0; i < num_pads; i++) {
		ret = mx53_iomux_setup(pad[i]);
		if (ret)
			break;
	}
	return ret;
}

static void show_sys_info(void)
{
	cyg_uint32 sbmr = readl(SRC_BASE_ADDR + 0x4);
	cyg_uint32 srsr = readl(SRC_BASE_ADDR + 0x8);
	const char *dlm = "";
	int bt_mem = (SBMR_BOOT_CFG1(sbmr) & (0xf << 4)) >> 4;

	if (find_correct_chip == CHIP_VERSION_UNKNOWN) {
		diag_printf("Unrecognized chip version: 0x%08x!\n", read_system_rev());
		diag_printf("Assuming chip version=0x%08x\n", system_rev);
	} else if (find_correct_chip == CHIP_VERSION_NONE) {
		diag_printf("Unrecognized chip: 0x%08x!!!\n", readl(IIM_BASE_ADDR + IIM_PREV_OFF));
	}

	diag_printf("Reset reason: ");

	if (srsr & (1 << 0)) {
		diag_printf("%sPOWER_ON", dlm);
		dlm = " | ";
	}
	if (srsr & (1 << 2)) {
		diag_printf("%sCSU", dlm);
		dlm = " | ";
	}
	if (srsr & (1 << 3)) {
		diag_printf("%sUSER", dlm);
		dlm = " | ";
	}
	if (srsr & (1 << 4)) {
		CYG_WORD16 wrsr;

		HAL_READ_UINT16(WDOG_BASE_ADDR + 4, wrsr);
		if (wrsr & (1 << 0)) {
			diag_printf("%sSOFT", dlm);
			dlm = " | ";
		}
		if (wrsr & (1 << 1)) {
			diag_printf("%sWATCHDOG", dlm);
			dlm = " | ";
		}
	}
	if (srsr & (1 << 5)) {
		diag_printf("%sJTAG_HW", dlm);
		dlm = " | ";
	}
	if (srsr & (1 << 6)) {
		diag_printf("%sJTAG_SW", dlm);
		dlm = " | ";
	}
	if (srsr & (1 << 16)) {
		diag_printf("%sWARM BOOT", dlm);
		dlm = " | ";
	}

	if (*dlm == '\0') {
		diag_printf("UNKNOWN: %08x\n", srsr);
	} else {
		diag_printf(" RESET\n");
	}

	diag_printf("BOOT_MODE:   ");
	switch (SBMR_BMOD(sbmr)) {
	case 0:
		diag_printf("Internal Boot (from %s)\n",
					SBMR_BT_FUSE_SEL(sbmr) ? "FUSES" : "GPIO");
		break;

	case 1:
		diag_printf("FSL Test Mode\n");
		break;

	case 2:
		diag_printf("Internal Boot (from %s)\n",
					SBMR_BT_FUSE_SEL(sbmr) ? "FUSES" : "UART/USB");
		break;

	case 3:
		diag_printf("Serial Boot Loader\n");
	}
	diag_printf("Boot Medium: ");
	switch (SBMR_BMOD(sbmr)) {
	case 0:
	case 2:
		if (bt_mem == 0) {
			diag_printf("WEIM %s\n", SBMR_BOOT_CFG1(sbmr) & (1 << 3) ?
						"NOR" : "OneNAND");
		} else if (bt_mem == 2) {
			diag_printf("HD %sATA\n", SBMR_BOOT_CFG1(sbmr) & (1 << 3) ?
						"S" : "P");
		} else if (bt_mem == 3) {
			diag_printf("Serial ROM %s\n", SBMR_BOOT_CFG1(sbmr) & (1 << 3) ?
						"SPI" : "I2C");
		} else if ((bt_mem & ~1) == 4) {
			diag_printf("SD %s speed\n", SBMR_BOOT_CFG1(sbmr) & (1 << 3) ?
						"low" : "high");
		} else if ((bt_mem & ~1) == 6) {
			diag_printf("MMC %s speed\n", SBMR_BOOT_CFG1(sbmr) & (1 << 3) ?
						"low" : "high");
		} else if (bt_mem & (1 << 3)) {
			diag_printf("NAND\n");
		} else {
			diag_printf("UNKNOWN: 0x%x\n", bt_mem);
		}
		break;

	case 1:
		diag_printf("UNKNOWN\n");
		break;

	case 3:
		diag_printf("UART/USB\n");
	}
	diag_printf("Boot Clock:  %d MHz\n",
				(SBMR_BOOT_CFG1(sbmr) & (1 << 1)) ? 400 : 800);
	diag_printf("OSC Freq:    %s\n",
				(SBMR_BOOT_CFG2(sbmr) & (1 << 3)) ? " 24 MHz" : "auto");
	diag_printf("PLL2 Freq:   %d MHz\n",
				(SBMR_BOOT_CFG2(sbmr) & (1 << 4)) ? 333 : 400);
	diag_printf("Secure Boot: %s\n",
				(SBMR_BOOT_CFG2(sbmr) & (2 << 0)) ? "On" : "Off");
}

RedBoot_init(show_sys_info, RedBoot_INIT_LAST);
