//==========================================================================
//
//      cmds.c
//
//      SoC [platform] specific RedBoot commands
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
#include <redboot.h>
#include <cyg/hal/hal_intr.h>
#include <cyg/hal/plf_mmap.h>
#include <cyg/hal/hal_soc.h>		 // Hardware definitions
#include <cyg/hal/hal_cache.h>
#include CYGBLD_HAL_PLF_DEFS_H

#include "hab_super_root.h"

#ifndef FUSE_PROG_START
#define FUSE_PROG_START()		CYG_EMPTY_STATEMENT
#define FUSE_PROG_DONE()		CYG_EMPTY_STATEMENT
#endif

//#define IIM_FUSE_DEBUG
//#define CMD_CLOCK_DEBUG
#ifdef CMD_CLOCK_DEBUG
static int dbg_enable;

#define enable_dbg()		\
  CYG_MACRO_START			\
  dbg_enable = 1;			\
  CYG_MACRO_END

#define dbg(fmt...)								\
  CYG_MACRO_START								\
  if (dbg_enable)								\
	  diag_printf(fmt);							\
  CYG_MACRO_END
#else
#define dbg_enable 0
#define enable_dbg()		CYG_EMPTY_STATEMENT
#define dbg(fmt...)		CYG_EMPTY_STATEMENT
#endif

static int gcd(int m, int n);

typedef unsigned long long	u64;
typedef unsigned int		u32;
typedef unsigned short		u16;
typedef unsigned char		u8;

#define SZ_DEC_1M		1000000
#define PLL_PD_MAX		16		//actual pd+1
#define PLL_MFI_MAX		15
#define PLL_MFI_MIN		5
#define ARM_DIV_MAX		8
#define IPG_DIV_MAX		4
#define AHB_DIV_MAX		8
#define EMI_DIV_MAX		8
#define NFC_DIV_MAX		8

struct pll_param {
	u32 pd;
	u32 mfi;
	u32 mfn;
	u32 mfd;
};

#define PLL_FREQ_MAX(_ref_clk_)	   (4 * _ref_clk_ * PLL_MFI_MAX)
#define PLL_FREQ_MIN(_ref_clk_)	   ((4 * _ref_clk_ * PLL_MFI_MIN) / PLL_PD_MAX)
#define MAX_DDR_CLK		400000000
#define AHB_CLK_MAX		133333333
#define IPG_CLK_MAX		(AHB_CLK_MAX / 2)
#define NFC_CLK_MAX		25000000
// IPU-HSP clock is independent of the HCLK and can go up to 177MHz but requires
// higher voltage support. For simplicity, limit it to 133MHz
#define HSP_CLK_MAX		133333333

#define ERR_WRONG_CLK	(-1)
#define ERR_NO_MFI		(-2)
#define ERR_NO_MFN		(-3)
#define ERR_NO_PD		(-4)
#define ERR_NO_PRESC	(-5)
#define ERR_NO_AHB_DIV	(-6)

u32 pll_clock(enum plls pll);
u32 get_main_clock(enum main_clocks clk);
u32 get_peri_clock(enum peri_clocks clk);

static volatile u32 *pll_base[] =
{
	&REG32(PLL1_BASE_ADDR),
	&REG32(PLL2_BASE_ADDR),
	&REG32(PLL3_BASE_ADDR),
	&REG32(PLL4_BASE_ADDR),
};

static void clock_setup(int argc, char *argv[]);

RedBoot_cmd("clock",
			"Setup/Display clock\nSyntax:",
			"[<core clock in MHz> :<DDR clock in MHz>]\n\n"
			"   Examples:\n"
			"   [clock]         -> Show various clocks\n"
			"   [clock 665]     -> Core=665\n"
			"   [clock 800:133] -> Core=800 DDR=133\n"
			"   [clock :166]    -> Core=no change DDR=166\n",
			clock_setup
	);

/*!
 * This is to calculate various parameters based on reference clock and
 * targeted clock based on the equation:
 *      t_clk = 2*ref_freq*(mfi + mfn/(mfd+1))/(pd+1)
 * This calculation is based on a fixed MFD value for simplicity.
 *
 * @param ref       reference clock freq in Hz
 * @param target    targeted clock in Hz
 * @param p_pd      calculated pd value (pd value from register + 1) upon return
 * @param p_mfi     calculated actual mfi value upon return
 * @param p_mfn     calculated actual mfn value upon return
 * @param p_mfd     fixed mfd value (mfd value from register + 1) upon return
 *
 * @return          0 if successful; non-zero otherwise.
 */
int calc_pll_params(u32 ref, u32 target, struct pll_param *pll)
{
	int pd, mfi = 1, mfn, mfd, i;
	u64 n_target = target, n_ref = ref;

	// make sure targeted freq is in the valid range. Otherwise the
	// following calculation might be wrong!!!
	if (n_target < PLL_FREQ_MIN(ref) || n_target > PLL_FREQ_MAX(ref))
		return ERR_WRONG_CLK;

	mfd = 24 * 16;

	// Use n_target and n_ref to avoid overflow
	for (pd = 1; pd <= PLL_PD_MAX; pd++) {
		mfi = (n_target * pd) / (4 * n_ref);
		if (mfi > PLL_MFI_MAX) {
			return ERR_NO_MFI;
		} else if (mfi < 5) {
			continue;
		}
		break;
	}
	// Now got pd and mfi already
	mfn = (((n_target * pd) / 4 - n_ref * mfi) * mfd) / n_ref;

	dbg("%d: ref=%d, target=%d, pd=%d, mfi=%d, mfn=%d, mfd=%d\n",
		__LINE__, ref, target, pd, mfi, mfn, mfd);

	if (mfn != 0) {
		i = gcd(mfd, mfn);
		mfn /= i;
		mfd /= i;
	} else {
		mfd = 1;
	}
	pll->pd = pd;
	pll->mfi = mfi;
	pll->mfn = mfn;
	pll->mfd = mfd;
	return 0;
}

/*!
 * This function returns the low power audio clock.
 */
u32 get_lp_apm(void)
{
	u32 ret_val;
	u32 ccsr = readl(CCM_BASE_ADDR + CLKCTL_CCSR);

	if (((ccsr >> 9) & 1) == 0) {
		ret_val = FREQ_24MHZ;
	} else {
		ret_val = FREQ_32768HZ;
	}
	dbg("%s: CCSR[%08lx]=%08x freq=%u.%03uMHz\n", __FUNCTION__,
		CCM_BASE_ADDR + CLKCTL_CCSR, ccsr, ret_val / 1000000, ret_val / 1000 % 1000);
	return ret_val;
}

/*!
 * This function returns the periph_clk.
 */
u32 get_periph_clk(void)
{
	u32 ret_val, clk_sel;

	u32 cbcdr = readl(CCM_BASE_ADDR + CLKCTL_CBCDR);
	u32 cbcmr = readl(CCM_BASE_ADDR + CLKCTL_CBCMR);

	if (!(cbcdr & (1 << 25))) {
		ret_val = pll_clock(PLL2);
		dbg("%s: CBCDR[%08lx]=%08x CBCMR[%08lx]=%08x freq=%u.%03uMHz\n", __FUNCTION__,
			CCM_BASE_ADDR + CLKCTL_CBCDR, cbcdr,
			CCM_BASE_ADDR + CLKCTL_CBCMR, cbcmr,
			ret_val / 1000000, ret_val / 1000 % 1000);
	} else {
		clk_sel = (cbcmr >> 12) & 3;
		if (clk_sel == 0) {
			ret_val = pll_clock(PLL1);
		} else if (clk_sel == 1) {
			ret_val = pll_clock(PLL3);
		} else if (clk_sel == 2) {
			ret_val = get_lp_apm();
		} else {
			diag_printf("Invalid CBCMR[CLK_SEL]: %d\n", clk_sel);
			return ERR_WRONG_CLK;
		}
		dbg("%s: CBCDR[%08lx]=%08x CBCMR[%08lx]=%08x clk_sel=%d freq=%u.%03uMHz\n", __FUNCTION__,
			CCM_BASE_ADDR + CLKCTL_CBCDR, cbcdr,
			CCM_BASE_ADDR + CLKCTL_CBCMR, cbcmr,
			clk_sel, ret_val / 1000000, ret_val / 1000 % 1000);
	}
	return ret_val;
}

/*!
 * This function assumes the expected core clock has to be changed by
 * modifying the PLL. This is NOT true always but for most of the times,
 * it is. So it assumes the PLL output freq is the same as the expected
 * core clock (presc=1) unless the core clock is less than PLL_FREQ_MIN.
 * In the latter case, it will try to increase the presc value until
 * (presc*core_clk) is greater than PLL_FREQ_MIN. It then makes call to
 * calc_pll_params() and obtains the values of PD, MFI,MFN, MFD based
 * on the targeted PLL and reference input clock to the PLL. Lastly,
 * it sets the register based on these values along with the dividers.
 * Note 1) There is no value checking for the passed-in divider values
 *         so the caller has to make sure those values are sensible.
 *      2) Also adjust the NFC divider such that the NFC clock doesn't
 *         exceed NFC_CLK_MAX.
 *      3) IPU HSP clock is independent of AHB clock. Even it can go up to
 *         177MHz for higher voltage, this function fixes the max to 133MHz.
 *      4) This function should not have allowed diag_printf() calls since
 *         the serial driver has been stopped. But leave then here to allow
 *         easy debugging by NOT calling the cyg_hal_plf_serial_stop().
 *
 * @param ref       pll input reference clock (24MHz)
 * @param core_clk  core clock in Hz
 * @param emi_clk   emi clock in Hz
 # @return          0 if successful; non-zero otherwise
 */
int configure_clock(u32 ref, u32 core_clk, u32 emi_clk)
{
	u32 pll, clk_src;
	struct pll_param pll_param;
	int ret, clk_sel, div = 1, div_core = 1, div_per = 1, shift = 0;
	u32 cbcdr = readl(CCM_BASE_ADDR + CLKCTL_CBCDR);
	u32 cbcmr = readl(CCM_BASE_ADDR + CLKCTL_CBCMR);
	u32 ccsr = readl(CCM_BASE_ADDR + CLKCTL_CCSR);
	u32 icgc = readl(PLATFORM_BASE_ADDR + PLATFORM_ICGC);

	dbg("%s: cbcdr[%08lx]=%08x\n", __FUNCTION__,
		CCM_BASE_ADDR + CLKCTL_CBCDR, cbcdr);
	dbg("%s: cbcmr[%08lx]=%08x\n", __FUNCTION__,
		CCM_BASE_ADDR + CLKCTL_CBCMR, cbcdr);
	dbg("%s: ccsr[%08lx]=%08x\n", __FUNCTION__,
		CCM_BASE_ADDR + CLKCTL_CCSR, cbcdr);
	dbg("%s: icgc[%08lx]=%08x\n", __FUNCTION__,
		PLATFORM_BASE_ADDR + PLATFORM_ICGC, icgc);

	if (core_clk != 0) {
		// assume pll default to core clock first
		pll = core_clk;
		if ((ret = calc_pll_params(ref, pll, &pll_param)) != 0) {
			diag_printf("can't find pll parameters: %d\n", ret);
			return ret;
		}

		dbg("%s: ref=%d, pll=%d, pd=%d, mfi=%d,mfn=%d, mfd=%d\n", __FUNCTION__,
			ref, pll, pll_param.pd, pll_param.mfi, pll_param.mfn, pll_param.mfd);

		/* Applies for TO 2 only */
		if (((cbcdr >> 30) & 0x1) == 0x1) {
			/* Disable IPU and HSC dividers */
			writel(0x60000, CCM_BASE_ADDR + CLKCTL_CCDR);
			/* Switch DDR to different source */
			writel(cbcdr & ~0x40000000, CCM_BASE_ADDR + CLKCTL_CBCDR);
			while (readl(CCM_BASE_ADDR + CLKCTL_CDHIPR) != 0);
			writel(0x0, CCM_BASE_ADDR + CLKCTL_CCDR);
		}

		/* Switch ARM to PLL2 clock */
		writel(ccsr | 0x4, CCM_BASE_ADDR + CLKCTL_CCSR);

		if ((core_clk > 665000000) && (core_clk <= 800000000)) {
			div_per = 5;
		} else if (core_clk > 800000000) {
			div_per = 6;
		} else {
			div_per = 4;
		}

		if (core_clk > 800000000) {
			div_core = 3;
		} else {
			div_core = 2;
		}
		ret = adjust_core_voltage(core_clk / 1000000);
		if (ret) {
			diag_printf("Failed to adjust core voltage for %u MHz\n",
						core_clk / 1000000);
			return ret;
		}
		cyg_hal_plf_serial_stop();

		// adjust pll settings
		writel(((pll_param.pd - 1) << 0) | (pll_param.mfi << 4),
			PLL1_BASE_ADDR + PLL_DP_OP);
		writel(pll_param.mfn, PLL1_BASE_ADDR + PLL_DP_MFN);
		writel(pll_param.mfd - 1, PLL1_BASE_ADDR + PLL_DP_MFD);
		writel(((pll_param.pd - 1) << 0) | (pll_param.mfi << 4),
			PLL1_BASE_ADDR + PLL_DP_HFS_OP);
		writel(pll_param.mfn, PLL1_BASE_ADDR + PLL_DP_HFS_MFN);
		writel(pll_param.mfd - 1, PLL1_BASE_ADDR + PLL_DP_HFS_MFD);

		icgc &= ~0x77;
		icgc |= div_core << 4;
		icgc |= div_per;
		/* Set the platform clock dividers */
		writel(icgc, PLATFORM_BASE_ADDR + PLATFORM_ICGC);
		/* Switch ARM back to PLL1 */
		writel((ccsr & ~0x4), CCM_BASE_ADDR + CLKCTL_CCSR);
		/* Applies for TO 2 only */
		if (((cbcdr >> 30) & 0x1) == 0x1) {
			/* Disable IPU and HSC dividers */
			writel(0x60000, CCM_BASE_ADDR + CLKCTL_CCDR);
			/* Switch DDR back to PLL1 */
			writel(cbcdr | 0x40000000, CCM_BASE_ADDR + CLKCTL_CBCDR);
			while (readl(CCM_BASE_ADDR + CLKCTL_CDHIPR) != 0);
			writel(0x0, CCM_BASE_ADDR + CLKCTL_CCDR);
			if (emi_clk == 0) {
				/* Keep EMI clock to the max if not specified */
				emi_clk = 200000000;
			}
		}
		cyg_hal_plf_serial_init();
	}

	if (emi_clk != 0) {
		/* Applies for TO 2 only */
		if (((cbcdr >> 30) & 0x1) == 0x1) {
			clk_src = pll_clock(PLL1);
			shift = 27;
		} else {
			clk_src = get_periph_clk();
			/* Find DDR clock input */
			clk_sel = (cbcmr >> 10) & 0x3;
			if (clk_sel == 0) {
				shift = 16;
			} else if (clk_sel == 1) {
				shift = 19;
			} else if (clk_sel == 2) {
				shift = 22;
			} else if (clk_sel == 3) {
				shift = 10;
			}
		}
		if ((clk_src % emi_clk) == 0)
			div = clk_src / emi_clk;
		else
			div = (clk_src / emi_clk) + 1;
		if (div > 8)
			div = 8;

		cbcdr &= ~(0x7 << shift);
		cbcdr |= (div - 1) << shift;

		dbg("%s@%d: \n", __FUNCTION__, __LINE__);

		/* Disable IPU and HSC dividers */
		writel(0x60000, CCM_BASE_ADDR + CLKCTL_CCDR);
		writel(cbcdr, CCM_BASE_ADDR + CLKCTL_CBCDR);
		while (readl(CCM_BASE_ADDR + CLKCTL_CDHIPR) != 0);
		writel(0x0, CCM_BASE_ADDR + CLKCTL_CCDR);
	}
	return 0;
}

static void clock_setup(int argc,char *argv[])
{
	u32 i, core_clk, ddr_clk, data[3];
	unsigned long temp;
	int ret;

	if (argc == 1)
		goto print_clock;

	enable_dbg();
	for (i = 0; i < 2; i++) {
		if (!parse_num(argv[1], &temp, &argv[1], ":")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}
		data[i] = temp;
	}

	core_clk = data[0] * SZ_DEC_1M;
	ddr_clk = data[1] * SZ_DEC_1M;

	if (core_clk != 0) {
		if ((core_clk < PLL_FREQ_MIN(PLL_REF_CLK)) || (core_clk > PLL_FREQ_MAX(PLL_REF_CLK))) {
			diag_printf("Targeted core clock should be within [%d - %d] MHz\n",
						PLL_FREQ_MIN(PLL_REF_CLK) / SZ_DEC_1M,
						PLL_FREQ_MAX(PLL_REF_CLK) / SZ_DEC_1M);
			return;
		}
	}

	if (ddr_clk != 0) {
		if (ddr_clk > MAX_DDR_CLK) {
			diag_printf("DDR clock should be less than %d MHz, assuming max value\n",
						MAX_DDR_CLK / SZ_DEC_1M);
			ddr_clk = MAX_DDR_CLK;
		}
	}

	// adjust the clock
	ret = configure_clock(PLL_REF_CLK, core_clk, ddr_clk);
	if (ret != 0) {
		diag_printf("Failed to setup clock: %d\n", ret);
		return;
	}
	diag_printf("\n<<<New clock setting>>>\n");

	// Now printing clocks
print_clock:

	diag_printf("\nPLL1\t\tPLL2\t\tPLL3\t\tPLL4\n");
	diag_printf("========================================================\n");
	diag_printf("%-16d%-16d%-16d%-16d\n\n", pll_clock(PLL1), pll_clock(PLL2),
				pll_clock(PLL3), pll_clock(PLL4));
	diag_printf("AXI_A\t\tAXI_B\t\tEMI_SLOW_CLK\n");
	diag_printf("========================================================\n");
	diag_printf("%-16d%-16d%-16d\n\n",
				get_main_clock(AXI_A_CLK),
				get_main_clock(AXI_B_CLK),
				get_main_clock(EMI_SLOW_CLK));
	diag_printf("CPU\t\tAHB\t\tIPG\t\tDDR_CLK\n");
	diag_printf("========================================================\n");
	diag_printf("%-16d%-16d%-16d%-16d\n\n",
				get_main_clock(CPU_CLK),
				get_main_clock(AHB_CLK),
				get_main_clock(IPG_CLK),
				get_main_clock(DDR_CLK));

	diag_printf("NFC\t\tUSB\t\tIPG_PER_CLK\n");
	diag_printf("========================================\n");
	diag_printf("%-16d%-16d%-16d\n\n",
				get_main_clock(NFC_CLK),
				get_main_clock(USB_CLK),
				get_main_clock(IPG_PER_CLK));

	diag_printf("UART1-3\t\tSSI1\t\tSSI2\t\tSPI\n");
	diag_printf("===========================================");
	diag_printf("=============\n");

	diag_printf("%-16d%-16d%-16d%-16d\n\n",
				get_peri_clock(UART1_BAUD),
				get_peri_clock(SSI1_BAUD),
				get_peri_clock(SSI2_BAUD),
				get_peri_clock(SPI1_CLK));

#if 0
	diag_printf("IPG_PERCLK as baud clock for: UART1-5, I2C, OWIRE, SDHC");
	if (((readl(EPIT1_BASE_ADDR) >> 24) & 0x3) == 0x2) {
		diag_printf(", EPIT");
	}
	if (((readl(GPT1_BASE_ADDR) >> 6) & 0x7) == 0x2) {
		diag_printf(", GPT");
	}
#endif
	diag_printf("\n");

}

/*!
 * This function returns the PLL output value in Hz based on pll.
 */
u32 pll_clock(enum plls pll)
{
	u64 ref_clk;
	u32 mfi, mfn, mfd, pdf, pll_out;
	int sign;
	u32 dp_ctrl, dp_op, dp_mfd, dp_mfn;
	int clk_sel;
	int dbl;

	dp_ctrl = pll_base[pll][PLL_DP_CTL >> 2];
	clk_sel = MXC_GET_FIELD(dp_ctrl, 2, 8);
	ref_clk = PLL_REF_CLK;

	dbg("clk_sel=%d\n", clk_sel);

	if ((pll_base[pll][PLL_DP_CTL >> 2] & 0x80) == 0) {
		dp_op = pll_base[pll][PLL_DP_OP >> 2];
		dp_mfd = pll_base[pll][PLL_DP_MFD >> 2];
		dp_mfn = pll_base[pll][PLL_DP_MFN >> 2];
	} else {
		dp_op = pll_base[pll][PLL_DP_HFS_OP >> 2];
		dp_mfd = pll_base[pll][PLL_DP_HFS_MFD >> 2];
		dp_mfn = pll_base[pll][PLL_DP_HFS_MFN >> 2];
	}
	pdf = dp_op & 0xF;
	mfi = (dp_op >> 4) & 0xF;
	mfi = (mfi <= 5) ? 5: mfi;
	mfd = dp_mfd & 0x07FFFFFF;
	mfn = dp_mfn & 0x07FFFFFF;

	sign = (mfn < 0x4000000) ? 1 : -1;
	mfn = (mfn < 0x4000000) ? mfn : (0x8000000 - mfn);

	dbl = 2 * (((dp_ctrl >> 12) & 0x1) + 1);

	dbg("%s: ref=%llu.%03lluMHz, dbl=%d, pd=%d, mfi=%d, mfn=%d, mfd=%d\n",
		__FUNCTION__, ref_clk / 1000000, ref_clk / 1000 % 1000,
		dbl, pdf + 1, mfi, sign * mfn, mfd + 1);

	pll_out = (dbl * ref_clk * mfi + dbl * ref_clk * sign * mfn / (mfd + 1)) /
		(pdf + 1);

	return pll_out;
}

/*!
 * This function returns the emi_core_clk_root clock.
 */
u32 get_emi_core_clk(void)
{
	u32 cbcdr = readl(CCM_BASE_ADDR + CLKCTL_CBCDR);
	u32 clk_sel, max_pdf, peri_clk, ahb_clk;
	u32 ret_val;

	max_pdf = (cbcdr >> 10) & 0x7;
	peri_clk = get_periph_clk();
	ahb_clk = peri_clk / (max_pdf + 1);

	clk_sel = (cbcdr >> 26) & 1;
	if (clk_sel == 0) {
		ret_val = peri_clk;
	} else {
		ret_val = ahb_clk ;
	}
	dbg("%s: CBCDR[%08lx]=%08x freq=%u.%03uMHz\n", __FUNCTION__,
		CCM_BASE_ADDR + CLKCTL_CBCDR, cbcdr, ret_val / 1000000, ret_val / 1000 % 1000);
	return ret_val;
}

/*!
 * This function returns the main clock value in Hz.
 */
u32 get_main_clock(enum main_clocks clk)
{
	u32 pdf, max_pdf, ipg_pdf, nfc_pdf, clk_sel;
	u32 pll, ret_val;
	u32 cacrr = readl(CCM_BASE_ADDR + CLKCTL_CACRR);
	u32 cbcdr = readl(CCM_BASE_ADDR + CLKCTL_CBCDR);
	u32 cbcmr = readl(CCM_BASE_ADDR + CLKCTL_CBCMR);
	u32 cscmr1 = readl(CCM_BASE_ADDR + CLKCTL_CSCMR1);
	u32 cscdr1 = readl(CCM_BASE_ADDR + CLKCTL_CSCDR1);

	dbg("%s: \n", __FUNCTION__);
	switch (clk) {
	case CPU_CLK:
		pdf = cacrr & 0x7;
		pll = pll_clock(PLL1);
		ret_val = pll / (pdf + 1);
		break;

	case AHB_CLK:
		max_pdf = (cbcdr >> 10) & 0x7;
		pll = get_periph_clk();
		ret_val = pll / (max_pdf + 1);
		break;

	case AXI_A_CLK:
		pdf = (cbcdr >> 16) & 0x7;
		pll = get_periph_clk();
		ret_val = pll / (pdf + 1);
		break;

	case AXI_B_CLK:
		pdf = (cbcdr >> 19) & 0x7;
		pll = get_periph_clk();
		ret_val = pll / (pdf + 1);
		break;

	case EMI_SLOW_CLK:
		pll = get_emi_core_clk();
		pdf = (cbcdr >> 22) & 0x7;
		ret_val = pll / (pdf + 1);
		break;

	case IPG_CLK:
		max_pdf = (cbcdr >> 10) & 0x7;
		ipg_pdf = (cbcdr >> 8) & 0x3;
		pll = get_periph_clk();
		ret_val = pll / ((max_pdf + 1) * (ipg_pdf + 1));
		break;

	case IPG_PER_CLK:
		clk_sel = cbcmr & 1;
		if (clk_sel == 0) {
			clk_sel = (cbcmr >> 1) & 1;
			pdf = (((cbcdr >> 6) & 3) + 1) * (((cbcdr >> 3) & 7) + 1) * ((cbcdr & 7) + 1);
			if (clk_sel == 0) {
				ret_val = get_periph_clk() / pdf;
			} else {
				ret_val = get_lp_apm();
			}
		} else {
			/* Same as IPG_CLK */
			max_pdf = (cbcdr >> 10) & 0x7;
			ipg_pdf = (cbcdr >> 8) & 0x3;
			pll = get_periph_clk();
			ret_val = pll / ((max_pdf + 1) * (ipg_pdf + 1));
		}
		break;

	case DDR_CLK:
		clk_sel = (cbcmr >> 10) & 3;
		pll = get_periph_clk();
		if (clk_sel == 0) {
			/* AXI A */
			pdf = (cbcdr >> 16) & 0x7;
		} else if (clk_sel == 1) {
			/* AXI B */
			pdf = (cbcdr >> 19) & 0x7;
		} else if (clk_sel == 2) {
			/* EMI SLOW CLOCK ROOT */
			pll = get_emi_core_clk();
			pdf = (cbcdr >> 22) & 0x7;
		} else if (clk_sel == 3) {
			/* AHB CLOCK */
			pdf = (cbcdr >> 10) & 0x7;
		}

		ret_val = pll / (pdf + 1);
		break;

	case NFC_CLK:
		pdf = (cbcdr >> 22) & 0x7;
		nfc_pdf = (cbcdr >> 13) & 0x7;
		pll = get_emi_core_clk();
		ret_val = pll / ((pdf + 1) * (nfc_pdf + 1));
		break;

	case USB_CLK:
		clk_sel = (cscmr1 >> 22) & 3;
		if (clk_sel == 0) {
			pll = pll_clock(PLL1);
		} else if (clk_sel == 1) {
			pll = pll_clock(PLL2);
		} else if (clk_sel == 2) {
			pll = pll_clock(PLL3);
		} else if (clk_sel == 3) {
			pll = get_lp_apm();
		}
		pdf = (cscdr1 >> 8) & 0x7;
		max_pdf = (cscdr1 >> 6) & 0x3;
		ret_val = pll / ((pdf + 1) * (max_pdf + 1));
		break;

	default:
		diag_printf("Unknown clock: %d\n", clk);
		return ERR_WRONG_CLK;
	}

	return ret_val;
}

/*!
 * This function returns the peripheral clock value in Hz.
 */
u32 get_peri_clock(enum peri_clocks clk)
{
	u32 ret_val = 0, pdf, pre_pdf, clk_sel;
	u32 cscmr1 = readl(CCM_BASE_ADDR + CLKCTL_CSCMR1);
	u32 cscdr1 = readl(CCM_BASE_ADDR + CLKCTL_CSCDR1);
	u32 cscdr2 = readl(CCM_BASE_ADDR + CLKCTL_CSCDR2);
	u32 cs1cdr = readl(CCM_BASE_ADDR + CLKCTL_CS1CDR);
	u32 cs2cdr = readl(CCM_BASE_ADDR + CLKCTL_CS2CDR);

	dbg("%s: \n", __FUNCTION__);
	switch (clk) {
	case UART1_BAUD:
	case UART2_BAUD:
	case UART3_BAUD:
		pre_pdf = (cscdr1 >> 3) & 0x7;
		pdf = cscdr1 & 0x7;
		clk_sel = (cscmr1 >> 24) & 3;
		if (clk_sel == 0) {
			ret_val = pll_clock(PLL1) / ((pre_pdf + 1) * (pdf + 1));
		} else if (clk_sel == 1) {
			ret_val = pll_clock(PLL2) / ((pre_pdf + 1) * (pdf + 1));
		} else if (clk_sel == 2) {
			ret_val = pll_clock(PLL3) / ((pre_pdf + 1) * (pdf + 1));
		} else {
			ret_val = get_lp_apm() / ((pre_pdf + 1) * (pdf + 1));
		}
		break;
	case SSI1_BAUD:
		pre_pdf = (cs1cdr >> 6) & 0x7;
		pdf = cs1cdr & 0x3F;
		clk_sel = (cscmr1 >> 14) & 3;
		if (clk_sel == 0) {
			ret_val = pll_clock(PLL1) / ((pre_pdf + 1) * (pdf + 1));
		} else if (clk_sel == 0x1) {
			ret_val = pll_clock(PLL2) / ((pre_pdf + 1) * (pdf + 1));
		} else if (clk_sel == 0x2) {
			ret_val = pll_clock(PLL3) / ((pre_pdf + 1) * (pdf + 1));
		} else {
			ret_val = CKIH /((pre_pdf + 1) * (pdf + 1));
		}
		break;
	case SSI2_BAUD:
		pre_pdf = (cs2cdr >> 6) & 0x7;
		pdf = cs2cdr & 0x3F;
		clk_sel = (cscmr1 >> 12) & 3;
		if (clk_sel == 0) {
			ret_val = pll_clock(PLL1) / ((pre_pdf + 1) * (pdf + 1));
		} else if (clk_sel == 0x1) {
			ret_val = pll_clock(PLL2) / ((pre_pdf + 1) * (pdf + 1));
		} else if (clk_sel == 0x2) {
			ret_val = pll_clock(PLL3) / ((pre_pdf + 1) * (pdf + 1));
		} else {
			ret_val = CKIH /((pre_pdf + 1) * (pdf + 1));
		}
		break;
	case SPI1_CLK:
	case SPI2_CLK:
		pre_pdf = (cscdr2 >> 25) & 0x7;
		pdf = (cscdr2 >> 19) & 0x3F;
		clk_sel = (cscmr1 >> 4) & 3;
		if (clk_sel == 0) {
			ret_val = pll_clock(PLL1) / ((pre_pdf + 1) * (pdf + 1));
		} else if (clk_sel == 1) {
			ret_val = pll_clock(PLL2) / ((pre_pdf + 1) * (pdf + 1));
		} else if (clk_sel == 2) {
			ret_val = pll_clock(PLL3) / ((pre_pdf + 1) * (pdf + 1));
		} else {
			ret_val = get_lp_apm() / ((pre_pdf + 1) * (pdf + 1));
		}
		break;
	default:
		diag_printf("%s(): This clock: %d not supported yet\n",
					__FUNCTION__, clk);
	}

	return ret_val;
}

#ifdef L2CC_ENABLED
/*
 * This command is added for some simple testing only. It turns on/off
 * L2 cache regardless of L1 cache state. The side effect of this is
 * when doing any flash operations such as "fis init", the L2
 * will be turned back on along with L1 caches even though it is off
 * by using this command.
 */
RedBoot_cmd("L2",
			"L2 cache",
			"[ON | OFF]",
			do_L2_caches
	);

void do_L2_caches(int argc, char *argv[])
{
	u32 oldints;

	if (argc == 2) {
		if (strcasecmp(argv[1], "on") == 0) {
			HAL_DISABLE_INTERRUPTS(oldints);
			HAL_ENABLE_L2();
			HAL_RESTORE_INTERRUPTS(oldints);
		} else if (strcasecmp(argv[1], "off") == 0) {
			HAL_DISABLE_INTERRUPTS(oldints);
			HAL_DCACHE_DISABLE_L1();
			HAL_CACHE_FLUSH_ALL();
			HAL_DISABLE_L2();
			HAL_DCACHE_ENABLE_L1();
			HAL_RESTORE_INTERRUPTS(oldints);
		} else {
			diag_printf("Invalid L2 cache mode: %s\n", argv[1]);
		}
	} else {
		int L2cache_on;

		HAL_L2CACHE_IS_ENABLED(L2cache_on);
		diag_printf("L2 cache: %s\n", L2cache_on ? "On" : "Off");
	}
}
#endif //L2CC_ENABLED

#define IIM_ERR_SHIFT		8
#define POLL_FUSE_PRGD		(IIM_STAT_PRGD | (IIM_ERR_PRGE << IIM_ERR_SHIFT))
#define POLL_FUSE_SNSD		(IIM_STAT_SNSD | (IIM_ERR_SNSE << IIM_ERR_SHIFT))

static void fuse_op_start(void)
{
	/* Do not generate interrupt */
	writel(0, IIM_BASE_ADDR + IIM_STATM_OFF);
	// clear the status bits and error bits
	writel(0x3, IIM_BASE_ADDR + IIM_STAT_OFF);
	writel(0xFE, IIM_BASE_ADDR + IIM_ERR_OFF);
}

/*
 * The action should be either:
 *          POLL_FUSE_PRGD
 * or:
 *          POLL_FUSE_SNSD
 */
static int poll_fuse_op_done(int action)
{
	u32 status, error;

	if (action != POLL_FUSE_PRGD && action != POLL_FUSE_SNSD) {
		diag_printf("%s(%d) invalid operation\n", __FUNCTION__, action);
		return -1;
	}

	/* Poll busy bit till it is NOT set */
	while ((readl(IIM_BASE_ADDR + IIM_STAT_OFF) & IIM_STAT_BUSY) != 0 ) {
	}

	/* Test for successful write */
	status = readl(IIM_BASE_ADDR + IIM_STAT_OFF);
	error = readl(IIM_BASE_ADDR + IIM_ERR_OFF);

	if ((status & action) != 0 && (error & (action >> IIM_ERR_SHIFT)) == 0) {
		if (error) {
			diag_printf("Even though the operation seems successful...\n");
			diag_printf("There are some error(s) at addr=0x%08lx: 0x%08x\n",
						(IIM_BASE_ADDR + IIM_ERR_OFF), error);
		}
		return 0;
	}
	diag_printf("%s(%d) failed\n", __FUNCTION__, action);
	diag_printf("status address=0x%08lx, value=0x%08x\n",
				(IIM_BASE_ADDR + IIM_STAT_OFF), status);
	diag_printf("There are some error(s) at addr=0x%08lx: 0x%08x\n",
				(IIM_BASE_ADDR + IIM_ERR_OFF), error);
	return -1;
}

unsigned int sense_fuse(int bank, int row, int bit)
{
	int addr, addr_l, addr_h, reg_addr;

	fuse_op_start();

	addr = ((bank << 11) | (row << 3) | (bit & 0x7));
	/* Set IIM Program Upper Address */
	addr_h = (addr >> 8) & 0x000000FF;
	/* Set IIM Program Lower Address */
	addr_l = (addr & 0x000000FF);

#ifdef IIM_FUSE_DEBUG
	diag_printf("%s: addr_h=0x%02x, addr_l=0x%02x\n",
				__FUNCTION__, addr_h, addr_l);
#endif
	writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
	writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
	/* Start sensing */
	writel(0x8, IIM_BASE_ADDR + IIM_FCTL_OFF);
	if (poll_fuse_op_done(POLL_FUSE_SNSD) != 0) {
		diag_printf("%s(bank: %d, row: %d, bit: %d failed\n",
					__FUNCTION__, bank, row, bit);
	}
	reg_addr = IIM_BASE_ADDR + IIM_SDAT_OFF;
	return readl(reg_addr);
}

void do_fuse_read(int argc, char *argv[])
{
	unsigned long bank, row;
	unsigned long fuse_val;

	if (argc == 1) {
		diag_printf("Usage: fuse_read <bank> <row>\n");
		return;
	} else if (argc == 3) {
		if (!parse_num(argv[1], &bank, &argv[1], " ")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}
		if (!parse_num(argv[2], &row, &argv[2], " ")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}

		diag_printf("Read fuse at bank:%ld row:%ld\n", bank, row);
		fuse_val = sense_fuse(bank, row, 0);
		diag_printf("fuses at (bank:%ld, row:%ld) = 0x%02lx\n", bank, row, fuse_val);
	} else {
		diag_printf("Passing in wrong arguments: %d\n", argc);
		diag_printf("Usage: fuse_read <bank> <row>\n");
	}
}

/* Blow fuses based on the bank, row and bit positions (all 0-based)
*/
static int fuse_blow(int bank, int row, int bit)
{
	int addr, addr_l, addr_h, ret = -1;

	fuse_op_start();

	/* Disable IIM Program Protect */
	writel(0xAA, IIM_BASE_ADDR + IIM_PREG_P_OFF);

	addr = ((bank << 11) | (row << 3) | (bit & 0x7));
	/* Set IIM Program Upper Address */
	addr_h = (addr >> 8) & 0x000000FF;
	/* Set IIM Program Lower Address */
	addr_l = (addr & 0x000000FF);

#ifdef IIM_FUSE_DEBUG
	diag_printf("blowing fuse %d %d bit %d addr_h=0x%02x, addr_l=0x%02x\n",
				bank, row, bit, addr_h, addr_l);
#endif

	writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
	writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
	/* Start Programming */
	writel(0x71, IIM_BASE_ADDR + IIM_FCTL_OFF);
	if (poll_fuse_op_done(POLL_FUSE_PRGD) == 0) {
		ret = 0;
	}

	/* Enable IIM Program Protect */
	writel(0x0, IIM_BASE_ADDR + IIM_PREG_P_OFF);
	return ret;
}

/*
 * This command is added for burning IIM fuses
 */
RedBoot_cmd("fuse_read",
			"read some fuses",
			"<bank> <row>",
			do_fuse_read
	);

RedBoot_cmd("fuse_blow",
			"blow some fuses",
			"<bank> <row> <value>",
			do_fuse_blow
	);

void quick_itoa(u32 num, char *a)
{
	int i, j, k;
	for (i = 0; i <= 7; i++) {
		j = (num >> (4 * i)) & 0xF;
		k = (j < 10) ? '0' : ('a' - 0xa);
		a[i] = j + k;
	}
}

// slen - streng length, e.g.: 23 -> slen=2; abcd -> slen=4
// only convert hex value as string input. so "12" is 0x12.
u32 quick_atoi(char *a, u32 slen)
{
	u32 i, num = 0, digit;

	for (i = 0; i < slen; i++) {
		if (a[i] >= '0' && a[i] <= '9') {
			digit = a[i] - '0';
		} else if (a[i] >= 'a' && a[i] <= 'f') {
			digit = a[i] - 'a' + 10;
		} else if (a[i] >= 'A' && a[i] <= 'F') {
			digit = a[i] - 'A' + 10;
		} else {
			diag_printf("ERROR: %c\n", a[i]);
			return -1;
		}
		num = (num * 16) + digit;
	}
	return num;
}

void fuse_blow_row(int bank, int row, int value)
{
	unsigned int reg, i;

	FUSE_PROG_START();

	// enable fuse blown
	reg = readl(CCM_BASE_ADDR + 0x64);
	reg |= 0x10;
	writel(reg, CCM_BASE_ADDR + 0x64);

	for (i = 0; i < 8; i++) {
		if (((value >> i) & 0x1) == 0) {
			continue;
		}
		if (fuse_blow(bank, row, i) != 0) {
			diag_printf("fuse_blow(bank: %d, row: %d, bit: %d failed\n",
						bank, row, i);
		}
	}
	reg &= ~0x10;
	writel(reg, CCM_BASE_ADDR + 0x64);

	FUSE_PROG_DONE();
}

void do_fuse_blow(int argc, char *argv[])
{
	unsigned long bank, row, value, i;
	unsigned int fuse_val;
	char *s;
	char val[3];

	if (argc == 1) {
		diag_printf("It is too dangeous for you to use this command.\n");
		return;
	}

	if (argc == 3) {
		if (strcasecmp(argv[1], "scc") == 0) {
			// fuse_blow scc C3D153EDFD2EA9982226EF5047D3B9A0B9C7138EA87C028401D28C2C2C0B9AA2
			diag_printf("Ready to burn SCC fuses\n");
			s=argv[2];
			for (i = 0; ;i++) {
				memcpy(val, s, 2);
				val[2]='\0';
				value = quick_atoi(val, 2);
				//    diag_printf("fuse_blow_row(2, %d, value=0x%02x)\n", i, value);
				fuse_blow_row(2, i, value);

				if ((++s)[0] == '\0') {
					diag_printf("ERROR: Odd string input\n");
					break;
				}
				if ((++s)[0] == '\0') {
					diag_printf("Successful\n");
					break;
				}
			}
		} else if (strcasecmp(argv[1], "srk") == 0) {
			// fuse_blow srk 418bccd09b53bee1ab59e2662b3c7877bc0094caee201052add49be8780dff95
			diag_printf("Ready to burn SRK key fuses\n");
			s=argv[2];
			for (i = 0; ;i++) {
				memcpy(val, s, 2);
				val[2]='\0';
				value = quick_atoi(val, 2);
				if (i == 0) {
					fuse_blow_row(1, 1, value); // 0x41 goes to SRK_HASH[255:248], bank 1, row 1
				} else
					fuse_blow_row(3, i, value);  // 0x8b in SRK_HASH[247:240] bank 3, row 1
												 // 0xcc in SRK_HASH[239:232] bank 3, row 2
												 // ...
				if ((++s)[0] == '\0') {
					diag_printf("ERROR: Odd string input\n");
					break;
				}
				if ((++s)[0] == '\0') {
					diag_printf("Successful\n");
					break;
				}
			}
		} else
			diag_printf("This command is not supported\n");

		return;
	} else if (argc == 4) {
		if (!parse_num(argv[1], &bank, &argv[1], " ")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}
		if (!parse_num(argv[2], &row, &argv[2], " ")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}
		if (!parse_num(argv[3], &value, &argv[3], " ")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}

		diag_printf("Blowing fuse at bank:%ld row:%ld value:%ld\n",
					bank, row, value);
		fuse_blow_row(bank, row, value);
		fuse_val = sense_fuse(bank, row, 0);
		diag_printf("fuses at (bank:%ld, row:%ld) = 0x%02x\n", bank, row, fuse_val);

	} else {
		diag_printf("Passing in wrong arguments: %d\n", argc);
	}
}

/* precondition: m>0 and n>0.  Let g=gcd(m,n). */
static int gcd(int m, int n)
{
	while (m > 0) {
		if (n > m) {
			/* swap */
			m ^= n;
			n ^= m;
			m ^= n;
		}
		m -= n;
	}
	return n;
}

int read_mac_addr_from_fuse(unsigned char* data)
{
	data[0] = sense_fuse(1, 9, 0) ;
	data[1] = sense_fuse(1, 10, 0) ;
	data[2] = sense_fuse(1, 11, 0) ;
	data[3] = sense_fuse(1, 12, 0) ;
	data[4] = sense_fuse(1, 13, 0) ;
	data[5] = sense_fuse(1, 14, 0) ;

	if ((data[0] == 0) && (data[1] == 0) && (data[2] == 0) &&
		(data[3] == 0) && (data[4] == 0) && (data[5] == 0)) {
		return 0;
	}

	return 1;
}

#if 0
void imx_power_mode(int mode)
{
	volatile unsigned int val;
	switch (mode) {
	case 2:
		writel(0x0000030f, GPC_PGR);
		writel(0x1, SRPGCR_EMI);
		writel(0x1, SRPGCR_ARM);
		writel(0x1, PGC_PGCR_VPU);
		writel(0x1, PGC_PGCR_IPU);


	case 1:
		// stop mode - from validation code
		// Set DSM_INT_HOLDOFF bit in TZIC
		// If the TZIC didn't write the bit then there was interrupt pending
		// It will be serviced while we're in the loop
		// So we write to this bit again
		while (readl(INTC_BASE_ADDR + 0x14) == 0) {
			writel(1, INTC_BASE_ADDR + 0x14);
			// Wait few cycles
			__asm("nop");
			__asm("nop");
			__asm("nop");
			__asm("nop");
			__asm("nop");
			__asm("nop");
			__asm("nop");
		}
		diag_printf("Entering stop mode\n");
		val = readl(CCM_BASE_ADDR + 0x74);
		val = (val & 0xfffffffc) | 0x2; // set STOP mode
		writel(val, CCM_BASE_ADDR + 0x74);
		val = readl(PLATFORM_LPC_REG);
		writel(val | (1 << 16), PLATFORM_LPC_REG);// ENABLE DSM in ELBOW submodule of ARM platform
		writel(val | (1 << 17), PLATFORM_LPC_REG);// ENABLE DSM in ELBOW submodule of ARM platform
		break;
	}

	hal_delay_us(50);

	asm("mov r1, #0");
	asm("mcr p15, 0, r1, c7, c0, 4");
}

void do_power_mode(int argc, char *argv[])
{
	unsigned long mode;

	if (argc == 1) {
		diag_printf("Usage: power_mode <mode>\n");
		return;
	} else if (argc == 2) {
		if (!parse_num(argv[1], &mode, &argv[1], " ")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}
		diag_printf("Entering power mode: %lu\n", mode);
		imx_power_mode(mode);

	} else {
		diag_printf("Passing in wrong arguments: %d\n", argc);
		diag_printf("Usage: power_mode <mode>\n");
	}
}

/*
 * This command is added for burning IIM fuses
 */
RedBoot_cmd("power_mode",
			"Enter various power modes:",
			"\n"
			"	    <0> - WAIT\n"
			"	    <1> - SRPG\n"
			"	    <2> - STOP\n"
			"	    <3> - STOP with Power-Gating\n"
			"	    -- need reset after issuing the command",
			do_power_mode
	);
#endif

/* Super Root key moduli */
static const UINT8 hab_super_root_moduli[] = {
	/* modulus data */
	0xb9, 0x84, 0xc8, 0x8a, 0xd3, 0x7e, 0xcc, 0xc0, 0xe7, 0x3e, 0x11, 0x53,
	0x6b, 0x5e, 0xea, 0xf4, 0xd9, 0xac, 0x5a, 0x63, 0x8a, 0x79, 0x96, 0x83,
	0xb1, 0x39, 0xb2, 0x6f, 0x9c, 0x54, 0x87, 0xf4, 0x3b, 0x9e, 0xd8, 0x0f,
	0x89, 0xf5, 0x01, 0x53, 0xb8, 0xe2, 0xcc, 0x75, 0x0d, 0xe1, 0x13, 0xfa,
	0xa7, 0xb9, 0x1e, 0xff, 0x6a, 0x05, 0xdb, 0x58, 0x10, 0xbf, 0x2b, 0xf4,
	0xe7, 0x0a, 0x63, 0x82, 0x2c, 0xa3, 0xb5, 0x0a, 0x72, 0x1c, 0xdc, 0x29,
	0xc1, 0x81, 0xb5, 0x9a, 0xf0, 0x25, 0x7d, 0xd6, 0xee, 0x01, 0x64, 0xc7,
	0x07, 0x2d, 0xcb, 0x31, 0x4c, 0x8d, 0x82, 0xf6, 0x44, 0x95, 0x4a, 0xbc,
	0xae, 0xe8, 0x2a, 0x89, 0xd4, 0xf2, 0x66, 0x72, 0x2b, 0x09, 0x4e, 0x56,
	0xe9, 0xbf, 0x5e, 0x38, 0x5c, 0xd5, 0x7e, 0x15, 0x55, 0x86, 0x0f, 0x19,
	0xf6, 0x00, 0xee, 0xa1, 0x92, 0x78, 0xef, 0x93, 0xcb, 0xfa, 0xb4, 0x98,
	0x19, 0xef, 0x10, 0x70, 0xde, 0x36, 0x1c, 0x12, 0x2e, 0xd2, 0x09, 0xc7,
	0x7b, 0xd1, 0xaa, 0xd3, 0x46, 0x65, 0xa1, 0x5b, 0xee, 0xa5, 0x96, 0x97,
	0x98, 0x3e, 0xfc, 0xf8, 0x74, 0x22, 0x51, 0xe7, 0xf1, 0x2f, 0x30, 0x79,
	0x13, 0xe5, 0x42, 0xc6, 0x7c, 0x18, 0x76, 0xd3, 0x7f, 0x5a, 0x13, 0xde,
	0x2f, 0x51, 0x07, 0xfa, 0x93, 0xfe, 0x10, 0x8a, 0x0c, 0x18, 0x60, 0x3c,
	0xff, 0x6a, 0x9b, 0xe7, 0x10, 0x2d, 0x71, 0xd2, 0x34, 0xc0, 0xdf, 0xbe,
	0x17, 0x4e, 0x75, 0x40, 0x83, 0xaa, 0x90, 0xd1, 0xed, 0xbd, 0xbf, 0xac,
	0x9a, 0x30, 0xbd, 0x69, 0x4d, 0xd8, 0x00, 0x63, 0x92, 0x69, 0x98, 0xf8,
	0x89, 0xdc, 0x7b, 0xe3, 0x66, 0x7e, 0xdd, 0xfa, 0x8c, 0x74, 0xe2, 0xb1,
	0xeb, 0x94, 0xf7, 0xab, 0x0e, 0x92, 0x06, 0xab, 0x60, 0xe5, 0x00, 0x43,
	0xb2, 0x5e, 0x6e, 0xeb
};

/* Super Root key */
const hab_rsa_public_key hab_super_root_key[] = {
	{
		{
			/* RSA public exponent, right-padded */
			0x01, 0x00, 0x01, 0x00,
		},
		/* pointer to modulus data */
		hab_super_root_moduli,
		/* Exponent size in bytes */
		0x03,
		/* Modulus size in bytes */
		0x100,
		/* Key data valid */
		TRUE
	}
};
