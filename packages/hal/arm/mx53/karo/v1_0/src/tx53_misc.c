//==========================================================================
//
//	tx53_misc.c
//
//	HAL misc board support code for the tx53
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <stdlib.h>
#include <redboot.h>
#include <string.h>
#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>		// base types
#include <cyg/infra/cyg_trac.h>		// tracing macros
#include <cyg/infra/cyg_ass.h>		// assertion macros

#include <cyg/hal/hal_io.h>			// IO macros
#include <cyg/hal/hal_arch.h>		// Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>		// Interrupt names
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_soc.h>		// Hardware definitions
#include CYGBLD_HAL_PLF_DEFS_H		// Platform specifics

#include <cyg/infra/diag.h>			// diag_printf

// All the MM table layout is here:
#include <cyg/hal/hal_mm.h>

/* MMU table definitions */
#define SD_P0		(RAM_BANK0_BASE >> 20)			/* physical RAM bank 0 address */
#define SD_C0		SD_P0							/* virtual address for cached 1:1 mapping */
#define SD_S0		(RAM_BANK0_SIZE >> 20)			/* RAM bank 0 size */
#define SD_U0		(UNCACHED_RAM_BASE_VIRT >> 20)
#ifdef RAM_BANK1_SIZE
#define SD_P1		(RAM_BANK1_BASE >> 20)			/* physical RAM bank 1 address */
#define SD_C1		(SD_P0 + SD_S0)
#define SD_S1		(RAM_BANK1_SIZE >> 20)			/* RAM bank 1 size */
#define SD_U1		(SD_U0 + SD_S0)
#define SD_HI		(SD_P1 + (SD_S1 - 1))
#endif

static unsigned long ttb_base = RAM_BANK0_BASE + 0x4000;

void hal_mmu_init(void)
{
	unsigned long i;
	/*
	 * Set the TTB register
	 */
	asm volatile ("mcr  p15,0,%0,c2,c0,0" : : "r"(ttb_base));

	/*
	 * Set the Domain Access Control Register
	 */
	i = ARM_ACCESS_DACR_DEFAULT;
	asm volatile ("mcr  p15,0,%0,c3,c0,0" : : "r"(i) /*:*/);

	/*
	 * First clear all TT entries - ie Set them to Faulting
	 */
	memset((void *)ttb_base, 0, ARM_FIRST_LEVEL_PAGE_TABLE_SIZE);

	/*		   Physical	 Virtual	 Size			Attributes				  access permissions		Function */
	/*		   Base	 Base	  MB			 cached?	   buffered?											 */
	/*		   xxx00000	 xxx00000										 */
	X_ARM_MMU_SECTION(0x000, 0xfff,	0x001, ARM_CACHEABLE,	ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* Boot Rom */
	X_ARM_MMU_SECTION(0xF80, 0xF80, 0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* IRAM */
	X_ARM_MMU_SECTION(0x180, 0x480, 0x080, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* IPUv3D */
	X_ARM_MMU_SECTION(0x500, 0x500,	0x200, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* Internal Registers */
	X_ARM_MMU_SECTION(SD_P0, 0x000,	SD_S0, ARM_CACHEABLE,	ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(SD_P0, SD_C0,	SD_S0, ARM_CACHEABLE,	ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(SD_P0, SD_U0,	SD_S0, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* SDRAM (uncached) */
#ifdef RAM_BANK1_SIZE
	X_ARM_MMU_SECTION(SD_P1, SD_S0, SD_S1, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(SD_P1, SD_C1, SD_S1, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(SD_P1, SD_U1, SD_S1, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	/* make sure the last MiB in the upper bank of SDRAM (where RedBoot resides)
	 * has a unity mapping (required when switching MMU on).
	 * This mapping will overwrite the last MiB of the uncached mapping above
	 * which will be restored in plf_hardware_init().
	 */
	X_ARM_MMU_SECTION(SD_HI, SD_HI, 0x001, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RO_RO); /* SDRAM bank1 identity mapping */
#endif
	X_ARM_MMU_SECTION(0xF40, 0xF40,	0x040, ARM_UNCACHEABLE,	ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* CS1 EIM control & NFC buffer */
}

static inline void set_reg(CYG_ADDRESS addr, CYG_WORD32 set, CYG_WORD32 clr)
{
	CYG_WORD32 val;

	HAL_READ_UINT32(addr, val);
	val = (val & ~clr) | set;
	HAL_WRITE_UINT32(addr, val);
}

static inline void setup_gpio(int grp, int bit)
{
	CYG_ADDRESS base = MX53_GPIO_ADDR(grp);

	if (base == 0)
		return;
	set_reg(base + GPIO_DR, 0, 1 << bit);
	set_reg(base + GPIO_GDIR, 1 << bit, 0);
}

//
// Platform specific initialization
//
#define PAD_CTL_UART_OUT		MUX_PAD_CTRL(PAD_CTL_PKE | PAD_CTL_PUE | \
							PAD_CTL_PUS_100K_DOWN | PAD_CTL_DSE_HIGH | \
							PAD_CTL_SRE_FAST)
#define PAD_CTL_UART_IN		(MUX_PAD_CTRL(PAD_CTL_HYS) | PAD_CTL_UART_OUT)

static iomux_v3_cfg_t tx53_uart_pads[] = {
	MX53_PAD_PATA_DIOW__UART1_TXD_MUX | PAD_CTL_UART_OUT,
	MX53_PAD_PATA_DMACK__UART1_RXD_MUX | PAD_CTL_UART_IN,

	MX53_PAD_PATA_DMARQ__UART2_TXD_MUX | PAD_CTL_UART_OUT,
	MX53_PAD_PATA_BUFFER_EN__UART2_RXD_MUX | PAD_CTL_UART_IN,

	MX53_PAD_PATA_CS_0__UART3_TXD_MUX | PAD_CTL_UART_OUT,
	MX53_PAD_PATA_CS_1__UART3_RXD_MUX | PAD_CTL_UART_IN,
};

static void uart_gpio_init(void)
{
	mx53_iomux_setup_pads(tx53_uart_pads, CYG_NELEM(tx53_uart_pads));
}

/* GPIOs to set up for TX53/Starterkit-5:
   Function  FCT  GPIO        Pad    IOMUXC SW_PAD  SW_INP mode
                                     OFFSET  CTRL    SEL
FEC_REF_CLK   0   1,23 FEC_REF_CLK   0x24c   0x5c8
FEC_MDC       0   1,31 FEC_MDC       0x26c   0x5e8
FEC_MDIO      0   1,22 FEC_MDIO      0x248   0x5c4  0x804  1
FEC_RXD0      0   1,27 FEC_RXD0      0x25c   0x5d8
FEC_RXD1      0   1,26 FEC_RXD1      0x258   0x5d4
FEC_RX_ER     0   1,24 FEC_RX_ER     0x250   0x5cc
FEC_TX_EN     0   1,28 FEC_TX_EN     0x260   0x5dc
FEC_TXD0      0   1,30 FEC_TXD0      0x268   0x5e4
FEC_TXD1      0   1,29 FEC_TXD1      0x264   0x5e0
FEC_CRS_DV    0   1,25 FEC_CRS_DV    0x254   0x5d0

FEC_RESET#    1   7,6  PATA_DA_0     0x290   0x610
FEC_ENABLE    1   3,20 EIM_D20       0x128   0x470
*/
static iomux_v3_cfg_t tx53_fec_pads[] = {
	/* setup FEC PHY pins for GPIO function (with SION set) */
	MX53_PAD_FEC_REF_CLK__GPIO_1_23 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_MDC__GPIO_1_31 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_MDIO__GPIO_1_22 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_RXD0__GPIO_1_27 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_RXD1__GPIO_1_26 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_RX_ER__GPIO_1_24 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_TX_EN__GPIO_1_28 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_TXD0__GPIO_1_30 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_TXD1__GPIO_1_29 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_CRS_DV__GPIO_1_25 | IOMUX_CONFIG_SION,

	/* PHY reset */
	MX53_PAD_PATA_DA_0__GPIO_7_6 | IOMUX_CONFIG_SION,
	/* PHY power */
	MX53_PAD_EIM_D20__GPIO_3_20 | IOMUX_CONFIG_SION,
};

static iomux_v3_cfg_t tx53_i2c_pads[] = {
	MX53_PAD_EIM_D21__I2C1_SCL | MUX_PAD_CTRL(PAD_CTL_DSE_HIGH |
											PAD_CTL_HYS |
											PAD_CTL_ODE) |
								IOMUX_CONFIG_SION,
	MX53_PAD_EIM_D28__I2C1_SDA | MUX_PAD_CTRL(PAD_CTL_DSE_HIGH |
											PAD_CTL_HYS |
											PAD_CTL_ODE) |
								IOMUX_CONFIG_SION,
};

static void fec_gpio_init(void)
{
	/* setup GPIO data register to 0 and DDIR output for FEC PHY pins */
	setup_gpio(1, 23);
	setup_gpio(1, 31);
	setup_gpio(1, 22);
	setup_gpio(1, 25);
	setup_gpio(1, 27);
	setup_gpio(1, 26);
	setup_gpio(1, 24);
	setup_gpio(1, 28);
	setup_gpio(1, 30);
	setup_gpio(1, 29);

	setup_gpio(7, 6);
	setup_gpio(3, 20);

	/* setup input mux for FEC pins */
	mx53_iomux_setup_pads(tx53_fec_pads, CYG_NELEM(tx53_fec_pads));
	mx53_iomux_setup_pads(tx53_i2c_pads, CYG_NELEM(tx53_i2c_pads));
}

#ifdef CYGHWR_MX53_LCD_LOGO
static iomux_v3_cfg_t tx53_lcd_pads[] = {
#define PAD_CTL_LCD		MUX_PAD_CTRL(PAD_CTL_DSE_HIGH | PAD_CTL_DSE_HIGH)
	MX53_PAD_DISP0_DAT0__IPU_DISP0_DAT_0 | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT1, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT2, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT3, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT4, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT5, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT6, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT7, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT8, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT9, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT10, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT12, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT13, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT14, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT15, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT16, | PAD_CTL_LCD,
	MX53_PAD_DISP0_DAT17, | PAD_CTL_LCD,

	/* HSYNC, VSYNC */
	MX53_PAD_DI0_PIN2__IPU_DI0_PIN2 | PAD_CTL_LCD,
	MX53_PAD_DI0_PIN3__IPU_DI0_PIN3 | PAD_CTL_LCD,
	/* OE_ACD */
	MX53_PAD_DI0_PIN15__IPU_DI0_PIN15 | MUX_PAD_CRTL(PAD_CTL_LCD | PAD_CTL_PKE),
	MX53_PAD_DI0_DISP_CLK__IPU_DI0_DISP_CLK | MUX_PAD_CTRL(PAD_CTL_LCD),

	/* LCD Power Enable */
	MX53_PAD_EIM_EB3__GPIO_2_31,
	/* LCD Reset */
	MX53_PAD_EIM_D29__GPIO_3_29,
	/* LCD backlight */
	MX53_PAD_GPIO_1__GPIO_1_1,
};

void mxc_ipu_iomux_config(void)
{
	/* LCD Power Enable GPIO_2_31 (active High) */
	gpio_clr_bit(2, 31);

	/* LCD Reset GPIO_3_29 (active Low) */
	gpio_clr_bit(3, 29);

	/* LCD Backlight GPIO_1_1 (PWM 0: full brightness 1: off) */
	gpio_set_bit(1, 1);

	mx53_iomux_setup_pads(tx53_lcd_pads, CYG_NELEM(tx53_lcd_pads));
}
RedBoot_init(mxc_ipu_iomux_config, RedBoot_INIT_SECOND);
#endif

//
// Platform specific initialization
//

void plf_hardware_init(void)
{
#ifdef RAM_BANK1_SIZE
	/* overwrite temporary mapping for high area in SDRAM with actual mapping */
	X_ARM_MMU_SECTION(SD_P0 + SD_S0 - 1, SD_U0 + SD_S0 - 1,	1, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW);
	HAL_CACHE_FLUSH_ALL();
#endif
	uart_gpio_init();
	fec_gpio_init();

	/* NFC setup */
	writel(readl(NFC_FLASH_CONFIG3_REG) |
			(1 << 15) | /* assert RBB_MODE  (see Errata: ENGcm09970) */
			(1 << 20) | /* assert NO_SDMA */
			(1 << 3), /* set bus width to 8bit */
			NFC_FLASH_CONFIG3_REG);
}

#define SOC_I2C1_BASE		UL(0x63fc8000)

/* Address offsets of the I2C registers */
#define MXC_IADR				0x00	/* Address Register */
#define MXC_IFDR				0x04	/* Freq div register */
#define MXC_I2CR				0x08	/* Control regsiter */
#define MXC_I2SR				0x0C	/* Status register */
#define MXC_I2DR				0x10	/* Data I/O register */

/* Bit definitions of I2CR */
#define MXC_I2CR_IEN			0x0080
#define MXC_I2CR_IIEN			0x0040
#define MXC_I2CR_MSTA			0x0020
#define MXC_I2CR_MTX			0x0010
#define MXC_I2CR_TXAK			0x0008
#define MXC_I2CR_RSTA			0x0004

/* Bit definitions of I2SR */
#define MXC_I2SR_ICF			0x0080
#define MXC_I2SR_IAAS			0x0040
#define MXC_I2SR_IBB			0x0020
#define MXC_I2SR_IAL			0x0010
#define MXC_I2SR_SRW			0x0004
#define MXC_I2SR_IIF			0x0002
#define MXC_I2SR_RXAK			0x0001

#define LP3972_SLAVE_ADDR	0x34

static inline cyg_uint8 i2c_addr(cyg_uint8 addr, int rw)
{
	return (addr << 1) | !!rw;
}

static inline cyg_uint8 tx53_i2c_read(cyg_uint8 reg)
{
	cyg_uint16 val;
	HAL_READ_UINT16(SOC_I2C1_BASE + reg, val);
	return val;
}

static inline void tx53_i2c_write(cyg_uint8 reg, cyg_uint8 val)
{
	HAL_WRITE_UINT16(SOC_I2C1_BASE + reg, val);
}

static inline void tx53_i2c_set_reg(cyg_uint8 reg, cyg_uint8 set, cyg_uint8 clr)
{
	cyg_uint8 val = tx53_i2c_read(reg);
	val = (val & ~clr) | set;
	tx53_i2c_write(reg, val);
}

static void tx53_i2c_disable(void)
{
	/* disable I2C controller */
	tx53_i2c_set_reg(MXC_I2CR, 0, MXC_I2CR_IEN);
	/* disable I2C clock */
	set_reg(CCM_BASE_ADDR + CLKCTL_CGPR, 0, (1 << 4));
}

static int tx53_i2c_init(void)
{
	int ret;

	/* enable I2C clock */
	set_reg(CCM_BASE_ADDR + CLKCTL_CGPR, (1 << 4), 0);

	/* setup I2C clock divider */
	tx53_i2c_write(MXC_IFDR, 0x2c);
	tx53_i2c_write(MXC_I2SR, 0);

	/* enable I2C controller in master mode */
	tx53_i2c_write(MXC_I2CR, MXC_I2CR_IEN);

	ret = tx53_i2c_read(MXC_I2SR);
	if (ret & MXC_I2SR_IBB) {
		diag_printf("I2C bus busy\n");
		tx53_i2c_disable();
		return -EIO;
	}
	return 0;
}

static int tx53_i2c_wait_busy(int set)
{
	int ret;
	const int max_loops = 100;
	int retries = max_loops;

	cyg_uint8 mask = set ? MXC_I2SR_IBB : 0;

	while ((ret = mask ^ (tx53_i2c_read(MXC_I2SR) & MXC_I2SR_IBB)) && --retries > 0) {
		HAL_DELAY_US(3);
	}
	if (ret != 0) {
		diag_printf("i2c: Waiting for IBB to %s timed out\n", set ? "set" : "clear");
		return -ETIMEDOUT;
	}
	return ret;
}

static int tx53_i2c_wait_tc(void)
{
	int ret;
	const int max_loops = 1000;
	int retries = max_loops;

	while (!((ret = tx53_i2c_read(MXC_I2SR)) & MXC_I2SR_IIF) && --retries > 0) {
		HAL_DELAY_US(3);
	}
	tx53_i2c_write(MXC_I2SR, 0);
	if (!(ret & MXC_I2SR_IIF)) {
		diag_printf("i2c: Wait for transfer completion timed out\n");
		return -ETIMEDOUT;
	}
	if (ret & MXC_I2SR_ICF) {
		if (tx53_i2c_read(MXC_I2CR) & MXC_I2CR_MTX) {
			if (!(ret & MXC_I2SR_RXAK)) {
				ret = 0;
			} else {
				diag_printf("i2c: No ACK received after writing data\n");
				return -ENXIO;
			}
		}
	}
	return ret;
}

static int tx53_i2c_stop(void)
{
	int ret;

	tx53_i2c_set_reg(MXC_I2CR, 0, MXC_I2CR_MSTA | MXC_I2CR_MTX);
	ret = tx53_i2c_wait_busy(0);
	return ret;
}

static int tx53_i2c_start(cyg_uint8 addr, int rw)
{
	int ret;

	ret = tx53_i2c_init();
	if (ret < 0) {
		diag_printf("I2C bus init failed; cannot switch fuse programming voltage\n");
		return ret;
	}
	tx53_i2c_set_reg(MXC_I2CR, MXC_I2CR_MSTA, 0);
	ret = tx53_i2c_wait_busy(1);
	if (ret == 0) {
		tx53_i2c_set_reg(MXC_I2CR, MXC_I2CR_MTX, 0);
		tx53_i2c_write(MXC_I2DR, i2c_addr(addr, rw));
		ret = tx53_i2c_wait_tc();
		if (ret < 0) {
			tx53_i2c_stop();
		}
	}
	return ret;
}

static int tx53_i2c_repeat_start(cyg_uint8 addr, int rw)
{
	int ret;

	tx53_i2c_set_reg(MXC_I2CR, MXC_I2CR_RSTA, 0);
	HAL_DELAY_US(3);
	tx53_i2c_write(MXC_I2DR, i2c_addr(addr, rw));
	ret = tx53_i2c_wait_tc();
	if (ret < 0) {
		tx53_i2c_stop();
	}
	return ret;
}

static int tx53_i2c_read_byte(void)
{
	int ret;

	tx53_i2c_set_reg(MXC_I2CR, MXC_I2CR_TXAK, MXC_I2CR_MTX);
	(void)tx53_i2c_read(MXC_I2DR); /* dummy read after address cycle */
	ret = tx53_i2c_wait_tc();
	tx53_i2c_stop();
	if (ret < 0) {
		return ret;
	}
	ret = tx53_i2c_read(MXC_I2DR);
	return ret;
}

static int tx53_i2c_write_byte(cyg_uint8 data, int last)
{
	int ret;

	tx53_i2c_set_reg(MXC_I2CR, MXC_I2CR_MTX, 0);
	tx53_i2c_write(MXC_I2DR, data);
	if ((ret = tx53_i2c_wait_tc()) < 0 || last) {
		tx53_i2c_stop();
	}
	return ret;
}

static int tx53_i2c_reg_read(cyg_uint8 slave_addr, cyg_uint8 reg)
{
	int ret;

	ret = tx53_i2c_start(slave_addr, 0);
	if (ret < 0) {
		return ret;
	}
	ret = tx53_i2c_write_byte(reg, 0);
	if (ret < 0) {
		return ret;
	}
	ret = tx53_i2c_repeat_start(slave_addr, 1);
	if (ret < 0) {
		return ret;
	}
	ret = tx53_i2c_read_byte();
	tx53_i2c_disable();
	return ret;
}

static int tx53_i2c_reg_write(cyg_uint8 slave_addr, cyg_uint8 reg, cyg_uint8 val)
{
	int ret;

	ret = tx53_i2c_start(slave_addr, 0);
	if (ret < 0) {
		return ret;
	}
	ret = tx53_i2c_write_byte(reg, 0);
	if (ret < 0) {
		return ret;
	}
	ret = tx53_i2c_write_byte(val, 1);
	tx53_i2c_disable();
	return ret;
}

int tx53_mac_addr_program(unsigned char mac_addr[ETHER_ADDR_LEN])
{
	int ret = 0;
	int i;

	for (i = 0; i < ETHER_ADDR_LEN; i++) {
		unsigned char fuse = readl(SOC_FEC_MAC_BASE + (i << 2));

		if ((fuse | mac_addr[i]) != mac_addr[i]) {
			diag_printf("MAC address fuse cannot be programmed: fuse[%d]=0x%02x -> 0x%02x\n",
						i, fuse, mac_addr[i]);
			return -1;
		}
		if (fuse != mac_addr[i]) {
			ret = 1;
		}
	}
	if (ret == 0) {
		return ret;
	}

	for (i = 0; i < ETHER_ADDR_LEN; i++) {
		unsigned char fuse = readl(SOC_FEC_MAC_BASE + (i << 2));
		int row = SOC_MAC_ADDR_FUSE + i;

		if (fuse == mac_addr[i]) {
			continue;
		}
		fuse_blow_row(SOC_MAC_ADDR_FUSE_BANK, row, mac_addr[i]);
		ret = sense_fuse(SOC_MAC_ADDR_FUSE_BANK, row, 0);
		if (ret != mac_addr[i]) {
			diag_printf("Failed to verify fuse bank %d row %d; expected %02x got %02x\n",
						SOC_MAC_ADDR_FUSE_BANK, row, mac_addr[i], ret);
			goto out;
		}
	}
#ifdef SOC_MAC_ADDR_LOCK_BIT
	fuse_blow_row(SOC_MAC_ADDR_FUSE_BANK, SOC_MAC_ADDR_LOCK_FUSE,
				(1 << SOC_MAC_ADDR_LOCK_BIT));
#endif
out:
	return ret;
}

#include CYGHWR_MEMORY_LAYOUT_H

typedef void code_fun(void);

void tx53_program_new_stack(void *func)
{
	register CYG_ADDRESS stack_ptr asm("sp");
	register CYG_ADDRESS old_stack asm("r4");
	register code_fun *new_func asm("r0");

	old_stack = stack_ptr;
	stack_ptr = CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE - sizeof(CYG_ADDRESS);
	new_func = (code_fun*)func;
	new_func();
	stack_ptr = old_stack;
}

#define PMIC_NAME			"LT3598"

static int pmic_reg_read(cyg_uint8 reg)
{
	int ret;

	ret = tx53_i2c_reg_read(LP3972_SLAVE_ADDR, reg);
	if (ret < 0)
		diag_printf("Failed to read %s reg 0x%02x %d\n", PMIC_NAME,
					reg, ret);
#ifdef DEBUG
	else
		diag_printf("read %02x from reg %02x\n", ret, reg);
#endif
	return ret;
}

static int pmic_reg_write(cyg_uint8 reg, cyg_uint8 val)
{
	int ret;

	ret = tx53_i2c_reg_write(LP3972_SLAVE_ADDR, reg, val);
	if (ret)
		diag_printf("Failed to write 0x%02x to %s reg 0x%02x: %d\n",
					val, PMIC_NAME, reg, ret);
#ifdef DEBUG
	else
		diag_printf("wrote %02x to reg %02x\n", val, reg);
#endif
	return ret;
}

int tx53_fuse_voltage(int on)
{
	int ret;
	int retries = 0;

	if (on) {
		ret = pmic_reg_read(0x33);
		if (ret < 0)
			return ret;
		if ((ret & 0xe0) != 0xe0) {
			ret = pmic_reg_write(0x33, ret | 0xe0);
			if (ret)
				return ret;

			ret = pmic_reg_read(0x33);
			if (ret < 0)
				return ret;

			if ((ret & 0xe0) != 0xe0) {
				diag_printf("Could not adjust LT3589 LDO4 output voltage\n");
				return -EIO;
			}
		}

		ret = pmic_reg_read(0x12);
		if (ret < 0)
			return ret;

		if (!(ret & (1 << 6))) {
			ret = pmic_reg_write(0x12, ret | (1 << 6));
			if (ret)
				return ret;
		}

		/* enable SW regulator control and all regulators */
		ret = pmic_reg_write(0x10, 0xff);
		if (ret)
			return ret;

		for (;;) {
			ret = pmic_reg_read(0x13);
			if (ret < 0)
				return ret;
			if (ret & (1 << 7))
				break;

#ifdef DEBUG
			if (retries == 0)
				diag_printf("Waiting for LDO4 PGOOD\n");
#endif
			HAL_DELAY_US(100);
			if (++retries >= 1000) {
				diag_printf("Failed to enable LDO4\n");
				return -ETIMEDOUT;
			}
		}
	} else {
		ret = pmic_reg_write(0x10, 0xbf);
		if (ret)
			return ret;

		ret = pmic_reg_read(0x12);
		if (ret < 0)
			return ret;
		if (ret & (1 << 6)) {
			ret = pmic_reg_write(0x12, ret & ~(1 << 6));
			if (ret)
				return ret;
		}
	}

	ret = pmic_reg_read(0x10);
	if (ret < 0)
		return ret;

	if (!(ret & (1 << 6)) ^ !on) {
		diag_printf("Could not %sable LT3589 LDO4 output\n",
					on ? "en" : "dis");
		return -EIO;
	}
	return 0;
}

#define LT3589_PGOOD_MASK		(1 << 5)
#define LT3589_SLEW_RATE(n)		(((n) & 3) << 6)
#define CORE_VOLTAGE_1200		0x1e
#define CORE_VOLTAGE_1000		0x19
#define CORE_VOLTAGE_800		0x13

int adjust_core_voltage(unsigned int clock)
{
	int ret;
	int volt;
	int retries = 0;

	if (clock <= 800) {
		volt = CORE_VOLTAGE_800;
	} else if (clock <= 1000) {
		volt = CORE_VOLTAGE_1000;
	} else if (clock <= 1200) {
		volt = CORE_VOLTAGE_1200;
	} else {
		diag_printf("No core voltage assigned for %u MHz core clock\n",
			clock);
		return -EINVAL;
	}

	ret = pmic_reg_read(0x23);
	if (ret < 0) {
		return ret;
	}

	ret = pmic_reg_write(0x23, volt | LT3589_SLEW_RATE(3) | LT3589_PGOOD_MASK);
	if (ret < 0) {
		return ret;
	}

	ret = pmic_reg_read(0x20);
	if (ret < 0)
		return ret;

	/* Select V1 reference and enable slew */
	ret = pmic_reg_write(0x20, (ret & ~(1 << 1)) | (1 << 0));
	if (ret)
		return ret;

	do {
		ret = pmic_reg_read(0x20);
		if (ret < 0)
			return ret;
		if (++retries >= 1000)
			return -ETIMEDOUT;
		HAL_DELAY_US(100);
	} while (ret & (1 << 0));
	return ret;
}

static void display_board_type(void)
{
	diag_printf("\nBoard Type: Ka-Ro TX53 v3\n");
	adjust_core_voltage(CYGNUM_HAL_ARM_TX53_CPU_CLK);
}

static void display_board_info(void)
{
	display_board_type();
}
RedBoot_init(display_board_info, RedBoot_INIT_LAST);

void mxc_i2c_init(unsigned int module_base)
{
	switch (module_base) {
	case I2C1_BASE_ADDR:
		writel(0x15, IOMUXC_BASE_ADDR + 0x12c);
		writel(0x10c, IOMUXC_BASE_ADDR + 0x474);
		writel(0x1, IOMUXC_BASE_ADDR + 0x814);

		writel(0x15, IOMUXC_BASE_ADDR + 0x14c);
		writel(0x10c, IOMUXC_BASE_ADDR + 0x494);
		writel(0x1, IOMUXC_BASE_ADDR + 0x818);
		break;

	case I2C3_BASE_ADDR:
		writel(0x11, IOMUXC_BASE_ADDR + 0x120);
		writel(0x1fc, IOMUXC_BASE_ADDR + 0x468);

		writel(0x11, IOMUXC_BASE_ADDR + 0x118);
		writel(0x1fc, IOMUXC_BASE_ADDR + 0x460);
		break;

	default:
		diag_printf("Invalid I2C base: 0x%x\n", module_base);
	}
}
