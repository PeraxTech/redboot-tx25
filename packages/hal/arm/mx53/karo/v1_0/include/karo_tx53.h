#ifndef CYGONCE_KARO_TX53_H
#define CYGONCE_KARO_TX53_H

//=============================================================================
//
//	Platform specific support (register layout, etc)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/hal/hal_soc.h>	// Hardware definitions
#include <cyg/hal/mx53_iomux.h>

#include CYGHWR_MEMORY_LAYOUT_H

#define GPIO_DR						0x00
#define GPIO_GDIR					0x04
#define GPIO_PSR					0x08

#define STK5_LED_MASK				(1 << 20)
#define STK5_LED_REG_ADDR			(GPIO2_BASE_ADDR + GPIO_DR)

#define LED_MAX_NUM					1

#define SOC_FEC_MAC_BASE			(IIM_BASE_ADDR + 0xc24)

#define TX53_SDRAM_SIZE				SDRAM_SIZE

#define LED_IS_ON(n) ({							\
	CYG_WORD32 __val;							\
	HAL_READ_UINT32(STK5_LED_REG_ADDR, __val);	\
	__val & STK5_LED_MASK;						\
})

#define TURN_LED_ON(n)							\
	CYG_MACRO_START								\
	CYG_WORD32 __val;							\
	HAL_READ_UINT32(STK5_LED_REG_ADDR, __val);	\
	__val |= STK5_LED_MASK;						\
	HAL_WRITE_UINT32(STK5_LED_REG_ADDR, __val);	\
	CYG_MACRO_END

#define TURN_LED_OFF(n)							\
	CYG_MACRO_START								\
	CYG_WORD32 __val;							\
	HAL_READ_UINT32(STK5_LED_REG_ADDR, __val);	\
	__val &= ~STK5_LED_MASK;					\
	HAL_WRITE_UINT32(STK5_LED_REG_ADDR, __val);	\
	CYG_MACRO_END

#define BOARD_DEBUG_LED(n)						\
	CYG_MACRO_START								\
	if (n >= 0 && n < LED_MAX_NUM) {			\
		if (LED_IS_ON(n))						\
			TURN_LED_OFF(n);					\
		else									\
			TURN_LED_ON(n);						\
	}											\
	CYG_MACRO_END

#define BLINK_LED(l, n)							\
	CYG_MACRO_START								\
	int _i;										\
	for (_i = 0; _i < (n); _i++) {				\
		BOARD_DEBUG_LED(l);						\
		HAL_DELAY_US(200000);					\
		BOARD_DEBUG_LED(l);						\
		HAL_DELAY_US(300000);					\
	}											\
	HAL_DELAY_US(1000000);						\
	CYG_MACRO_END

#if !defined(__ASSEMBLER__)
extern int tx53_fuse_voltage(int on);
#define FUSE_PROG_START()		tx53_fuse_voltage(1)
#define FUSE_PROG_DONE()		tx53_fuse_voltage(0)

void mxc_ipu_iomux_config(void);

enum {
	BOARD_TYPE_TX53KARO,
};

#define gpio_tst_bit(__grp, __gpio)				\
	CYG_MACRO_START								\
	CYG_ADDRESS addr = MX53_GPIO_ADDR(__grp);	\
	CYG_WORD32 val;								\
												\
	if (addr == 0)								\
		return;									\
	if (__gpio < 0 || __gpio >= GPIO_NUM_PIN) {	\
		return 0;								\
	}											\
	HAL_READ_UINT32(addr + GPIO_PSR, val);		\
	return !!(val & (1 << __gpio));				\
	CYG_MACRO_END

#define gpio_set_bit(__grp, __gpio)								\
	CYG_MACRO_START												\
	CYG_WORD32 val;												\
	CYG_ADDRESS addr = MX53_GPIO_ADDR(__grp);					\
																\
	if (addr == 0)												\
		return;													\
	if (__gpio < 0 || __gpio >= GPIO_NUM_PIN)					\
		return;													\
																\
	HAL_READ_UINT32(addr + GPIO_DR, val);						\
	HAL_WRITE_UINT32(addr + GPIO_DR, val | (1 << __gpio));		\
																\
	HAL_READ_UINT32(addr + GPIO_GDIR, val);						\
	HAL_WRITE_UINT32(addr + GPIO_GDIR, val | (1 << __gpio));	\
	CYG_MACRO_END

#define gpio_clr_bit(__grp, __gpio)								\
	CYG_MACRO_START												\
	CYG_WORD32 val;												\
	CYG_ADDRESS addr = MX53_GPIO_ADDR(__grp);					\
																\
	if (addr == 0)												\
		return;													\
	if (__gpio < 0 || __gpio >= GPIO_NUM_PIN)					\
		return;													\
																\
	HAL_READ_UINT32(addr + GPIO_DR, val);						\
	HAL_WRITE_UINT32(addr + GPIO_DR, val & ~(1 << __gpio));		\
																\
	HAL_READ_UINT32(addr + GPIO_GDIR, val);						\
	HAL_WRITE_UINT32(addr + GPIO_GDIR, val | (1 << __gpio));	\
	CYG_MACRO_END

#endif /* __ASSEMBLER__ */

#endif /* CYGONCE_KARO_TX53_H */
