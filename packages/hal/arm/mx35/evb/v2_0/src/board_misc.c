//==========================================================================
//
//      board_misc.c
//
//      HAL misc board support code for the board
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include <redboot.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>         // base types
#include <cyg/infra/cyg_trac.h>         // tracing macros
#include <cyg/infra/cyg_ass.h>          // assertion macros

#include <cyg/hal/hal_io.h>             // IO macros
#include <cyg/hal/hal_arch.h>           // Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>           // Interrupt names
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_soc.h>         // Hardware definitions
#include <cyg/hal/fsl_board.h>             // Platform specifics

#include <cyg/infra/diag.h>             // diag_printf

// All the MM table layout is here:
#include <cyg/hal/hal_mm.h>

externC void* memset(void *, int, size_t);
static void plf_setup_uart(void);
static void plf_setup_fec(void);

void hal_mmu_init(void)
{
    unsigned long ttb_base = RAM_BANK1_BASE + 0x4000;
    unsigned long i;

    /*
     * Set the TTB register
     */
    asm volatile ("mcr  p15,0,%0,c2,c0,0" : : "r"(ttb_base) /*:*/);

    /*
     * Set the Domain Access Control Register
     */
    i = ARM_ACCESS_DACR_DEFAULT;
    asm volatile ("mcr  p15,0,%0,c3,c0,0" : : "r"(i) /*:*/);

    /*
     * First clear all TT entries - ie Set them to Faulting
     */
    memset((void *)ttb_base, 0, ARM_FIRST_LEVEL_PAGE_TABLE_SIZE);

    /*              Actual   Virtual  Size   Attributes                                                    Function  */
    /*              Base     Base     MB     cached?           buffered?        access permissions                 */
    /*              xxx00000 xxx00000                                                                                */
    X_ARM_MMU_SECTION(0x000, 0xF00,   0x1,   ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* ROM */
    X_ARM_MMU_SECTION(0x300, 0x300,   0x1,   ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* L2CC */
    X_ARM_MMU_SECTION(0x400, 0x400,   0x400, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* Internal Regsisters upto SDRAM*/
    X_ARM_MMU_SECTION(0x900, 0x000,   0x80,  ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM 1:128M*/
    X_ARM_MMU_SECTION(0x900, 0x900,   0x80,  ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM 1:128M*/
    X_ARM_MMU_SECTION(0x900, 0x980,   0x80,  ARM_UNCACHEABLE, ARM_UNBUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM 1:128M*/
    X_ARM_MMU_SECTION(0xA00, 0xA00,   0x200,  ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* Flash */
    X_ARM_MMU_SECTION(0xB00, 0xB00,   0x20,   ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* PSRAM */
    X_ARM_MMU_SECTION(0xB20, 0xB20,   0x1E0,  ARM_UNCACHEABLE, ARM_UNBUFFERABLE,ARM_ACCESS_PERM_RW_RW); /* ESDCTL, WEIM, M3IF, EMI, NFC, External I/O */
}

//
// Platform specific initialization
//

unsigned int g_clock_src;

void plf_hardware_init(void)
{
    int i;
    unsigned long val = readl(CCM_BASE_ADDR + CLKCTL_CCMR);

    g_clock_src = FREQ_24MHZ;
    /* PBC setup */
    /* Reset interrupt status reg */
#if 0
    writel(0x0000007, PBC_CTRL1_SET); 
    for(i=0; i<10000; i++);
    writel(0x0000007, PBC_CTRL1_CLR); 
#endif
    plf_setup_uart();
    plf_setup_fec();
#if 0
     // UART1
     /*RXD1*/
     writel(0, IOMUXC_BASE_ADDR + 0x188);
     writel(0x1E0, IOMUXC_BASE_ADDR + 0x55C);
   
     /*TXD1*/
     writel(0, IOMUXC_BASE_ADDR + 0x18C);
     writel(0x40, IOMUXC_BASE_ADDR + 0x560);
     
     /*RTS1*/
     writel(0, IOMUXC_BASE_ADDR + 0x190);
     writel(0x1E0, IOMUXC_BASE_ADDR + 0x564);

     /*CTS1*/
     writel(0, IOMUXC_BASE_ADDR + 0x194);
     writel(0x40, IOMUXC_BASE_ADDR + 0x568);

    // UART2
    //writel(0x13131300, IOMUXC_BASE_ADDR + 0x70);
    //writel(0x00001313, IOMUXC_BASE_ADDR + 0x74);
    //writel(0x00000040, IOMUXC_BASE_ADDR + 0x7C);
    //writel(0x40400000, IOMUXC_BASE_ADDR + 0x78);
#endif
}

static void plf_setup_uart(void)
{
     // UART1
     /*RXD1*/
     writel(0, IOMUXC_BASE_ADDR + 0x188);
     writel(0x1E0, IOMUXC_BASE_ADDR + 0x55C);
   
     /*TXD1*/
     writel(0, IOMUXC_BASE_ADDR + 0x18C);
     writel(0x40, IOMUXC_BASE_ADDR + 0x560);
     
     /*RTS1*/
     writel(0, IOMUXC_BASE_ADDR + 0x190);
     writel(0x1E0, IOMUXC_BASE_ADDR + 0x564);

     /*CTS1*/
     writel(0, IOMUXC_BASE_ADDR + 0x194);
     writel(0x40, IOMUXC_BASE_ADDR + 0x568);

    // UART2
}

static void plf_setup_fec(void)
{
	/*FEC_TX_CLK*/
	writel(0, IOMUXC_BASE_ADDR + 0x02E0);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x0744);
	
	/*FEC_RX_CLK*/
	writel(0, IOMUXC_BASE_ADDR + 0x02E4);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x0748);
	
	/*FEC_RX_DV*/
	writel(0, IOMUXC_BASE_ADDR + 0x02E8);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x074C);
	
	/*FEC_COL*/
	writel(0, IOMUXC_BASE_ADDR + 0x02EC);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x0750);
	
	/*FEC_RDATA0*/
	writel(0, IOMUXC_BASE_ADDR + 0x02F0);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x0754);

	/*FEC_TDATA0*/
	writel(0, IOMUXC_BASE_ADDR + 0x02F4);
	writel(0x40, IOMUXC_BASE_ADDR + 0x0758);
	
	/*FEC_TX_EN*/
	writel(0, IOMUXC_BASE_ADDR + 0x02F8);
	writel(0x40, IOMUXC_BASE_ADDR + 0x075C);

	/*FEC_MDC*/
	writel(0, IOMUXC_BASE_ADDR + 0x02FC);
	writel(0x40, IOMUXC_BASE_ADDR + 0x0760);

	/*FEC_MDIO*/
	writel(0, IOMUXC_BASE_ADDR + 0x0300);
	writel(0x1F0, IOMUXC_BASE_ADDR + 0x0764);

	/*FEC_TX_ERR*/
	writel(0, IOMUXC_BASE_ADDR + 0x0304);
	writel(0x40, IOMUXC_BASE_ADDR + 0x0768);

	/*FEC_RX_ERR*/
	writel(0, IOMUXC_BASE_ADDR + 0x0308);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x076C);

	/*FEC_CRS*/
	writel(0, IOMUXC_BASE_ADDR + 0x030C);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x0770);

	/*FEC_RDATA1*/
	writel(0, IOMUXC_BASE_ADDR + 0x0310);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x0774);
	
	/*FEC_TDATA1*/
	writel(0, IOMUXC_BASE_ADDR + 0x0314);
	writel(0x40, IOMUXC_BASE_ADDR + 0x0778);

	/*FEC_RDATA2*/
	writel(0, IOMUXC_BASE_ADDR + 0x0318);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x077C);
	
	/*FEC_TDATA2*/
	writel(0, IOMUXC_BASE_ADDR + 0x031C);
	writel(0x40, IOMUXC_BASE_ADDR + 0x0780);
	
	/*FEC_RDATA3*/
	writel(0, IOMUXC_BASE_ADDR + 0x0320);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x0784);
	
	/*FEC_TDATA3*/
	writel(0, IOMUXC_BASE_ADDR + 0x0324);
	writel(0x40, IOMUXC_BASE_ADDR + 0x0788);
}

#include CYGHWR_MEMORY_LAYOUT_H

typedef void code_fun(void);

void board_program_new_stack(void *func)
{
    register CYG_ADDRESS stack_ptr asm("sp");
    register CYG_ADDRESS old_stack asm("r4");
    register code_fun *new_func asm("r0");
    old_stack = stack_ptr;
    stack_ptr = CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE - sizeof(CYG_ADDRESS);
    new_func = (code_fun*)func;
    new_func();
    stack_ptr = old_stack;
}

static void display_clock_src(void)
{
    diag_printf("\n");
    diag_printf("Clock input is 24 MHz");
}
RedBoot_init(display_clock_src, RedBoot_INIT_LAST);

// ------------------------------------------------------------------------
