#ifndef CYGONCE_HAL_PLATFORM_SETUP_H
#define CYGONCE_HAL_PLATFORM_SETUP_H

//=============================================================================
//
//      hal_platform_setup.h
//
//      Platform specific support for HAL (assembly code)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <pkgconf/system.h>             // System-wide configuration info
#include CYGBLD_HAL_VARIANT_H           // Variant specific configuration
#include CYGBLD_HAL_PLATFORM_H          // Platform specific configuration
#include <cyg/hal/hal_soc.h>            // Variant specific hardware definitions
#include <cyg/hal/hal_mmu.h>            // MMU definitions
#include <cyg/hal/fsl_board.h>          // Platform specific hardware definitions

#if defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1 _platform_setup1

//#define BOOT_FROM_MMC

#if defined(BOOT_FROM_MMC)
#define PLATFORM_PREAMBLE setup_flash_header
//#define MEMORY_MDDR_ENABLE
#endif

#define CYGHWR_HAL_ARM_HAS_MMU

#ifdef CYG_HAL_STARTUP_ROMRAM
#define CYGSEM_HAL_ROM_RESET_USES_JUMP
#endif

#define CYGHWR_HAL_ROM_VADDR    0x0

#if 0
#define UNALIGNED_ACCESS_ENABLE
#define SET_T_BIT_DISABLE
#define LOW_INT_LATENCY_ENABLE
#define BRANCH_PREDICTION_ENABLE
#endif

#define UNALIGNED_ACCESS_ENABLE
#define LOW_INT_LATENCY_ENABLE
#define BRANCH_PREDICTION_ENABLE

//#define TURN_OFF_IMPRECISE_ABORT

// This macro represents the initial startup code for the platform
// r11 is reserved to contain chip rev info in this file
    .macro  _platform_setup1
FSL_BOARD_SETUP_START:
/*
 *       ARM1136 init
 *       - invalidate I/D cache/TLB and drain write buffer;
 *       - invalidate L2 cache
 *       - unaligned access
 *       - branch predictions
 */
#ifdef TURN_OFF_IMPRECISE_ABORT
    mrs r0, cpsr
    bic r0, r0, #0x100
    msr cpsr, r0
#endif

    mrc 15, 0, r1, c1, c0, 0
    bic r1, r1, #(0x3<<21)
    bic r1, r1, #(0x3<<11)
    bic r1, r1, #0x5

#ifndef BRANCH_PREDICTION_ENABLE
    mrc 15, 0, r0, c1, c0, 1
    bic r0, r0, #7
    mcr 15, 0, r0, c1, c0, 1
#else
    mrc 15, 0, r0, c1, c0, 1
    orr r0, r0, #7
    mcr 15, 0, r0, c1, c0, 1
    orr r1, r1, #(1<<11)
#endif

#ifdef UNALIGNED_ACCESS_ENABLE
    orr r1, r1, #(1<<22)
#endif

#ifdef LOW_INT_LATENCY_ENABLE
    orr r1, r1, #(1<<21)
#endif
    mcr 15, 0, r1, c1, c0, 0

#ifdef BRANCH_PREDICTION_ENABLE
    mov r0, #0
    mcr 15, 0, r0, c15, c2, 4
#endif

    mov r0, #0
    mcr 15, 0, r0, c7, c7, 0        /* invalidate I cache and D cache */
    mcr 15, 0, r0, c8, c7, 0        /* invalidate TLBs */
    mcr 15, 0, r0, c7, c10, 4       /* Drain the write buffer */

    /* Also setup the Peripheral Port Remap register inside the core */
    ldr r0, ARM_PPMRR        /* start from AIPS 2GB region */
    mcr p15, 0, r0, c15, c2, 4

    /*** L2 Cache setup/invalidation/disable ***/
    /* Disable L2 cache first */
    mov r0, #L2CC_BASE_ADDR
    ldr r2, [r0, #L2_CACHE_CTL_REG]
    bic r2, r2, #0x1
    str r2, [r0, #L2_CACHE_CTL_REG]
    /*
     * Configure L2 Cache:
     * - 128k size(16k way)
     * - 8-way associativity
     * - 0 ws TAG/VALID/DIRTY
     * - 4 ws DATA R/W
     */
    ldr r1, [r0, #L2_CACHE_AUX_CTL_REG]
    and r1, r1, #0xFE000000
    ldr r2, L2CACHE_PARAM
    orr r1, r1, r2
    str r1, [r0, #L2_CACHE_AUX_CTL_REG]
    ldr r1, ROM_VER_ADDR_W
    ldr r2, [r1]
    cmp r2, #0x1
    /* Workaournd for DDR issue:WT*/
    ldreq r1, [r0, #L2_CACHE_DBG_CTL_REG]
    orreq r1, r1, #2
    streq r1, [r0, #L2_CACHE_DBG_CTL_REG]

    /* Invalidate L2 */
    mov r1, #0x000000FF
    str r1, [r0, #L2_CACHE_INV_WAY_REG]
L2_loop:
    /* Poll Invalidate By Way register */
    ldr r2, [r0, #L2_CACHE_INV_WAY_REG]
    cmp r2, #0
    bne L2_loop
    /*** End of L2 operations ***/

#if defined(BOOT_FROM_MMC)
    mov r0, #MMC_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1] // if MMC is selected, set MXCFIS_FLAG_REG
#else
    mov r0, #SDRAM_NON_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1] // for checking boot source from nand or sdram
#endif
/*
 * End of ARM1136 init
 */
init_spba_start:
    init_spba
init_aips_start:
    init_aips
init_max_start:
    init_max
init_m3if_start:
    init_m3if

/* TODO::
 * Use default setting is more stable then the settinig from IC team for EINCE
 */
    /*init_iomuxc */
#ifndef BOOT_FROM_MMC
    /* If SDRAM has been setup, bypass clock/WEIM setup */
    cmp pc, #SDRAM_BASE_ADDR
    blo init_clock_start
    cmp pc, #(SDRAM_BASE_ADDR + SDRAM_SIZE)
    blo HWInitialise_skip_SDRAM_setup

    mov r0, #NOR_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]
#endif
init_clock_start:
    init_clock
#ifndef BOOT_FROM_MMC
init_cs5_start:
    init_cs5

init_sdram_start:
    /* Assuming DDR memory first */
    setup_sdram
#endif
HWInitialise_skip_SDRAM_setup:

    mov r0, #NFC_BASE
    add r2, r0, #0x1000      // 4K window
    cmp pc, r0
    blo Normal_Boot_Continue
    cmp pc, r2
    bhi Normal_Boot_Continue
NAND_Boot_Start:
    /* Copy image from flash to SDRAM first */
    ldr r1, MXC_REDBOOT_ROM_START

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    blo 1b
    /* Jump to SDRAM */
    ldr r1, CONST_0x0FFF
    and r0, pc, r1     /* offset of pc */
    ldr r1, MXC_REDBOOT_ROM_START
    add r1, r1, #0x10
    add pc, r0, r1
    nop
    nop
    nop
    nop
NAND_Copy_Main:
    mov r0, #NAND_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]
    mov r0, #MXCFIS_NAND
    ldr r1, AVIC_VECTOR1_ADDR_W
    str r0, [r1]

    mov r0, #NFC_BASE;   //r0: nfc base. Reloaded after each page copying
    add r12, r0, #0x1E00  //r12: NFC register base. Doesn't change
    ldrh r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]
    orr r3, r3, #0x1

    /* Setting NFC */
    ldr r7, CCM_BASE_ADDR_W
    ldr r1, [r7, #CLKCTL_RCSR]
    /*BUS WIDTH setting*/
    tst r1, #0x20000000
    orrne r1, r1, #0x4000
    biceq r1, r1, #0x4000

    /*4K PAGE*/
    tst r1, #0x10000000
    orrne r1, r1, #0x200
    bne  1f
    /*2K PAGE*/
    bic r1, r1, #0x200
    tst r1, #0x08000000
    orrne r1, r1, #0x100 /*2KB page size*/
    biceq r1, r1, #0x100 /*512B page size*/
    movne r2, #32 /*64 bytes*/
    moveq r2, #8  /*16 bytes*/
    b NAND_setup
1:
    tst r1, #0x08000000
    bicne r3, r3, #1   /*Enable 8bit ECC mode*/
    movne r2, #109 /*218 bytes*/
    moveq r2, #64  /*128 bytes*/
NAND_setup:
    str r1, [r7, #CLKCTL_RCSR]
    strh r2, [r12, #ECC_RSLT_SPARE_AREA_REG_OFF]
    strh r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]

    //unlock internal buffer
    mov r3, #0x2
    strh r3, [r12, #NFC_CONFIGURATION_REG_OFF]
    //unlock nand device
    mov r3, #0
    strh r3, [r12, #UNLOCK_START_BLK_ADD_REG_OFF]
    sub r3, r3, #1
    strh r3, [r12, #UNLOCK_END_BLK_ADD_REG_OFF]
    mov r3, #4
    strh r3, [r12, #NF_WR_PROT_REG_OFF]

    /* r0: NFC base address. RAM buffer base address. [constantly]
     * r1: starting flash address to be copied. [constantly]
     * r2: page size. [Doesn't change]
     * r3: used as argument.
     * r11: starting SDRAM address for copying. [Updated constantly].
     * r12: NFC register base address. [constantly].
     * r13: end of SDRAM address for copying. [Doesn't change].
     */

    mov r1, #0x1000
    ldr r3, [r7, #CLKCTL_RCSR]
    tst r3, #0x200
    movne r2, #0x1000
    bne 1f
    tst r3, #0x100
    mov r1, #0x800  /*Strang Why is not 4K offset*/
    movne r2, #0x800
    moveq r2, #0x200
1: /*Update the indicator of copy area */
    ldr r11, MXC_REDBOOT_ROM_START
    add r13, r11, #REDBOOT_IMAGE_SIZE
    add r11, r11, r1

Nfc_Read_Page:
    mov r3, #0x0
    nfc_cmd_input

    cmp r2, #0x800
    bhi nfc_addr_ops_4kb
    beq nfc_addr_ops_2kb

    mov r3, r1
    do_addr_input       //1st addr cycle
    mov r3, r1, lsr #9
    do_addr_input       //2nd addr cycle
    mov r3, r1, lsr #17
    do_addr_input       //3rd addr cycle
    mov r3, r1, lsr #25
    do_addr_input       //4th addr cycle
    b end_of_nfc_addr_ops

nfc_addr_ops_2kb:
    mov r3, #0
    do_addr_input       //1st addr cycle
    mov r3, #0
    do_addr_input       //2nd addr cycle
    mov r3, r1, lsr #11
    do_addr_input       //3rd addr cycle
    mov r3, r1, lsr #19
    do_addr_input       //4th addr cycle
    mov r3, r1, lsr #27
    do_addr_input       //5th addr cycle

    mov r3, #0x30
    nfc_cmd_input
    b end_of_nfc_addr_ops

nfc_addr_ops_4kb:
    mov r3, #0
    do_addr_input       //1st addr cycle
    mov r3, #0
    do_addr_input       //2nd addr cycle
    mov r3, r1, lsr #12
    do_addr_input       //3rd addr cycle
    mov r3, r1, lsr #20
    do_addr_input       //4th addr cycle
    mov r3, r1, lsr #27
    do_addr_input       //5th addr cycle

    mov r3, #0x30
    nfc_cmd_input

end_of_nfc_addr_ops:
    mov r8, #0
    bl nfc_data_output
    bl do_wait_op_done
    // Check if x16/2kb page
    cmp r2, #0x800
    bhi nfc_addr_data_output_done_4k
    beq nfc_addr_data_output_done_2k
    beq nfc_addr_data_output_done_512

    // check for bad block
//    mov r3, r1, lsl #(32-17)    // get rid of block number
//    cmp r3, #(0x800 << (32-17)) // check if not page 0 or 1
    b nfc_addr_data_output_done

nfc_addr_data_output_done_4k:
//TODO
    b nfc_addr_data_output_done

nfc_addr_data_output_done_2k:
// end of 4th
    // check for bad block
//TODO    mov r3, r1, lsl #(32-17)    // get rid of block number
//    cmp r3, #(0x800 << (32-17)) // check if not page 0 or 1
    b nfc_addr_data_output_done

nfc_addr_data_output_done_512:
    // check for bad block
// TODO   mov r3, r1, lsl #(32-5-9)    // get rid of block number
// TODO   cmp r3, #(512 << (32-5-9))   // check if not page 0 or 1

nfc_addr_data_output_done:
#if 0
    bhi Copy_Good_Blk
    add r4, r0, #0x1000  //r3 -> spare area buf 0
    ldrh r4, [r4, #0x4]
    and r4, r4, #0xFF00
    cmp r4, #0xFF00
    beq Copy_Good_Blk
    // really sucks. Bad block!!!!
    cmp r3, #0x0
    beq Skip_bad_block
    // even suckier since we already read the first page!
    // Check if x16/2kb page
    cmp r2, #0x800
    // for 4k page
    subhi r11, r11, #0x1000  //rewind 1 page for the sdram pointer
    subhi r1, r1, #0x1000    //rewind 1 page for the flash pointer
    // for 2k page
    subeq r11, r11, #0x800  //rewind 1 page for the sdram pointer
    subeq r1, r1, #0x800    //rewind 1 page for the flash pointer
    // for 512 page
    sublo r11, r11, #512  //rewind 1 page for the sdram pointer
    sublo r1, r1, #512    //rewind 1 page for the flash pointer
Skip_bad_block:
    // Check if x16/2kb page
    ldr r7, CCM_BASE_ADDR_W
    ldr r7, [r7, #CLKCTL_RCSR]
    tst r7, #0x200
    addne r1, r1, #(128*4096)
    bne Skip_bad_block_done
    tst r7, #0x100
    addeq r1, r1, #(32*512)
    addne r1, r1, #(64*2048)
Skip_bad_block_done:
    b Nfc_Read_Page
#endif
Copy_Good_Blk:
    //copying page
    add r2, r2, #NFC_BASE
1:  ldmia r0!, {r3-r10}
    stmia r11!, {r3-r10}
    cmp r0, r2
    blo 1b
    sub r2, r2, #NFC_BASE

    cmp r11, r13
    bge NAND_Copy_Main_done
    // Check if x16/2kb page
    add r1, r1, r2
    mov r0, #NFC_BASE
    b Nfc_Read_Page

NAND_Copy_Main_done:

Normal_Boot_Continue:

#ifdef CYG_HAL_STARTUP_ROMRAM     /* enable running from RAM */
    /* Copy image from flash to SDRAM first */
    ldr r0, =0xFFFFF000
    and r0, r0, pc
    ldr r1, MXC_REDBOOT_ROM_START
    cmp r0, r1
    beq HWInitialise_skip_SDRAM_copy

    add r2, r0, #REDBOOT_IMAGE_SIZE

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    ble 1b
    /* Jump to SDRAM */
    ldr r1, =0xFFFF
    and r0, pc, r1         /* offset of pc */
    ldr r1, =(SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000 + 0x8)
    add pc, r0, r1
    nop
    nop
    nop
    nop
#endif /* CYG_HAL_STARTUP_ROMRAM */

HWInitialise_skip_SDRAM_copy:
   init_cs0

/*
 * Note:
 *     IOMUX/PBC setup is done in C function plf_hardware_init() for simplicity
 */

STACK_Setup:
    // Set up a stack [for calling C code]
    ldr r1, =__startup_stack
    ldr r2, =RAM_BANK0_BASE
    orr sp, r1, r2

    // Create MMU tables
    bl hal_mmu_init

    // Enable MMU
    ldr r2, =10f
    mrc MMU_CP, 0, r1, MMU_Control, c0      // get c1 value to r1 first
    orr r1, r1, #7                          // enable MMU bit
    orr r1, r1, #0x800                      // enable z bit
    mcr MMU_CP, 0, r1, MMU_Control, c0
    mov pc,r2    /* Change address spaces */
    nop
    nop
    nop
10:

    // Save shadow copy of BCR, also hardware configuration
    ldr r1, =_board_BCR
    str r2, [r1]
    ldr r1, =_board_CFG
    str r9, [r1]                // Saved far above...

    .endm                       // _platform_setup1

do_wait_op_done:
    1:
        ldrh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        ands r3, r3, #NAND_FLASH_CONFIG2_INT_DONE
        beq 1b
    bx lr     // do_wait_op_done

nfc_data_output:
    ldrh r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]
    orr r3, r3, #(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN)
    strh r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]

    strh r8, [r12, #RAM_BUFFER_ADDRESS_REG_OFF]

    mov r3, #FDO_PAGE_SPARE_VAL
    strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
    bx lr

#else // defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1
#endif

    /* Do nothing */
    .macro  init_spba
    .endm  /* init_spba */

    /* AIPS setup - Only setup MPROTx registers. The PACR default values are good.*/
    .macro init_aips
        /*
         * Set all MPROTx to be non-bufferable, trusted for R/W,
         * not forced to user-mode.
         */
        ldr r0, AIPS1_CTRL_BASE_ADDR_W
        ldr r1, AIPS1_PARAM_W
        str r1, [r0, #0x00]
        str r1, [r0, #0x04]
        ldr r0, AIPS2_CTRL_BASE_ADDR_W
        str r1, [r0, #0x00]
        str r1, [r0, #0x04]

        /*
         * Clear the on and off peripheral modules Supervisor Protect bit
         * for SDMA to access them. Did not change the AIPS control registers
         * (offset 0x20) access type
         */
        ldr r0, AIPS1_CTRL_BASE_ADDR_W
        ldr r1, =0x0
        str r1, [r0, #0x40]
        str r1, [r0, #0x44]
        str r1, [r0, #0x48]
        str r1, [r0, #0x4C]
        ldr r1, [r0, #0x50]
        and r1, r1, #0x00FFFFFF
        str r1, [r0, #0x50]

        ldr r0, AIPS2_CTRL_BASE_ADDR_W
        ldr r1, =0x0
        str r1, [r0, #0x40]
        str r1, [r0, #0x44]
        str r1, [r0, #0x48]
        str r1, [r0, #0x4C]
        ldr r1, [r0, #0x50]
        and r1, r1, #0x00FFFFFF
        str r1, [r0, #0x50]
    .endm /* init_aips */

    /* MAX (Multi-Layer AHB Crossbar Switch) setup */
    .macro init_max
        ldr r0, MAX_BASE_ADDR_W
        /* MPR - priority is M4 > M2 > M3 > M5 > M0 > M1 */
        ldr r1, MAX_PARAM1
        str r1, [r0, #0x000]        /* for S0 */
        str r1, [r0, #0x100]        /* for S1 */
        str r1, [r0, #0x200]        /* for S2 */
        str r1, [r0, #0x300]        /* for S3 */
        str r1, [r0, #0x400]        /* for S4 */
        /* SGPCR - always park on last master */
        ldr r1, =0x10
        str r1, [r0, #0x010]        /* for S0 */
        str r1, [r0, #0x110]        /* for S1 */
        str r1, [r0, #0x210]        /* for S2 */
        str r1, [r0, #0x310]        /* for S3 */
        str r1, [r0, #0x410]        /* for S4 */
        /* MGPCR - restore default values */
        ldr r1, =0x0
        str r1, [r0, #0x800]        /* for M0 */
        str r1, [r0, #0x900]        /* for M1 */
        str r1, [r0, #0xA00]        /* for M2 */
        str r1, [r0, #0xB00]        /* for M3 */
        str r1, [r0, #0xC00]        /* for M4 */
        str r1, [r0, #0xD00]        /* for M5 */
    .endm /* init_max */

    /* Clock setup */
    .macro    init_clock
        ldr r0, CCM_BASE_ADDR_W

        /* default CLKO to 1/32 of the ARM core*/
        ldr r1, [r0, #CLKCTL_COSR]
        bic r1, r1, #0x00000FF00
        bic r1, r1, #0x0000000FF
        mov r2, #0x00006C00
        add r2, r2, #0x67
        orr r1, r1, r2
        str r1, [r0, #CLKCTL_COSR]

	ldr r2, CCM_CCMR_W
        str r2, [r0, #CLKCTL_CCMR]

        /*check clock path*/
        ldr r3, ROM_VER_ADDR_W
        ldr r4, [r3]
        cmp r4, #0x1
        ldreq r2, [r0, #CLKCTL_PDR0]
        movne r2, #0x1
        tst r2, #0x1
        ldrne r3, MPCTL_PARAM_532_W  /* consumer path*/
        ldreq r3, MPCTL_PARAM_399_W  /* auto path*/

	/*Set MPLL , arm clock and ahb clock*/
        str r3, [r0, #CLKCTL_MPCTL]

        ldr r1, PPCTL_PARAM_W
        str r1, [r0, #CLKCTL_PPCTL]

        cmp r4, #0x1
        ldreq r1, [r0, #CLKCTL_PDR0]
        orreq r1, r1, #0x800000
        streq r1, [r0, #CLKCTL_PDR0]

        ldr r1, CCM_PDR0_W
        bicne r1, r1, #0x800000
        str r1, [r0, #CLKCTL_PDR0]

	ldr r1, [r0, #CLKCTL_CGR0]
	orr r1, r1, #0x00300000
	str r1, [r0, #CLKCTL_CGR0]

	ldr r1, [r0, #CLKCTL_CGR1]
	orr r1, r1, #0x00000C00
	orr r1, r1, #0x00000003
	str r1, [r0, #CLKCTL_CGR1]

    .endm /* init_clock */

    /* M3IF setup */
    .macro init_m3if
        /* Configure M3IF registers */
        ldr r1, M3IF_BASE_W
        /*
        * M3IF Control Register (M3IFCTL)
        * MRRP[0] = L2CC0 not on priority list (0 << 0)        = 0x00000000
        * MRRP[1] = MAX1 not on priority list (0 << 0)        = 0x00000000
        * MRRP[2] = L2CC1 not on priority list (0 << 0)        = 0x00000000
        * MRRP[3] = USB  not on priority list (0 << 0)        = 0x00000000
        * MRRP[4] = SDMA not on priority list (0 << 0)        = 0x00000000
        * MRRP[5] = GPU not on priority list (0 << 0)       = 0x00000000
        * MRRP[6] = IPU1 on priority list (1 << 6)             = 0x00000040
        * MRRP[7] = IPU2 not on priority list (0 << 0)   = 0x00000000
        *                                                       ------------
        *                                                       0x00000040
        */
        ldr r0, =0x00000040
        str r0, [r1]  /* M3IF control reg */
    .endm /* init_m3if */

    .macro init_cs0
	ldr r0, WEIM_CTRL_CS0_W
	ldr r1, CS0_CSCRU_0x0000CC03
	str r1, [r0, #CSCRU]
	ldr r1, CS0_CSCRL_0xA0330D01
	str r1, [r0, #CSCRL]
	ldr r1, CS0_CSCRA_0x00220800
	str r1, [r0, #CSCRA]
    .endm

     /* CPLD on CS5 setup */
    .macro init_cs5
        ldr r0, WEIM_CTRL_CS5_W
        ldr r1, CS5_CSCRU_0x0000D843
        str r1, [r0, #CSCRU]
        ldr r1, CS5_CSCRL_0x22252521
        str r1, [r0, #CSCRL]
        ldr r1, CS5_CSCRA_0x22220A00
        str r1, [r0, #CSCRA]
    .endm /* init_cs5 */

    .macro setup_sdram
        ldr r0, ESDCTL_BASE_W
        mov r3, #0x2000
        str r3, [r0, #0x0]
        ldr r2, ROM_VER_ADDR_W
        ldr r4, [r2]
        cmp r4, #0x1
        streq r3, [r0, #0x8]

	mov r12, #0x00
	mov r2, #0x00
	mov r1, #RAM_BANK0_BASE
	bl setup_sdram_bank
	cmp r3, #0x0
	orreq r12, r12, #1
	eorne r2, r2, #0x1
	blne setup_sdram_bank
#if 0
	/* CSD1 */
	mov r1, #RAM_BANK1_BASE
	bl setup_sdram_bank
	cmp r3, #0x0
	beq 1b
	eorne r2, r2, #0x1
	blne setup_sdram_bank
	orr r12, r12, #1
1:
#endif
	cmp r12, #0
	movne r3, #L2CC_BASE_ADDR
	ldrne r4, [r3, #L2_CACHE_AUX_CTL_REG]
	orrne r4, r4, #0x1000
	strne r4, [r3, #L2_CACHE_AUX_CTL_REG]

	ldr r3, ESDCTL_DELAY5
	str r3, [r0, #0x30]
    .endm

    .macro nfc_cmd_input
        strh r3, [r12, #NAND_FLASH_CMD_REG_OFF]
        mov r3, #NAND_FLASH_CONFIG2_FCMD_EN;
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        bl do_wait_op_done
    .endm   // nfc_cmd_input

    .macro do_addr_input
        and r3, r3, #0xFF
        strh r3, [r12, #NAND_FLASH_ADD_REG_OFF]
        mov r3, #NAND_FLASH_CONFIG2_FADD_EN
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        bl do_wait_op_done
    .endm   // do_addr_input

    /* To support 133MHz DDR */
    .macro  init_iomuxc
	mov r0, #0x2
	ldr r1, IOMUXC_BASE_ADDR_W
	add r1, r1, #0x368
	add r2, r1, #0x4C8 - 0x368
1:      str r0, [r1], #4
	cmp r1, r2
	ble 1b
    .endm /* init_iomuxc */

/*
 * r0: control base, r1: ram bank base
 * r2: ddr type(0:DDR2, 1:MDDR) r3, r4: working
 */
setup_sdram_bank:
	mov r3, #0xE /*0xA + 0x4*/
	tst r2, #0x1
	orreq r3, r3, #0x300 /*DDR2*/
	str r3, [r0, #0x10]
	bic r3, r3, #0x00A
	str r3, [r0, #0x10]
	beq 2f

	mov r3, #0x20000
1:	subs r3, r3, #1
	bne 1b

2:      adr r4, ESDCTL_CONFIG
	tst r2, #0x1
	ldreq r3, [r4, #0x0]
	ldrne r3, [r4, #0x4]
	cmp r1, #RAM_BANK1_BASE
        strlo r3, [r0, #0x4]
        strhs r3, [r0, #0xC]

        ldr r3, ESDCTL_0x92220000
        strlo r3, [r0, #0x0]
        strhs r3, [r0, #0x8]
	mov r3, #0xDA
        ldr r4, RAM_PARAM1_MDDR
        strb r3, [r1, r4]

	tst r2, #0x1
	bne skip_set_mode

	cmp r1, #RAM_BANK1_BASE
	ldr r3, ESDCTL_0xB2220000
        strlo r3, [r0, #0x0]
        strhs r3, [r0, #0x8]
	mov r3, #0xDA
	ldr r4, RAM_PARAM4_MDDR
        strb r3, [r1, r4]
        ldr r4, RAM_PARAM5_MDDR
        strb r3, [r1, r4]
        ldr r4, RAM_PARAM3_MDDR
        strb r3, [r1, r4]
        ldr r4, RAM_PARAM2_MDDR
        strb r3, [r1, r4]

        ldr r3, ESDCTL_0x92220000
        strlo r3, [r0, #0x0]
        strhs r3, [r0, #0x8]
	mov r3, #0xDA
        ldr r4, RAM_PARAM1_MDDR
        strb r3, [r1, r4]

skip_set_mode:
	cmp r1, #RAM_BANK1_BASE
        ldr r3, ESDCTL_0xA2220000
        strlo r3, [r0, #0x0]
        strhs r3, [r0, #0x8]
        mov r3, #0xDA
        strb r3, [r1]
        strb r3, [r1]

        ldr r3, ESDCTL_0xB2220000
        strlo r3, [r0, #0x0]
        strhs r3, [r0, #0x8]
	adr r4, RAM_PARAM6_MDDR
	tst r2, #0x1
	ldreq r4, [r4, #0x0]
	ldrne r4, [r4, #0x4]
        mov r3, #0xDA
        strb r3, [r1, r4]
        ldreq r4, RAM_PARAM7_MDDR
        streqb r3, [r1, r4]
	adr r4, RAM_PARAM3_MDDR
	ldreq r4, [r4, #0x0]
	ldrne r4, [r4, #0x4]
        strb r3, [r1, r4]

	cmp r1, #RAM_BANK1_BASE
        ldr r3, ESDCTL_0x82226080
        strlo r3, [r0, #0x0]
        strhs r3, [r0, #0x8]

	tst r2, #0x1
	moveq r4, #0x20000
	movne r4, #0x200
1:	subs r4, r4, #1
	bne 1b

	str r3, [r1, #0x100]
	ldr r4, [r1, #0x100]
	cmp r3, r4
	movne r3, #1
	moveq r3, #0

	mov pc, lr

#define PLATFORM_VECTORS         _platform_vectors
    .macro  _platform_vectors
        .globl  _board_BCR, _board_CFG
_board_BCR:   .long   0       // Board Control register shadow
_board_CFG:   .long   0       // Board Configuration (read at RESET)
    .endm

//Internal Boot, from MMC/SD cards
#ifdef MXCFLASH_SELECT_MMC
#define DCDGEN(i,type, addr, data) \
dcd_##i:                         ;\
    .long type                   ;\
    .long addr                   ;\
    .long data

#define FHEADER_OFFSET 0x400
#if 0
#define GEN_FHEADERADDR(x) ((x) + FHEADER_OFFSET)
#else
#define GEN_FHEADERADDR(x) (x)
#endif

     .macro setup_flash_header
     b reset_vector
     //   .org 0x400
#if defined(FHEADER_OFFSET)
     .org FHEADER_OFFSET
#endif
app_code_jump_v:    .long GEN_FHEADERADDR(reset_vector)
app_code_barker:    .long 0xB1
app_code_csf:       .long 0
hwcfg_ptr_ptr:      .long GEN_FHEADERADDR(hwcfg_ptr)
super_root_key:     .long 0
hwcfg_ptr:          .long GEN_FHEADERADDR(dcd_data)
app_dest_ptr:       .long SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000
dcd_data:           .long 0xB17219E9
#ifdef MEMORY_MDDR_ENABLE
                    .long 228
//real dcd data

//arm clock is 266Mhz and ahb clock is 133Mhz
//DCDGEN(1, 4, 0x53F80004, 0x00821000)

//WEIM config-CS5 init
DCDGEN(1, 4, 0xB8002054, 0x444a4541)
DCDGEN(1_1, 4, 0xB8002050, 0x0000dcf6)
DCDGEN(1_2, 4, 0xB8002058, 0x44443302)
//MDDR init
//enable mDDR
DCDGEN(2, 4, 0xB8001010, 0x00000004)
//reset delay time
DCDGEN(3, 4, 0xB8001010, 0x0000000C)
DCDGEN(4, 4, 0xB800100C, 0x007ffc3f)
DCDGEN(5, 4, 0xB800100C, 0x007ffc3f)
DCDGEN(6, 4, 0xB8001004, 0x007ffc3f)
DCDGEN(7, 4, 0xB8001000, 0x92220000)
DCDGEN(8, 1, 0x80000400, 0xda)
DCDGEN(9, 4, 0xB8001000, 0xA2220000)
DCDGEN(10, 4, 0x80000000, 0x87654321)
DCDGEN(11, 4, 0x80000000, 0x87654321)
DCDGEN(12, 4, 0xB8001000, 0xB2220000)
DCDGEN(13, 1, 0x80000033, 0xda)
DCDGEN(14, 1, 0x82000000, 0xda)
DCDGEN(15, 4, 0xB8001000, 0x82226080)
DCDGEN(16, 4, 0xB8001010, 0x00000004)
DCDGEN(17, 4, 0xB8001008, 0x00002000)

//add MMC FLASH BOOT mode in MXCFIS_FLAG_REG
//DCDGEN(18, 4, 0x68000100, 0x40000000)

#else
                    .long 240

//arm clock is 266Mhz and ahb clock is 133Mhz
//DCDGEN(1, 4, 0x53F80004, 0x00821000)

//WEIM config-CS5 init
DCDGEN(1, 4, 0xB8002050, 0x0000d843)
DCDGEN(1_1, 4, 0xB8002054, 0x22252521)
DCDGEN(1_2, 4, 0xB8002058, 0x22220a00)

//DDR2 init
DCDGEN(2, 4, 0xB8001010, 0x00000304)
DCDGEN(3, 4, 0xB8001010, 0x0000030C)
DCDGEN(4, 4, 0xB8001004, 0x007ffc3f)
DCDGEN(5, 4, 0xB8001000, 0x92220000)
DCDGEN(6, 4, 0x80000400, 0x12345678)
DCDGEN(7, 4, 0xB8001000, 0xA2220000)
DCDGEN(8, 4, 0x80000000, 0x87654321)
DCDGEN(9, 4, 0x80000000, 0x87654321)
DCDGEN(10, 4, 0xB8001000, 0xB2220000)
DCDGEN(11, 1, 0x80000233, 0xda)
DCDGEN(12, 1, 0x82000780, 0xda)
DCDGEN(13, 1, 0x82000400, 0xda)
DCDGEN(14, 4, 0xB8001000, 0x82226080)
DCDGEN(15, 4, 0xB8001004, 0x007ffc3f)
DCDGEN(16, 4, 0xB800100C, 0x007ffc3f)
DCDGEN(17, 4, 0xB8001010, 0x00000304)
DCDGEN(18, 4, 0xB8001008, 0x00002000)

//add MMC FLASH BOOT mode in MXCFIS_FLAG_REG
//DCDGEN(17, 4, #MMC_FLASH_BOOT, #MXCBOOT_FLAG_REG)
//DCDGEN(19, 4, 0x68000100, 0x40000000)
#endif

//CARD_FLASH_CFG_PARMS_T---length
card_cfg:           .long REDBOOT_IMAGE_SIZE
     .endm
#endif

ARM_PPMRR:              .word   0x40000015
L2CACHE_PARAM:          .word   0x00030024
IIM_SREV_REG_VAL:       .word   IIM_BASE_ADDR + IIM_SREV_OFF
AIPS1_CTRL_BASE_ADDR_W: .word   AIPS1_CTRL_BASE_ADDR
AIPS2_CTRL_BASE_ADDR_W: .word   AIPS2_CTRL_BASE_ADDR
AIPS1_PARAM_W:          .word   0x77777777
MAX_BASE_ADDR_W:        .word   MAX_BASE_ADDR
MAX_PARAM1:             .word   0x00302154
CLKCTL_BASE_ADDR_W:     .word   CLKCTL_BASE_ADDR
ESDCTL_BASE_W:          .word   ESDCTL_BASE
M3IF_BASE_W:            .word   M3IF_BASE
RAM_PARAM1_MDDR:	.word	0x00000400
RAM_PARAM2_MDDR:	.word	0x00000333
RAM_PARAM3_MDDR:	.word	0x02000400
			.word	0x02000000
RAM_PARAM4_MDDR:	.word	0x04000000
RAM_PARAM5_MDDR:	.word	0x06000000
RAM_PARAM6_MDDR:	.word	0x00000233
			.word	0x00000033
RAM_PARAM7_MDDR:	.word	0x02000780
ESDCTL_0x92220000:      .word   0x92220000
ESDCTL_0xA2220000:      .word   0xA2220000
ESDCTL_0xB2220000:      .word   0xB2220000
ESDCTL_0x82226080:      .word   0x82226080
ESDCTL_CONFIG:       	.word   0x007FFC3F	//DDR2
			.word 	0x00295729	//MDDR
ESDCTL_DELAY5:		.word   0x00F49F00
IOMUXC_BASE_ADDR_W:     .word   IOMUXC_BASE_ADDR
CCM_CCMR_W:             .word   0x003F4208
CCM_PDR0_W:             .word   0x00801000
MPCTL_PARAM_399_W:      .word   MPCTL_PARAM_399
MPCTL_PARAM_532_W:      .word   MPCTL_PARAM_532
PPCTL_PARAM_W:    	.word   PPCTL_PARAM_300
AVIC_VECTOR0_ADDR_W:    .word   MXCBOOT_FLAG_REG
AVIC_VECTOR1_ADDR_W:    .word   MXCFIS_FLAG_REG
MXC_REDBOOT_ROM_START:  .word   SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000
CONST_0x0FFF:           .word   0x0FFF
CCM_BASE_ADDR_W:        .word   CCM_BASE_ADDR
IPU_CTRL_BASE_ADDR_W:   .word   IPU_CTRL_BASE_ADDR
WEIM_CTRL_CS5_W:    .word   WEIM_CTRL_CS5
WEIM_CTRL_CS0_W:    .word   WEIM_CTRL_CS0
CS0_CSCRU_0x0000CC03:   .word   0x0000DCF6
CS0_CSCRL_0xA0330D01:   .word   0x444A4541
CS0_CSCRA_0x00220800:   .word   0x44443302
CS5_CSCRU_0x0000D843:   .word   0x0000D843
CS5_CSCRL_0x22252521:   .word   0x22252521
CS5_CSCRA_0x22220A00:   .word   0x22220A00
ROM_VER_ADDR_W:         .word	ROM_BASE_ADDR + ROM_SI_REV_OFFSET
/*---------------------------------------------------------------------------*/
/* end of hal_platform_setup.h                                               */
#endif /* CYGONCE_HAL_PLATFORM_SETUP_H */
