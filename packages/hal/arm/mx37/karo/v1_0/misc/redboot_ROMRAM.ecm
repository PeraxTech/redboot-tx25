cdl_savefile_version 1;
cdl_savefile_command cdl_savefile_version {};
cdl_savefile_command cdl_savefile_command {};
cdl_savefile_command cdl_configuration { description hardware template package };
cdl_savefile_command cdl_package { value_source user_value wizard_value inferred_value };
cdl_savefile_command cdl_component { value_source user_value wizard_value inferred_value };
cdl_savefile_command cdl_option { value_source user_value wizard_value inferred_value };
cdl_savefile_command cdl_interface { value_source user_value wizard_value inferred_value };

cdl_configuration eCos {
    description "RedBoot configuration for Ka-Ro TX37 processor module" ;

    # These fields should not be modified.
    hardware    tx37karo ;
    template    redboot ;
    package -hardware CYGPKG_HAL_ARM current ;
    package -hardware CYGPKG_HAL_ARM_MX37 current ;
    package -hardware CYGPKG_HAL_ARM_TX37KARO current ;
    package -template CYGPKG_HAL current ;
    package -template CYGPKG_INFRA current ;
    package -template CYGPKG_REDBOOT current ;
    package -template CYGPKG_ISOINFRA current ;
    package -template CYGPKG_LIBC_STRING current ;
    package -template CYGPKG_CRC current ;
    package -hardware CYGPKG_IO_ETH_DRIVERS current ;
    package -hardware CYGPKG_DEVS_ETH_ARM_TX37 current ;
    package -hardware CYGPKG_DEVS_ETH_FEC current ;
    package -hardware CYGPKG_COMPRESS_ZLIB current ;
    package -hardware CYGPKG_IO_FLASH current ;
    package -hardware CYGPKG_DEVS_FLASH_ONMXC current ;
    package -template CYGPKG_MEMALLOC current ;
    package -template CYGPKG_DEVS_ETH_PHY current ;
    package -template CYGPKG_LIBC_I18N current ;
    package -template CYGPKG_LIBC_STDLIB current ;
    package -template CYGPKG_ERROR current ;
};

cdl_option CYGFUN_LIBC_STRING_BSD_FUNCS {
    inferred_value 0
};

cdl_option CYGHWR_DEVS_ETH_PHY_LAN8700 {
    inferred_value 1
};

cdl_option CYGIMP_LIBC_RAND_SIMPLE1 {
    user_value 0
};

cdl_option CYGIMP_LIBC_RAND_KNUTH1 {
    user_value 1
};

cdl_option CYGFUN_LIBC_STDLIB_CONV_LONGLONG {
    user_value 0
};

cdl_option CYGIMP_HAL_COMMON_INTERRUPTS_USE_INTERRUPT_STACK {
    inferred_value 0
};

cdl_option CYGDBG_HAL_COMMON_INTERRUPTS_SAVE_MINIMUM_CONTEXT {
    inferred_value 0
};

cdl_option CYGDBG_HAL_COMMON_CONTEXT_SAVE_MINIMUM {
    inferred_value 0
};

cdl_option CYGDBG_HAL_DEBUG_GDB_INCLUDE_STUBS {
    inferred_value 0
};

cdl_option CYGSEM_HAL_ROM_MONITOR {
    inferred_value 1
};

cdl_component CYGBLD_BUILD_REDBOOT {
    inferred_value 1
};

cdl_option CYGBLD_BUILD_REDBOOT_WITH_GDB {
    inferred_value 0
};

cdl_option CYGDAT_REDBOOT_CUSTOM_VERSION {
    inferred_value 1 "Ka-Ro [exec date -I]"
};

cdl_option CYGSEM_REDBOOT_FIS_CRC_CHECK {
    inferred_value 0
};

cdl_option CYGPRI_REDBOOT_ZLIB_FLASH_FORCE {
    inferred_value 1
};

cdl_option CYGBLD_BUILD_REDBOOT_WITH_WINCE_SUPPORT {
    user_value 1
};

cdl_option CYGSEM_REDBOOT_NET_HTTP_DOWNLOAD {
    user_value 0
};

cdl_option CYGPKG_REDBOOT_ANY_CONSOLE {
    inferred_value 0
};

cdl_option CYGSEM_REDBOOT_PLF_ESA_VALIDATE {
    inferred_value 1
};

cdl_option CYGPKG_REDBOOT_MAX_CMD_LINE {
    inferred_value 1024
};

cdl_option CYGBLD_REDBOOT_MIN_IMAGE_SIZE {
    inferred_value 0x00040000
};

cdl_option CYGNUM_REDBOOT_FLASH_SCRIPT_SIZE {
    inferred_value 2048
};

cdl_component CYGPKG_REDBOOT_DISK {
    user_value 0
};

cdl_option CYGNUM_REDBOOT_BOOT_SCRIPT_TIMEOUT_RESOLUTION {
    inferred_value 10
};

cdl_option CYGNUM_REDBOOT_BOOT_SCRIPT_DEFAULT_TIMEOUT {
    inferred_value 1
};

cdl_option CYGHWR_REDBOOT_ARM_LINUX_EXEC_ADDRESS {
    inferred_value 0x40108000
};

cdl_option CYGHWR_REDBOOT_ARM_LINUX_EXEC_ADDRESS_DEFAULT {
    inferred_value 0x40108000
};

cdl_option CYGBLD_ISO_CTYPE_HEADER {
    inferred_value 1 <cyg/libc/i18n/ctype.inl>
};

cdl_option CYGBLD_ISO_ERRNO_CODES_HEADER {
    inferred_value 1 <cyg/error/codes.h>
};

cdl_option CYGBLD_ISO_ERRNO_HEADER {
    inferred_value 1 <cyg/error/errno.h>
};

cdl_option CYGBLD_ISO_STDLIB_STRCONV_HEADER {
    inferred_value 1 <cyg/libc/stdlib/atox.inl>
};

cdl_option CYGBLD_ISO_STDLIB_ABS_HEADER {
    inferred_value 1 <cyg/libc/stdlib/abs.inl>
};

cdl_option CYGBLD_ISO_STDLIB_DIV_HEADER {
    inferred_value 1 <cyg/libc/stdlib/div.inl>
};

cdl_option CYGBLD_ISO_STRERROR_HEADER {
    inferred_value 1 <cyg/error/strerror.h>
};

cdl_option CYGBLD_ISO_STRTOK_R_HEADER {
    inferred_value 1 <cyg/libc/string/string.h>
};

cdl_option CYGBLD_ISO_STRING_LOCALE_FUNCS_HEADER {
    inferred_value 1 <cyg/libc/string/string.h>
};

cdl_option CYGBLD_ISO_STRING_BSD_FUNCS_HEADER {
    inferred_value 1 <cyg/libc/string/bsdstring.h>
};

cdl_option CYGBLD_ISO_STRING_MEMFUNCS_HEADER {
    inferred_value 1 <cyg/libc/string/string.h>
};

cdl_option CYGBLD_ISO_STRING_STRFUNCS_HEADER {
    inferred_value 1 <cyg/libc/string/string.h>
};

cdl_option CYGSEM_IO_FLASH_READ_INDIRECT {
    inferred_value 1
};

cdl_option CYGSEM_IO_FLASH_VERIFY_PROGRAM {
    inferred_value 0
};

cdl_option CYGHWR_DEVS_FLASH_MXC_NOR {
    inferred_value 0
};

cdl_option CYGHWR_DEVS_FLASH_MXC_NAND {
    inferred_value 1
};

cdl_component CYGHWR_FLASH_NAND_BBT_HEADER {
    inferred_value 1 <cyg/io/tx37_nand_bbt.h>
};

cdl_option CYGHWR_DEVS_FLASH_MXC_MULTI {
    inferred_value 0
};

cdl_option CYGSEM_ERROR_PER_THREAD_ERRNO {
    inferred_value 0
};

