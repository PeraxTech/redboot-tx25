//==========================================================================
//
//		tx37_misc.c
//
//		HAL misc board support code for the TX37 board
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <string.h>
#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include <redboot.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>			// base types
#include <cyg/infra/cyg_trac.h>			// tracing macros
#include <cyg/infra/cyg_ass.h>			// assertion macros

#include <cyg/hal/hal_io.h>			// IO macros
#include <cyg/hal/hal_arch.h>			// Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>			// Interrupt names
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_soc.h>			// Hardware definitions
#include CYGBLD_HAL_PLF_DEFS_H			// Platform specifics
#include <cyg/infra/diag.h>			// diag_printf

// All the MM table layout is here:
#include <cyg/hal/hal_mm.h>

void hal_mmu_init(void)
{
	unsigned long ttb_base = RAM_BANK0_BASE + 0x4000;
	unsigned long i;

	/*
	 * Set the TTB register
	 */
	asm volatile ("mcr	p15, 0, %0, c2, c0, 0" : : "r"(ttb_base) /*:*/);

	/*
	 * Set the Domain Access Control Register
	 */
	i = ARM_ACCESS_DACR_DEFAULT;
	asm volatile ("mcr	p15, 0, %0, c3, c0, 0" : : "r"(i) /*:*/);

	/*
	 * First clear all TT entries - ie Set them to Faulting
	 */
	memset((void *)ttb_base, 0, ARM_FIRST_LEVEL_PAGE_TABLE_SIZE);

	/*	     Physical Virtual   Size   Attributes						     Function */
	/*	     Base     Base      MB     cached?		buffered?	  access permissions		      */
	/*	     xxx00000 xxx00000										      */
	X_ARM_MMU_SECTION(0x000, 0x200,	0x200, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* ROM */
	X_ARM_MMU_SECTION(0x100, 0x100,	0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* IRAM */
	X_ARM_MMU_SECTION(0x400, 0x000,	TX37_SDRAM_SIZE >> 20, ARM_CACHEABLE,	ARM_BUFFERABLE,	  ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(0x400, 0x400,	TX37_SDRAM_SIZE >> 20, ARM_CACHEABLE,	ARM_BUFFERABLE,	  ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(0x400, 0x480,	TX37_SDRAM_SIZE >> 20, ARM_UNCACHEABLE,	ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(0x7ff, 0x7ff,	0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* NAND Flash buffer */
	X_ARM_MMU_SECTION(0x800, 0x800,	0x020, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* IPUv3D */
	X_ARM_MMU_SECTION(0xB00, 0xB00,	0x400, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* periperals */
}

//
// Platform specific initialization
//

static inline void set_reg(unsigned long addr, CYG_WORD32 set, CYG_WORD32 clr)
{
	CYG_WORD32 val;
	HAL_READ_UINT32(addr, val);
	val = (val & ~clr) | set;
	HAL_WRITE_UINT32(addr, val);
}

#define GPIO_BASE(grp)		(GPIO1_BASE_ADDR + (((grp) - 1) << 14))
static inline void setup_gpio(int grp, int bit)
{
	set_reg(GPIO_BASE(grp) + GPIO_DR, 0, 1 << bit);
	set_reg(GPIO_BASE(grp) + GPIO_GDIR, 1 << bit, 0);
}

/* GPIOs to set up for TX27/Starterkit-5:
   Function  FCT  GPIO        Pad      IOMUXC SW_PAD  SW_PAD mode
                                       OFFSET  CTRL    MUX
FEC_MDC       4   GPIO3_1  CSPI1_MISO  0x138   0x398
FEC_MDIO      4   GPIO2_23 AUD5_WB_FS  0x130   0x390  0x5a8   0
FEC_RX_CLK    1   GPIO2_1  EIM_CS1     0x058   0x2b8  0x5b0   0
FEC_RX_DV     3   GPIO1_10 EIM_BCLK    0x064   0x2c4  0x5b4   0
FEC_RXD0      4   GPIO2_30 UART1_RI    0x174   0x3d4  0x5ac   1
FEC_RXD1      4   GPIO3_3  CSPI1_SS1   0x140   0x3a0
FEC_RXD2      4   GPIO3_5  CSPI2_MOSI  0x148   0x3a8
FEC_RXD3      4   GPIO3_6  CSPI2_MISO  0x14c   0x3ac
FEC_RX_ER     4   GPIO3_0  CSPI1_MOSI  0x134   0x394  0x5b8   1
FEC_TX_CLK    3   GPIO1_9  EIM_RW      0x068   0x2c8  0x5bc   0
FEC_TX_EN     3   GPIO1_13 EIM_OE      0x050   0x2b0
FEC_TXD0      4   GPIO2_31 UART1_DCD   0x178   0x3d8
FEC_TXD1      4   GPIO3_7  CSPI2_SS0   0x150   0x3b0
FEC_TXD2      4   GPIO3_8  CSPI2_SS1   0x154   0x3b4 :( reference Manual says: 0xBASE_
FEC_TXD3      4   GPIO3_9  CSPI2_SCLK  0x158   0x3b8
FEC_COL       1   GPIO2_0  EIM_CS0     0x054   0x2b4  0x5a0   0
FEC_CRS       4   GPIO3_2  CSPI1_SS0   0x13c   0x39c  0x5a4   1
FEC_TX_ER     4   GPIO3_4  CSPI1_SCLK  0x144   0x3a4

FEC_RESET#    1   GPIO1_7  GPIO1_7     0x22c   0x484
FEC_ENABLE    4   GPIO2_9  NANDF_CS1   0x088   0x2e8
---
OSC26M_ENABLE LP3972 GPIO2
*/
static void fec_gpio_init(void)
{
	/* setup GPIO data register to 0 and DDIR output for FEC PHY pins */
	setup_gpio(3, 1);
	setup_gpio(2, 23);
	setup_gpio(2, 1);
	setup_gpio(1, 10);
	setup_gpio(2, 30);
	setup_gpio(3, 3);
	setup_gpio(3, 5);
	setup_gpio(3, 6);
	setup_gpio(3, 0);
	setup_gpio(1, 9);
	setup_gpio(1, 13);
	setup_gpio(2, 31);
	setup_gpio(3, 7);
	setup_gpio(3, 8);
	setup_gpio(3, 9);
	setup_gpio(2, 0);
	setup_gpio(3, 2);
	setup_gpio(3, 4);

	setup_gpio(1, 7);
	setup_gpio(2, 9);

	/* setup input mux for FEC pins */
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x5a8, 0);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x5b0, 0);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x5b4, 0);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x5ac, 1);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x5b8, 1);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x5bc, 0);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x5a0, 0);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x5a4, 1);

	/* setup FEC PHY pins for GPIO function (with SION set) */
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x138, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x130, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x058, 1 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x064, 3 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x174, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x140, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x148, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x14c, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x134, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x068, 3 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x050, 3 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x178, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x150, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x154, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x158, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x054, 1 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x13c, 4 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x144, 4 | 0x10);

	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x22c, 1 | 0x10);
	HAL_WRITE_UINT32(IOMUXC_BASE_ADDR + 0x088, 4 | 0x10);
}

void plf_hardware_init(void)
{
	unsigned int v;

	fec_gpio_init();

	v = (32 << 16) |	/* spare area size: 32 half words */
		(1 << 12) |		/* 8bit bus */
		(3 << 9) |		/* extra dead cycles [0..7] */
		(1 << 7) |		/* PPB: 0: 32 1: 64 2: 128 3: 256 */
		(1 << 6) |		/* ECC mode: 0: 8bit 1: 4bit */
		(0 << 5) |		/* little endian */
		(1 << 4) |		/* disable interrupt */
		(1 << 3) |		/* enable ECC */
		(0 << 2) |		/* SYM: 0: asymmetric 1: symmetric */
		(1 << 0);		/* PS: 0: 512 1: 2K 2: 4K 3: 4K */
	writel(v, NFC_FLASH_CONFIG2_REG);

	writel(0xFFFF0000, UNLOCK_BLK_ADD0_REG);
	writel(0xFFFF0000, UNLOCK_BLK_ADD1_REG);
	writel(0xFFFF0000, UNLOCK_BLK_ADD2_REG);
	writel(0xFFFF0000, UNLOCK_BLK_ADD3_REG);

	v = NFC_WR_PROT_CS0 | NFC_WR_PROT_BLS_UNLOCK | NFC_WR_PROT_WPC;
	writel(v, NFC_WR_PROT_REG);

	writel(0, NFC_IPC_REG);

	// UART1
	//RXD
	writel(0x0, IOMUXC_BASE_ADDR + 0x15C);
	writel(0x4, IOMUXC_BASE_ADDR + 0x604);
	writel(0x1C5, IOMUXC_BASE_ADDR + 0x3BC);

	//TXD
	writel(0x0, IOMUXC_BASE_ADDR + 0x160);
	writel(0x1C5, IOMUXC_BASE_ADDR + 0x3C0);

	//RTS
	writel(0x0, IOMUXC_BASE_ADDR + 0x164);
	writel(0x4, IOMUXC_BASE_ADDR + 0x600);
	writel(0x1C4, IOMUXC_BASE_ADDR + 0x3C4);

	//CTS
	writel(0x0, IOMUXC_BASE_ADDR + 0x168);
	writel(0x1C4, IOMUXC_BASE_ADDR + 0x3C8);
}

typedef void code_fun(void);

void tx37_program_new_stack(void *func)
{
	register CYG_ADDRESS stack_ptr asm("sp");
	register CYG_ADDRESS old_stack asm("r4");
	register code_fun *new_func asm("r0");
	old_stack = stack_ptr;
	stack_ptr = CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE - sizeof(CYG_ADDRESS);
	new_func = (code_fun*)func;
	new_func();
	stack_ptr = old_stack;
}

static void display_board_info(void)
{
	const char *dlm = "";
	CYG_WORD32 srsr;
	CYG_WORD16 wrsr;

	diag_printf("\nBoard Type: Ka-Ro TX37\n");

	HAL_READ_UINT32(SRC_SRSR_REG, srsr);
	diag_printf("Last RESET cause: ");

	if (srsr & (1 << 16)) {
		diag_printf("%s%s", dlm, "WARM BOOT");
		dlm = " | ";
	}
	if (srsr & (1 << 0)) {
		diag_printf("%s%s", dlm, "POWER_ON");
		dlm = " | ";
	}
	if (srsr & (1 << 2)) {
		diag_printf("%s%s", dlm, "EXTERNAL");
		dlm = " | ";
	}
	if (srsr & (1 << 3)) {
		diag_printf("%s%s", dlm, "COLD");
		dlm = " | ";
	}
	if (srsr & (1 << 4)) {
		HAL_READ_UINT16(WDOG_WRSR_REG, wrsr);
		if (wrsr & (1 << 0)) {
			diag_printf("%s%s", dlm, "SOFTWARE");
			dlm = " | ";
		}
		if (wrsr & (1 << 1)) {
			diag_printf("%s%s", dlm, "WATCHDOG");
			dlm = " | ";
		}
	}
	if (srsr & (1 << 5)) {
		diag_printf("%s%s", dlm, "JTAG");
		dlm = " | ";
	}
	if (*dlm == '\0') {
		diag_printf("Last RESET cause: UNKNOWN: 0x%08x\n", srsr);
	} else {
		diag_printf(" RESET\n");
	}
	return;
}
RedBoot_init(display_board_info, RedBoot_INIT_LAST);
