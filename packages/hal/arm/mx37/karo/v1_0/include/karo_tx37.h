#ifndef CYGONCE_KARO_TX37_H
#define CYGONCE_KARO_TX37_H

//=============================================================================
//
//      Platform specific support (register layout, etc)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/hal/hal_soc.h>        // Hardware definitions

#include CYGHWR_MEMORY_LAYOUT_H

#define SZ_1K           	0x00000400
#define SZ_2K           	0x00000800
#define SZ_4K           	0x00001000
#define SZ_8K           	0x00002000
#define SZ_16K          	0x00004000
#define SZ_32K          	0x00008000
#define SZ_64K          	0x00010000
#define SZ_128K         	0x00020000
#define SZ_256K         	0x00040000
#define SZ_512K         	0x00080000
#define SZ_1M           	0x00100000
#define SZ_2M           	0x00200000
#define SZ_4M           	0x00400000
#define SZ_8M           	0x00800000
#define SZ_16M          	0x01000000
#define SZ_32M          	0x02000000
#define SZ_64M          	0x04000000
#define SZ_128M         	0x08000000
#define SZ_256M         	0x10000000
#define SZ_512M         	0x20000000
#define SZ_1G           	0x40000000


#define RAM_BANK0_BASE		SDRAM_BASE_ADDR
#define TX37_SDRAM_SIZE		SDRAM_SIZE

#define GPIO_DR			0x00
#define GPIO_GDIR		0x04
#define GPIO_PSR		0x08

#define STK5_LED_MASK		(1 << 19)
#define STK5_LED_REG_ADDR	(GPIO2_BASE_ADDR + GPIO_DR)

#define LED_MAX_NUM		1

#define LED_IS_ON(n) ({							\
	CYG_WORD32 __val;							\
	HAL_READ_UINT32(STK5_LED_REG_ADDR, __val);	\
	__val & STK5_LED_MASK;						\
})

#define TURN_LED_ON(n)							\
    CYG_MACRO_START								\
	CYG_WORD32 __val;							\
	HAL_READ_UINT32(STK5_LED_REG_ADDR, __val);	\
	__val |= STK5_LED_MASK;						\
	HAL_WRITE_UINT32(STK5_LED_REG_ADDR, __val);	\
    CYG_MACRO_END

#define TURN_LED_OFF(n)							\
    CYG_MACRO_START								\
	CYG_WORD32 __val;							\
	HAL_READ_UINT32(STK5_LED_REG_ADDR, __val);	\
	__val &= ~STK5_LED_MASK;					\
	HAL_WRITE_UINT32(STK5_LED_REG_ADDR, __val);	\
    CYG_MACRO_END

#define BOARD_DEBUG_LED(n)						\
    CYG_MACRO_START								\
	if (n >= 0 && n < LED_MAX_NUM) {			\
		if (LED_IS_ON(n))						\
			TURN_LED_OFF(n);					\
		else									\
			TURN_LED_ON(n);						\
	}											\
    CYG_MACRO_END

#define BLINK_LED(l, n)							\
    CYG_MACRO_START								\
	int _i;										\
	for (_i = 0; _i < (n); _i++) {				\
		BOARD_DEBUG_LED(l);						\
		HAL_DELAY_US(200000);					\
		BOARD_DEBUG_LED(l);						\
		HAL_DELAY_US(300000);					\
	}											\
	HAL_DELAY_US(1000000);						\
    CYG_MACRO_END

#if !defined(__ASSEMBLER__)
#ifdef CYGOPT_HAL_ARM_TX37_DEBUG // REMOVE ME
extern void plf_dumpmem(unsigned long addr, int len);
#else
static inline void plf_dumpmem(unsigned long addr, int len)
{
}
#endif // CYGOPT_HAL_ARM_TX37_DEBUG

enum {
	BOARD_TYPE_TX37KARO,
};

#define gpio_tst_bit(grp, gpio)		_gpio_tst_bit(grp, gpio, __FUNCTION__, __LINE__)
static inline int _gpio_tst_bit(int grp, int gpio, const char *func, int line)
{
	if (grp < 1 || grp > 3) {
		return 0;
	}
	if (gpio < 0 || gpio > 31) {
		return 0;
	}
	unsigned long val = readl(GPIO1_BASE_ADDR + ((grp - 1) << 14) + GPIO_PSR);
	return !!(val & (1 << gpio));
}

#include <cyg/infra/diag.h>
static inline void gpio_set_bit(int grp, int gpio)
{
	if (grp < 1 || grp > 3) {
		return;
	}
	if (gpio < 0 || gpio > 31) {
		return;
	}
	unsigned long val = readl(GPIO1_BASE_ADDR + ((grp - 1) << 14) + GPIO_DR);
	writel(val | (1 << gpio), GPIO1_BASE_ADDR + ((grp - 1) << 14) + GPIO_DR);
#if 0
	diag_printf("%s: Changing GPIO_DR[%d]@%08lx from %08lx to %08lx\n", __FUNCTION__,
				grp, GPIO1_BASE_ADDR + ((grp - 1) << 14), val, val | (1 << gpio));
#endif
}

static inline void gpio_clr_bit(int grp, int gpio)
{
	if (grp < 1 || grp > 3) {
		return;
	}
	if (gpio < 0 || gpio > 31) {
		return;
	}
	unsigned long val = readl(GPIO1_BASE_ADDR + ((grp - 1) << 14) + GPIO_DR);
	writel(val & ~(1 << gpio), GPIO1_BASE_ADDR + ((grp - 1) << 14) + GPIO_DR);
#if 0
	diag_printf("%s: Changing GPIO_DR[%d]@%08lx from %08lx to %08lx\n", __FUNCTION__,
				grp, GPIO1_BASE_ADDR + ((grp - 1) << 14), val, val & ~(1 << gpio));
#endif
}
#endif /* __ASSEMBLER__ */

#endif /* CYGONCE_KARO_TX37_H */
