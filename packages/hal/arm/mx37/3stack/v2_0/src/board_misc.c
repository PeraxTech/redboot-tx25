//==========================================================================
//
//      board_misc.c
//
//      HAL misc board support code for the board
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include <redboot.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>         // base types
#include <cyg/infra/cyg_trac.h>         // tracing macros
#include <cyg/infra/cyg_ass.h>          // assertion macros

#include <cyg/hal/hal_io.h>             // IO macros
#include <cyg/hal/hal_arch.h>           // Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>           // Interrupt names
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_soc.h>         // Hardware definitions
#include <cyg/hal/fsl_board.h>             // Platform specifics
#include <cyg/io/mxc_i2c.h>
#include <cyg/infra/diag.h>             // diag_printf

// All the MM table layout is here:
#include <cyg/hal/hal_mm.h>

externC void* memset(void *, int, size_t);

volatile void *gpio_mcu1 = (volatile void *)GPIO1_BASE_ADDR;
volatile void *gpio_mcu2 = (volatile void *)GPIO2_BASE_ADDR;
volatile void *gpio_mcu3 = (volatile void *)GPIO3_BASE_ADDR;
volatile void *iomux = (volatile void *)IOMUXC_BASE_ADDR;

/* point to Data Direction Registers (DDIR) of all GPIO ports */
static volatile cyg_uint32 *const data_dir_reg_ptr_array[3] = {
    (cyg_uint32 *) ((cyg_uint32) GPIO1_BASE_ADDR + 4),
    (cyg_uint32 *) ((cyg_uint32) GPIO1_BASE_ADDR + 4),
    (cyg_uint32 *) ((cyg_uint32) GPIO1_BASE_ADDR + 4)
};

/* point to Data Registers (DR) of all GPIO ports */
static volatile unsigned int *const data_reg_ptr_array[3] = {
    (cyg_uint32 *) GPIO1_BASE_ADDR,
    (cyg_uint32 *) GPIO2_BASE_ADDR,
    (cyg_uint32 *) GPIO3_BASE_ADDR
};

/* point to Pad Status Registers (PSR) of all GPIO ports */
static volatile unsigned int *const pad_status_reg_ptr_array[3] = {
    (cyg_uint32 *) ((cyg_uint32) GPIO1_BASE_ADDR + 4 * 2),
    (cyg_uint32 *) ((cyg_uint32) GPIO2_BASE_ADDR + 4 * 2),
    (cyg_uint32 *) ((cyg_uint32) GPIO3_BASE_ADDR + 4 * 2)
};

/* point to IOMUX SW MUX Control Registers*/
static volatile unsigned int *const iomux_sw_mux_ctrl_reg_array[3] = {
    (cyg_uint32 *) ((cyg_uint32) IOMUXC_BASE_ADDR + 0x0008),	//the offset of sw_mux_ctrl_reg_array is 0x0004
    (cyg_uint32 *) ((cyg_uint32) IOMUXC_BASE_ADDR + 0x0230),	//the offset of sw_pad_ctrl_reg_array is 0x0328
    (cyg_uint32 *) ((cyg_uint32) IOMUXC_BASE_ADDR + 0x0508),	//the offset of daisy_sel_in_reg_array is 0x07AC
};

void hal_mmu_init(void)
{
    unsigned long ttb_base = RAM_BANK0_BASE + 0x4000;
    unsigned long i;

    /*
     * Set the TTB register
     */
    asm volatile ("mcr  p15,0,%0,c2,c0,0" : : "r"(ttb_base) /*:*/);

    /*
     * Set the Domain Access Control Register
     */
    i = ARM_ACCESS_DACR_DEFAULT;
    asm volatile ("mcr  p15,0,%0,c3,c0,0" : : "r"(i) /*:*/);

    /*
     * First clear all TT entries - ie Set them to Faulting
     */
    memset((void *)ttb_base, 0, ARM_FIRST_LEVEL_PAGE_TABLE_SIZE);

    /*              Actual   Virtual  Size   Attributes                                                    Function  */
    /*              Base     Base     MB     cached?           buffered?        access permissions                 */
    /*              xxx00000 xxx00000                                                                                */
    X_ARM_MMU_SECTION(0x000, 0x200,   0x200, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* ROM */
    X_ARM_MMU_SECTION(0x100, 0x100,   0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* IRAM */
    X_ARM_MMU_SECTION(0x400, 0x000,   0x080, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
    X_ARM_MMU_SECTION(0x400, 0x400,   0x080, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
    X_ARM_MMU_SECTION(0x7ff, 0x7ff,   0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* NAND Flash buffer */
    X_ARM_MMU_SECTION(0x800, 0x800,   0x020, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* IPUv3D */
    X_ARM_MMU_SECTION(0xB00, 0xB00,   0x400, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* periperals */
}

//
// Platform specific initialization
//

void plf_hardware_init(void)
{
    unsigned int v;

    v = 0x0040174A; // modified
    writel(v, NFC_FLASH_CONFIG2_REG);

    writel(0xFFFF0000, UNLOCK_BLK_ADD0_REG);
    writel(0xFFFF0000, UNLOCK_BLK_ADD1_REG);
    writel(0xFFFF0000, UNLOCK_BLK_ADD2_REG);
    writel(0xFFFF0000, UNLOCK_BLK_ADD3_REG);

    v = NFC_WR_PROT_CS0 | NFC_WR_PROT_BLS_UNLOCK | NFC_WR_PROT_WPC;
    writel(v, NFC_WR_PROT_REG);

    writel(0, NFC_IPC_REG);

#if 0
    /* PBC setup */
    //Enable UART transceivers also reset the Ethernet/external UART
    temp = readw(PBC_BASE + PBC_BCTRL1);

    writew(0x8023, PBC_BASE + PBC_BCTRL1);

    for (i = 0; i < 100000; i++) {
    }

    // clear the reset, toggle the LEDs
    writew(0xDF, PBC_BASE + PBC_BCTRL1_CLR);

    for (i = 0; i < 100000; i++) {
    }

    dummy = readb(0xB4000008);
    dummy = readb(0xB4000007);
    dummy = readb(0xB4000008);
    dummy = readb(0xB4000007);
#endif

#if 0
    /* Reset interrupt status reg */
    writew(0x1F, PBC_INT_REST);
    writew(0x00, PBC_INT_REST);
    writew(0xFFFF, PBC_INT_MASK);
#endif
    // UART1
    //RXD
    writel(0x0, IOMUXC_BASE_ADDR + 0x15C);
    writel(0x4, IOMUXC_BASE_ADDR + 0x604);
    writel(0x1C5, IOMUXC_BASE_ADDR + 0x3BC);

    //TXD
    writel(0x0, IOMUXC_BASE_ADDR + 0x160);
    writel(0x1C5, IOMUXC_BASE_ADDR + 0x3C0);

    //RTS
    writel(0x0, IOMUXC_BASE_ADDR + 0x164);
    writel(0x4, IOMUXC_BASE_ADDR + 0x600);
    writel(0x1C4, IOMUXC_BASE_ADDR + 0x3C4);

    //CTS
    writel(0x0, IOMUXC_BASE_ADDR + 0x168);
    writel(0x1C4, IOMUXC_BASE_ADDR + 0x3C8);
}

static void configure_gpio(cyg_uint32 port, cyg_uint32 pin,
                                              cyg_uint32 io_select, cyg_uint32 dir)
{
    cyg_uint32 tmp, rnum, roffset, cfg_val;

    if ((io_select & 0x200) == 0x200) {
        rnum = (io_select >> 12) & 0xff;
        roffset = (io_select >> 10) & 0x3;
        cfg_val = (io_select & 0xff);
        tmp = iomux_sw_mux_ctrl_reg_array[port][rnum];
        tmp &= ~(0xff << (roffset * 8));
        tmp |= (cfg_val << (roffset * 8));
        iomux_sw_mux_ctrl_reg_array[port][rnum] = tmp;
    }
    if ((io_select & 0x100) == 0x100) {
        /* Configure the direction of GPIO */
        if (dir) {
            *data_dir_reg_ptr_array[port] |= (1 << pin);
        } else {
            *data_dir_reg_ptr_array[port] &= ~(1 << pin);
        }
    }
}

static void configure_pad(cyg_uint32 port, cyg_uint32 reg_index, cyg_uint32 val)
{
    iomux_sw_mux_ctrl_reg_array[port][reg_index] = val;
}

void mxc_mmc_init(base_address)
{
    configure_gpio(IOMUX_SD1_CMD_PORT, IOMUX_SD1_CMD_PIN,
                              IOMUX_SD1_CMD_SEL, IOMUX_SD1_CMD_DIR);
    configure_gpio(IOMUX_SD1_CLK_PORT, IOMUX_SD1_CLK_PIN,
                              IOMUX_SD1_CLK_SEL, IOMUX_SD1_CLK_DIR);
    configure_gpio(IOMUX_SD1_DATA0_PORT,
                              IOMUX_SD1_DATA0_PIN, IOMUX_SD1_DATA0_SEL,
                              IOMUX_SD1_DATA0_DIR);
    configure_gpio(IOMUX_SD1_DATA1_PORT,
                              IOMUX_SD1_DATA1_PIN, IOMUX_SD1_DATA1_SEL,
                              IOMUX_SD1_DATA1_DIR);
    configure_gpio(IOMUX_SD1_DATA2_PORT,
                              IOMUX_SD1_DATA2_PIN, IOMUX_SD1_DATA2_SEL,
                              IOMUX_SD1_DATA2_DIR);
    configure_gpio(IOMUX_SD1_DATA3_PORT,
                              IOMUX_SD1_DATA3_PIN, IOMUX_SD1_DATA3_SEL,
                              IOMUX_SD1_DATA3_DIR);

    configure_gpio(IOMUX_SD2_DATA0_PORT,
                              IOMUX_SD2_DATA0_PIN, IOMUX_SD2_DATA0_SEL,
                              IOMUX_SD2_DATA0_DIR);
    configure_gpio(IOMUX_SD2_DATA1_PORT,
                              IOMUX_SD2_DATA1_PIN, IOMUX_SD2_DATA1_SEL,
                              IOMUX_SD2_DATA1_DIR);
    configure_gpio(IOMUX_SD2_DATA2_PORT,
                             IOMUX_SD2_DATA2_PIN, IOMUX_SD2_DATA2_SEL,
                             IOMUX_SD2_DATA2_DIR);
    configure_gpio(IOMUX_SD2_DATA3_PORT,
                              IOMUX_SD2_DATA3_PIN, IOMUX_SD2_DATA3_SEL,
                              IOMUX_SD2_DATA3_DIR);

    if((system_rev & 0xf) == 0x1) {
        /* for Marley TO1.1.1, WP is not supported because of IC bug */
        /* use default value */
        configure_gpio(IOMUX_PAD_GPIO1_4_PORT,
                                  IOMUX_PAD_GPIO1_4_PIN,
                                  IOMUX_PAD_GPIO1_4_SEL_1,
                                  IOMUX_PAD_GPIO1_4_DIR);
    } else {   /* for TO 1.0 */
        configure_gpio(IOMUX_PAD_GPIO1_4_PORT,
                                  IOMUX_PAD_GPIO1_4_PIN,
                                  IOMUX_PAD_GPIO1_4_SEL,
                                  IOMUX_PAD_GPIO1_4_DIR);
    }

    configure_gpio(IOMUX_PAD_GPIO1_5_PORT,
                              IOMUX_PAD_GPIO1_5_PIN,
                              IOMUX_PAD_GPIO1_5_SEL,
                              IOMUX_PAD_GPIO1_5_DIR);
    configure_gpio(IOMUX_PAD_GPIO1_6_PORT,
                              IOMUX_PAD_GPIO1_6_PIN,
                              IOMUX_PAD_GPIO1_6_SEL,
                              IOMUX_PAD_GPIO1_6_DIR);

     /* Configure PAD setting for MMC/SD */
     configure_pad(1, 65, 0x0190);
     configure_pad(1, 66, 0x00d0);
     configure_pad(1, 67, 0x01d0);
     configure_pad(1, 68, 0x01d0);
     configure_pad(1, 69, 0x01d0);
     configure_pad(1, 70, 0x01A0);
     configure_pad(1, 73, 0x0190);
     configure_pad(1, 74, 0x0190);
     configure_pad(1, 75, 0x0190);
     configure_pad(1, 76, 0x0190);
     configure_pad(1, 146, 0x01e0);
     configure_pad(1, 147, 0x01e0);
     configure_pad(1, 148, 0x0005);
}

#include CYGHWR_MEMORY_LAYOUT_H

typedef void code_fun(void);

void board_program_new_stack(void *func)
{
    register CYG_ADDRESS stack_ptr asm("sp");
    register CYG_ADDRESS old_stack asm("r4");
    register code_fun *new_func asm("r0");
    old_stack = stack_ptr;
    stack_ptr = CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE - sizeof(CYG_ADDRESS);
    new_func = (code_fun*)func;
    new_func();
    stack_ptr = old_stack;
}

static void mx37_3stack_read_pmic_id()
{
    struct mxc_i2c_request rq;
    unsigned char buf[4];

    if (i2c_init(I2C2_BASE_ADDR, 170000) == 0) {
        rq.dev_addr = 0x8;
        rq.reg_addr = 0x7;
        rq.reg_addr_sz = 1;
        rq.buffer_sz = 3;
        rq.buffer = buf;
        i2c_xfer(1, &rq, 1);
        if ((buf[1] == 0x41) && (buf[2] == 0xc8 || buf[2] == 0xc9)) {
            diag_printf("PMIC is Atlas AP Lite\n");
            system_rev |= 0x1 << PMIC_ID_OFFSET;
        } else {
            /* Reinitialize I2C */
            i2c_init(I2C2_BASE_ADDR, 170000);
            rq.dev_addr = (0x34 >> 1);
            rq.reg_addr = 0x0;
            rq.reg_addr_sz = 1;
            rq.buffer_sz = 2;
            rq.buffer = buf;
            i2c_xfer(1, &rq, 1);
            if ((buf[0] == 0x61) && (buf[1] == 0x43)) {
                diag_printf("PMIC is WM8350\n");
            } else {
                diag_printf("Unable to read the PMIC ID, assuming Atlas AP Lite\n");
                system_rev |= 0x1 << PMIC_ID_OFFSET;
            }
        }
    } else {
        diag_printf("Error Initializing I2C2, assuming PMIC to be Atlas AP Lite\n");
        system_rev |= 0x1 << PMIC_ID_OFFSET;
    }

}

RedBoot_init(mx37_3stack_read_pmic_id, RedBoot_INIT_PRIO(900));
