#ifndef CYGONCE_FSL_BOARD_H
#define CYGONCE_FSL_BOARD_H

//=============================================================================
//
//      Platform specific support (register layout, etc)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/hal/hal_soc.h>         // Hardware definitions

#define BOARD_CS_LAN_BASE       (CS4_BASE_ADDR + 0x300)
#define BOARD_CS_UART_BASE      CS5_BASE_ADDR

#define REDBOOT_IMAGE_SIZE      0x40000
#define BOARD_FLASH_START       CS0_BASE_ADDR
#define BOARD_FLASH_SIZE	0x4000000

#define SDRAM_BASE_ADDR         CSD1_BASE_ADDR
#define SDRAM_SIZE              0x04000000
#define RAM_BANK0_BASE          SDRAM_BASE_ADDR
#define EXT_UART_x16

#define PMIC_SPI_BASE           CSPI1_BASE_ADDR
#define BOARD_DEBUG_LED(n) 			do {} while (0)
#endif /* CYGONCE_FSL_BOARD_H */
