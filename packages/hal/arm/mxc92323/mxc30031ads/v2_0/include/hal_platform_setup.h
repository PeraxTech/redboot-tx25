#ifndef CYGONCE_HAL_PLATFORM_SETUP_H
#define CYGONCE_HAL_PLATFORM_SETUP_H

//=============================================================================
//
//      hal_platform_setup.h
//
//      Platform specific support for HAL (assembly code)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <pkgconf/system.h>             // System-wide configuration info
#include CYGBLD_HAL_VARIANT_H           // Variant specific configuration
#include CYGBLD_HAL_PLATFORM_H          // Platform specific configuration
#include <cyg/hal/hal_soc.h>            // Variant specific hardware definitions
#include <cyg/hal/hal_mmu.h>            // MMU definitions
#include <cyg/hal/fsl_board.h>          // Platform specific hardware definitions

#if defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1 _platform_setup1
#define CYGHWR_HAL_ARM_HAS_MMU

#ifdef CYG_HAL_STARTUP_ROMRAM
#define CYGSEM_HAL_ROM_RESET_USES_JUMP
#endif

#define CYGHWR_HAL_ROM_VADDR    0x0
#undef CLOCK_SETUP_ALIGNED
#define NFC_2K_BI_SWAP

// This macro represents the initial startup code for the platform
    .macro  _platform_setup1
FSL_BOARD_SETUP_START:
/*
 *       ARM1136 init
 *       - invalidate I/D cache/TLB and drain write buffer;
 *       - invalidate L2 cache
 *       - unaligned access
 *       - branch predictions
 */
#ifdef TURN_OFF_IMPRECISE_ABORT
    mrs r0, cpsr
    bic r0, r0, #0x100
    msr cpsr, r0
#endif
    mov r0, #0
    mcr 15, 0, r0, c7, c7, 0        /* invalidate I cache and D cache */
    mcr 15, 0, r0, c8, c7, 0        /* invalidate TLBs */
    mcr 15, 0, r0, c7, c10, 4       /* Drain the write buffer */

    /* Also setup the Peripheral Port Remap register inside the core */
    ldr r0, ARM_PPMRR        /* start from AIPS 2GB region */
    mcr p15, 0, r0, c15, c2, 4

    /* Reload data from spare area to 0x400 of main area if booting from NAND */
    ldr r0, NFC_BASE_W
    add r1, r0, #0x400
    cmp pc, r0
    blo 1f
    cmp pc, r1
    bhi 1f
    // Now must boot from NAND
#ifdef MXC_NAND_BOOT_LOAD_AT_0x400
    // Recover the word at 0x400 offset using the one stored in the spare area
    ldr r2, [r0, #0x820]
    str r2, [r1]
#endif
#ifdef NFC_2K_BI_SWAP
    ldr r3, [r0, #0x7D0]    // load word at addr 464 of last 512 RAM buffer
    and r3, r3, #0xFFFFFF00 // mask off the LSB
    ldr r4, [r0, #0x834]    // load word at addr 4 of the 3rd spare area buffer
    mov r4, r4, lsr #8      // shift it to get the byte at addr 5
    and r4, r4, #0xFF       // throw away upper 3 bytes
    add r3, r4, r3          // construct the word
    str r3, [r0, #0x7D0]    // write back
#endif

1:
    /*** L2 Cache setup/invalidation/disable ***/
    /* Disable L2 cache first */
    mov r0, #L2CC_BASE_ADDR
    ldr r2, [r0, #L2_CACHE_CTL_REG]
    bic r2, r2, #0x1
    str r2, [r0, #L2_CACHE_CTL_REG]
    /*
     * Configure L2 Cache:
     * - 128k size(16k way)
     * - 8-way associativity
     * - 0 ws TAG/VALID/DIRTY
     * - 4 ws DATA R/W
     */
    ldr r1, [r0, #L2_CACHE_AUX_CTL_REG]
    and r1, r1, #0xFE000000
    ldr r2, L2CACHE_PARAM
    orr r1, r1, r2
    str r1, [r0, #L2_CACHE_AUX_CTL_REG]

    /* Invalidate L2 */
    mov r1, #0x000000FF
    str r1, [r0, #L2_CACHE_INV_WAY_REG]
L2_loop:
    /* Poll Invalidate By Way register */
    ldr r2, [r0, #L2_CACHE_INV_WAY_REG]
    cmp r2, #0
    bne L2_loop
    /*** End of L2 operations ***/

    mov r0, #SDRAM_NON_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1] // for checking boot source from nand, nor or sdram
/*
 * End of ARM1136 init
 */
init_spba_start:
    init_spba
init_aips_start:
    init_aips
init_max_start:
    init_max
init_m3if_start:
    init_m3if

    init_drive_strength
init_cs0_start:
    init_cs0_async
    // Note: enabling above setup causes the following "mov r0, #NOR_FLASH_BOOT"
    // loading r0=0xb8002000 instead of NOR_FLASH_BOOT(0x0). This can be fixed
    // by force aligning this "mov r0, #NOR_FLASH_BOOT" to 32-byte boundry.
    // Remove it now as it is really not needed anyway.

    // If SDRAM has been setup, bypass clock/WEIM setup
    cmp pc, #SDRAM_BASE_ADDR
    blo init_clock_start
    cmp pc, #(SDRAM_BASE_ADDR + SDRAM_SIZE)
    blo HWInitialise_skip_SDRAM_setup

    mov r0, #NOR_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]

init_clock_start:
    init_clock

init_sdram_start:
    init_ddr_sdram

HWInitialise_skip_SDRAM_setup:
    ldr r0, NFC_BASE_W
    add r2, r0, #0x800      // 2K window
    cmp pc, r0
    blo Normal_Boot_Continue
    cmp pc, r2
    bhi Normal_Boot_Continue
NAND_Boot_Start:
    /* Copy image from flash to SDRAM first */
    ldr r1, MXC_REDBOOT_ROM_START

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    blo 1b
    /* Jump to SDRAM */
    ldr r1, CONST_0x0FFF
    and r0, pc, r1     /* offset of pc */
    ldr r1, MXC_REDBOOT_ROM_START
    add r1, r1, #0x10
    add pc, r0, r1
    nop
    nop
    nop
    nop
    nop

NAND_Copy_Main:
    mov r0, #NAND_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]
    mov r0, #MXCFIS_NAND
    ldr r1, AVIC_VECTOR1_ADDR_W
    str r0, [r1]

    ldr r0, NFC_BASE_W   //r0: nfc base. Reloaded after each page copying
    mov r1, #0x800       //r1: starting flash addr to be copied. Updated constantly
    add r2, r0, #0x800   //r2: end of 3rd RAM buf. Doesn't change
    ldr r11, NFC_IP_BASE_W  //r11: NFC IP register base. Doesn't change
    add r12, r0, #0xE00  //r12: NFC AXI register base. Doesn't change
    ldr r14, MXC_REDBOOT_ROM_START
    add r13, r14, #REDBOOT_IMAGE_SIZE //r13: end of SDRAM address for copying. Doesn't change
    add r14, r14, r1     //r14: starting SDRAM address for copying. Updated constantly

    //unlock internal buffer
    mov r3, #0xFF000000
    add r3, r3, #0x00FF0000
    str r3, [r11, #0x4]
    mov r3, #0x20000
    add r3, r3, #0x4
    str r3, [r11, #0x0]

Nfc_Read_Page:
//  writew(FLASH_Read_Mode1, NAND_ADD_CMD_REG);
    mov r3, #0x0;
    str r3, [r12, #0x0]
    mov r3, #NAND_LAUNCH_FCMD
    str r3, [r12, #0xC]
    do_wait_op_done

//    start_nfc_addr_ops(ADDRESS_INPUT_READ_PAGE, addr, nflash_dev_info->base_mask);
    mov r4, r1, lsl #1
    and r3, r4, #0xFF
    mov r3, r3, lsl #16
    do_addr_input       //1st addr cycle
    mov r3, r4, lsr #8
    and r3, r3, #0xF
    mov r3, r3, lsl #16
    do_addr_input       //2nd addr cycle
    mov r3, r4, lsr #12
    and r3, r3, #0xFF
    mov r3, r3, lsl #16
    do_addr_input       //3rd addr cycle
    mov r3, r4, lsr #20
    mov r3, r3, lsl #16
    do_addr_input       //4th addr cycle

//  writew(FLASH_Read_Mode1_2K, NAND_ADD_CMD_REG);
    mov r3, #0x30;
    str r3, [r12, #0x0]
    mov r3, #NAND_LAUNCH_FCMD
    str r3, [r12, #0xC]
    do_wait_op_done

// write RBA=0 to NFC_CONFIGURATION1
    mov r3, #0
    str r3, [r12, #0x4]

//    writel(mode & 0xFF, NAND_LAUNCH_REG);
    mov r3, #0x8
    str r3, [r12, #0xC]
//        wait_op_done();
    do_wait_op_done
#ifdef NFC_2K_BI_SWAP
    ldr r3, [r0, #0x7D0]    // load word at addr 464 of last 512 RAM buffer
    and r3, r3, #0xFFFFFF00 // mask off the LSB
    ldr r4, [r0, #0x834]    // load word at addr 4 of the 3rd spare area buffer
    mov r4, r4, lsr #8      // shift it to get the byte at addr 5
    and r4, r4, #0xFF       // throw away upper 3 bytes
    add r3, r4, r3          // construct the word
    str r3, [r0, #0x7D0]    // write back
#endif

#if 0
    // check for bad block
    and r3, r1, #(64*2*1024 - 1)    // block mask
    lsr r3, r3, 12
    cmp r3, #0
    bhi Copy_Good_Blk
    add r4, r0, #0x800  //r4 -> spare area buf 0
    ldrh r4, [r4, #0x4]
    and r4, r4, #0xFF00
    cmp r4, #0xFF00
    beq Copy_Good_Blk
    // really sucks. Bad block!!!!
    cmp r3, #0x0
    beq Skip_bad_block
    // even suckier since we already read the first page!
    sub r14, r14, #512  //rewind 1 page for the sdram pointer
    sub r1, r1, #512    //rewind 1 page for the flash pointer
Skip_bad_block:
    add r1, r1, #(32*512)
    b Nfc_Read_Page
#endif

Copy_Good_Blk:
    //copying page
1:  ldmia r0!, {r3-r10}
    stmia r14!, {r3-r10}
    cmp r0, r2
    blo 1b
    cmp r14, r13
    bge NAND_Copy_Main_done
    add r1, r1, #0x800
    ldr r0, NFC_BASE_W
    //mov r0, #NFC_BASE
    b Nfc_Read_Page

NAND_Copy_Main_done:

Normal_Boot_Continue:

#ifdef CYG_HAL_STARTUP_ROMRAM     /* enable running from RAM */
    /* Copy image from flash to SDRAM first */
    ldr r0, =0xFFFFF000
    and r0, r0, pc
    ldr r1, MXC_REDBOOT_ROM_START
    cmp r0, r1
    beq HWInitialise_skip_SDRAM_copy

    add r2, r0, #REDBOOT_IMAGE_SIZE

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    ble 1b
    /* Jump to SDRAM */
    ldr r1, =0xFFFF
    and r0, pc, r1         /* offset of pc */
    ldr r1, =(SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000 + 0x8)
    add pc, r0, r1
    nop
    nop
    nop
    nop
#endif /* CYG_HAL_STARTUP_ROMRAM */

HWInitialise_skip_SDRAM_copy:
    //init_dsp
init_cs4_start:
    init_cs4
init_cs5_start:
    init_cs5
init_cs0_sync_start:
    init_cs0_sync

NAND_ClockSetup:

/*
 * Note:
 *     IOMUX/PBC setup is done in C function plf_hardware_init() for simplicity
 */

STACK_Setup:
    // Set up a stack [for calling C code]
    ldr r1, =__startup_stack
    ldr r2, =RAM_BANK0_BASE
    orr sp, r1, r2

    // Create MMU tables
    bl hal_mmu_init

    // Enable MMU
    ldr r2, =10f
    mrc MMU_CP, 0, r1, MMU_Control, c0      // get c1 value to r1 first
    orr r1, r1, #7                          // enable MMU bit
    mcr MMU_CP, 0, r1, MMU_Control, c0
    mov pc,r2    /* Change address spaces */
    nop
    nop
    nop
10:

    // Save shadow copy of BCR, also hardware configuration
    ldr r1, =_board_BCR
    str r2, [r1]
    ldr r1, =_board_CFG
    str r9, [r1]                // Saved far above...

    .endm                       // _platform_setup1

#else // defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1
#endif

    /* Allow all 3 masters to have access to these shared peripherals */
    .macro  init_spba
        ldr r0, SPBA_CTRL_BASE_ADDR_W
        add r4, r0, #0x74
        add r5, r0, #0x3C
        ldr r1, =0x7            /* allow all 3 masters access */
        ldr r2, SPBA_LOCK_VAL
spba_continue:
        str r1, [r0]
spba_check_loop:
        cmp r0, r5
        beq spba_skip_check
        ldr r3, [r0]
        cmp r2, r3
        bne spba_check_loop
spba_skip_check:
        add r0, r0, #4
        cmp r0, r4
        ble spba_continue



    .endm  /* init_spba */

    /* AIPS setup - Only setup MPROTx registers. The PACR default values are good.*/
    .macro init_aips
        /*
         * Set all MPROTx to be non-bufferable, trusted for R/W,
         * not forced to user-mode.
         */
        ldr r0, AIPS1_CTRL_BASE_ADDR_W
        ldr r1, AIPS1_PARAM_W
        str r1, [r0, #0x00]
        str r1, [r0, #0x04]
        ldr r0, AIPS2_CTRL_BASE_ADDR_W
        str r1, [r0, #0x00]
        str r1, [r0, #0x04]

        /*
         * Clear the on and off peripheral modules Supervisor Protect bit
         * for SDMA to access them. Did not change the AIPS control registers
         * (offset 0x20) access type
         */
        ldr r0, AIPS1_CTRL_BASE_ADDR_W
        ldr r1, =0x0
        str r1, [r0, #0x40]
        str r1, [r0, #0x44]
        str r1, [r0, #0x48]
        str r1, [r0, #0x4C]
        ldr r1, [r0, #0x50]
        and r1, r1, #0x00FFFFFF
        str r1, [r0, #0x50]

        ldr r0, AIPS2_CTRL_BASE_ADDR_W
        ldr r1, =0x0
        str r1, [r0, #0x40]
        str r1, [r0, #0x44]
        str r1, [r0, #0x48]
        str r1, [r0, #0x4C]
        ldr r1, [r0, #0x50]
        and r1, r1, #0x00FFFFFF
        str r1, [r0, #0x50]
    .endm /* init_aips */

    /* MAX (Multi-Layer AHB Crossbar Switch) setup */
    .macro init_max
        ldr r0, MAX_BASE_ADDR_W
        /* MPR - priority is M3 > M2 > M0 > M1 > M4 > M5 > */
        ldr r1, MAX_PARAM1
        str r1, [r0, #0x000]        /* for S0 */
        str r1, [r0, #0x100]        /* for S1 */
        str r1, [r0, #0x200]        /* for S2 */
        str r1, [r0, #0x300]        /* for S3 */
        str r1, [r0, #0x400]        /* for S4 */
        /* SGPCR - always park on last master */
        ldr r1, =0x10
        str r1, [r0, #0x010]        /* for S0 */
        str r1, [r0, #0x110]        /* for S1 */
        str r1, [r0, #0x210]        /* for S2 */
        str r1, [r0, #0x310]        /* for S3 */
        str r1, [r0, #0x410]        /* for S4 */
        /* MGPCR - restore default values */
        ldr r1, =0x0
        str r1, [r0, #0x800]        /* for M0 */
        str r1, [r0, #0x900]        /* for M1 */
        str r1, [r0, #0xA00]        /* for M2 */
        str r1, [r0, #0xB00]        /* for M3 */
        str r1, [r0, #0xC00]        /* for M4 */
        str r1, [r0, #0xD00]        /* for M5 */

        ldr r0, MAX_SP_BASE_ADDR_W
        /* MPR - priority is M2 > M1 > M3 > M0 */
        ldr r1, MAX_SP_PARAM1
        str r1, [r0, #0x000]        /* for S0 */
        /* SGPCR - always park on last master */
        ldr r1, =0x10
        str r1, [r0, #0x010]        /* for S0 */
        /* MGPCR - restore default values */
        ldr r1, =0x0
        str r1, [r0, #0x800]        /* for M0 */
        str r1, [r0, #0x900]        /* for M1 */
        str r1, [r0, #0xA00]        /* for M2 */
        str r1, [r0, #0xB00]        /* for M3 */
    .endm /* init_max */

    /* Clock setup */
    .macro init_clock
        /*
         * Clock setup
         * After this step, AP domain is running out of PLL0 with:

           Module           Freq (MHz)   Note
           =========================================================================
           ARM core         399          ap_clk
           AHB              133          known ahb_clk or ahb_com_clk
           IP               66.5         ap_pclk and ap_com_pclk
           EMI              166          emi_clk

         * All other clocks can be figured out based on this.
         */
        /*
        * Switch to patref
        */

        ldr r0, CRM_AP_BASE_ADDR_W
        mov r1, #0x00000210
        add r1, r1, #0x00000001
        str r1, [r0, #CRM_AP_ACDR]
        ldr r1, =0x0
        str r1, [r0, #CRM_AP_ACSR]

        /*
        * Step 2: Setup PLL0 - for AP domain.
        */
        ldr r0, PLL0_BASE_ADDR_W

        //ldr r1, =0x1022
        ldr r1, =0x22
        str r1, [r0, #PLL_DP_CTL]     /* Set DPLL ON (set UPEN bit); BRMO=1 */
        ldr r1, =0x2
        str r1, [r0, #PLL_DP_CONFIG]  /* Enable auto-restart AREN bit */

        /*
        * Set PLL0 to be 100MHz.
        * MFI=5, PDF=1, MFD=25, MFN=3 ->
        * PLL0=2*26MHzInput*(5+10/(12+1))/(2+1)=266 MHz
        */
        ldr r1, =0x52
        str r1, [r0, #PLL_DP_OP]
        ldr r1, =0xC
        str r1, [r0, #PLL_DP_MFD]
        ldr r1, =0xA
        str r1, [r0, #PLL_DP_MFN]

        ldr r1, =0x52
        str r1, [r0, #PLL_DP_HFS_OP]
        ldr r1, =0xC
        str r1, [r0, #PLL_DP_HFS_MFD]
        ldr r1, =0xA
        str r1, [r0, #PLL_DP_HFS_MFN]
#if 0
        /*
        * Set PLL0 to be 664MHz.
        * MFI=6, PDF=0, MFD=12, MFN=5 ->
        * PLL0=4*26MHzInput*(6+5/(12+1))/(0+1)=664 MHz
        */
        ldr r1, =0x60
        str r1, [r0, #PLL_DP_OP]
        ldr r1, =0x0C
        str r1, [r0, #PLL_DP_MFD]
        ldr r1, =0x05
        str r1, [r0, #PLL_DP_MFN]

        ldr r1, =0x60
        str r1, [r0, #PLL_DP_HFS_OP]
        ldr r1, =0x0C
        str r1, [r0, #PLL_DP_HFS_MFD]
        ldr r1, =0x05
        str r1, [r0, #PLL_DP_HFS_MFN]
#endif
        /* Now restart DPLL */
        //ldr r1, =0x1032
        ldr r1, =0x32
        str r1, [r0, #PLL_DP_CTL]
wait_pll0_lock:
        ldr r1, [r0, #PLL_DP_CTL]
        ands r1, r1, #0x1
        beq wait_pll0_lock

        /*
        * Step 2: Setup PLL1.
        */
        ldr r0, PLL1_BASE_ADDR_W

        ldr r1, =0x1000
        add r1, r1, #0x22
        str r1, [r0, #PLL_DP_CTL]     /* Set DPLL ON (set UPEN bit); BRMO=1 */
        ldr r1, =0x2
        str r1, [r0, #PLL_DP_CONFIG]  /* Enable auto-restart AREN bit */

        /*
        * Set PLL1 to be 532MHz.
        * MFI=5, PDF=0, MFD=25, MFN=3 ->
        * PLL0=4*26MHzInput*(5+3/(25+1))/(0+1)=532 MHz
        */
        ldr r1, =0x50
        str r1, [r0, #PLL_DP_OP]
        ldr r1, =0x19
        str r1, [r0, #PLL_DP_MFD]
        ldr r1, =0x03
        str r1, [r0, #PLL_DP_MFN]

        ldr r1, =0x50
        str r1, [r0, #PLL_DP_HFS_OP]
        ldr r1, =0x19
        str r1, [r0, #PLL_DP_HFS_MFD]
        ldr r1, =0x03
        str r1, [r0, #PLL_DP_HFS_MFN]

        /* Now restart DPLL */
        ldr r1, =0x1000
        add r1, r1, #0x32
        str r1, [r0, #PLL_DP_CTL]
wait_pll1_lock:
        ldr r1, [r0, #PLL_DP_CTL]
        ands r1, r1, #0x1
        beq wait_pll1_lock

        /*
        * Set PLL_USB to 240MHz
        */
        ldr r0, PLL2_BASE_ADDR_W

        ldr r1, =0x22
        str r1, [r0, #PLL_DP_CTL]     /* Set DPLL ON (set UPEN bit); BRMO=1 */
        ldr r1, =0x2
        str r1, [r0, #PLL_DP_CONFIG]  /* Enable auto-restart AREN bit */

        /*
        * MFI=9, PDF=1, MFD=12, MFN=3 ->
        * PLL2 = 2*26*(9+3/(12+1))/(1+1)=240 MHz
        */
        ldr r1, =0x91
        str r1, [r0, #PLL_DP_OP]
        ldr r1, =0xC
        str r1, [r0, #PLL_DP_MFD]
        ldr r1, =0x3
        str r1, [r0, #PLL_DP_MFN]

        ldr r1, =0x91
        str r1, [r0, #PLL_DP_HFS_OP]
        ldr r1, =0xC
        str r1, [r0, #PLL_DP_HFS_MFD]
        ldr r1, =0x3
        str r1, [r0, #PLL_DP_HFS_MFN]

        /* Now restart DPLL */
        ldr r1, =0x32
        str r1, [r0, #PLL_DP_CTL]

wait_pll2_lock:
        ldr r1, [r0, #PLL_DP_CTL]
        ands r1, r1, #0x1
        beq wait_pll2_lock
        /* End of PLL_USB setup */

        /*
        * Step 3: switching to PLL_AP for AP domain and restore default register values.
        */
        // AP CKO/CKOH selected
        ldr r0, SRC_BASE_ADDR_W
        ldr r1, [r0, #SRC_SSCR]
        orr r1, r1, #0x60000
        str r1, [r0, #SRC_SSCR]
        /* Default CKOH as AP_CLK with div by 10 */
        ldr r0, CRM_AP_BASE_ADDR_W
        ldr r1, [r0, #CRM_AP_ACR]
        bic r1, r1, #0xFF00
        bic r1, r1, #0x00FF
        orr r1, r1, #0x02C0
        str r1, [r0, #CRM_AP_ACR]

        /* Select PLL core for CS and USB CLK */
        mov r1, #0x83
        str r1, [r0, #CRM_AP_ASCSR]
        /*Dividers setup */
        mov r1, #0x010000
        add r1, r1, #0x0000200
        add r1, r1, #0x00000011
        str r1, [r0, #CRM_AP_ACDR]      /* ARM core=399MHz, AHB=133MHz, IP=66.5MHz */
        ldr r1, =0x1
        str r1, [r0, #CRM_AP_ACSR]      /* select DPLL for AP domain at new freq */
        ldr r1, CRM_AP_ACDER_W
        str r1, [r0, #CRM_AP_ACDER]     /* set nfc_div=5 (5+1 actual divider)  */
        ldr r1, CRM_AP_APR_W
        str r1, [r0, #CRM_AP_APR]
        mov r1, #0x7
        str r1, [r0, #CRM_AP_ACGCR]     /* restore default */
        mov r1, #0x5
        str r1, [r0, #CRM_AP_ARCGR]    /* restore default */
        mov r1, #0x0
        str r1, [r0, #CRM_AP_AMORA]    /* restore default */
        str r1, [r0, #CRM_AP_AMORB]    /* restore default */
        str r1, [r0, #CRM_AP_AMORC]    /* restore default */
    .endm /* init_clock */

    /* M3IF setup */
    .macro init_m3if
#if 0
        /* Configure M3IF registers */
        ldr r1, M3IF_BASE_W
        /*
        * M3IF Control Register (M3IFCTL)
        * MRRP[0] = TMAX not on priority list (0 << 0)        = 0x00000000
        * MRRP[1] = SMIF not on priority list (0 << 0)        = 0x00000000
        * MRRP[2] = MAX0 not on priority list (0 << 0)        = 0x00000000
        * MRRP[3] = MAX1 not on priority list (0 << 0)        = 0x00000000
        * MRRP[4] = SDMA not on priority list (0 << 0)        = 0x00000000
        * MRRP[5] = MPEG4 not on priority list (0 << 0)       = 0x00000000
        * MRRP[6] = IPU on priority list (1 << 6)             = 0x00000040
        * MRRP[7] = SMIF-L2CC not on priority list (0 << 0)   = 0x00000000
        *                                                       ------------
        *                                                       0x00000040
        */
        ldr r0, =0x00000040
        str r0, [r1]  /* M3IF control reg */
#endif
    .endm /* init_m3if */

    /* CS0 sync mode setup */
    .macro init_cs0_sync
#if 0
        // setup the sync mode in the flash itself first
        mov r0, #CS0_BASE_ADDR
        add r0, r0, #0x00004300
        mov r1, #0x60
        strh r1, [r0, #0x9E]
        mov r1, #0x03
        strh r1, [r0, #0x9E]
        // setup the sync mode in the WEIM
        ldr r0, WEIM_BASE_ADDR_W
        ldr r1, CS0_CSCRU_0x23D29000
        str r1, [r0, #CSCRU]
        ldr r1, CS0_CSCRL_0x60000D01
        str r1, [r0, #CSCRL]
        mov r1, #0x00000080
        str r1, [r0, #CSCRA]
#endif
    .endm /* init_cs0_sync */

    /* CS0 async mode setup */
    .macro init_cs0_async

        // Configure CS0 CS0GCR1
        ldr r0, WEIM_BASE_ADDR_W
        ldr r1, CS0_CSGCR1_0x00411A91
        str r1, [r0, #CSGCR1]
        // Configure CS0 CS0GCR2
        ldr r1, =0x0
        str r1, [r0, #CSGCR2]
        // Configure CS0 CS0RCR1
        ldr r1, CS0_CSRCR1_0x0E007000
        str r1, [r0, #CSRCR1]
        // Configure CS0 CS0RCR2
        ldr r1, =0x0
        str r1, [r0, #CSRCR2]
        // Configure CS0 CS0WCR1
        ldr r1, CS0_CSWCR1_0x061C0000
        str r1, [r0, #CSWCR1]

        // configure memory RCR
        mov r0, #CS0_BASE_ADDR
        mov r1, #0xFF
        strh r1, [r0]

        add r0, r0, #0x0001A000
        add r0, r0, #0x00000200
        mov r1, #0x60
        strh r1, [r0, #0xE]
        mov r1, #0x03
        strh r1, [r0, #0xE]

        // put memory in Read Array mode
        mov r0, #CS0_BASE_ADDR
        mov r1, #0xFF
        strh r1, [r0]
    .endm /* init_cs0_async */

    /* CS4 setup */
    .macro init_cs4
        ldr r0, =0x5006C014
        ldr r1, =0x00001000
        str r1, [r0]
        ldr r0, =WEIM_CTRL_CS4
        ldr r1, =0x00010081
        str r1, [r0, #CSGCR1]
        ldr r1, =0x00000002
        str r1, [r0, #CSGCR2]
        ldr r1, =0x1C0A2400
        str r1, [r0, #CSRCR1]
        ldr r1, =0x00000019
        str r1, [r0, #CSRCR2]
        ldr r1, =0x9C092480
        str r1, [r0, #CSWCR1]

        ldr r0, =0xB400000A
        ldr r1, =0x00000114
        strh r1, [r0]
   .endm /* init_cs4 */

    /* CS5 setup */
    .macro init_cs5
        ldr r0, IOMUXC_BASE_ADDR_W
        ldr r1, [r0, #0x600]
        orr r1, r1, #0x1000
        str r1, [r0, #0x600]

        ldr r0, =WEIM_CTRL_CS5
        ldr r1, =0x00010081
        str r1, [r0, #CSGCR1]
        ldr r1, =0x00000002
        str r1, [r0, #CSGCR2]
        ldr r1, =0x1C0A0000
        str r1, [r0, #CSRCR1]
        ldr r1, =0x00000000
        str r1, [r0, #CSRCR2]
        ldr r1, =0x9C092480
        str r1, [r0, #CSWCR1]
    .endm /* init_cs5 */

// DDR SDRAM setup
    .macro  init_ddr_sdram
        ldr r0, ESDCTL_BASE_W
        mov r12, #SDRAM_BASE_ADDR

        //delay line setting for Write - offset = 0 instead of -3(default)
        mov r1, #0x0
        str r1, [r0, #ESDCTL_ESDCDLY5]
        ldr r1, SDRAM_0x82128000
        str r1, [r0, #ESDCTL_ESDCTL0]

        //DIS_init_166 Vinz inits
        ldr r1, SDRAM_0x00008004
        str r1, [r0, #ESDCTL_ESDSCR]
        ldr r1, SDRAM_0x82120000
        str r1, [r0, #ESDCTL_ESDCTL1]
        ldr r1, SDRAM_0x208967a9
        str r1, [r0, #ESDCTL_ESDCFG1]
        ldr r1, SDRAM_0x00000684
        str r1, [r0, #ESDCTL_ESDMISC]
        ldr r1, SDRAM_0x0400800C
        str r1, [r0, #ESDCTL_ESDSCR]
        ldr r1, SDRAM_0x00008014
        str r1, [r0, #ESDCTL_ESDSCR]
        str r1, [r0, #ESDCTL_ESDSCR]
        ldr r1, SDRAM_0x0033801C
        str r1, [r0, #ESDCTL_ESDSCR]
        ldr r1, SDRAM_0x0000801E
        str r1, [r0, #ESDCTL_ESDSCR]
        ldr r1, SDRAM_0x00008004
        str r1, [r0, #ESDCTL_ESDSCR]
        ldr r1, SDRAM_0x82128000
        str r1, [r0, #ESDCTL_ESDCTL1]
        mov r1, #0x4
        str r1, [r0, #ESDCTL_ESDSCR]

        //Discrete DDR 166 MHz simple write testing
        ldr r1, SDRAM_0xDEADBEEF
        str r1, [r12]
        ldr r1, SDRAM_0xC001DEAD
        str r1, [r12, #0x4]
        ldr r1, SDRAM_0xDEADC001
        str r1, [r12, #0x8]
        ldr r1, SDRAM_0xFFFFFFFF
        str r1, [r12, #0xC]
        ldr r1, SDRAM_0x55555555
        str r1, [r12, #0x10]
        ldr r1, SDRAM_0xAAAAAAAA
        str r1, [r12, #0x14]
        mov r1, #0x0
        str r1, [r12, #0x18]
    .endm

    .macro do_wait_op_done
    1:
        ldr r3, [r11, #0x18]
        ands r3, r3, #NFC_IPC_INT
        beq 1b
        mov r3, #0x0
        str r3, [r11, #0x18]
    .endm   // do_wait_op_done

    .macro do_addr_input
        str r3, [r12, #0x0]
        mov r3, #NAND_LAUNCH_FADD
        str r3, [r12, #0xC]
        do_wait_op_done
    .endm   // do_addr_input

    /* To support 133MHz SDR */
    .macro  init_drive_strength
        // Increase drive strength
        ldr r0, IOMUXC_BASE_ADDR_W
        mov r1, #0x00060000
        add r1, r1, #0x86
        str r1, [r0, #0x200]
        /* Add some delay */
        mov r1, #0x1000
    1:
        subs r1, r1, #0x1
        bne 1b
    .endm /* init_drive_strength */

    .macro init_dsp
/*
 * Deal with DSP reset
 */
        /* Set DSP to LE */
        ldr r0, =0x5001C808
        ldr r1, [r0]
        tst r1, #(1 << 5)
        beq skip_dsp_switch_le
        bic r1, r1, #(1 << 5)
        str r1, [r0]
    skip_dsp_switch_le:
        ldr r0, =0x43F84024
        /* Put DSP in reset */
        ldr r1, =0x00000010
        str r1, [r0]

        /* Hold for some time */
        ldr r2, =0x80000
dsp_reset_delay:
        subs r2, r2, #0x1
        bne dsp_reset_delay

        /* Put DSP out of reset */
        ldr r1, =0x0
        str r1, [r0]
    .endm /* init_dsp */
#define PLATFORM_VECTORS         _platform_vectors
    .macro  _platform_vectors
        .globl  _board_BCR, _board_CFG
_board_BCR:   .long   0       // Board Control register shadow
_board_CFG:   .long   0       // Board Configuration (read at RESET)
    .endm

#define PLATFORM_PREAMBLE _switch_to_le

    .macro  _switch_to_le
        .word 0xEE110F10        // mrc 15, 0, r0, c1, c0, 0
        .word 0xE3C00080        // bic r0, r0, #0x80
        .word 0xEE010F10        // mcr 15, 0, r0, c1, c0, 0

        .word 0x0F10EE11        // mrc 15, 0, r0, c1, c0, 0
        .word 0x0080E3C0        // bic r0, r0, #0x80
        .word 0x0F10EE01        // mcr 15, 0, r0, c1, c0, 0
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
        .word 0                 // dummy
    .endm

ARM_PPMRR:              .word   0x40000015
L2CACHE_PARAM:          .word   0x00030024
AIPS1_CTRL_BASE_ADDR_W: .word   AIPS1_CTRL_BASE_ADDR
AIPS2_CTRL_BASE_ADDR_W: .word   AIPS2_CTRL_BASE_ADDR
AIPS1_PARAM_W:          .word   0x77777777
MAX_BASE_ADDR_W:        .word   MAX_BASE_ADDR
MAX_PARAM1:             .word   0x00540132
MAX_SP_BASE_ADDR_W:     .word   MAX_SP_BASE_ADDR
MAX_SP_PARAM1:          .word   0x00002013
CLKCTL_BASE_ADDR_W:     .word   CLKCTL_BASE_ADDR
PLL0_BASE_ADDR_W:       .word   PLL_AP_BASE_ADDR
CRM_AP_BASE_ADDR_W:     .word   CRM_AP_BASE_ADDR
SRC_BASE_ADDR_W:        .word   SRC_BASE_ADDR
PLL1_BASE_ADDR_W:       .word   PLL_BP_BASE_ADDR
PLL2_BASE_ADDR_W:       .word   PLL_USB_BASE_ADDR
SPBA_CTRL_BASE_ADDR_W:  .word   SPBA_CTRL_BASE_ADDR
SPBA_LOCK_VAL:          .word   0xC0010007
ESDCTL_BASE_W:          .word   ESDCTL_BASE
M3IF_BASE_W:            .word   M4IF_BASE
NFC_BASE_W:             .word   NFC_BASE
NFC_IP_BASE_W:          .word   NFC_IP_BASE
CRM_AP_ACDER_W:         .word   0x5DC62C2C
CRM_AP_APR_W:           .word   0x444486F6
SDRAM_0x00008004:       .word   0x00008004
SDRAM_0x82120000:       .word   0x82120000
SDRAM_0x208967a9:       .word   0x208967a9
SDRAM_0x00000684:       .word   0x00000684
SDRAM_0x0400800C:       .word   0x0400800C
SDRAM_0x00008014:       .word   0x00008014
SDRAM_0x0033801C:       .word   0x0033801C
SDRAM_0x0000801E:       .word   0x0000801E
SDRAM_0x82128000:       .word   0x82128000
SDRAM_0xDEADBEEF:       .word   0xDEADBEEF
SDRAM_0xC001DEAD:       .word   0xC001DEAD
SDRAM_0xDEADC001:       .word   0xDEADC001
SDRAM_0xFFFFFFFF:       .word   0xFFFFFFFF
SDRAM_0x55555555:       .word   0x55555555
SDRAM_0xAAAAAAAA:       .word   0xAAAAAAAA
WEIM_BASE_ADDR_W:       .word   WEIM_BASE_ADDR
CS0_CSGCR1_0x00411A91:  .word   0x00411A91
CS0_CSRCR1_0x0E007000:  .word   0x0E007000
CS0_CSWCR1_0x061C0000:  .word   0x061C0000
IOMUXC_BASE_ADDR_W:     .word   IOMUXC_BASE_ADDR
MXC_REDBOOT_ROM_START:  .word   SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000
CONST_0x0FFF:           .word   0x0FFF
AVIC_VECTOR0_ADDR_W:    .word   MXCBOOT_FLAG_REG
AVIC_VECTOR1_ADDR_W:    .word   MXCFIS_FLAG_REG

/*---------------------------------------------------------------------------*/
/* end of hal_platform_setup.h                                               */
#endif /* CYGONCE_HAL_PLATFORM_SETUP_H */
