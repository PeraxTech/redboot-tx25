//==========================================================================
//
//      hal_soc.h
//
//      SoC chip definitions
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
// Copyright (C) 2002 Gary Thomas
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#ifndef __HAL_SOC_H__
#define __HAL_SOC_H__

#ifdef __ASSEMBLER__

#define REG8_VAL(a)          (a)
#define REG16_VAL(a)         (a)
#define REG32_VAL(a)         (a)

#define REG8_PTR(a)          (a)
#define REG16_PTR(a)         (a)
#define REG32_PTR(a)         (a)

#else /* __ASSEMBLER__ */

extern char HAL_PLATFORM_EXTRA[];
#define REG8_VAL(a)          ((unsigned char)(a))
#define REG16_VAL(a)         ((unsigned short)(a))
#define REG32_VAL(a)         ((unsigned int)(a))

#define REG8_PTR(a)          ((volatile unsigned char *)(a))
#define REG16_PTR(a)         ((volatile unsigned short *)(a))
#define REG32_PTR(a)         ((volatile unsigned int *)(a))
#define readb(a)             (*(volatile unsigned char *)(a))
#define readw(a)             (*(volatile unsigned short *)(a))
#define readl(a)             (*(volatile unsigned int *)(a))
#define writeb(v,a)          (*(volatile unsigned char *)(a) = (v))
#define writew(v,a)          (*(volatile unsigned short *)(a) = (v))
#define writel(v,a)          (*(volatile unsigned int *)(a) = (v))

#endif /* __ASSEMBLER__ */

/*
 * Default Memory Layout Definitions
 */

#define L2CC_BASE_ADDR          0x30000000

/*
 * AIPS 1
 */
#define AIPS1_BASE_ADDR         0x43F00000
#define AIPS1_CTRL_BASE_ADDR    AIPS1_BASE_ADDR
#define MAX_BASE_ADDR           0x43F04000
#define EVTMON_BASE_ADDR        0x43F08000
#define CLKCTL_BASE_ADDR        0x43F0C000
#define ETB_SLOT4_BASE_ADDR     0x43F10000
#define ETB_SLOT5_BASE_ADDR     0x43F14000
#define ECT_CTIO_BASE_ADDR      0x43F18000
#define I2C_BASE_ADDR           0x43F80000
#define MU_BASE_ADDR            0x43F84000
#define GPC_BASE_ADDR           0x43F88000
#define VPU_BASE_ADDR           0x43F8C000
#define CTI_AP_BASE_ADDR        0x43F90000
#define DSM_BASE_ADDR           0x43F98000
#define OWIRE_BASE_ADDR         0x43F9C000
#define KPP_BASE_ADDR           0x43FA8000

/*
 * SPBA
 */
#define SPBA_BASE_ADDR          0x50000000
#define UART4_BASE_ADDR         0x50000000
#define MMC_SDHC1_BASE_ADDR     0x50004000
#define MMC_SDHC2_BASE_ADDR     0x50008000
#define UART1_BASE_ADDR         0x5000C000
#define UART2_BASE_ADDR         0x50010000
#define SSI1_BASE_ADDR          0x50014000
#define SIM1_BASE_ADDR          0x50018000
#define IIM_BASE_ADDR           0x5001C000
#define SDMA_CTI_BASE_ADDR      0x50020000
#define USBOTG_BASE_ADDR        0x50024000
#define UART3_BASE_ADDR         0x5002C000
#define CSPI1_BASE_ADDR         0x50030000
#define SPBA_CTRL_BASE_ADDR     0x5003C000
#define IOMUXC_BASE_ADDR        0x50040000
#define SRC_BASE_ADDR           0x50044000
#define CRM_AP_BASE_ADDR        0x50048000
#define PLL_AP_BASE_ADDR        0x5004C000
#define PLL_BP_BASE_ADDR        0x50050000
#define PLL_USB_BASE_ADDR       0x50054000
#define GPIO2_BASE_ADDR         0x50058000
#define RTIC_BASE_ADDR          0x5005C000
#define SCC_BASE                0x50060000
#define SAHARA_BASE             0x50064000
#define MAX_SP_BASE_ADDR        0x50068000
#define IOMUX_AP_BASE_ADDR      0x5006C000
#define GPIO1_BASE_ADDR         0x50070000
#define EMIV2_BASE_ADDR         0x50074000

/*
 * AIPS 2
 */
#define AIPS2_BASE_ADDR         0x53F00000
#define AIPS2_CTRL_BASE_ADDR    AIPS2_BASE_ADDR
#define GPT_BASE_ADDR           0x53F90000
#define EPIT1_BASE_ADDR         0x53F94000
#define RTR_BASE_ADDR           0x53F98000
#define IPU_CTRL_BASE_ADDR      0x53FC0000
#define AUDMUX_BASE             0x53FC4000
#define EDIO_BASE_ADDR          0x53FC8000
#define HSCS_BASE_ADDR          0x53FCC000
#define LPMC_BASE_ADDR          0x53FD0000
#define SDMA_BASE_ADDR          0x53FD4000
#define RTC_BASE_ADDR           0x53FD8000
#define WDOG1_BASE_ADDR         0x53FDC000
#define WDOG_BASE_ADDR          WDOG1_BASE_ADDR

/*
 * DSP Peripheral registers
 */
#define DSP_CRM_BP              0xFFFC8000

/*
 * ROMPATCH and AVIC
 */
#define ROMPATCH_BASE_ADDR      0x60000000
#define AVIC_BASE_ADDR          0x68000000

/*
 * NAND, SDRAM, WEIM, M3IF, EMI controllers
 */
#define ESDCTL_BASE             (EMIV2_BASE_ADDR + 0x1000)
#define WEIM_BASE_ADDR          (EMIV2_BASE_ADDR + 0x2000)
#define NFC_IP_BASE             (EMIV2_BASE_ADDR + 0x3000)
#define WEIM_CTRL_CS0           WEIM_BASE_ADDR
#define WEIM_CTRL_CS1           (WEIM_BASE_ADDR + 0x18)
#define WEIM_CTRL_CS2           (WEIM_BASE_ADDR + 0x30)
#define WEIM_CTRL_CS3           (WEIM_BASE_ADDR + 0x48)
#define WEIM_CTRL_CS4           (WEIM_BASE_ADDR + 0x60)
#define WEIM_CTRL_CS5           (WEIM_BASE_ADDR + 0x78)
#define M4IF_BASE               EMIV2_BASE_ADDR
#define EMIV2_CTRL_BASE_ADDR    (EMIV2_BASE_ADDR + 0x3F00)

/*
 * Memory regions and CS
 */
#define IPU_MEM_BASE_ADDR       0x70000000
#define CSD0_BASE_ADDR          0x80000000
#define CSD1_BASE_ADDR          0x90000000
#define CS0_BASE_ADDR           0xA0000000
#define CS1_BASE_ADDR           0xA8000000
#define CS2_BASE_ADDR           0xB0000000
#define CS3_BASE_ADDR           0xB2000000
#define CS4_BASE_ADDR           0xB4000000
#define CS5_BASE_ADDR           0xB6000000

#define INTERNAL_ROM_VA         0xF0000000

/*
 * IRQ Controller Register Definitions.
 */
#define AVIC_NIMASK                     REG32_PTR(AVIC_BASE_ADDR + (0x04))
#define AVIC_INTTYPEH                   REG32_PTR(AVIC_BASE_ADDR + (0x18))
#define AVIC_INTTYPEL                   REG32_PTR(AVIC_BASE_ADDR + (0x1C))

/* L210 */
#define L2CC_BASE_ADDR                  0x30000000
#define L2_CACHE_LINE_SIZE              32
#define L2_CACHE_CTL_REG                0x100
#define L2_CACHE_AUX_CTL_REG            0x104
#define L2_CACHE_SYNC_REG               0x730
#define L2_CACHE_INV_LINE_REG           0x770
#define L2_CACHE_INV_WAY_REG            0x77C
#define L2_CACHE_CLEAN_LINE_REG         0x7B0
#define L2_CACHE_CLEAN_INV_LINE_REG     0x7F0

/* CRM_AP */
#define CRM_AP_ASCSR        0x00
#define CRM_AP_ACSR         0x04
#define CRM_AP_ACDR         0x08
#define CRM_AP_ACDER        0x0C
#define CRM_AP_APR          0x10
#define CRM_AP_ACGCR        0x14
#define CRM_AP_ARCGR        0x18
#define CRM_AP_L1CGR0       0x1C
#define CRM_AP_L1CGR1       0x20
#define CRM_AP_L1CGR2       0x24
#define CRM_AP_L1CGR3       0x28
#define CRM_AP_L2CGR0       0x2C
#define CRM_AP_L2CGR1       0x30
#define CRM_AP_L2CGR2       0x34
#define CRM_AP_L2CGR3       0x38
#define CRM_AP_L2CGR4       0x3C
#define CRM_AP_L2CGR5       0x40
#define CRM_AP_L2CGR6       0x44
#define CRM_AP_L2CGR7       0x48
#define CRM_AP_L2CGR8       0x4C
#define CRM_AP_AMORA        0x50
#define CRM_AP_AMORB        0x54
#define CRM_AP_AMORC        0x58
#define CRM_AP_APOR         0x5C
#define CRM_AP_AMCR         0x60
#define CRM_AP_ADFMR        0x64
#define CRM_AP_ACR          0x68
#define CRM_AP_AGPR         0x6C

/* SRC */
#define SRC_SBMR            0x00
#define SRC_SRSR            0x04
#define SRC_SSCR            0x08
#define SRC_SCRCR           0x18

#define SPBA_IIM  0x1C

/* DSP CRM_BP - only available from the DSP, or through MU */
#define CRM_BP_BSCSR        0x00
#define CRM_BP_BCSR         0x04
#define CRM_BP_BCDR         0x08
#define CRM_BP_BDCR         0x24
#define CRM_BP_BCGCR        0x08
#define CRM_BP_BMLPMRA      0x0C
#define CRM_BP_BMLPMRB      0x10
#define CRM_BP_BMLPMRC      0x14
#define CRM_BP_BMLPMRD      0x18
#define CRM_BP_BMLPMRE      0x1C


#define CRM_BP_BCR          0x28
#define CRM_BP_BMCR         0x2C
#define CRM_BP_BPCR         0x30

#define FREQ_26MHZ                      26000000
#define FREQ_32768HZ                    (32768 * 512)
#define FREQ_32000HZ                    (32000 * 512)
#define PLL_REF_CLK                     FREQ_26MHZ

/* WEIM  */
#define CSGCR1                          0x00
#define CSGCR2                          0x04
#define CSRCR1                          0x08
#define CSRCR2                          0x0C
#define CSWCR1                          0x10

/* ESDCTL */
#define ESDCTL_ESDCTL0                  0x00
#define ESDCTL_ESDCFG0                  0x04
#define ESDCTL_ESDCTL1                  0x08
#define ESDCTL_ESDCFG1                  0x0C
#define ESDCTL_ESDMISC                  0x10
#define ESDCTL_ESDSCR                   0x14
#define ESDCTL_ESDCDLY1                 0x20
#define ESDCTL_ESDCDLY2                 0x24
#define ESDCTL_ESDCDLY3                 0x28
#define ESDCTL_ESDCDLY4                 0x2C
#define ESDCTL_ESDCDLY5                 0x30
#define ESDCTL_ESDCDLYGD                0x34

/* DPLL */
#define PLL_DP_CTL          0x00
#define PLL_DP_CONFIG       0x04
#define PLL_DP_OP           0x08
#define PLL_DP_MFD          0x0C
#define PLL_DP_MFN          0x10
#define PLL_DP_MFNMINUS     0x14
#define PLL_DP_MFNPLUS      0x18
#define PLL_DP_HFS_OP       0x1C
#define PLL_DP_HFS_MFD      0x20
#define PLL_DP_HFS_MFN      0x24
#define PLL_DP_TOGC         0x28
#define PLL_DP_DESTAT       0x2C

/* MU */
#define MU_MTR0             0x00
#define MU_MTR1             0x04
#define MU_MTR2             0x08
#define MU_MTR3             0x0C
#define MU_MRR0             0x10
#define MU_MRR1             0x14
#define MU_MRR2             0x18
#define MU_MRR3             0x1C
#define MU_MSR              0x20
#define MU_MCR              0x24

/* IIM */
#define CHIP_REV_1_0        0x10      /* PASS 1.0 */
#define CHIP_REV_1_1        0x11      /* PASS 1.0 */
#define CHIP_REV_1_2        0x12      /* PASS 1.2 */
#define CHIP_REV_2_0        0x20      /* PASS 2.0 */
#define CHIP_REV_2_1        0x21      /* PASS 2.1 */
#define CHIP_LATEST         CHIP_REV_1_0

#define IIM_STAT_OFF        0x00
#define IIM_STAT_BUSY       (1 << 7)
#define IIM_STAT_PRGD       (1 << 1)
#define IIM_STAT_SNSD       (1 << 0)
#define IIM_STATM_OFF       0x04
#define IIM_ERR_OFF         0x08
#define IIM_ERR_PRGE        (1 << 7)
#define IIM_ERR_WPE         (1 << 6)
#define IIM_ERR_OPE         (1 << 5)
#define IIM_ERR_RPE         (1 << 4)
#define IIM_ERR_WLRE        (1 << 3)
#define IIM_ERR_SNSE        (1 << 2)
#define IIM_ERR_PARITYE     (1 << 1)
#define IIM_EMASK_OFF       0x0C
#define IIM_FCTL_OFF        0x10
#define IIM_UA_OFF          0x14
#define IIM_LA_OFF          0x18
#define IIM_SDAT_OFF        0x1C
#define IIM_PREV_OFF        0x20
#define IIM_SREV_OFF        0x24
#define IIM_PREG_P_OFF      0x28
#define IIM_SCS0_OFF        0x2C
#define IIM_SCS1_P_OFF      0x30
#define IIM_SCS2_OFF        0x34
#define IIM_SCS3_P_OFF      0x38
#define IIM_HAB0            0x810

#define FREQ_CKIH_26M       26000000

#define EPIT_BASE_ADDR      EPIT1_BASE_ADDR
#define EPITCR              0x00
#define EPITSR              0x04
#define EPITLR              0x08
#define EPITCMPR            0x0C
#define EPITCNR             0x10

#define NFC_BASE                        0xBFFF0000
#define NAND_REG_BASE                   (NFC_BASE + 0xE00)
#define NAND_ADD_CMD_REG                (NAND_REG_BASE + 0x00)
#define NAND_CONFIGURATION1_REG         (NAND_REG_BASE + 0x04)
#define NAND_ECC_STATUS_RESULT_REG      (NAND_REG_BASE + 0x08)
#define NAND_LAUNCH_REG                 (NAND_REG_BASE + 0x0C)

#define NAND_LAUNCH_FCMD                (1 << 0)
#define NAND_LAUNCH_FADD                (1 << 1)
#define NAND_LAUNCH_FDI                 (1 << 2)

#define NAND_CONFIGURATION1_NFC_RST     (1 << 2)
#define NAND_CONFIGURATION1_NF_CE       (1 << 1)
#define NAND_CONFIGURATION1_SP_EN       (1 << 0)

#define NFC_WR_PROT_REG                 (NFC_IP_BASE + 0x00)
#define UNLOCK_BLK_ADD0_REG             (NFC_IP_BASE + 0x04)
#define UNLOCK_BLK_ADD1_REG             (NFC_IP_BASE + 0x08)
#define UNLOCK_BLK_ADD2_REG             (NFC_IP_BASE + 0x0C)
#define UNLOCK_BLK_ADD3_REG             (NFC_IP_BASE + 0x10)
#define NFC_FLASH_CONFIG2_REG           (NFC_IP_BASE + 0x14)
#define NFC_IPC_REG                     (NFC_IP_BASE + 0x18)
#define NFC_AXI_ERR_ADD_REG             (NFC_IP_BASE + 0x1C)
#define NFC_IPC_INT                     (1 << 31)
#define NFC_IPC_LPS                     (1 << 30)
#define NFC_IPC_RB_B                    (1 << 29)
#define NFC_IPC_CACK                    (1 << 1)
#define NFC_IPC_CREQ                    (1 << 0)
#define NFC_WR_PROTECT_CS0              (0 << 20)
#define NFC_WR_PROTECT_BLS_UNLOCK       (2 << 16)
#define NFC_WR_PROTECT_WPC              (4 << 0)

#define NFC_FLASH_CONFIG2_EDC0          (0 << 9)
#define NFC_FLASH_CONFIG2_EDC1          (1 << 9)
#define NFC_FLASH_CONFIG2_EDC2          (2 << 9)
#define NFC_FLASH_CONFIG2_EDC3          (3 << 9)
#define NFC_FLASH_CONFIG2_EDC4          (4 << 9)
#define NFC_FLASH_CONFIG2_EDC5          (5 << 9)
#define NFC_FLASH_CONFIG2_EDC6          (6 << 9)
#define NFC_FLASH_CONFIG2_EDC7          (7 << 9)
#define NFC_FLASH_CONFIG2_PPB_32        (0 << 7)
#define NFC_FLASH_CONFIG2_PPB_64        (1 << 7)
#define NFC_FLASH_CONFIG2_PPB_128       (2 << 7)
#define NFC_FLASH_CONFIG2_PPB_256       (3 << 7)
#define NFC_FLASH_CONFIG2_INT_MSK       (1 << 4)
#define NFC_FLASH_CONFIG2_ECC_EN        (1 << 3)
#define NFC_FLASH_CONFIG2_SYM           (1 << 2)
#define NFC_FLASH_CONFIG2_DLP_DISABLE   (0 << 0)

#define RAM_BUFFER_ADDRESS_RBA_3        0x3
#define NFC_BUFSIZE_1KB                 0x0
#define NFC_BUFSIZE_2KB                 0x1
#define NFC_CONFIGURATION_UNLOCKED      0x2
#define ECC_STATUS_RESULT_NO_ERR        0x0
#define ECC_STATUS_RESULT_1BIT_ERR      0x1
#define ECC_STATUS_RESULT_2BIT_ERR      0x2
#define NF_WR_PROT_UNLOCK               0x4
#define NAND_FLASH_CONFIG1_FORCE_CE     (1 << 7)
#define NAND_FLASH_CONFIG1_RST          (1 << 6)
#define NAND_FLASH_CONFIG1_BIG          (1 << 5)
#define NAND_FLASH_CONFIG1_INT_MSK      (1 << 4)
#define NAND_FLASH_CONFIG1_ECC_EN       (1 << 3)
#define NAND_FLASH_CONFIG1_SP_EN        (1 << 2)

#define MXC_NAND_BASE_DUMMY             0x00000000
#define NOR_FLASH_BOOT                  0
#define NAND_FLASH_BOOT                 0x10000000
#define SDRAM_NON_FLASH_BOOT            0x20000000
#define MXCBOOT_FLAG_REG                (AVIC_BASE_ADDR + 0x100)
#define MXCFIS_NOTHING                  0x00000000
#define MXCFIS_NAND                     0x10000000
#define MXCFIS_NOR                      0x20000000
#define MXCFIS_FLAG_REG                 (AVIC_BASE_ADDR + 0x104)

#define IS_BOOTING_FROM_NAND()          (readl(MXCBOOT_FLAG_REG) == NAND_FLASH_BOOT)
#define IS_BOOTING_FROM_NOR()           (readl(MXCBOOT_FLAG_REG) == NOR_FLASH_BOOT)
#define IS_BOOTING_FROM_SDRAM()         (readl(MXCBOOT_FLAG_REG) == SDRAM_NON_FLASH_BOOT)

#ifndef MXCFLASH_SELECT_NAND
#define IS_FIS_FROM_NAND()              0
#else
#define IS_FIS_FROM_NAND()              (readl(MXCFIS_FLAG_REG) == MXCFIS_NAND)
#endif

#ifndef MXCFLASH_SELECT_NOR
#define IS_FIS_FROM_NOR()               0
#else
#define IS_FIS_FROM_NOR()               (!IS_FIS_FROM_NAND())
#endif

#define MXC_ASSERT_NOR_BOOT()           writel(MXCFIS_NOR, MXCFIS_FLAG_REG)
#define MXC_ASSERT_NAND_BOOT()          writel(MXCFIS_NAND, MXCFIS_FLAG_REG)

// MXC30031ADS board NAND boot is different from "normal" case. Instead of
// jumping directly to starting NAND base, it loads the value at 0x400 of
// the beginning of NAND and jumps to there.
#define MXC_NAND_BOOT_LOAD_AT_0x400
/*
 * This macro is used to get certain bit field from a number
 */
#define MXC_GET_FIELD(val, len, sh)          ((val >> sh) & ((1 << len) - 1))

/*
 * This macro is used to set certain bit field inside a number
 */
#define MXC_SET_FIELD(val, len, sh, nval)    ((val & ~(((1 << len) - 1) << sh)) | (nval << sh))

#define UART_WIDTH_32         /* internal UART is 32bit access only */

#define L2CC_ENABLED

#if !defined(__ASSEMBLER__)
void cyg_hal_plf_serial_init(void);
void cyg_hal_plf_serial_stop(void);
void hal_delay_us(unsigned int usecs);
#define HAL_DELAY_US(n)     hal_delay_us(n)

enum plls {
    PLL0,
    PLL1,
    PLL2,
};

enum main_clocks {
    CPU_CLK,
    AHB_CLK,
    IPG_CLK,
    NFC_CLK,
    USB_CLK,
    EMI_CLK,
};

enum peri_clocks {
    UART1_BAUD,
    UART2_BAUD,
    UART3_BAUD,
    UART4_BAUD,
    SSI1_BAUD,
    CSI_BAUD,
    SPI1_CLK = CSPI1_BASE_ADDR,
};

unsigned int pll_clock(enum plls pll);

unsigned int get_main_clock(enum main_clocks clk);

unsigned int get_peri_clock(enum peri_clocks clk);

typedef unsigned int nfc_setup_func_t(unsigned int, unsigned int, unsigned int);

#endif //#if !defined(__ASSEMBLER__)

#define HAL_MMU_OFF() \
CYG_MACRO_START          \
    asm volatile (                                                      \
        "mcr p15, 0, r0, c7, c14, 0;"                                   \
        "mcr p15, 0, r0, c7, c10, 4;" /* drain the write buffer */      \
        "mcr p15, 0, r0, c7, c5, 0;" /* invalidate I cache */           \
        "mrc p15, 0, r0, c1, c0, 0;" /* read c1 */                      \
        "bic r0, r0, #0x7;" /* disable DCache and MMU */                \
        "bic r0, r0, #0x1000;" /* disable ICache */                     \
        "mcr p15, 0, r0, c1, c0, 0;" /*  */                             \
        "nop;" /* flush i+d-TLBs */                                     \
        "nop;" /* flush i+d-TLBs */                                     \
        "nop;" /* flush i+d-TLBs */                                     \
        "nop;" /* flush i+d-TLBs */                                     \
        "nop;" /* flush i+d-TLBs */                                     \
        "nop;" /* flush i+d-TLBs */                                     \
        "nop;" /* flush i+d-TLBs */                                     \
        "nop;" /* flush i+d-TLBs */                                     \
        "nop;" /* flush i+d-TLBs */                                     \
        :                                                               \
        :                                                               \
        : "r0","memory" /* clobber list */);                            \
CYG_MACRO_END

#endif // __HAL_SOC_H__
