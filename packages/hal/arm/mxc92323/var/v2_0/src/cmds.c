//==========================================================================
//
//      cmds.c
//
//      SoC [platform] specific RedBoot commands
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
#include <redboot.h>
#include <cyg/hal/hal_intr.h>
#include <cyg/hal/plf_mmap.h>
#include <cyg/hal/hal_soc.h>         // Hardware definitions
#include <cyg/hal/hal_cache.h>

typedef unsigned long long  u64;
typedef unsigned int        u32;
typedef unsigned short      u16;
typedef unsigned char       u8;

#define SZ_DEC_1M       1000000
#define PLL_PD_MAX      16      //actual pd+1
#define PLL_MFI_MAX     15
#define PLL_MFI_MIN     5
#define ARM_DIV_MAX     4
#define IPG_DIV_MAX     2
#define AHB_DIV_MAX     3
#define EMI_DIV_MAX     4
#define NFC_DIV_MAX     16

#define REF_IN_CLK_NUM  4
struct fixed_pll_mfd {
    u32 ref_clk_hz;
    u32 mfd;
};
const struct fixed_pll_mfd fixed_mfd[REF_IN_CLK_NUM] = {
    {FREQ_CKIH_26M,       26 * 16},    // 416
    {0,                   0},      // reserved
    {2 * FREQ_CKIH_26M,   26 * 16},    // 416
    {0,                   0},      // reserved
};

struct pll_param {
    u32 pd;
    u32 mfi;
    u32 mfn;
    u32 mfd;
};

#define PLL_FREQ_MAX(_ref_clk_)    (2 * _ref_clk_ * PLL_MFI_MAX)
#define PLL_FREQ_MIN(_ref_clk_)    ((2 * _ref_clk_ * (PLL_MFI_MIN - 1)) / PLL_PD_MAX)
#define AHB_CLK_MAX     133333333
#define IPG_CLK_MAX     (AHB_CLK_MAX / 2)
#define NFC_CLK_MAX     25000000

#define ERR_WRONG_CLK   -1
#define ERR_NO_MFI      -2
#define ERR_NO_MFN      -3
#define ERR_NO_PD       -4
#define ERR_NO_ARM_DIV  -5
#define ERR_NO_AHB_DIV  -6

int gcd(int m, int n);

static void clock_setup(int argc, char *argv[]);
static void ckol(int argc, char *argv[]);
static void ckoh(int argc, char *argv[]);

static volatile u32 *crm_ap_base = REG32_PTR(CRM_AP_BASE_ADDR);

static volatile u32 *pll_base[] =
{
    REG32_PTR(PLL_AP_BASE_ADDR),     // MCU PLL
    REG32_PTR(PLL_BP_BASE_ADDR),     // DSP PLL
    REG32_PTR(PLL_USB_BASE_ADDR),     // USB PLL
};

#define NOT_ON_VAL  0xDEADBEEF

RedBoot_cmd("clock",
            "Setup/Display clock. Syntax:",
            "<core clock in Hz>:<emi clock in Hz>:<AHB-to-core divider>:<IPG-to-AHB divider>\n\n\
If no EMI clock is specified, then 100MHz or an optimal value is chosen.\n\
If a divider is zero or no divider is specified, the optimal divider values \n\
will be chosen.\n\
MAX core clk=399MHz, MAX EMI clk=166MHz, MAX AHB=133MHz, MAX IPG=66.5MHz\n\n\
 Examples:\n\
clock                        ->Show various clocks\n\
clock 399000000              ->Core=399 EMIclk=100 AHB=133 IPG=66.5\n\
clock 266000000:66500000     ->Core=266 EMIclk=66.5 AHB=133 IPG=66.5\n\
clock 399000000:133000000:6  ->Core=399 EMIclk=133 AHB=66.5(/6) IPG=66.5\n\
clock 399000000:133000000:6:2->Core=399 EMIclk=133 AHB=66.5 IPG=33.25(AHB/2)\n",
            clock_setup
           );

/*!
 * This is to calculate various parameters based on reference clock and
 * targeted clock based on the equation:
 *      t_clk = 2*ref_freq*(mfi + mfn/(mfd+1))/(pd+1)
 * This calculation is based on a fixed MFD value for simplicity.
 *
 * @param ref       reference clock freq in Hz
 * @param target    targeted clock in Hz
 * @param p_pd      calculated pd value (pd value from register + 1) upon return
 * @param p_mfi     calculated actual mfi value upon return
 * @param p_mfn     calculated actual mfn value upon return
 * @param p_mfd     fixed mfd value (mfd value from register + 1) upon return
 *
 * @return          0 if successful; non-zero otherwise.
 */
int calc_pll_params(u32 ref, u32 target, struct pll_param *pll)
{
    u64 pd, mfi = 1, mfn, mfd, n_target = target, n_ref = ref, i;

    // make sure targeted freq is in the valid range. Otherwise the
    // following calculation might be wrong!!!
    if (n_target < PLL_FREQ_MIN(ref) || n_target > PLL_FREQ_MAX(ref))
        return ERR_WRONG_CLK;
    for (i = 0; ; i++) {
        if (i == REF_IN_CLK_NUM)
            return ERR_WRONG_CLK;
        if (fixed_mfd[i].ref_clk_hz == ref) {
            mfd = fixed_mfd[i].mfd;
            break;
        }
    }
    // Use n_target and n_ref to avoid overflow
    for (pd = 1; pd <= PLL_PD_MAX; pd++) {
        mfi = (n_target * pd) / (2 * n_ref);
        if (mfi > PLL_MFI_MAX) {
            return ERR_NO_MFI;
        } else if (mfi < 5) {
            continue;
        }
        break;
    }
    // Now got pd and mfi already
    mfn = (((n_target * pd) / 2 - n_ref * mfi) * mfd) / n_ref;
#ifdef CMD_CLOCK_DEBUG
    diag_printf("%d: ref=%d, target=%d, pd=%d, mfi=%d,mfn=%d, mfd=%d\n",
                __LINE__, ref, (u32)n_target, (u32)pd, (u32)mfi, (u32)mfn, (u32)mfd);
#endif
    i = 1;
    if (mfn != 0)
        i = gcd(mfd, mfn);
    pll->pd = (u32)pd;
    pll->mfi = (u32)mfi;
    pll->mfn = (u32)(mfn / i);
    pll->mfd = (u32)(mfd / i);
    return 0;
}

/*!
 * This function assumes the expected core clock has to be changed by
 * modifying the PLL. This is NOT true always but for most of the times,
 * it is. So it assumes the PLL output freq is the same as the expected
 * core clock (arm_div=1) unless the core clock is less than PLL_FREQ_MIN.
 * In the latter case, it will try to increase the arm_div value until
 * (arm_div*core_clk) is greater than PLL_FREQ_MIN. It then makes call to
 * calc_pll_params() and obtains the values of PD, MFI,MFN, MFD based
 * on the targeted PLL and reference input clock to the PLL. Lastly,
 * it sets the register based on these values along with the dividers.
 * Note 1) There is no value checking for the passed-in divider values
 *         so the caller has to make sure those values are sensible.
 *      2) Also adjust the NFC divider such that the NFC clock doesn't
 *         exceed NFC_CLK_MAX.
 *      3) This function should not have allowed diag_printf() calls since
 *         the serial driver has been stoped. But leave then here to allow
 *         easy debugging by NOT calling the cyg_hal_plf_serial_stop().
 *      4) The IPG divider doesn't go through AHB divider
 *
 * @param ref       pll input reference clock (32KHz or 26MHz)
 * @param core_clk  core clock in Hz
 * @param emi_clk   emi clock in Hz
 * @param ahb_div   ahb divider to divide the core clock to get ahb clock
 *                  (ahb_div - 1) needs to be set in the register
 * @param ipg_div   ipg divider to divide the core clock to get ipg clock
 *                  (ipg_div - 1) needs to be set in the register
 # @return          0 if successful; non-zero otherwise
 */
int configure_clock(u32 ref, u32 core_clk, u32 emi_clk, u32 ahb_div, u32 ipg_div)
{
    u32 pll, arm_div = 1, emi_div = 0, nfc_div, ascsr, acdr, acder2;
    struct pll_param pll_param;
    int ret;

    pll = core_clk;

    // get nfc_div - make sure optimal NFC clock but less than NFC_CLK_MAX
    for (nfc_div = 1; nfc_div <= NFC_DIV_MAX; nfc_div++) {
        if ((pll / (ahb_div * nfc_div)) <= NFC_CLK_MAX) {
            break;
        }
    }

    if (ahb_div <= ARM_DIV_MAX) {
        arm_div = ahb_div;
        ahb_div = 1;
    } else {
        arm_div = ahb_div / AHB_DIV_MAX;
        ahb_div = AHB_DIV_MAX;
    }

    // switch to ap_ref_clk
    writel(readl(CRM_AP_BASE_ADDR + CRM_AP_ACDR) & (~0x30000),
           CRM_AP_BASE_ADDR + CRM_AP_ACDR);
    writel(readl(CRM_AP_BASE_ADDR + CRM_AP_ACSR) & (~0x1),
           CRM_AP_BASE_ADDR + CRM_AP_ACSR);

    ascsr = readl(CRM_AP_BASE_ADDR + CRM_AP_ASCSR);

    if (core_clk == (399 * SZ_DEC_1M)) {
        // use bp_div_gen if requested core clock is 399MHz
        ascsr |= 0x80;
        // use ap_pll for emi clock
        ascsr &= (~0x4);
        writel(ascsr, CRM_AP_BASE_ADDR + CRM_AP_ASCSR);
        // Set the ap_pll to get emi clock
        pll = emi_clk;
        emi_div = 1;
    } else {
        if ((core_clk % emi_clk) == 0) {
            emi_div = core_clk / emi_clk;
        } else {
            emi_div = (core_clk / emi_clk) + 1;
            diag_printf("Cannot generate %d emi_clk from %d core_clk, emi_clk set to %d",
                        emi_clk, core_clk, (core_clk / emi_div));
        }

        if (emi_div > EMI_DIV_MAX) {
            diag_printf("Invalid EMI divider: %d. Maximum value is %d\n",
                        emi_div, EMI_DIV_MAX);
            // switch back to pll before returning
            writel(readl(CRM_AP_BASE_ADDR + CRM_AP_ACDR) | 0x30000,
                   CRM_AP_BASE_ADDR + CRM_AP_ACDR);
            writel(readl(CRM_AP_BASE_ADDR + CRM_AP_ACSR) | 0x1,
                   CRM_AP_BASE_ADDR + CRM_AP_ACSR);
            return -1;
        }

        // use ap_pll for core and emi_clock
        ascsr &= (~0x84);
        writel(ascsr, CRM_AP_BASE_ADDR + CRM_AP_ASCSR);
    }

    // pll is now the targeted pll output. Use it along with ref input clock
    // to get pd, mfi, mfn, mfd
    if ((ret = calc_pll_params(ref, pll, &pll_param)) != 0) {
         diag_printf("can't find pll parameters: %d\n", ret);
         return ret;
    }
#ifdef CMD_CLOCK_DEBUG
    diag_printf("ref=%d, pll=%d, pd=%d, mfi=%d,mfn=%d, mfd=%d\n",
                ref, pll, pll_param.pd, pll_param.mfi, pll_param.mfn, pll_param.mfd);
#endif

    // adjust pll settings
    writel(((pll_param.pd - 1) << 0) | (pll_param.mfi << 4),
           PLL_AP_BASE_ADDR + PLL_DP_OP);
    writel(pll_param.mfn, PLL_AP_BASE_ADDR + PLL_DP_MFN);
    writel(pll_param.mfd - 1, PLL_AP_BASE_ADDR + PLL_DP_MFD);
    writel(((pll_param.pd - 1) << 0) | (pll_param.mfi << 4),
           PLL_AP_BASE_ADDR + PLL_DP_HFS_OP);
    writel(pll_param.mfn, PLL_AP_BASE_ADDR + PLL_DP_HFS_MFN);
    writel(pll_param.mfd - 1, PLL_AP_BASE_ADDR + PLL_DP_HFS_MFD);

    acder2 = readl(CRM_AP_BASE_ADDR + CRM_AP_ACDER);
    acder2 = (acder2 & 0x0FCFFFFF) | ((nfc_div - 1) << 28) | ((emi_div - 1) << 20);
    acdr = (0x1 << 16) | ((arm_div - 1) << 3) | ((ahb_div - 1) << 1) | (ipg_div - 1);
    // change the dividers
    writel(acdr, CRM_AP_BASE_ADDR + CRM_AP_ACDR);
    writel(acder2, CRM_AP_BASE_ADDR + CRM_AP_ACDER);

    // switch back to pll
    writel(readl(CRM_AP_BASE_ADDR + CRM_AP_ACSR) | 0x1,
           CRM_AP_BASE_ADDR + CRM_AP_ACSR);

    return 0;
}

static void clock_setup(int argc,char *argv[])
{
    u32 i, core_clk, emi_clk, ipg_div, data[4], uart1_baud, ssi1_baud;
    u32 csi_baud, ahb_div, ahb_clk, ipg_clk, clk_sel, ref_clk;
    int ret;

    if (argc == 1)
        goto print_clock;
    for (i = 0;  i < 4;  i++) {
        unsigned long temp;
        if (!parse_num(*(&argv[1]), &temp, &argv[1], ":")) {
            diag_printf("Error: Invalid parameter\n");
            return;
        }
        data[i] = temp;
    }

    core_clk = data[0];
    emi_clk = data[1];
    ahb_div = data[2];  // actual register field + 1
    ipg_div = data[3];  // actual register field + 1

    // since only support set clock for the AP domain, get ref input clock
    // for the AP domain.
    clk_sel = MXC_GET_FIELD(readl(PLL_AP_BASE_ADDR + PLL_DP_CTL), 2, 8);
    ref_clk = fixed_mfd[clk_sel].ref_clk_hz;

    if (core_clk < (PLL_FREQ_MIN(ref_clk)) ||
        core_clk > (399 * SZ_DEC_1M)) {
        diag_printf("Requested clock is %d. Targeted core clock should be within [%d - %d]\n",
                    core_clk, PLL_FREQ_MIN(ref_clk),
                    (399 * SZ_DEC_1M));
        return;
    }

    if (emi_clk == 0) {
        emi_clk = 100 * SZ_DEC_1M;
    }

    if (emi_clk < (PLL_FREQ_MIN(ref_clk)) ||
        emi_clk > (166 * SZ_DEC_1M)) {
        diag_printf("Requested emi clock is %d. Targeted emi clock should be within [%d - %d]\n",
                    emi_clk, PLL_FREQ_MIN(ref_clk),
                    (166 * SZ_DEC_1M));
        return;
    }

    // find the ahb divider
    if (ahb_div > (AHB_DIV_MAX * ARM_DIV_MAX)) {
        diag_printf("Invalid AHB divider: %d. Maximum value is %d\n",
                    ahb_div, (AHB_DIV_MAX * ARM_DIV_MAX));
        return;
    }
    if (ahb_div == 0) {
        // no HCLK divider specified
        for (ahb_div = 1; ; ahb_div++) {
            if ((core_clk / ahb_div) <= AHB_CLK_MAX) {
                break;
            }
        }
    }
    if ((core_clk / ahb_div) > AHB_CLK_MAX) {
        diag_printf("Can't make AHB=%d since max=%d\n",
                    core_clk / ahb_div, AHB_CLK_MAX);
        return;
    }

    // find the ipg divider
    ahb_clk = core_clk / ahb_div;
    if (ipg_div == 0) {
        ipg_div++;          // At least =1
        if (ahb_clk > IPG_CLK_MAX)
            ipg_div++;      // Make it =2
    }
    ipg_clk = ahb_clk / ipg_div;
    if (ipg_div > IPG_DIV_MAX || ipg_clk > IPG_CLK_MAX) {
        if (ipg_div > IPG_DIV_MAX)
            diag_printf("Invalid IPG divider: %d. Max is: %d\n",
                        ipg_div / ahb_div, IPG_DIV_MAX / ahb_div);
        else
            diag_printf("Can't make IPG=%dHz since max=%dHz\n",
                        ipg_clk, IPG_CLK_MAX);
        return;
    }

    diag_printf("Trying to set core=%d ahb=%d ipg=%d...\n",
                core_clk, ahb_clk, ipg_clk);

    // stop the serial to be ready to adjust the clock
    hal_delay_us(100000);
    cyg_hal_plf_serial_stop();
    // adjust the clock
    ret = configure_clock(ref_clk, core_clk, emi_clk, ahb_div, ipg_div);
    // restart the serial driver
    cyg_hal_plf_serial_init();
    hal_delay_us(100000);

    if (ret != 0) {
        diag_printf("Failed to setup clock: %d\n", ret);
        return;
    }
    diag_printf("\n<<<New clock setting>>>\n");

    // Now printing clocks
print_clock:
    diag_printf("\nMCUPLL\t\tUSBPLL\t\tDSPPLL\n");
    diag_printf("========================================\n");
    diag_printf("%-16d%-16d%-16d\n\n", pll_clock(PLL0), pll_clock(PLL2),
                pll_clock(PLL1));
    diag_printf("CPU\t\tAHB\t\tIPG\t\tNFC\n");
    diag_printf("==============================");
    diag_printf("=============================\n");
    diag_printf("%-16d%-16d%-16d%-16d\n\n",
                get_main_clock(CPU_CLK),
                get_main_clock(AHB_CLK),
                get_main_clock(IPG_CLK),
                get_main_clock(NFC_CLK));
    diag_printf("EMI\t\tUSB\n");
    diag_printf("==========================\n");
    diag_printf("%-16d%-16d\n\n",
                get_main_clock(EMI_CLK), get_main_clock(USB_CLK));

    uart1_baud = get_peri_clock(UART1_BAUD);
    ssi1_baud = get_peri_clock(SSI1_BAUD);
    csi_baud = get_peri_clock(CSI_BAUD);

    diag_printf("UART1/2/3/4\tSSI1\t\tCSI\n");
    diag_printf("===========================================");
    diag_printf("=============================\n");

    (uart1_baud != NOT_ON_VAL) ? diag_printf("%-16d", uart1_baud) :
                                 diag_printf("%-16s", "OFF");
    (ssi1_baud != NOT_ON_VAL) ? diag_printf("%-16d", ssi1_baud) :
                                diag_printf("%-16s", "OFF");
    (csi_baud != NOT_ON_VAL) ? diag_printf("%-16d", csi_baud ) :
                               diag_printf("%-16s", "OFF");
    diag_printf("\n\n");
}

/*!
 * This function returns the PLL output value in Hz based on pll.
 */
u32 pll_clock(enum plls pll)
{
    u64 mfi, mfn, mfd, pdf, ref_clk, pll_out, sign;
    u64 dp_ctrl, dp_op, dp_mfd, dp_mfn, clk_sel;
    u8 dbl = 0;

    dp_ctrl = pll_base[pll][PLL_DP_CTL >> 2];
    clk_sel = MXC_GET_FIELD(dp_ctrl, 2, 8);
    ref_clk = fixed_mfd[clk_sel].ref_clk_hz;

    if ((pll_base[pll][PLL_DP_CTL >> 2] & 0x80) == 0) {
        dp_op = pll_base[pll][PLL_DP_OP >> 2];
        dp_mfd = pll_base[pll][PLL_DP_MFD >> 2];
        dp_mfn = pll_base[pll][PLL_DP_MFN >> 2];
    } else {
        dp_op = pll_base[pll][PLL_DP_HFS_OP >> 2];
        dp_mfd = pll_base[pll][PLL_DP_HFS_MFD >> 2];
        dp_mfn = pll_base[pll][PLL_DP_HFS_MFN >> 2];
    }
    pdf = dp_op & 0xF;
    mfi = (dp_op >> 4) & 0xF;
    mfi = (mfi <= 5) ? 5: mfi;
    mfd = dp_mfd & 0x07FFFFFF;
    mfn = dp_mfn & 0x07FFFFFF;

    sign = (mfn < 0x4000000) ? 0: 1;
    mfn = (mfn <= 0x4000000) ? mfn: (0x8000000 - mfn);

    dbl = ((dp_ctrl >> 12) & 0x1) + 1;

    dbl = dbl * 2;
    if (sign == 0) {
        pll_out = (dbl * ref_clk * mfi + ((dbl * ref_clk * mfn) / (mfd + 1))) /
                  (pdf + 1);
    } else {
        pll_out = (dbl * ref_clk * mfi - ((dbl * ref_clk * mfn) / (mfd + 1))) /
                  (pdf + 1);
    }

    return (u32)pll_out;
}

void clock_spi_enable(unsigned int spi_clk)
{
    if (spi_clk == SPI1_CLK) {
        // do nothing now as it is already enabled by default
    }
}

/*!
 * This function returns the main clock dividers.
 */
u32 clock_divider(enum main_clocks clk)
{
    u32 div = 0;
    u32 acdr, acder2;

    acdr = crm_ap_base[CRM_AP_ACDR >> 2];
    acder2 = crm_ap_base[CRM_AP_ACDER >> 2];

    switch (clk) {
    case CPU_CLK:
        div = ((acdr >> 3) & 0x3) + 1;
        break;
    case AHB_CLK:
        div = ((acdr >> 1) & 0x3) + 1;
        break;
    case IPG_CLK:
        div = ((acdr >> 0) & 0x1) + 1;
        break;
    case NFC_CLK:
        div = ((acder2 >> 28) & 0xF) + 1;
        break;
    case EMI_CLK:
        div = ((acder2 >> 20) & 0x3) + 1;
        break;
    case USB_CLK:
        div = ((acder2 >> 16) & 0x3) + 2;
        break;
    default:
        diag_printf("Wrong clock: %d\n", clk);
        break;
    }

    return div;
}

const u32 CRM_SMALL_DIV[] = {6, 8, 12, 30};

/*!
 * This function returns the peripheral clock dividers.
 */
u32 clock_peri_divider(enum peri_clocks clk)
{
    u32 div = 0;
    u32 apra, acder;

    apra = crm_ap_base[CRM_AP_APR >> 2];
    acder = crm_ap_base[CRM_AP_ACDER >> 2];

    switch (clk) {
    case UART1_BAUD:
        div = (apra >> 20) & 0x3;
        div = CRM_SMALL_DIV[div];
        break;
    case UART2_BAUD:
        div = (apra >> 24) & 0x3;
        div = CRM_SMALL_DIV[div];
        break;
    case UART3_BAUD:
        div = (apra >> 28) & 0x3;
        div = CRM_SMALL_DIV[div];
        break;
    case UART4_BAUD:
        div = (apra >> 13) & 0x3;
        div = CRM_SMALL_DIV[div];
        break;
    case SSI1_BAUD:
        div = acder & 0x1F;
        break;
    case CSI_BAUD:
        div = (acder >> 8) & 0x1F;
        break;
    default:
        diag_printf("Wrong clock: %d\n", clk);
        break;
    }

    return div;
}

#if 0
void get_ref_clk(u32 *ap_unc_pat_ref, u32 *ap_ref_x2,
                 u32 *ap_ref)
{
    u32 ap_pat_ref_div_1, ap_pat_ref_div_2, ap_isel,
        ascsr, adcr, acder2, clk_sel, ref_clk;

    clk_sel = MXC_GET_FIELD(readl(PLL_AP_BASE_ADDR + PLL_DP_CTL), 2, 8);
    ref_clk = fixed_mfd[clk_sel].ref_clk_hz;

    ascsr = crm_ap_base[CRM_AP_ASCSR >> 2];
    adcr = crm_ap_base[CRM_AP_ADCR >> 2];
    acder2 = crm_ap_base[CRM_AP_ACDER2 >> 2];

    ap_isel = ascsr & 0x1;
    ap_pat_ref_div_1 = ((ascsr >> 2) & 0x1) + 1;
    ap_pat_ref_div_2 = ((ascsr >> 15) & 0x1) + 1;

    *ap_unc_pat_ref = ref_clk * (ap_isel + 1);
    *ap_ref_x2 =  (*ap_unc_pat_ref)/ ap_pat_ref_div_1;
    *ap_ref = (*ap_ref_x2) / ap_pat_ref_div_2;
}
#endif

u32 get_core_clk_source(void)
{
    u32 ascsr;
    u64 ret_val = 0;

    ascsr = crm_ap_base[CRM_AP_ASCSR >> 2];
    if (((ascsr >> 7) & 0x1) == 0) {
        ret_val = pll_clock(PLL0);
    } else {
        ret_val = (pll_clock(PLL1) * 3) / 4;
    }
    return ret_val;
}

/*!
 * This function returns the main clock value in Hz.
 */
u32 get_main_clock(enum main_clocks clk)
{
    u32 brmm = 0, ret_val = 0, apsel, ap_clk_pre_dfs, acsr, ascsr, acdr, acder2, emi_clk;
    u32 lfdf = 1, ap_ref_x2_clk, ap_ref_clk, ap_unc_pat_ref;

    acsr = crm_ap_base[CRM_AP_ACSR >> 2];
    ascsr = crm_ap_base[CRM_AP_ASCSR >> 2];
    acder2 = crm_ap_base[CRM_AP_ACDER >> 2];
    acdr = crm_ap_base[CRM_AP_ACDR >> 2];

    brmm = (acdr >> 5) & 0x7;

    //get_ref_clk(&ap_unc_pat_ref, &ap_ref_x2_clk, &ap_ref_clk);

    if ((acsr & 0x1) == 0) {
        // inverted pat_ref is selected
        ap_clk_pre_dfs = ap_ref_clk;
    } else {
        // Now AP domain runs off the pll
        //apsel = (ascsr >> 7) & 0x1;
        ap_clk_pre_dfs = get_core_clk_source();
    }

    switch (clk) {
    case CPU_CLK:
        switch (brmm) {
        case 0:
            ret_val = ap_clk_pre_dfs;
            break;
        case 1:
            ret_val = ((3 * ap_clk_pre_dfs) / 4) + (ap_clk_pre_dfs / (4 * clock_divider(CPU_CLK)));
            break;
        case 2:
            ret_val = (ap_clk_pre_dfs / 2) + (ap_clk_pre_dfs / (2 * clock_divider(CPU_CLK)));
            break;
        case 3:
            ret_val = (ap_clk_pre_dfs / 4) + ((3 * ap_clk_pre_dfs) / (4 * clock_divider(CPU_CLK)));
            break;
        case 4:
            ret_val = ap_clk_pre_dfs / clock_divider(CPU_CLK);
            break;
        default:
            break;
        }
        break;
    case AHB_CLK:
        ret_val = ap_clk_pre_dfs / (clock_divider(CPU_CLK) * clock_divider(AHB_CLK));
        break;
    case IPG_CLK:
        ret_val = ap_clk_pre_dfs / (clock_divider(CPU_CLK) * clock_divider(AHB_CLK) * clock_divider(IPG_CLK));
        break;
    case NFC_CLK:
        if ((acder2 & (1 << 27)) == 0) {
            diag_printf("Warning: NFC clock is not enabled !!!\n");
        } else {
            ret_val = ap_clk_pre_dfs / (clock_divider(CPU_CLK) * clock_divider(AHB_CLK) * clock_divider(NFC_CLK));
        }
        break;
    case USB_CLK:
        if ((acder2 & (1 << 18)) == 0) {
            diag_printf("Warning: USB clock is not enabled !!!\n");
        } else {
            ret_val = pll_clock(2) / clock_divider(USB_CLK);
        }
        break;
    case EMI_CLK:
        if ((acder2 & (1 << 22)) == 0) {
            diag_printf("Warning: EMI clock is not enabled !!!\n");
        } else {
            emi_clk = (ascsr >> 2) & 0x1;
            if (emi_clk == 0) {
                ret_val = pll_clock(0) / clock_divider(EMI_CLK);
            } else {
                ret_val = ap_clk_pre_dfs / clock_divider(EMI_CLK);
            }
        }
        break;

    default:
        break;
    }

    return ret_val;
}

/*!
 * This function returns the peripheral clock value in Hz.
 */
u32 get_peri_clock(enum peri_clocks clk)
{
    u32 apra, ascsr, acder, ap_unc_pat_ref,
    ap_ref_x2_clk, ap_ref_clk, ret_val = 0, sel;

    apra = crm_ap_base[CRM_AP_APR >> 2];
    acder = crm_ap_base[CRM_AP_ACDER >> 2];
    ascsr = crm_ap_base[CRM_AP_ASCSR >> 2];

    //get_ref_clk(&ap_unc_pat_ref, &ap_ref_x2_clk, &ap_ref_clk);

    switch (clk) {
    case UART1_BAUD:
        if ((apra & 0x400000) == 0) {
            return NOT_ON_VAL;
        }

        ret_val = get_core_clk_source() / clock_peri_divider(UART1_BAUD);
        break;
    case UART2_BAUD:
        if ((apra & 0x4000000) == 0) {
            return NOT_ON_VAL;
        }
        ret_val = get_core_clk_source() / clock_peri_divider(UART2_BAUD);
        break;
    case UART3_BAUD:
        if ((apra & 0x40000000) == 0) {
            return NOT_ON_VAL;
        }
        ret_val = get_core_clk_source() / clock_peri_divider(UART3_BAUD);
        break;
    case UART4_BAUD:
        if ((apra & 0x8000) == 0) {
            return NOT_ON_VAL;
        }
        ret_val = get_core_clk_source() / clock_peri_divider(UART4_BAUD);
        break;

    case SSI1_BAUD:
        if ((acder & (1 << 5)) == 0) {
            return NOT_ON_VAL;
        }

        sel = ascsr & 0x1;

        if (sel == 1) {
            ret_val = get_core_clk_source() / clock_peri_divider(SSI1_BAUD);
        } else {
            ret_val = FREQ_CKIH_26M / clock_peri_divider(SSI1_BAUD);
        }
        break;
    case CSI_BAUD:
        if ((acder & (1 << 13)) == 0) {
            return NOT_ON_VAL;
        }

        sel = (ascsr >> 1) & 0x1;

        if (sel == 1) {
            ret_val = get_core_clk_source() / clock_peri_divider(CSI_BAUD);
        } else {
            ret_val = FREQ_CKIH_26M / clock_peri_divider(CSI_BAUD);
        }
        break;
    case SPI1_CLK:
        ret_val = get_main_clock(IPG_CLK);
        break;
    }

    return ret_val;
}

#if 0
RedBoot_cmd("ckoh",
            "Select clock source for CKOH (J9 on CPU daughter card)",
            " Default is 1/10 of ARM core\n\
          <0> - display current ckoh selection \n\
          <1> - ap_uncorrected_pat_ref_clk \n\
          <2> - ungated_ap_clk (ARM Core in normal case) \n\
          <3> - ungated_ap_ahb_clk (AHB) \n\
          <4> - ungated_ap_pclk (IPG) \n\
          <5> - usb_clk \n\
          <6> - ap_perclk (baud clock) \n\
          <7> - ap_ckil_clk (sync) \n\
          <8> - ap_pat_ref_clk (ungated sync) \n\
          <<The following only valid for Rev2.0 silicon and above>> \n\
          <9> - crm_ap_nfc_clk \n\
          <10> - ap_async_pat_ref_clk for EL1T and MQSPI \n\
          <11> - ap_sdhc1_perclk \n\
          <12> - ap_ahb_div2_clk (for SAHARA) \n\
          <13> - ipu_lpmc_hsp_clk\n",
            ckoh
           );

static u8* div_str[] = {
    "1/2 of ",
    "1/3 of ",
    "1/4 of ",
    "1/5 of ",
    "1/6 of ",
    "1/8 of ",
    "1/10 of ",
    "1/12 of ",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
};

static u8* ckoh_name[] ={
    "NULL",
    "ap_uncorrected_pat_ref_clk",
    "ungated_ap_clk (ARM Core in normal case)",
    "ungated_ap_ahb_clk (AHB)",
    "ungated_ap_pclk (IPG)",
    "usb_clk",
    "ap_perclk (baud clock)",
    "ap_ckil_clk (sync)",
    "ap_pat_ref_clk (ungated sync)",
    "crm_ap_nfc_clk",
    "ap_async_pat_ref_clk for EL1T and MQSPI",
    "ap_sdhc1_perclk",
    "ap_ahb_div2_clk (for SAHARA)",
    "ipu_lpmc_hsp_clk",
};

#define CKOH_MAX_INDEX          (sizeof(ckoh_name) / sizeof(u8*))
#define CKOH_DIV                6  // default divide by 10

extern u32 system_rev;

static void ckoh(int argc,char *argv[])
{
    u32 action = 0, val, new_val, div = 0x8, i;

    if (!scan_opts(argc, argv, 1, 0, 0, (void*) &action,
                   OPTION_ARG_TYPE_NUM, "action"))
        return;

    if (action >= CKOH_MAX_INDEX ||
        (system_rev == CHIP_REV_1_0 && action > 8)) {
        diag_printf("%d is not supported\n\n", action);
        return;
    }

    val = readl(CRM_AP_BASE_ADDR + CRM_AP_ACR);

    if (action != 0) {
        // set CKOHDIV to be 6 for dividing by 10
        if (action == 2 || action == 3)
            div = CKOH_DIV;
        action--;
        // clear CKOHS-HIGH, CKOHD, CHOHS, CKOHDIV bits and
        new_val = (val & (~(1 << 18 | 0xFF00))) | (div << 8);
        if (action > 7) {
            new_val |= (1 << 18) | ((action & 7) << 12);
        } else {
            new_val |= action << 12;
        }
        writel(new_val, CRM_AP_BASE_ADDR + CRM_AP_ACR);
        diag_printf("\nSet ckoh to ");
    }

    val = readl(CRM_AP_BASE_ADDR + CRM_AP_ACR);
    /* locate the index in the name table */
    new_val = ((val >> 15) & 8) | ((val >> 12) & 7);
    i = (val >> 8) & 0xF;
    diag_printf("%s%s\n", div_str[i], ckoh_name[new_val + 1]);
    diag_printf("ACR register[0x%x]=0x%x\n\n",
                (CRM_AP_BASE_ADDR + CRM_AP_ACR), val);
}

RedBoot_cmd("ckol",
            "Select clock source for CKO (J10 on EVB CPU card)",
            " Default is CKIL\n\
          <0> - display current cko selection\n\
          <1> - ckil \n\
          <2> - ap_pat_ref_clk (ungated sync) \n\
          <3> - ap_ref_x2_clk \n\
          <4} - ssi1_clk \n\
          <6> - cs_clk \n\
          <7> - RESERVED \n\
          <8> - RESERVED \n\
          <<The following only valid for Rev2.0 silicon and above>> \n\
          <9> - dfm_ckil_multiply_clk \n\
          <10> - ap_sdhc2_perclk \n\
          <11> - ap_uart3_per_clk \n",
            ckol
           );

static u8* cko_name[] ={
    "NULL",
    "ckil",
    "ap_pat_ref_clk (ungated sync)",
    "ap_ref_x2_clk",
    "ssi1_clk",
    "cs_clk",
    "RESERVED",
    "RESERVED",
    "dfm_ckil_multiply_clk",
    "ap_sdhc2_perclk",
    "ap_uart3_per_clk",
};

#define CKO_MAX_INDEX           (sizeof(cko_name) / sizeof(u8*))

static void ckol(int argc,char *argv[])
{
    u32 action = 0, val, new_val, t;

    if (!scan_opts(argc, argv, 1, 0, 0, (void*) &action,
                   OPTION_ARG_TYPE_NUM, "action"))
        return;

    if (action >= CKO_MAX_INDEX ||
        (system_rev == CHIP_REV_1_0 && action > 6) ||
        (action >= 7 && action <= 8)) {
        diag_printf("%d is not supported\n\n", action);
        return;
    }

    val = readl(CRM_AP_BASE_ADDR + CRM_AP_ACR);

    if (action != 0) {
        // turn on these clocks
        switch (action) {
        case 4: //SSI1
            t = readl(CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            writel(t | (1 << 6), CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            break;
        case 6: //cs_clk
            t = readl(CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            writel(t | (1 << 30), CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            break;

        }
        action--;
        /* clear CKOS-HIGH, CKOD, CHOS bits and */
        new_val = val & (~((1 << 16) | 0xF0));
        if (action > 5) {
            new_val |= (1 << 16) | ((action & 7) << 4);
        } else {
            new_val |= action << 4;
        }
        writel(new_val, CRM_AP_BASE_ADDR + CRM_AP_ACR);
        diag_printf("\nSet cko to ");
    }

    val = readl(CRM_AP_BASE_ADDR + CRM_AP_ACR);
    /* locate the index in the name table */
    new_val = ((val >> 13) & 8) | ((val >> 4) & 7);

    diag_printf("%s\nACR register[0x%x]=0x%x\n\n", cko_name[new_val + 1],
                (CRM_AP_BASE_ADDR + CRM_AP_ACR), val);
}
#endif

#ifdef L2CC_ENABLED
/*
 * This command is added for some simple testing only. It turns on/off
 * L2 cache regardless of L1 cache state. The side effect of this is
 * when doing any flash operations such as "fis init", the L2
 * will be turned back on along with L1 caches even though it is off
 * by using this command.
 */
RedBoot_cmd("L2",
            "L2 cache",
            "[ON | OFF]",
            do_L2_caches
           );

void do_L2_caches(int argc, char *argv[])
{
    u32 oldints;
    int L2cache_on=0;

    if (argc == 2) {
        if (strcasecmp(argv[1], "on") == 0) {
            HAL_DISABLE_INTERRUPTS(oldints);
            HAL_ENABLE_L2();
            HAL_RESTORE_INTERRUPTS(oldints);
        } else if (strcasecmp(argv[1], "off") == 0) {
            HAL_DISABLE_INTERRUPTS(oldints);
            HAL_CLEAN_INVALIDATE_L2();
            HAL_DISABLE_L2();
            HAL_RESTORE_INTERRUPTS(oldints);
        } else {
            diag_printf("Invalid L2 cache mode: %s\n", argv[1]);
        }
    } else {
        HAL_L2CACHE_IS_ENABLED(L2cache_on);
        diag_printf("L2 cache: %s\n", L2cache_on?"On":"Off");
    }
}
#endif //L2CC_ENABLED

#define IIM_ERR_SHIFT       8
#define POLL_FUSE_PRGD      (IIM_STAT_PRGD | (IIM_ERR_PRGE << IIM_ERR_SHIFT))
#define POLL_FUSE_SNSD      (IIM_STAT_SNSD | (IIM_ERR_SNSE << IIM_ERR_SHIFT))

static void fuse_op_start(void)
{
    /* Do not generate interrupt */
    writel(0, IIM_BASE_ADDR + IIM_STATM_OFF);
    // clear the status bits and error bits
    writel(0x3, IIM_BASE_ADDR + IIM_STAT_OFF);
    writel(0xFE, IIM_BASE_ADDR + IIM_ERR_OFF);
}

/*
 * The action should be either:
 *          POLL_FUSE_PRGD
 * or:
 *          POLL_FUSE_SNSD
 */
static int poll_fuse_op_done(int action)
{

    u32 status, error;

    if (action != POLL_FUSE_PRGD && action != POLL_FUSE_SNSD) {
        diag_printf("%s(%d) invalid operation\n", __FUNCTION__, action);
        return -1;
    }

    /* Poll busy bit till it is NOT set */
    while ((readl(IIM_BASE_ADDR + IIM_STAT_OFF) & IIM_STAT_BUSY) != 0 ) {
    }

    /* Test for successful write */
    status = readl(IIM_BASE_ADDR + IIM_STAT_OFF);
    error = readl(IIM_BASE_ADDR + IIM_ERR_OFF);

    if ((status & action) != 0 && (error & (action >> IIM_ERR_SHIFT)) == 0) {
        if (error) {
            diag_printf("Even though the operation seems successful...\n");
            diag_printf("There are some error(s) at addr=0x%x: 0x%x\n",
                        (IIM_BASE_ADDR + IIM_ERR_OFF), error);
        }
        return 0;
    }
    diag_printf("%s(%d) failed\n", __FUNCTION__, action);
    diag_printf("status address=0x%x, value=0x%x\n",
                (IIM_BASE_ADDR + IIM_STAT_OFF), status);
    diag_printf("There are some error(s) at addr=0x%x: 0x%x\n",
                (IIM_BASE_ADDR + IIM_ERR_OFF), error);
    return -1;
}

static void sense_fuse(int bank, int row, int bit)
{
    int addr, addr_l, addr_h, reg_addr;

    fuse_op_start();

    addr = ((bank << 11) | (row << 3) | (bit & 0x7));
    /* Set IIM Program Upper Address */
    addr_h = (addr >> 8) & 0x000000FF;
    /* Set IIM Program Lower Address */
    addr_l = (addr & 0x000000FF);

#ifdef IIM_FUSE_DEBUG
    diag_printf("%s: addr_h=0x%x, addr_l=0x%x\n",
                __FUNCTION__, addr_h, addr_l);
#endif
    writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
    writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
    /* Start sensing */
    writel(0x8, IIM_BASE_ADDR + IIM_FCTL_OFF);
    if (poll_fuse_op_done(POLL_FUSE_SNSD) != 0) {
        diag_printf("%s(bank: %d, row: %d, bit: %d failed\n",
                    __FUNCTION__, bank, row, bit);
    }
    reg_addr = IIM_BASE_ADDR + IIM_SDAT_OFF;
    diag_printf("fuses at (bank:%d, row:%d) = 0x%x\n", bank, row, readl(reg_addr));
}

void do_fuse_read(int argc, char *argv[])
{
    int bank, row;

    if (argc == 1) {
        diag_printf("Useage: fuse_read <bank> <row>\n");
        return;
    } else if (argc == 3) {
        if (!parse_num(*(&argv[1]), (unsigned long *)&bank, &argv[1], " ")) {
                diag_printf("Error: Invalid parameter\n");
            return;
        }
        if (!parse_num(*(&argv[2]), (unsigned long *)&row, &argv[2], " ")) {
                diag_printf("Error: Invalid parameter\n");
                return;
            }

        diag_printf("Read fuse at bank:%d row:%d\n", bank, row);
        sense_fuse(bank, row, 0);

    } else {
        diag_printf("Passing in wrong arguments: %d\n", argc);
        diag_printf("Useage: fuse_read <bank> <row>\n");
    }
}

/* Blow fuses based on the bank, row and bit positions (all 0-based)
*/
static int fuse_blow(int bank,int row,int bit)
{
    int addr, addr_l, addr_h, ret = -1;

    fuse_op_start();

    /* Disable IIM Program Protect */
    writel(0xAA, IIM_BASE_ADDR + IIM_PREG_P_OFF);

    addr = ((bank << 11) | (row << 3) | (bit & 0x7));
    /* Set IIM Program Upper Address */
    addr_h = (addr >> 8) & 0x000000FF;
    /* Set IIM Program Lower Address */
    addr_l = (addr & 0x000000FF);

#ifdef IIM_FUSE_DEBUG
    diag_printf("blowing addr_h=0x%x, addr_l=0x%x\n", addr_h, addr_l);
#endif

    writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
    writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
    /* Start Programming */
    writel(0x31, IIM_BASE_ADDR + IIM_FCTL_OFF);
    if (poll_fuse_op_done(POLL_FUSE_PRGD) == 0) {
        ret = 0;
    }

    /* Enable IIM Program Protect */
    writel(0x0, IIM_BASE_ADDR + IIM_PREG_P_OFF);
    return ret;
}

/*
 * This command is added for burning IIM fuses
 */
RedBoot_cmd("fuse_read",
            "read some fuses",
            "<bank> <row>",
            do_fuse_read
           );

RedBoot_cmd("fuse_blow",
            "blow some fuses",
            "<bank> <row> <value>",
            do_fuse_blow
           );

#define         INIT_STRING              "12345678"
static char ready_to_blow[] = INIT_STRING;

void quick_itoa(u32 num, char *a)
{
    int i, j, k;
    for (i = 0; i <= 7; i++) {
        j = (num >> (4 * i)) & 0xF;
        k = (j < 10) ? '0' : ('a' - 0xa);
        a[i] = j + k;
    }
}

void do_fuse_blow(int argc, char *argv[])
{
    int bank, row, value, i;

    if (argc == 1) {
        diag_printf("It is too dangeous for you to use this command.\n");
        return;
    } else if (argc == 2) {
        if (strcasecmp(argv[1], "nandboot") == 0) {
            quick_itoa(readl(EPIT_BASE_ADDR + EPITCNR), ready_to_blow);
            diag_printf("%s\n", ready_to_blow);
        }
        return;
    } else if (argc == 3) {
        if (strcasecmp(argv[1], "nandboot") == 0 &&
            strcasecmp(argv[2], ready_to_blow) == 0) {
            diag_printf("Ready to burn NAND boot fuses\n");
            if (fuse_blow(0, 16, 1) != 0 || fuse_blow(0, 16, 7) != 0) {
                diag_printf("NAND BOOT fuse blown failed miserably ...\n");
            } else {
                diag_printf("NAND BOOT fuse blown successfully ...\n");
            }
        } else {
            diag_printf("Not ready: %s, %s\n", argv[1], argv[2]);
        }
    } else if (argc == 4) {
        if (!parse_num(*(&argv[1]), (unsigned long *)&bank, &argv[1], " ")) {
                diag_printf("Error: Invalid parameter\n");
            return;
        }
        if (!parse_num(*(&argv[2]), (unsigned long *)&row, &argv[2], " ")) {
                diag_printf("Error: Invalid parameter\n");
                return;
        }
        if (!parse_num(*(&argv[3]), (unsigned long *)&value, &argv[3], " ")) {
                diag_printf("Error: Invalid parameter\n");
                return;
        }

        diag_printf("Blowing fuse at bank:%d row:%d value:%d\n",
                    bank, row, value);
        for (i = 0; i < 8; i++) {
            if (((value >> i) & 0x1) == 0) {
                continue;
            }
            if (fuse_blow(bank, row, i) != 0) {
                diag_printf("fuse_blow(bank: %d, row: %d, bit: %d failed\n",
                            bank, row, i);
            } else {
                diag_printf("fuse_blow(bank: %d, row: %d, bit: %d successful\n",
                            bank, row, i);
            }
        }
        sense_fuse(bank, row, 0);

    } else {
        diag_printf("Passing in wrong arguments: %d\n", argc);
    }
    /* Reset to default string */
    strcpy(ready_to_blow, INIT_STRING);;
}

/* precondition: m>0 and n>0.  Let g=gcd(m,n). */
int gcd(int m, int n)
{
    int t;
    while(m > 0) {
        if(n > m) {t = m; m = n; n = t;} /* swap */
        m -= n;
    }
    return n;
 }
