//==========================================================================
//
//		hal_soc.h
//
//		SoC chip definitions
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
// Copyright (C) 2002 Gary Thomas
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#ifndef __HAL_SOC_H__
#define __HAL_SOC_H__

#include <cyg/hal/mx27_pins.h>

#ifdef __ASSEMBLER__
#define UL(a)		 (a)
#define REG8_VAL(a)	 (a)
#define REG16_VAL(a) (a)
#define REG32_VAL(a) (a)

#define REG8_PTR(a)	 (a)
#define REG16_PTR(a) (a)
#define REG32_PTR(a) (a)

#else /* __ASSEMBLER__ */

#define UL(a)		 (a##UL)

extern char HAL_PLATFORM_EXTRA[20];
#define REG8_VAL(a)						((unsigned char)(a))
#define REG16_VAL(a)					((unsigned short)(a))
#define REG32_VAL(a)					((unsigned int)(a))

#define REG8_PTR(a)						((volatile unsigned char *)(a))
#define REG16_PTR(a)					((volatile unsigned short *)(a))
#define REG32_PTR(a)					((volatile unsigned int *)(a))
#define readb(a)						(*(volatile unsigned char *)(a))
#define readw(a)						(*(volatile unsigned short *)(a))
#define readl(a)						(*(volatile unsigned int *)(a))
#define writeb(v,a)						(*(volatile unsigned char *)(a) = (v))
#define writew(v,a)						(*(volatile unsigned short *)(a) = (v))
#define writel(v,a)						(*(volatile unsigned int *)(a) = (v))

#endif /* __ASSEMBLER__ */

/*
 * Default Memory Layout Definitions
 */

#define SOC_AIPI1_BASE					UL(0x10000000)
#define SOC_AIPI2_BASE					UL(0x10020000)

#define SOC_AIPI_PAR_OFF				8

#define CSPI1_BASE_ADDR					(SOC_AIPI1_BASE + 0x0E000)
#define CSPI2_BASE_ADDR					(SOC_AIPI1_BASE + 0x0F000)
#define CSPI3_BASE_ADDR					(SOC_AIPI1_BASE + 0x17000)

#define SOC_CRM_BASE					UL(0x10027000)
#define SOC_CRM_CSCR					(SOC_CRM_BASE + 0x0)
#define SOC_CRM_MPCTL0					(SOC_CRM_BASE + 0x4)
#define SOC_CRM_MPCTL1					(SOC_CRM_BASE + 0x8)
#define SOC_CRM_SPCTL0					(SOC_CRM_BASE + 0xC)
#define SOC_CRM_SPCTL1					(SOC_CRM_BASE + 0x10)
#define SOC_CRM_OSC26MCTL				(SOC_CRM_BASE + 0x14)
#define SOC_CRM_PCDR0					(SOC_CRM_BASE + 0x18)
#define SOC_CRM_PCDR1					(SOC_CRM_BASE + 0x1C)
#define SOC_CRM_PCCR0					(SOC_CRM_BASE + 0x20)
#define SOC_CRM_PCCR1					(SOC_CRM_BASE + 0x24)
#define SOC_CRM_CCSR					(SOC_CRM_BASE + 0x28)
#define SOC_CRM_PMCTL					(SOC_CRM_BASE + 0x2C)
#define SOC_CRM_PMCOUNT					(SOC_CRM_BASE + 0x30)
#define SOC_CRM_WKGDCTL					(SOC_CRM_BASE + 0x34)

#define CRM_CSCR_IPDIV_OFFSET			8
#define CRM_CSCR_BCLKDIV_OFFSET			9
#define CRM_CSCR_PRESC_OFFSET			13
#define CRM_CSCR_SSI1_SEL_OFFSET		22
#define CRM_CSCR_SSI2_SEL_OFFSET		23
#define CRM_CSCR_USB_DIV_OFFSET			28

#define CRM_CSCR_ARM_OFFSET				12
#define CRM_CSCR_ARM_SRC				(1<<15)
#define CRM_CSCR_AHB_OFFSET				8

#define FREQ_26MHZ						26000000
#define FREQ_27MHZ						27000000
#define FREQ_32768HZ					(32768 * 512 * 2)
#define FREQ_32000HZ					(32000 * 512 * 2)

#if 0
/* These should be defined in platform specific files */
//#define CLOCK_266_133_66
#define CLOCK_399_133_66
//#define CLOCK_399_100_50

//#define PLL_REF_CLK					FREQ_32768HZ
#define PLL_REF_CLK						FREQ_26MHZ
//#define PLL_REF_CLK					FREQ_32000HZ
#else
#define PLL_REF_CLK		CYGHWR_HAL_ARM_SOC_PLL_REF_CLOCK
#if (CYGHWR_HAL_ARM_SOC_PROCESSOR_CLOCK == 399)
  #if (CYGHWR_HAL_ARM_SOC_SYSTEM_CLOCK == 133)
	#define CLOCK_399_133_66
  #elif (CYGHWR_HAL_ARM_SOC_SYSTEM_CLOCK == 100)
	#define CLOCK_399_100_50
  #else
	#error Invalid SYSTEM clock (CYGHWR_HAL_ARM_SOC_SYSTEM_CLOCK) defined
  #endif
#elif (CYGHWR_HAL_ARM_SOC_PROCESSOR_CLOCK == 266)
  #define CLOCK_266_133_66
#else
  #error Invalid processor clock (CYGHWR_HAL_ARM_SOC_PROCESSOR_CLOCK) defined
#endif
#endif

//													PD				MFD				MFI				MFN
#define CRM_PLL_PCTL_PARAM(pd, fd, fi, fn)		((((pd)-1)<<26) + (((fd)-1)<<16) + ((fi)<<10) + (((fn) & 0x3ff) << 0))

#define SPLL_REF_CLK_kHz				240000

#if (PLL_REF_CLK == FREQ_32768HZ)
	#define PLL_REF_CLK_32768HZ
	// SPCTL0  for 240 MHz
	#define CRM_SPCTL0_VAL				CRM_PLL_PCTL_PARAM(2, 124, 7, 19)
	#define CRM_SPCTL0_VAL_27MHZ		CRM_SPCTL0_VAL
	#define CRM_SPCTL0_VAL2				CRM_PLL_PCTL_PARAM(4, 567, 14, 173)
	#define CRM_SPCTL0_VAL2_27MHZ		CRM_SPCTL0_VAL2
	#if defined (CLOCK_266_133_66)
		#define MPLL_REF_CLK_kHz		(CYGHWR_HAL_ARM_SOC_PROCESSOR_CLOCK * 1500)
		#define CRM_MPCTL0_VAL			CRM_PLL_PCTL_PARAM(2, 400, 7, 371)
		#define CRM_MPCTL0_VAL_27MHZ	CRM_MPCTL0_VAL
		#define CRM_CSCR_VAL			0x33F00307
		#define CRM_MPCTL0_VAL2			CRM_PLL_PCTL_PARAM(1, 496, 5, 469)
		#define CRM_MPCTL0_VAL2_27MHZ	CRM_MPCTL0_VAL2
		#define CRM_CSCR_VAL2			0x33F00107
	#elif defined (CLOCK_399_133_66)
		#define MPLL_REF_CLK_kHz		(CYGHWR_HAL_ARM_SOC_PROCESSOR_CLOCK * 1000)
		#define CRM_MPCTL0_VAL			CRM_PLL_PCTL_PARAM(2, 100, 11, 89)
		#define CRM_MPCTL0_VAL_27MHZ	CRM_MPCTL0_VAL
		#define CRM_CSCR_VAL			0x33F00507
		#define CRM_MPCTL0_VAL2			CRM_PLL_PCTL_PARAM(1, 496, 5, 469)
		#define CRM_MPCTL0_VAL2_27MHZ	CRM_MPCTL0_VAL2
		#define CRM_CSCR_VAL2			0x33F08107
	#elif defined (CLOCK_399_100_50)
		#define MPLL_REF_CLK_kHz		(CYGHWR_HAL_ARM_SOC_PROCESSOR_CLOCK * 1000)
		#define CRM_MPCTL0_VAL			CRM_PLL_PCTL_PARAM(2, 100, 11, 89)
		#define CRM_MPCTL0_VAL_27MHZ	CRM_MPCTL0_VAL
		#define CRM_CSCR_VAL			0x33F00307
		#define CRM_MPCTL0_VAL2			CRM_PLL_PCTL_PARAM(1, 100, 11, 94)
		#define CRM_MPCTL0_VAL2_27MHZ	CRM_MPCTL0_VAL2
		#define CRM_CSCR_VAL2			0x33F00307
	#else
		#error This clock is not supported !!!!
	#endif	 // CLOCK_266_133_66
#else // PLL_REF_CLK == FREQ_32768HZ
#define PLL_VAL_239_999					CRM_PLL_PCTL_PARAM(2, 13, 9, 3)
#define PLL_VAL_240						CRM_PLL_PCTL_PARAM(3, 13, 13, 11)
#define PLL_VAL_265_999					CRM_PLL_PCTL_PARAM(2, 26, 10, 6)
#define PLL_VAL_266						CRM_PLL_PCTL_PARAM(3, 26, 15, 9)
#define PLL_VAL_399						CRM_PLL_PCTL_PARAM(1, 52, 7, 35)
#define PLL_VAL_399_ALT					CRM_PLL_PCTL_PARAM(2, 26, 15, 9)
#define PLL_VAL_400						CRM_PLL_PCTL_PARAM(2, 13, 15, 5)
#define PLL_VAL_600						CRM_PLL_PCTL_PARAM(1, 13, 11, 7)
#define PLL_VAL_600_ALT					CRM_PLL_PCTL_PARAM(1, 52, 11, 28)
#define PLL_VAL_598_5					CRM_PLL_PCTL_PARAM(1, 104, 11, 53)

	// SPCTL0  for 240 MHz
	#define CRM_SPCTL0_VAL				PLL_VAL_240
	#define CRM_SPCTL0_VAL_27MHZ		CRM_PLL_PCTL_PARAM(2, 9, 8, 8)
	#define CRM_SPCTL0_VAL2				CRM_SPCTL0_VAL
	#define CRM_SPCTL0_VAL2_27MHZ		CRM_SPCTL0_VAL_27MHZ

	#if defined (CLOCK_266_133_66)
		#define MPLL_REF_CLK_kHz		(CYGHWR_HAL_ARM_SOC_PROCESSOR_CLOCK * 1500)
		#define CRM_MPCTL0_VAL			PLL_VAL_266 // 265.999
		#define CRM_MPCTL0_VAL_27MHZ	CRM_PLL_PCTL_PARAM(2, 15, 9, 13)	// 266.4 MHz
		#define CRM_CSCR_VAL			0x33F30307
		#define CRM_MPCTL0_VAL2			PLL_VAL_399
		#define CRM_MPCTL0_VAL2_27MHZ	CRM_PLL_PCTL_PARAM(1, 5, 7, 2)		// 399.6 MHz
		#define CRM_CSCR_VAL2			0x33F30107
	#elif defined (CLOCK_399_133_66)
		#define MPLL_REF_CLK_kHz		(CYGHWR_HAL_ARM_SOC_PROCESSOR_CLOCK * 1000)
		#define CRM_MPCTL0_VAL			PLL_VAL_399
		#define CRM_MPCTL0_VAL_27MHZ	CRM_PLL_PCTL_PARAM(1, 5, 7, 2)		// 399.6 MHz
		#define CRM_CSCR_VAL			0x33F30507
		#define CRM_MPCTL0_VAL2			CRM_MPCTL0_VAL
		#define CRM_MPCTL0_VAL2_27MHZ	CRM_MPCTL0_VAL_27MHZ
		#define CRM_CSCR_VAL2			0x33F38107
	#elif defined (CLOCK_399_100_50)
		#define MPLL_REF_CLK_kHz		(CYGHWR_HAL_ARM_SOC_PROCESSOR_CLOCK * 1000)
		#define CRM_MPCTL0_VAL			PLL_VAL_399
		#define CRM_MPCTL0_VAL_27MHZ	CRM_PLL_PCTL_PARAM(1, 5, 7, 2)		// 399.6 MHz
		#define CRM_CSCR_VAL			0x33F30307
		#define CRM_MPCTL0_VAL2			PLL_VAL_399
		#define CRM_MPCTL0_VAL2_27MHZ	CRM_MPCTL0_VAL_27MHZ
		#define CRM_CSCR_VAL2			0x33F38307
	#else
		#error This clock is not supported !!!!
	#endif	 // CLOCK_266_133_66

#endif	// PLL_REF_CLK == FREQ_26MHZ

// system control
#define SOC_SYSCTRL_BASE				UL(0x10027800)
#define SOC_SYSCTRL_CID					(SOC_SYSCTRL_BASE + 0x00)
#define SOC_SYSCTRL_FMCR				(SOC_SYSCTRL_BASE + 0x14)
#define FMCR_FMS						(1 << 5)
#define FMCR_NF_16BIT					(1 << 4)
#define FMCR_SLCDC_SEL					(1 << 2)
#define FMCR_SDCS1_SEL					(1 << 1)
#define FMCR_SDCS0_SEL					(1 << 0)
#define SOC_SYSCTRL_GPCR				(SOC_SYSCTRL_BASE + 0x18)
#define SOC_SYSCTRL_WBCR				(SOC_SYSCTRL_BASE + 0x1C)
#define SOC_SYSCTRL_DSCR1				(SOC_SYSCTRL_BASE + 0x20)
#define SOC_SYSCTRL_DSCR2				(SOC_SYSCTRL_BASE + 0x24)
#define SOC_SYSCTRL_DSCR3				(SOC_SYSCTRL_BASE + 0x28)
#define SOC_SYSCTRL_DSCR4				(SOC_SYSCTRL_BASE + 0x2C)
#define SOC_SYSCTRL_DSCR5				(SOC_SYSCTRL_BASE + 0x30)
#define SOC_SYSCTRL_DSCR6				(SOC_SYSCTRL_BASE + 0x34)
#define SOC_SYSCTRL_DSCR7				(SOC_SYSCTRL_BASE + 0x38)
#define SOC_SYSCTRL_DSCR8				(SOC_SYSCTRL_BASE + 0x3C)
#define SOC_SYSCTRL_DSCR9				(SOC_SYSCTRL_BASE + 0x40)
#define SOC_SYSCTRL_DSCR10				(SOC_SYSCTRL_BASE + 0x44)
#define SOC_SYSCTRL_DSCR11				(SOC_SYSCTRL_BASE + 0x48)
#define SOC_SYSCTRL_DSCR12				(SOC_SYSCTRL_BASE + 0x4C)
#define SOC_SYSCTRL_DSCR13				(SOC_SYSCTRL_BASE + 0x50)
#define SOC_SYSCTRL_PSCR				(SOC_SYSCTRL_BASE + 0x54)
#define SOC_SYSCTRL_PCSR				(SOC_SYSCTRL_BASE + 0x58)
#define SOC_SYSCTRL_PMCR				(SOC_SYSCTRL_BASE + 0x60)
#define SOC_SYSCTRL_DCVR0				(SOC_SYSCTRL_BASE + 0x64)
#define SOC_SYSCTRL_DCVR1				(SOC_SYSCTRL_BASE + 0x68)
#define SOC_SYSCTRL_DCVR2				(SOC_SYSCTRL_BASE + 0x6C)
#define SOC_SYSCTRL_DCVR3				(SOC_SYSCTRL_BASE + 0x70)

// Interrupt Controller Register Definitions.
#define SOC_AITC_BASE					UL(0x10040000)
#define SOC_AITC_INTCNTL				(SOC_AITC_BASE + 0x00)
#define SOC_AITC_NIMASK					(SOC_AITC_BASE + 0x04)
#define SOC_AITC_INTENNUM				(SOC_AITC_BASE + 0x08)
#define SOC_AITC_INTDISNUM				(SOC_AITC_BASE + 0x0C)
#define SOC_AITC_INTENABLEH				(SOC_AITC_BASE + 0x10)
#define SOC_AITC_INTENABLEL				(SOC_AITC_BASE + 0x14)
#define SOC_AITC_INTTYPEH				(SOC_AITC_BASE + 0x18)
#define SOC_AITC_INTTYPEL				(SOC_AITC_BASE + 0x1C)
#define SOC_AITC_NIPRIORITY7			(SOC_AITC_BASE + 0x20)
#define SOC_AITC_NIPRIORITY6			(SOC_AITC_BASE + 0x24)
#define SOC_AITC_NIPRIORITY5			(SOC_AITC_BASE + 0x28)
#define SOC_AITC_NIPRIORITY4			(SOC_AITC_BASE + 0x2C)
#define SOC_AITC_NIPRIORITY3			(SOC_AITC_BASE + 0x30)
#define SOC_AITC_NIPRIORITY2			(SOC_AITC_BASE + 0x34)
#define SOC_AITC_NIPRIORITY1			(SOC_AITC_BASE + 0x38)
#define SOC_AITC_NIPRIORITY0			(SOC_AITC_BASE + 0x3C)

#define UART_WIDTH_32

// UART Base Addresses
#define SOC_UART1_BASE					UL(0x1000A000)
#define SOC_UART2_BASE					UL(0x1000B000)
#define SOC_UART3_BASE					UL(0x1000C000)
#define SOC_UART4_BASE					UL(0x1000D000)
#define SOC_UART5_BASE					UL(0x1001B000)
#define SOC_UART6_BASE					UL(0x1001C000)

#define SOC_MAX_BASE					UL(0x1003F000)
// Slave port base offset
#define MAX_SLAVE_PORT0_OFFSET			0x0
#define MAX_SLAVE_PORT1_OFFSET			0x100
#define MAX_SLAVE_PORT2_OFFSET			0x200
// Register offset for slave port
#define MAX_SLAVE_MPR_OFFSET			0x0				/* Master Priority register */
#define MAX_SLAVE_AMPR_OFFSET			0x4				/* Alternate Master Priority register */
#define MAX_SLAVE_SGPCR_OFFSET			0x10	/* Slave General Purpose Control register */
#define MAX_SLAVE_ASGPCR_OFFSET			0x14	/* Alternate Slave General Purpose control register */
// Master port base offset
#define MAX_MASTER_PORT0_OFFSET			0x800
#define MAX_MASTER_PORT1_OFFSET			0x900
#define MAX_MASTER_PORT2_OFFSET			0xA00
#define MAX_MASTER_PORT3_OFFSET			0xB00
#define MAX_MASTER_PORT4_OFFSET			0xC00
#define MAX_MASTER_PORT5_OFFSET			0xD00
// Register offset for master port
#define MAX_MASTER_MGPCR_OFFSET			0x0				/* Master General Purpose Control Register */
/*
 * MX27 GPIO Register Definitions
 */
#define SOC_GPIOA_BASE					UL(0x10015000)
#define SOC_GPIOB_BASE					UL(0x10015100)
#define SOC_GPIOC_BASE					UL(0x10015200)
#define SOC_GPIOD_BASE					UL(0x10015300)
#define SOC_GPIOE_BASE					UL(0x10015400)
#define SOC_GPIOF_BASE					UL(0x10015500)
#define SOC_GPIO_PMASK					UL(0x10015600)
#define GPIO_DDIR						0x0				/* Data direction reg */
#define GPIO_OCR1						0x4				/* Output config reg 1 */
#define GPIO_OCR2						0x8				/* Output config reg 2 */
#define GPIO_ICONFA1					0xC				/* Input config reg A1 */
#define GPIO_ICONFA2					0x10			/* Input config reg A2 */
#define GPIO_ICONFB1					0x14			/* Input config reg B1 */
#define GPIO_ICONFB2					0x18			/* Input config reg B2 */
#define GPIO_DR							0x1C			/* Data reg */
#define GPIO_GIUS						0x20			/* GPIO in use reg */
#define GPIO_SSR						0x24			/* Sample status reg */
#define GPIO_ICR1						0x28			/* Int config reg 1 */
#define GPIO_ICR2						0x2C			/* Int config reg 2 */
#define GPIO_IMR						0x30			/* Int mask reg */
#define GPIO_ISR						0x34			/* Int status reg */
#define GPIO_GPR						0x38			/* Gen purpose reg */
#define GPIO_SWR						0x3C			/* Software reset reg */
#define GPIO_PUEN						0x40			/* Pull-up enable reg */

#define GPIO_OCR_A						0				/* External input a_IN */
#define GPIO_OCR_B						1				/* External input b_IN */
#define GPIO_OCR_C						2				/* External input c_IN */
#define GPIO_OCR_DR						3				/* Data register */
#define GPIO_ICONF_In					0				/* GPIO-in */
#define GPIO_ICONF_Isr					1				/* Interrupt status register */
#define GPIO_ICONF_0					2				/* 0 */
#define GPIO_ICONF_1					3				/* 1 */
#define GPIO_ICR_PosEdge				0				/* Positive edge */
#define GPIO_ICR_NegEdge				1				/* Negative edge */
#define GPIO_ICR_PosLvl					2				/* Positive level */
#define GPIO_ICR_NegLvl					3				/* Negative level */
#define GPIO_SWR_SWR					1				/* Software reset */

/*
 * GPT Timer defines
 */
#define HAL_DELAY_TIMER					SOC_GPT2_BASE	// use timer2 for hal_delay_us()

#define SOC_GPT1_BASE					UL(0x10003000)
#define SOC_GPT2_BASE					UL(0x10004000)
#define SOC_GPT3_BASE					UL(0x10005000)
#define SOC_GPT4_BASE					UL(0x10019000)
#define SOC_GPT5_BASE					UL(0x1001A000)
#define SOC_GPT6_BASE					UL(0x1001F000)
#define GPT_TCTL_OFFSET					0x0
#define GPT_TPRER_OFFSET				0x4
#define GPT_TCMP_OFFSET					0x8
#define GPT_TCR_OFFSET					0xC
#define GPT_TCN_OFFSET					0x10
#define GPT_TSTAT_OFFSET				0x14
#define MX_STARTUP_DELAY				(1000000 / 10)	// 0.1s delay to get around the ethernet reset failure problem

#define TIMER_PRESCALER					3
#define SOC_SI_ID_REG					UL(0x10027800)
#define SOC_SILICONID_Rev1_0			0x0
#define SOC_SILICONID_Rev2_0			0x1
#define SOC_SILICONID_Rev2_1			0x2
#define CHIP_REV_1_x					1
#define CHIP_REV_2_x					2
#define CHIP_REV_3_0					3
#define CHIP_REV_3_1					4
#define CHIP_REV_unknown				0x100

#define SOC_WDOG_BASE					UL(0x10002000)
#define WDOG_BASE_ADDR					SOC_WDOG_BASE

#define NFC_BASE						UL(0xD8000000)
#define SOC_ESDCTL_BASE					UL(0xD8001000)
#define SOC_EIM_BASE					UL(0xD8002000)
#define SOC_M3IF_BASE					UL(0xD8003000)
#define SOC_PCMCIA_BASE					UL(0xD8004000)

#define SOC_CS0_CTL_BASE				SOC_EIM_BASE
#define SOC_CS1_CTL_BASE				(SOC_EIM_BASE + 0x10)
#define SOC_CS2_CTL_BASE				(SOC_EIM_BASE + 0x20)
#define SOC_CS3_CTL_BASE				(SOC_EIM_BASE + 0x30)
#define SOC_CS4_CTL_BASE				(SOC_EIM_BASE + 0x40)
#define SOC_CS5_CTL_BASE				(SOC_EIM_BASE + 0x50)

/* WEIM */
#define CSCRU_OFFSET					0x00
#define CSCRL_OFFSET					0x04
#define CSCRA_OFFSET					0x08
#define CSWCR_OFFSET					0x60

// Memories
#define SOC_CSD0_BASE					UL(0xA0000000)
#define SOC_CSD1_BASE					UL(0xB0000000)
#define SOC_CS0_BASE					UL(0xC0000000)
#define CS0_BASE_ADDR					SOC_CS0_BASE
#define SOC_CS1_BASE					UL(0xC8000000)
#define SOC_CS2_BASE					UL(0xD0000000)
#define SOC_CS3_BASE					UL(0xD2000000)
#define SOC_CS4_BASE					UL(0xD4000000)
#define SOC_CS5_BASE					UL(0xD6000000)
#define NAND_REG_BASE					(NFC_BASE + 0xE00)

#define SOC_IIM_BASE					UL(0x10028000)
#define SOC_FEC_MAC_BASE				UL(0x10028C04)
#define SOC_FEC_MAC_BASE2				UL(0x10028814)
#define SOC_FEC_BASE					UL(0x1002B000)
#define IIM_BASE_ADDR					SOC_IIM_BASE
/* IIM */
#define CHIP_REV_1_0					0x0						/* PASS 1.0 */
#define CHIP_REV_2_0					0x1						/* PASS 2.0 */

#define IIM_STAT_OFF					0x00
#define IIM_STAT_BUSY					(1 << 7)
#define IIM_STAT_PRGD					(1 << 1)
#define IIM_STAT_SNSD					(1 << 0)
#define IIM_STATM_OFF					0x04
#define IIM_ERR_OFF						0x08
#define IIM_ERR_PRGE					(1 << 7)
#define IIM_ERR_WPE						(1 << 6)
#define IIM_ERR_OPE						(1 << 5)
#define IIM_ERR_RPE						(1 << 4)
#define IIM_ERR_WLRE					(1 << 3)
#define IIM_ERR_SNSE					(1 << 2)
#define IIM_ERR_PARITYE					(1 << 1)
#define IIM_EMASK_OFF					0x0C
#define IIM_FCTL_OFF					0x10
#define IIM_UA_OFF						0x14
#define IIM_LA_OFF						0x18
#define IIM_SDAT_OFF					0x1C
#define IIM_PREV_OFF					0x20
#define IIM_SREV_OFF					0x24
#define IIM_PREG_P_OFF					0x28
#define IIM_SCS0_OFF					0x2C
#define IIM_SCS1_P_OFF					0x30
#define IIM_SCS2_OFF					0x34
#define IIM_SCS3_P_OFF					0x38

#define ESDCTL_ESDCTL0					0x00
#define ESDCTL_ESDCFG0					0x04
#define ESDCTL_ESDCTL1					0x08
#define ESDCTL_ESDCFG1					0x0C
#define ESDCTL_ESDMISC					0x10
#define ESDCTL_ESDCDLY1					0x20
#define ESDCTL_ESDCDLY2					0x24
#define ESDCTL_ESDCDLY3					0x28
#define ESDCTL_ESDCDLY4					0x2c
#define ESDCTL_ESDCDLY5					0x30

#define NFC_BUFSIZE_REG_OFF				(0 + 0x00)
#define RAM_BUFFER_ADDRESS_REG_OFF		(0 + 0x04)
#define NAND_FLASH_ADD_REG_OFF			(0 + 0x06)
#define NAND_FLASH_CMD_REG_OFF			(0 + 0x08)
#define NFC_CONFIGURATION_REG_OFF		(0 + 0x0A)
#define ECC_STATUS_RESULT_REG_OFF		(0 + 0x0C)
#define ECC_RSLT_MAIN_AREA_REG_OFF		(0 + 0x0E)
#define ECC_RSLT_SPARE_AREA_REG_OFF		(0 + 0x10)
#define NF_WR_PROT_REG_OFF				(0 + 0x12)
#define UNLOCK_START_BLK_ADD_REG_OFF	(0 + 0x14)
#define UNLOCK_END_BLK_ADD_REG_OFF		(0 + 0x16)
#define NAND_FLASH_WR_PR_ST_REG_OFF		(0 + 0x18)
#define NAND_FLASH_CONFIG1_REG_OFF		(0 + 0x1A)
#define NAND_FLASH_CONFIG2_REG_OFF		(0 + 0x1C)
#define RAM_BUFFER_ADDRESS_RBA_3		0x3
#define NFC_BUFSIZE_1KB					0x0
#define NFC_BUFSIZE_2KB					0x1
#define NFC_CONFIGURATION_UNLOCKED		0x2
#define ECC_STATUS_RESULT_NO_ERR		0x0
#define ECC_STATUS_RESULT_1BIT_ERR		0x1
#define ECC_STATUS_RESULT_2BIT_ERR		0x2
#define NF_WR_PROT_UNLOCK				0x4
#define NAND_FLASH_CONFIG1_FORCE_CE		(1 << 7)
#define NAND_FLASH_CONFIG1_RST			(1 << 6)
#define NAND_FLASH_CONFIG1_BIG			(1 << 5)
#define NAND_FLASH_CONFIG1_INT_MSK		(1 << 4)
#define NAND_FLASH_CONFIG1_ECC_EN		(1 << 3)
#define NAND_FLASH_CONFIG1_SP_EN		(1 << 2)
#define NAND_FLASH_CONFIG2_INT_DONE		(1 << 15)
#define NAND_FLASH_CONFIG2_FDO_PAGE		(0 << 3)
#define NAND_FLASH_CONFIG2_FDO_ID		(2 << 3)
#define NAND_FLASH_CONFIG2_FDO_STATUS	(4 << 3)
#define NAND_FLASH_CONFIG2_FDI_EN		(1 << 2)
#define NAND_FLASH_CONFIG2_FADD_EN		(1 << 1)
#define NAND_FLASH_CONFIG2_FCMD_EN		(1 << 0)
#define FDO_PAGE_SPARE_VAL				0x8

#define MXC_NAND_BASE_DUMMY				UL(0xE0000000)
#define NOR_FLASH_BOOT					0
#define NAND_FLASH_BOOT					0x10
#define SDRAM_NON_FLASH_BOOT			0x20
#define MXCBOOT_FLAG_REG				SOC_AITC_NIPRIORITY7

#define MXCFIS_NOTHING					0x00000000
#define MXCFIS_NAND						0x10000000
#define MXCFIS_NOR						0x20000000
#define MXCFIS_FLAG_REG					SOC_AITC_NIPRIORITY6
#ifndef MXCFLASH_SELECT_NAND
#define IS_BOOTING_FROM_NAND()			0
#else
#define IS_BOOTING_FROM_NAND()			(readl(MXCBOOT_FLAG_REG) == NAND_FLASH_BOOT)
#endif
#ifndef MXCFLASH_SELECT_NOR
#define IS_BOOTING_FROM_NOR()			0
#else
#define IS_BOOTING_FROM_NOR()			(readl(MXCBOOT_FLAG_REG) == NOR_FLASH_BOOT)
#endif
#ifndef IS_BOOTING_FROM_SDRAM
#define IS_BOOTING_FROM_SDRAM()			(readl(MXCBOOT_FLAG_REG) == SDRAM_NON_FLASH_BOOT)
#endif

#ifndef MXCFLASH_SELECT_NAND
#define IS_FIS_FROM_NAND()				0
#else
#ifndef MXCFLASH_SELECT_NOR
#define IS_FIS_FROM_NAND()				1
#else
#define IS_FIS_FROM_NAND()				(readl(MXCFIS_FLAG_REG) == MXCFIS_NAND)
#endif
#endif

#ifndef MXCFLASH_SELECT_NOR
#define IS_FIS_FROM_NOR()				0
#else
#define IS_FIS_FROM_NOR()				(!IS_FIS_FROM_NAND())
#endif

#define MXC_ASSERT_NOR_BOOT()			writel(MXCFIS_NOR, MXCFIS_FLAG_REG)
#define MXC_ASSERT_NAND_BOOT()			writel(MXCFIS_NAND, MXCFIS_FLAG_REG)

#define SERIAL_DOWNLOAD_MAGIC			0x000000AA
#define SERIAL_DOWNLOAD_MAGIC_REG		SOC_AITC_NIPRIORITY3
#define SERIAL_DOWNLOAD_SRC_REG			SOC_AITC_NIPRIORITY2
#define SERIAL_DOWNLOAD_TGT_REG			SOC_AITC_NIPRIORITY1
#define SERIAL_DOWNLOAD_SZ_REG			SOC_AITC_NIPRIORITY0

#if !defined(__ASSEMBLER__)
void cyg_hal_plf_serial_init(void);
void cyg_hal_plf_serial_stop(void);
void hal_delay_us(unsigned int usecs);
#define HAL_DELAY_US(n)					hal_delay_us(n)

enum plls {
		MCU_PLL = SOC_CRM_MPCTL0,
		SER_PLL = SOC_CRM_SPCTL0,
};

enum main_clocks {
		CPU_CLK,
		AHB_CLK,
		IPG_CLK,
		NFC_CLK,
		USB_CLK,
};

enum peri_clocks {
		PER_CLK1,
		PER_CLK2,
		PER_CLK3,
		PER_CLK4,
		H264_BAUD,
		MSHC_BAUD,
		SSI1_BAUD,
		SSI2_BAUD,
		SPI1_CLK = CSPI1_BASE_ADDR,
		SPI2_CLK = CSPI2_BASE_ADDR,
};

unsigned int pll_clock(enum plls pll);

unsigned int get_main_clock(enum main_clocks clk);

unsigned int get_peri_clock(enum peri_clocks clk);
#define GPIO_PORT_NUM			6
#define GPIO_NUM_PIN			32
#define MXC_MAX_GPIO_LINES		(GPIO_NUM_PIN * GPIO_PORT_NUM)

#define IOMUX_TO_GPIO(pin)		((((unsigned int)pin >> MUX_IO_P) * GPIO_NUM_PIN) + ((pin >> MUX_IO_I) & ((1 << (MUX_IO_P - MUX_IO_I)) -1)))
#define IOMUX_TO_IRQ(pin)		(MXC_GPIO_BASE + IOMUX_TO_GPIO(pin))
#define GPIO_TO_PORT(n)			(n / GPIO_NUM_PIN)
#define GPIO_TO_INDEX(n)		(n % GPIO_NUM_PIN)

typedef enum {
		GPIO_MUX_PRIMARY,
		GPIO_MUX_ALT,
		GPIO_MUX_GPIO,
		GPIO_MUX_INPUT1,
		GPIO_MUX_INPUT2,
		GPIO_MUX_OUTPUT1,
		GPIO_MUX_OUTPUT2,
		GPIO_MUX_OUTPUT3,
} gpio_mux_mode_t;

int gpio_request_mux(iomux_pin_name_t pin, gpio_mux_mode_t mode);
void clock_spi_enable(unsigned int spi_clk);

typedef unsigned int nfc_setup_func_t(unsigned int, unsigned int, unsigned int, unsigned int);

#endif //#if !defined(__ASSEMBLER__)

#define HAL_MMU_OFF()													\
CYG_MACRO_START															\
	asm volatile (														\
		"1: "															\
		"mrc p15, 0, r15, c7, c14, 3;"	/*test clean and inval*/		\
		"bne 1b;"														\
		"mov r0, #0;"													\
		"mcr p15,0,r0,c7,c10,4;"   /* Data Write Barrier */				\
		"mcr p15,0,r0,c7,c5,0;" /* invalidate I cache */				\
		"mrc p15,0,r0,c1,c0,0;" /* read c1 */							\
		"bic r0,r0,#0x7;" /* disable DCache and MMU */					\
		"bic r0,r0,#0x1000;" /* disable ICache */						\
		"mcr p15,0,r0,c1,c0,0;" /*	*/									\
		"nop;" /* flush i+d-TLBs */										\
		"nop;" /* flush i+d-TLBs */										\
		"nop;" /* flush i+d-TLBs */										\
		:																\
		:																\
		: "r0","memory" /* clobber list */);							\
CYG_MACRO_END

#endif /* __HAL_SOC_H__ */
