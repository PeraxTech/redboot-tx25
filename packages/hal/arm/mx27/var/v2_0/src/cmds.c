//==========================================================================
//
//      cmds.c
//
//      SoC [platform] specific RedBoot commands
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
#include <redboot.h>
#include <cyg/hal/hal_intr.h>
#include <cyg/hal/plf_mmap.h>
#include <cyg/hal/hal_soc.h>		 // Hardware definitions
#include <cyg/hal/hal_cache.h>

typedef unsigned long long	u64;
typedef unsigned int		u32;
typedef unsigned short		u16;
typedef unsigned char		u8;

#define SZ_DEC_1M		1000000
#define PLL_PD_MAX		16		//actual pd+1
#define PLL_MFI_MAX		15
#define PLL_MFI_MIN		6		// See TLSbo80174
#define PLL_MFD_MAX		1024	//actual mfd+1
#define PLL_MFN_MAX		1022
#define PLL_MFN_MAX_2	510
#define PRESC_MAX		8
#define IPG_DIV_MAX		2
#define AHB_DIV_MAX		16
#define ARM_DIV_MAX		4

#define CPLM_SETUP		0

#define PLL_FREQ_MAX	(2 * PLL_REF_CLK * PLL_MFI_MAX)
#define PLL_FREQ_MIN	((2 * PLL_REF_CLK * PLL_MFI_MIN) / PLL_PD_MAX)
#define AHB_CLK_MAX		133333333
#define IPG_CLK_MAX		(AHB_CLK_MAX / 2)
#define NFC_CLK_MAX		33333333

#define ERR_WRONG_CLK	-1
#define ERR_NO_MFI		-2
#define ERR_NO_MFN		-3
#define ERR_NO_PD		-4
#define ERR_NO_PRESC	-5

u32 pll_clock(enum plls pll);
u32 get_main_clock(enum main_clocks clk);
u32 get_peri_clock(enum peri_clocks clk);

static u32 pll_mfd_fixed;

static void clock_setup(int argc, char *argv[]);
static void clko(int argc, char *argv[]);
extern unsigned int g_clock_src;
extern unsigned int system_rev;
extern int sys_ver;

#define MXC_PERCLK_NUM  4

#ifdef CYGOPT_MX27_WORKAROUND_ENGcm11563
#define avoid_arm_src 1
#else
#define avoid_arm_src 0
#endif

RedBoot_cmd("clock",
			"Setup/Display clock (max AHB=133MHz, max IPG=66.5MHz)\nSyntax:",
			"[<core clock in MHz> [:<AHB-to-core divider>[:<IPG-to-AHB divider>]]]\n\n"
			"If a divider is zero or no divider is specified, the optimum divider values\n"
			"will be chosen. Examples:\n"
			"   [clock]         -> Show various clocks\n"
			"   [clock 266]     -> Core=266  AHB=133           IPG=66.5\n"
			"   [clock 350]     -> Core=350  AHB=117           IPG=58.5\n"
			"   [clock 266:4]   -> Core=266  AHB=66.5(Core/4)  IPG=66.5\n"
			"   [clock 266:4:2] -> Core=266  AHB=66.5(Core/4)  IPG=33.25(AHB/2)\n",
			clock_setup
	);

/*!
 * This is to calculate various parameters based on reference clock and
 * targeted clock based on the equation:
 *      t_clk = 2*ref_freq*(mfi + mfn/(mfd+1))/(pd+1)
 * This calculation is based on a fixed MFD value for simplicity.
 *
 * @param ref       reference clock freq
 * @param target    targeted clock in HZ
 * @param p_pd      calculated pd value (pd value from register + 1) upon return
 * @param p_mfi     calculated actual mfi value upon return
 * @param p_mfn     calculated actual mfn value upon return
 * @param p_mfd     fixed mfd value (mfd value from register + 1) upon return
 *
 * @return          0 if successful; non-zero otherwise.
 */
int calc_pll_params(u32 ref, u32 target, int *p_pd,
					int *p_mfi, int *p_mfn, int *p_mfd)
{
	int pd, mfi, mfn;
	u64 n_target = target, n_ref = ref;

	if (g_clock_src == FREQ_26MHZ) {
		pll_mfd_fixed = 26 * 16;
	} else if (g_clock_src == FREQ_27MHZ) {
		pll_mfd_fixed = 27 * 16;
	} else {
		pll_mfd_fixed = 512;
	}

	// Make sure targeted freq is in the valid range. Otherwise the
	// following calculation might be wrong!!!
	if (target < PLL_FREQ_MIN || target > PLL_FREQ_MAX) {
		return ERR_WRONG_CLK;
	}
	// Use n_target and n_ref to avoid overflow
	for (pd = 1; pd <= PLL_PD_MAX; pd++) {
		mfi = (n_target * pd) / (2 * n_ref);
		if (mfi > PLL_MFI_MAX) {
			return ERR_NO_MFI;
		} else if (mfi < PLL_MFI_MIN) {
			continue;
		}
		break;
	}
	// Now got pd and mfi already
	mfn = (((n_target * pd) / 2 - n_ref * mfi) * pll_mfd_fixed) / n_ref;
	// Check mfn within limit and mfn < denominator
	if (sys_ver == SOC_SILICONID_Rev1_0) {
		if (mfn < 0 || mfn > PLL_MFN_MAX || mfn >= pll_mfd_fixed) {
			return ERR_NO_MFN;
		}
	} else {
		if (mfn < -PLL_MFN_MAX_2 || mfn > PLL_MFN_MAX_2 || mfn >= pll_mfd_fixed) {
			return ERR_NO_MFN;
		}
	}

	if (pd > PLL_PD_MAX) {
		return ERR_NO_PD;
	}
	*p_pd = pd;
	*p_mfi = mfi;
	*p_mfn = mfn;
	*p_mfd = pll_mfd_fixed;
	return 0;
}

static u32 per_clk_old[MXC_PERCLK_NUM];

/*!
 * This function assumes the expected core clock has to be changed by
 * modifying the PLL. This is NOT true always but for most of the times,
 * it is. So it assumes the PLL output freq is the same as the expected
 * core clock (presc=1) unless the core clock is less than PLL_FREQ_MIN.
 * In the latter case, it will try to increase the presc value until
 * (presc*core_clk) is greater than PLL_FREQ_MIN. It then makes call to
 * calc_pll_params() and obtains the values of PD, MFI,MFN, MFD based
 * on the targeted PLL and reference input clock to the PLL. Lastly,
 * it sets the register based on these values along with the dividers.
 * Note 1) There is no value checking for the passed-in divider values
 *         so the caller has to make sure those values are sensible.
 *      2) Also adjust the NFC divider such that the NFC clock doesn't
 *         exceed NFC_CLK_MAX (which is 33MHz now).
 *      3) Added feature to maintain the perclock before and after the call.
 * !!!! 4) This function can't have printf in it since the serial i/f is
 *         stopped.
 *
 * @param ref       pll input reference clock (32KHz or 26MHz)
 * @param core_clk  core clock in Hz
 * @param ahb_div   ahb divider to divide the core clock to get ahb clock
 *                  (ahb_div - 1) needs to be set in the register
 * @param ipg_div   ipg divider to divide the ahb clock to get ipg clock
 *                  (ipg_div - 1) needs to be set in the register
 # @return          0 if successful; non-zero otherwise
 */
#define CMD_CLOCK_DEBUG
int configure_clock(u32 ref, u32 core_clk, u32 ahb_div, u32 ipg_div)
{
	u32 pll, presc = 1;
	int pd, mfi, mfn, mfd;
	u32 cscr, mpctl0;
	u32 pcdr0, nfc_div, hdiv, nfc_div_factor;
	u32 per_div[MXC_PERCLK_NUM];
	int ret, i, arm_src = 0;

	per_clk_old[0] = get_peri_clock(PER_CLK1);
	per_clk_old[1] = get_peri_clock(PER_CLK2);
	per_clk_old[2] = get_peri_clock(PER_CLK3);
	per_clk_old[3] = get_peri_clock(PER_CLK4);
	diag_printf("per1=%9u\n", per_clk_old[0]);
	diag_printf("per2=%9u\n", per_clk_old[1]);
	diag_printf("per3=%9u\n", per_clk_old[2]);
	diag_printf("per4=%9u\n", per_clk_old[3]);
	// assume pll default to core clock first
	if (sys_ver == SOC_SILICONID_Rev1_0) {
		pll = core_clk;
		nfc_div_factor = 1;
	} else {
		if (!avoid_arm_src && core_clk > 266 * SZ_DEC_1M) {
			pll = core_clk;
			arm_src = 1;
		} else {
			pll = core_clk * 3 / 2;
		}
		nfc_div_factor = ahb_div;
	}

	// when core_clk >= PLL_FREQ_MIN, the presc can be 1.
	// Otherwise, need to calculate presc value below and adjust the targeted pll
	if (pll < PLL_FREQ_MIN) {
		int presc_max;

		if (sys_ver == SOC_SILICONID_Rev1_0) {
			presc_max = PRESC_MAX;
		} else {
			presc_max = ARM_DIV_MAX;
		}

		for (presc = 1; presc <= presc_max; presc++) {
			if (pll * presc > PLL_FREQ_MIN) {
				break;
			}
		}
		if (presc == presc_max + 1) {
			diag_printf("can't make presc=%d\n", presc);
			return ERR_NO_PRESC;
		}
		if (sys_ver == SOC_SILICONID_Rev1_0) {
			pll = core_clk * presc;
		} else {
			pll = 3 * core_clk * presc / 2;
		}
	}
	// pll is now the targeted pll output. Use it along with ref input clock
	// to get pd, mfi, mfn, mfd
	if ((ret = calc_pll_params(ref, pll, &pd, &mfi, &mfn, &mfd)) != 0) {
#ifdef CMD_CLOCK_DEBUG
		diag_printf("can't find pll parameters: %d\n", ret);
#endif
		return ret;
	}
#ifdef CMD_CLOCK_DEBUG
	diag_printf("ref=%d, pll=%d, pd=%d, mfi=%d,mfn=%d, mfd=%d\n",
				ref, pll, pd, mfi, mfn, mfd);
#endif

	// blindly increase divider first to avoid too fast ahbclk and ipgclk
	// in case the core clock increases too much
	cscr = readl(SOC_CRM_CSCR);
	if (sys_ver == SOC_SILICONID_Rev1_0) {
		hdiv = (pll + AHB_CLK_MAX - 1) / AHB_CLK_MAX;
		cscr = (cscr & ~0x0000FF00) | ((hdiv - 1) << 9) | (1 << 8);
	} else {
		if (core_clk > 266 * SZ_DEC_1M) {
			hdiv = (pll + AHB_CLK_MAX - 1) / AHB_CLK_MAX;
		} else {
			hdiv = (2 * pll + 3 * AHB_CLK_MAX - 1) / (3 * AHB_CLK_MAX);
		}
		cscr = (cscr & ~0x0000FF00) | ((hdiv - 1) << 8);
	}
	writel(cscr, SOC_CRM_CSCR);

	// update PLL register
	if (!((mfd < 10 * mfn) && (10 * mfn < 9 * mfd)))
		writel(1 << 6, SOC_CRM_MPCTL1);

	mpctl0 = readl(SOC_CRM_MPCTL0);
	mpctl0 = (mpctl0 & 0xC000C000)  |
		CPLM_SETUP			   |
		((pd - 1) << 26)	   |
		((mfd - 1) << 16)	   |
		(mfi << 10)			   |
		mfn;
	writel(mpctl0, SOC_CRM_MPCTL0);

	// restart mpll
	writel((cscr | (1 << 18)), SOC_CRM_CSCR);
	// check the LF bit to insure lock
	while ((readl(SOC_CRM_MPCTL1) & (1 << 15)) == 0);
	// have to add some delay for new values to take effect
	for (i = 0; i < 100000; i++);

	// PLL locked already so use the new divider values
	cscr = readl(SOC_CRM_CSCR);
	cscr &= ~0x0000FF00;

	if (sys_ver == SOC_SILICONID_Rev1_0) {
		cscr |= ((presc - 1) << 13) | ((ahb_div - 1) << 9) | ((ipg_div - 1) << 8);
	} else {
		cscr |= (arm_src << 15) | ((presc - 1) << 12) | ((ahb_div - 1) << 8);
	}
	writel(cscr, SOC_CRM_CSCR);

	// Make sure optimal NFC clock but less than NFC_CLK_MAX
	for (nfc_div = 1; nfc_div <= 16; nfc_div++) {
		if ((core_clk / (nfc_div_factor * nfc_div)) <= NFC_CLK_MAX) {
			break;
		}
	}
	pcdr0 = readl(SOC_CRM_PCDR0);
	if (sys_ver == SOC_SILICONID_Rev1_0) {
		writel(((pcdr0 & 0xFFFF0FFF) | ((nfc_div - 1) << 12)),
			SOC_CRM_PCDR0);
	} else {
		writel(((pcdr0 & 0xFFFFF3CF) | ((nfc_div - 1) << 6)),
			SOC_CRM_PCDR0);
	}

	if (sys_ver == SOC_SILICONID_Rev1_0) {
		pll = pll_clock(MCU_PLL) + 500000;
	} else {
		if (core_clk > (266 * SZ_DEC_1M)) {
			pll = pll_clock(MCU_PLL) + 500000;
		} else {
			pll = 2 * pll_clock(MCU_PLL) / 3 + 500000;
		}
	}
	for (i = 0; i < MXC_PERCLK_NUM; i++) {
		per_div[i] = (pll / per_clk_old[i]) - 1;
	}
	writel((per_div[3] << 24) | (per_div[2] << 16) | (per_div[1] << 8) |
		(per_div[0]), SOC_CRM_PCDR1);

	return 0;
}

static void clock_setup(int argc, char *argv[])
{
	u32 i, core_clk, ipg_div, data[3], ahb_div, ahb_clk, ahb_clk_in, ipg_clk;
	u32 presc_max,  ahb_div_max, pll;
	unsigned long temp;
	int ret;

	if (argc == 1)
		goto print_clock;
	if (g_clock_src == FREQ_27MHZ) {
		diag_printf("Error: clock setup is not supported for 27MHz source\n");
		return;
	}
	for (i = 0;  i < 3;  i++) {
		if (!parse_num(argv[1], &temp, &argv[1], ":")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}
		data[i] = temp;
	}

	core_clk = data[0] * SZ_DEC_1M;
	ahb_div = data[1];  // actual register field + 1
	ipg_div = data[2];  // actual register field + 1

	if (sys_ver == SOC_SILICONID_Rev1_0) {
		presc_max = PRESC_MAX;
		ahb_div_max = AHB_DIV_MAX;
		pll = core_clk;
		ahb_clk_in = core_clk;
	} else {
		presc_max = ARM_DIV_MAX;
		ahb_div_max = AHB_DIV_MAX / ARM_DIV_MAX;
		if (core_clk > (266 * SZ_DEC_1M)) {
			pll = core_clk;
			ahb_clk_in = core_clk * 2 / 3;
		} else {
			pll = 3 * core_clk / 2;
			ahb_clk_in = core_clk;
		}
		ipg_div = 2;
	}

	if (pll < (PLL_FREQ_MIN / presc_max) || pll > PLL_FREQ_MAX) {
		diag_printf("Targeted core clock should be within [%d - %d]\n",
					PLL_FREQ_MIN / presc_max, PLL_FREQ_MAX);
		return;
	}

	// find the ahb divider
	if (ahb_div > ahb_div_max) {
		diag_printf("Invalid AHB divider: %d. Maximum value is %d\n",
					ahb_div, ahb_div_max);
		return;
	}
	if (ahb_div == 0) {
		// no AHBCLK divider specified
		for (ahb_div = 1; ; ahb_div++) {
			if ((ahb_clk_in / ahb_div) <= AHB_CLK_MAX) {
				break;
			}
		}
	}
	if (ahb_div > ahb_div_max || (ahb_clk_in / ahb_div) > AHB_CLK_MAX) {
		diag_printf("Can't make AHB=%d since max=%d\n",
					core_clk / ahb_div, AHB_CLK_MAX);
		return;
	}

	// find the ipg divider
	ahb_clk = ahb_clk_in / ahb_div;
	if (ipg_div > IPG_DIV_MAX) {
		diag_printf("Invalid IPG divider: %d. Maximum value is %d\n",
					ipg_div, IPG_DIV_MAX);
		return;
	}
	if (ipg_div == 0) {
		ipg_div++;			// At least =1
		if (ahb_clk > IPG_CLK_MAX)
			ipg_div++;		// Make it =2
	}
	if (ipg_div > IPG_DIV_MAX || (ahb_clk / ipg_div) > IPG_CLK_MAX) {
		diag_printf("Can't make IPG=%d since max=%d\n",
					(ahb_clk / ipg_div), IPG_CLK_MAX);
		return;
	}
	ipg_clk = ahb_clk / ipg_div;

	diag_printf("Trying to set core=%d ahb=%d ipg=%d...\n",
				core_clk, ahb_clk, ipg_clk);

	// stop the serial to be ready to adjust the clock
	hal_delay_us(100000);
	cyg_hal_plf_serial_stop();
	// adjust the clock
	ret = configure_clock(PLL_REF_CLK, core_clk, ahb_div, ipg_div);
	// restart the serial driver
	cyg_hal_plf_serial_init();
	hal_delay_us(100000);

	if (ret != 0) {
		diag_printf("Failed to setup clock: %d\n", ret);
		return;
	}

	// check for new per clock settings and warn user if there is a change.
	if (per_clk_old[0] != get_peri_clock(PER_CLK1)) {
		diag_printf("per_clk1 changed; old clock was: %u\n", per_clk_old[0]);
	}
	if (per_clk_old[1] != get_peri_clock(PER_CLK2)) {
		diag_printf("per_clk2 changed; old clock was: %u\n", per_clk_old[1]);
	}
	if (per_clk_old[2] != get_peri_clock(PER_CLK3)) {
		diag_printf("per_clk3 changed; old clock was: %u\n", per_clk_old[2]);
	}
	if (per_clk_old[3] != get_peri_clock(PER_CLK4)) {
		diag_printf("per_clk4 changed; old clock was: %u\n", per_clk_old[3]);
	}

	diag_printf("\n<<<New clock setting>>>\n");

	// Now printing clocks
print_clock:
	diag_printf("\nMPLL\t\tSPLL\n");
	diag_printf("=========================\n");
	diag_printf("%-16d%-16d\n\n", pll_clock(MCU_PLL), pll_clock(SER_PLL));
	diag_printf("CPU\t\tAHB\t\tIPG\t\tNFC\t\tUSB\n");
	diag_printf("========================================================================\n");
	diag_printf("%-16d%-16d%-16d%-16d%-16d\n\n",
				get_main_clock(CPU_CLK),
				get_main_clock(AHB_CLK),
				get_main_clock(IPG_CLK),
				get_main_clock(NFC_CLK),
				get_main_clock(USB_CLK));

	diag_printf("PER1\t\tPER2\t\tPER3\t\tPER4\n");
	diag_printf("===========================================");
	diag_printf("=============\n");

	diag_printf("%-16d%-16d%-16d%-16d\n\n",
				get_peri_clock(PER_CLK1),
				get_peri_clock(PER_CLK2),
				get_peri_clock(PER_CLK3),
				get_peri_clock(PER_CLK4));

	diag_printf("H264\t\tMSHC\t\tSSI1\t\tSSI2\n");
	diag_printf("========================================================\n");
	diag_printf("%-16d%-16d%-16d%-16d\n\n",
				get_peri_clock(H264_BAUD),
				get_peri_clock(MSHC_BAUD),
				get_peri_clock(SSI1_BAUD),
				get_peri_clock(SSI2_BAUD));
	diag_printf("PERCLK: 1-<UART|GPT|PWM> 2-<SDHC|CSPI> 3-<LCDC> 4-<CSI>\n");
}

/*!
 * This function returns the PLL output value in Hz based on pll.
 */
u32 pll_clock(enum plls pll)
{
	int mfi, mfn, mfd, pdf;
	u32 pll_out;
	u32 reg = readl(pll);
	u64 ref_clk;

	if ((pll == SER_PLL) && (sys_ver == SOC_SILICONID_Rev2_0)) {
		writel(reg, pll);
	}
	pdf = (reg >> 26) & 0xF;
	mfd = (reg >> 16) & 0x3FF;
	mfi = (reg >> 10) & 0xF;
	if (mfi < 5) {
		mfi = 5;
	}
	mfn = reg & 0x3FF;
	if (mfn >= 512) {
		mfn = 1024 - mfn;
	}
	ref_clk = g_clock_src;

	pll_out = (2 * ref_clk * mfi + ((2 * ref_clk * mfn) / (mfd + 1))) /
		(pdf + 1);

	return pll_out;
}

/*!
 * This function returns the main clock value in Hz.
 */
u32 get_main_clock(enum main_clocks clk)
{
	u32 presc, ahb_div, ipg_pdf, nfc_div;
	u32 ret_val = 0, usb_div;
	u32 cscr = readl(SOC_CRM_CSCR);
	u32 pcdr0 = readl(SOC_CRM_PCDR0);

	if (sys_ver == SOC_SILICONID_Rev1_0) {
		presc = ((cscr >> CRM_CSCR_PRESC_OFFSET) & 0x7) + 1;
	} else {
		presc = ((cscr >> CRM_CSCR_ARM_OFFSET) & 0x3) + 1;
	}

	switch (clk) {
	case CPU_CLK:
		if ((sys_ver == SOC_SILICONID_Rev1_0) || (cscr & CRM_CSCR_ARM_SRC)) {
			ret_val = pll_clock(MCU_PLL) / presc;
		} else {
			ret_val = 2 * pll_clock(MCU_PLL) / (3 * presc);
		}
		break;
	case AHB_CLK:
		if (sys_ver == SOC_SILICONID_Rev1_0) {
			ahb_div = ((cscr >> CRM_CSCR_BCLKDIV_OFFSET) & 0xF) + 1;
			ret_val = pll_clock(MCU_PLL) / (presc * ahb_div);
		} else {
			ahb_div = ((cscr >> CRM_CSCR_AHB_OFFSET) & 0x3) + 1;
			ret_val = 2 * pll_clock(MCU_PLL) / (3 * ahb_div);
		}
		break;
	case IPG_CLK:
		if (sys_ver == SOC_SILICONID_Rev1_0) {
			ahb_div = ((cscr >> CRM_CSCR_BCLKDIV_OFFSET) & 0xF) + 1;
			ipg_pdf = ((cscr >> CRM_CSCR_IPDIV_OFFSET) & 0x1) + 1;
			ret_val = pll_clock(MCU_PLL) / (presc * ahb_div * ipg_pdf);
		} else {
			ahb_div = ((cscr >> CRM_CSCR_AHB_OFFSET) & 0x3) + 1;
			ret_val = pll_clock(MCU_PLL) / (3 * ahb_div);
		}
		break;
	case NFC_CLK:
		if (sys_ver == SOC_SILICONID_Rev1_0) {
			nfc_div = ((pcdr0 >> 12) & 0xF) + 1;
			/* AHB/nfc_div */
			ret_val = pll_clock(MCU_PLL) / (presc * nfc_div);
		} else {
			nfc_div = ((pcdr0 >> 6) & 0xF) + 1;
			ahb_div = ((cscr >> CRM_CSCR_AHB_OFFSET) & 0x3) + 1;
			ret_val = 2*pll_clock(MCU_PLL) / (3 * ahb_div * nfc_div);
		}
		break;
	case USB_CLK:
		usb_div = ((cscr >> CRM_CSCR_USB_DIV_OFFSET) & 0x7) + 1;
		ret_val = pll_clock(SER_PLL) / usb_div;
		break;
	default:
		diag_printf("Unknown clock: %d\n", clk);
		break;
	}
	return ret_val;
}

/*!
 * This function returns the peripheral clock value in Hz.
 */
u32 get_peri_clock(enum peri_clocks clk)
{
	u32 ret_val = 0, div;
	u32 pcdr0 = readl(SOC_CRM_PCDR0);
	u32 pcdr1 = readl(SOC_CRM_PCDR1);
	u32 cscr = readl(SOC_CRM_CSCR);

	switch (clk) {
	case PER_CLK1:
		div = (pcdr1 & 0x3F) + 1;
		if (sys_ver == SOC_SILICONID_Rev1_0) {
			ret_val = pll_clock(MCU_PLL) / div;
		} else {
			ret_val = 2 * pll_clock(MCU_PLL) / (3 * div);
		}
		break;
	case PER_CLK2:
	case SPI1_CLK:
	case SPI2_CLK:
		div = ((pcdr1 >> 8) & 0x3F) + 1;
		if (sys_ver == SOC_SILICONID_Rev1_0) {
			ret_val = pll_clock(MCU_PLL) / div;
		} else {
			ret_val = 2 * pll_clock(MCU_PLL) / (3 * div);
		}
		break;
	case PER_CLK3:
		div = ((pcdr1 >> 16) & 0x3F) + 1;
		if (sys_ver == SOC_SILICONID_Rev1_0) {
			ret_val = pll_clock(MCU_PLL) / div;
		} else {
			ret_val = 2 * pll_clock(MCU_PLL) / (3 * div);
		}
		break;
	case PER_CLK4:
		div = ((pcdr1 >> 24) & 0x3F) + 1;
		if (sys_ver == SOC_SILICONID_Rev1_0) {
			ret_val = pll_clock(MCU_PLL) / div;
		} else {
			ret_val = 2 * pll_clock(MCU_PLL) / (3 * div);
		}
		break;
	case SSI1_BAUD:
		div = (pcdr0 >> 16) & 0x3F;
		if (sys_ver == SOC_SILICONID_Rev1_0) {
			if (div < 2) {
				div = 62 * 2;
			}
		} else {
			div += 4;
		}
		if ((cscr & (1 << 22)) != 0) {
			// This takes care of 0.5*SSIDIV[0] by x2
			if (sys_ver == SOC_SILICONID_Rev1_0) {
				ret_val = (2 * pll_clock(MCU_PLL)) / div;
			} else {
				ret_val = (4 * pll_clock(MCU_PLL)) / (3 * div);
			}
		} else {
			ret_val = (2 * pll_clock(SER_PLL)) / div;
		}
		break;
	case SSI2_BAUD:
		div = (pcdr0 >> 26) & 0x3F;
		if (sys_ver == SOC_SILICONID_Rev1_0) {
			if (div < 2) {
				div = 62 * 2;
			}
		} else {
			div += 4;
		}
		if ((cscr & (1 << 23)) != 0) {
			if (sys_ver == SOC_SILICONID_Rev1_0) {
				ret_val = (2 * pll_clock(MCU_PLL)) / div;
			} else {
				ret_val = (4 * pll_clock(MCU_PLL)) / (3 * div);
			}
		} else {
			ret_val = (2 * pll_clock(SER_PLL)) / div;
		}
		break;
	case H264_BAUD:
		if (sys_ver == SOC_SILICONID_Rev1_0) {
			div = (pcdr0 >> 8) & 0xF;
			if (div < 2) {
				div = 62 * 2;
			}
		} else {
			div = (pcdr0 >> 10) & 0x3F;
			div += 4;
		}
		if ((cscr & (1 << 21)) != 0) {
			if (sys_ver == SOC_SILICONID_Rev1_0) {
				ret_val = (2 * pll_clock(MCU_PLL)) / div;
			} else {
				ret_val = (4 * pll_clock(MCU_PLL)) / (3 * div);
			}
		} else {
			ret_val = (2 * pll_clock(SER_PLL)) / div;
		}
		break;
	case MSHC_BAUD:
		if ((cscr & (1 << 20)) != 0) {
			if (sys_ver == SOC_SILICONID_Rev1_0) {
				div = (pcdr0 & 0x1F) + 1;
				ret_val = pll_clock(MCU_PLL) / div;
			} else {
				div = (pcdr0 & 0x3F) + 1;
				ret_val = 2 * pll_clock(MCU_PLL) / (3 * div);
			}
		} else {
			div = (pcdr0 & 0x1F) + 1;
			ret_val = (2 * pll_clock(SER_PLL)) / div;
		}
		break;
	default:
		diag_printf("%s(): This clock: %d not supported yet\n",
					__FUNCTION__, clk);
		break;
	}

	return ret_val;
}

RedBoot_cmd("clko",
			"Select clock source for CLKO (TP1 on EVB or S3 Pin 1)",
			" The output clock is the actual clock source freq divided by 8. Default is FCLK\n"
			"         Note that the module clock will be turned on for reading!\n"
			"          <0> - display current clko selection\n"
			"          <1> - CLK32\n"
			"          <2> - PREMCLK\n"
			"          <3> - CLK26M (may see nothing if 26MHz Crystal is not connected)\n"
			"          <4> - MPLL Reference CLK\n"
			"          <5> - SPLL Reference CLK\n"
			"          <6> - MPLL CLK\n"
			"          <7> - SPLL CLK\n"
			"          <8> - FCLK\n"
			"          <9> - AHBCLK\n"
			"          <10> - IPG_CLK (PERCLK)\n"
			"          <11> - PERCLK1\n"
			"          <12> - PERCLK2\n"
			"          <13> - PERCLK3\n"
			"          <14> - PERCLK4\n"
			"          <15> - SSI 1 Baud\n"
			"          <16> - SSI 2 Baud\n"
			"          <17> - NFC\n"
			"          <18> - MSHC Baud\n"
			"          <19> - H264 Baud\n"
			"          <20> - CLK60M Always\n"
			"          <21> - CLK32K Always\n"
			"          <22> - CLK60M\n"
			"          <23> - DPTC Ref",
			clko
	);

static u8* clko_name[] = {
	"NULL",
	"CLK32",
	"PREMCLK",
	"CLK26M (may see nothing if 26MHz Crystal is not connected)",
	"MPLL Reference CLK",
	"SPLL Reference CLK",
	"MPLL CLK",
	"SPLL CLK",
	"FCLK",
	"AHBCLK",
	"IPG_CLK (PERCLK)",
	"PERCLK1",
	"PERCLK2",
	"PERCLK3",
	"PERCLK4",
	"SSI 1 Baud",
	"SSI 2 Baud",
	"NFC",
	"MSHC Baud",
	"H264 Baud",
	"CLK60M Always",
	"CLK32K Always",
	"CLK60M",
	"DPTC Ref",
};

#define CLKO_MAX_INDEX			(sizeof(clko_name) / sizeof(u8*))

static void clko(int argc,char *argv[])
{
	u32 action = 0, ccsr;

	if (!scan_opts(argc, argv, 1, 0, 0, &action,
					OPTION_ARG_TYPE_NUM, "action"))
		return;

	if (action >= CLKO_MAX_INDEX) {
		diag_printf("%d is not supported\n", action);
		return;
	}

	ccsr = readl(SOC_CRM_CCSR);

	if (action != 0) {
		ccsr = (ccsr & (~0x1F)) + action - 1;
		writel(ccsr, SOC_CRM_CCSR);
		diag_printf("Set clko to ");
	}

	ccsr = readl(SOC_CRM_CCSR);
	diag_printf("%s\n", clko_name[(ccsr & 0x1F) + 1]);
	diag_printf("CCSR register[0x%08lx] = 0x%08x\n", SOC_CRM_CCSR, ccsr);
}

extern int flash_program(void *_addr, void *_data, int len, void **err_addr);
extern int flash_erase(void *addr, int len, void **err_addr);

void auto_flash_start(void)
{
	void *err_addr;
	int stat;
	int nor_update = 1; //todo: need to support NAND
	u32 src = readl(SERIAL_DOWNLOAD_SRC_REG);
	u32 dst = readl(SERIAL_DOWNLOAD_TGT_REG);
	u32 sz = readl(SERIAL_DOWNLOAD_SZ_REG);

	if (readl(SERIAL_DOWNLOAD_MAGIC_REG) != SERIAL_DOWNLOAD_MAGIC) {
		return;
	}

	if (nor_update) {
		// Erase area to be programmed
		if ((stat = flash_erase((void *)dst, sz, &err_addr)) != 0) {
			diag_printf("BEADDEAD\n");
			return;
		}
		diag_printf("BEADBEEF\n");
		// Now program it
		if ((stat = flash_program((void *)dst, (void *)src, sz,
										&err_addr)) != 0) {
			diag_printf("BEADFEEF\n");
		}
	}
	diag_printf("BEADCEEF\n");
}

RedBoot_init(auto_flash_start, RedBoot_INIT_LAST);

#define IIM_ERR_SHIFT		8
#define POLL_FUSE_PRGD		(IIM_STAT_PRGD | (IIM_ERR_PRGE << IIM_ERR_SHIFT))
#define POLL_FUSE_SNSD		(IIM_STAT_SNSD | (IIM_ERR_SNSE << IIM_ERR_SHIFT))

static void fuse_op_start(void)
{
	/* Do not generate interrupt */
	writel(0, IIM_BASE_ADDR + IIM_STATM_OFF);
	// clear the status bits and error bits
	writel(0x3, IIM_BASE_ADDR + IIM_STAT_OFF);
	writel(0xFE, IIM_BASE_ADDR + IIM_ERR_OFF);
}

/*
 * The action should be either:
 *          POLL_FUSE_PRGD
 * or:
 *          POLL_FUSE_SNSD
 */
static int poll_fuse_op_done(int action)
{
	u32 status, error;

	if (action != POLL_FUSE_PRGD && action != POLL_FUSE_SNSD) {
		diag_printf("%s(%d) invalid operation\n", __FUNCTION__, action);
		return -1;
	}

	/* Poll busy bit till it is NOT set */
	while ((readl(IIM_BASE_ADDR + IIM_STAT_OFF) & IIM_STAT_BUSY) != 0 ) {
	}

	/* Test for successful write */
	status = readl(IIM_BASE_ADDR + IIM_STAT_OFF);
	error = readl(IIM_BASE_ADDR + IIM_ERR_OFF);

	if ((status & action) != 0 && (error & (action >> IIM_ERR_SHIFT)) == 0) {
		if (error) {
			diag_printf("Even though the operation seems successful...\n");
			diag_printf("There are some error(s) at addr=0x%08lx: 0x%08x\n",
						(IIM_BASE_ADDR + IIM_ERR_OFF), error);
		}
		return 0;
	}
	diag_printf("%s(%d) failed\n", __FUNCTION__, action);
	diag_printf("status address=0x%08lx, value=0x%08x\n",
				(IIM_BASE_ADDR + IIM_STAT_OFF), status);
	diag_printf("There are some error(s) at addr=0x%08lx: 0x%08x\n",
				(IIM_BASE_ADDR + IIM_ERR_OFF), error);
	return -1;
}

static void sense_fuse(int bank, int row, int bit)
{
	int ret;
	int addr, addr_l, addr_h, reg_addr;

	fuse_op_start();

	addr = ((bank << 11) | (row << 3) | (bit & 0x7));
	/* Set IIM Program Upper Address */
	addr_h = (addr >> 8) & 0x000000FF;
	/* Set IIM Program Lower Address */
	addr_l = (addr & 0x000000FF);

#ifdef IIM_FUSE_DEBUG
	diag_printf("%s: addr_h=0x%02x, addr_l=0x%02x\n",
				__FUNCTION__, addr_h, addr_l);
#endif
	writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
	writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
	/* Start sensing */
	writel(0x8, IIM_BASE_ADDR + IIM_FCTL_OFF);
	if ((ret = poll_fuse_op_done(POLL_FUSE_SNSD)) != 0) {
		diag_printf("%s(bank: %d, row: %d, bit: %d failed\n",
					__FUNCTION__, bank, row, bit);
	}
	reg_addr = IIM_BASE_ADDR + IIM_SDAT_OFF;
	if (ret == 0)
		diag_printf("fuses at (bank:%d, row:%d) = 0x%02x\n", bank, row, readl(reg_addr));
}

void do_fuse_read(int argc, char *argv[])
{
	unsigned long bank, row;

	if (argc == 1) {
		diag_printf("Usage: fuse_read <bank> <row>\n");
		return;
	} else if (argc == 3) {
		if (!parse_num(argv[1], &bank, &argv[1], " ")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}
		if (!parse_num(argv[2], &row, &argv[2], " ")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}

		diag_printf("Read fuse at bank:%ld row:%ld\n", bank, row);
		sense_fuse(bank, row, 0);

	} else {
		diag_printf("Passing in wrong arguments: %d\n", argc);
		diag_printf("Usage: fuse_read <bank> <row>\n");
	}
}

/* Blow fuses based on the bank, row and bit positions (all 0-based)
*/
int fuse_blow(int bank, int row, int bit)
{
	int addr, addr_l, addr_h, ret = -1;

	fuse_op_start();

	/* Disable IIM Program Protect */
	writel(0xAA, IIM_BASE_ADDR + IIM_PREG_P_OFF);

	addr = ((bank << 11) | (row << 3) | (bit & 0x7));
	/* Set IIM Program Upper Address */
	addr_h = (addr >> 8) & 0x000000FF;
	/* Set IIM Program Lower Address */
	addr_l = (addr & 0x000000FF);

	diag_printf("blowing fuse bank %d row %d bit %d\n", bank, row, bit & 7);
#ifdef IIM_FUSE_DEBUG
	diag_printf("blowing addr_h=0x%02x, addr_l=0x%02x\n", addr_h, addr_l);
#endif

	writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
	writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
	/* Start Programming */
	writel(0x71, IIM_BASE_ADDR + IIM_FCTL_OFF);
	if (poll_fuse_op_done(POLL_FUSE_PRGD) == 0) {
		ret = 0;
	}

	/* Enable IIM Program Protect */
	writel(0x0, IIM_BASE_ADDR + IIM_PREG_P_OFF);
	return ret;
}

/*
 * This command is added for burning IIM fuses
 */
RedBoot_cmd("fuse_read",
			"read some fuses",
			"<bank> <row>",
			do_fuse_read
	);

RedBoot_cmd("fuse_blow",
			"blow some fuses",
			"<bank> <row> <value>",
			do_fuse_blow
	);

#define			INIT_STRING				 "12345678"
static char ready_to_blow[] = INIT_STRING;

void do_fuse_blow(int argc, char *argv[])
{
	unsigned long bank, row, value;
	int i;

	if (argc == 1) {
		diag_printf("It is too dangeous for you to use this command.\n");
		return;
	} else if (argc == 2) {
		if (strcasecmp(argv[1], "nandboot") == 0) {
			diag_printf("%s\n", "fuse blown not needed");
		}
		return;
	} else if (argc == 3) {
		if (strcasecmp(argv[1], "nandboot") == 0) {
#if defined(CYGPKG_HAL_ARM_MXC91131) || defined(CYGPKG_HAL_ARM_MX21) || defined(CYGPKG_HAL_ARM_MX27) || defined(CYGPKG_HAL_ARM_MX31)
			diag_printf("No need to blow any fuses for NAND boot on this platform\n");
#else
			diag_printf("Ready to burn NAND boot fuses\n");
			if (fuse_blow(0, 16, 1) != 0 || fuse_blow(0, 16, 7) != 0) {
				diag_printf("NAND BOOT fuse blown failed miserably ...\n");
			} else {
				diag_printf("NAND BOOT fuse blown successfully ...\n");
			}
		} else {
			diag_printf("Not ready: %s, %s\n", argv[1], argv[2]);
#endif
		}
	} else if (argc == 4) {
		if (!parse_num(argv[1], &bank, &argv[1], " ")) {
			diag_printf("Error: Invalid fuse bank\n");
			return;
		}
		if (!parse_num(argv[2], &row, &argv[2], " ")) {
			diag_printf("Error: Invalid fuse row\n");
			return;
		}
		if (!parse_num(argv[3], &value, &argv[3], " ")) {
			diag_printf("Error: Invalid value\n");
			return;
		}

		if (!verify_action("Confirm to blow fuse at bank:%ld row:%ld value:0x%02lx (%ld)",
							bank, row, value)) {
			diag_printf("fuse_blow canceled\n");
			return;
		}

		for (i = 0; i < 8; i++) {
			if (((value >> i) & 0x1) == 0) {
				continue;
			}
			if (fuse_blow(bank, row, i) != 0) {
				diag_printf("fuse_blow(bank: %ld, row: %ld, bit: %d failed\n",
							bank, row, i);
			} else {
				diag_printf("fuse_blow(bank: %ld, row: %ld, bit: %d successful\n",
							bank, row, i);
			}
		}
		sense_fuse(bank, row, 0);
	} else {
		diag_printf("Passing in wrong arguments: %d\n", argc);
	}
	/* Reset to default string */
	strcpy(ready_to_blow, INIT_STRING);
}

/* precondition: m>0 and n>0.  Let g=gcd(m,n). */
int gcd(int m, int n)
{
	int t;
	while (m > 0) {
		if (n > m) {t = m; m = n; n = t;} /* swap */
		m -= n;
	}
	return n;
}

#define CLOCK_SRC_DETECT_MS         100
#define CLOCK_IPG_DEFAULT           66500000
#define CLOCK_SRC_DETECT_MARGIN     500000
void mxc_show_clk_input(void)
{
#if 0
	u32 c1, c2, diff, ipg_real, num = 0;
	u32 prcs = (readl(CCM_BASE_ADDR + CLKCTL_CCMR) >> 1) & 0x3;

	return;  // FIXME

	switch (prcs) {
	case 0x01:
		diag_printf("FPM enabled --> 32KHz input source\n");
		return;
	case 0x02:
		break;
	default:
		diag_printf("Error %d: unknown clock source %d\n", __LINE__, prcs);
		return;
	}

	// enable GPT with IPG clock input
	writel(0x241, GPT_BASE_ADDR + GPTCR);
	// prescaler = 1
	writel(0, GPT_BASE_ADDR + GPTPR);

	c1 = readl(GPT_BASE_ADDR + GPTCNT);
	// use 32KHz input clock to get the delay
	hal_delay_us(CLOCK_SRC_DETECT_MS * 1000);
	c2 = readl(GPT_BASE_ADDR + GPTCNT);
	diff = (c2 > c1) ? (c2 - c1) : (0xFFFFFFFF - c1 + c2);

	ipg_real = diff * (1000 / CLOCK_SRC_DETECT_MS);

	if (ipg_real > (CLOCK_IPG_DEFAULT + CLOCK_SRC_DETECT_MARGIN)) {
		if (g_clock_src != FREQ_27MHZ)
			num = 27;
	} else if (ipg_real < (CLOCK_IPG_DEFAULT - CLOCK_SRC_DETECT_MARGIN)) {
		if (g_clock_src != FREQ_26MHZ)
			num = 26;
	}
	if (num != 0) {
		diag_printf("Error: Actual clock input is %d MHz\n", num);
		diag_printf("       ipg_real=%d CLOCK_IPG_DEFAULT - CLOCK_SRC_DETECT_MARGIN=%d\n",
					ipg_real, CLOCK_IPG_DEFAULT - CLOCK_SRC_DETECT_MARGIN);
		diag_printf("       But clock source defined to be %d\n", g_clock_src);
		hal_delay_us(2000000);
	} else {
		diag_printf("ipg_real=%d CLOCK_IPG_DEFAULT - CLOCK_SRC_DETECT_MARGIN=%d\n",
					ipg_real, CLOCK_IPG_DEFAULT - CLOCK_SRC_DETECT_MARGIN);
		diag_printf("clock source defined to be %d\n", g_clock_src);
	}
#endif
}

RedBoot_init(mxc_show_clk_input, RedBoot_INIT_LAST);

void clock_spi_enable(unsigned int spi_clk)
{
    unsigned int reg = readl(SOC_CRM_PCCR1);

    // turn on PERCLK2
    writel(reg | (1 << 9), SOC_CRM_PCCR1);

    reg = readl(SOC_CRM_PCCR0);

    if (spi_clk == SPI1_CLK) {
	writel(reg | (1 << 31), SOC_CRM_PCCR0);
	gpio_request_mux(MX27_PIN_CSPI1_MOSI, GPIO_MUX_PRIMARY);
	gpio_request_mux(MX27_PIN_CSPI1_MISO, GPIO_MUX_PRIMARY);
	gpio_request_mux(MX27_PIN_CSPI1_SCLK, GPIO_MUX_PRIMARY);
	gpio_request_mux(MX27_PIN_CSPI1_RDY, GPIO_MUX_PRIMARY);
	gpio_request_mux(MX27_PIN_CSPI1_SS0, GPIO_MUX_PRIMARY);
	gpio_request_mux(MX27_PIN_CSPI1_SS1, GPIO_MUX_PRIMARY);
	gpio_request_mux(MX27_PIN_CSPI1_SS2, GPIO_MUX_PRIMARY);
    } else if (spi_clk == SPI2_CLK) {
	writel(reg | (1 << 30), SOC_CRM_PCCR0);
    }
}
