/*
 * Copyright 2006 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

/*!
 * This structure defines the offset of registers in gpio module.
 */
 
#include <redboot.h>
#include <cyg/hal/hal_intr.h>
#include <cyg/hal/plf_mmap.h>
#include <cyg/hal/hal_soc.h>         // Hardware definitions
#include <cyg/hal/hal_cache.h>

#undef MXC_MUX_DEBUG
//#define MXC_MUX_DEBUG

#ifdef MXC_MUX_DEBUG
#define diag_printf1    diag_printf
#else
#define diag_printf1(fmt,args...)
#endif

typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned char u8;

struct mxc_gpio_port {
	u32 num;		/*!< gpio port number */
	u32 base;		/*!< gpio port base VA */
	u16 irq;		/*!< irq number to the core */
	u16 virtual_irq_start;	/*!< virtual irq start number */
};

/*!
 * This enumeration data type defines the configuration for input mode.
 */
typedef enum {
	GPIO_INPUT_GPIO = 0x00,
	GPIO_INPUT_INTR = 0x01,
	GPIO_INPUT_LOW = 0x02,
	GPIO_INPUT_HIGH = 0x03
} gpio_input_cfg_t;

/*!
 * This enumeration data type defines the configuration for output mode.
 */
typedef enum {
	GPIO_OUTPUT_A = 0x00,
	GPIO_OUTPUT_B = 0x01,
	GPIO_OUTPUT_C = 0x02,
	GPIO_OUTPUT_DR = 0x03
} gpio_output_cfg_t;

struct gpio_port {
	u32 num;		/*!< gpio port number */
	u32 base;		/*!< gpio port base VA */
	u16 irq;		/*!< irq number to the core */
	u16 virtual_irq_start;	/*!< virtual irq start number */
	u32 reserved_map;	/*!< keep track of which pins are in use */
	u32 irq_is_level_map;	/*!< if a pin's irq is level sensitive. default is edge */
};

#define IO_ADDRESS(x)       x
#define GPIO_BASE_ADDR      SOC_GPIOA_BASE

struct mxc_gpio_port mxc_gpio_ports[GPIO_PORT_NUM] = {
	{
	 .num = 0,
	 .base = IO_ADDRESS(GPIO_BASE_ADDR),
	 },
	{
	 .num = 1,
	 .base = IO_ADDRESS(GPIO_BASE_ADDR) + 0x100,
	 },
	{
	 .num = 2,
	 .base = IO_ADDRESS(GPIO_BASE_ADDR) + 0x200,
	 },
	{
	 .num = 3,
	 .base = IO_ADDRESS(GPIO_BASE_ADDR) + 0x300,
	 },
	{
	 .num = 4,
	 .base = IO_ADDRESS(GPIO_BASE_ADDR) + 0x400,
	 },
	{
	 .num = 5,
	 .base = IO_ADDRESS(GPIO_BASE_ADDR) + 0x500,
	 },
};

static struct gpio_port gpio_port[GPIO_PORT_NUM];


static inline int _request_gpio(struct gpio_port *port, u32 index)
{
	if (port->reserved_map & (1 << index)) {
		diag_printf("GPIO port %d (0-based), pin %d is already reserved!\n",
		       port->num, index);
		return -1;
	}
	port->reserved_map |= (1 << index);
	return 0;
}

static inline struct gpio_port *get_gpio_port(u32 gpio)
{
	return &gpio_port[GPIO_TO_PORT(gpio)];
}

static int check_gpio(u32 gpio)
{
	if (gpio >= MXC_MAX_GPIO_LINES) {
		diag_printf("mxc-gpio: invalid GPIO %d\n", gpio);
		return -1;
	}
	return 0;
}

/*!
 * Request ownership for a GPIO pin. The caller has to check the return value
 * of this function to make sure it returns 0 before make use of that pin.
 * @param pin		a name defined by \b iomux_pin_name_t
 * @return		0 if successful; Non-zero otherwise
 */
int mxc_request_gpio(iomux_pin_name_t pin)
{
	struct gpio_port *port;
	u32 index, gpio = IOMUX_TO_GPIO(pin);

	if (check_gpio(gpio) < 0)
		return -1;

	port = get_gpio_port(gpio);
	index = GPIO_TO_INDEX(gpio);

	return _request_gpio(port, index);
}

/*!
 * This function enable or disable the pullup feature to the pin. 
 * @param port  	a pointer of gpio port
 * @param index 	the index of the  pin in the port
 * @param en		0 if disable pullup, otherwise enable it.
 * @return		none
 */
static inline void _gpio_set_puen(struct mxc_gpio_port *port, u32 index,
				  bool en)
{
	u32 reg;

	reg = readl(port->base + GPIO_PUEN);
	if (en) {
		reg |= 1 << index;
	} else {
		reg &= ~(1 << index);
	}
	writel(reg, port->base + GPIO_PUEN);
}

/*!
 * This function set the input configuration A. 
 * @param port  	a pointer of gpio port
 * @param index 	the index of the  pin in the port
 * @param config	a mode as define in \b #gpio_input_cfg_t
 * @return		none
 */
static inline void _gpio_set_iconfa(struct mxc_gpio_port *port, u32 index,
				    gpio_input_cfg_t config)
{
	u32 reg, val;
	u32 mask;

	mask = 0x3 << ((index % 16) << 1);

	if (index >= 16) {
		reg = port->base + GPIO_ICONFA2;
		val = config << ((index - 16) * 2);
	} else {
		reg = port->base + GPIO_ICONFA1;
		val = config << (index * 2);
	}
	val |= readl(reg) & ~(mask);
	writel(val, reg);
}

/*!
 * This function set the input configuration B. 
 * @param port  	a pointer of gpio port
 * @param index 	the index of the  pin in the port
 * @param config	a mode as define in \b #gpio_input_cfg_t
 * @return		none
 */
static inline void _gpio_set_iconfb(struct mxc_gpio_port *port, u32 index,
				    gpio_input_cfg_t config)
{
	u32 reg, val;
	u32 mask;

	mask = 0x3 << ((index % 16) << 1);

	if (index >= 16) {
		reg = port->base + GPIO_ICONFB2;
		val = config << ((index - 16) * 2);
	} else {
		reg = port->base + GPIO_ICONFB1;
		val = config << (index * 2);
	}
	val |= readl(reg) & (~mask);
	writel(val, reg);
}

/*!
 * This function set the output configuration. 
 * @param port  	a pointer of gpio port
 * @param index 	the index of the  pin in the port
 * @param config	a mode as define in \b #gpio_output_cfg_t
 * @return		none
 */
static inline void _gpio_set_ocr(struct mxc_gpio_port *port, u32 index,
				 gpio_output_cfg_t config)
{
	u32 reg, val;
	u32 mask;

	mask = 0x3 << ((index % 16) << 1);
	if (index >= 16) {
		reg = port->base + GPIO_OCR2;
		val = config << ((index - 16) * 2);
	} else {
		reg = port->base + GPIO_OCR1;
		val = config << (index * 2);
	}
	val |= readl(reg) & (~mask);
	writel(val, reg);
}

/*!
 *@brief gpio_config_mux - just configure the mode of the gpio pin.
 *@param pin   a pin number as defined in \b #iomux_pin_name_t
 *@param mode  a module as define in \b #gpio_mux_mode_t;
 *	GPIO_MUX_PRIMARY set pin to work as primary function.
 *	GPIO_MUX_ALT set pin to work as alternate function.
 *	GPIO_MUX_GPIO set pin to work as output function based the data register
 *	GPIO_MUX_INPUT1 set pin to work as input function connected with  A_OUT
 *	GPIO_MUX_INPUT2 set pin to work as input function connected with B_OUT
 *	GPIO_MUX_OUTPUT1 set pin to work as output function connected with A_IN
 *	GPIO_MUX_OUTPUT2 set pin to work as output function connected with B_IN
 *	GPIO_MUX_OUTPUT3 set pin to work as output function connected with C_IN
 *@return      0 if successful, Non-zero otherwise
 */

int gpio_config_mux(iomux_pin_name_t pin, gpio_mux_mode_t mode)
{
	u32 gius_reg, gpr_reg;
	struct mxc_gpio_port *port;
	u32 index, gpio = IOMUX_TO_GPIO(pin);

	port = &(mxc_gpio_ports[GPIO_TO_PORT(gpio)]);
	index = GPIO_TO_INDEX(gpio);

	diag_printf1("%s: Configuring PORT %c, bit %d\n",
		 __FUNCTION__, port->num + 'A', index);

	gius_reg = readl(port->base + GPIO_GIUS);
	gpr_reg = readl(port->base + GPIO_GPR);

	switch (mode) {
	case GPIO_MUX_PRIMARY:
		gius_reg &= ~(1L << index);
		gpr_reg &= ~(1L << index);
		break;
	case GPIO_MUX_ALT:
		gius_reg &= ~(1L << index);
		gpr_reg |= (1L << index);
		break;
	case GPIO_MUX_GPIO:
		gius_reg |= (1L << index);
		_gpio_set_ocr(port, index, GPIO_OUTPUT_DR);
		break;
	case GPIO_MUX_INPUT1:
		gius_reg |= (1L << index);
		_gpio_set_iconfa(port, index, GPIO_INPUT_GPIO);
		break;
	case GPIO_MUX_INPUT2:
		gius_reg |= (1L << index);
		_gpio_set_iconfb(port, index, GPIO_INPUT_GPIO);
		break;
	case GPIO_MUX_OUTPUT1:
		gius_reg |= (1L << index);
		_gpio_set_ocr(port, index, GPIO_OUTPUT_A);
		break;
	case GPIO_MUX_OUTPUT2:
		gius_reg |= (1L << index);
		_gpio_set_ocr(port, index, GPIO_OUTPUT_B);
		break;
	case GPIO_MUX_OUTPUT3:
		gius_reg |= (1L << index);
		_gpio_set_ocr(port, index, GPIO_OUTPUT_C);
		break;
	default:
		return -1;
	}

	writel(gius_reg, port->base + GPIO_GIUS);
	writel(gpr_reg, port->base + GPIO_GPR);

	return 0;
}

/*!
 * This function is just used to enable or disable the pull up feature .
 * @param pin   a pin number as defined in \b #iomux_pin_name_t
 * @param en    0 if disable, Non-zero enable
 * @return      0 if successful, Non-zero otherwise
 */
int gpio_set_puen(iomux_pin_name_t pin, bool en)
{
	struct mxc_gpio_port *port;
	u32 index, gpio = IOMUX_TO_GPIO(pin);

	port = &(mxc_gpio_ports[GPIO_TO_PORT(gpio)]);
	index = GPIO_TO_INDEX(gpio);

//	diag_printf("%s: Configuring output mode of PORT %c, bit %d\n",
//		 __FUNCTION__, port->num + 'A', index);

	_gpio_set_puen(port, index, en);
	return 0;

}

/*!
 * This function is just used to request a pin and configure it.
 * @param pin	a pin number as defined in \b #iomux_pin_name_t
 * @param mode	a module as define in \b #gpio_mux_mode_t;
 * @return	0 if successful, Non-zero otherwise
 */
int gpio_request_mux(iomux_pin_name_t pin, gpio_mux_mode_t mode)
{
	int ret;
	ret = mxc_request_gpio(pin);
	if (ret == 0) {
		ret = gpio_config_mux(pin, mode);
		if (ret) {
			diag_printf("%s(pin=%d, mode=%d) failed\n", __FUNCTION__, pin, mode);
		}
	}
	return ret;
}
