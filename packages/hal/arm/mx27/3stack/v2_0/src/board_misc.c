//==========================================================================
//
//      board_misc.c
//
//      HAL misc board support code for the board
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <redboot.h>
#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>         // base types
#include <cyg/infra/cyg_trac.h>         // tracing macros
#include <cyg/infra/cyg_ass.h>          // assertion macros

#include <cyg/hal/hal_io.h>             // IO macros
#include <cyg/hal/hal_arch.h>           // Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>           // Interrupt names
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_soc.h>            // Hardware definitions
#include <cyg/hal/fsl_board.h>          // Platform specifics

#include <cyg/infra/diag.h>             // diag_printf

// All the MM table layout is here:
#include <cyg/hal/hal_mm.h>
#include <cyg/io/mxc_spi.h>

externC void* memset(void *, int, size_t);

void hal_mmu_init(void)
{
    unsigned long ttb_base = RAM_BANK0_BASE + 0x4000;
    unsigned long i;

    /*
     * Set the TTB register
     */
    asm volatile ("mcr  p15,0,%0,c2,c0,0" : : "r"(ttb_base) /*:*/);

    /*
     * Set the Domain Access Control Register
     */
    i = ARM_ACCESS_DACR_DEFAULT;
    asm volatile ("mcr  p15,0,%0,c3,c0,0" : : "r"(i) /*:*/);

    /*
     * First clear all TT entries - ie Set them to Faulting
     */
    memset((void *)ttb_base, 0, ARM_FIRST_LEVEL_PAGE_TABLE_SIZE);

    /*             Actual    Virtual  Size   Attributes                                                    Function  */
    /*             Base      Base     MB      cached?           buffered?        access permissions                 */
    /*             xxx00000  xxx00000                                                                                */
    X_ARM_MMU_SECTION(0x000, 0xF00,   0x001, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* Boot Rom */
    X_ARM_MMU_SECTION(0x100, 0x100,   0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* Internal Regsisters */
    X_ARM_MMU_SECTION(0x800, 0x800,   0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* CSI/ATA Regsisters */
    X_ARM_MMU_SECTION(0xA00, 0x000,   0x080, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
    X_ARM_MMU_SECTION(0xA00, 0xA00,   0x080, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
    X_ARM_MMU_SECTION(0xA00, 0xA80,   0x080, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* SDRAM */
    X_ARM_MMU_SECTION(0xC00, 0xC00,   0x020, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* Flash */
    X_ARM_MMU_SECTION(0xD60, 0xD60,   0x020, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* CS5 for External I/0 */
    X_ARM_MMU_SECTION(0xD80, 0xD80,   0x100, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* EMI control/PCMCIA */
    X_ARM_MMU_SECTION(0xFFF, 0xFFF,   0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* VRAM */
}

//
// Platform specific initialization
//
static void fec_gpio_init(void)
{
        unsigned long addr , val;

        /* PF23 PF10*/
        addr = SOC_GPIOF_BASE;
        /* OCR2: AIN=0x00 */
        HAL_READ_UINT32(addr+GPIO_OCR2, val);
        val = val & (~(0x00000003<<(13)));
        HAL_WRITE_UINT32(addr+GPIO_OCR2, val);

	 /* OCR1: DR */
        HAL_READ_UINT32(addr+GPIO_OCR1, val);
        val = val | ((0x00000003<<(20)));
        HAL_WRITE_UINT32(addr+GPIO_OCR1, val);

        /* DDR: OUTPUT */
        HAL_READ_UINT32(addr+GPIO_DDIR, val);
        val = val | (0x00000001<<(23)) | (1<<10);
        HAL_WRITE_UINT32(addr+GPIO_DDIR, val);

        /* GIUS: GPIO */
        HAL_READ_UINT32(addr+GPIO_GIUS, val);
        val = val | (0x00000001<<23) | (1<<10);
        HAL_WRITE_UINT32(addr+GPIO_GIUS, val);

    	/* DR: DATA */
        HAL_READ_UINT32(addr+GPIO_DR, val);
        val = val & (~(1<<10));
        HAL_WRITE_UINT32(addr+GPIO_DR, val);


        /* PD16-0*/
        addr = SOC_GPIOD_BASE;
        /* PD16 OCR2: AIN=0x00 */
        HAL_READ_UINT32(addr+GPIO_OCR2, val);
        val = val & ~0x00000003;
        HAL_WRITE_UINT32(addr+GPIO_OCR2, val);

        /* PD9, 3-0  OCR1: AIN=0x00 */
        HAL_READ_UINT32(addr+GPIO_OCR1, val);
        val = val & ~(0x000C00FF);
        HAL_WRITE_UINT32(addr+GPIO_OCR1, val);

        /* PD15~10, P7~4  ICONFIGA1: AOUT=0x00 */
        HAL_READ_UINT32(addr+GPIO_ICONFA1, val);
        val = val & ~(0xFFF0FF00);
        HAL_WRITE_UINT32(addr+GPIO_ICONFA1, val);

        /* PD8  GPR: ALT */
        HAL_READ_UINT32(addr+GPIO_GPR, val);
        val = val | 0x00000100;
        HAL_WRITE_UINT32(addr+GPIO_GPR, val);

        /* DDR: OUTPUT */
        HAL_READ_UINT32(addr+GPIO_DDIR, val);
        val = (val & 0xFFFE0000) | 0x0001020F;
        HAL_WRITE_UINT32(addr+GPIO_DDIR, val);

        /* GIUS: GPIO */
        HAL_READ_UINT32(addr+GPIO_GIUS, val);
        val = (val & 0xFFFE0000) | 0x0001FEFF;
        HAL_WRITE_UINT32(addr+GPIO_GIUS, val);

	/* PB24: */
        addr = SOC_GPIOB_BASE;

        HAL_READ_UINT32(addr+GPIO_DDIR, val);
        val = val | (1<<24);
        HAL_WRITE_UINT32(addr+GPIO_DDIR, val);

	HAL_READ_UINT32(addr+GPIO_OCR2, val);
        val = val | (3<<16);
        HAL_WRITE_UINT32(addr+GPIO_OCR2, val);

        HAL_READ_UINT32(addr+GPIO_GIUS, val);
        val = val | (1<<24);
        HAL_WRITE_UINT32(addr+GPIO_GIUS, val);

	HAL_READ_UINT32(addr+GPIO_DR, val);
        val = val | (1<<24);
        HAL_WRITE_UINT32(addr+GPIO_DR, val);
}

static void fec_power_init(void)
{
	unsigned long addr , val;

	/* Turn on the power of PHY*/
	val = pmic_reg(34, val, 0);
	val |= (1<<6) | (1<< 10) | (1<<12);
	pmic_reg(34, val, 1);

	/* Wait until the power is stable*/
	for(val = 0; val< 5000; val++)
		hal_delay_us(5);

        /*Issue the reset signal*/
        addr = SOC_GPIOF_BASE;

	HAL_READ_UINT32(addr+GPIO_DR, val);
        val = val & (~(1<<10));
        HAL_WRITE_UINT32(addr+GPIO_DR, val);

	for(val = 0; val< 300; val++)
		hal_delay_us(2);

   	HAL_READ_UINT32(addr+GPIO_DR, val);
        val = val | (1<<10);
        HAL_WRITE_UINT32(addr+GPIO_DR, val);

	for(val = 0; val< 5000; val++)
		hal_delay_us(5);
}

RedBoot_init(fec_power_init, 9000);

//
// Platform specific initialization
//

unsigned int g_clock_src;
unsigned int g_board_type = BOARD_TYPE_UNKNOWN;

void plf_hardware_init(void)
{
    unsigned long val = readl(SOC_CRM_CSCR);

    if ((val & (1 << 16)) != 0) {
            g_clock_src = FREQ_26MHZ;
    } else {
        g_clock_src = FREQ_32768HZ;
    }

    g_board_type = BOARD_TYPE_3STACK;

    fec_gpio_init();
}

#include CYGHWR_MEMORY_LAYOUT_H

typedef void code_fun(void);

void board_program_new_stack(void *func)
{
    register CYG_ADDRESS stack_ptr asm("sp");
    register CYG_ADDRESS old_stack asm("r4");
    register code_fun *new_func asm("r0");
    old_stack = stack_ptr;
    stack_ptr = CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE - sizeof(CYG_ADDRESS);
    new_func = (code_fun*)func;
    new_func();
    stack_ptr = old_stack;
}

static void display_clock_src(void)
{
    if (g_clock_src == FREQ_26MHZ) {
        diag_printf("Clock input: 26 MHz");
    } else if (g_clock_src == FREQ_32768HZ) {
        diag_printf("Clock input: 32KHz");
    } else {
        diag_printf("Unknown clock input source. Something is wrong!");
    }
}

static void display_board_type(void)
{
    if (g_board_type == BOARD_TYPE_3STACK) {
        diag_printf("\nBoard Type: 3-Stack\n");
    } else {
        diag_printf("\nBoard Type: Unknown val %d\n", g_board_type);
    }
}

static void display_board_info(void)
{
    display_board_type();
    display_clock_src();
}

RedBoot_init(display_board_info, RedBoot_INIT_LAST);
// ------------------------------------------------------------------------
