#ifndef CYGONCE_HAL_PLATFORM_SETUP_H
#define CYGONCE_HAL_PLATFORM_SETUP_H

//=============================================================================
//
//      hal_platform_setup.h
//
//      Platform specific support for HAL (assembly code)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <pkgconf/system.h>             // System-wide configuration info
#include CYGBLD_HAL_VARIANT_H           // Variant specific configuration
#include CYGBLD_HAL_PLATFORM_H          // Platform specific configuration
#include <cyg/hal/hal_soc.h>            // Variant specific hardware definitions
#include <cyg/hal/hal_mmu.h>            // MMU definitions
#include <cyg/hal/fsl_board.h>          // Platform specific hardware definitions

#if defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1 _platform_setup1
#define CYGHWR_HAL_ARM_HAS_MMU

#ifdef CYG_HAL_STARTUP_ROMRAM
#define CYGSEM_HAL_ROM_RESET_USES_JUMP
#endif

#define CYGHWR_HAL_ROM_VADDR        0x0

//#define NFC_2K_BI_SWAP

// This macro represents the initial startup code for the platform
// r11 is reserved to contain chip rev info in this file
    .macro  _platform_setup1
FSL_BOARD_SETUP_START:
    // invalidate I/D cache/TLB and drain write buffer
    mov r0, #0
    mcr 15, 0, r0, c7, c7, 0    /* invalidate I cache and D cache */
    mcr 15, 0, r0, c8, c7, 0    /* invalidate TLBs */
    mcr 15, 0, r0, c7, c10, 4   /* Drain the write buffer */

    mov r0, #NFC_BASE
    add r1, r0, #0x400
    cmp pc, r0
    blo init_aipi_start
    cmp pc, r1
    bhi init_aipi_start
#ifdef NFC_2K_BI_SWAP
    ldr r3, [r0, #0x7D0]    // load word at addr 464 of last 512 RAM buffer
    and r3, r3, #0xFFFFFF00 // mask off the LSB
    ldr r4, [r0, #0x834]    // load word at addr 4 of the 3rd spare area buffer
    mov r4, r4, lsr #8      // shift it to get the byte at addr 5
    and r4, r4, #0xFF       // throw away upper 3 bytes
    add r3, r4, r3          // construct the word
    str r3, [r0, #0x7D0]    // write back
#endif

init_aipi_start:
    init_aipi

    mov r0, #SDRAM_NON_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1] // for checking boot source from nand, nor or sdram

    // setup System Controls
    ldr r0, SOC_SYSCTRL_BASE_W
    mov r1, #0x03
    str r1, [r0, #(SOC_SYSCTRL_PCSR - SOC_SYSCTRL_BASE)]
    ldr r1, [r0, #(SOC_SYSCTRL_FMCR - SOC_SYSCTRL_BASE)]
    and r1, r1, #0xFFFFFFF0
    orr r1, r1, #9
    str r1, [r0, #(SOC_SYSCTRL_FMCR - SOC_SYSCTRL_BASE)]

init_max_start:
    init_max
init_drive_strength_start:
    init_drive_strength
init_cs4_start:
    init_cs4

//init_cs0_start:
//    init_cs0

    // check if sdram has been setup
    cmp pc, #SDRAM_BASE_ADDR
    blo init_clock_start
    cmp pc, #(SDRAM_BASE_ADDR + SDRAM_SIZE)
    blo HWInitialise_skip_SDRAM_setup
init_clock_start:
    init_clock

    // Now we must boot from Flash
    mov r0, #NOR_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]

init_sdram_start:
    setup_sdram_ddr

HWInitialise_skip_SDRAM_setup:

    ldr r0, NFC_BASE_W
    add r2, r0, #0x800      // 2K window
    cmp pc, r0
    blo Normal_Boot_Continue
    cmp pc, r2
    bhi Normal_Boot_Continue

NAND_Boot_Start:
    /* Copy image from flash to SDRAM first */
    ldr r1, MXC_REDBOOT_ROM_START

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    blo 1b

    /* Jump to SDRAM */
    ldr r1, CONST_0xFFF
    and r0, pc, r1     /* offset of pc */
    ldr r1, MXC_REDBOOT_ROM_START
    add r1, r1, #0x10
    add pc, r0, r1
    nop
    nop
    nop
    nop

NAND_Copy_Main:
    // Check if x16/2kb page
    ldr r7, SOC_SYSCTRL_BASE_W
    ldr r7, [r7, #0x14]
    ands r7, r7, #(1 << 5)

    mov r0, #NAND_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]
    mov r0, #MXCFIS_NAND
    ldr r1, AVIC_VECTOR1_ADDR_W
    str r0, [r1]

    ldr r0, NFC_BASE_W   //r0: nfc base. Reloaded after each page copying
    mov r1, #0x800       //r1: starting flash addr to be copied. Updated constantly
    add r2, r0, #0x800     //2K Page:: r2: end of 1st RAM buf. Doesn't change
    addeq r2, r0, #0x200   //512 Page:: r2: end of 1st RAM buf. Doesn't change
    add r12, r0, #0xE00  //r12: NFC register base. Doesn't change
    ldr r11, MXC_REDBOOT_ROM_START
    add r13, r11, #REDBOOT_IMAGE_SIZE //r13: end of SDRAM address for copying. Doesn't change
    add r11, r11, r1     //r11: starting SDRAM address for copying. Updated constantly

    //unlock internal buffer
    mov r3, #0x2
    strh r3, [r12, #0xA]

Nfc_Read_Page:
//  NFC_CMD_INPUT(FLASH_Read_Mode1);
    mov r3, #0x0
    nfc_cmd_input

    // Check if x16/2kb page
    ldr r7, SOC_SYSCTRL_BASE_W
    ldr r7, [r7, #0x14]
    ands r7, r7, #(1 << 5)
    bne nfc_addr_ops_2kb
//    start_nfc_addr_ops(ADDRESS_INPUT_READ_PAGE, addr, nflash_dev_info->base_mask);
    mov r3, r1
    do_addr_input       //1st addr cycle
    mov r3, r1, lsr #9
    do_addr_input       //2nd addr cycle
    mov r3, r1, lsr #17
    do_addr_input       //3rd addr cycle
    mov r3, r1, lsr #25
    do_addr_input       //4th addr cycle
    b end_of_nfc_addr_ops

nfc_addr_ops_2kb:
//    start_nfc_addr_ops(ADDRESS_INPUT_READ_PAGE, addr, nflash_dev_info->base_mask);
    mov r3, #0
    do_addr_input       //1st addr cycle
    mov r3, #0
    do_addr_input       //2nd addr cycle
    mov r3, r1, lsr #11
    do_addr_input       //3rd addr cycle
    mov r3, r1, lsr #19
    do_addr_input       //4th addr cycle
    mov r3, r1, lsr #27
    do_addr_input       //4th addr cycle

//    NFC_CMD_INPUT(FLASH_Read_Mode1_2K);
    mov r3, #0x30
    nfc_cmd_input

end_of_nfc_addr_ops:
//    NFC_DATA_OUTPUT(buf, FDO_PAGE_SPARE_VAL);
//        writew(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN,
//               NAND_FLASH_CONFIG1_REG);
    mov r8, #0
    bl nfc_data_output
    bl do_wait_op_done
    // Check if x16/2kb page
    ldr r7, SOC_SYSCTRL_BASE_W
    ldr r7, [r7, #0x14]
    ands r7, r7, #(1 << 5)
    beq nfc_addr_data_output_done_512

// For 2K page - 2nd 512
    mov r8, #1
    bl nfc_data_output
    bl do_wait_op_done

// 3rd 512
    mov r8, #2
    bl nfc_data_output
    bl do_wait_op_done

// 4th 512
    mov r8, #3
    bl nfc_data_output
    bl do_wait_op_done
// end of 4th
#ifdef NFC_2K_BI_SWAP
    ldr r3, [r0, #0x7D0]    // load word at addr 464 of last 512 RAM buffer
    and r3, r3, #0xFFFFFF00 // mask off the LSB
    ldr r4, [r0, #0x834]    // load word at addr 4 of the 3rd spare area buffer
    mov r4, r4, lsr #8      // shift it to get the byte at addr 5
    and r4, r4, #0xFF       // throw away upper 3 bytes
    add r3, r4, r3          // construct the word
    str r3, [r0, #0x7D0]    // write back
#endif
    // check for bad block
    mov r3, r1, lsl #(32-17)    // get rid of block number
    cmp r3, #(0x800 << (32-17)) // check if not page 0 or 1
    b nfc_addr_data_output_done

nfc_addr_data_output_done_512:
    // check for bad block
    mov r3, r1, lsl #(32-5-9)    // get rid of block number
    cmp r3, #(512 << (32-5-9))   // check if not page 0 or 1

nfc_addr_data_output_done:
    b Copy_Good_Blk // workaround for now. See ENGR00067497
//    bhi Copy_Good_Blk
    add r4, r0, #0x800  //r3 -> spare area buf 0
    ldrh r4, [r4, #0x4]
    and r4, r4, #0xFF00
    cmp r4, #0xFF00
    beq Copy_Good_Blk
    // really sucks. Bad block!!!!
    cmp r3, #0x0
    beq Skip_bad_block
    // even suckier since we already read the first page!

    // Check if x16/2kb page
    ldr r7, SOC_SYSCTRL_BASE_W
    ldr r7, [r7, #0x14]
    ands r7, r7, #(1 << 5)

    subeq r11, r11, #512  //rewind 1 page for the sdram pointer
    subeq r1, r1, #512    //rewind 1 page for the flash pointer

    // for 2k page
    subne r11, r11, #0x800  //rewind 1 page for the sdram pointer
    subne r1, r1, #0x800    //rewind 1 page for the flash pointer

Skip_bad_block:
    // Check if x16/2kb page
    ldr r7, SOC_SYSCTRL_BASE_W
    ldr r7, [r7, #0x14]
    ands r7, r7, #(1 << 5)

    addeq r1, r1, #(32*512)
    addne r1, r1, #(64*2048)

    b Nfc_Read_Page
Copy_Good_Blk:
    //copying page
1:  ldmia r0!, {r3-r10}
    stmia r11!, {r3-r10}
    cmp r0, r2
    blo 1b
    cmp r11, r13
    bge NAND_Copy_Main_done
    // Check if x16/2kb page
    ldr r7, SOC_SYSCTRL_BASE_W
    ldr r7, [r7, #0x14]
    ands r7, r7, #(1 << 5)

    addeq r1, r1, #0x200
    addne r1, r1, #0x800
    mov r0, #NFC_BASE
    b Nfc_Read_Page

NAND_Copy_Main_done:

Normal_Boot_Continue:

#ifdef CYG_HAL_STARTUP_ROMRAM    /* enable running from RAM */
    /* Copy image from flash to SDRAM first */
    ldr r0, =0xFFFFF000
    and r0, r0, pc
    ldr r1, MXC_REDBOOT_ROM_START
    cmp r0, r1
    beq HWInitialise_skip_SDRAM_copy

    add r2, r0, #REDBOOT_IMAGE_SIZE

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    ble 1b
    /* Jump to SDRAM */
    ldr r1, =0xFFFF
    and r0, pc, r1      /* offset of pc */
    ldr r1, =(SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000 + 0x8)
    add pc, r0, r1
    nop
    nop
    nop
    nop
#endif /* CYG_HAL_STARTUP_ROMRAM */

HWInitialise_skip_SDRAM_copy:

init_cs0_sync_start:
    init_cs0_sync

NAND_ClockSetup:
    ldr r1, =(SOC_CRM_BASE)
    ldr r2, [r1, #(SOC_CRM_PCDR0 - SOC_CRM_BASE)]
/*Get chip ID://eq:i.MX27 TO1; neq:i.MX27 TO2*/
    ldr r1, =SOC_SI_ID_REG
    ldr r1, [r1]
    ands r1, r1, #0xF0000000

    orreq r2, r2, #0xF000
    orrne r2, r2, #0x01C0

    ldr r1, =(SOC_CRM_BASE)
    str r2, [r1, #(SOC_CRM_PCDR0 - SOC_CRM_BASE)]

/* end of NAND clock divider setup */

    // TLSbo76381: enable USB/PP/DMA burst override bits in GPCR
    ldr r1, =(SOC_SYSCTRL_GPCR)
    ldr r2, [r1]
    orr r2, r2, #0x700
    str r2, [r1]

    // Set up a stack [for calling C code]
    ldr r1, =__startup_stack
    ldr r2, =RAM_BANK0_BASE
    orr sp, r1, r2

    // Create MMU tables
    bl hal_mmu_init

    // Enable MMU
    ldr r2, =10f
    mrc MMU_CP, 0, r1, MMU_Control, c0      // get c1 value to r1 first
    orr r1, r1, #7                          // enable MMU bit
    mcr MMU_CP, 0, r1, MMU_Control, c0
    mov pc,r2    /* Change address spaces */
    nop
    nop
    nop
10:

    // Save shadow copy of BCR, also hardware configuration
    ldr r1, =_board_BCR
    str r2,[r1]
    ldr r1, =_board_CFG
    str r9,[r1]                // Saved far above...

    .endm                       // _platform_setup1

do_wait_op_done:
    1:
        ldrh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        ands r3, r3, #NAND_FLASH_CONFIG2_INT_DONE
        beq 1b
    bx lr     // do_wait_op_done

nfc_data_output:
    mov r3, #(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN)
    strh r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]

    // writew(buf_no, RAM_BUFFER_ADDRESS_REG);
    strh r8, [r12, #RAM_BUFFER_ADDRESS_REG_OFF]
    // writew(FDO_PAGE_SPARE_VAL & 0xFF, NAND_FLASH_CONFIG2_REG);
    mov r3, #FDO_PAGE_SPARE_VAL
    strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
    bx lr

#else // defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1
#endif

    .macro init_clock
        ldr r0, SOC_CRM_BASE_W
        // disable MPLL/SPLL first
        ldr r1, [r0, #(SOC_CRM_CSCR - SOC_CRM_BASE)]
        bic r1, r1, #0x3
        str r1, [r0, #(SOC_CRM_CSCR - SOC_CRM_BASE)]

        /* Get the chip version and configure PLLs*/
        ldr r1, SOC_SI_ID_REG_W
        ldr r1, [r1]
        ands r1, r1, #0xF0000000
        beq 1f

        ldr r1, PBC_BASE_W
        ldrh r1, [r1, #PBC_VERSION]
        ands r1, r1, #CLK_INPUT_27MHZ_SET

        // configure MPCTL0
        ldrne r1, CRM_MPCTL0_VAL2_W
        ldreq r1, CRM_MPCTL0_VAL2_27MHZ_W
        str r1, [r0, #(SOC_CRM_MPCTL0 - SOC_CRM_BASE)]
        // configure SPCTL0
        ldrne r1, CRM_SPCTL0_VAL2_W
        ldreq r1, CRM_SPCTL0_VAL2_27MHZ_W
        str r1, [r0, #(SOC_CRM_SPCTL0 - SOC_CRM_BASE)]
        b 2f

    1:  ldr r1, PBC_BASE_W
        ldrh r1, [r1, #PBC_VERSION]
        ands r1, r1, #CLK_INPUT_27MHZ_SET

        // configure MPCTL0
        ldrne r1, CRM_MPCTL0_VAL_W
        ldreq r1, CRM_MPCTL0_VAL_27MHZ_W
        str r1, [r0, #(SOC_CRM_MPCTL0 - SOC_CRM_BASE)]
        // configure SPCTL0
        ldrne r1, CRM_SPCTL0_VAL_W
        ldreq r1, CRM_SPCTL0_VAL_27MHZ_W
        str r1, [r0, #(SOC_CRM_SPCTL0 - SOC_CRM_BASE)]

        // enable/restart SPLL/MPLL
    2:  ldr r1, [r0, #(SOC_CRM_CSCR - SOC_CRM_BASE)]
#ifdef PLL_REF_CLK_32768HZ
        // Make sure to use CKIL
        bic r1, r1, #(3 << 16)
#else
        orr r1, r1, #(3 << 16)      // select 26MHz
#endif
        orr r1, r1, #0x000C0000
        orr r1, r1, #0x00000003
        str r1, [r0, #(SOC_CRM_CSCR - SOC_CRM_BASE)]

        // add some delay here
        mov r1, #0x1000
    1:  subs r1, r1, #0x1
        bne 1b

        //Check The chip version TO1 or TO2
        ldr r1, SOC_SI_ID_REG_W
        ldr r1, [r1]
        ands r1, r1, #0xF0000000

        ldreq r2, SOC_CRM_CSCR_W
        ldrne r2, SOC_CRM_CSCR2_W
        str r2, [r0, #(SOC_CRM_CSCR - SOC_CRM_BASE)]

        //for i.MX27 TO2, Set divider of H264_CLK to zero, NFC to 3.
        ldrne r2, [r0, #(SOC_CRM_PCDR0 - SOC_CRM_BASE)]
        bicne r2, r2, #0x0000FC00
        strne r2, [r0, #(SOC_CRM_PCDR0 - SOC_CRM_BASE)]

        /* Configure PCDR */
        /* Configure PCDR1 */
        ldr r1, SOC_CRM_PCDR1_W
        str r1, [r0, #(SOC_CRM_PCDR1 - SOC_CRM_BASE)]

        // Configure PCCR0 and PCCR1
        ldr r1, SOC_CRM_PCCR0_W
        str r1, [r0, #(SOC_CRM_PCCR0 - SOC_CRM_BASE)]

        ldr r1, [r0, #(SOC_CRM_PCCR1 - SOC_CRM_BASE)]
        orr r1, r1, #0x0780
        str r1, [r0, #(SOC_CRM_PCCR1 - SOC_CRM_BASE)]
        // make default CLKO to be FCLK
        ldr r1, [r0, #(SOC_CRM_CCSR - SOC_CRM_BASE)]
        and r1, r1, #0xFFFFFFE0
        orr r1, r1, #0x7
        str r1, [r0, #(SOC_CRM_CCSR - SOC_CRM_BASE)]
    .endm //init_clock

    .macro init_cs0
        ldr r1, SOC_CS0_CTL_BASE_W
        ldr r2, CS0_0x0000CC03
        str r2, [r1, #CSCRU_OFFSET]
        ldr r2, CS0_0xA0330D01
        str r2, [r1, #CSCRL_OFFSET]
        ldr r2, CS0_0x00220800
        str r2, [r1, #CSCRA_OFFSET]
    .endm // init_cs0

    /* CS0 sync mode setup */
    .macro init_cs0_sync
        /*
         * Sync mode (AHB Clk = 133MHz ; BCLK = 44.3MHz):
         */
        /* Flash reset command */
        ldr     r0, CS0_BASE_ADDR_W
        mov     r1, #0x00F0
        strh    r1, [r0]
        /* 1st command */
        ldr     r2, CS0_CMD_0xAAA
        add     r2, r2, r0
        mov     r1, #0x00AA
        strh    r1, [r2]
        /* 2nd command */
        ldr     r2, CS0_CMD_0x554
        add     r2, r2, r0
        mov     r1, #0x0055
        strh    r1, [r2]
        /* 3rd command */
        ldr     r2, CS0_CMD_0xAAA
        add     r2, r2, r0
        mov     r1, #0x00D0
        strh    r1, [r2]
        /* Write flash config register */
        ldr     r1, CS0_CFG_0x66CA
        strh    r1, [r2]
        /* Flash reset command */
        mov     r1, #0x00F0
        strh    r1, [r0]

        ldr r0, =SOC_CS0_CTL_BASE
        ldr r1, CS0_0x23524E80
        str r1, [r0, #CSCRU_OFFSET]
        ldr r1, CS0_0x10000D03
        str r1, [r0, #CSCRL_OFFSET]
        ldr r1, CS0_0x00720900
        str r1, [r0, #CSCRA_OFFSET]
    .endm /* init_cs0_sync */

    .macro init_cs4 /* ADS board expanded IOs */
        ldr r1, SOC_CS4_CTL_BASE_W
        ldr r2, CS4_CSCRU_0x0000DCF6
        str r2, [r1, #CSCRU_OFFSET]
        ldr r2, CS4_CSCRL_0x444A4541
        str r2, [r1, #CSCRL_OFFSET]
        ldr r2, CS4_CSCRA_0x44443302
        str r2, [r1, #CSCRA_OFFSET]
    .endm   /* init_cs4 */

    .macro init_aipi
        // setup AIPI1 and AIPI2
        mov r0, #SOC_AIPI1_BASE
        ldr r1, AIPI1_0x20040304
        str r1, [r0]  /* PSR0 */
        ldr r2, AIPI1_0xDFFBFCFB
        str r2, [r0, #4]  /* PSR1 */
        // set r0 = AIPI2 base
        add r0, r0, #0x20000
        mov r1, #0x0
        str r1, [r0]  /* PSR0 */
        mov r2, #0xFFFFFFFF
        str r2, [r0, #4]  /* PSR1 */
    .endm // init_aipi

    .macro init_max
        ldr r0, SOC_MAX_BASE_W
        add r1, r0, #MAX_SLAVE_PORT1_OFFSET
        add r2, r0, #MAX_SLAVE_PORT2_OFFSET
        add r0, r0, #MAX_SLAVE_PORT0_OFFSET

        /* MPR and AMPR */
        ldr r6, SOC_MAX_0x00302145         /* Priority SLCD>EMMA>DMA>Codec>DAHB>IAHB */
        str r6, [r0, #MAX_SLAVE_MPR_OFFSET]   /* same for all slave ports */
        str r6, [r0, #MAX_SLAVE_AMPR_OFFSET]
        str r6, [r1, #MAX_SLAVE_MPR_OFFSET]
        str r6, [r1, #MAX_SLAVE_AMPR_OFFSET]
        str r6, [r2, #MAX_SLAVE_MPR_OFFSET]
        str r6, [r2, #MAX_SLAVE_AMPR_OFFSET]
    .endm //init_max

   .macro init_drive_strength
        ldr r0, SOC_SYSCTRL_BASE_W
        ldr r1, DS_0x55555555
        str r1, [r0, #(SOC_SYSCTRL_DSCR3 - SOC_SYSCTRL_BASE)]
        str r1, [r0, #(SOC_SYSCTRL_DSCR5 - SOC_SYSCTRL_BASE)]
        str r1, [r0, #(SOC_SYSCTRL_DSCR6 - SOC_SYSCTRL_BASE)]
        ldr r1, DS_0x00005005
        str r1, [r0, #(SOC_SYSCTRL_DSCR7 - SOC_SYSCTRL_BASE)]
        ldr r1, DS_0x15555555
        str r1, [r0, #(SOC_SYSCTRL_DSCR8 - SOC_SYSCTRL_BASE)]
    .endm       // init_drive_strength

    .macro setup_sdram_ddr
        ldr r0, SOC_ESDCTL_BASE_W
        mov r2, #SOC_CSD0_BASE
        mov r1, #0x8        // initial reset
        str r1, [r0, #0x10]
        // Hold for more than 200ns
        ldr r1, =0x10000
    1:
        subs r1, r1, #0x1
        bne 1b

        mov r1, #0x4
        str r1, [r0, #0x10]

        //Check The chip version TO1 or TO2
        ldr r1, SOC_SI_ID_REG_W
        ldr r1, [r1]
        ands r1, r1, #0xF0000000
        // add Latency on CAS only for TO2
       // TO 1.0's ID = 0x0 ==>> CAS = 3
       bne 2f
        ldr r1, SDRAM_0x00795729
       b 3f
       // now handles TO 2.x
    2:
       ands r1, r1, #0xE0000000
       // TO 2.0's ID = 0x1 => CAS = 4 due to the MPEG4 issue
       ldreq r1, SDRAM_0x00795429
        // subesquent TO's are OK w/ CAS = 3
        ldrne r1, SDRAM_0x00795729
    3:
        str r1, [r0, #0x4]
        ldr r1, SDRAM_0x92200000
        str r1, [r0, #0x0]
        ldr r1, [r2, #0xF00]
        ldr r1, SDRAM_0xA2200000
        str r1, [r0, #0x0]
        ldr r1, [r2, #0xF00]
        ldr r1, [r2, #0xF00]
        ldr r1, SDRAM_0xB2200000
        str r1, [r0, #0x0]
        ldrb r1, [r2, #0x33]
        add r3, r2, #0x1000000
        ldrb r1, [r3]
        ldr r1, SDRAM_0x82228485
        str r1, [r0, #0x0]
    .endm   // setup_sdram_ddr

    .macro nfc_cmd_input
        strh r3, [r12, #NAND_FLASH_CMD_REG_OFF]
        mov r3, #NAND_FLASH_CONFIG2_FCMD_EN;
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        bl do_wait_op_done
    .endm   // nfc_cmd_input

    .macro do_addr_input
        and r3, r3, #0xFF
        strh r3, [r12, #NAND_FLASH_ADD_REG_OFF]
        mov r3, #NAND_FLASH_CONFIG2_FADD_EN
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        bl do_wait_op_done
    .endm   // do_addr_input

#define PLATFORM_VECTORS         _platform_vectors
    .macro  _platform_vectors
        .globl  _board_BCR, _board_CFG
_board_BCR:   .long   0       // Board Control register shadow
_board_CFG:   .long   0       // Board Configuration (read at RESET)
    .endm

MXC_REDBOOT_ROM_START:      .word   SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000
CONST_0xFFF:                .word   0xFFF
AVIC_VECTOR0_ADDR_W:        .word   MXCBOOT_FLAG_REG
AVIC_VECTOR1_ADDR_W:        .word   MXCFIS_FLAG_REG
SOC_SYSCTRL_BASE_W:         .word   SOC_SYSCTRL_BASE
SOC_MAX_BASE_W:             .word   SOC_MAX_BASE
SOC_MAX_0x00302145:         .word   0x00302145
SOC_CRM_BASE_W:             .word   SOC_CRM_BASE
CRM_MPCTL0_VAL_W:           .word   CRM_MPCTL0_VAL
CRM_SPCTL0_VAL_W:           .word   CRM_SPCTL0_VAL
CRM_MPCTL0_VAL_27MHZ_W:     .word   CRM_MPCTL0_VAL_27MHZ
CRM_SPCTL0_VAL_27MHZ_W:     .word   CRM_SPCTL0_VAL_27MHZ
SOC_CRM_CSCR_W:             .word   CRM_CSCR_VAL
CRM_MPCTL0_VAL2_W:           .word   CRM_MPCTL0_VAL2
CRM_SPCTL0_VAL2_W:           .word   CRM_SPCTL0_VAL2
CRM_MPCTL0_VAL2_27MHZ_W:     .word   CRM_MPCTL0_VAL2_27MHZ
CRM_SPCTL0_VAL2_27MHZ_W:     .word   CRM_SPCTL0_VAL2_27MHZ
SOC_CRM_CSCR2_W:             .word   CRM_CSCR_VAL2
SOC_CRM_PCDR1_W:            .word   0x09030913   // p1=20 p2=10 p3=4 p4=10
SOC_CRM_PCCR0_W:            .word   0x3108480F
SOC_CS4_CTL_BASE_W:         .word   SOC_CS4_CTL_BASE
CS4_CSCRU_0x0000DCF6:       .word   0x0000DCF6
CS4_CSCRL_0x444A4541:       .word   0x444A4541
CS4_CSCRA_0x44443302:       .word   0x44443302
NFC_BASE_W:                 .word   NFC_BASE
SOC_ESDCTL_BASE_W:          .word   SOC_ESDCTL_BASE
SDRAM_0x00795429:           .word   0x00795429
SDRAM_0x00795729:           .word   0x00795729
SDRAM_0x92200000:           .word   0x92200000
SDRAM_0xA2200000:           .word   0xA2200000
SDRAM_0xB2200000:           .word   0xB2200000
SDRAM_0x82228485:           .word   0x82228485
CS0_0x0000CC03:             .word   0x0000CC03
CS0_0xA0330D01:             .word   0xA0330D01
CS0_0x00220800:             .word   0x00220800
CS0_0x23524E80:             .word   0x23524E80
CS0_0x10000D03:             .word   0x10000D03
CS0_0x00720900:             .word   0x00720900
CS0_CMD_0xAAA:              .word   0x0AAA
CS0_CMD_0x554:              .word   0x0554
CS0_CFG_0x66CA:		    .word   0x66CA
CS0_BASE_ADDR_W:            .word   CS0_BASE_ADDR
SOC_CS0_CTL_BASE_W:         .word   SOC_CS0_CTL_BASE
DS_0x55555555:              .word   0x55555555
DS_0x00005005:              .word   0x00005005
DS_0x15555555:              .word   0x15555555
AIPI1_0x20040304:           .word   0x20040304
AIPI1_0xDFFBFCFB:           .word   0xDFFBFCFB
PBC_BASE_W:                 .word   PBC_BASE
SOC_SI_ID_REG_W:            .word   SOC_SI_ID_REG

/*---------------------------------------------------------------------------*/
/* end of hal_platform_setup.h                                               */
#endif /* CYGONCE_HAL_PLATFORM_SETUP_H */
