//==========================================================================
//
//      board_misc.c
//
//      HAL misc board support code for the board
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <redboot.h>
#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>         // base types
#include <cyg/infra/cyg_trac.h>         // tracing macros
#include <cyg/infra/cyg_ass.h>          // assertion macros

#include <cyg/hal/hal_io.h>             // IO macros
#include <cyg/hal/hal_arch.h>           // Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>           // Interrupt names
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_soc.h>            // Hardware definitions
#include <cyg/hal/fsl_board.h>          // Platform specifics

#include <cyg/infra/diag.h>             // diag_printf

// All the MM table layout is here:
#include <cyg/hal/hal_mm.h>

externC void* memset(void *, int, size_t);
#define REG_REGULATOR_MODE_1  33

void hal_mmu_init(void)
{
    unsigned long ttb_base = RAM_BANK0_BASE + 0x4000;
    unsigned long i;

    /*
     * Set the TTB register
     */
    asm volatile ("mcr  p15,0,%0,c2,c0,0" : : "r"(ttb_base) /*:*/);

    /*
     * Set the Domain Access Control Register
     */
    i = ARM_ACCESS_DACR_DEFAULT;
    asm volatile ("mcr  p15,0,%0,c3,c0,0" : : "r"(i) /*:*/);

    /*
     * First clear all TT entries - ie Set them to Faulting
     */
    memset((void *)ttb_base, 0, ARM_FIRST_LEVEL_PAGE_TABLE_SIZE);

    /*             Actual    Virtual  Size   Attributes                                                    Function  */
    /*             Base      Base     MB      cached?           buffered?        access permissions                 */
    /*             xxx00000  xxx00000                                                                                */
    X_ARM_MMU_SECTION(0x000, 0xF00,   0x001, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* Boot Rom */
    X_ARM_MMU_SECTION(0x100, 0x100,   0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* Internal Regsisters */
    X_ARM_MMU_SECTION(0x800, 0x800,   0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* CSI/ATA Regsisters */
    X_ARM_MMU_SECTION(0xA00, 0x000,   0x080, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
    X_ARM_MMU_SECTION(0xA00, 0xA00,   0x080, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
    X_ARM_MMU_SECTION(0xA00, 0xA80,   0x080, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* SDRAM */
    X_ARM_MMU_SECTION(0xC00, 0xC00,   0x020, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* Flash */
    X_ARM_MMU_SECTION(0xD40, 0xD40,   0x020, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* CS4 for External I/O */
    X_ARM_MMU_SECTION(0xD60, 0xD60,   0x020, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* CS5 PSRAM */
    X_ARM_MMU_SECTION(0xD80, 0xD80,   0x100, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* EMI control/PCMCIA */
    X_ARM_MMU_SECTION(0xFFF, 0xFFF,   0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* VRAM */
}

//
// Platform specific initialization
//
static void fec_gpio_init(void)
{
        unsigned long addr , val;

        /* PF23 */
        addr = SOC_GPIOF_BASE;
        /* OCR2: AIN=0x00 */
        HAL_READ_UINT32(addr+GPIO_OCR2, val);
        val = val & (~(0x00000003<<(13)));
        HAL_WRITE_UINT32(addr+GPIO_OCR2, val);

        /* DDR: OUTPUT */
        HAL_READ_UINT32(addr+GPIO_DDIR, val);
        val = val | (0x00000001<<(23));
        HAL_WRITE_UINT32(addr+GPIO_DDIR, val);

        /* GIUS: GPIO */
        HAL_READ_UINT32(addr+GPIO_GIUS, val);
        val = val | (0x00000001<<23);
        HAL_WRITE_UINT32(addr+GPIO_GIUS, val);

        /* PD16-0*/
        addr = SOC_GPIOD_BASE;
        /* PD16 OCR2: AIN=0x00 */
        HAL_READ_UINT32(addr+GPIO_OCR2, val);
        val = val & ~0x00000003;
        HAL_WRITE_UINT32(addr+GPIO_OCR2, val);

        /* PD9, 3-0  OCR1: AIN=0x00 */
        HAL_READ_UINT32(addr+GPIO_OCR1, val);
        val = val & ~(0x000C00FF);
        HAL_WRITE_UINT32(addr+GPIO_OCR1, val);

        /* PD15~10, P7~4  ICONFIGA1: AOUT=0x00 */
        HAL_READ_UINT32(addr+GPIO_ICONFA1, val);
        val = val & ~(0xFFF0FF00);
        HAL_WRITE_UINT32(addr+GPIO_ICONFA1, val);

        /* PD8  GPR: ALT */
        HAL_READ_UINT32(addr+GPIO_GPR, val);
        val = val | 0x00000100;
        HAL_WRITE_UINT32(addr+GPIO_GPR, val);

        /* DDR: OUTPUT */
        HAL_READ_UINT32(addr+GPIO_DDIR, val);
        val = (val & 0xFFFE0000) | 0x0001020F;
        HAL_WRITE_UINT32(addr+GPIO_DDIR, val);

        /* GIUS: GPIO */
        HAL_READ_UINT32(addr+GPIO_GIUS, val);
        val = (val & 0xFFFE0000) | 0x0001FEFF;
        HAL_WRITE_UINT32(addr+GPIO_GIUS, val);

}

void fec_cpld_init(void)
{
        unsigned long addr, val;

        addr = PBC_BASE;

        //Select FEC data through data path
        val = 0x0020;
        HAL_WRITE_UINT16(addr+0x10, val);

        //Enable CPLD FEC data path
        val = 0x0010;
        HAL_WRITE_UINT16(addr+0x14, val);
}

//
// Platform specific initialization
//

unsigned int g_clock_src;
unsigned int g_board_type = BOARD_TYPE_UNKNOWN;

void plf_hardware_init(void)
{
    unsigned long val = readl(SOC_CRM_CSCR);

    if ((val & (1 << 16)) != 0) {
        if ((readw(PBC_BASE + PBC_VERSION) & CLK_INPUT_27MHZ_SET) == 0) {
            g_clock_src = FREQ_27MHZ;
        } else {
            g_clock_src = FREQ_26MHZ;
        }
    } else {
        g_clock_src = FREQ_32768HZ;
    }

    if ((BOARD_PBC_VERSION & 0x80) != 0) {
        g_board_type = BOARD_TYPE_ADS;
    } else {
        if ((BOARD_PBC_VERSION & 0x40) != 0) {
            g_board_type = BOARD_TYPE_EVB_B;
        } else {
            g_board_type = BOARD_TYPE_EVB_A;
        }
    }

    fec_gpio_init();
    fec_cpld_init();
}

static void board_raise_voltage(void)
{
    unsigned int val = 0, temp = 0;
#if defined (CLOCK_399_133_66)
    /* Increase core voltage to 1.45 */
    setCoreVoltage(0x16);
#endif

    temp = 0x9240;
    val = (1 << 31) | (REG_REGULATOR_MODE_1 << 25) | (temp & 0x00FFFFFF);
    /* Enable VCAM */
    spi_xchg_single(val, PMIC_SPI_BASE);
}

RedBoot_init(board_raise_voltage, RedBoot_INIT_PRIO(101));

#include CYGHWR_MEMORY_LAYOUT_H

typedef void code_fun(void);

void board_program_new_stack(void *func)
{
    register CYG_ADDRESS stack_ptr asm("sp");
    register CYG_ADDRESS old_stack asm("r4");
    register code_fun *new_func asm("r0");
    old_stack = stack_ptr;
    stack_ptr = CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE - sizeof(CYG_ADDRESS);
    new_func = (code_fun*)func;
    new_func();
    stack_ptr = old_stack;
}

static void display_clock_src(void)
{
    if (g_clock_src == FREQ_27MHZ) {
        diag_printf("Clock input: 27 MHz");
    } else if (g_clock_src == FREQ_26MHZ) {
        diag_printf("Clock input: 26 MHz");
    } else if (g_clock_src == FREQ_32768HZ) {
        diag_printf("Clock input: 32KHz");
    } else {
        diag_printf("Unknown clock input source. Something is wrong!");
    }
}

static void display_board_type(void)
{
    if (g_board_type == BOARD_TYPE_ADS) {
        diag_printf("\nBoard Type: ADS\n");
    } else if (g_board_type == BOARD_TYPE_EVB_A) {
        diag_printf("\nBoard Type: EVB [rev A] (external UART doesn't work)\n");
    } else if (g_board_type == BOARD_TYPE_EVB_B) {
        diag_printf("\nBoard Type: EVB [rev AA/B]\n");
    } else {
        diag_printf("\nBoard Type: Unknown val %d\n", g_board_type);
    }
}

static void display_board_info(void)
{
    display_board_type();
    display_clock_src();
}

RedBoot_init(display_board_info, RedBoot_INIT_LAST);
// ------------------------------------------------------------------------
