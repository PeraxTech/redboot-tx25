#ifndef CYGONCE_HAL_PLATFORM_SETUP_H
#define CYGONCE_HAL_PLATFORM_SETUP_H

//=============================================================================
//
//	hal_platform_setup.h
//
//	Platform specific support for HAL (assembly code)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <pkgconf/system.h>		// System-wide configuration info
#include CYGBLD_HAL_VARIANT_H		// Variant specific configuration
#include CYGBLD_HAL_PLATFORM_H		// Platform specific configuration
#include <cyg/hal/hal_soc.h>		// Variant specific hardware definitions
#include <cyg/hal/hal_mmu.h>		// MMU definitions
#include CYGBLD_HAL_PLF_DEFS_H		// Platform specific hardware definitions
#include CYGHWR_MEMORY_LAYOUT_H

#if defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1 _platform_setup1
#define CYGHWR_HAL_ARM_HAS_MMU

#ifdef CYG_HAL_STARTUP_ROMRAM
#define CYGSEM_HAL_ROM_RESET_USES_JUMP
#endif

#define TX27_NAND_PAGE_SIZE		2048
#define TX27_NAND_BLKS_PER_PAGE		64

#define CYGHWR_HAL_ROM_VADDR		0x0

#define DEBUG_LED_BIT			13
#define DEBUG_LED_PORT			GPIOF_BASE

#ifndef CYGOPT_HAL_ARM_TX27_DEBUG
#define LED_ON
#define LED_OFF
	.macro	LED_CTRL,val
	.endm
	.macro	LED_BLINK,val
	.endm
	.macro	DELAY,val
	.endm
#else
#define CYGHWR_LED_MACRO	LED_BLINK #\x
#define LED_ON			bl	led_on
#define LED_OFF			bl	led_off

	.macro	DELAY,ms
	ldr	r10, =\ms
111:
	subs	r10, r10, #1
	bmi	113f
	mov	r9, #3600
112:
	subs	r9, r9, #1
	bne	112b
113:
	.endm

	.macro LED_CTRL,val
	// switch user LED (PF13) on STK5
	ldr	r10, DEBUG_LED_PORT
	// PTF_DR
	mov	r9, \val
	cmp	r9, #0
	ldr	r9, [r10, #GPIO_DR]
	orrne	r9, #(1 << DEBUG_LED_BIT)	// LED ON
	biceq	r9, #(1 << DEBUG_LED_BIT)	// LED OFF
	str	r9, [r10, #GPIO_DR]
	.endm

	.macro LED_BLINK,val
	mov	r3, \val
211:
	subs	r3, r3, #1
	bmi	212f
	LED_CTRL #1
	DELAY	200
	LED_CTRL #0
	DELAY	300
	b	211b
212:
	DELAY	1000
	.endm
#endif

	.macro LED_INIT
	// initialize GPIO PF13 for LED on STK5
	ldr	r10, DEBUG_LED_PORT
	// PTF_DR
	ldr	r9, [r10, #GPIO_DR]
	bic	r9, #(1 << DEBUG_LED_BIT)
	str	r9, [r10, #GPIO_DR]
	// PTF_OCR1
	ldr	r9, [r10, #GPIO_OCR1]
	orr	r9, #(3 << (2 * (DEBUG_LED_BIT % 16)))
	str	r9, [r10, #GPIO_OCR1]
	// PTF_GIUS
	ldr	r9, [r10, #GPIO_GIUS]
	orr	r9, r9, #(1 << DEBUG_LED_BIT)
	str	r9, [r10, #GPIO_GIUS]
	// PTF_DDIR
	ldr	r9, [r10, #GPIO_DDIR]
	orr	r9, #(1 << DEBUG_LED_BIT)
	str	r9, [r10, #GPIO_DDIR]
	.endm

	.macro	WDOG_RESET
	ldr	r10, SOC_CRM_BASE_W
	ldr	r9, [r10, #(SOC_CRM_CSCR - SOC_CRM_BASE)]
	orr	r9, r9, #(1 << 2)	/* enable FPM */
	str	r9, [r10, #(SOC_CRM_CSCR - SOC_CRM_BASE)]

	ldr	r9, [r10, #(SOC_CRM_PCCR1 - SOC_CRM_BASE)]
	orr	r9, r9, #(1 << 24)	/* enable WDT clock */
	str	r9, [r10, #(SOC_CRM_PCCR1 - SOC_CRM_BASE)]

	/* Wait for clocks to be enabled */
	mov	r10, #0x1000
111:
	subs	r10, r10, #1
	bmi	113f
	mov	r9, #3600
112:
	subs	r9, r9, #1
	bne	112b
113:
	ldr	r10, WDOG_BASE
	/* enable watchdog timeout */
	mov	r9, #0x0034
	strh	r9, [r10, #0]
	/* wait for watchdog to trigger */
	b	.
	.endm
// This macro represents the initial startup code for the platform
// r11 is reserved to contain chip rev info in this file
	.macro	_platform_setup1
KARO_TX27_SETUP_START:
	// invalidate I/D cache/TLB
	mov	r0, #0
	mcr	15, 0, r0, c7, c7, 0	/* invalidate I cache and D cache */
	mcr	15, 0, r0, c8, c7, 0	/* invalidate TLBs */
	mcr	15, 0, r0, c7, c10, 4	/* Data Write Barrier */

init_aipi_start:
	init_aipi

	/* configure GPIO PB22 (OSC26M enable) as output high */
	ldr	r10, GPIOB_BASE
	// PTB_OCR1
	ldr	r9, [r10, #GPIO_OCR2]
	orr	r9, #(3 << (2 * (22 % 16)))
	str	r9, [r10, #GPIO_OCR2]
	// PTB_DR
	ldr	r9, [r10, #GPIO_DR]
	orr	r9, r9, #(1 << 22)
	str	r9, [r10, #GPIO_DR]
	// PTB_GIUS
	ldr	r9, [r10, #GPIO_GIUS]
	orr	r9, r9, #(1 << 22)
	str	r9, [r10, #GPIO_GIUS]
	// PTB_DDIR
	ldr	r9, [r10, #GPIO_DDIR]
	orr	r9, #(1 << 22)
	str	r9, [r10, #GPIO_DDIR]

	LED_INIT

	// setup System Controls
	ldr	r0, SOC_SYSCTRL_BASE_W
	mov	r1, #0x03
	str	r1, [r0, #(SOC_SYSCTRL_PCSR - SOC_SYSCTRL_BASE)]
	// select 2kpage NAND (NF_FMS), CSD0, CS3
	mvn	r1, #(FMCR_SDCS1_SEL | FMCR_NF_16BIT | FMCR_SLCDC_SEL)
	str	r1, [r0, #(SOC_SYSCTRL_FMCR - SOC_SYSCTRL_BASE)]

init_max_start:
	init_max
init_drive_strength_start:
@	init_drive_strength

	// check if sdram has been setup
	cmp	pc, #SDRAM_BASE_ADDR
	blo	init_clock_start
	cmp	pc, #(SDRAM_BASE_ADDR + SDRAM_SIZE)
	blo	HWInitialise_skip_SDRAM_setup

init_clock_start:
	init_clock
init_sdram_start:
	LED_BLINK #1
	setup_sdram_ddr
	LED_BLINK #2
HWInitialise_skip_SDRAM_setup:
	ldr	r0, NFC_BASE_W
	add	r2, r0, #0x800		// 2K window
	cmp	pc, r0
	blo	Normal_Boot_Continue
	cmp	pc, r2
	bhi	Normal_Boot_Continue

NAND_Boot_Start:
	/* Copy image from flash to SDRAM first */
	ldr	r1, MXC_REDBOOT_RAM_START
1:
	ldmia	r0!, {r3-r10}
	stmia	r1!, {r3-r10}
	cmp	r0, r2
	blo	1b

	LED_ON
	ldr	r1, MXC_REDBOOT_RAM_START
	ldr	r0, NFC_BASE_W
2:
	ldr	r3, [r1], #4
	ldr	r4, [r0], #4
	cmp	r3, r4
	bne	3f

	cmp	r0, r2
	blo	2b
	LED_OFF
	b	4f
3:
#ifdef CYGOPT_HAL_ARM_TX27_DEBUG
	LED_BLINK #3
#else
	WDOG_RESET
#endif
	b	3b
4:
	LED_ON
	/* Jump to SDRAM */
	bl	jump_to_sdram
/* Code and all data used up to here must fit within the first 2KiB of FLASH ROM! */
Now_in_SDRAM:
	LED_OFF
NAND_Copy_Main:
	ldr	r0, NFC_BASE_W	//r0: nfc base. Reloaded after each page copying
	mov	r1, #TX27_NAND_PAGE_SIZE //r1: starting flash addr to be copied. Updated constantly
	add	r2, r0, #TX27_NAND_PAGE_SIZE //r2: end of 1st RAM buf. Doesn't change
	add	r4, r0, #0xE00	//r4: NFC register base. Doesn't change
	ldr	r5, MXC_REDBOOT_RAM_START
	add	r6, r5, #REDBOOT_IMAGE_SIZE //r6: end of SDRAM address for copying. Doesn't change
	add	r5, r5, r1	//r5: starting SDRAM address for copying. Updated constantly

	// enable ECC, disable interrupts
	mov	r3, #(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN)
	strh	r3, [r4, #NAND_FLASH_CONFIG1_REG_OFF]

	// unlock internal buffer
	mov	r3, #0x2
	strh	r3, [r4, #0xA]

Nfc_Read_Page:
	LED_ON
	mov	r8, #0
	strh	r8, [r4, #RAM_BUFFER_ADDRESS_REG_OFF]

	mov	r3, #0x0
	bl	nfc_cmd_input

	LED_ON
	mov	r3, #0
	bl	nfc_addr_input
	LED_ON
	mov	r3, #0
	bl	nfc_addr_input	//2nd addr cycle
	LED_ON
	mov	r3, r1, lsr #11
	bl	nfc_addr_input	//3rd addr cycle
	LED_ON
	mov	r3, r1, lsr #19
	bl	nfc_addr_input	//4th addr cycle

	LED_ON
	mov	r3, #0x30
	bl	nfc_cmd_input

	LED_ON
	bl	nfc_data_output
	LED_ON
	strh	r8, [r4, #RAM_BUFFER_ADDRESS_REG_OFF]
	bl	nfc_data_output
	LED_ON
	strh	r8, [r4, #RAM_BUFFER_ADDRESS_REG_OFF]
	bl	nfc_data_output
	LED_ON
	strh	r8, [r4, #RAM_BUFFER_ADDRESS_REG_OFF]
	bl	nfc_data_output

	// check for bad block
	mov	r3, r1, lsl #(32-17)	// get rid of block number
	cmp	r3, #(TX27_NAND_PAGE_SIZE << (32-17)) // check if not first or second page in block
	bhi	Copy_Good_Blk

	add	r9, r0, #TX27_NAND_PAGE_SIZE		//r3 -> spare area buf 0
	ldrh	r9, [r9, #0x4]
	and	r9, r9, #0xFF00
	cmp	r9, #0xFF00
	beq	Copy_Good_Blk
	// really sucks. Bad block!!!!
	cmp	r3, #0x0
	beq	Skip_bad_block
	// even suckier since we already read the first page!
	sub	r5, r5, #TX27_NAND_PAGE_SIZE	//rewind 1 page for the sdram pointer
	sub	r1, r1, #TX27_NAND_PAGE_SIZE		//rewind 1 page for the flash pointer
Skip_bad_block:
#ifdef CYGOPT_HAL_ARM_TX27_DEBUG
	LED_BLINK #1
	b	Skip_bad_block
#endif
	add	r1, r1, #(TX27_NAND_BLKS_PER_PAGE * TX27_NAND_PAGE_SIZE)
	b	Nfc_Read_Page

Copy_Good_Blk:
	// copying page
1:
	ldmia	r0!, {r7-r14}
	stmia	r5!, {r7-r14}
	cmp	r0, r2
	blo	1b

	LED_ON
	LED_OFF
	cmp	r5, r6
	bge	NAND_Copy_Main_done

	add	r1, r1, #TX27_NAND_PAGE_SIZE
	ldr	r0, NFC_BASE_W
	b	Nfc_Read_Page
NAND_Copy_Main_done:

Normal_Boot_Continue:
	init_cs0_sync

NAND_ClockSetup:
	ldr	r1, SOC_CRM_BASE_W
	ldr	r2, [r1, #(SOC_CRM_PCDR0 - SOC_CRM_BASE)]
	bic	r2, r2, #0x0200
	orr	r2, r2, #0x01C0
	str	r2, [r1, #(SOC_CRM_PCDR0 - SOC_CRM_BASE)]

/* end of NAND clock divider setup */

	// TLSbo76381: enable USB/PP/DMA burst override bits in GPCR
	ldr	r1, =SOC_SYSCTRL_GPCR
	ldr	r2, [r1]
	orr	r2, r2, #0x700
	str	r2, [r1]

	// Set up a stack [for calling C code]
	ldr	sp, =__startup_stack

	LED_ON
	// Create MMU tables
	bl	hal_mmu_init
	LED_OFF

	// Enable MMU
	ldr	r2, =10f
	mrc	MMU_CP, 0, r1, MMU_Control, c0		// get c1 value to r1 first
	orr	r1, r1, #7				// enable MMU bit
	mcr	MMU_CP, 0, r1, MMU_Control, c0
	b	9f
	.align	5
9:
	mov	pc, r2	/* Change address spaces */
10:
	nop
	.endm			// _platform_setup1

#else // defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1
#endif

	.macro init_clock
	ldr	r0, SOC_CRM_BASE_W
	// disable PLL update first
	ldr	r1, [r0, #(SOC_CRM_CSCR - SOC_CRM_BASE)]
	/* clear ARM_SRC & ARM_DIV required as workaround for ENGcm12387 */
	bic	r1, r1, #((1 << 15) | (3 << 12))
	orr	r1, r1, #(1 << 31)
#ifdef PLL_REF_CLK_32768HZ
	orr	r1, r1, #(1 << 3)	// disable 26MHz osc
#else
	bic	r1, r1, #(1 << 3)	// enable 26MHz osc
#endif
	str	r1, [r0, #(SOC_CRM_CSCR - SOC_CRM_BASE)]

	// configure MPCTL0
	ldr	r1, CRM_MPCTL0_VAL2_W
	str	r1, [r0, #(SOC_CRM_MPCTL0 - SOC_CRM_BASE)]

	// configure SPCTL0
	ldr	r1, CRM_SPCTL0_VAL2_W
	str	r1, [r0, #(SOC_CRM_SPCTL0 - SOC_CRM_BASE)]

	ldr	r1, [r0, #(SOC_CRM_CSCR - SOC_CRM_BASE)]
#ifdef PLL_REF_CLK_32768HZ
	// Make sure to use CKIL
	bic	r1, r1, #(3 << 16)
#else
	orr	r1, r1, #(3 << 16)	// select 26MHz
#endif
	orr	r1, r1, #(0x3 << 18)	// SPLL_RESTART | MPLL_RESTART
	orr	r1, r1, #(0x3 << 0)	// SPLL_ENABLE | MPLL_ENABLE
	str	r1, [r0, #(SOC_CRM_CSCR - SOC_CRM_BASE)]
1:
	ldr	r1, [r0, #(SOC_CRM_CSCR - SOC_CRM_BASE)]
	tst	r1, #(0x3 << 18)	// wait for SPLL/MPLL restart to clear
	bne	1b

	ldr	r1, SOC_CRM_CSCR2_W
	str	r1, [r0, #(SOC_CRM_CSCR - SOC_CRM_BASE)]

	// Set divider of H264_CLK to zero, NFC to 3.
	ldr	r1, SOC_CRM_PCDR0_W
	str	r1, [r0, #(SOC_CRM_PCDR0 - SOC_CRM_BASE)]

	/* Configure PCDR1 */
	ldr	r1, SOC_CRM_PCDR1_W
	str	r1, [r0, #(SOC_CRM_PCDR1 - SOC_CRM_BASE)]

	// Configure PCCR0 and PCCR1
	ldr	r1, SOC_CRM_PCCR0_W
	str	r1, [r0, #(SOC_CRM_PCCR0 - SOC_CRM_BASE)]

	ldr	r1, [r0, #(SOC_CRM_PCCR1 - SOC_CRM_BASE)]
	orr	r1, r1, #0x0780
	str	r1, [r0, #(SOC_CRM_PCCR1 - SOC_CRM_BASE)]
	.endm //init_clock

	.macro init_cs0
	ldr	r1, SOC_CS0_CTL_BASE_W
	ldr	r2, CS0_CSCRU_VAL
	str	r2, [r1, #CSCRU_OFFSET]
	ldr	r2, CS0_CSCRL_VAL
	str	r2, [r1, #CSCRL_OFFSET]
	ldr	r2, CS0_CSCRA_VAL
	str	r2, [r1, #CSCRA_OFFSET]
	.endm // init_cs0

	/* CS0 sync mode setup */
	.macro init_cs0_sync
	/*
	* Sync mode (AHB Clk = 133MHz ; BCLK = 44.3MHz):
	*/
	ldr	r0, =SOC_CS0_CTL_BASE
	ldr	r1, CS0_CSCRU_SYNC_VAL
	str	r1, [r0, #CSCRU_OFFSET]
	ldr	r1, CS0_CSCRL_SYNC_VAL
	str	r1, [r0, #CSCRL_OFFSET]
	ldr	r1, CS0_CSCRA_SYNC_VAL
	str	r1, [r0, #CSCRA_OFFSET]
	.endm /* init_cs0_sync */

	.macro init_cs4	/* ADS board expanded IOs */
	ldr	r1, SOC_CS4_CTL_BASE_W
	ldr	r2, CS4_CSCRU_VAL
	str	r2, [r1, #CSCRU_OFFSET]
	ldr	r2, CS4_CSCRL_VAL
	str	r2, [r1, #CSCRL_OFFSET]
	ldr	r2, CS4_CSCRA_VAL
	str	r2, [r1, #CSCRA_OFFSET]
	.endm	/* init_cs4 */

	.macro init_aipi
	// setup AIPI1 and AIPI2
	mov	r0, #SOC_AIPI1_BASE
	ldr	r1, AIPI1_PSR0_VAL
	str	r1, [r0]  /* PSR0 */
	ldr	r2, AIPI1_PSR1_VAL
	str	r2, [r0, #4]  /* PSR1 */
	// set r0 = AIPI2 base
	add	r0, r0, #0x20000
	mov	r1, #0x0
	str	r1, [r0]  /* PSR0 */
	mvn	r2, #0
	str	r2, [r0, #4]  /* PSR1 */
	.endm // init_aipi

	.macro init_max
	ldr	r0, SOC_MAX_BASE_W
	add	r1, r0, #MAX_SLAVE_PORT1_OFFSET
	add	r2, r0, #MAX_SLAVE_PORT2_OFFSET
	add	r0, r0, #MAX_SLAVE_PORT0_OFFSET

	/* MPR and AMPR */
	ldr	r6, SOC_MAX_MPR_VAL	/* Priority SLCD>EMMA>DMA>Codec>DAHB>IAHB */
	str	r6, [r0, #MAX_SLAVE_MPR_OFFSET]	  /* same for all slave ports */
	str	r6, [r0, #MAX_SLAVE_AMPR_OFFSET]
	str	r6, [r1, #MAX_SLAVE_MPR_OFFSET]
	str	r6, [r1, #MAX_SLAVE_AMPR_OFFSET]
	str	r6, [r2, #MAX_SLAVE_MPR_OFFSET]
	str	r6, [r2, #MAX_SLAVE_AMPR_OFFSET]
	.endm //init_max

	.macro init_drive_strength
	ldr	r0, SOC_SYSCTRL_BASE_W
	ldr	r1, DS_DSCR_VAL
	str	r1, [r0, #(SOC_SYSCTRL_DSCR3 - SOC_SYSCTRL_BASE)]
	str	r1, [r0, #(SOC_SYSCTRL_DSCR5 - SOC_SYSCTRL_BASE)]
	str	r1, [r0, #(SOC_SYSCTRL_DSCR6 - SOC_SYSCTRL_BASE)]
	ldr	r1, DS_DSCR7_VAL
	str	r1, [r0, #(SOC_SYSCTRL_DSCR7 - SOC_SYSCTRL_BASE)]
	ldr	r1, DS_DSCR8_VAL
	str	r1, [r0, #(SOC_SYSCTRL_DSCR8 - SOC_SYSCTRL_BASE)]
	.endm	// init_drive_strength

	.macro setup_sdram_ddr
	// SDRAM controller base address
	ldr	r0, SOC_ESDCTL_BASE_W
	// base address of SDRAM for SET MODE commands written to SDRAM via address lines
	mov	r2, #SOC_CSD0_BASE

	mov	r1, #(1 << 1)		// SDRAM controller reset
	str	r1, [r0, #ESDCTL_ESDMISC]
1:
	// wait until SDRAMRDY bit is set indicating SDRAM is usable
	ldr	r1, [r0, #ESDCTL_ESDMISC]
	tst	r1, #(1 << 31)
	beq	1b

	mov	r1, #(1 << 3)		@ delay line soft reset
	str	r1, [r0, #ESDCTL_ESDMISC]
1:
	// wait until SDRAMRDY bit is set indicating SDRAM is usable
	ldr	r1, [r0, #ESDCTL_ESDMISC]
	tst	r1, #(1 << 31)
	beq	1b

	mov	r1, #(1 << 2)		@ enable DDR pipeline
	str	r1, [r0, #ESDCTL_ESDMISC]

	ldr	r1, SDRAM_ESDCFG0_VAL
	str	r1, [r0, #ESDCTL_ESDCFG0]

	ldr	r1, SDRAM_DLY_VAL
	str	r1, [r0, #ESDCTL_ESDCDLY1]
	str	r1, [r0, #ESDCTL_ESDCDLY2]
	str	r1, [r0, #ESDCTL_ESDCDLY3]
	str	r1, [r0, #ESDCTL_ESDCDLY4]
	str	r1, [r0, #ESDCTL_ESDCDLY5]

	ldr	r1, SDRAM_PRE_ALL_CMD
	str	r1, [r0, #ESDCTL_ESDCTL0]

	str	r1, [r2, #(1 << 10)]	@ contents of r1 irrelevant, data written via A0-A11

	ldr	r1, SDRAM_AUTO_REF_CMD
	str	r1, [r0, #ESDCTL_ESDCTL0]
	@ initiate 2 auto refresh cycles
	.rept 2
	str	r1, [r2]
	.endr

	ldr	r1, SDRAM_SET_MODE_REG_CMD
	str	r1, [r0, #ESDCTL_ESDCTL0]

	@ address offset for extended mode register
	add	r3, r2, #(2 << 24)
	@ select drive strength via extended mode register:
	@ 0=full 1=half 2=quarter 3=3-quarter
	ldrb	r1, [r3, #(0 << 5)]

	ldrb	r1, [r2, #0x033]	@ write to SDRAM MODE register (via A0-A12)

	ldr	r1, SDRAM_NORMAL_MODE
	str	r1, [r0, #ESDCTL_ESDCTL0]

	str	r1, [r2]
	mov	r1, #((1 << 3) | (1 << 2) | (1 << 5))
	str	r1, [r0, #ESDCTL_ESDMISC]
	.endm	// setup_sdram_ddr

#ifdef CYGOPT_HAL_ARM_TX27_DEBUG
led_on:
	ldr	r10, GPIOF_BASE
	// PTF_DR
	mov	r9, #(1 << 13)	// LED ON
	str	r9, [r10, #GPIO_DR]
	mov	pc, lr

led_off:
	ldr	r10, GPIOF_BASE
	// PTF_DR
	mov	r9, #0		// LED OFF
	str	r9, [r10, #GPIO_DR]
	mov	pc, lr
#endif

nfc_cmd_input:
	strh	r3, [r4, #NAND_FLASH_CMD_REG_OFF]
	mov	r3, #NAND_FLASH_CONFIG2_FCMD_EN
	strh	r3, [r4, #NAND_FLASH_CONFIG2_REG_OFF]
	b	nfc_wait_op_done

nfc_addr_input:
	and	r3, r3, #0xFF
	strh	r3, [r4, #NAND_FLASH_ADD_REG_OFF]
	mov	r3, #NAND_FLASH_CONFIG2_FADD_EN
	strh	r3, [r4, #NAND_FLASH_CONFIG2_REG_OFF]
	b	nfc_wait_op_done

nfc_data_output:
	mov	r3, #FDO_PAGE_SPARE_VAL
	strh	r3, [r4, #NAND_FLASH_CONFIG2_REG_OFF]
	add	r8, r8, #1
	b	nfc_wait_op_done

nfc_wait_op_done:
311:
	ldrh	r3, [r4, #NAND_FLASH_CONFIG2_REG_OFF]
	ands	r3, r3, #NAND_FLASH_CONFIG2_INT_DONE
	movne	r3, #0x0
	strneh	r3, [r4, #NAND_FLASH_CONFIG2_REG_OFF]
	movne	pc, lr
	b	311b

jump_to_sdram:
	b	1f
	.align	5
1:
	mcr	15, 0, r0, c7, c7, 0	/* invalidate I cache and D cache */
	mcr	15, 0, r0, c8, c7, 0	/* invalidate TLBs */
	mcr	15, 0, r0, c7, c10, 4	/* Data Write Barrier */
	ldr	r0, SDRAM_ADDR_MASK
	ldr	r1, MXC_REDBOOT_RAM_START
	and	r0, lr, r0
	sub	r0, r1, r0
	add	lr, lr, r0
	mov	pc, lr

#define PLATFORM_VECTORS	 _platform_vectors
	.macro  _platform_vectors
	.globl	_KARO_MAGIC
_KARO_MAGIC:
	.ascii	"KARO_CE6"
	.globl	_KARO_STRUCT_SIZE
_KARO_STRUCT_SIZE:
	.word	0	// reserve space structure length

	.globl	_KARO_CECFG_START
_KARO_CECFG_START:
	.rept	1024/4
	.word	0	// reserve space for CE configuration
	.endr

	.globl	_KARO_CECFG_END
_KARO_CECFG_END:
	.endm

	.align	5
	.ascii	"KARO TX27 " __DATE__ " " __TIME__
	.align

/* SDRAM configuration */
#define RA_BITS		2	/* row addr bits - 11 */
#define CA_BITS		(SDRAM_SIZE / SZ_64M)	/* 0-2: col addr bits - 8 3: rsrvd */
#define DSIZ		2	/* 0: D[31..16] 1: D[15..D0] 2: D[31..0] 3: rsrvd */
#define SREFR		3	/* 0: disabled 1-5: 2^n rows/clock *: rsrvd */
#define PWDT		1	/* 0: disabled 1: precharge pwdn
				   2: pwdn after 64 clocks 3: pwdn after 128 clocks */
#define FP		0	/* 0: not full page 1: full page */
#define BL		1	/* 0: 4(not for LPDDR) 1: 8 */
#define PRCT		5	/* 0: disabled *: clks / 2 (0..63) */
#define ESDCTLVAL	(0x80000000 | (RA_BITS << 24) | (CA_BITS << 20) |		\
			 (DSIZ << 16) | (SREFR << 13) | (PWDT << 10) | (FP << 8) |	\
			 (BL << 7) | (PRCT << 0))

/* SDRAM timing definitions */
#define SDRAM_CLK	133
#define NS_TO_CK(ns)	(((ns) * SDRAM_CLK + 999) / 1000)

	.macro		CK_VAL,	name, clks, offs
	.iflt		\clks - \offs
	.set		\name, 0
	.else
	.ifle		\clks - 16
	.set		\name, \clks - \offs
	.else
	.set		\name, 0
	.endif
	.endif
	.endm

	.macro		NS_VAL,	name, ns, offs
	.iflt		\ns - \offs
	.set		\name, 0
	.else
	CK_VAL		\name, NS_TO_CK(\ns), \offs
	.endif
	.endm

#if SDRAM_SIZE <= SZ_64M
/* MT46H16M32LF-75 */
CK_VAL	tXP,	2, 1	/* clks - 1 (0..7)  */
CK_VAL	tWTR,	2, 1	/* clks - 1 (0..1)  */
NS_VAL	tRP,	23, 2	/* clks - 2 (0..3)  */
CK_VAL	tMRD,	2, 1	/* clks - 1 (0..3)  */
NS_VAL	tWR,	15, 2	/* clks - 2 (0..1)  */
NS_VAL	tRAS,	45, 1	/* clks - 1 (0..15) */
CK_VAL	tCAS,	3, 0	/* clks - 1 (0..3)  */
NS_VAL	tRRD,	15, 1	/* clks - 1 (0..3)  */
NS_VAL	tRCD,	23, 1	/* clks - 1 (0..7) */
/* tRC is actually max(tRC,tRFC,tXSR) */
NS_VAL	tRC,	120, 1	/* 0: 20 *: clks - 1 (0..15) */
#else
/* MT46H32M32LF-6 or -75 */
NS_VAL	tXP,	25, 1	/* clks - 1 (0..7)  */
CK_VAL	tWTR,	1, 1	/* clks - 1 (0..1)  */
NS_VAL	tRP,	23, 2	/* clks - 2 (0..3)  */
CK_VAL	tMRD,	2, 1	/* clks - 1 (0..3)  */
NS_VAL	tWR,	15, 2	/* clks - 2 (0..1)  */
NS_VAL	tRAS,	45, 1	/* clks - 1 (0..15) */
CK_VAL	tCAS,	3, 0	/* clks - 1 (0..3)  */
NS_VAL	tRRD,	15, 1	/* clks - 1 (0..3)  */
NS_VAL	tRCD,	23, 1	/* clks - 1 (0..7) */
NS_VAL	tRC,	138, 1	/* 0: 20 *: clks - 1 (0..15) */
#endif

#define ESDCFGVAL	((tXP << 21) | (tWTR << 20) | (tRP << 18) | (tMRD << 16) |	\
			 (tWR << 15) | (tRAS << 12) | (tRRD << 10) | (tCAS << 8) |	\
			 (tRCD << 4) | (tRC << 0))

// All these constants need to be in the first 2KiB of FLASH
WDOG_BASE:			.word	WDOG_BASE_ADDR
GPIOB_BASE:			.word	0x10015100
GPIOF_BASE:			.word	0x10015500
SDRAM_ADDR_MASK:		.word	0xffff0000
MXC_REDBOOT_RAM_START:		.word	SDRAM_BASE_ADDR + SDRAM_SIZE - REDBOOT_OFFSET
SOC_SYSCTRL_BASE_W:		.word	SOC_SYSCTRL_BASE
SOC_MAX_BASE_W:			.word	SOC_MAX_BASE
SOC_MAX_MPR_VAL:		.word	0x00302145
SOC_CRM_BASE_W:			.word	SOC_CRM_BASE
CRM_MPCTL0_VAL2_W:		.word	CRM_MPCTL0_VAL2
CRM_SPCTL0_VAL2_W:		.word	CRM_SPCTL0_VAL2

#define AHBDIV			(MPLL_REF_CLK_kHz * 2 / 3 / 1000 / CYGHWR_HAL_ARM_SOC_SYSTEM_CLOCK)

#define CSCR_AHB_DIV(n)		((((n) & 3) - 1) << 8)
#define CSCR_ARM_DIV(n)		((((n) & 3) - 1) << 12)
#define CSCR_ARM_SRC(n)		((!!(n)) << 15)
#define CSCR_MCU_SEL(n)		((!!(n)) << 16)
#define CSCR_SP_SEL(n)		((!!(n)) << 17)
#define CSCR_USB_DIV(n)		((((n) & 7) - 1) << 28)

#define MPLL_CLK_DIV(khz)	((MPLL_REF_CLK_kHz * 2 / 3 + (khz) - 1) / (khz) - 1)
#define MPLL_CLK_DIV2(khz)	((MPLL_REF_CLK_kHz * 4 / 3 + (khz) - 1) / (khz) - 4)
#define SPLL_CLK_DIV(khz)	((SPLL_REF_CLK_kHz + (khz) - 1) / (khz) - 1)
#define SPLL_CLK_DIV2(khz)	((SPLL_REF_CLK_kHz * 2 + (khz) - 1) / (khz) - 4)

#define PCDR0_SSI2_DIV(pll, n)	((pll##_CLK_DIV2(n) & 0x3f) << 26)
#define PCDR0_CLKO_DIV(n)	((((n) - 1) & 0x7) << 22)
#define PCDR0_SSI1_DIV(pll, n)	((pll##_CLK_DIV2(n) & 0x3f) << 16)
#define PCDR0_H264_DIV(pll, n)	((pll##_CLK_DIV2(n) & 0x3f) << 10)
#define PCDR0_NFC_DIV(n)	((MPLL_CLK_DIV(n) & 0xf) << 6)
#define PCDR0_MSHC_DIV(pll, n)	((pll##_CLK_DIV(n) & 0x3f) << 0)
#define PCDR0_CLKO_EN		(1 << 25)

#define PCDR1_PER1_DIV(n)	((MPLL##_CLK_DIV(n) & 0x3f) << 0)
#define PCDR1_PER2_DIV(n)	((MPLL##_CLK_DIV(n) & 0x3f) << 8)
#define PCDR1_PER3_DIV(n)	((MPLL##_CLK_DIV(n) & 0x3f) << 16)
#define PCDR1_PER4_DIV(n)	((MPLL##_CLK_DIV(n) & 0x3f) << 24)

#ifndef PLL_REF_CLK_32768HZ
#define MPLL_SRC		(1 << 16)
#define SPLL_SRC		(1 << 17)
#define FPM_ENABLE		(1 << 2)
#else
#define MPLL_SRC		(0 << 16)
#define SPLL_SRC		(0 << 17)
#define FPM_ENABLE		(1 << 2)
#endif

SOC_CRM_CSCR2_W:		.word	0x03f00003 | \
					FPM_ENABLE | MPLL_SRC | SPLL_SRC | \
					CSCR_AHB_DIV(AHBDIV) | \
					CSCR_ARM_DIV(1) | \
					CSCR_USB_DIV(4) | \
					CSCR_ARM_SRC(MPLL_REF_CLK_kHz / 1000 == CYGHWR_HAL_ARM_SOC_PROCESSOR_CLOCK)
SOC_CRM_PCDR0_W:		.word	PCDR0_SSI2_DIV(MPLL, 66500) | \
					PCDR0_CLKO_DIV(8) | PCDR0_CLKO_EN | \
					PCDR0_SSI1_DIV(MPLL, 66500) | \
					PCDR0_H264_DIV(MPLL, 133000) | \
					PCDR0_NFC_DIV(16625) | \
					PCDR0_MSHC_DIV(MPLL, 66500)
SOC_CRM_PCDR1_W:		.word	PCDR1_PER1_DIV(13300) | \
					PCDR1_PER2_DIV(26600) | \
					PCDR1_PER3_DIV(66500) | \
					PCDR1_PER4_DIV(26600)
SOC_CRM_PCCR0_W:		.word	0x3108480F
SOC_CS4_CTL_BASE_W:		.word	SOC_CS4_CTL_BASE
CS4_CSCRU_VAL:			.word	0x0000DCF6
CS4_CSCRL_VAL:			.word	0x444A4541
CS4_CSCRA_VAL:			.word	0x44443302
NFC_BASE_W:			.word	NFC_BASE
SOC_ESDCTL_BASE_W:		.word	SOC_ESDCTL_BASE
SDRAM_ESDCFG0_VAL:		.word	ESDCFGVAL
SDRAM_DLY_VAL:			.word	0x002c0000
SDRAM_PRE_ALL_CMD:		.word	0x92120000
SDRAM_AUTO_REF_CMD:		.word	0xA2120000
SDRAM_SET_MODE_REG_CMD:		.word	0xB2120000
SDRAM_NORMAL_MODE:		.word	ESDCTLVAL
CS0_CSCRU_VAL:			.word	0x0000CC03
CS0_CSCRL_VAL:			.word	0xA0330D01
CS0_CSCRA_VAL:			.word	0x00220800
CS0_CSCRU_SYNC_VAL:		.word	0x23524E80
CS0_CSCRL_SYNC_VAL:		.word	0x10000D03
CS0_CSCRA_SYNC_VAL:		.word	0x00720900
CS0_BASE_ADDR_W:		.word	CS0_BASE_ADDR
SOC_CS0_CTL_BASE_W:		.word	SOC_CS0_CTL_BASE
DS_DSCR_VAL:			.word   0x55555555
DS_DSCR7_VAL:			.word   0x00005005
DS_DSCR8_VAL:			.word   0x15555555
AIPI1_PSR0_VAL:			.word   0x20040304
AIPI1_PSR1_VAL:			.word   0xDFFBFCFB

/*----------------------------------------------------------------------*/
/* end of hal_platform_setup.h						*/
#endif /* CYGONCE_HAL_PLATFORM_SETUP_H */
