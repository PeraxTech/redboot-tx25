// eCos memory layout - Fri Oct 20 05:56:55 2000

//#define REDBOOT_BOTTOM

// This is a generated file - do not edit
#include <pkgconf/system.h>
#include CYGBLD_HAL_VARIANT_H
#include CYGBLD_HAL_PLATFORM_H
#include <cyg/hal/hal_soc.h>	    // Hardware definitions

#define SDRAM_BASE_ADDR		SOC_CSD0_BASE
#define SDRAM_SIZE		CYGNUM_HAL_ARM_TX27_SDRAM_SIZE

#define REDBOOT_IMAGE_SIZE	0x00040000

#ifndef REDBOOT_BOTTOM
#define REDBOOT_OFFSET		REDBOOT_IMAGE_SIZE
#define CYGMEM_REGION_ram	SDRAM_BASE_ADDR
#define CYGMEM_REGION_rom	(CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE)
#else
#define REDBOOT_OFFSET		0x00100000
#define CYGMEM_REGION_ram	(SDRAM_BASE_ADDR + REDBOOT_OFFSET)
#define CYGMEM_REGION_rom	SDRAM_BASE_ADDR
#endif

#define CYGMEM_REGION_ram_SIZE	(SDRAM_SIZE - REDBOOT_OFFSET)
#define CYGMEM_REGION_ram_ATTR	(CYGMEM_REGION_ATTR_R | CYGMEM_REGION_ATTR_W)

#define CYGMEM_REGION_rom_SIZE	REDBOOT_OFFSET
#define CYGMEM_REGION_rom_ATTR	CYGMEM_REGION_ATTR_R
#ifndef __ASSEMBLER__
extern char CYG_LABEL_NAME(__heap1)[];
#endif
#define CYGMEM_SECTION_heap1	(CYG_LABEL_NAME(__heap1))
#define CYGMEM_SECTION_heap1_SIZE (CYGMEM_REGION_rom - (size_t)CYG_LABEL_NAME(__heap1))
