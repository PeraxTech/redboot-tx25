//==========================================================================
//
//	tx27_misc.c
//
//	HAL misc board support code for the tx27
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <stdlib.h>
#include <redboot.h>
#include <string.h>
#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>		// base types
#include <cyg/infra/cyg_trac.h>		// tracing macros
#include <cyg/infra/cyg_ass.h>		// assertion macros

#include <cyg/hal/hal_io.h>		// IO macros
#include <cyg/hal/hal_arch.h>		// Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>		// Interrupt names
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_soc.h>		// Hardware definitions
#include CYGBLD_HAL_PLF_DEFS_H		// Platform specifics

#include <cyg/infra/diag.h>		// diag_printf

// All the MM table layout is here:
#include <cyg/hal/hal_mm.h>

void hal_mmu_init(void)
{
	unsigned long ttb_base = RAM_BANK0_BASE + 0x4000;
	unsigned long i;

	/*
	 * Set the TTB register
	 */
	asm volatile ("mcr  p15,0,%0,c2,c0,0" : : "r"(ttb_base) /*:*/);

	/*
	 * Set the Domain Access Control Register
	 */
	i = ARM_ACCESS_DACR_DEFAULT;
	asm volatile ("mcr  p15,0,%0,c3,c0,0" : : "r"(i) /*:*/);

	/*
	 * First clear all TT entries - ie Set them to Faulting
	 */
	memset((void *)ttb_base, 0, ARM_FIRST_LEVEL_PAGE_TABLE_SIZE);

	/*	       Physical	 Virtual Size	Attributes						      Function	 */
	/*	       Base	 Base	  MB	 cached?	   buffered?	    access permissions			*/
	/*	       xxx00000	 xxx00000										 */
	X_ARM_MMU_SECTION(0x000, 0xF00,	0x001, ARM_CACHEABLE,	ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* Boot Rom */
	X_ARM_MMU_SECTION(0x100, 0x100,	0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW);   /* Internal Registers */
	X_ARM_MMU_SECTION(0x800, 0x800,	0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW);   /* CSI/ATA Registers */
	X_ARM_MMU_SECTION(0xA00, 0x000,	TX27_SDRAM_SIZE >> 20,	ARM_CACHEABLE,	  ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(0xA00, 0xA00,	TX27_SDRAM_SIZE >> 20,	ARM_CACHEABLE,	  ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(0xA00, 0xA80,	TX27_SDRAM_SIZE >> 20,	ARM_UNCACHEABLE,  ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW);   /* SDRAM */
//	X_ARM_MMU_SECTION(0xC00, 0xC00,	0x020, ARM_CACHEABLE,	ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* Flash */
	X_ARM_MMU_SECTION(0xD40, 0xD40,	0x020, ARM_UNCACHEABLE,	ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW);   /* CS4 for External I/O */
	X_ARM_MMU_SECTION(0xD60, 0xD60,	0x020, ARM_CACHEABLE,	ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* CS5 PSRAM */
	X_ARM_MMU_SECTION(0xD80, 0xD80,	0x100, ARM_UNCACHEABLE,	ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW);   /* EMI control/PCMCIA */
	X_ARM_MMU_SECTION(0xFFF, 0xFFF,	0x001, ARM_UNCACHEABLE,	ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW);   /* VRAM */
}

static inline void set_reg(unsigned long addr, CYG_WORD32 set, CYG_WORD32 clr)
{
	CYG_WORD32 val;
	HAL_READ_UINT32(addr, val);
	val = (val & ~clr) | set;
	HAL_WRITE_UINT32(addr, val);
}

//
// Platform specific initialization
//
static void fec_gpio_init(void)
{
	/* GPIOs to set up for TX27/Starterkit-5:
	   Function       GPIO  Dir  act.  FCT
                                     Lvl
	   FEC_RESET      PB30  OUT  LOW   GPIO
	   FEC_ENABLE     PB27  OUT  HIGH  GPIO
	   OSCM26_ENABLE  PB22  OUT  HIGH  GPIO
	   EXT_WAKEUP     PB24  IN   HIGH  GPIO
	   FEC_TXEN       PF23  OUT  OUT   AIN
	   FEC_TXCLK      PD11  OUT  IN    AOUT
	   ...
	 */
#define OCR_SHIFT(bit)		(((bit) * 2) % 32)
#define OCR_MASK(bit)		(3 << (OCR_SHIFT(bit)))
#define OCR_VAL(bit,val)	(((val) << (OCR_SHIFT(bit))) & (OCR_MASK(bit)))
#define GPR_SHIFT(bit)		(bit)
#define GPR_MASK(bit)		(1 << (GPR_SHIFT(bit)))
#define GPR_VAL(bit,val)	(((val) << (GPR_SHIFT(bit))) & (GPR_MASK(bit)))
#define ICONF_SHIFT(bit)	(((bit) * 2) % 32)
#define ICONF_MASK(bit)		(3 << (ICONF_SHIFT(bit)))
#define ICONF_VAL(bit,val)	(((val) << (ICONF_SHIFT(bit))) & (ICONF_MASK(bit)))

	/*
	 * make sure the ETH PHY strap pins are pulled to the right voltage
	 * before deasserting the PHY reset GPIO
	 */
	// deassert PD0-15
	set_reg(SOC_GPIOD_BASE + GPIO_OCR1, 0xffffffff, 0);
	set_reg(SOC_GPIOD_BASE + GPIO_DR, 0, 0xffff);
	set_reg(SOC_GPIOD_BASE + GPIO_DDIR, 0xffff, 0);
	set_reg(SOC_GPIOD_BASE + GPIO_GIUS, 0xffff, 0);

	// deassert PD16
	set_reg(SOC_GPIOD_BASE + GPIO_OCR2, OCR_MASK(16), 0);
	set_reg(SOC_GPIOD_BASE + GPIO_DR, 0, GPR_MASK(16));
	set_reg(SOC_GPIOD_BASE + GPIO_DDIR, GPR_MASK(16), 0);
	set_reg(SOC_GPIOD_BASE + GPIO_GIUS, GPR_MASK(16), 0);

	// deassert PF23
	set_reg(SOC_GPIOF_BASE + GPIO_OCR2, OCR_MASK(23), 0);
	set_reg(SOC_GPIOF_BASE + GPIO_DR, 0, GPR_MASK(23));
	set_reg(SOC_GPIOF_BASE + GPIO_DDIR, GPR_MASK(23), 0);
	set_reg(SOC_GPIOF_BASE + GPIO_GIUS, GPR_MASK(23), 0);

	// assert FEC PHY Reset (PB30) and switch PHY power off
	/* PB22, PB27, PB30 => GPIO out */
	set_reg(SOC_GPIOB_BASE + GPIO_OCR2, OCR_MASK(27) | OCR_MASK(30), 0);
	set_reg(SOC_GPIOB_BASE + GPIO_DR, 0, GPR_MASK(27) | GPR_MASK(30));
	set_reg(SOC_GPIOB_BASE + GPIO_DDIR, GPR_MASK(27) | GPR_MASK(30), 0);
	set_reg(SOC_GPIOB_BASE + GPIO_GIUS, GPR_MASK(27) | GPR_MASK(30), 0);
}

//
// Platform specific initialization
//

unsigned int g_clock_src;
unsigned int g_board_type = BOARD_TYPE_TX27KARO;

void plf_hardware_init(void)
{
	g_clock_src = PLL_REF_CLK;
	fec_gpio_init();
}

#define SOC_FBAC0_REG		0x10028800UL

extern int fuse_blow(int bank, int row, int bit);

#define SOC_I2C2_BASE		UL(0x1001D000)

/* Address offsets of the I2C registers */
#define MXC_IADR				0x00	/* Address Register */
#define MXC_IFDR				0x04	/* Freq div register */
#define MXC_I2CR				0x08	/* Control regsiter */
#define MXC_I2SR				0x0C	/* Status register */
#define MXC_I2DR				0x10	/* Data I/O register */

/* Bit definitions of I2CR */
#define MXC_I2CR_IEN			0x0080
#define MXC_I2CR_IIEN			0x0040
#define MXC_I2CR_MSTA			0x0020
#define MXC_I2CR_MTX			0x0010
#define MXC_I2CR_TXAK			0x0008
#define MXC_I2CR_RSTA			0x0004

/* Bit definitions of I2SR */
#define MXC_I2SR_ICF			0x0080
#define MXC_I2SR_IAAS			0x0040
#define MXC_I2SR_IBB			0x0020
#define MXC_I2SR_IAL			0x0010
#define MXC_I2SR_SRW			0x0004
#define MXC_I2SR_IIF			0x0002
#define MXC_I2SR_RXAK			0x0001

#define LP3972_SLAVE_ADDR	0x34

static inline cyg_uint8 i2c_addr(cyg_uint8 addr, int rw)
{
	return (addr << 1) | !!rw;
}

static inline cyg_uint8 mx27_i2c_read(cyg_uint8 reg)
{
	cyg_uint16 val;
	HAL_READ_UINT16(SOC_I2C2_BASE + reg, val);
	return val;
}

static inline void mx27_i2c_write(cyg_uint8 reg, cyg_uint8 val)
{
	HAL_WRITE_UINT16(SOC_I2C2_BASE + reg, val);
}

static inline void mx27_i2c_set_reg(cyg_uint8 reg, cyg_uint8 set, cyg_uint8 clr)
{
	cyg_uint8 val = mx27_i2c_read(reg);
	val = (val & ~clr) | set;
	mx27_i2c_write(reg, val);
}

static void mx27_i2c_disable(void)
{
	/* disable I2C controller */
	mx27_i2c_set_reg(MXC_I2CR, 0, MXC_I2CR_IEN);
	/* disable I2C clock */
	set_reg(SOC_CRM_PCCR0, 0, (1 << 17));
}

static int mx27_i2c_init(void)
{
	int ret;

	/* configure PC5,PC6 as Primary Function */
	set_reg(SOC_GPIOC_BASE + GPIO_GPR, 0, GPR_MASK(5) | GPR_MASK(6));
	set_reg(SOC_GPIOC_BASE + GPIO_GIUS, 0, GPR_MASK(5) | GPR_MASK(6));

	/* enable I2C clock */
	set_reg(SOC_CRM_PCCR0, (1 << 17), 0);

	/* setup I2C clock divider */
	mx27_i2c_write(MXC_IFDR, 0x2c);
	mx27_i2c_write(MXC_I2SR, 0);

	/* enable I2C controller in master mode */
	mx27_i2c_write(MXC_I2CR, MXC_I2CR_IEN);

	ret = mx27_i2c_read(MXC_I2SR);
	if (ret & MXC_I2SR_IBB) {
		diag_printf("I2C bus busy\n");
		mx27_i2c_disable();
		return -EIO;
	}
	return 0;
}

static int mx27_i2c_wait_busy(int set)
{
	int ret;
	const int max_loops = 100;
	int retries = max_loops;

	cyg_uint8 mask = set ? MXC_I2SR_IBB : 0;

	while ((ret = mask ^ (mx27_i2c_read(MXC_I2SR) & MXC_I2SR_IBB)) && --retries > 0) {
		HAL_DELAY_US(3);
	}
	if (ret != 0) {
		diag_printf("i2c: Waiting for IBB to %s timed out\n", set ? "set" : "clear");
		return -ETIMEDOUT;
	}
	return ret;
}

static int mx27_i2c_wait_tc(void)
{
	int ret;
	const int max_loops = 1000;
	int retries = max_loops;

	while (!((ret = mx27_i2c_read(MXC_I2SR)) & MXC_I2SR_IIF) && --retries > 0) {
		HAL_DELAY_US(3);
	}
	mx27_i2c_write(MXC_I2SR, 0);
	if (!(ret & MXC_I2SR_IIF)) {
		diag_printf("i2c: Wait for transfer completion timed out\n");
		return -ETIMEDOUT;
	}
	if (ret & MXC_I2SR_ICF) {
		if (mx27_i2c_read(MXC_I2CR) & MXC_I2CR_MTX) {
			if (!(ret & MXC_I2SR_RXAK)) {
				ret = 0;
			} else {
				diag_printf("i2c: No ACK received after writing data\n");
				return -ENXIO;
			}
		}
	}
	return ret;
}

static int mx27_i2c_stop(void)
{
	int ret;

	mx27_i2c_set_reg(MXC_I2CR, 0, MXC_I2CR_MSTA | MXC_I2CR_MTX);
	ret = mx27_i2c_wait_busy(0);
	return ret;
}

static int mx27_i2c_start(cyg_uint8 addr, int rw)
{
	int ret;

	ret = mx27_i2c_init();
	if (ret < 0) {
		diag_printf("I2C bus init failed; cannot switch fuse programming voltage\n");
		return ret;
	}
	mx27_i2c_set_reg(MXC_I2CR, MXC_I2CR_MSTA, 0);
	ret = mx27_i2c_wait_busy(1);
	if (ret == 0) {
		mx27_i2c_set_reg(MXC_I2CR, MXC_I2CR_MTX, 0);
		mx27_i2c_write(MXC_I2DR, i2c_addr(addr, rw));
		ret = mx27_i2c_wait_tc();
		if (ret < 0) {
			mx27_i2c_stop();
		}
	}
	return ret;
}

static int mx27_i2c_repeat_start(cyg_uint8 addr, int rw)
{
	int ret;

	mx27_i2c_set_reg(MXC_I2CR, MXC_I2CR_RSTA, 0);
	HAL_DELAY_US(3);
	mx27_i2c_write(MXC_I2DR, i2c_addr(addr, rw));
	ret = mx27_i2c_wait_tc();
	if (ret < 0) {
		mx27_i2c_stop();
	}
	return ret;
}

static int mx27_i2c_read_byte(void)
{
	int ret;

	mx27_i2c_set_reg(MXC_I2CR, MXC_I2CR_TXAK, MXC_I2CR_MTX);
	(void)mx27_i2c_read(MXC_I2DR); /* dummy read after address cycle */
	ret = mx27_i2c_wait_tc();
	mx27_i2c_stop();
	if (ret < 0) {
		return ret;
	}
	ret = mx27_i2c_read(MXC_I2DR);
	return ret;
}

static int mx27_i2c_write_byte(cyg_uint8 data, int last)
{
	int ret;

	mx27_i2c_set_reg(MXC_I2CR, MXC_I2CR_MTX, 0);
	mx27_i2c_write(MXC_I2DR, data);
	if ((ret = mx27_i2c_wait_tc()) < 0 || last) {
		mx27_i2c_stop();
	}
	return ret;
}

static int lp3972_reg_read(cyg_uint8 reg)
{
	int ret;

	ret = mx27_i2c_start(LP3972_SLAVE_ADDR, 0);
	if (ret < 0) {
		return ret;
	}
	ret = mx27_i2c_write_byte(reg, 0);
	if (ret < 0) {
		return ret;
	}
	ret = mx27_i2c_repeat_start(LP3972_SLAVE_ADDR, 1);
	if (ret < 0) {
		return ret;
	}
	ret = mx27_i2c_read_byte();
	mx27_i2c_disable();
	return ret;
}

static int lp3972_reg_write(cyg_uint8 reg, cyg_uint8 val)
{
	int ret;

	ret = mx27_i2c_start(LP3972_SLAVE_ADDR, 0);
	if (ret < 0) {
		return ret;
	}
	ret = mx27_i2c_write_byte(reg, 0);
	if (ret < 0) {
		return ret;
	}
	ret = mx27_i2c_write_byte(val, 1);
	mx27_i2c_disable();
	return ret;
}

int tx27_mac_addr_program(unsigned char mac_addr[ETHER_ADDR_LEN])
{
	int ret = 0;
	int i;

	for (i = 0; i < ETHER_ADDR_LEN; i++) {
		unsigned char fuse = readl(SOC_FEC_MAC_BASE2 + (i << 2));

		if ((fuse | mac_addr[i]) != mac_addr[i]) {
			diag_printf("MAC address fuse cannot be programmed: fuse[%d]=0x%02x -> 0x%02x\n",
						i, fuse, mac_addr[i]);
			return -1;
		}
		if (fuse != mac_addr[i]) {
			ret = 1;
		}
	}
	if (ret == 0) {
		return ret;
	}
	ret = lp3972_reg_write(0x39, 0xf0);
	if (ret < 0) {
		diag_printf("Failed to switch fuse programming voltage\n");
		return ret;
	}
	ret = lp3972_reg_read(0x39);
	if (ret != 0xf0) {
		diag_printf("Failed to switch fuse programming voltage\n");
		return ret;
	}
	for (i = 0; i < ETHER_ADDR_LEN; i++) {
		int bit;
		unsigned char fuse = readl(SOC_FEC_MAC_BASE2 + (i << 2));

		for (bit = 0; bit < 8; bit++) {
			if (((mac_addr[i] >> bit) & 0x1) == 0)
				continue;
			if (((mac_addr[i] >> bit) & 1) == ((fuse >> bit) & 1)) {
				continue;
			}
			if (fuse_blow(0, i + 5, bit)) {
				diag_printf("Failed to blow fuse bank 0 row %d bit %d\n",
							i, bit);
				ret = -1;
				goto out;
			}
		}
	}
	/* would like to blow the MAC_ADDR_LOCK fuse, but that's not available on MX27 */
	//fuse_blow(0, 0, SOC_MAC_ADDR_LOCK_BIT);
out:
	lp3972_reg_write(0x39, 0);
	return ret;
}

#include CYGHWR_MEMORY_LAYOUT_H

typedef void code_fun(void);

void tx27_program_new_stack(void *func)
{
	register CYG_ADDRESS stack_ptr asm("sp");
	register CYG_ADDRESS old_stack asm("r4");
	register code_fun *new_func asm("r0");
	old_stack = stack_ptr;
	stack_ptr = CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE - sizeof(CYG_ADDRESS);
	new_func = (code_fun*)func;
	new_func();
	stack_ptr = old_stack;
}

static void display_clock_src(void)
{
	if (g_clock_src == FREQ_32000HZ) {
		diag_printf("Clock input: 32kHz\n");
	} else if (g_clock_src == FREQ_26MHZ) {
		diag_printf("Clock input: 26MHz\n");
	} else if (g_clock_src == FREQ_32768HZ) {
		diag_printf("Clock input: 32.768kHz\n");
	} else {
		diag_printf("Unknown clock input source. Something is wrong!\n");
	}
}

#define WDOG_WRSR	((CYG_WORD16 *)0x10002004)
static void display_board_type(void)
{
	char *reset_cause;
	CYG_WORD16 wrsr;

	diag_printf("\nBoard Type: Ka-Ro TX27\n");
	HAL_READ_UINT16(WDOG_WRSR, wrsr);
	switch (wrsr) {
	case (1 << 4):
		reset_cause = "POWER_ON RESET";
		break;
	case (1 << 3):
		reset_cause = "EXTERNAL RESET";
		break;
	case (1 << 1):
		reset_cause = "WATCHDOG RESET";
		break;
	case (1 << 0):
		reset_cause = "SOFT RESET";
		break;
	default:
		reset_cause = "UNKNOWN";
	}
	diag_printf("Last RESET cause: %s\n", reset_cause);
}

static void display_board_info(void)
{
	display_board_type();
	display_clock_src();
}

RedBoot_init(display_board_info, RedBoot_INIT_LAST);
