//==========================================================================
//
//	redboot_cmds.c
//
//	Board [platform] specific RedBoot commands
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
#include <redboot.h>
#include <cyg/hal/hal_intr.h>
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/plf_mmap.h>
#include CYGBLD_HAL_PLF_DEFS_H		// Platform specific hardware definitions

#ifdef CYGSEM_REDBOOT_FLASH_CONFIG
#include <flash_config.h>

#if (REDBOOT_IMAGE_SIZE != CYGBLD_REDBOOT_MIN_IMAGE_SIZE)
#error REDBOOT_IMAGE_SIZE != CYGBLD_REDBOOT_MIN_IMAGE_SIZE
#endif

#endif	//CYGSEM_REDBOOT_FLASH_CONFIG

#ifdef CYGPKG_IO_FLASH
#include <cyg/io/flash.h>
#endif

static void runImg(int argc, char *argv[]);
static void do_mem(int argc, char *argv[]);

RedBoot_cmd("mem",
	    "Set a memory location",
	    "[-h|-b] [-n] [-a <address>] <data>",
	    do_mem
    );

RedBoot_cmd("run",
	    "Run an image at a location with MMU off",
	    "[<virtual addr>]",
	    runImg
	   );

static void do_mem(int argc, char *argv[])
{
	struct option_info opts[4];
	bool mem_half_word, mem_byte;
	bool no_verify;
	bool addr_set;
	unsigned long address;
	unsigned int value;
	int ret;
	init_opts(&opts[0], 'b', false, OPTION_ARG_TYPE_FLG,
		  &mem_byte, NULL, "write a byte");
	init_opts(&opts[1], 'h', false, OPTION_ARG_TYPE_FLG,
		  &mem_half_word, NULL, "write a half-word");
	init_opts(&opts[2], 'a', true, OPTION_ARG_TYPE_NUM,
		  &address, &addr_set, "address to write to");
	init_opts(&opts[3], 'n', false, OPTION_ARG_TYPE_FLG,
		  &no_verify, NULL, "noverify");

	ret = scan_opts(argc, argv, 1, opts, sizeof(opts) / sizeof(opts[0]),
			&value, OPTION_ARG_TYPE_NUM, "value to be written");
	if (ret == 0) {
		return;
	}
	if (!addr_set) {
		diag_printf("** Error: '-a <address>' must be specified\n");
		return;
	}
	if (ret == argc + 1) {
		diag_printf("** Error: non-option argument '<value>' must be specified\n");
		return;
	}
	if (mem_byte && mem_half_word) {
		diag_printf("** Error: Should not specify both byte and half-word access\n");
	} else if (mem_byte) {
		value &= 0xff;
		*(volatile cyg_uint8*)address = (cyg_uint8)value;
		if (no_verify) {
			diag_printf("  Set 0x%08lX to 0x%02X\n", address, value);
		} else {
			diag_printf("  Set 0x%08lX to 0x%02X (result 0x%02X)\n",
				    address, value, (int)*(cyg_uint8*)address );
		}
	} else if (mem_half_word) {
		if (address & 1) {
			diag_printf("** Error: address for half-word access must be half-word aligned\n");
		} else {
			value &= 0xffff;
			*(volatile cyg_uint16*)address = (cyg_uint16)value;
			if (no_verify) {
				diag_printf("  Set 0x%08lX to 0x%04X\n", address, value);
			} else {
				diag_printf("  Set 0x%08lX to 0x%04X (result 0x%04X)\n",
					    address, value, (int)*(cyg_uint16*)address);
			}
		}
	} else {
		if (address & 3) {
			diag_printf("** Error: address for word access must be word aligned\n");
		} else {
			*(volatile cyg_uint32*)address = (cyg_uint32)value;
			if (no_verify) {
				diag_printf("  Set 0x%08lX to 0x%08X\n", address, value);
			} else {
				diag_printf("  Set 0x%08lX to 0x%08X (result 0x%08X)\n",
					    address, value, (int)*(cyg_uint32*)address);
			}
		}
	}
}

void launchRunImg(unsigned long addr)
{
    asm volatile ("mov r1, r0;");
    HAL_MMU_OFF();
    asm volatile (
		 "mov r11, #0;"
		 "mov r12, #0;"
		 "mrs r10, cpsr;"
		 "bic r10, r10, #0xF0000000;"
		 "msr cpsr_f, r10;"
		 "mov pc, r1"
		 );
}

static void runImg(int argc,char *argv[])
{
	unsigned int virt_addr, phys_addr;

	// Default physical entry point for Symbian
	if (entry_address == 0xFFFFFFFF)
		virt_addr = 0x800000;
	else
		virt_addr = entry_address;

	if (!scan_opts(argc, argv, 1, 0, 0, &virt_addr,
		       OPTION_ARG_TYPE_NUM, "virtual address"))
		return;

	if (entry_address != 0xFFFFFFFF)
		diag_printf("load entry_address=0x%lx\n", entry_address);
	HAL_VIRT_TO_PHYS_ADDRESS(virt_addr, phys_addr);

	diag_printf("virt_addr=0x%x\n",virt_addr);
	diag_printf("phys_addr=0x%x\n",phys_addr);

	launchRunImg(phys_addr);
}

#if defined(CYGSEM_REDBOOT_FLASH_CONFIG) && defined(CYG_HAL_STARTUP_ROMRAM)

RedBoot_cmd("romupdate",
	    "Update Redboot with currently running image",
	    "",
	    romupdate
	   );

#ifdef CYGPKG_IO_FLASH
void romupdate(int argc, char *argv[])
{
	void *err_addr, *base_addr;
	int stat;

	base_addr = (void*)(MXC_NAND_BASE_DUMMY + CYGBLD_REDBOOT_FLASH_BOOT_OFFSET);
	diag_printf("Updating RedBoot in NAND flash\n");

	// Erase area to be programmed
	if ((stat = flash_erase(base_addr, CYGBLD_REDBOOT_MIN_IMAGE_SIZE, &err_addr)) != 0) {
		diag_printf("Can't erase region at %p: %s\n",
			    err_addr, flash_errmsg(stat));
		return;
	}
	// Now program it
	if ((stat = flash_program(base_addr, ram_end,
				  CYGBLD_REDBOOT_MIN_IMAGE_SIZE, &err_addr)) != 0) {
		diag_printf("Can't program region at %p: %s\n",
			    err_addr, flash_errmsg(stat));
	}
}
#endif //CYGPKG_IO_FLASH
#endif /* CYG_HAL_STARTUP_ROMRAM */
