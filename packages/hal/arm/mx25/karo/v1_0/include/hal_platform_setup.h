#ifndef CYGONCE_HAL_PLATFORM_SETUP_H
#define CYGONCE_HAL_PLATFORM_SETUP_H

//=============================================================================
//
//  hal_platform_setup.h
//
//  Platform specific support for HAL (assembly code)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <pkgconf/system.h>		// System-wide configuration info
#include CYGBLD_HAL_VARIANT_H		// Variant specific configuration
#include CYGBLD_HAL_PLATFORM_H		// Platform specific configuration
#include <cyg/hal/hal_soc.h>		// Variant specific hardware definitions
#include <cyg/hal/hal_mmu.h>		// MMU definitions
#include CYGBLD_HAL_PLF_DEFS_H		// Platform specific hardware definitions
#include CYGHWR_MEMORY_LAYOUT_H

#if defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1 _platform_setup1
#define CYGHWR_HAL_ARM_HAS_MMU

//#define INTERNAL_BOOT_MODE

#if defined(INTERNAL_BOOT_MODE)
#define PLATFORM_PREAMBLE setup_flash_header
#endif

#ifdef CYG_HAL_STARTUP_ROMRAM
#define CYGSEM_HAL_ROM_RESET_USES_JUMP
#endif

#define TX25_NAND_PAGE_SIZE		2048
#define TX25_NAND_BLKS_PER_PAGE		64

#define DEBUG_LED_BIT			7

#ifndef CYGOPT_HAL_ARM_TX25_DEBUG
	.macro  LED_CTRL,val
	.endm
	.macro  LED_BLINK,val
	.endm
	.macro  DELAY,ms
	.endm
#else
#define USE_SERIAL
#define CYGHWR_LED_MACRO    LED_BLINK #\x
	.macro  DELAY,ms
	ldr	r10, =\ms
111:
	subs	r10, r10, #1
	bmi	113f
	ldr	r9, =3600
112:
	subs	r9, r9, #1
	bne	112b
	b	111b
	.ltorg
113:
	.endm

	.macro	LED_CTRL,val
	// switch user LED (GPIO2[7] PAD A21) on STK5
	ldr	r10, GPIO2_BASE_ADDR_W
	// GPIO_DR
	mov	r9, \val
	cmp	r9, #0
	ldr	r9, [r10, #GPIO_DR]
	orne	r9, #(1 << DEBUG_LED_BIT)  // LED ON
	biceq	r9, #(1 << DEBUG_LED_BIT)  // LED OFF
	str	r9, [r10, #GPIO_DR]
	.endm

	.macro	LED_BLINK,val
	mov	r3, \val
211:
	subs	r3, r3, #1
	bmi	212f
	LED_CTRL #1
	DELAY	200
	LED_CTRL #0
	DELAY	300
	b	211b
212:
	DELAY	1000
	.endm
#endif

#ifdef USE_SERIAL
	.macro	early_uart_setup
	ldr	r1, IOMUXC_BASE_ADDR_W
	mov	r0, #0
	str	r0, [r1, #0x170]
	str	r0, [r1, #0x17c]
	str	r0, [r1, #0x174]
	str	r0, [r1, #0x178]
	mov	r0, #0x1e0
	str	r0, [r1, #0x368]
	str	r0, [r1, #0x370]
	mov	r0, #0x040
	str	r0, [r1, #0x36c]
	str	r0, [r1, #0x374]

	ldr	r1, UART1_BASE_ADDR_W
	mov	r0, #(1 << 0)		@ UART_EN
//	orr	r0, r0, #(1 << 14)	@ ADEN
	str	r0, [r1, #0x80]		@ UCR1

	mov	r0, #(1 << 14)		@ IRTS
	orr	r0, r0, #((1 << 5) | (1 << 2) | (1 << 1))	@ word size 8bit, TXEN, RXEN
	str	r0, [r1, #0x84]		@ UCR2

	ldr	r0, [r1, #0x88]		@ UCR3
	orr	r0, r0, #(1 << 2)	@ RXDMUXSEL
	str	r0, [r1, #0x88]		@ UCR3

	ldr	r0, UART1_UBIR_W
	str	r0, [r1, #0xa4]		@ UBIR

	ldr	r0, UART1_UBMR_W
	str	r0, [r1, #0xa8]		@ UBMR
	.endm

	.macro	uart_putc,c
	ldr	r9, UART1_BASE_ADDR_W
111:
	ldr	r10, [r9, #0xb4]	@ UTS
	tst	r10, #(1 << 4)		@ TXFULL
	bne	111b
	mov	r10, \c
	str	r10, [r9, #0x40]
	.endm

	.set	progress_ind, 'A'
	.macro	progress
	uart_putc #progress_ind
	.set	progress_ind, progress_ind + 1
	.endm
#else
	.macro	early_uart_setup
	.endm
	.macro	uart_putc,c
	.endm
	.macro	progress
	.endm
#endif

	.macro LED_INIT
	// initialize GPIO2[7] (Pad A21) for LED on STK5
	ldr	r10, GPIO2_BASE_ADDR_W
	// GPIO_GDIR
	ldr	r9, [r10, #GPIO_GDIR]
	orr	r9, #(1 << DEBUG_LED_BIT)
	str	r9, [r10, #GPIO_GDIR]

	ldr	r10, IOMUXC_BASE_ADDR_W
	mov	r9, #0x15
	str	r9, [r10, #0x2c]

	ldr	r10, GPIO2_BASE_ADDR_W
	// GPIO_DR
	ldr	r9, [r10, #GPIO_DR]
	orr	r9, #(1 << DEBUG_LED_BIT)  // LED ON
	str	r9, [r10, #GPIO_DR]
	.endm

	.macro LCD_GPIO_INIT
#ifdef CYGHWR_TX25_BOOTSPLASH
	ldr	r10, IOMUXC_BASE_ADDR_W

	mov	r9, #1
	str	r9, [r10, #0x088]
	str	r9, [r10, #0x08c]

	mov	r9, #0
	str	r9, [r10, #0x0c8]
	str	r9, [r10, #0x0cc]
	str	r9, [r10, #0x0d0]
	str	r9, [r10, #0x0d4]
	str	r9, [r10, #0x0d8]
	str	r9, [r10, #0x0dc]
	str	r9, [r10, #0x0e0]
	str	r9, [r10, #0x0e4]
	str	r9, [r10, #0x0e8]
	str	r9, [r10, #0x0ec]
	str	r9, [r10, #0x0f0]
	str	r9, [r10, #0x0f4]
	str	r9, [r10, #0x0f8]
	str	r9, [r10, #0x0fc]
	str	r9, [r10, #0x100]
	str	r9, [r10, #0x104]
	str	r9, [r10, #0x108]
	str	r9, [r10, #0x10c]
	str	r9, [r10, #0x110]
	str	r9, [r10, #0x114]

	mov	r9, #5
	str	r9, [r10, #0x11c]	/* configure PAD_PWM as GPIO1_26 LCD_BACKLIGHT */
	str	r9, [r10, #0x020]	/* dto. for PAD_A18 (GPIO2_4) LCD_RESET */
	str	r9, [r10, #0x024]	/* dto. for PAD_A19 (GPIO2_5) LCD_POWER */

	ldr	r10, GPIO1_BASE_ADDR_W

	ldr	r9, [r10, #GPIO_DR]
	orr	r9, #(1 << (LCD_BACKLIGHT_GPIO % 32))
	str	r9, [r10, #GPIO_DR]

	ldr	r9, [r10, #GPIO_GDIR]
	orr	r9, #(1 << (LCD_BACKLIGHT_GPIO % 32))
	str	r9, [r10, #GPIO_GDIR]

	ldr	r10, GPIO2_BASE_ADDR_W

	ldr	r9, [r10, #GPIO_DR]
	bic	r9, #(1 << (LCD_POWER_GPIO % 32) | (1 << (LCD_RESET_GPIO % 32)))
	str	r9, [r10, #GPIO_DR]

	ldr	r9, [r10, #GPIO_GDIR]
	orr	r9, #((1 << (LCD_POWER_GPIO % 32)) | (1 << (LCD_RESET_GPIO % 32)))
	str	r9, [r10, #GPIO_GDIR]

#endif
	.endm

// This macro represents the initial startup code for the platform
// r11 is reserved to contain chip rev info in this file
	.macro  _platform_setup1
KARO_TX25_SETUP_START:
	// invalidate I/D cache/TLB
	mov	r0, #0
	mcr	15, 0, r0, c7, c7, 0	/* invalidate I cache and D cache */
	mcr	15, 0, r0, c8, c7, 0	/* invalidate TLBs */
	mcr	15, 0, r0, c7, c10, 4	/* Data Write Barrier */

init_spba_start:
	init_spba
init_aips_start:
	init_aips
init_max_start:
	init_max
init_m3if_start:
	init_m3if

#ifndef INTERNAL_BOOT_MODE
	// check if sdram has been setup
#ifdef RAM_BANK1_SIZE
	cmp	pc, #RAM_BANK1_BASE
	blo	init_clock_start
	cmp	pc, #(RAM_BANK1_BASE + RAM_BANK1_SIZE)
	blo	HWInitialise_skip_SDRAM_setup
#else
	cmp	pc, #RAM_BANK0_BASE
	blo	init_clock_start
	cmp	pc, #(RAM_BANK0_BASE + RAM_BANK0_SIZE)
	blo	HWInitialise_skip_SDRAM_setup
#endif // RAM_BANK1_SIZE
#endif // INTERNAL_BOOT_MODE
init_clock_start:
	init_clock

	LCD_GPIO_INIT
	LED_INIT
	LED_BLINK #1

	early_uart_setup

#ifndef INTERNAL_BOOT_MODE

init_sdram_start:
	setup_sdram
	LED_BLINK #2

	progress
#endif

HWInitialise_skip_SDRAM_setup:
	mov	r0, #NFC_BASE
	add	r2, r0, #0x0800		// 2K window
	cmp	pc, r0
	blo	Normal_Boot_Continue
	cmp	pc, r2
	bhi	Normal_Boot_Continue

NAND_Boot_Start:
	progress
	/* Copy image from NFC buffer to SDRAM first */
	ldr	r1, MXC_REDBOOT_RAM_START
1:
	ldmia	r0!, {r3-r10}
	stmia	r1!, {r3-r10}
	cmp	r0, r2
	blo	1b
	
	progress

	bl	jump_to_sdram
	progress

NAND_Copy_Main:
	mov	r0, #NFC_BASE		// r0: nfc base. Reloaded after each page copy
	add	r12, r0, #0x1E00	// r12: NFC register base. Doesn't change
	ldrh	r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]
	orr	r3, r3, #0x1

	/* Setting NFC */
	ldr	r7, CCM_BASE_ADDR_W
	ldr	r1, [r7, #CLKCTL_RCSR]
	/* BUS WIDTH setting */
	tst	r1, #(1 << 24)
	orrne	r1, r1, #(1 << 14)
	biceq	r1, r1, #(1 << 14)

	/* 4K PAGE */
	tst	r1, #(1 << 27)
	orrne	r1, r1, #(1 << 9)
	bne	 1f
	/* 2K PAGE */
	bic	r1, r1, #(1 << 9)
	tst	r1, #(1 << 26)
	orrne	r1, r1, #(1 << 8)	/* 2KB page size */
	biceq	r1, r1, #(1 << 8)	/* 512B page size */
	movne	r2, #(64 >> 1) /* 64 bytes */
	moveq	r2, #8  /* 16 bytes */
	b	NAND_setup
1:
	tst	r1, #(1 << 26)
	bicne	r3, r3, #1   /* Enable 8bit ECC mode */
	movne	r2, #109 /* 218 bytes */
	moveq	r2, #(128 >> 1)  /* 128 bytes */
NAND_setup:
	str	r1, [r7, #CLKCTL_RCSR]
	strh	r2, [r12, #ECC_RSLT_SPARE_AREA_REG_OFF]
	strh	r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]

	//unlock internal buffer
	mov	r3, #0x2
	strh	r3, [r12, #NFC_CONFIGURATION_REG_OFF]
	//unlock nand device
	mov	r3, #0
	strh	r3, [r12, #UNLOCK_START_BLK_ADD_REG_OFF]
	sub	r3, r3, #1
	strh	r3, [r12, #UNLOCK_END_BLK_ADD_REG_OFF]
	mov	r3, #4
	strh	r3, [r12, #NF_WR_PROT_REG_OFF]

	/* r0: NFC base address. RAM buffer base address. [Updated constantly]
	 * r1: starting flash address to be copied. [Updated constantly]
	 * r2: page size. [Doesn't change]
	 * r3: used as argument.
	 * r11: starting SDRAM address for copying. [Updated constantly].
	 * r12: NFC register base address. [Updated constantly].
	 * r13: end of SDRAM address for copying. [Doesn't change].
	 */
	mov	r1, #0x1000
	ldr	r3, [r7, #CLKCTL_RCSR]
	tst	r3, #(1 << 9)
	movne	r2, #0x1000
	bne	1f
	tst	r3, #(1 << 8)
	mov	r1, #0x800
	movne	r2, #0x800
	moveq	r2, #0x200
1:
	/* Update the indicator of copy area */
	ldr	r11, MXC_REDBOOT_RAM_START
	add	r13, r11, #REDBOOT_IMAGE_SIZE
	add	r11, r11, r1

Nfc_Read_Page:
	mov	r3, #0x0
	nfc_cmd_input

	cmp	r2, #0x800
	bhi	nfc_addr_ops_4kb
	beq	nfc_addr_ops_2kb

	mov	r3, r1
	do_addr_input	//1st addr cycle
	mov	r3, r1, lsr #9
	do_addr_input	//2nd addr cycle
	mov	r3, r1, lsr #17
	do_addr_input	//3rd addr cycle
	mov	r3, r1, lsr #25
	do_addr_input	//4th addr cycle
	b	end_of_nfc_addr_ops

nfc_addr_ops_2kb:
	mov	r3, #0
	do_addr_input	//1st addr cycle
	mov	r3, #0
	do_addr_input	//2nd addr cycle
	mov	r3, r1, lsr #11
	do_addr_input	//3rd addr cycle
	mov	r3, r1, lsr #19
	do_addr_input	//4th addr cycle
	mov	r3, r1, lsr #27
	do_addr_input	//5th addr cycle

	mov	r3, #0x30
	nfc_cmd_input
	b	end_of_nfc_addr_ops

nfc_addr_ops_4kb:
	mov	r3, #0
	do_addr_input	//1st addr cycle
	mov	r3, #0
	do_addr_input	//2nd addr cycle
	mov	r3, r1, lsr #12
	do_addr_input	//3rd addr cycle
	mov	r3, r1, lsr #20
	do_addr_input	//4th addr cycle
	mov	r3, r1, lsr #27
	do_addr_input	//5th addr cycle

	mov	r3, #0x30
	nfc_cmd_input

end_of_nfc_addr_ops:
	mov	r8, #0
	bl	nfc_data_output
	bl	do_wait_op_done
	// Check if x16/2kb page
	cmp	r2, #0x800
	bhi	nfc_addr_data_output_done_4k
	beq	nfc_addr_data_output_done_2k
	beq	nfc_addr_data_output_done_512

	// check for bad block
//    mov r3, r1, lsl #(32-17)		// get rid of block number
//    cmp r3, #(0x800 << (32-17))	// check if not page 0 or 1
	b	nfc_addr_data_output_done

nfc_addr_data_output_done_4k:
//TODO
	b	nfc_addr_data_output_done

nfc_addr_data_output_done_2k:
	// check for bad block
//TODO:	mov r3, r1, lsl #(32-17) 	// get rid of block number
//    cmp r3, #(0x800 << (32-17))	// check if not page 0 or 1
	b	nfc_addr_data_output_done

nfc_addr_data_output_done_512:
	// check for bad block
// TODO: mov r3, r1, lsl #(32-5-9)	// get rid of block number
// TODO: cmp r3, #(512 << (32-5-9))	// check if not page 0 or 1

nfc_addr_data_output_done:
#if 0
	bhi	Copy_Good_Blk
	add	r4, r0, #0x1000		//r3 -> spare area buf 0
	ldrh	r4, [r4, #0x4]
	and	r4, r4, #0xFF00
	cmp	r4, #0xFF00
	beq	Copy_Good_Blk
	// really sucks. Bad block!!!!
	cmp	r3, #0x0
	beq	Skip_bad_block
	// even suckier since we already read the first page!
	// Check if x16/2kb page
	cmp	r2, #0x800
	// for 4k page
	subhi	r11, r11, #0x1000	//rewind 1 page for the sdram pointer
	subhi	r1, r1, #0x1000		//rewind 1 page for the flash pointer
	// for 2k page
	subeq	r11, r11, #0x800	//rewind 1 page for the sdram pointer
	subeq	r1, r1, #0x800		//rewind 1 page for the flash pointer
	// for 512 page
	sublo	r11, r11, #512		//rewind 1 page for the sdram pointer
	sublo	r1, r1, #512		//rewind 1 page for the flash pointer
Skip_bad_block:
	// Check if x16/2kb page
	ldr	r7, CCM_BASE_ADDR_W
	ldr	r7, [r7, #CLKCTL_RCSR]
	tst	r7, #0x200
	addne	r1, r1, #(128 * 4096)
	bne	Skip_bad_block_done
	tst	r7, #0x100
	addeq	r1, r1, #(32 * 512)
	addne	r1, r1, #(64 * 2048)
Skip_bad_block_done:
	b	Nfc_Read_Page
#endif
Copy_Good_Blk:
	uart_putc #'.'
	// copying page
	add	r2, r2, #NFC_BASE
1:
	ldmia	r0!, {r3-r10}
	stmia	r11!, {r3-r10}
	cmp	r0, r2
	blo	1b
	sub	r2, r2, #NFC_BASE

	cmp	r11, r13
	bge	NAND_Copy_Main_done
	// Check if x16/2kb page
	add	r1, r1, r2
	mov	r0, #NFC_BASE
	b	Nfc_Read_Page

NAND_Copy_Main_done:
Normal_Boot_Continue:
Now_in_SDRAM:
	LED_BLINK #4

STACK_Setup:
	// Set up a stack [for calling C code]
	ldr	sp, =__startup_stack

	// Create MMU tables
	bl	hal_mmu_init
	LED_BLINK #5

	// Enable MMU
	ldr	r2, =10f
	mrc	MMU_CP, 0, r1, MMU_Control, c0		// get c1 value to r1 first
	orr	r1, r1, #7				// enable MMU bit

	mcr	MMU_CP, 0, r1, MMU_Control, c0
	mov	pc, r2	 /* Change address spaces */
	nop
10:
	LED_BLINK #6
	.endm	// _platform_setup1

jump_to_sdram:
	ldr	r0, SDRAM_ADDR_MASK
	ldr	r1, MXC_REDBOOT_RAM_START
	and	r0, lr, r0
	sub	r0, r1, r0
	add	lr, lr, r0
	mov	pc, lr

do_wait_op_done:
	ldrh	r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
	ands	r3, r3, #NAND_FLASH_CONFIG2_INT_DONE
	beq	do_wait_op_done
	bx	lr     // do_wait_op_done

nfc_data_output:
	ldrh	r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]
	orr	r3, r3, #(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN)
	strh	r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]

	strh	r8, [r12, #RAM_BUFFER_ADDRESS_REG_OFF]

	mov	r3, #FDO_PAGE_SPARE_VAL
	strh	r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
	bx	lr

#else // defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1
#endif

	/* Do nothing */
	.macro  init_spba
	.endm  /* init_spba */

	/* AIPS setup - Only setup MPROTx registers. The PACR default values are good.*/
	.macro init_aips
	/*
	 * Set all MPROTx to be non-bufferable, trusted for R/W,
	 * not forced to user-mode.
	 */
	ldr	r0, AIPS1_CTRL_BASE_ADDR_W
	ldr	r1, AIPS1_PARAM_W
	str	r1, [r0, #0x00]
	str	r1, [r0, #0x04]
	ldr	r0, AIPS2_CTRL_BASE_ADDR_W
	str	r1, [r0, #0x00]
	str	r1, [r0, #0x04]
	.endm /* init_aips */

	/* MAX (Multi-Layer AHB Crossbar Switch) setup */
	.macro init_max
	ldr	r0, MAX_BASE_ADDR_W
	/* MPR - priority for MX25 is IAHB>DAHB>USBOTG>RTIC>(SDHC2/SDMA) */
	ldr	r1, MAX_PARAM1
	str	r1, [r0, #0x000]	/* for S0 */
	str	r1, [r0, #0x100]	/* for S1 */
	str	r1, [r0, #0x200]	/* for S2 */
	str	r1, [r0, #0x300]	/* for S3 */
	str	r1, [r0, #0x400]	/* for S4 */
	/* SGPCR - always park on last master */
	ldr	r1, =0x10
	str	r1, [r0, #0x010]	/* for S0 */
	str	r1, [r0, #0x110]	/* for S1 */
	str	r1, [r0, #0x210]	/* for S2 */
	str	r1, [r0, #0x310]	/* for S3 */
	str	r1, [r0, #0x410]	/* for S4 */
	/* MGPCR - restore default values */
	ldr	r1, =0x0
	str	r1, [r0, #0x800]	/* for M0 */
	str	r1, [r0, #0x900]	/* for M1 */
	str	r1, [r0, #0xA00]	/* for M2 */
	str	r1, [r0, #0xB00]	/* for M3 */
	str	r1, [r0, #0xC00]	/* for M4 */
	.endm /* init_max */

	/* Clock setup */
	.macro    init_clock
	ldr	r0, CCM_BASE_ADDR_W

	/* default CLKO to 1/6 of the USB PLL */
	ldr	r1, [r0, #CLKCTL_MCR]
	bic	r1, r1, #0x00F00000
	bic	r1, r1, #0x7F000000
	mov	r2,	#0x45000000 /* set CLKO divider to 6 */
	add	r2, r2, #0x00600000 /* select usb_clk clock source for CLKO */
	orr	r1, r1, r2
	str	r1, [r0, #CLKCTL_MCR]

	ldr	r1, CCM_CCTL_VAL_W
	str	r1, [r0, #CLKCTL_CCTL] /* configure ARM clk */

	/* enable all the clocks */
	ldr	r2, CCM_CGR0_W
	str	r2, [r0, #CLKCTL_CGR0]
	ldr	r2, CCM_CGR1_W
	str	r2, [r0, #CLKCTL_CGR1]
	ldr	r2, CCM_CGR2_W
	str	r2, [r0, #CLKCTL_CGR2]
	.endm /* init_clock */

	/* M3IF setup */
	.macro init_m3if
	/* Configure M3IF registers */
	ldr	r1, M3IF_BASE_W
	/*
	* M3IF Control Register (M3IFCTL) for MX25
	* MRRP[0] = LCDC	    on priority list (1 << 0)  = 0x00000001
	* MRRP[1] = MAX1	not on priority list (0 << 1)  = 0x00000000
	* MRRP[2] = MAX0	not on priority list (0 << 2)  = 0x00000000
	* MRRP[3] = USB HOST	not on priority list (0 << 3)  = 0x00000000
	* MRRP[4] = SDMA	not on priority list (0 << 4)  = 0x00000000
	* MRRP[5] = SD/ATA/FEC	not on priority list (0 << 5)  = 0x00000000
	* MRRP[6] = SCMFBC	not on priority list (0 << 6)  = 0x00000000
	* MRRP[7] = CSI		not on priority list (0 << 7)  = 0x00000000
	*                                                       ----------
	*                                                       0x00000001
	*/
	ldr	r0, =0x00000001
	str	r0, [r1]  /* M3IF control reg */
	.endm /* init_m3if */

	.macro setup_sdram
	ldr	r0, IOMUXC_BASE_ADDR_W
	mov	r1, #0x800
	str	r1, [r0, #0x454]

	ldr	r0, ESDCTL_BASE_W
	mov	r1, #RAM_BANK0_BASE
	bl	setup_sdram_bank
#ifdef RAM_BANK1_SIZE
	mov	r1, #RAM_BANK1_BASE
	bl	setup_sdram_bank
#endif
	.endm

	.macro nfc_cmd_input
	strh	r3, [r12, #NAND_FLASH_CMD_REG_OFF]
	mov	r3, #NAND_FLASH_CONFIG2_FCMD_EN;
	strh	r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
	bl	do_wait_op_done
	.endm   // nfc_cmd_input

	.macro do_addr_input
	and	r3, r3, #0xFF
	strh	r3, [r12, #NAND_FLASH_ADD_REG_OFF]
	mov	r3, #NAND_FLASH_CONFIG2_FADD_EN
	strh	r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
	bl	do_wait_op_done
	.endm   // do_addr_input

	/* To support 133MHz DDR */
	.macro  init_iomuxc
	mov	r0, #0x2
	ldr	r1, IOMUXC_BASE_ADDR_W
	add	r1, r1, #0x368
	add	r2, r1, #0x4C8 - 0x368
1:
	str	r0, [r1], #4
	cmp	r1, r2
	ble	1b
	.endm /* init_iomuxc */

#define ESDCTL_NORMAL	(0 << 28)
#define ESDCTL_PCHG	(1 << 28)
#define ESDCTL_AREF	(2 << 28)
#define ESDCTL_LMOD	(3 << 28)
#define ESDCTL_SLFRFSH	(4 << 28)

#define RA_BITS		2	/* row addr bits - 11 */
#define CA_BITS		1	/* 0-2: col addr bits - 8 3: rsrvd */
#define DSIZ		1	/* 0: D[31..16] 1: D[15..D0] 2: D[31..0] 3: rsrvd */
#define SREFR		3	/* 0: disabled 1-5: 2^n rows/clock *: rsrvd */
#define PWDT		1	/* 0: disabled 1: precharge pwdn
				   2: pwdn after 64 clocks 3: pwdn after 128 clocks */
#define FP		0	/* 0: not full page 1: full page */
#define BL		1	/* 0: 4(not for LPDDR) 1: 8 */
#define PRCT		0	/* 0: disabled *: clks / 2 (0..63) */
#define ESDCTLVAL	(0x80000000 | (RA_BITS << 24) | (CA_BITS << 20) |		\
			 (DSIZ << 16) | (SREFR << 13) | (PWDT << 10) | (FP << 8) |	\
			 (BL << 7) | (PRCT << 0))

#define tXP		0	/* clks - 1 (0..3) */ // N/A
#define tWTR		0	/* clks - 1 (0..1) */ // N/A
#define tRP		2	/* clks - 1 (0..3) */ // 2
#define tMRD		1	/* clks - 1 (0..3) */ // 1
#define tWR		0	/* clks - 2 (0..1) */ // 0
#define tRAS		5	/* clks - 1 (0..7) */ // 5
#define tRRD		1	/* clks - 1 (0..3) */ // 1
#define tCAS		3	/* 0: 3 clks[LPDDR] 1: rsrvd *: clks (2..3) */ // 3
#define tRCD		2	/* clks - 1 (0..7) */ // 2
#define tRC		8	/* 0: 20 *: clks - 1 (0..15) */ // 8

#define ESDCFGVAL	((tXP << 21) | (tWTR << 20) | (tRP << 18) | (tMRD << 16) |	\
			 (tWR << 15) | (tRAS << 12) | (tRRD << 10) | (tCAS << 8) |	\
			 (tRCD << 4) | (tRC << 0))

/*
 * r0: control base, r1: ram bank base
 * r3, r4: working
 */
setup_sdram_bank:
	mov	r3, #(1 << 1)		/* SDRAM controller reset */
	str	r3, [r0, #ESDCTL_ESDMISC]
1:
	ldr	r3, [r0, #ESDCTL_ESDMISC]
	tst	r3, #(1 << 31)
	beq	1b

	ldr	r3, ESDCTL_CONFIG
	cmp	r1, #RAM_BANK1_BASE
	movhs	r2, #0xc		// bank 1 ESDCFG offset
	movlo	r2, #0x4		// bank 0 ESDCFG offset
	str	r3, [r0, r2]
	sub	r2, r2, #4		// adjust to ESDCTL offset

	ldr	r3, ESDCTL_CMD_PRECHARGE
	str	r3, [r0, r2]
	str	r3, [r1, #(1 << 10)]	// precharge all command

	ldr	r3, ESDCTL_CMD_AUTOREFR
	str	r3, [r0, r2]
	.rept	2
	ldrb	r3, [r1]		// perform auto refresh cycles
	.endr

	ldr	r3, ESDCTL_CMD_MODEREG
	str	r3, [r0, r2]
	strb	r3, [r1, #((tCAS << 4) | (FP << 2) | 0x03)]		// load mode reg via A0..A11

	ldr	r3, ESDCTL_CMD_NORMAL
	str	r3, [r0, r2]

	mov	pc, lr

#define PLATFORM_VECTORS	 _platform_vectors
	.macro  _platform_vectors
	.globl	_KARO_MAGIC
_KARO_MAGIC:
	.ascii	"KARO_CE6"
	.globl	_KARO_STRUCT_SIZE
_KARO_STRUCT_SIZE:
	.word	0	// reserve space structure length

	.globl	_KARO_CECFG_START
_KARO_CECFG_START:
	.rept	1024/4
	.word	0	// reserve space for CE configuration
	.endr

	.globl	_KARO_CECFG_END
_KARO_CECFG_END:
	.endm

//Internal Boot, from MMC/SD cards or NAND flash
#ifdef INTERNAL_BOOT_MODE
#define DCDGEN(type, addr, data) \
	.long	type			 ;\
	.long	addr			 ;\
	.long	data

#define FHEADER_OFFSET 0x400

#ifdef RAM_BANK1_SIZE
#define PHYS_ADDR(a)	((a) - RAM_BANK0_BASE - RAM_BANK0_SIZE + RAM_BANK1_BASE)
#else
#define PHYS_ADDR(a)	(a)
#endif

	.macro setup_flash_header
	b	reset_vector
#if defined(FHEADER_OFFSET)
	.org    FHEADER_OFFSET
#endif
app_code_jump_v:	.long PHYS_ADDR(reset_vector)
app_code_barker:	.long 0xB1
app_code_csf:		.long 0
hwcfg_ptr_ptr:		.long PHYS_ADDR(hwcfg_ptr)
super_root_key:		.long 0
hwcfg_ptr:		.long PHYS_ADDR(dcd_data)
#ifdef RAM_BANK1_SIZE
app_dest_ptr:		.long RAM_BANK1_BASE + RAM_BANK1_SIZE - REDBOOT_OFFSET
#else
app_dest_ptr:		.long RAM_BANK0_BASE + RAM_BANK0_SIZE - REDBOOT_OFFSET
#endif
dcd_data:		.long 0xB17219E9
			.long (dcd_end - .)

// real dcd data table
// SDRAM
DCDGEN(4, 0xB8001010, 0x00000000)	// disable mDDR
DCDGEN(4, 0xB8001000, 0x92100000)	// precharge command
DCDGEN(1, 0x80000400, 0x00000000)	// precharge all dummy write
DCDGEN(4, 0xB8001000, 0xA2100000)	// auto-refresh command
DCDGEN(4, 0x80000000, 0x00000000)	// dummy write for refresh
DCDGEN(4, 0x80000000, 0x00000000)	// dummy write for refresh
DCDGEN(4, 0x80000000, 0x00000000)	// dummy write for refresh
DCDGEN(4, 0x80000000, 0x00000000)	// dummy write for refresh
DCDGEN(4, 0x80000000, 0x00000000)	// dummy write for refresh
DCDGEN(4, 0x80000000, 0x00000000)	// dummy write for refresh
DCDGEN(4, 0x80000000, 0x00000000)	// dummy write for refresh
DCDGEN(4, 0x80000000, 0x00000000)	// dummy write for refresh
DCDGEN(4, 0xB8001000, 0xB2100000)	// Load Mode Register command - cas=3 bl=8
DCDGEN(1, 0x80000033, 0x00)		// dummy write -- address has the mode bits
// 
// For DDR clock speed max = 133 MHz, HYB18M1G320BF-7.5 memory
// based on data sheet HYx18M1G16x_BF_rev100.pdf.
// 
// ESDCTL0=0x82216880:
//  SDE=1	ESDRAM Controller Enable: Enabled
//  SMODE=000	SDRAM Controller Operating Mode: Normal Read/Write
//  SP=0	Supervisor Protect: User mode accesses allowed
//  ROW=010	Row Address Width: 13 Row Addresses
//  COL=10	Column Address Width: 10 Column Addresses
//  DSIZ=01	SDRAM Memory Data Width: 16-bit memory width aligned to D[15:0]
//  SREFR=011	SDRAM Refresh Rate: 4 rows each refresh clock,
//			8192 rows/64 mS @ 32 kHz
//			7.81 uS row rate at 32 kHz
//  PWDT=10	Power Down Timer: 64 clocks (HCLK) after completion of last access
//			with Active Power Down (most aggressive)
//  FP=0	Full Page: Not Full Page
//  BL=1	Burst Length: 8
//  PRCT=000000 Precharge Timer: Disabled
//
DCDGEN(4, 0xB8001000, ESDCTLVAL)
//
// ESDCFG0=0x00295728:
//   tXP=01	LPDDR exit power down to next valid command delay: 2 clocks
//   tWTR=0	LPDDR WRITE to READ Command Delay: 1 clock
//   tRP=10	SDRAM Row Precharge Delay: 3 clocks
//   tMRD=01	SDRAM Load Mode Register to ACTIVE Command: 2 clocks
//   tWR=0	SDRAM WRITE to PRECHARGE Command: 2 clocks
//   tRAS=101	SDRAM ACTIVE to PRECHARGE Command: 6 clocks
//   tRRD=01	ACTIVE Bank A to ACTIVE Bank B Command: 2 clocks
//   tCAS=11	SDRAM CAS Latency: 3 clocks
//   tRCD=010	SDRAM Row to Column Delay: 3 clocks
//   tRC=1000	SDRAM Row Cycle Delay: 9 clocks
//
DCDGEN(4, 0xB8001004, ESDCFGVAL)

// CLK 
DCDGEN(4, 0x53F80008, 0x20034000)	// CLKCTL ARM=400 AHB=133
dcd_end:

//CARD_FLASH_CFG_PARMS_T---length
card_cfg:
	.long REDBOOT_IMAGE_SIZE
	.endm
#endif

SDRAM_ADDR_MASK:	.word	0xFFFC0000
#ifdef RAM_BANK1_SIZE
MXC_REDBOOT_RAM_START:	.word	RAM_BANK1_BASE + RAM_BANK1_SIZE - REDBOOT_OFFSET
#else
MXC_REDBOOT_RAM_START:	.word	RAM_BANK0_BASE + RAM_BANK0_SIZE - REDBOOT_OFFSET
#endif
AIPS1_CTRL_BASE_ADDR_W: .word	AIPS1_CTRL_BASE_ADDR
AIPS2_CTRL_BASE_ADDR_W: .word	AIPS2_CTRL_BASE_ADDR
AIPS1_PARAM_W:		.word	0x77777777
MAX_BASE_ADDR_W:	.word	MAX_BASE_ADDR
MAX_PARAM1:		.word	0x00043210
ESDCTL_BASE_W:		.word	ESDCTL_BASE_ADDR
M3IF_BASE_W:		.word	M3IF_BASE
ESDCTL_CMD_NORMAL:	.word	ESDCTLVAL | ESDCTL_NORMAL
ESDCTL_CMD_AUTOREFR:	.word	ESDCTLVAL | ESDCTL_AREF
ESDCTL_CMD_PRECHARGE:	.word	ESDCTLVAL | ESDCTL_PCHG
ESDCTL_CMD_MODEREG:	.word	ESDCTLVAL | ESDCTL_LMOD
ESDCTL_CONFIG:		.word	ESDCFGVAL
IOMUXC_BASE_ADDR_W:	.word	IOMUXC_BASE_ADDR
GPIO1_BASE_ADDR_W:	.word	GPIO1_BASE_ADDR
GPIO2_BASE_ADDR_W:	.word	GPIO2_BASE_ADDR
#ifdef USE_SERIAL
UART1_BASE_ADDR_W:	.word	UART1_BASE_ADDR
UART1_UBIR_W:		.word	0x0f
UART1_UBMR_W:		.word	0x2e
#endif
CCM_CGR0_W:		.word	0x1FFFFFFF
CCM_CGR1_W:		.word	0xFFFFFFFF
CCM_CGR2_W:		.word	0x000FDFFF
CCM_BASE_ADDR_W:	.word	CCM_BASE_ADDR
CCM_CCTL_VAL_W:		.word	0x20034000
/*-----------------------------------------------------------------------*/
/* end of hal_platform_setup.h						 */
#endif /* CYGONCE_HAL_PLATFORM_SETUP_H */
