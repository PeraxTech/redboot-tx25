#ifndef CYGONCE_HAL_BOARD_PLATFORM_PLF_MMAP_H
#define CYGONCE_HAL_BOARD_PLATFORM_PLF_MMAP_H
//=============================================================================
//
//      plf_mmap.h
//
//      Platform specific memory map support
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/hal/hal_misc.h>
#include CYGHWR_MEMORY_LAYOUT_H

// Get the pagesize for a particular virtual address:

// This does not depend on the vaddr.
#define HAL_MM_PAGESIZE(vaddr, pagesize)		\
	CYG_MACRO_START								\
	(pagesize) = SZ_1M;							\
	CYG_MACRO_END

// Get the physical address from a virtual address:

#define HAL_VIRT_TO_PHYS_ADDRESS(vaddr, paddr)			\
	CYG_MACRO_START										\
	paddr = hal_virt_to_phy((unsigned long)(vaddr));	\
	CYG_MACRO_END

/*
 * translate the virtual address of ram space to physical address
 * It is dependent on the implementation of hal_mmu_init
 */
#ifndef RAM_BANK0_SIZE
#warning using SDRAM_SIZE for RAM_BANK0_SIZE
#define RAM_BANK0_SIZE		SDRAM_SIZE
#endif

static unsigned long __inline__ hal_virt_to_phy(unsigned long virt)
{
	/* SDRAM mappings:
	 * virt               -> phys
	 * 00000000..01ffffff -> 80000000..81ffffff
	 * 02000000..03ffffff -> 90000000..91ffffff
	 * 80000000..81ffffff -> 80000000..81ffffff
	 * 82000000..83ffffff -> 90000000..91ffffff
	 * 88000000..89ffffff -> 80000000..81ffffff (uncached)
	 * 8a000000..8bffffff -> 90000000..91ffffff (uncached)
	 */
	if (virt < SDRAM_SIZE) {
		return virt | (virt < RAM_BANK0_SIZE ? CSD0_BASE_ADDR : CSD1_BASE_ADDR);
	}
	if ((virt & 0xF0000000) == CSD0_BASE_ADDR) {
		virt &= ~0x08000000; /* clear uncached mapping indicator */
		if (virt >= CSD0_BASE_ADDR + RAM_BANK0_SIZE) {
			virt = virt - (CSD0_BASE_ADDR + RAM_BANK0_SIZE) + CSD1_BASE_ADDR;
		}
	}
	return virt;
}

/*
 * remap the physical address of ram space to uncacheable virtual address space
 * It is dependent on the implementation of hal_mmu_init
 */
static unsigned long __inline__ hal_ioremap_nocache(unsigned long phy)
{
	/* 0x88000000~0x8BFFFFFF is uncacheable memory space which is mapped to SDRAM */
	if ((phy & 0xF0000000) == CSD0_BASE_ADDR) {
		phy |= 0x08000000;
	}
	if ((phy & 0xF0000000) == CSD1_BASE_ADDR) {
		phy = (phy - CSD1_BASE_ADDR + CSD0_BASE_ADDR + RAM_BANK0_SIZE) | 0x08000000;
	}
	return phy;
}

//---------------------------------------------------------------------------
#endif // CYGONCE_HAL_BOARD_PLATFORM_PLF_MMAP_H
