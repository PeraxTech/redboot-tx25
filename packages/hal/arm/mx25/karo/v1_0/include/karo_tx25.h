#ifndef CYGONCE_KARO_TX25_H
#define CYGONCE_KARO_TX25_H

//=============================================================================
//
//	Platform specific support (register layout, etc)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/hal/hal_soc.h>	// Hardware definitions

#include CYGHWR_MEMORY_LAYOUT_H

#define SZ_1K				0x00000400
#define SZ_2K				0x00000800
#define SZ_4K				0x00001000
#define SZ_8K				0x00002000
#define SZ_16K				0x00004000
#define SZ_32K				0x00008000
#define SZ_64K				0x00010000
#define SZ_128K				0x00020000
#define SZ_256K				0x00040000
#define SZ_512K				0x00080000
#define SZ_1M				0x00100000
#define SZ_2M				0x00200000
#define SZ_4M				0x00400000
#define SZ_8M				0x00800000
#define SZ_16M				0x01000000
#define SZ_32M				0x02000000
#define SZ_64M				0x04000000
#define SZ_128M				0x08000000
#define SZ_256M				0x10000000
#define SZ_512M				0x20000000
#define SZ_1G				0x40000000

#define TX25_LED_MASK		(1 << 7)
#define TX25_LED_REG_ADDR	(GPIO2_BASE_ADDR + GPIO_DR)

#define LCD_BACKLIGHT_GPIO	(0 * 32 + 26)
#define LCD_RESET_GPIO		(1 * 32 + 4)
#define LCD_POWER_GPIO		(1 * 32 + 5)

#define LED_MAX_NUM		1

#define LED_IS_ON(n) ({							\
	CYG_WORD32 __val;							\
	HAL_READ_UINT32(TX25_LED_REG_ADDR, __val);	\
	__val & TX25_LED_MASK;						\
})

#define TURN_LED_ON(n)							\
	CYG_MACRO_START								\
	CYG_WORD32 __val;							\
	HAL_READ_UINT32(TX25_LED_REG_ADDR, __val);	\
	__val |= TX25_LED_MASK;						\
	HAL_WRITE_UINT32(TX25_LED_REG_ADDR, __val);	\
	CYG_MACRO_END

#define TURN_LED_OFF(n)							\
	CYG_MACRO_START								\
	CYG_WORD32 __val;							\
	HAL_READ_UINT32(TX25_LED_REG_ADDR, __val);	\
	__val &= ~TX25_LED_MASK;					\
	HAL_WRITE_UINT32(TX25_LED_REG_ADDR, __val);	\
	CYG_MACRO_END

#define BOARD_DEBUG_LED(n)						\
	CYG_MACRO_START								\
	if (n >= 0 && n < LED_MAX_NUM) {			\
		if (LED_IS_ON(n))						\
			TURN_LED_OFF(n);					\
		else									\
			TURN_LED_ON(n);						\
	}											\
	CYG_MACRO_END

#define BLINK_LED(l, n)							\
	CYG_MACRO_START								\
	int _i;										\
	for (_i = 0; _i < (n); _i++) {				\
		BOARD_DEBUG_LED(l);						\
		HAL_DELAY_US(200000);					\
		BOARD_DEBUG_LED(l);						\
		HAL_DELAY_US(300000);					\
	}											\
	HAL_DELAY_US(1000000);						\
	CYG_MACRO_END

#if !defined(__ASSEMBLER__)
enum {
	BOARD_TYPE_TX25KARO,
};

#include <cyg/infra/cyg_ass.h>

static inline CYG_ADDRESS MX25_GPIO_ADDR(int grp)
{

	switch (grp) {
	case 1:
		return GPIO1_BASE_ADDR;
	case 2:
		return GPIO2_BASE_ADDR;
	case 3:
		return GPIO3_BASE_ADDR;
	case 4:
		return GPIO4_BASE_ADDR;
	default:
		return 0;
	}
}

#define gpio_tst_bit(grp, gpio)		_gpio_tst_bit(grp, gpio, __FUNCTION__, __LINE__)
static inline int _gpio_tst_bit(int grp, int gpio, const char *func, int line)
{
	unsigned long val;
	CYG_ADDRESS reg = MX25_GPIO_ADDR(grp);

	CYG_ASSERT(reg != 0, "Invalid GPIO register bank");
	if (!reg)
		return 0;
	CYG_ASSERT(gpio >= 0 && gpio < 32, "Invalid GPIO register number");
	if (gpio < 0 || gpio > 31) {
		return 0;
	}
	val = readl(reg + GPIO_PSR0);
	return !!(val & (1 << gpio));
}

static inline void gpio_set_bit(int grp, int gpio)
{
	unsigned long val;
	CYG_ADDRESS reg = MX25_GPIO_ADDR(grp);

	CYG_ASSERT(reg != 0, "Invalid GPIO register bank");
	if (!reg)
		return;
	CYG_ASSERT(gpio >= 0 && gpio < 32, "Invalid GPIO register number");
	if (gpio < 0 || gpio > 31) {
		return;
	}
	val = readl(reg + GPIO_DR);
	writel(val | (1 << gpio), reg + GPIO_DR);
}

static inline void gpio_clr_bit(int grp, int gpio)
{
	unsigned long val;
	CYG_ADDRESS reg = MX25_GPIO_ADDR(grp);

	CYG_ASSERT(reg != 0, "Invalid GPIO register bank");
	if (!reg)
		return;
	CYG_ASSERT(gpio >= 0 && gpio < 32, "Invalid GPIO register number");
	if (gpio < 0 || gpio > 31) {
		return;
	}
	val = readl(reg + GPIO_DR);
	writel(val & ~(1 << gpio), reg + GPIO_DR);
}

static inline void gpio_direction_output(int grp, int gpio, int level)
{
	unsigned long val;
	CYG_ADDRESS reg = MX25_GPIO_ADDR(grp);

	CYG_ASSERT(reg != 0, "Invalid GPIO register bank");
	if (!reg)
		return;
	CYG_ASSERT(gpio >= 0 && gpio < 32, "Invalid GPIO register number");
	if (gpio < 0 || gpio > 31) {
		return;
	}
	val = readl(reg + GPIO_DR);
	if (level)
		val |= 1 << gpio;
	else
		val &= ~(1 << gpio);
	writel(val, reg + GPIO_DR);

	val = readl(reg + GPIO_GDIR);
	writel(val | (1 << gpio), reg + GPIO_GDIR);
}

static inline void gpio_direction_input(int grp, int gpio)
{
	unsigned long val;
	CYG_ADDRESS reg = MX25_GPIO_ADDR(grp);

	CYG_ASSERT(reg != 0, "Invalid GPIO register bank");
	if (!reg)
		return;
	CYG_ASSERT(gpio >= 0 && gpio < 32, "Invalid GPIO register number");
	if (gpio < 0 || gpio > 31) {
		return;
	}
	val = readl(reg + GPIO_GDIR);
	writel(val & ~(1 << gpio), reg + GPIO_GDIR);
}
#endif

#endif /* CYGONCE_KARO_TX25_H */
