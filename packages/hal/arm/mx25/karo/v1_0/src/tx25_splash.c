//==========================================================================
//
//      tx25_bootsplash.c
//
//      TX25 Bootsplash Implementation
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================

#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#ifdef CYGPKG_REDBOOT
#include <redboot.h>
#endif
#include <cyg/infra/cyg_type.h>
#include <cyg/io/flash.h>
#ifdef CYGSEM_REDBOOT_FLASH_CONFIG
#include <flash_config.h>
#endif
#ifdef CYGOPT_REDBOOT_FIS
#include <fis.h>
#endif
#include CYGBLD_HAL_PLATFORM_H
#include <cyg/hal/plf_mmap.h>
#include CYGBLD_HAL_PLF_DEFS_H		// Platform specific hardware definitions

#define LCDC_BASE			0x53fbc000

#define LCDC_SSA			0x00

#define LCDC_SIZE			0x04
#define SIZE_XMAX(x)		((((x) >> 4) & 0x3f) << 20)

#define SIZE_YMAX(y)		((y) & 0x3ff)

#define LCDC_VPW			0x08
#define VPW_VPW(x)			((x) & 0x3ff)

#define LCDC_CPOS			0x0C
#define CPOS_CC1			(1 << 31)
#define CPOS_CC0			(1 << 30)
#define CPOS_OP				(1 << 28)
#define CPOS_CXP(x)			(((x) & 3ff) << 16)

#define CPOS_CYP(y)			((y) & 0x3ff)

#define LCDC_LCWHB			0x10
#define LCWHB_BK_EN			(1 << 31)
#define LCWHB_CW(w)			(((w) & 0x1f) << 24)
#define LCWHB_CH(h)			(((h) & 0x1f) << 16)
#define LCWHB_BD(x)			((x) & 0xff)

#define LCDC_LCHCC			0x14

#define LCHCC_CUR_COL_R(r)	(((r) & 0x3f) << 12)
#define LCHCC_CUR_COL_G(g)	(((g) & 0x3f) << 6)
#define LCHCC_CUR_COL_B(b)	((b) & 0x3f)

#define LCDC_PCR			0x18

#define PCR_TFT		(1 << 31)
#define PCR_COLOR	(1 << 30)
#define PCR_PBSIZ_1	(0 << 28)
#define PCR_PBSIZ_2	(1 << 28)
#define PCR_PBSIZ_4	(2 << 28)
#define PCR_PBSIZ_8	(3 << 28)
#define PCR_BPIX_1	(0 << 25)
#define PCR_BPIX_2	(1 << 25)
#define PCR_BPIX_4	(2 << 25)
#define PCR_BPIX_8	(3 << 25)
#define PCR_BPIX_12	(4 << 25)
#define PCR_BPIX_16	(5 << 25)
#define PCR_BPIX_18	(6 << 25)
#define PCR_PIXPOL	(1 << 24)
#define PCR_FLMPOL	(1 << 23)
#define PCR_LPPOL	(1 << 22)
#define PCR_CLKPOL	(1 << 21)
#define PCR_OEPOL	(1 << 20)
#define PCR_SCLKIDLE	(1 << 19)
#define PCR_END_SEL	(1 << 18)
#define PCR_END_BYTE_SWAP (1 << 17)
#define PCR_REV_VS	(1 << 16)
#define PCR_ACD_SEL	(1 << 15)
#define PCR_ACD(x)	(((x) & 0x7f) << 8)
#define PCR_SCLK_SEL	(1 << 7)
#define PCR_SHARP	(1 << 6)
#define PCR_PCD(x)	((x) & 0x3f)

#define LCDC_HCR			0x1C
#define HCR_H_WIDTH(x)		(((x) & 0x3f) << 26)
#define HCR_H_WAIT_1(x)		(((x) & 0xff) << 8)
#define HCR_H_WAIT_2(x)		((x) & 0xff)

#define LCDC_VCR			0x20
#define VCR_V_WIDTH(x)		(((x) & 0x3f) << 26)
#define VCR_V_WAIT_1(x)		(((x) & 0xff) << 8)
#define VCR_V_WAIT_2(x)		((x) & 0xff)

#define LCDC_POS			0x24
#define POS_POS(x)			((x) & 1f)

#define LCDC_LSCR1			0x28

#define LCDC_PWMR			0x2C

#define LCDC_DMACR			0x30

#define LCDC_RMCR			0x34

#define RMCR_SELF_REF		(1 << 0)

#define LCDC_LCDICR			0x38
#define LCDICR_INT_SYN		(1 << 2)
#define LCDICR_INT_CON		(1)

#define LCDC_LCDISR			0x40
#define LCDISR_UDR_ERR		(1 << 3)
#define LCDISR_ERR_RES		(1 << 2)
#define LCDISR_EOF			(1 << 1)
#define LCDISR_BOF			(1 << 0)

#define BMP_MAGIC			0x4D42
#define BMP_HEADER_V3_SIZE	sizeof(V3BITMAPINFOHEADER)

typedef struct lcd_display_metrics {
	cyg_uint16 *frame_buffer;
	unsigned long pixclk;
	unsigned long width;
	unsigned long height;
	unsigned long hsync;
	unsigned long vsync;
	unsigned long bpp;
	/* margin settings */
	unsigned long top;
	unsigned long bottom;
	unsigned long left;
	unsigned long right;
	/* control signal polarities */
	bool pix_pol;
	bool clk_pol;	
	bool hsync_pol;
	bool vsync_pol;
} LCDDIM;

typedef struct {
	cyg_uint32		size;
	cyg_int32		width;
	cyg_int32		height;
	cyg_uint16		planes;
	cyg_uint16		bit_count;
	cyg_uint32		compression;
	cyg_uint32		size_image;
	cyg_int32		x_pels_per_meter;
	cyg_int32		y_pels_per_meter;
	cyg_uint32		clr_used;
	cyg_uint32		clr_important;
} V3BITMAPINFOHEADER;

typedef struct {
	cyg_uint16	type;			// Signatur
	cyg_uint32	size;			// Groesse der datei
	cyg_uint16	reserved[2];
	cyg_uint32	offset;			// Offset der Pixeldaten
} __attribute__((packed)) BITMAPFILEHEADER;

#define lcd_reg_write(val, reg)	__lcd_reg_write(val, reg, #reg)
static inline void __lcd_reg_write(cyg_uint32 val, unsigned short reg,
								const char *name)
{
	writel(val, LCDC_BASE + reg);
}

static inline cyg_uint32 lcd_reg_read(unsigned short reg)
{
	cyg_uint32 val;

	val = readl(LCDC_BASE + reg);
	return val;
}


static bool get_var(const char *name, void *val, int type)
{
	bool ok;

	ok = CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
									(char *)name, val, type);
	if (!ok) {
		diag_printf("fconfig variable %s not set; fconfig -i is needed!\n",
					name);
	}
	return ok;
}

static inline void lcdc_enable(int on)
{
	cyg_uint32 cgcr1 = readl(CCM_BASE_ADDR + CLKCTL_CGR1);

	if (on) {
		cgcr1 |= 1 << 29;
	} else {
		cgcr1 &= ~(1 << 29);
	}
	writel(cgcr1, CCM_BASE_ADDR + CLKCTL_CGR1);
}

static inline int bytes_per_pixel(unsigned long bpp)
{
	switch (bpp) {
	case 8:
		return 1;

	case 16:
		return 2;

	default:
		return 4;
	}
}

static void lcd_pin_cfg(void)
{
#if 0
	writel(1, IOMUXC_BASE_ADDR + 0x088);
	writel(1, IOMUXC_BASE_ADDR + 0x08c);
	writel(0, IOMUXC_BASE_ADDR + 0x0c8);
	writel(0, IOMUXC_BASE_ADDR + 0x0cc);
	writel(0, IOMUXC_BASE_ADDR + 0x0d0);
	writel(0, IOMUXC_BASE_ADDR + 0x0d4);
	writel(0, IOMUXC_BASE_ADDR + 0x0d8);
	writel(0, IOMUXC_BASE_ADDR + 0x0dc);
	writel(0, IOMUXC_BASE_ADDR + 0x0e0);
	writel(0, IOMUXC_BASE_ADDR + 0x0e4);
	writel(0, IOMUXC_BASE_ADDR + 0x0e8);
	writel(0, IOMUXC_BASE_ADDR + 0x0ec);
	writel(0, IOMUXC_BASE_ADDR + 0x0f0);
	writel(0, IOMUXC_BASE_ADDR + 0x0f4);
	writel(0, IOMUXC_BASE_ADDR + 0x0f8);
	writel(0, IOMUXC_BASE_ADDR + 0x0fc);
	writel(0, IOMUXC_BASE_ADDR + 0x100);
	writel(0, IOMUXC_BASE_ADDR + 0x104);
	writel(0, IOMUXC_BASE_ADDR + 0x108);
	writel(0, IOMUXC_BASE_ADDR + 0x10c);
	writel(0, IOMUXC_BASE_ADDR + 0x110);
	writel(0, IOMUXC_BASE_ADDR + 0x114);
#endif
}

/* evaluate the config parameters */
static bool bootsplash_setup(LCDDIM *ldim, unsigned long img_len)
{
	bool ok;

	ok = get_var("lcd_buffer_addr", &ldim->frame_buffer, CONFIG_INT);
	ok &= get_var("lcd_clk_period", &ldim->pixclk, CONFIG_INT);
	ok &= get_var("lcd_clk_polarity", &ldim->clk_pol, CONFIG_BOOL);
	ok &= get_var("lcd_pix_polarity", &ldim->pix_pol, CONFIG_BOOL);

	ok &= get_var("lcd_panel_width", &ldim->width, CONFIG_INT);
	ok &= get_var("lcd_panel_height", &ldim->height, CONFIG_INT);
	ok &= get_var("lcd_bpp", &ldim->bpp, CONFIG_INT);

	ok &= get_var("lcd_hsync_width", &ldim->hsync, CONFIG_INT);
	ok &= get_var("lcd_hsync_polarity", &ldim->hsync_pol, CONFIG_BOOL);
	ok &= get_var("lcd_margin_left", &ldim->left, CONFIG_INT);
	ok &= get_var("lcd_margin_right", &ldim->right, CONFIG_INT);

	ok &= get_var("lcd_vsync_width", &ldim->vsync, CONFIG_INT);
	ok &= get_var("lcd_vsync_polarity", &ldim->vsync_pol, CONFIG_BOOL);
	ok &= get_var("lcd_margin_top", &ldim->top, CONFIG_INT);
	ok &= get_var("lcd_margin_bottom", &ldim->bottom, CONFIG_INT);

	if (!ok)
		return false;

	if (ldim->width < 16 || ldim->width > 1024) {
		diag_printf("Invalid LCD panel width: %lu\n", ldim->width);
		ok = false;
	}

	if (ldim->height < 1 || ldim->height > 511) {
		diag_printf("Invalid LCD panel height: %lu\n", ldim->height);
		ok = false;
	}

	if (ldim->hsync < 1 || ldim->hsync > 64) {
		diag_printf("Invalid HSYNC width: %lu\n", ldim->hsync);
		ok = false;
	}

	if (ldim->vsync > 63) {
		diag_printf("Invalid VSYNC width: %lu\n", ldim->vsync);
		ok = false;
	}

	if (ldim->left < 3 || ldim->left > 258) {
		diag_printf("Invalid left margin: %lu\n", ldim->left);
		ok = false;
	}

	if (ldim->right < 1 || ldim->right > 256) {
		diag_printf("Invalid right margin: %lu\n", ldim->right);
		ok = false;
	}

	if (ldim->top > 255) {
		diag_printf("Invalid top margin: %lu\n", ldim->top);
		ok = false;
	}

	if (ldim->bottom > 255) {
		diag_printf("Invalid bottom margin: %lu\n", ldim->bottom);
		ok = false;
	}

	switch (ldim->bpp) {
	default:
		diag_printf("Invalid color depth: %lu\n", ldim->bpp);
		return false;

	case 16:
	case 8:
		break;
	}

	if (ldim->width * ldim->height * bytes_per_pixel(ldim->bpp) > img_len) {
		diag_printf("LCD panel size %lux%lux%u = %lu does not match image size %lu\n",
					ldim->width, ldim->height, bytes_per_pixel(ldim->bpp),
					ldim->width * ldim->height * bytes_per_pixel(ldim->bpp),
					img_len);
		ok = false;
	}

	return ok;
}

/* initialize the LCD controller */
static bool bootsplash_display(LCDDIM *ldim)
{
	unsigned long lcdc_clk = get_peri_clock(LCDC_CLK);
	unsigned long pcr;
	CYG_ADDRESS disp_addr_phys;

	HAL_VIRT_TO_PHYS_ADDRESS(ldim->frame_buffer, disp_addr_phys);

	diag_printf("LCD %lux%lu-%lu %lu.%03luMHz\n",
				ldim->width, ldim->height, ldim->bpp,
				1000000 / ldim->pixclk,
				1000000000 / ldim->pixclk % 1000);

	ldim->pixclk = 1000000000 / ldim->pixclk * 1000;
	if (ldim->pixclk > lcdc_clk / 2) {
		diag_printf("LCD pixel clock %lu too high; max.: %lu\n",
					ldim->pixclk, lcdc_clk / 2);
		return false;
	}

	pcr = lcdc_clk / ldim->pixclk;
	if (pcr-- > 63) {
		diag_printf("LCD pixel clock %lu too low; min.: %lu\n",
					ldim->pixclk, lcdc_clk / 128);
		return false;
	}

	pcr |= PCR_TFT | PCR_COLOR | PCR_SCLK_SEL |
		(ldim->vsync_pol * PCR_FLMPOL) |
		(ldim->hsync_pol * PCR_LPPOL) |
		(ldim->pix_pol * PCR_PIXPOL) |
		(ldim->clk_pol * PCR_CLKPOL);

	switch (ldim->bpp) {
	case 18:
		pcr |= PCR_PBSIZ_8 | PCR_BPIX_18;
		break;

	case 16:
		pcr |= PCR_PBSIZ_8 | PCR_BPIX_16;
		break;

	case 8:
		pcr |= PCR_BPIX_8 | PCR_END_BYTE_SWAP;
		break;
	}

	lcdc_enable(0);
	lcd_pin_cfg();

	lcd_reg_write(VPW_VPW(ldim->width * ldim->bpp / 8 / 4), LCDC_VPW);
	lcd_reg_write(HCR_H_WIDTH(ldim->hsync - 1) | HCR_H_WAIT_2(ldim->left - 3) |
				HCR_H_WAIT_1(ldim->right - 1), LCDC_HCR);
	lcd_reg_write(VCR_V_WIDTH(ldim->vsync) | VCR_V_WAIT_2(ldim->top) |
				VCR_V_WAIT_1(ldim->bottom), LCDC_VCR);
	lcd_reg_write(SIZE_XMAX(ldim->width) | SIZE_YMAX(ldim->height), LCDC_SIZE);
	lcd_reg_write(pcr, LCDC_PCR);
	lcd_reg_write(0, LCDC_PWMR);
	lcd_reg_write(0, LCDC_LSCR1);
	lcd_reg_write(0x80040060, LCDC_DMACR);

	lcd_reg_write(disp_addr_phys, LCDC_SSA);
	lcd_reg_write(0, LCDC_POS);
	lcd_reg_write(0, LCDC_CPOS);
	lcd_reg_write(0, LCDC_RMCR);

	lcdc_enable(1);

	while (!(lcd_reg_read(LCDC_LCDISR) & LCDISR_EOF));

	gpio_set_bit(LCD_POWER_GPIO / 32 + 1, LCD_POWER_GPIO % 32);
	gpio_set_bit(LCD_RESET_GPIO / 32 + 1, LCD_RESET_GPIO % 32);

	HAL_DELAY_US(300000);
	gpio_clr_bit(LCD_BACKLIGHT_GPIO / 32 + 1, LCD_BACKLIGHT_GPIO % 32);

	return true;
}

static void set_pixel(LCDDIM *ldim, unsigned short x, unsigned short y,
			unsigned char red, unsigned char green, unsigned char blue)
{
	unsigned int offset = y * ldim->width + x;
	unsigned short *fb = (unsigned short *)(ldim->frame_buffer + offset);

	*fb = ((red & 0x1f) << 11) |
		((green & 0x3f) << 5) |
		((blue & 0x1f) << 0);
}

static unsigned long paint_test_picture(LCDDIM *ldim)
{
	const int stripes = 8;
	const unsigned int fill_size = ldim->width * ldim->height / stripes;
	cyg_uint16 *frame_pointer = ldim->frame_buffer;
	int x;

	memset(frame_pointer, 0, fill_size * bytes_per_pixel(ldim->bpp));
	frame_pointer += fill_size;

	for (x = 0; x < fill_size; x++) {
		*frame_pointer++ = 0xf800;			// red screen
	}

	for (x = 0; x < fill_size; x++) {
		*frame_pointer++ = 0x07e0;			// green screen
	}

	for (x = 0; x < fill_size; x++) {
		*frame_pointer++ = 0x001f;			// blue screen
	}

	for (x = 0; x < fill_size; x++) {
		*frame_pointer++ = 0x07ff;			// yellow screen
	}

	for (x = 0; x < fill_size; x++) {
		*frame_pointer++ = 0xf81f;			// magenta screen
	}

	for (x = 0; x < fill_size; x++) {
		*frame_pointer++ = 0xffe0;			// cyan screen
	}

	memset(frame_pointer, 0xff, fill_size * bytes_per_pixel(ldim->bpp));
	frame_pointer += fill_size;

	// draw a white frame
	for (x = 0; x < ldim->width; x++)
		set_pixel(ldim, x, 0,
				0xff, 0xff, 0xff);

	for (x = 0; x < ldim->height; x++)
		set_pixel(ldim, ldim->width - 1, x,
				0xff, 0xff, 0xff);

	for (x = 0; x < ldim->width; x++)
		set_pixel(ldim, x, ldim->height - 1,
				0xff, 0xff, 0xff);

	for (x = 0; x < ldim->height; x++)
		set_pixel(ldim, 0, x,
				0xff, 0xff, 0xff);

	// diagonal lines
	for (x = 0; x < ldim->width; x++)
		set_pixel(ldim, x, x * ldim->height / ldim->width,
				(x & 1) * 0xff, (x & 1) * 0xff, (x & 1) * 0xff);

	for (x = 0; x < ldim->width; x++)
		set_pixel(ldim, ldim->width - x - 1, x * ldim->height / ldim->width,
				(x & 1) * 0xff, (x & 1) * 0xff, (x & 1) * 0xff);
	return (frame_pointer - ldim->frame_buffer) * bytes_per_pixel(ldim->bpp);
}

static void load_bitmap(LCDDIM *ldim, void *ram_addr,
						unsigned long img_len)
{
	cyg_uint8		*sp;
	cyg_uint8		red, green, blue;
	cyg_uint16		*ppixel;

	BITMAPFILEHEADER	bmfh;
	V3BITMAPINFOHEADER	bmih;
	int line, col;

	/* copy bitmap file header */
	memcpy(&bmfh, ram_addr, sizeof(bmfh));

	/* copy bitmap info header */
	memcpy(&bmih, (cyg_uint8 *)ram_addr + sizeof(bmfh), sizeof(bmih));

	if (bmfh.type != 0x4d42) {
		if (ldim->width * ldim->height * bytes_per_pixel(ldim->bpp) > img_len) {
			diag_printf("LCD panel size %lux%lux%u = %lu does not match image size %lu\n",
						ldim->width, ldim->height, bytes_per_pixel(ldim->bpp),
						ldim->width * ldim->height * bytes_per_pixel(ldim->bpp),
						img_len);
			goto end_failure;
		}
		memcpy(ldim->frame_buffer, ram_addr, img_len);
		return;
	}

	if (bmfh.offset >= bmfh.size) {
		diag_printf("Corrupted BMP header\n");
		goto end_failure;
	}

	if (bmih.size != BMP_HEADER_V3_SIZE ) {
		diag_printf("** ERROR: unsupported bitmap header version, structure size must be %u\n",
					BMP_HEADER_V3_SIZE);
		goto end_failure;
	}

	if (bmih.bit_count != 24) {
		diag_printf("** ERROR: only 24 bit BMP files are supported\n");
		goto end_failure;
	}

	if ((bmih.width != ldim->width) || (bmih.height != ldim->height)) {
		diag_printf("** ERROR: BMP image size %ux%u does not match configured size: %lux%lu\n",
					bmih.width, bmih.height, ldim->width, ldim->height);
		goto end_failure;
	}

	/* pointer to picture data */
	sp = (cyg_uint8 *)ram_addr + bmfh.offset;

	if (bmih.bit_count == 16) {
		for (line = ldim->height; line > 0; line--) {
			memcpy(ldim->frame_buffer + (line - 1) * ldim->width,
				sp, ldim->width * 2);
			sp += ldim->width * 2;
		}
		return;
	}

	for (line = ldim->height; line > 0; line--) {
		ppixel = ldim->frame_buffer + (line - 1) * ldim->width;
		for (col = 0; col < ldim->width; col ++) {
			blue	= *sp++ >> 3;	// 5 bits
			green	= *sp++ >> 2;	// 6 bits
			red		= *sp++ >> 3;	// 5 bits
			*ppixel++	= ((red << 11) | (green << 5) | (blue << 0));
		}
	}
	return;

end_failure:
	paint_test_picture(ldim);
}

/*
 * load the logo from nand flash to memory.
 */
static bool do_logo_load(LCDDIM *ldim)
{
	void *fis_addr;
	int ret = 0xFF;
	void *err_addr;
	unsigned int logo_size;
	struct fis_image_desc *img;
	void *ram_addr;

	/* Read the logo from storage media */
	if ((img = fis_lookup("logo", NULL)) == NULL) {
		diag_printf("No logo partition found in the fis table, logo not loaded\n");
		return false;
	}

	fis_addr = (void *)img->flash_base;
	ram_addr = (void *)img->mem_base;

	logo_size = img->data_length;
	ret = FLASH_READ(fis_addr, ram_addr, logo_size, &err_addr);
	if (ret != 0) {
		diag_printf("Loading logo from FLASH to MEMORY failed. error code: %d", ret);
		return false;
	}
	if (!bootsplash_setup(ldim, logo_size)) {
		diag_printf("Cannot init splash screen; Invalid display parameters\n");
		return false;
	}
	load_bitmap(ldim, ram_addr, logo_size);
	bootsplash_display(ldim);
	return true;
}

static void redboot_bootsplash_display(void)
{
	bool bootsplash_feature_enable;
	int ok;
	LCDDIM ldim;

	gpio_set_bit(LCD_RESET_GPIO / 32, LCD_RESET_GPIO % 32);
	gpio_set_bit(LCD_POWER_GPIO / 32, LCD_POWER_GPIO % 32);
	gpio_set_bit(LCD_BACKLIGHT_GPIO / 32, LCD_BACKLIGHT_GPIO % 32);

	ok = CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
									"bootsplash_enable",
									&bootsplash_feature_enable, CONFIG_BOOL);
	if (!ok) {
		diag_printf("fconfig variable bootsplash_enable not found\n");
	}
	if (ok && bootsplash_feature_enable) {
		do_logo_load(&ldim);
	}
}

#ifdef CYGPKG_REDBOOT
RedBoot_init(redboot_bootsplash_display, RedBoot_INIT_SECOND);

RedBoot_config_option("Enable splash display at boot",
					bootsplash_enable,
					ALWAYS_ENABLED, true,
					CONFIG_BOOL,
					false
	);

RedBoot_config_option("LCD frame buffer address",
					lcd_buffer_addr,
					"bootsplash_enable", true,
					CONFIG_INT,
					RAM_BANK0_BASE + RAM_BANK0_SIZE
	);

RedBoot_config_option("Pixel clock period (in ps)",
					lcd_clk_period,
					"bootsplash_enable", true,
					CONFIG_INT,
					33333
	);

RedBoot_config_option("Pixel polarity active low",
					lcd_pix_polarity,
					"bootsplash_enable", true,
					CONFIG_BOOL,
					false
	);

RedBoot_config_option("Pixel clock polarity active low",
					lcd_clk_polarity,
					"bootsplash_enable", true,
					CONFIG_BOOL,
					false
	);

RedBoot_config_option("LCD panel width (in pixels)",
					lcd_panel_width,
					"bootsplash_enable", true,
					CONFIG_INT,
					640
	);

RedBoot_config_option("LCD panel height (in pixels)",
					lcd_panel_height,
					"bootsplash_enable", true,
					CONFIG_INT,
					480
	);

RedBoot_config_option("LCD color depth (BPP)",
					lcd_bpp,
					"bootsplash_enable", true,
					CONFIG_INT,
					16
	);

RedBoot_config_option("HSYNC pulse width (in pixels)",
					lcd_hsync_width,
					"bootsplash_enable", true,
					CONFIG_INT,
					64
	);

RedBoot_config_option("HSYNC polarity active low",
					lcd_hsync_polarity,
					"bootsplash_enable", true,
					CONFIG_BOOL,
					true
	);

RedBoot_config_option("Left margin (in pixels)",
					lcd_margin_left,
					"bootsplash_enable", true,
					CONFIG_INT,
					96
	);

RedBoot_config_option("Right margin (in pixels)",
					lcd_margin_right,
					"bootsplash_enable", true,
					CONFIG_INT,
					80
	);

RedBoot_config_option("VSYNC pulse width (in scan lines)",
					lcd_vsync_width,
					"bootsplash_enable", true,
					CONFIG_INT,
					3
	);

RedBoot_config_option("VSYNC polarity active low",
					lcd_vsync_polarity,
					"bootsplash_enable", true,
					CONFIG_BOOL,
					true
	);

RedBoot_config_option("Top margin (in scan lines)",
					lcd_margin_top,
					"bootsplash_enable", true,
					CONFIG_INT,
					46
	);

RedBoot_config_option("Bottom margin (in scan lines)",
					lcd_margin_bottom,
					"bootsplash_enable", true,
					CONFIG_INT,
					39
	);
#endif
