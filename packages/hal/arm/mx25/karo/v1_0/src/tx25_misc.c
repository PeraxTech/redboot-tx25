//==========================================================================
//
//	tx25_misc.c
//
//	HAL misc board support code for the tx25
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <stdlib.h>
#include <redboot.h>
#include <string.h>
#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>		// base types
#include <cyg/infra/cyg_trac.h>		// tracing macros
#include <cyg/infra/cyg_ass.h>		// assertion macros

#include <cyg/hal/hal_io.h>		// IO macros
#include <cyg/hal/hal_arch.h>		// Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>		// Interrupt names
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_soc.h>		// Hardware definitions
#include CYGBLD_HAL_PLF_DEFS_H		// Platform specifics

#include <cyg/infra/diag.h>		// diag_printf

// All the MM table layout is here:
#include <cyg/hal/hal_mm.h>

//#define EARLY_SERIAL_SETUP

#ifdef EARLY_SERIAL_SETUP
static void mxc_serial_setup(void);

static void uart_putc(int c)
{
	while (readl(UART1_BASE_ADDR + 0xb4) & (1 << 4)) {
		/* wait for !TX_FULL */
	}
	writel(c, UART1_BASE_ADDR + 0x40);
}

static void putcr(void)
{
	uart_putc('\r');
	uart_putc('\n');
}

static void printhex(unsigned int x, int len)
{
	int i;

	for (i = len - 1; i >= 0; i--) {
		int c = (x >> (i * 4)) & 0xf;
		if (c > 9) {
			c += 'A' - 10;
		} else {
			c += '0';
		}
		uart_putc(c);
	}
}

static void dump_reg(unsigned long addr)
{
	printhex(addr, 8);
	uart_putc('=');
	printhex(readl(addr), 8);
	putcr();
}
#endif

#define SD_SZ		(RAM_BANK0_SIZE >> 20)
#ifdef RAM_BANK1_SIZE
#define SD_B0		0x800
#define SD_B1		(SD_B0 + SD_SZ)
#define SD_B2		(SD_B0 + 0x80 + SD_SZ)
#define SD_HI		(0x900 + ((RAM_BANK1_SIZE >> 20) - 1))
#endif

void hal_mmu_init(void)
{
	unsigned long ttb_base = RAM_BANK0_BASE + 0x4000;
	unsigned long i;

	/*
	 * Set the TTB register
	 */
	asm volatile ("mcr  p15, 0, %0, c2, c0, 0" : : "r"(ttb_base));

	/*
	 * Set the Domain Access Control Register
	 */
	i = ARM_ACCESS_DACR_DEFAULT;
	asm volatile ("mcr  p15, 0, %0, c3, c0, 0" : : "r"(i));

	/*
	 * First clear all TT entries - ie Set them to Faulting
	 */
	memset((void *)ttb_base, 0, ARM_FIRST_LEVEL_PAGE_TABLE_SIZE);

	/*                Phys   Virt   Size              Attributes                              Function      */
	/*	              Base   Base   MB     cached?          buffered?	      access permissions            */
	/*	                xxx00000 */
	X_ARM_MMU_SECTION(0x000, 0xF00, 0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* Boot Rom */
	X_ARM_MMU_SECTION(0x43f, 0x43f, 0x3c1, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* Internal Registers */
	X_ARM_MMU_SECTION(0x800, 0x000, SD_SZ, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(0x800, 0x800, SD_SZ, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(0x800, 0x880, SD_SZ, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* SDRAM */
#ifdef RAM_BANK1_SIZE
	X_ARM_MMU_SECTION(0x900, SD_SZ, SD_SZ, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(0x900, SD_B1, SD_SZ, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	X_ARM_MMU_SECTION(0x900, SD_B2, SD_SZ, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* SDRAM */
	/* make sure the last MiB in the upper bank of SDRAM (where RedBoot resides)
	 * has a unity mapping (required when switching MMU on) */
	X_ARM_MMU_SECTION(SD_HI, SD_HI, 0x001, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RO_RO); /* SDRAM bank1 identity mapping */
#endif
	X_ARM_MMU_SECTION(0xB20, 0xB20, 0x0E0, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* ESDCTL, WEIM, M3IF, EMI, NFC, External I/O */
#ifdef EARLY_SERIAL_SETUP
	mxc_serial_setup();
#endif
}

static inline void set_reg(unsigned long addr, CYG_WORD32 set, CYG_WORD32 clr)
{
	CYG_WORD32 val;
	HAL_READ_UINT32(addr, val);
	val = (val & ~clr) | set;
	HAL_WRITE_UINT32(addr, val);
}

//
// Platform specific initialization
//
static void fec_gpio_init(void)
{
	/* GPIOs to set up for TX25/Starterkit-5:
	   Function       GPIO  Dir  act.  FCT
                                 Lvl
	   FEC_RESET      PB30  OUT  LOW   GPIO
	   FEC_ENABLE     PB27  OUT  HIGH  GPIO
	   OSCM26_ENABLE  PB22  OUT  HIGH  GPIO
	   EXT_WAKEUP     PB24  IN   HIGH  GPIO
	   FEC_TXEN       PF23  OUT  OUT   AIN
	   FEC_TXCLK      PD11  OUT  IN    AOUT
	   ...
	 */
#define OCR_SHIFT(bit)		(((bit) * 2) % 32)
#define OCR_MASK(bit)		(3 << (OCR_SHIFT(bit)))
#define OCR_VAL(bit,val)	(((val) << (OCR_SHIFT(bit))) & (OCR_MASK(bit)))
#define GPR_SHIFT(bit)		(bit)
#define GPR_MASK(bit)		(1 << (GPR_SHIFT(bit)))
#define GPR_VAL(bit,val)	(((val) << (GPR_SHIFT(bit))) & (GPR_MASK(bit)))
#define ICONF_SHIFT(bit)	(((bit) * 2) % 32)
#define ICONF_MASK(bit)		(3 << (ICONF_SHIFT(bit)))
#define ICONF_VAL(bit,val)	(((val) << (ICONF_SHIFT(bit))) & (ICONF_MASK(bit)))

	/*
	 * make sure the ETH PHY strap pins are pulled to the right voltage
	 * before deasserting the PHY reset GPIO
	 */
	unsigned long val;

	/* FEC_TX_CLK */
	writel(0, IOMUXC_BASE_ADDR + 0x01E8);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x03E0);

	/* FEC_RX_DV */
	writel(0, IOMUXC_BASE_ADDR + 0x01E4);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x03DC);

	/* FEC_RDATA0 */
	writel(0, IOMUXC_BASE_ADDR + 0x01DC);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x03D4);

	/* FEC_TDATA0 */
	writel(0, IOMUXC_BASE_ADDR + 0x01D0);
	writel(0x40, IOMUXC_BASE_ADDR + 0x03C8);

	/* FEC_TX_EN */
	writel(0, IOMUXC_BASE_ADDR + 0x01D8);
	writel(0x40, IOMUXC_BASE_ADDR + 0x03D0);

	/* FEC_MDC */
	writel(0, IOMUXC_BASE_ADDR + 0x01C8);
	writel(0x40, IOMUXC_BASE_ADDR + 0x03C0);

	/* FEC_MDIO */
	writel(0, IOMUXC_BASE_ADDR + 0x01CC);
	writel(0x1F0, IOMUXC_BASE_ADDR + 0x03C4);

	/* FEC_RDATA1 */
	writel(0, IOMUXC_BASE_ADDR + 0x01E0);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x03D8);

	/* FEC_TDATA1 */
	writel(0, IOMUXC_BASE_ADDR + 0x01D4);
	writel(0x40, IOMUXC_BASE_ADDR + 0x03CC);

	/*
	 * Set up the FEC_RESET_B and FEC_ENABLE GPIO pins.
	 * Assert FEC_RESET_B, then power up the PHY by asserting
	 * FEC_ENABLE, at the same time lifting FEC_RESET_B.
	 *
	 * FEC_RESET_B: gpio4[7] is ALT 5 mode of pin D13
	 * FEC_ENABLE_B: gpio4[9] is ALT 5 mode of pin D11
	 */
	writel(0x15, IOMUXC_BASE_ADDR + 0x0090);
	writel(0x15, IOMUXC_BASE_ADDR + 0x0098);

	writel(0x8, IOMUXC_BASE_ADDR + 0x0288); // open drain
	writel(0x0, IOMUXC_BASE_ADDR + 0x0290); // cmos, no pu/pd

	/* drop PHY power and assert reset (low) */
	val = readl(GPIO4_BASE_ADDR + GPIO_DR) & ~((1 << 7) | (1 << 9));
	writel(val, GPIO4_BASE_ADDR + GPIO_DR);

	/* make the pins output */
	val = readl(GPIO4_BASE_ADDR + GPIO_GDIR) | (1 << 7) | (1 << 9);
	writel(val, GPIO4_BASE_ADDR + GPIO_GDIR);
}

//
// Platform specific initialization
//

unsigned int g_clock_src;
unsigned int g_board_type = BOARD_TYPE_TX25KARO;

static void mxc_serial_setup(void)
{
	// UART1
	/*RXD1*/
	writel(0, IOMUXC_BASE_ADDR + 0x170);
	writel(0x1E0, IOMUXC_BASE_ADDR + 0x368);

	/*TXD1*/
	writel(0, IOMUXC_BASE_ADDR + 0x174);
	writel(0x40, IOMUXC_BASE_ADDR + 0x36c);

	/*RTS1*/
	writel(0, IOMUXC_BASE_ADDR + 0x178);
	writel(0x1E0, IOMUXC_BASE_ADDR + 0x370);

	/*CTS1*/
	writel(0, IOMUXC_BASE_ADDR + 0x17c);
	writel(0x40, IOMUXC_BASE_ADDR + 0x374);
}

void plf_hardware_init(void)
{
	g_clock_src = FREQ_24MHZ;
	mxc_serial_setup();
	fec_gpio_init();
}

#define SOC_FBAC0_REG			IIM_BASE_ADDR
#define SOC_MAC_ADDR_LOCK_BIT	2

extern int fuse_blow(int bank, int row, int bit);

int tx25_mac_addr_program(unsigned char mac_addr[ETHER_ADDR_LEN])
{
	int ret = 0;
	int i;

	for (i = 0; i < ETHER_ADDR_LEN; i++) {
		unsigned char fuse = readl(SOC_MAC_ADDR_BASE + (i << 2));

		if ((fuse | mac_addr[i]) != mac_addr[i]) {
			diag_printf("MAC address fuse cannot be programmed: fuse[%d]=0x%02x -> 0x%02x\n",
						i, fuse, mac_addr[i]);
			return -1;
		}
		if (fuse != mac_addr[i]) {
			ret = 1;
		}
	}
	if (ret == 0) {
		return ret;
	}
	if (readl(SOC_FBAC0_REG) & (1 << SOC_MAC_ADDR_LOCK_BIT)) {
		diag_printf("MAC address is locked\n");
		return -1;
	}
	for (i = 0; i < ETHER_ADDR_LEN; i++) {
		int bit;
		unsigned char fuse = readl(SOC_MAC_ADDR_BASE + (i << 2));

		for (bit = 0; bit < 8; bit++) {
			if (((mac_addr[i] >> bit) & 0x1) == 0)
				continue;
			if (((mac_addr[i] >> bit) & 1) == ((fuse >> bit) & 1)) {
				continue;
			}
			if (fuse_blow(0, i + ((SOC_MAC_ADDR_BASE & 0xff) >> 2), bit)) {
				diag_printf("Failed to blow fuse bank 0 row %d bit %d\n",
							i, bit);
				ret = -1;
				goto out;
			}
		}
		if ((fuse = readl(SOC_MAC_ADDR_BASE + (i << 2))) != mac_addr[i]) {
			diag_printf("Fuse[%d] verify failed; wrote: 0x%02x read: 0x%02x\n",
						i + 26, mac_addr[i], fuse);
			ret = -1;
			goto out;
		}
	}
	fuse_blow(0, 0, SOC_MAC_ADDR_LOCK_BIT);
out:
	return ret;
}

#include CYGHWR_MEMORY_LAYOUT_H

typedef void code_fun(void);

void tx25_program_new_stack(void *func)
{
	register CYG_ADDRESS stack_ptr asm("sp");
	register CYG_ADDRESS old_stack asm("r4");
	register code_fun *new_func asm("r0");
	old_stack = stack_ptr;
	stack_ptr = CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE - sizeof(CYG_ADDRESS);
	new_func = (code_fun*)func;
	new_func();
	stack_ptr = old_stack;
}

static void display_clock_src(void)
{
	diag_printf("Clock input is 24 MHz\n");
}

#define WDOG_WRSR	((CYG_WORD16 *)(WDOG_BASE_ADDR + 0x4))
#define CRM_RCSR	((CYG_WORD32 *)(CCM_BASE_ADDR + 0x28))

static void display_board_type(void)
{
	diag_printf("\nBoard Type: SOM-TX25 on PXIETHUSB by PERAX Technologies\n");
}

static void display_board_info(void)
{
	display_board_type();
	display_clock_src();
}
RedBoot_init(display_board_info, RedBoot_INIT_LAST);
