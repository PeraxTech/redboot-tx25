//==========================================================================
//
//      board_misc.c
//
//      HAL misc board support code for the board
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#include <pkgconf/hal.h>
#include <pkgconf/system.h>
#include <redboot.h>
#include CYGBLD_HAL_PLATFORM_H

#include <cyg/infra/cyg_type.h>         // base types
#include <cyg/infra/cyg_trac.h>         // tracing macros
#include <cyg/infra/cyg_ass.h>          // assertion macros

#include <cyg/hal/hal_io.h>             // IO macros
#include <cyg/hal/hal_arch.h>           // Register state info
#include <cyg/hal/hal_diag.h>
#include <cyg/hal/hal_intr.h>           // Interrupt names
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_soc.h>         // Hardware definitions
#include <cyg/hal/fsl_board.h>             // Platform specifics

#include <cyg/infra/diag.h>             // diag_printf

// All the MM table layout is here:
#include <cyg/hal/hal_mm.h>

externC void* memset(void *, int, size_t);
static void mxc_fec_setup(void);
static void mxc_serial_setup(void);

void hal_mmu_init(void)
{
    unsigned long ttb_base = RAM_BANK0_BASE + 0x4000;
    unsigned long i;

    /*
     * Set the TTB register
     */
    asm volatile ("mcr  p15,0,%0,c2,c0,0" : : "r"(ttb_base) /*:*/);

    /*
     * Set the Domain Access Control Register
     */
    i = ARM_ACCESS_DACR_DEFAULT;
    asm volatile ("mcr  p15,0,%0,c3,c0,0" : : "r"(i) /*:*/);

    /*
     * First clear all TT entries - ie Set them to Faulting
     */
    memset((void *)ttb_base, 0, ARM_FIRST_LEVEL_PAGE_TABLE_SIZE);

    /*              Actual   Virtual  Size   Attributes                                                    Function  */
    /*              Base     Base     MB     cached?           buffered?        access permissions                 */
    /*              xxx00000 xxx00000                                                                                */
    X_ARM_MMU_SECTION(0x000, 0xF00,   0x001, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* ROM */
    X_ARM_MMU_SECTION(0x400, 0x400,   0x400, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* Internal Regsisters upto SDRAM*/
    X_ARM_MMU_SECTION(0x800, 0x000,   0x080, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM 0:128M*/
    X_ARM_MMU_SECTION(0x800, 0x800,   0x080, ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* SDRAM 0:128M*/
    X_ARM_MMU_SECTION(0x800, 0x880,   0x080, ARM_UNCACHEABLE, ARM_UNBUFFERABLE, ARM_ACCESS_PERM_RW_RW); /* SDRAM 0:128M*/
    X_ARM_MMU_SECTION(0xB00, 0xB00,   0x20,  ARM_CACHEABLE,   ARM_BUFFERABLE,   ARM_ACCESS_PERM_RW_RW); /* PSRAM */
    X_ARM_MMU_SECTION(0xB20, 0xB20,   0x1E0, ARM_UNCACHEABLE, ARM_UNBUFFERABLE,ARM_ACCESS_PERM_RW_RW); /* ESDCTL, WEIM, M3IF, EMI, NFC, External I/O */
}

//
// Platform specific initialization
//

unsigned int g_clock_src;

void plf_hardware_init(void)
{
    g_clock_src = FREQ_24MHZ;

    mxc_serial_setup();
    mxc_fec_setup();
}

static void mxc_serial_setup(void)
{
	// UART1
	/*RXD1*/
	writel(0, IOMUXC_BASE_ADDR + 0x170);
	writel(0x1E0, IOMUXC_BASE_ADDR + 0x368);

	/*TXD1*/
	writel(0, IOMUXC_BASE_ADDR + 0x174);
	writel(0x40, IOMUXC_BASE_ADDR + 0x36c);

	/*RTS1*/
	writel(0, IOMUXC_BASE_ADDR + 0x178);
	writel(0x1E0, IOMUXC_BASE_ADDR + 0x370);

	/*CTS1*/
	writel(0, IOMUXC_BASE_ADDR + 0x17c);
	writel(0x40, IOMUXC_BASE_ADDR + 0x374);
}

static void mxc_fec_setup(void)
{
	unsigned int val;

	/* FEC_TX_CLK */
	writel(0, IOMUXC_BASE_ADDR + 0x01E8);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x03E0);

	/* FEC_RX_DV */
	writel(0, IOMUXC_BASE_ADDR + 0x01E4);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x03DC);

	/* FEC_RDATA0 */
	writel(0, IOMUXC_BASE_ADDR + 0x01DC);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x03D4);

	/* FEC_TDATA0 */
	writel(0, IOMUXC_BASE_ADDR + 0x01D0);
	writel(0x40, IOMUXC_BASE_ADDR + 0x03C8);

	/* FEC_TX_EN */
	writel(0, IOMUXC_BASE_ADDR + 0x01D8);
	writel(0x40, IOMUXC_BASE_ADDR + 0x03D0);

	/* FEC_MDC */
	writel(0, IOMUXC_BASE_ADDR + 0x01C8);
	writel(0x40, IOMUXC_BASE_ADDR + 0x03C0);

	/* FEC_MDIO */
	writel(0, IOMUXC_BASE_ADDR + 0x01CC);
	writel(0x1F0, IOMUXC_BASE_ADDR + 0x03C4);

	/* FEC_RDATA1 */
	writel(0, IOMUXC_BASE_ADDR + 0x01E0);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x03D8);

	/* FEC_TDATA1 */
	writel(0, IOMUXC_BASE_ADDR + 0x01D4);
	writel(0x40, IOMUXC_BASE_ADDR + 0x03CC);

	/* 
	 * Set up the FEC_RESET_B and FEC_ENABLE GPIO pins.
	 * Assert FEC_RESET_B, then power up the PHY by asserting
	 * FEC_ENABLE, at the same time lifting FEC_RESET_B.
	 *
	 * FEC_RESET_B: gpio2[3] is ALT 5 mode of pin A17
	 * FEC_ENABLE_B: gpio4[8] is ALT 5 mode of pin D12
	 */
	writel(0x5, IOMUXC_BASE_ADDR + 0x001C);
	writel(0x5, IOMUXC_BASE_ADDR + 0x0094);

	writel(0x8, IOMUXC_BASE_ADDR + 0x0238); // open drain
	writel(0x0, IOMUXC_BASE_ADDR + 0x028C); // cmos, no pu/pd

	/* make the pins output */
	val = (1 << 3) | readl(GPIO2_BASE_ADDR + GPIO_GDIR);
	writel(val, GPIO2_BASE_ADDR + GPIO_GDIR);

	val = (1 << 8) | readl(GPIO4_BASE_ADDR + GPIO_GDIR);
	writel(val, GPIO4_BASE_ADDR + GPIO_GDIR);

	/* drop PHY power */
	val = readl(GPIO2_BASE_ADDR + GPIO_DR) & ~(1 << 3);
	writel(val, GPIO2_BASE_ADDR + GPIO_DR);

	/* assert reset */
	val = readl(GPIO4_BASE_ADDR + GPIO_DR) & ~(1 << 8);
	writel(val, GPIO4_BASE_ADDR + GPIO_DR);
	hal_delay_us(2);	// spec says 1us min

	/* turn on power & lift reset */
	val = (1 << 3) | readl(GPIO2_BASE_ADDR + GPIO_DR);
	writel(val, GPIO2_BASE_ADDR + GPIO_DR);
	val = (1 << 8) | readl(GPIO4_BASE_ADDR + GPIO_DR);
	writel(val, GPIO4_BASE_ADDR + GPIO_DR);
}

static void mxc_cspi_setup(void)
{
	/*CSPI1*/
	/*SCLK*/
	writel(0, IOMUXC_BASE_ADDR + 0x180);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x5c4);
	/*SPI_RDY*/
	writel(0, IOMUXC_BASE_ADDR + 0x184);
	writel(0x1E0, IOMUXC_BASE_ADDR + 0x5c8);
	/*MOSI*/
	writel(0, IOMUXC_BASE_ADDR + 0x170);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x5b4);
	/*MISO*/
	writel(0, IOMUXC_BASE_ADDR + 0x174);
	writel(0x1C0, IOMUXC_BASE_ADDR + 0x5b8);
	/*SS1*/
	writel(0, IOMUXC_BASE_ADDR + 0x17C);
	writel(0x1E0, IOMUXC_BASE_ADDR + 0x5C0);
}

void mxc_i2c_init(unsigned int module_base)
{
	switch(module_base) {
	case I2C_BASE_ADDR:
		/* Pins: SION */
		writel(0x10, IOMUXC_BASE_ADDR + 0x150);		/* I2C1_CLK */
		writel(0x10, IOMUXC_BASE_ADDR + 0x154);		/* I2C1_DAT */

		/* Pads: HYS, 100k Pull-up, open drain */
		writel(0x1E8, IOMUXC_BASE_ADDR + 0x348);	/* I2C1_CLK */
		writel(0x1E8, IOMUXC_BASE_ADDR + 0x34c);	/* I2C1_DAT */
		break;
	case I2C2_BASE_ADDR:
		/* Pins: ALT1 (of FEC_RDATA1, FEC_RX_DV pins), SION */
		writel(0x11, IOMUXC_BASE_ADDR + 0x1e0);		/* I2C2_CLK */
		writel(0x11, IOMUXC_BASE_ADDR + 0x1e4);		/* I2C2_DAT */

		/* Pads: HYS, 100k Pull-up, open drain */
		writel(0x1E8, IOMUXC_BASE_ADDR + 0x3d8);	/* I2C2_CLK */
		writel(0x1E8, IOMUXC_BASE_ADDR + 0x3dc);	/* I2C2_DAT */
		break;
	case I2C3_BASE_ADDR:
		/* Pins: ALT2 (of HSYNC, VSYNC pins), SION */
		writel(0x12, IOMUXC_BASE_ADDR + 0x108);		/* I2C3_CLK */
		writel(0x12, IOMUXC_BASE_ADDR + 0x10c);		/* I2C3_DAT */

		/* Pads: HYS, 100k Pull-up, open drain */
		writel(0x1E8, IOMUXC_BASE_ADDR + 0x300);	/* I2C2_CLK */
		writel(0x1E8, IOMUXC_BASE_ADDR + 0x304);	/* I2C2_DAT */
		break;
	default:
		break;
	}
}
void mxc_mmc_init(base_address)
{
	unsigned int val;

	switch(base_address) {
	case MMC_SDHC1_BASE_ADDR:
		/* Pins */
		writel(0x10, IOMUXC_BASE_ADDR + 0x190);	/* SD1_CMD */
		writel(0x10, IOMUXC_BASE_ADDR + 0x194);	/* SD1_CLK */
		writel(0x00, IOMUXC_BASE_ADDR + 0x198);	/* SD1_DATA0 */
		writel(0x00, IOMUXC_BASE_ADDR + 0x19c);	/* SD1_DATA1 */
		writel(0x00, IOMUXC_BASE_ADDR + 0x1a0);	/* SD1_DATA2 */
		writel(0x00, IOMUXC_BASE_ADDR + 0x1a4);	/* SD1_DATA3 */
		writel(0x06, IOMUXC_BASE_ADDR + 0x094);	/* D12 (SD1_DATA4) */
		writel(0x06, IOMUXC_BASE_ADDR + 0x090);	/* D13 (SD1_DATA5) */
		writel(0x06, IOMUXC_BASE_ADDR + 0x08c);	/* D14 (SD1_DATA6) */
		writel(0x06, IOMUXC_BASE_ADDR + 0x088);	/* D15 (SD1_DATA7) */
		writel(0x05, IOMUXC_BASE_ADDR + 0x010);	/* A14 (SD1_WP) */
		writel(0x05, IOMUXC_BASE_ADDR + 0x014);	/* A15 (SD1_DET) */

		/* Pads */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x388);	/* SD1_CMD */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x38c);	/* SD1_CLK */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x390);	/* SD1_DATA0 */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x394);	/* SD1_DATA1 */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x398);	/* SD1_DATA2 */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x39c);	/* SD1_DATA3 */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x28c);	/* D12 (SD1_DATA4) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x288);	/* D13 (SD1_DATA5) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x284);	/* D14 (SD1_DATA6) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x280);	/* D15 (SD1_DATA7) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x230);	/* A14 (SD1_WP) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x234);	/* A15 (SD1_DET) */

		/*
		 * Set write protect and card detect gpio as inputs
		 * A14 (SD1_WP) and A15 (SD1_DET)
		 */
		val = ~(3 << 0) & readl(GPIO1_BASE_ADDR + GPIO_GDIR);
		writel(val, GPIO1_BASE_ADDR + GPIO_GDIR);

		break;
	case MMC_SDHC2_BASE_ADDR:
		/* Pins */
		writel(0x16, IOMUXC_BASE_ADDR + 0x0e8);	/* LD8 (SD1_CMD) */
		writel(0x16, IOMUXC_BASE_ADDR + 0x0ec);	/* LD9 (SD1_CLK) */
		writel(0x06, IOMUXC_BASE_ADDR + 0x0f0);	/* LD10 (SD1_DATA0) */
		writel(0x06, IOMUXC_BASE_ADDR + 0x0f4);	/* LD11 (SD1_DATA1) */
		writel(0x06, IOMUXC_BASE_ADDR + 0x0f8);	/* LD12 (SD1_DATA2) */
		writel(0x06, IOMUXC_BASE_ADDR + 0x0fc);	/* LD13 (SD1_DATA3) */
		writel(0x02, IOMUXC_BASE_ADDR + 0x120);	/* CSI_D2 (SD1_DATA4) */
		writel(0x02, IOMUXC_BASE_ADDR + 0x124);	/* CSI_D3 (SD1_DATA5) */
		writel(0x02, IOMUXC_BASE_ADDR + 0x128);	/* CSI_D4 (SD1_DATA6) */
		writel(0x02, IOMUXC_BASE_ADDR + 0x12c);	/* CSI_D5 (SD1_DATA7) */

		/* Pads */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x2e0);	/* LD8 (SD1_CMD) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x2e4);	/* LD9 (SD1_CLK) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x2e8);	/* LD10 (SD1_DATA0) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x2ec);	/* LD11 (SD1_DATA1) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x2f0);	/* LD12 (SD1_DATA2) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x2f4);	/* LD13 (SD1_DATA3) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x318);	/* CSI_D2 (SD1_DATA4) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x31c);	/* CSI_D3 (SD1_DATA5) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x320);	/* CSI_D4 (SD1_DATA6) */
		writel(0xD1, IOMUXC_BASE_ADDR + 0x324);	/* CSI_D5 (SD1_DATA7) */

	default:
		break;
	}
}

#include CYGHWR_MEMORY_LAYOUT_H

typedef void code_fun(void);

void board_program_new_stack(void *func)
{
    register CYG_ADDRESS stack_ptr asm("sp");
    register CYG_ADDRESS old_stack asm("r4");
    register code_fun *new_func asm("r0");
    old_stack = stack_ptr;
    stack_ptr = CYGMEM_REGION_ram + CYGMEM_REGION_ram_SIZE - sizeof(CYG_ADDRESS);
    new_func = (code_fun*)func;
    new_func();
    stack_ptr = old_stack;
}

static void display_clock_src(void)
{
    diag_printf("\n");
    diag_printf("Clock input is 24 MHz");
}
RedBoot_init(display_clock_src, RedBoot_INIT_LAST);

// ------------------------------------------------------------------------
