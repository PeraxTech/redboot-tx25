//==========================================================================
//
//      redboot_cmds.c
//
//      Board [platform] specific RedBoot commands
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
#include <redboot.h>
#include <cyg/hal/hal_intr.h>
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/plf_mmap.h>
#include <cyg/hal/fsl_board.h>          // Platform specific hardware definitions

#ifdef CYGSEM_REDBOOT_FLASH_CONFIG
#include <flash_config.h>

#if (REDBOOT_IMAGE_SIZE != CYGBLD_REDBOOT_MIN_IMAGE_SIZE)
#error REDBOOT_IMAGE_SIZE != CYGBLD_REDBOOT_MIN_IMAGE_SIZE
#endif

RedBoot_config_option("Board specifics",
                      brd_specs,
                      ALWAYS_ENABLED,
                      true,
                      CONFIG_INT,
                      0
                     );
#endif  //CYGSEM_REDBOOT_FLASH_CONFIG

char HAL_PLATFORM_EXTRA[55] = " PASS 1.0 [x32 DDR]";

static void runImg(int argc, char *argv[]);

RedBoot_cmd("run",
            "Run an image at a location with MMU off",
            "[<virtual addr>]",
            runImg
           );

void launchRunImg(unsigned long addr)
{
    asm volatile ("mov r12, r0;");
    HAL_MMU_OFF();
    asm volatile (
                 "mov r0, #0;"
                 "mov r1, r12;"
                 "mov r11, #0;"
                 "mov r12, #0;"
                 "mrs r10, cpsr;"
                 "bic r10, r10, #0xF0000000;"
                 "msr cpsr_f, r10;"
                 "mov pc, r1"
                 );
}

extern unsigned long entry_address;

static void runImg(int argc,char *argv[])
{
    unsigned int virt_addr, phys_addr;

    // Default physical entry point for Symbian
    if (entry_address == 0xFFFFFFFF)
        virt_addr = 0x800000;
    else
    virt_addr = entry_address;

    if (!scan_opts(argc,argv,1,0,0,(void*)&virt_addr,
                   OPTION_ARG_TYPE_NUM, "virtual address"))
        return;

    if (entry_address != 0xFFFFFFFF)
        diag_printf("load entry_address=0x%lx\n", entry_address);
    HAL_VIRT_TO_PHYS_ADDRESS(virt_addr, phys_addr);

    diag_printf("virt_addr=0x%x\n",virt_addr);
    diag_printf("phys_addr=0x%x\n",phys_addr);

    launchRunImg(phys_addr);
}

#if defined(CYGSEM_REDBOOT_FLASH_CONFIG) && defined(CYG_HAL_STARTUP_ROMRAM)

RedBoot_cmd("romupdate",
            "Update Redboot with currently running image",
            "",
            romupdate
           );

extern int flash_program(void *_addr, void *_data, int len, void **err_addr);
extern int flash_erase(void *addr, int len, void **err_addr);
extern char *flash_errmsg(int err);
extern unsigned char *ram_end; //ram end is where the redboot starts FIXME: use PC value

#ifdef CYGPKG_IO_FLASH
void romupdate(int argc, char *argv[])
{
    void *err_addr, *base_addr;
    int stat;

    if (IS_FIS_FROM_MMC()) {
        diag_printf("Updating ROM in MMC/SD flash\n");
        base_addr = (void*)0;
        /* Read the first 1K from the card */
        mmc_data_read((cyg_uint32*)ram_end, 0x400, base_addr);
        diag_printf("Programming Redboot to MMC/SD flash\n");
        mmc_data_write((cyg_uint32*)ram_end, CYGBLD_REDBOOT_MIN_IMAGE_SIZE, (cyg_uint32)base_addr);
        return;
    } else if (IS_FIS_FROM_NAND()) {
        base_addr = (void*)MXC_NAND_BASE_DUMMY;
        diag_printf("Updating ROM in NAND flash\n");
    } else if (IS_FIS_FROM_NOR()) {
        base_addr = (void*)BOARD_FLASH_START;
        diag_printf("Updating ROM in NOR flash\n");
    } else {
        diag_printf("romupdate not supported\n");
        diag_printf("Use \"factive [NOR|NAND|MMC]\" to select either NOR, NAND or MMC flash\n");
        return;
    }

    // Erase area to be programmed
    if ((stat = flash_erase((void *)base_addr,
                            CYGBLD_REDBOOT_MIN_IMAGE_SIZE,
                            (void **)&err_addr)) != 0) {
        diag_printf("Can't erase region at %p: %s\n",
                    err_addr, flash_errmsg(stat));
        return;
    }
    // Now program it
    if ((stat = flash_program((void *)base_addr, (void *)ram_end,
                              CYGBLD_REDBOOT_MIN_IMAGE_SIZE,
                              (void **)&err_addr)) != 0) {
        diag_printf("Can't program region at %p: %s\n",
                    err_addr, flash_errmsg(stat));
    }
}

RedBoot_cmd("factive",
            "Enable one flash media for Redboot",
            "[NOR | NAND | MMC]",
            factive
           );

void factive(int argc, char *argv[])
{
    unsigned long phys_addr;

    if (argc != 2) {
        diag_printf("Invalid factive cmd\n");
        return;
    }

    if (strcasecmp(argv[1], "NOR") == 0) {
#ifndef MXCFLASH_SELECT_NOR
        diag_printf("Not supported\n");
        return;
#else
        MXC_ASSERT_NOR_BOOT();
#endif
    } else if (strcasecmp(argv[1], "NAND") == 0) {
#ifndef MXCFLASH_SELECT_NAND
        diag_printf("Not supported\n");
        return;
#else
        MXC_ASSERT_NAND_BOOT();
#endif
    } else if (strcasecmp(argv[1], "MMC") == 0) {
#ifndef MXCFLASH_SELECT_MMC
        diag_printf("Not supported\n");
        return;
#else
        MXC_ASSERT_MMC_BOOT();
#endif
    } else {
        diag_printf("Invalid command: %s\n", argv[1]);
        return;
    }
    HAL_VIRT_TO_PHYS_ADDRESS(ram_end, phys_addr);

    launchRunImg(phys_addr);
}
#endif //CYGPKG_IO_FLASH
#endif /* CYG_HAL_STARTUP_ROMRAM */

static void setcorevol(int argc, char *argv[]);

RedBoot_cmd("setcorevol",
            "Set the core voltage. Setting is not checked against current core frequency.",
            "[1.16 | 1.2 | 1.23 | 1.27 | 1.3 | 1.34 | 1.38 | 1.41 | 1.45 | 1.49 | 1.52 | 1.56 | 1.59 | 1.63 | 1.67 | 1.7]",
            setcorevol
           );

static void setcorevol(int argc, char *argv[])
{
    unsigned int coreVol;

    /* check if the number of args is OK. 1 arg expected. argc = 2 */
    if(argc != 2) {
        diag_printf("Invalid argument. Need to specify a voltage\n");
        return;
    }

    /* check if the argument is valid. */
    if (strcasecmp(argv[1], "1.16") == 0) {	/* -20% */
        coreVol = 0x8;
    } else if (strcasecmp(argv[1], "1.2") == 0) {
        coreVol = 0x9;
    } else if (strcasecmp(argv[1], "1.23") == 0) {
        coreVol = 0xA;
    } else if (strcasecmp(argv[1], "1.27") == 0) {
        coreVol = 0xB;
    } else if (strcasecmp(argv[1], "1.3") == 0) {
        coreVol = 0xC;
    } else if (strcasecmp(argv[1], "1.34") == 0) {
        coreVol = 0xD;
    } else if (strcasecmp(argv[1], "1.38") == 0) {
        coreVol = 0xE;
    } else if (strcasecmp(argv[1], "1.41") == 0) {
        coreVol = 0xF;
    } else if (strcasecmp(argv[1], "1.45") == 0) {	/* 0% */
        coreVol = 0x0;
    } else if (strcasecmp(argv[1], "1.49") == 0) {
        coreVol = 0x1;
    } else if (strcasecmp(argv[1], "1.52") == 0) {
        coreVol = 0x2;
    } else if (strcasecmp(argv[1], "1.56") == 0) {
        coreVol = 0x3;
    } else if (strcasecmp(argv[1], "1.59") == 0) {
        coreVol = 0x4;
    } else if (strcasecmp(argv[1], "1.63") == 0) {
        coreVol = 0x5;
    } else if (strcasecmp(argv[1], "1.67") == 0) {
        coreVol = 0x6;
    } else if (strcasecmp(argv[1], "1.7") == 0) {	/* +17.5% */
        coreVol = 0x7;
    } else {
        diag_printf("Invalid argument. Type \"help setcorevol\" for valid settings\n");
        return;
    }

    pmic_reg(0x08, coreVol << 1, 1);

    return;
}
