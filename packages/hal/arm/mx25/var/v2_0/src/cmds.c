//==========================================================================
//
//		cmds.c
//
//		SoC [platform] specific RedBoot commands
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
#include <redboot.h>
#include <cyg/hal/hal_intr.h>
#include <cyg/hal/plf_mmap.h>
#include <cyg/hal/hal_soc.h>		// Hardware definitions
#include <cyg/hal/hal_cache.h>

#define IIM_FUSE_DEBUG

typedef unsigned long long	u64;
typedef unsigned int		u32;
typedef unsigned short		u16;
typedef unsigned char		u8;

u32 pll_clock(enum plls pll);
u32 get_main_clock(enum main_clocks clk);
u32 get_peri_clock(enum peri_clocks clk);

static void clock_setup(int argc, char *argv[]);

RedBoot_cmd("clock",
			"Setup/Display clock\nSyntax:",
			"[<ARM core clock in MHz> [:<ARM-AHB clock divider>]\n\
If a selection is zero or no divider is specified, the optimal divider values\n\
will be chosen. Examples:\n\
   [clock]         -> Show various clocks\n\
   [clock 399]     -> Core=399   AHB=133           IPG=66.5(AHB/2)\n\
   [clock 532:4]   -> Core=532   AHB=133(Core/4)   IPG=66.5(AHB/2)\n\
   [clock 399:4]   -> Core=399   AHB=99.75(Core/4) IPG=49.875(AHB/2)\n\
   [clock 199:3]   -> Core=199.5 AHB=66.5(Core/3)  IPG=33.25(AHB/2)\n\
   [clock 133:2]   -> Core=133   AHB=66.5(Core/2)  IPG=33.25(AHB/2)\n\
                      Core range: 532-133, AHB range: 133-66.5, IPG is always AHB/2\n",
			clock_setup
		   );

void clock_spi_enable(unsigned int spi_clk)
{
	diag_printf("%s: stubbed\n", __func__);
}

static void clock_setup(int argc,char *argv[])
{
	u32 i, data[2], core_clk, ahb_div, cctl, arm_src, arm_div;
		unsigned long temp;

	if (argc == 1)
		goto print_clock;

	for (i = 0;	i < 2; i++) {
		if (!parse_num(argv[1], &temp, &argv[1], ":")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}
		data[i] = temp;
	}

	core_clk = data[0];
	ahb_div = data[1] - 1;

	if (core_clk / (ahb_div + 1) > 133 ||
		core_clk / (ahb_div + 1) < 66) {
		diag_printf("Illegal AHB divider value specified\n");
		return;
	}

	switch (core_clk) {
	case 532:
		arm_src = 0;
		arm_div = 1 - 1;
		break;

	case 399:
		arm_src = 1;
		arm_div = 1 - 1;
		break;

	case 199:
	case 200:
		arm_src = 1;
		arm_div = 2 - 1;
		break;

	case 133:
		arm_src = 1;
		arm_div = 3 - 1;
		break;

	default:
		diag_printf("Illegal core clock value specified\n");
		return;
	}

	cyg_hal_plf_serial_stop();

	cctl = readl(CCM_BASE_ADDR + CLKCTL_CCTL);
	cctl &= ~0xF0004000;
	cctl |= arm_div << 30;
	cctl |= ahb_div << 28;
	cctl |= arm_src << 14;
	writel(cctl, CCM_BASE_ADDR + CLKCTL_CCTL);

	hal_delay_us(10000);
	cyg_hal_plf_serial_init();

	diag_printf("\n<<<New clock settings>>>\n");

	// Now printing clocks
print_clock:
	diag_printf("\nMPLL\t\tUPLL\n");
	diag_printf("=========================\n");
	diag_printf("%-16d%-16d\n\n", pll_clock(MCU_PLL), pll_clock(USB_PLL));
	diag_printf("CPU\t\tAHB\t\tIPG\n");
	diag_printf("========================================\n");
	diag_printf("%-16d%-16d%-16d\n\n",
				get_main_clock(CPU_CLK),
				get_main_clock(AHB_CLK),
				get_main_clock(IPG_CLK));

	diag_printf("UART\n");
	diag_printf("========\n");
	diag_printf("%-16d\n\n",
				get_peri_clock(PER_UART_CLK));

	diag_printf("SPI\n");
	diag_printf("========\n");
	diag_printf("%-16d\n\n",
				get_peri_clock(SPI1_CLK));
}

/*!
 * This function returns the PLL output value in Hz based on pll.
 */
u32 pll_clock(enum plls pll)
{
	int mfi, mfn, mfd, pdf;
	u32 pll_out;
	u32 reg = readl(pll);
	u64 ref_clk;

	pdf = (reg >> 26) & 0xF;
	mfd = (reg >> 16) & 0x3FF;
	mfi = (reg >> 10) & 0xF;
	if (mfi < 5) {
		mfi = 5;
	}
	mfn = reg & 0x3FF;
	if (mfn >= 512) {
		mfn = 1024 - mfn;
	}
	ref_clk = PLL_REF_CLK;

	pll_out = (2 * ref_clk * mfi + ((2 * ref_clk * mfn) / (mfd + 1))) /
		(pdf + 1);
	return pll_out;
}

/*!
 * This function returns the main clock value in Hz.
 */
u32 get_main_clock(enum main_clocks clk)
{
	u32 cctl = readl(CCM_BASE_ADDR + CLKCTL_CCTL);
	u32 div;
	u32 ret_val = 0;

	switch (clk) {
	case CPU_CLK:
		ret_val = pll_clock(MCU_PLL);
		if (cctl & CRM_CCTL_ARM_SRC) {
			ret_val = (ret_val * 3) / 4;
		}
		div = ((cctl >> CRM_CCTL_ARM_OFFSET) & 3) + 1;
		ret_val /= div;
		break;

	case AHB_CLK:
		div = ((cctl >> CRM_CCTL_AHB_OFFSET) & 3) + 1;
		ret_val = get_main_clock(CPU_CLK) / div;
		break;

	case IPG_CLK:
	case IPG_PER_CLK:
		ret_val = get_main_clock(AHB_CLK) / 2;
		break;

	default:
		diag_printf("Unknown clock: %d\n", clk);
	}

	return ret_val;
}

/*!
 * This function returns the peripheral clock value in Hz.
 */
u32 get_peri_clock(enum peri_clocks clk)
{
	u32 ret_val = 0;
	u32 pcdr, div;

	switch (clk) {
	case PER_UART_CLK:
		pcdr = readl(CCM_BASE_ADDR + CLKCTL_PCDR3);
		div = (pcdr >> 24) + 1;
		ret_val = get_main_clock(AHB_CLK) / div;
		break;

	case SPI1_CLK:
	case SPI2_CLK:
		ret_val = get_main_clock(IPG_CLK);
		break;

	case LCDC_CLK:
		writel(readl(CCM_BASE_ADDR + CLKCTL_MCR) | (1 << 7),
			CCM_BASE_ADDR + CLKCTL_MCR);
		pcdr = readl(CCM_BASE_ADDR + CLKCTL_PCDR1);
		pcdr &= ~(0xff << 24);
		writel(pcdr, CCM_BASE_ADDR + CLKCTL_PCDR1);
		div = (pcdr >> 24) + 1;
		if (readl(CCM_BASE_ADDR + CLKCTL_MCR) & (1 << 7)) {
			ret_val = pll_clock(USB_PLL) / div;
		} else {
			ret_val = get_main_clock(AHB_CLK) / div;
		}
		break;

	default:
		diag_printf("%s(): This clock: %d not supported yet\n",
					__FUNCTION__, clk);
	}
	return ret_val;
}


#define IIM_ERR_SHIFT		8
#define POLL_FUSE_PRGD		(IIM_STAT_PRGD | (IIM_ERR_PRGE << IIM_ERR_SHIFT))
#define POLL_FUSE_SNSD		(IIM_STAT_SNSD | (IIM_ERR_SNSE << IIM_ERR_SHIFT))

static void fuse_op_start(void)
{
	/* Do not generate interrupt */
	writel(0, IIM_BASE_ADDR + IIM_STATM_OFF);
	// clear the status bits and error bits
	writel(0x3, IIM_BASE_ADDR + IIM_STAT_OFF);
	writel(0xFE, IIM_BASE_ADDR + IIM_ERR_OFF);
}

/*
 * The action should be either:
 *			POLL_FUSE_PRGD
 * or:
 *			POLL_FUSE_SNSD
 */
static int poll_fuse_op_done(int action)
{

	u32 status, error;

	if (action != POLL_FUSE_PRGD && action != POLL_FUSE_SNSD) {
		diag_printf("%s(%d) invalid operation\n", __FUNCTION__, action);
		return -1;
	}

	/* Poll busy bit till it is NOT set */
	while ((readl(IIM_BASE_ADDR + IIM_STAT_OFF) & IIM_STAT_BUSY) != 0 ) {
	}

	/* Test for successful write */
	status = readl(IIM_BASE_ADDR + IIM_STAT_OFF);
	error = readl(IIM_BASE_ADDR + IIM_ERR_OFF);

	if ((status & action) != 0 && (error & (action >> IIM_ERR_SHIFT)) == 0) {
		if (error) {
			diag_printf("Even though the operation seems successful...\n");
			diag_printf("There are some error(s) at addr=0x%02lx: 0x%02x\n",
						(IIM_BASE_ADDR + IIM_ERR_OFF), error);
		}
		return 0;
	}
	diag_printf("%s(%d) failed\n", __FUNCTION__, action);
	diag_printf("status address=0x%02lx, value=0x%02x\n",
				(IIM_BASE_ADDR + IIM_STAT_OFF), status);
	diag_printf("There are some error(s) at addr=0x%02lx: 0x%02x\n",
				(IIM_BASE_ADDR + IIM_ERR_OFF), error);
	return -1;
}

static void sense_fuse(int bank, int row, int bit)
{
	int ret;
	int addr, addr_l, addr_h, reg_addr;

	fuse_op_start();

	addr = ((bank << 11) | (row << 3) | (bit & 0x7));
	/* Set IIM Program Upper Address */
	addr_h = (addr >> 8) & 0x000000FF;
	/* Set IIM Program Lower Address */
	addr_l = (addr & 0x000000FF);

#ifdef IIM_FUSE_DEBUG
	diag_printf("%s: addr_h=0x%02x, addr_l=0x%02x\n",
				__FUNCTION__, addr_h, addr_l);
#endif
	writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
	writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
	/* Start sensing */
	writel(0x8, IIM_BASE_ADDR + IIM_FCTL_OFF);
	if ((ret = poll_fuse_op_done(POLL_FUSE_SNSD)) != 0) {
		diag_printf("%s(bank: %d, row: %d, bit: %d failed\n",
					__FUNCTION__, bank, row, bit);
	}
	reg_addr = IIM_BASE_ADDR + IIM_SDAT_OFF;
	if (ret == 0)
		diag_printf("fuses at (bank:%d, row:%d) = 0x%02x\n", bank, row, readl(reg_addr));
}

void do_fuse_read(int argc, char *argv[])
{
	unsigned long bank, row;

	if (argc == 1) {
		diag_printf("Useage: fuse_read <bank> <row>\n");
		return;
	} else if (argc == 3) {
		if (!parse_num(argv[1], &bank, &argv[1], " ")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}
		if (!parse_num(argv[2], &row, &argv[2], " ")) {
			diag_printf("Error: Invalid parameter\n");
			return;
		}

		diag_printf("Read fuse at bank:%ld row:%ld\n", bank, row);
		sense_fuse(bank, row, 0);

	} else {
		diag_printf("Passing in wrong arguments: %d\n", argc);
		diag_printf("Useage: fuse_read <bank> <row>\n");
	}
}

/* Blow fuses based on the bank, row and bit positions (all 0-based)
*/
int fuse_blow(int bank, int row, int bit)
{
	int addr, addr_l, addr_h, ret = -1;

	fuse_op_start();

	/* Disable IIM Program Protect */
	writel(0xAA, IIM_BASE_ADDR + IIM_PREG_P_OFF);

	addr = ((bank << 11) | (row << 3) | (bit & 0x7));
	/* Set IIM Program Upper Address */
	addr_h = (addr >> 8) & 0x000000FF;
	/* Set IIM Program Lower Address */
	addr_l = (addr & 0x000000FF);

#ifdef IIM_FUSE_DEBUG
	diag_printf("blowing fuse %d %d bit %d addr_h=0x%02x, addr_l=0x%02x\n",
				bank, row, bit, addr_h, addr_l);
#endif

	writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
	writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
	/* Start Programming */
	writel(0x71, IIM_BASE_ADDR + IIM_FCTL_OFF);
	if (poll_fuse_op_done(POLL_FUSE_PRGD) == 0) {
		ret = 0;
	}

	/* Enable IIM Program Protect */
	writel(0x0, IIM_BASE_ADDR + IIM_PREG_P_OFF);
	return ret;
}

/*
 * This command is added for burning IIM fuses
 */
RedBoot_cmd("fuse_read",
			"read some fuses",
			"<bank> <row>",
			do_fuse_read
	);

RedBoot_cmd("fuse_blow",
			"blow some fuses",
			"<bank> <row> <value>",
			do_fuse_blow
	);

#define			INIT_STRING				"12345678"
static char ready_to_blow[] = INIT_STRING;

void quick_itoa(u32 num, char *a)
{
	int i, j, k;
	for (i = 0; i <= 7; i++) {
		j = (num >> (4 * i)) & 0xF;
		k = (j < 10) ? '0' : ('a' - 0xa);
		a[i] = j + k;
	}
}

void do_fuse_blow(int argc, char *argv[])
{
	unsigned long bank, row, value;
	int i;

	if (argc == 1) {
		diag_printf("It is too dangeous for you to use this command.\n");
		return;
	} else if (argc == 2) {
		if (strcasecmp(argv[1], "nandboot") == 0) {
			quick_itoa(readl(EPIT_BASE_ADDR + EPITCNR), ready_to_blow);
			diag_printf("%s\n", ready_to_blow);
		}
		return;
	} else if (argc == 3) {
		if (strcasecmp(argv[1], "nandboot") == 0 &&
			strcasecmp(argv[2], ready_to_blow) == 0) {
#if defined(CYGPKG_HAL_ARM_MXC91131) || defined(CYGPKG_HAL_ARM_MX21) || \
	defined(CYGPKG_HAL_ARM_MX27) || defined(CYGPKG_HAL_ARM_MX31) || \
	defined(CYGPKG_HAL_ARM_MX35) || defined(CYGPKG_HAL_ARM_MX25)
			diag_printf("No need to blow any fuses for NAND boot on this platform\n\n");
#else
#error "Are you sure you want this?"
			diag_printf("Ready to burn NAND boot fuses\n");
			if (fuse_blow(0, 16, 1) != 0 || fuse_blow(0, 16, 7) != 0) {
				diag_printf("NAND BOOT fuse blown failed miserably ...\n");
			} else {
				diag_printf("NAND BOOT fuse blown successfully ...\n");
			}
		} else {
			diag_printf("Not ready: %s, %s\n", argv[1], argv[2]);
#endif
		}
	} else if (argc == 4) {
		if (!parse_num(argv[1], &bank, &argv[1], " ")) {
				diag_printf("Error: Invalid fuse bank\n");
				return;
		}
		if (!parse_num(argv[2], &row, &argv[2], " ")) {
				diag_printf("Error: Invalid fuse row\n");
				return;
		}
		if (!parse_num(argv[3], &value, &argv[3], " ")) {
				diag_printf("Error: Invalid value\n");
				return;
		}
		if (!verify_action("Confirm to blow fuse at bank:%ld row:%ld value:0x%02lx (%ld)",
						   bank, row, value)) {
			diag_printf("fuse_blow canceled\n");
			return;
		}

		diag_printf("Blowing fuse at bank:%ld row:%ld value:%ld\n",
					bank, row, value);
		for (i = 0; i < 8; i++) {
			if (((value >> i) & 0x1) == 0) {
				continue;
			}
			if (fuse_blow(bank, row, i) != 0) {
				diag_printf("fuse_blow(bank: %ld, row: %ld, bit: %d failed\n",
							bank, row, i);
			} else {
				diag_printf("fuse_blow(bank: %ld, row: %ld, bit: %d successful\n",
							bank, row, i);
			}
		}
		sense_fuse(bank, row, 0);
	} else {
		diag_printf("Passing in wrong arguments: %d\n", argc);
	}
	/* Reset to default string */
	strcpy(ready_to_blow, INIT_STRING);
}

/* precondition: m>0 and n>0.  Let g=gcd(m,n). */
int gcd(int m, int n)
{
	int t;
	while (m > 0) {
		if (n > m) {t = m; m = n; n = t;} /* swap */
		m -= n;
	}
	return n;
}
