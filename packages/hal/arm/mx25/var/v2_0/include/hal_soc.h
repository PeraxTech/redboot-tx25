//==========================================================================
//
//		hal_soc.h
//
//		SoC chip definitions
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
// Copyright (C) 2002 Gary Thomas
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#ifndef __HAL_SOC_H__
#define __HAL_SOC_H__

#ifdef __ASSEMBLER__
#define UL(a)		 (a)

#define REG8_VAL(a)			 (a)
#define REG16_VAL(a)		 (a)
#define REG32_VAL(a)		 (a)

#define REG8_PTR(a)			 (a)
#define REG16_PTR(a)		 (a)
#define REG32_PTR(a)		 (a)

#else /* __ASSEMBLER__ */
#define UL(a)		 (a##UL)

extern char HAL_PLATFORM_EXTRA[55];
#define REG8_VAL(a)			 ((unsigned char)(a))
#define REG16_VAL(a)		 ((unsigned short)(a))
#define REG32_VAL(a)		 ((unsigned int)(a))

#define REG8_PTR(a)			 ((volatile unsigned char *)(a))
#define REG16_PTR(a)		 ((volatile unsigned short *)(a))
#define REG32_PTR(a)		 ((volatile unsigned int *)(a))
#define readb(a)			 (*(volatile unsigned char *)(a))
#define readw(a)			 (*(volatile unsigned short *)(a))
#define readl(a)			 (*(volatile unsigned int *)(a))
#define writeb(v,a)			 (*(volatile unsigned char *)(a) = (v))
#define writew(v,a)			 (*(volatile unsigned short *)(a) = (v))
#define writel(v,a)			 (*(volatile unsigned int *)(a) = (v))

#endif /* __ASSEMBLER__ */

/*
 * Default Memory Layout Definitions
 */

/*
 * AIPS 1
 */
#define AIPS1_BASE_ADDR			UL(0x43F00000)
#define AIPS1_CTRL_BASE_ADDR	AIPS1_BASE_ADDR
#define MAX_BASE_ADDR			UL(0x43F04000)
#define CLKCTL_BASE_ADDR		UL(0x43F08000)
#define ETB_SLOT4_BASE_ADDR		UL(0x43F0C000)
#define ETB_SLOT5_BASE_ADDR		UL(0x43F10000)
#define ECT_CTIO_BASE_ADDR		UL(0x43F18000)
#define I2C_BASE_ADDR			UL(0x43F80000)
#define I2C3_BASE_ADDR			UL(0x43F84000)
#define CAN1_BASE_ADDR			UL(0x43F88000)
#define CAN2_BASE_ADDR			UL(0x43F8C000)
#define UART1_BASE_ADDR			UL(0x43F90000)
#define UART2_BASE_ADDR			UL(0x43F94000)
#define I2C2_BASE_ADDR			UL(0x43F98000)
#define OWIRE_BASE_ADDR			UL(0x43F9C000)
#define CSPI1_BASE_ADDR			UL(0x43FA4000)
#define KPP_BASE_ADDR			UL(0x43FA8000)
#define IOMUXC_BASE_ADDR		UL(0x43FAC000)
#define AUDMUX_BASE_ADDR		UL(0x43FB0000)
#define ECT_IP1_BASE_ADDR		UL(0x43FB8000)
#define ECT_IP2_BASE_ADDR		UL(0x43FBC000)

/*
 * SPBA
 */
#define SPBA_BASE_ADDR			UL(0x50000000)
#define CSPI3_BASE_ADDR			UL(0x50040000)
#define UART4_BASE_ADDR			UL(0x50008000)
#define UART3_BASE_ADDR			UL(0x5000C000)
#define CSPI2_BASE_ADDR			UL(0x50010000)
#define SSI2_BASE_ADDR			UL(0x50014000)
#define ESAI_BASE_ADDR			UL(0x50018000)
#define ATA_DMA_BASE_ADDR		UL(0x50020000)
#define SIM1_BASE_ADDR			UL(0x50024000)
#define SIM2_BASE_ADDR			UL(0x50028000)
#define UART5_BASE_ADDR			UL(0x5002C000)
#define TSC_BASE_ADDR			UL(0x50030000)
#define SSI1_BASE_ADDR			UL(0x50034000)
#define FEC_BASE_ADDR			UL(0x50038000)
#define SOC_FEC_BASE			FEC_BASE_ADDR
#define SPBA_CTRL_BASE_ADDR		UL(0x5003C000)

/*
 * AIPS 2
 */
#define AIPS2_BASE_ADDR			UL(0x53F00000)
#define AIPS2_CTRL_BASE_ADDR	AIPS2_BASE_ADDR
#define CCM_BASE_ADDR			UL(0x53F80000)
#define GPT4_BASE_ADDR			UL(0x53F84000)
#define GPT3_BASE_ADDR			UL(0x53F88000)
#define GPT2_BASE_ADDR			UL(0x53F8C000)
#define GPT1_BASE_ADDR			UL(0x53F90000)
#define EPIT1_BASE_ADDR			UL(0x53F94000)
#define EPIT2_BASE_ADDR			UL(0x53F98000)
#define GPIO4_BASE_ADDR			UL(0x53F9C000)
#define PWM2_BASE_ADDR			UL(0x53FA0000)
#define GPIO3_BASE_ADDR			UL(0x53FA4000)
#define PWM3_BASE_ADDR			UL(0x53FA8000)
#define SCC_BASE_ADDR			UL(0x53FAC000)
#define SCM_BASE_ADDR			UL(0x53FAE000)
#define SMN_BASE_ADDR			UL(0x53FAF000)
#define RNGD_BASE_ADDR			UL(0x53FB0000)
#define MMC_SDHC1_BASE_ADDR		UL(0x53FB4000)
#define MMC_SDHC2_BASE_ADDR		UL(0x53FB8000)
#define ESDHC1_REG_BASE			MMC_SDHC1_BASE_ADDR
#define LCDC_BASE_ADDR			UL(0x53FBC000)
#define SLCDC_BASE_ADDR			UL(0x53FC0000)
#define PWM4_BASE_ADDR			UL(0x53FC8000)
#define GPIO1_BASE_ADDR			UL(0x53FCC000)
#define GPIO2_BASE_ADDR			UL(0x53FD0000)
#define SDMA_BASE_ADDR			UL(0x53FD4000)
#define WDOG_BASE_ADDR			UL(0x53FDC000)
#define PWM1_BASE_ADDR			UL(0x53FE0000)
#define RTIC_BASE_ADDR			UL(0x53FEC000)
#define IIM_BASE_ADDR			UL(0x53FF0000)
#define USB_BASE_ADDR			UL(0x53FF4000)
#define CSI_BASE_ADDR			UL(0x53FF8000)
#define DRYICE_BASE_ADDR		UL(0x53FFC000)

/*
 * ROMPATCH and AVIC
 */
#define ROMPATCH_BASE_ADDR		UL(0x60000000)
#define ASIC_BASE_ADDR			UL(0x68000000)

#define RAM_BASE_ADDR			UL(0x78000000)

/*
 * NAND, SDRAM, WEIM, M3IF, EMI controllers
 */
#define EXT_MEM_CTRL_BASE		UL(0xB8000000)
#define ESDCTL_BASE_ADDR		UL(0xB8001000)
#define WEIM_BASE_ADDR			UL(0xB8002000)
#define WEIM_CTRL_CS0			WEIM_BASE_ADDR
#define WEIM_CTRL_CS1			(WEIM_BASE_ADDR + 0x10)
#define WEIM_CTRL_CS2			(WEIM_BASE_ADDR + 0x20)
#define WEIM_CTRL_CS3			(WEIM_BASE_ADDR + 0x30)
#define WEIM_CTRL_CS4			(WEIM_BASE_ADDR + 0x40)
#define WEIM_CTRL_CS5			(WEIM_BASE_ADDR + 0x50)
#define M3IF_BASE				UL(0xB8003000)
#define EMI_BASE				UL(0xB8004000)

#define NFC_BASE				UL(0xBB000000)
/*
 * Memory regions and CS
 */
#define CSD0_BASE_ADDR			UL(0x80000000)
#define CSD1_BASE_ADDR			UL(0x90000000)
#define CS0_BASE_ADDR			UL(0xA0000000)
#define CS1_BASE_ADDR			UL(0xA8000000)
#define CS2_BASE_ADDR			UL(0xB0000000)
#define CS3_BASE_ADDR			UL(0xB2000000)
#define CS4_BASE_ADDR			UL(0xB4000000)
#define CS5_BASE_ADDR			UL(0xB6000000)

/*
 * IRQ Controller Register Definitions.
 */
#define ASIC_NIMASK				REG32_PTR(ASIC_BASE_ADDR + 0x04)
#define ASIC_INTTYPEH			REG32_PTR(ASIC_BASE_ADDR + 0x18)
#define ASIC_INTTYPEL			REG32_PTR(ASIC_BASE_ADDR + 0x1C)

/* CCM */
#define CLKCTL_MPCTL			0x00
#define CLKCTL_UPCTL			0x04
#define CLKCTL_CCTL				0x08
#define CLKCTL_CGR0				0x0C
#define CLKCTL_CGR1				0x10
#define CLKCTL_CGR2				0x14
#define CLKCTL_PCDR0			0x18
#define CLKCTL_PCDR1			0x1C
#define CLKCTL_PCDR2			0x20
#define CLKCTL_PCDR3			0x24
#define CLKCTL_RCSR				0x28
#define CLKCTL_CRDR				0x2C
#define CLKCTL_DCVR0			0x30
#define CLKCTL_DCVR1			0x34
#define CLKCTL_DCVR2			0x38
#define CLKCTL_DCVR3			0x3C
#define CLKCTL_LTR0				0x40
#define CLKCTL_LTR1				0x44
#define CLKCTL_LTR2				0x48
#define CLKCTL_LTR3				0x4C
#define CLKCTL_LTBR0			0x50
#define CLKCTL_LTBR1			0x54
#define CLKCTL_PCMR0			0x58
#define CLKCTL_PCMR1			0x5C
#define CLKCTL_PCMR2			0x60
#define CLKCTL_MCR				0x64
#define CLKCTL_LPIMR0			0x68
#define CLKCTL_LPIMR1			0x6C

#define CRM_CCTL_ARM_SRC		(1 << 14)
#define CRM_CCTL_ARM_OFFSET		30
#define CRM_CCTL_AHB_OFFSET		28

#define SOC_MAC_ADDR_BASE		(IIM_BASE_ADDR + 0x868)

#define FREQ_24MHZ				24000000
#define PLL_REF_CLK				FREQ_24MHZ

/*
 * FIXME-DALE - Constants verified up to this point.
 *				Offsets and derived constants below should be confirmed.
 */

#define CLKMODE_AUTO			0
#define CLKMODE_CONSUMER		1

/* WEIM - CS0 */
#define CSCRU					0x00
#define CSCRL					0x04
#define CSCRA					0x08

#define CHIP_REV_1_0			0x0		 /* PASS 1.0 */
#define CHIP_REV_1_1			0x1		 /* PASS 1.1 */
#define CHIP_REV_2_0			0x2		 /* PASS 2.0 */
#define CHIP_LATEST				CHIP_REV_1_1

#define IIM_STAT_OFF			0x00
#define IIM_STAT_BUSY			(1 << 7)
#define IIM_STAT_PRGD			(1 << 1)
#define IIM_STAT_SNSD			(1 << 0)
#define IIM_STATM_OFF			0x04
#define IIM_ERR_OFF				0x08
#define IIM_ERR_PRGE			(1 << 7)
#define IIM_ERR_WPE				(1 << 6)
#define IIM_ERR_OPE				(1 << 5)
#define IIM_ERR_RPE				(1 << 4)
#define IIM_ERR_WLRE			(1 << 3)
#define IIM_ERR_SNSE			(1 << 2)
#define IIM_ERR_PARITYE			(1 << 1)
#define IIM_EMASK_OFF			0x0C
#define IIM_FCTL_OFF			0x10
#define IIM_UA_OFF				0x14
#define IIM_LA_OFF				0x18
#define IIM_SDAT_OFF			0x1C
#define IIM_PREV_OFF			0x20
#define IIM_SREV_OFF			0x24
#define IIM_PREG_P_OFF			0x28
#define IIM_SCS0_OFF			0x2C
#define IIM_SCS1_OFF			0x30
#define IIM_SCS2_OFF			0x34
#define IIM_SCS3_OFF			0x38

#define EPIT_BASE_ADDR			EPIT1_BASE_ADDR
#define EPITCR					0x00
#define EPITSR					0x04
#define EPITLR					0x08
#define EPITCMPR				0x0C
#define EPITCNR					0x10

#define GPT_BASE_ADDR			GPT1_BASE_ADDR
#define GPTCR					0x00
#define GPTPR					0x04
#define GPTSR					0x08
#define GPTIR					0x0C
#define GPTOCR1					0x10
#define GPTOCR2					0x14
#define GPTOCR3					0x18
#define GPTICR1					0x1C
#define GPTICR2					0x20
#define GPTCNT					0x24

/* ESDCTL */
#define ESDCTL_ESDCTL0			0x00
#define ESDCTL_ESDCFG0			0x04
#define ESDCTL_ESDCTL1			0x08
#define ESDCTL_ESDCFG1			0x0C
#define ESDCTL_ESDMISC			0x10

/* DRYICE */
#define DRYICE_DTCMR			0x00
#define DRYICE_DTCLR			0x04
#define DRYICE_DCAMR			0x08
#define DRYICE_DCALR			0x0C
#define DRYICE_DCR				0x10
#define DRYICE_DSR				0x14
#define DRYICE_DIER				0x18
#define DRYICE_DMCR				0x1C
#define DRYICE_DKSR				0x20
#define DRYICE_DKCR				0x24
#define DRYICE_DTCR				0x28
#define DRYICE_DACR				0x2C
#define DRYICE_DGPR				0x3C
#define DRYICE_DPKR0			0x40
#define DRYICE_DPKR1			0x44
#define DRYICE_DPKR2			0x48
#define DRYICE_DPKR3			0x4C
#define DRYICE_DPKR4			0x50
#define DRYICE_DPKR5			0x54
#define DRYICE_DPKR6			0x58
#define DRYICE_DPKR7			0x5C
#define DRYICE_DRKR0			0x60
#define DRYICE_DRKR1			0x64
#define DRYICE_DRKR2			0x68
#define DRYICE_DRKR3			0x6C
#define DRYICE_DRKR4			0x70
#define DRYICE_DRKR5			0x74
#define DRYICE_DRKR6			0x78
#define DRYICE_DRKR7			0x7C

/* GPIO */
#define GPIO_DR					0x00
#define GPIO_GDIR				0x04
#define GPIO_PSR0				0x08
#define GPIO_ICR1				0x0C
#define GPIO_ICR2				0x10
#define GPIO_IMR				0x14
#define GPIO_ISR				0x18
#define GPIO_EDGE_SEL			0x1C


#if (PLL_REF_CLK != 24000000)
#error Wrong PLL reference clock! The following macros will not work.
#endif

/* Assuming 24MHz input clock */
/*							  PD			 MFD			  MFI		   MFN */
#define MPCTL_PARAM_399		(((1-1) << 26) + ((16-1) << 16) + (8 << 10) + (5 << 0))
#define MPCTL_PARAM_532		((1 << 31) + ((1-1) << 26) + ((12-1) << 16) + (11 << 10) + (1 << 0))
#define MPCTL_PARAM_665		(((1-1) << 26) + ((48-1) << 16) + (13 << 10) + (41 << 0))

/* UPCTL					  PD			 MFD			  MFI		   MFN */
#define UPCTL_PARAM_300		(((1-1) << 26) + ((4-1) << 16) + (6	<< 10) + (1 << 0))

#define NFC_V1_1

#define NAND_REG_BASE					(NFC_BASE + 0x1E00)
#define NFC_BUFSIZE_REG_OFF				0x00
#define RAM_BUFFER_ADDRESS_REG_OFF		0x04
#define NAND_FLASH_ADD_REG_OFF			0x06
#define NAND_FLASH_CMD_REG_OFF			0x08
#define NFC_CONFIGURATION_REG_OFF		0x0A
#define ECC_STATUS_RESULT_REG_OFF		0x0C
#define ECC_RSLT_MAIN_AREA_REG_OFF		0x0E
#define ECC_RSLT_SPARE_AREA_REG_OFF		0x10
#define NF_WR_PROT_REG_OFF				0x12
#define NAND_FLASH_WR_PR_ST_REG_OFF		0x18
#define NAND_FLASH_CONFIG1_REG_OFF		0x1A
#define NAND_FLASH_CONFIG2_REG_OFF		0x1C
#define UNLOCK_START_BLK_ADD_REG_OFF	0x20
#define UNLOCK_END_BLK_ADD_REG_OFF		0x22
#define RAM_BUFFER_ADDRESS_RBA_3		0x3
#define NFC_BUFSIZE_1KB					0x0
#define NFC_BUFSIZE_2KB					0x1
#define NFC_CONFIGURATION_UNLOCKED		0x2
#define ECC_STATUS_RESULT_NO_ERR		0x0
#define ECC_STATUS_RESULT_1BIT_ERR		0x1
#define ECC_STATUS_RESULT_2BIT_ERR		0x2
#define NF_WR_PROT_UNLOCK				0x4
#define NAND_FLASH_CONFIG1_FORCE_CE		(1 << 7)
#define NAND_FLASH_CONFIG1_RST			(1 << 6)
#define NAND_FLASH_CONFIG1_BIG			(1 << 5)
#define NAND_FLASH_CONFIG1_INT_MSK		(1 << 4)
#define NAND_FLASH_CONFIG1_ECC_EN		(1 << 3)
#define NAND_FLASH_CONFIG1_SP_EN		(1 << 2)
#define NAND_FLASH_CONFIG2_INT_DONE		(1 << 15)
#define NAND_FLASH_CONFIG2_FDO_PAGE		(0 << 3)
#define NAND_FLASH_CONFIG2_FDO_ID		(2 << 3)
#define NAND_FLASH_CONFIG2_FDO_STATUS	(4 << 3)
#define NAND_FLASH_CONFIG2_FDI_EN		(1 << 2)
#define NAND_FLASH_CONFIG2_FADD_EN		(1 << 1)
#define NAND_FLASH_CONFIG2_FCMD_EN		(1 << 0)
#define FDO_PAGE_SPARE_VAL				0x8
#define NAND_BUF_NUM					8

#define MXC_NAND_BASE_DUMMY				0x00000000
#define MXC_MMC_BASE_DUMMY				0x00000000
#define NOR_FLASH_BOOT					0
#define NAND_FLASH_BOOT					0x10000000
#define SDRAM_NON_FLASH_BOOT			0x20000000
#define MMC_FLASH_BOOT					0x40000000
#define MXCBOOT_FLAG_REG				(CSI_BASE_ADDR + 0x28)	// use CSIDMASA-FB1
#define MXCFIS_NOTHING					0x00000000
#define MXCFIS_NAND						0x10000000
#define MXCFIS_NOR						0x20000000
#define MXCFIS_MMC						0x40000000
#define MXCFIS_FLAG_REG					(CSI_BASE_ADDR + 0x2C)	// use CSIDMASA-FB2

#define IS_BOOTING_FROM_NAND()			(readl(MXCBOOT_FLAG_REG) == NAND_FLASH_BOOT)
#define IS_BOOTING_FROM_NOR()			(readl(MXCBOOT_FLAG_REG) == NOR_FLASH_BOOT)
#define IS_BOOTING_FROM_SDRAM()			(readl(MXCBOOT_FLAG_REG) == SDRAM_NON_FLASH_BOOT)
#define IS_BOOTING_FROM_MMC()			(readl(MXCBOOT_FLAG_REG) == MMC_FLASH_BOOT)

#ifndef MXCFLASH_SELECT_NAND
#define IS_FIS_FROM_NAND()				0
#else
#define IS_FIS_FROM_NAND()				(readl(MXCFIS_FLAG_REG) == MXCFIS_NAND)
#endif

#ifndef MXCFLASH_SELECT_MMC
#define IS_FIS_FROM_MMC()				0
#else
#define IS_FIS_FROM_MMC()				(readl(MXCFIS_FLAG_REG) == MXCFIS_MMC)
#endif

#ifndef MXCFLASH_SELECT_NOR
#define IS_FIS_FROM_NOR()				0
#else
#define IS_FIS_FROM_NOR()				(readl(MXCFIS_FLAG_REG) == MXCFIS_NOR)
#endif

#define MXC_ASSERT_NOR_BOOT()			writel(MXCFIS_NOR, MXCFIS_FLAG_REG)
#define MXC_ASSERT_NAND_BOOT()			writel(MXCFIS_NAND, MXCFIS_FLAG_REG)
#define MXC_ASSERT_MMC_BOOT()			writel(MXCFIS_MMC, MXCFIS_FLAG_REG)

/*
 * This macro is used to get certain bit field from a number
 */
#define MXC_GET_FIELD(val, len, sh)			 ((val >> sh) & ((1 << len) - 1))

/*
 * This macro is used to set certain bit field inside a number
 */
#define MXC_SET_FIELD(val, len, sh, nval)	 ((val & ~(((1 << len) - 1) << sh)) | (nval << sh))

#define UART_WIDTH_32		  /* internal UART is 32bit access only */

#if !defined(__ASSEMBLER__)
void cyg_hal_plf_serial_init(void);
void cyg_hal_plf_serial_stop(void);
void hal_delay_us(unsigned int usecs);
#define HAL_DELAY_US(n)		hal_delay_us(n)

enum plls {
		MCU_PLL = CCM_BASE_ADDR + CLKCTL_MPCTL,
		USB_PLL = CCM_BASE_ADDR + CLKCTL_UPCTL,
};

enum main_clocks {
		CPU_CLK,
		AHB_CLK,
		IPG_CLK,
		IPG_PER_CLK, // not there on MX25 but simulated for compatibility
};

enum peri_clocks {
		PER_UART_CLK,
		LCDC_CLK,
		SPI1_CLK = CSPI1_BASE_ADDR,
		SPI2_CLK = CSPI2_BASE_ADDR,
};

unsigned int pll_clock(enum plls pll);

unsigned int get_main_clock(enum main_clocks clk);

unsigned int get_peri_clock(enum peri_clocks clk);

typedef unsigned int nfc_setup_func_t(unsigned int, unsigned int, unsigned int, unsigned int);

#endif //#if !defined(__ASSEMBLER__)

#define HAL_MMU_OFF()												\
CYG_MACRO_START														\
	asm volatile (													\
		"1: "														\
		"mrc p15, 0, r15, c7, c14, 3;"	 /*test clean and inval*/	\
		"bne 1b;"													\
		"mov r0, #0;"												\
		"mcr p15,0,r0,c7,c10,4;"   /*drain write buffer*/			\
		"mcr p15,0,r0,c7,c5,0;" /* invalidate I cache */			\
		"mrc p15,0,r0,c1,c0,0;" /* read c1 */						\
		"bic r0,r0,#0x7;" /* disable DCache and MMU */				\
		"bic r0,r0,#0x1000;" /* disable ICache */					\
		"mcr p15,0,r0,c1,c0,0;" /*	*/								\
		"nop;" /* flush i+d-TLBs */									\
		"nop;" /* flush i+d-TLBs */									\
		"nop;" /* flush i+d-TLBs */									\
		:															\
		:															\
		: "r0","memory" /* clobber list */);						\
CYG_MACRO_END

#endif /* __HAL_SOC_H__ */
