//==========================================================================
//
//      hal_soc.h
//
//      SoC chip definitions
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
// Copyright (C) 2002 Gary Thomas
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//========================================================================*/

#ifndef __HAL_SOC_H__
#define __HAL_SOC_H__

#ifdef __ASSEMBLER__

#define REG8_VAL(a)          (a)
#define REG16_VAL(a)         (a)
#define REG32_VAL(a)         (a)

#define REG8_PTR(a)          (a)
#define REG16_PTR(a)         (a)
#define REG32_PTR(a)         (a)

#else /* __ASSEMBLER__ */

extern char HAL_PLATFORM_EXTRA[];
#define REG8_VAL(a)          ((unsigned char)(a))
#define REG16_VAL(a)         ((unsigned short)(a))
#define REG32_VAL(a)         ((unsigned int)(a))

#define REG8_PTR(a)          ((volatile unsigned char *)(a))
#define REG16_PTR(a)         ((volatile unsigned short *)(a))
#define REG32_PTR(a)         ((volatile unsigned int *)(a))
#define readb(a)             (*(volatile unsigned char *)(a))
#define readw(a)             (*(volatile unsigned short *)(a))
#define readl(a)             (*(volatile unsigned int *)(a))
#define writeb(v,a)          (*(volatile unsigned char *)(a) = (v))
#define writew(v,a)          (*(volatile unsigned short *)(a) = (v))
#define writel(v,a)          (*(volatile unsigned int *)(a) = (v))

#endif /* __ASSEMBLER__ */

/*
 * Default Memory Layout Definitions
 */

#define L2CC_BASE_ADDR          0x30000000

/*
 * AIPS 1
 */
#define AIPS1_BASE_ADDR         0x43F00000
#define AIPS1_CTRL_BASE_ADDR    AIPS1_BASE_ADDR
#define MAX_BASE_ADDR           0x43F04000
#define EVTMON_BASE_ADDR        0x43F08000
#define CLKCTL_BASE_ADDR        0x43F0C000
#define ETB_SLOT4_BASE_ADDR     0x43F10000
#define ETB_SLOT5_BASE_ADDR     0x43F14000
#define ECT_CTIO_BASE_ADDR      0x43F18000
#define I2C_BASE_ADDR           0x43F80000
#define MU_BASE_ADDR            0x43F88000
#define UART1_BASE_ADDR         0x43F90000
#define UART2_BASE_ADDR         0x43F94000
#define DSM_BASE_ADDR           0x43F98000
#define OWIRE_BASE_ADDR         0x43F9C000
#define SSI1_BASE_ADDR          0x43FA0000
#define CSPI1_BASE_ADDR         0x43FA4000
#define KPP_BASE_ADDR           0x43FA8000
#define IOMUX_AP_BASE_ADDR      0x43FAC000
#define GPIO3_BASE_ADDR         0x43FB0000
#define CTI_AP_BASE_ADDR        0x43FB8000

/*
 * SPBA global module enabled #0
 */
#define SPBA_MOD0_BASE_ADDR     0x50000000
#define MMC_SDHC1_BASE_ADDR     0x50004000
#define MMC_SDHC2_BASE_ADDR     0x50008000
#define UART3_BASE_ADDR         0x5000C000
#define CSPI2_BASE_ADDR         0x50010000
#define SSI2_BASE_ADDR          0x50014000
#define SIM_BASE_ADDR           0x50018000
#define IIM_BASE_ADDR           0x5001C000
#define CTI_SDMA_BASE_ADDR      0x50020000
#define USBOTG_CTRL_BASE_ADDR   0x50024000
#define SPBA_CTRL_BASE_ADDR     0x5003C000
#define IOMUX_COM_BASE_ADDR     0x50040000
#define CRM_COM_BASE_ADDR       0x50044000
#define MRCG_BASE_ADDR          0x50048000
#define UDPLL_BASE_ADDR         0x5004C000
#define ADPLL_BASE_ADDR         0x50050000
#define BDPLL_BASE_ADDR         0x50054000
#define CRM_AP_BASE_ADDR        0x50058000

/*
 * AIPS 2
 */
#define AIPS2_BASE_ADDR         0x53F00000
#define AIPS2_CTRL_BASE_ADDR    AIPS2_BASE_ADDR
#define FIRI_BASE_ADDR          0x53F8C000
#define GPT_BASE_ADDR           0x53F90000
#define EPIT1_BASE_ADDR         0x53F94000
#define EPIT2_BASE_ADDR         0x53F98000
#define SCC_BASE_ADDR           0x53FAC000
#define RNGA_BASE_ADDR          0x53FB0000
#define IPU_CTRL_BASE_ADDR      0x53FC0000
#define AUDMUX_BASE_ADDR        0x53FC4000
#define EDIO_BASE_ADDR          0x53FC8000
#define GPIO1_BASE_ADDR         0x53FCC000
#define GPIO2_BASE_ADDR         0x53FD0000
#define SDMA_BASE_ADDR          0x53FD4000
#define RTC_BASE_ADDR           0x53FD8000
#define WDOG_BASE_ADDR          0x53FDC000
#define PWM_BASE_ADDR           0x53FE0000
#define HAC_BASE_ADDR           0x53FEC000

/*
 * ROMPATCH and AVIC
 */
#define ROMPATCH_BASE_ADDR      0x60000000
#define AVIC_BASE_ADDR          0x68000000

/*
 * NAND, SDRAM, WEIM, M3IF, EMI controllers
 */
#define EXT_MEM_CTRL_BASE       0xB8000000
#define NFC_BASE                EXT_MEM_CTRL_BASE
#define ESDCTL_BASE             0xB8001000
#define WEIM_BASE_ADDR          0xB8002000
#define WEIM_CTRL_CS0           WEIM_BASE_ADDR
#define WEIM_CTRL_CS1           (WEIM_BASE_ADDR + 0x10)
#define WEIM_CTRL_CS2           (WEIM_BASE_ADDR + 0x20)
#define WEIM_CTRL_CS3           (WEIM_BASE_ADDR + 0x30)
#define WEIM_CTRL_CS4           (WEIM_BASE_ADDR + 0x40)
#define M3IF_BASE               0xB8003000
#define PCMCIA_CTL_BASE         0xB8004000

/*
 * Memory regions and CS
 */
#define IPU_MEM_BASE_ADDR       0x70000000
#define CSD0_BASE_ADDR          0x80000000
#define CSD1_BASE_ADDR          0x90000000
#define CS0_BASE_ADDR           0xA0000000
#define CS1_BASE_ADDR           0xA8000000
#define CS2_BASE_ADDR           0xB0000000
#define CS3_BASE_ADDR           0xB2000000
#define CS4_BASE_ADDR           0xB4000000
#define CS4_BASE_PSRAM          0xB5000000
#define CS5_BASE_ADDR           0xB6000000

#define INTERNAL_ROM_VA         0xF0000000

/*
 * IRQ Controller Register Definitions.
 */
#define AVIC_NIMASK                     REG32_PTR(AVIC_BASE_ADDR + (0x04))
#define AVIC_INTTYPEH                   REG32_PTR(AVIC_BASE_ADDR + (0x18))
#define AVIC_INTTYPEL                   REG32_PTR(AVIC_BASE_ADDR + (0x1C))

/* L210 */
#define L2CC_BASE_ADDR                  0x30000000
#define L2_CACHE_LINE_SIZE              32
#define L2_CACHE_CTL_REG                0x100
#define L2_CACHE_AUX_CTL_REG            0x104
#define L2_CACHE_SYNC_REG               0x730
#define L2_CACHE_INV_LINE_REG           0x770
#define L2_CACHE_INV_WAY_REG            0x77C
#define L2_CACHE_CLEAN_LINE_REG         0x7B0
#define L2_CACHE_CLEAN_INV_LINE_REG     0x7F0

/* CRM_COM */
#define CRM_COM_CBMR        0x00
#define CRM_COM_CRSRBP      0x04
#define CRM_COM_CCRCR       0x08
#define CRM_COM_CSCR        0x0C
#define CRM_COM_CCCR        0x10
#define CRM_COM_CRSRAP      0x14

/* CRM_AP */
#define CRM_AP_ASCSR        0x00
#define CRM_AP_ACDR         0x04
#define CRM_AP_ACDER1       0x08
#define CRM_AP_ACDER2       0x0C
#define CRM_AP_ACGCR        0x10
#define CRM_AP_ACCGCR       0x14
#define CRM_AP_AMLPMRA      0x18
#define CRM_AP_AMLPMRB      0x1C
#define CRM_AP_AMLPMRC      0x20
#define CRM_AP_AMLPMRD      0x24
#define CRM_AP_AMLPMRE1     0x28
#define CRM_AP_AMLPMRE2     0x2C
#define CRM_AP_AMLPMRF      0x30
#define CRM_AP_AMLPMRG      0x34
#define CRM_AP_APGCR        0x38
#define CRM_AP_ACSR         0x3C
#define CRM_AP_ADCR         0x40
#define CRM_AP_ACR          0x44
#define CRM_AP_AMCR         0x48
#define CRM_AP_APCR         0x4C
#define CRM_AP_AMORA        0x50
#define CRM_AP_AMORB        0x54
#define CRM_AP_AGPR         0x58
#define CRM_AP_APRA         0x5C
#define CRM_AP_APRB         0x60
#define CRM_AP_APOR         0x64

/* WEIM - CS0 */
#define CSCRU               0x00
#define CSCRL               0x04
#define CSCRA               0x08
/* ESDRAM parameters */
#define SDRAM_CSD0          0x80000000
#define SDRAM_CSD1          0x90000000

/* ESDCTL */
#define ESDCTL_ESDCTL0      0x00
#define ESDCTL_ESDCFG0      0x04
#define ESDCTL_ESDCTL1      0x08
#define ESDCTL_ESDCFG1      0x0C
#define ESDCTL_ESDMISC      0x10

/* DPLL */
#define PLL_DP_CTL          0x00
#define PLL_DP_CONFIG       0x04
#define PLL_DP_OP           0x08
#define PLL_DP_MFD          0x0C
#define PLL_DP_MFN          0x10
#define PLL_DP_MFNMINUS     0x14
#define PLL_DP_MFNPLUS      0x18
#define PLL_DP_HFS_OP       0x1C
#define PLL_DP_HFS_MFD      0x20
#define PLL_DP_HFS_MFN      0x24
#define PLL_DP_TOGC         0x28
#define PLL_DP_DESTAT       0x2C


/* IIM */
#define CHIP_REV_1_0        0x10      /* PASS 1.0 */
#define CHIP_REV_2_0        0x20      /* PASS 2.0 */
#define CHIP_REV_2_2        0x30      /* PASS 2.2 */
#define CHIP_LATEST         CHIP_REV_2_2

#define IIM_STAT_OFF        0x00
#define IIM_STAT_BUSY       (1 << 7)
#define IIM_STAT_PRGD       (1 << 1)
#define IIM_STAT_SNSD       (1 << 0)
#define IIM_STATM_OFF       0x04
#define IIM_ERR_OFF         0x08
#define IIM_ERR_PRGE        (1 << 7)
#define IIM_ERR_WPE         (1 << 6)
#define IIM_ERR_OPE         (1 << 5)
#define IIM_ERR_RPE         (1 << 4)
#define IIM_ERR_WLRE        (1 << 3)
#define IIM_ERR_SNSE        (1 << 2)
#define IIM_ERR_PARITYE     (1 << 1)
#define IIM_EMASK_OFF       0x0C
#define IIM_FCTL_OFF        0x10
#define IIM_UA_OFF          0x14
#define IIM_LA_OFF          0x18
#define IIM_SDAT_OFF        0x1C
#define IIM_PREV_OFF        0x20
#define IIM_SREV_OFF        0x24
#define IIM_PREG_P_OFF      0x28
#define IIM_SCS0_OFF        0x2C
#define IIM_SCS1_P_OFF      0x30
#define IIM_SCS2_OFF        0x34
#define IIM_SCS3_P_OFF      0x38

#define FREQ_CKIH_16_8M     16800000
#define FREQ_DIGRF_26M      26000000
#define FREQ_32768HZ        (32768 * 512)
#define FREQ_32000HZ        (32000 * 512)
#define CKIH_CLK_FREQ       FREQ_CKIH_16_8M

#define EPIT_BASE_ADDR      EPIT1_BASE_ADDR
#define EPITCR              0x00
#define EPITSR              0x04
#define EPITLR              0x08
#define EPITCMPR            0x0C
#define EPITCNR             0x10

#define DelayTimerPresVal   3


#define NAND_REG_BASE                   (NFC_BASE + 0xE00)
#define NFC_BUFSIZE_REG_OFF             (0 + 0x00)
#define RAM_BUFFER_ADDRESS_REG_OFF      (0 + 0x04)
#define NAND_FLASH_ADD_REG_OFF          (0 + 0x06)
#define NAND_FLASH_CMD_REG_OFF          (0 + 0x08)
#define NFC_CONFIGURATION_REG_OFF       (0 + 0x0A)
#define ECC_STATUS_RESULT_REG_OFF       (0 + 0x0C)
#define ECC_RSLT_MAIN_AREA_REG_OFF      (0 + 0x0E)
#define ECC_RSLT_SPARE_AREA_REG_OFF     (0 + 0x10)
#define NF_WR_PROT_REG_OFF              (0 + 0x12)
#define UNLOCK_START_BLK_ADD_REG_OFF    (0 + 0x14)
#define UNLOCK_END_BLK_ADD_REG_OFF      (0 + 0x16)
#define NAND_FLASH_WR_PR_ST_REG_OFF     (0 + 0x18)
#define NAND_FLASH_CONFIG1_REG_OFF      (0 + 0x1A)
#define NAND_FLASH_CONFIG2_REG_OFF      (0 + 0x1C)
#define RAM_BUFFER_ADDRESS_RBA_3        0x3
#define NFC_BUFSIZE_1KB                 0x0
#define NFC_BUFSIZE_2KB                 0x1
#define NFC_CONFIGURATION_UNLOCKED      0x2
#define ECC_STATUS_RESULT_NO_ERR        0x0
#define ECC_STATUS_RESULT_1BIT_ERR      0x1
#define ECC_STATUS_RESULT_2BIT_ERR      0x2
#define NF_WR_PROT_UNLOCK               0x4
#define NAND_FLASH_CONFIG1_FORCE_CE     (1 << 7)
#define NAND_FLASH_CONFIG1_RST          (1 << 6)
#define NAND_FLASH_CONFIG1_BIG          (1 << 5)
#define NAND_FLASH_CONFIG1_INT_MSK      (1 << 4)
#define NAND_FLASH_CONFIG1_ECC_EN       (1 << 3)
#define NAND_FLASH_CONFIG1_SP_EN        (1 << 2)
#define NAND_FLASH_CONFIG2_INT_DONE     (1 << 15)
#define NAND_FLASH_CONFIG2_FDO_PAGE     (0 << 3)
#define NAND_FLASH_CONFIG2_FDO_ID       (2 << 3)
#define NAND_FLASH_CONFIG2_FDO_STATUS   (4 << 3)
#define NAND_FLASH_CONFIG2_FDI_EN       (1 << 2)
#define NAND_FLASH_CONFIG2_FADD_EN      (1 << 1)
#define NAND_FLASH_CONFIG2_FCMD_EN      (1 << 0)
#define FDO_PAGE_SPARE_VAL              0x8

#define MXC_NAND_BASE_DUMMY             0xE0000000
#define NOR_FLASH_BOOT                  0
#define NAND_FLASH_BOOT                 0x10000000
#define SDRAM_NON_FLASH_BOOT            0x20000000
#define MXCBOOT_FLAG_REG                (AVIC_BASE_ADDR + 0x100)
#define MXCFIS_NOTHING                  0x00000000
#define MXCFIS_NAND                     0x10000000
#define MXCFIS_NOR                      0x20000000
#define MXCFIS_FLAG_REG                 (AVIC_BASE_ADDR + 0x104)

#define IS_BOOTING_FROM_NAND()          (readl(MXCBOOT_FLAG_REG) == NAND_FLASH_BOOT)
#define IS_BOOTING_FROM_NOR()           (readl(MXCBOOT_FLAG_REG) == NOR_FLASH_BOOT)
#define IS_BOOTING_FROM_SDRAM()         (readl(MXCBOOT_FLAG_REG) == SDRAM_NON_FLASH_BOOT)

#ifndef MXCFLASH_SELECT_NAND
#define IS_FIS_FROM_NAND()              0
#else
#define IS_FIS_FROM_NAND()              (readl(MXCFIS_FLAG_REG) == MXCFIS_NAND)
#endif

#ifndef MXCFLASH_SELECT_NOR
#define IS_FIS_FROM_NOR()               0
#else
#define IS_FIS_FROM_NOR()               (!IS_FIS_FROM_NAND())
#endif

#define MXC_ASSERT_NOR_BOOT()           writel(MXCFIS_NOR, MXCFIS_FLAG_REG)
#define MXC_ASSERT_NAND_BOOT()          writel(MXCFIS_NAND, MXCFIS_FLAG_REG)

/*
 * This macro is used to get certain bit field from a number
 */
#define MXC_GET_FIELD(val, len, sh)          ((val >> sh) & ((1 << len) - 1))

/*
 * This macro is used to set certain bit field inside a number
 */
#define MXC_SET_FIELD(val, len, sh, nval)    ((val & ~(((1 << len) - 1) << sh)) | (nval << sh))

#define UART_WIDTH_32         /* internal UART is 32bit access only */

#define L2CC_ENABLED

#if !defined(__ASSEMBLER__)
void cyg_hal_plf_serial_init(void);
void cyg_hal_plf_serial_stop(void);
void hal_delay_us(unsigned int usecs);
#define HAL_DELAY_US(n)     hal_delay_us(n)

enum plls {
    MCU_PLL,
    DSP_PLL,
    USB_PLL,
};

enum main_clocks {
    CPU_CLK,
    AHB_CLK,
    IPG_CLK,
    NFC_CLK,
    USB_CLK,
};

enum peri_clocks {
    UART1_BAUD,
    UART2_BAUD,
    UART3_BAUD,
    SSI1_BAUD,
    SSI2_BAUD,
    CSI_BAUD,
    FIRI_BAUD,
};

unsigned int pll_clock(enum plls pll);

unsigned int get_main_clock(enum main_clocks clk);

unsigned int get_peri_clock(enum peri_clocks clk);

typedef unsigned int nfc_setup_func_t(unsigned int, unsigned int, unsigned int);

#endif //#if !defined(__ASSEMBLER__)

#define HAL_MMU_OFF() \
CYG_MACRO_START          \
    asm volatile (                                                      \
        "mcr p15, 0, r0, c7, c14, 0;"                                   \
        "mcr p15, 0, r0, c7, c10, 4;" /* drain the write buffer */      \
        "mcr p15, 0, r0, c7, c5, 0;" /* invalidate I cache */           \
        "mrc p15, 0, r0, c1, c0, 0;" /* read c1 */                      \
        "bic r0, r0, #0x7;" /* disable DCache and MMU */                \
        "bic r0, r0, #0x1000;" /* disable ICache */                     \
        "mcr p15, 0, r0, c1, c0, 0;" /*  */                             \
        "nop;" /* flush i+d-TLBs */                                     \
        "nop;" /* flush i+d-TLBs */                                     \
        "nop;" /* flush i+d-TLBs */                                     \
        :                                                               \
        :                                                               \
        : "r0","memory" /* clobber list */);                            \
CYG_MACRO_END

#endif // __HAL_SOC_H__
