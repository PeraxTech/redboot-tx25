//==========================================================================
//
//      cmds.c
//
//      SoC [platform] specific RedBoot commands
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
#include <redboot.h>
#include <cyg/hal/hal_intr.h>
#include <cyg/hal/plf_mmap.h>
#include <cyg/hal/hal_soc.h>         // Hardware definitions
#include <cyg/hal/hal_cache.h>

typedef unsigned long long  u64;
typedef unsigned int        u32;
typedef unsigned short      u16;
typedef unsigned char       u8;

#define SZ_DEC_1M       1000000
#define PLL_PD_MAX      16      //actual pd+1
#define PLL_MFI_MAX     15
#define PLL_MFI_MIN     5
#define ARM_DIV_MAX     6       //should be enough even though max is 12
#define IPG_DIV_MAX     16
#define AHB_DIV_MAX     16
#define NFC_DIV_MAX     16

#define REF_IN_CLK_NUM  4
struct fixed_pll_mfd {
    u32 ref_clk_hz;
    u32 mfd;
};
const struct fixed_pll_mfd fixed_mfd[REF_IN_CLK_NUM] = {
    {FREQ_CKIH_16_8M,       1680},
    {FREQ_DIGRF_26M,        26 * 16},    // 416
    {2 * FREQ_CKIH_16_8M,   1680},
    {2 * FREQ_DIGRF_26M,    26 * 16},    // 416
};

struct pll_param {
    u32 pd;
    u32 mfi;
    u32 mfn;
    u32 mfd;
};

#define PLL_FREQ_MAX(_ref_clk_)    (2 * _ref_clk_ * PLL_MFI_MAX)
#define PLL_FREQ_MIN(_ref_clk_)    ((2 * _ref_clk_ * (PLL_MFI_MIN - 1)) / PLL_PD_MAX)
#define AHB_CLK_MAX     133333333
#define IPG_CLK_MAX     (AHB_CLK_MAX / 2)
#define NFC_CLK_MAX     25000000

#define ERR_WRONG_CLK   -1
#define ERR_NO_MFI      -2
#define ERR_NO_MFN      -3
#define ERR_NO_PD       -4
#define ERR_NO_ARM_DIV  -5
#define ERR_NO_AHB_DIV  -6

static void clock_setup(int argc, char *argv[]);
static void ckoh(int argc, char *argv[]);
static void ckol(int argc, char *argv[]);
int gcd(int m, int n);

static volatile u32 *crm_ap_base = REG32_PTR(CRM_AP_BASE_ADDR);

#define PLL0_BASE_ADDR      ADPLL_BASE_ADDR
#define PLL1_BASE_ADDR      BDPLL_BASE_ADDR
#define PLL2_BASE_ADDR      UDPLL_BASE_ADDR

#define PLL0                MCU_PLL
#define PLL1                DSP_PLL
#define PLL2                USB_PLL

static volatile u32 *pll_base[] =
{
    REG32_PTR(ADPLL_BASE_ADDR),
    REG32_PTR(BDPLL_BASE_ADDR),
    REG32_PTR(UDPLL_BASE_ADDR),
};

#define NOT_ON_VAL  0xDEADBEEF

RedBoot_cmd("clock",
            "Setup/Display clock (max AHB=133MHz, max IPG=66.5MHz)\nSyntax:",
            "[<core clock in MHz> [:<AHB-to-core divider>[:<IPG-to-AHB divider>]]] \n\n\
If a divider is zero or no divider is specified, the optimal divider values \n\
will be chosen. Examples:\n\
   [clock]         -> Show various clocks\n\
   [clock 532]     -> Core=532  AHB=133           IPG=66.5\n\
   [clock 399]     -> Core=399  AHB=133           IPG=66.5\n\
   [clock 399:6]   -> Core=399  AHB=66.5(Core/8)  IPG=66.5\n\
   [clock 399:6:2] -> Core=532  AHB=66.5(Core/8)  IPG=33.25(AHB/2)\n",
            clock_setup
           );

/*!
 * This is to calculate various parameters based on reference clock and 
 * targeted clock based on the equation:
 *      t_clk = 2*ref_freq*(mfi + mfn/(mfd+1))/(pd+1)
 * This calculation is based on a fixed MFD value for simplicity.
 *
 * @param ref       reference clock freq in Hz
 * @param target    targeted clock in Hz
 * @param p_pd      calculated pd value (pd value from register + 1) upon return
 * @param p_mfi     calculated actual mfi value upon return
 * @param p_mfn     calculated actual mfn value upon return
 * @param p_mfd     fixed mfd value (mfd value from register + 1) upon return
 *
 * @return          0 if successful; non-zero otherwise.
 */
int calc_pll_params(u32 ref, u32 target, struct pll_param *pll)
{
    u64 pd, mfi = 1, mfn, mfd, n_target = target, n_ref = ref, i;

    // make sure targeted freq is in the valid range. Otherwise the 
    // following calculation might be wrong!!!
    if (n_target < PLL_FREQ_MIN(ref) || n_target > PLL_FREQ_MAX(ref))
        return ERR_WRONG_CLK;
    for (i = 0; ; i++) {
        if (i == REF_IN_CLK_NUM)
            return ERR_WRONG_CLK;
        if (fixed_mfd[i].ref_clk_hz == ref) {
            mfd = fixed_mfd[i].mfd;
            break;
        }
    }
    // use n_target and n_ref to avoid overflow
    for (pd = 1; pd <= PLL_PD_MAX; pd++) {
        mfi = (n_target * pd) / (2 * n_ref);
        if (mfi > PLL_MFI_MAX)
            return ERR_NO_MFI;
        else if (mfi < 5)
            continue;
        break;
    }
    // Now got pd and mfi already
    mfn = (((n_target * pd) / 2 - n_ref * mfi) * mfd) / n_ref;
#ifdef CMD_CLOCK_DEBUG
    diag_printf("%d: ref=%d, target=%d, pd=%d, mfi=%d,mfn=%d, mfd=%d\n", 
                __LINE__, ref, (u32)n_target, (u32)pd, (u32)mfi, (u32)mfn, (u32)mfd);
#endif    
    i = 1;
    if (mfn != 0)
        i = gcd(mfd, mfn);
    pll->pd = (u32)pd;
    pll->mfi = (u32)mfi;
    pll->mfn = (u32)(mfn / i);
    pll->mfd = (u32)(mfd / i);
    return 0;
}

/*!
 * This function assumes the expected core clock has to be changed by
 * modifying the PLL. This is NOT true always but for most of the times,
 * it is. So it assumes the PLL output freq is the same as the expected 
 * core clock (arm_div=1) unless the core clock is less than PLL_FREQ_MIN.
 * In the latter case, it will try to increase the arm_div value until 
 * (arm_div*core_clk) is greater than PLL_FREQ_MIN. It then makes call to
 * calc_pll_params() and obtains the values of PD, MFI,MFN, MFD based
 * on the targeted PLL and reference input clock to the PLL. Lastly, 
 * it sets the register based on these values along with the dividers.
 * Note 1) There is no value checking for the passed-in divider values
 *         so the caller has to make sure those values are sensible.
 *      2) Also adjust the NFC divider such that the NFC clock doesn't
 *         exceed NFC_CLK_MAX.
 *      3) This function should not have allowed diag_printf() calls since
 *         the serial driver has been stoped. But leave then here to allow
 *         easy debugging by NOT calling the cyg_hal_plf_serial_stop().
 *      4) The IPG divider doesn't go through AHB divider
 * 
 * @param ref       pll input reference clock (32KHz or 26MHz)
 * @param core_clk  core clock in Hz
 * @param ahb_div   ahb divider to divide the core clock to get ahb clock 
 *                  (ahb_div - 1) needs to be set in the register
 * @param ipg_div   ipg divider to divide the core clock to get ipg clock
 *                  (ipg_div - 1) needs to be set in the register
 # @return          0 if successful; non-zero otherwise
 */
int configure_clock(u32 ref, u32 core_clk, u32 ahb_div, u32 ipg_div)
{
    u32 pll, arm_div = 1, nfc_div, acdr, acder2;
    struct pll_param pll_param;
    int ret;

    // assume pll default to core clock first
    pll = core_clk;
    // when core_clk >= PLL_FREQ_MIN, the arm_div can be 1.
    // Otherwise, need to calculate arm_div value below and adjust the targeted pll
    if (core_clk < PLL_FREQ_MIN(ref)) {
        for (arm_div = 1; arm_div <= ARM_DIV_MAX; arm_div++) {
            if ((core_clk * arm_div) > PLL_FREQ_MIN(ref)) {
                break;
            }
        }
        if (arm_div == (ARM_DIV_MAX + 1)) {
            diag_printf("can't make arm_div=%d\n", arm_div);
            return ERR_NO_ARM_DIV;
        }
        pll = core_clk * arm_div;
    }

    // get nfc_div - make sure optimal NFC clock but less than NFC_CLK_MAX
    for (nfc_div = 1; nfc_div <= NFC_DIV_MAX; nfc_div++) {
        if ((pll / (ahb_div * nfc_div)) <= NFC_CLK_MAX) {
            break;
        }
    }

    // pll is now the targeted pll output. Use it along with ref input clock
    // to get pd, mfi, mfn, mfd
    if ((ret = calc_pll_params(ref, pll, &pll_param)) != 0) {
        diag_printf("can't find pll parameters: %d\n", ret);
        return ret;
    }
#ifdef CMD_CLOCK_DEBUG
    diag_printf("ref=%d, pll=%d, pd=%d, mfi=%d,mfn=%d, mfd=%d\n", 
                ref, pll, pll_param.pd, pll_param.mfi, pll_param.mfn, pll_param.mfd);
#endif
    acdr = (((arm_div == 1) ? 0x8 : (arm_div - 2)) << 8) |
           (((ahb_div == 16)? 0x0 : ahb_div) << 4) |
           ((ipg_div == 16)? 0x0 : ipg_div);

    acder2 = (readl(CRM_AP_BASE_ADDR + CRM_AP_ACDER2) & 0xFFF0FFFF) |
             ((nfc_div - 1) << 16);

    // switch to ap_ref_clk
    writel(readl(CRM_AP_BASE_ADDR + CRM_AP_ACSR) & (~0x1), 
           CRM_AP_BASE_ADDR + CRM_AP_ACSR);

    // change the dividers
    writel(acdr, CRM_AP_BASE_ADDR + CRM_AP_ACDR);
    writel(acder2, CRM_AP_BASE_ADDR + CRM_AP_ACDER2);

    // adjust pll settings
    writel(((pll_param.pd - 1) << 0) | (pll_param.mfi << 4), 
           PLL0_BASE_ADDR + PLL_DP_OP);
    writel(pll_param.mfn, PLL0_BASE_ADDR + PLL_DP_MFN);
    writel(pll_param.mfd - 1, PLL0_BASE_ADDR + PLL_DP_MFD);
    writel(((pll_param.pd - 1) << 0) | (pll_param.mfi << 4), 
           PLL0_BASE_ADDR + PLL_DP_HFS_OP);
    writel(pll_param.mfn, PLL0_BASE_ADDR + PLL_DP_HFS_MFN);
    writel(pll_param.mfd - 1, PLL0_BASE_ADDR + PLL_DP_HFS_MFD);

    // switch back to pll
    writel(readl(CRM_AP_BASE_ADDR + CRM_AP_ACSR) | 0x1, 
           CRM_AP_BASE_ADDR + CRM_AP_ACSR);

    return 0;
}

static void clock_setup(int argc,char *argv[])
{
    u32 i, core_clk, ipg_div, data[3], uart1_baud, uart3_baud, ssi1_baud, firi_baud;
    u32 ssi2_baud, csi_baud, ahb_div, ahb_clk, ipg_clk, clk_sel, ref_clk;
    int ret;

    if (argc == 1)
        goto print_clock;
    for (i = 0;  i < 3;  i++) {
        unsigned long temp;
        if (!parse_num(*(&argv[1]), &temp, &argv[1], ":")) {
            diag_printf("Error: Invalid parameter\n");
            return;
        }
        data[i] = temp;
    }

    core_clk = data[0] * SZ_DEC_1M;
    ahb_div = data[1];  // actual register field + 1
    ipg_div = data[2];  // actual register field + 1

    // since only support set clock for the AP domain, get ref input clock
    // for the AP domain.
    clk_sel = MXC_GET_FIELD(readl(PLL0_BASE_ADDR + PLL_DP_CTL), 2, 8);
    ref_clk = fixed_mfd[clk_sel].ref_clk_hz;

    if (core_clk < (PLL_FREQ_MIN(ref_clk) / ARM_DIV_MAX) || 
        core_clk > PLL_FREQ_MAX(ref_clk)) {
        diag_printf("Targeted core clock should be within [%d - %d]\n", 
                    PLL_FREQ_MIN(ref_clk) / ARM_DIV_MAX, 
                    PLL_FREQ_MAX(ref_clk));
        return;
    }

    // find the ahb divider  
    if (ahb_div > AHB_DIV_MAX) {
        diag_printf("Invalid AHB divider: %d. Maximum value is %d\n",
                    ahb_div, AHB_DIV_MAX);
        return;
    }
    if (ahb_div == 0) {
        // no HCLK divider specified
        for (ahb_div = 1; ; ahb_div++) {
            if ((core_clk / ahb_div) <= AHB_CLK_MAX)
                break;
        }
    }
    if (ahb_div > AHB_DIV_MAX || (core_clk / ahb_div) > AHB_CLK_MAX) {
        diag_printf("Can't make AHB=%d since max=%d\n", 
                    core_clk / ahb_div, AHB_CLK_MAX);
        return;
    }

    // find the ipg divider
    ahb_clk = core_clk / ahb_div;
    if (ipg_div == 0) {
        ipg_div++;          // At least =1
        if (ahb_clk > IPG_CLK_MAX)
            ipg_div++;      // Make it =2
    }
    // adjust ipg_div since IPG clock doesn't go through AHB divider
    ipg_div *= ahb_div;
    ipg_clk = core_clk / ipg_div;
    if (ipg_div > IPG_DIV_MAX || ipg_clk > IPG_CLK_MAX) {
        if (ipg_div > IPG_DIV_MAX)
            diag_printf("Invalid IPG divider: %d. Max is: %d\n", 
                        ipg_div / ahb_div, IPG_DIV_MAX / ahb_div);
        else
            diag_printf("Can't make IPG=%dHz since max=%dHz\n", 
                        ipg_clk, IPG_CLK_MAX);
        return;
    }

    diag_printf("Trying to set core=%d ahb=%d ipg=%d...\n", 
                core_clk, ahb_clk, ipg_clk);

    // stop the serial to be ready to adjust the clock
    hal_delay_us(100000);
    cyg_hal_plf_serial_stop();
    // adjust the clock
    ret = configure_clock(ref_clk, core_clk, ahb_div, ipg_div);
    // restart the serial driver
    cyg_hal_plf_serial_init();
    hal_delay_us(100000);

    if (ret != 0) {
        diag_printf("Failed to setup clock: %d\n", ret);
        return;
    }
    diag_printf("\n<<<New clock setting>>>\n");

    // Now printing clocks
print_clock:
    diag_printf("\nMCUPLL\t\tUSBPLL\t\tDSPPLL\n");
    diag_printf("========================================\n");
    diag_printf("%-16d%-16d%-16d\n\n", pll_clock(MCU_PLL), pll_clock(USB_PLL),
                pll_clock(DSP_PLL));
    diag_printf("CPU\t\tAHB\t\tIPG\t\tNFC\t\tUSB\n");
    diag_printf("===========================================");
    diag_printf("=============================\n");
    diag_printf("%-16d%-16d%-16d%-16d%-16d\n\n",
                get_main_clock(CPU_CLK),
                get_main_clock(AHB_CLK),
                get_main_clock(IPG_CLK),
                get_main_clock(NFC_CLK),
                get_main_clock(USB_CLK));

    uart1_baud = get_peri_clock(UART1_BAUD);
    uart3_baud = get_peri_clock(UART3_BAUD);
    ssi1_baud = get_peri_clock(SSI1_BAUD);
    ssi2_baud = get_peri_clock(SSI2_BAUD);
    csi_baud = get_peri_clock(CSI_BAUD);
    firi_baud = get_peri_clock(FIRI_BAUD);

    diag_printf("UART1/2\t\tUART3\t\tSSI1\t\tSSI2\t\tCSI\t\tFIRI\n");
    diag_printf("===========================================");
    diag_printf("================================================\n");

    (uart1_baud != NOT_ON_VAL) ? diag_printf("%-16d", uart1_baud) :
                                 diag_printf("%-16s", "OFF");
    (uart3_baud != NOT_ON_VAL) ? diag_printf("%-16d", uart3_baud) :
                                 diag_printf("%-16s", "OFF");
    (ssi1_baud != NOT_ON_VAL) ? diag_printf("%-16d", ssi1_baud) :
                                diag_printf("%-16s", "OFF");
    (ssi2_baud != NOT_ON_VAL) ? diag_printf("%-16d", ssi2_baud) :
                                diag_printf("%-16s", "OFF");
    (csi_baud != NOT_ON_VAL) ? diag_printf("%-16d", csi_baud ) :
                               diag_printf("%-16s", "OFF");
    (firi_baud != NOT_ON_VAL) ? diag_printf("%-16d", firi_baud ) :
                               diag_printf("%-16s", "OFF");
    diag_printf("\n\n");
}

/*!
 * This function returns the PLL output value in Hz based on pll.
 */
u32 pll_clock(enum plls pll)
{
    u64 mfi, mfn, mfd, pdf, ref_clk, pll_out, sign;
    u64 dp_op, dp_mfd, dp_mfn, clk_sel;

    clk_sel = MXC_GET_FIELD(pll_base[pll][PLL_DP_CTL >> 2], 2, 8);
    ref_clk = fixed_mfd[clk_sel].ref_clk_hz;

    if ((pll_base[pll][PLL_DP_CTL >> 2] & 0x80) == 0) {
        dp_op = pll_base[pll][PLL_DP_OP >> 2];
        dp_mfd = pll_base[pll][PLL_DP_MFD >> 2];
        dp_mfn = pll_base[pll][PLL_DP_MFN >> 2];
    } else {
        dp_op = pll_base[pll][PLL_DP_HFS_OP >> 2];
        dp_mfd = pll_base[pll][PLL_DP_HFS_MFD >> 2];
        dp_mfn = pll_base[pll][PLL_DP_HFS_MFN >> 2];
    }
    pdf = dp_op & 0xF;
    mfi = (dp_op >> 4) & 0xF;
    mfi = (mfi <= 5) ? 5: mfi;
    mfd = dp_mfd & 0x07FFFFFF;
    mfn = dp_mfn & 0x07FFFFFF;

    sign = (mfn < 0x4000000) ? 0: 1;
    mfn = (mfn <= 0x4000000) ? mfn: (0x8000000 - mfn);

    if (sign == 0) {
        pll_out = (2 * ref_clk * mfi + ((2 * ref_clk * mfn) / (mfd + 1))) /
                  (pdf + 1);
    } else {
        pll_out = (2 * ref_clk * mfi - ((2 * ref_clk * mfn) / (mfd + 1))) /
                  (pdf + 1);
    }

    return (u32)pll_out;
}

const u32 CRM_SMALL_DIV[] = {2, 3, 4, 5, 6, 8, 10, 12};

/*!
 * This function returns the main clock dividers.
 */
u32 clock_divider(enum main_clocks clk)
{
    u32 div = 0;
    u32 acdr, acder2;

    acdr = crm_ap_base[CRM_AP_ACDR >> 2];
    acder2 = crm_ap_base[CRM_AP_ACDER2 >> 2];

    switch (clk) {
    case CPU_CLK:
        div = (acdr >> 8) & 0xF;
        div = (div > 7) ? 1 : (CRM_SMALL_DIV[div]);
        break;
    case AHB_CLK:
        div = (acdr >> 4) & 0xF;
        div = (div == 0) ? 16 : div;
        break;
    case IPG_CLK:
        div = (acdr >> 0) & 0xF;
        div = (div == 0) ? 16 : div;
        break;
    case NFC_CLK:
        div = ((acder2 >> 16) & 0xF) + 1;
        break;
    case USB_CLK:
        div = (acder2 >> 8) & 0xF;
        div = (div > 7) ? 1 : (CRM_SMALL_DIV[div]);
        break;
    default:
        diag_printf("Wrong clock: %d\n", clk);
        break;
    }

    return div;
}

/*!
 * This function returns the peripheral clock dividers.
 */
u32 clock_peri_divider(enum peri_clocks clk)
{
    u32 div = 0;
    u32 apra, acder1, acder2;

    apra = crm_ap_base[CRM_AP_APRA >> 2];
    acder1 = crm_ap_base[CRM_AP_ACDER1 >> 2];
    acder2 = crm_ap_base[CRM_AP_ACDER2 >> 2];

    switch (clk) {
    case UART1_BAUD:
    case UART2_BAUD:
        div = acder2 & 0xF;
        div = (div > 7) ? 1 : (CRM_SMALL_DIV[div]);
        break;
    case UART3_BAUD:
        div = (apra >> 17) & 0xF;
        div = (div > 7) ? 1 : (CRM_SMALL_DIV[div]);
        break;
    case SSI1_BAUD:
        div = acder1 & 0x3F;
        //double the divider to avoid FP
        div = (div == 0 || div == 1) ? (2 * 62) : div;
        break;
    case SSI2_BAUD:
        div = (acder1 >> 8) & 0x3F;
        //double the divider to avoid FP
        div = (div == 0 || div == 1) ? 62 : div;
        break;
    case CSI_BAUD:
        div = (acder1 >> 24) & 0x3F;
        //double the divider to avoid FP
        div = (div == 0 || div == 1) ? 62 : div;
        break;
    case FIRI_BAUD:
        div = ((acder1 >> 16) & 0x1F) + 1;
        break;
    default:
        diag_printf("Wrong clock: %d\n", clk);
        break;
    }

    return div;
}

void get_ref_clk(u32 *ap_unc_pat_ref, u32 *ap_ref_x2,
                 u32 *ap_ref)
{
    u32 ap_pat_ref_div_1, ap_pat_ref_div_2, ap_isel,
        ascsr, adcr, acder2, clk_sel, ref_clk;

    clk_sel = MXC_GET_FIELD(readl(PLL0_BASE_ADDR + PLL_DP_CTL), 2, 8);
    ref_clk = fixed_mfd[clk_sel].ref_clk_hz;

    ascsr = crm_ap_base[CRM_AP_ASCSR >> 2];
    adcr = crm_ap_base[CRM_AP_ADCR >> 2];
    acder2 = crm_ap_base[CRM_AP_ACDER2 >> 2];

    ap_isel = ascsr & 0x1;
    ap_pat_ref_div_1 = ((ascsr >> 2) & 0x1) + 1;
    ap_pat_ref_div_2 = ((ascsr >> 15) & 0x1) + 1;

    *ap_unc_pat_ref = ref_clk * (ap_isel + 1);
    *ap_ref_x2 =  (*ap_unc_pat_ref)/ ap_pat_ref_div_1;
    *ap_ref = (*ap_ref_x2) / ap_pat_ref_div_2;
}

/*!
 * This function returns the main clock value in Hz.
 */
u32 get_main_clock(enum main_clocks clk)
{
    u32 ret_val = 0, apsel, ap_clk_pre_dfs, acsr, ascsr, adcr, acder2;
    u32 lfdf = 1, ap_ref_x2_clk, ap_ref_clk, usbsel, ap_unc_pat_ref;

    acsr = crm_ap_base[CRM_AP_ACSR >> 2];
    ascsr = crm_ap_base[CRM_AP_ASCSR >> 2];
    adcr = crm_ap_base[CRM_AP_ADCR >> 2];
    acder2 = crm_ap_base[CRM_AP_ACDER2 >> 2];

    get_ref_clk(&ap_unc_pat_ref, &ap_ref_x2_clk, &ap_ref_clk);

    if ((acsr & 0x1) == 0) {
        // inverted pat_ref is selected
        ap_clk_pre_dfs = ap_ref_clk;
    } else {
        // Now AP domain runs off the pll
        apsel = (ascsr >> 3) & 0x3;
        ap_clk_pre_dfs = pll_clock(apsel) / clock_divider(CPU_CLK);
    }

    switch (clk) {
    case CPU_CLK:
        if (((adcr & 0x2) == 0) && ((adcr & 0x20) != 0) && ((adcr & 0x80) == 0)) {
            // DFS divider used
            lfdf = 2 ^ ((adcr >> 8) & 0x3);
        }
        ret_val = ap_clk_pre_dfs / lfdf;
        break;
    case AHB_CLK:
        ret_val = ap_clk_pre_dfs / clock_divider(AHB_CLK);
        break;
    case IPG_CLK:
        ret_val = ap_clk_pre_dfs / clock_divider(IPG_CLK);
        break;
    case NFC_CLK:
        if ((acder2 & (1 << 20)) == 0) {
            diag_printf("Warning: NFC clock is not enabled !!!\n");
        } else {
            ret_val = ap_clk_pre_dfs / (clock_divider(AHB_CLK) *
                                        clock_divider(NFC_CLK));
        }
        break;
    case USB_CLK:
        if ((acder2 & (1 << 12)) == 0) {
            diag_printf("Warning: USB clock is not enabled !!!\n");
        } else {
            if ((acsr & 0x1) == 0) {
                // inverted pat_ref is selected
                ret_val = ap_ref_clk / clock_divider(USB_CLK);
            } else {
                usbsel = (ascsr >> 13) & 0x3;
                if (usbsel == 0x3)
                    diag_printf("unknown mrcg_2_clk for USB input\n");
                else
                    ret_val = pll_clock(usbsel) / clock_divider(USB_CLK);
            }
        }
        break;
    default:
        break;
    }

    return ret_val;
}

/*!
 * This function returns the peripheral clock value in Hz.
 */
u32 get_peri_clock(enum peri_clocks clk)
{
    u32 apra, ascsr, acder1, acder2, ap_unc_pat_ref,
    ap_ref_x2_clk, ap_ref_clk, ret_val = 0, sel;

    apra = crm_ap_base[CRM_AP_APRA >> 2];
    acder1 = crm_ap_base[CRM_AP_ACDER1 >> 2];
    acder2 = crm_ap_base[CRM_AP_ACDER2 >> 2];
    ascsr = crm_ap_base[CRM_AP_ASCSR >> 2];

    get_ref_clk(&ap_unc_pat_ref, &ap_ref_x2_clk, &ap_ref_clk);

    switch (clk) {
    case UART1_BAUD:
        if ((apra & 0x1) == 0) {
            return NOT_ON_VAL;
        }
        ret_val = ap_unc_pat_ref / clock_peri_divider(UART1_BAUD);
        break;
    case UART2_BAUD:
        if ((apra & 0x100) == 0) {
            return NOT_ON_VAL;
        }
        ret_val = ap_unc_pat_ref / clock_peri_divider(UART2_BAUD);
        break;
    case UART3_BAUD:
        if ((apra & 0x10000) == 0) {
            return NOT_ON_VAL;
        }
        ret_val = ap_unc_pat_ref / clock_peri_divider(UART3_BAUD);
        break;
    case SSI1_BAUD:
        if ((acder1 & (1 << 6)) == 0) {
            return NOT_ON_VAL;
        }

        sel = (ascsr >> 5) & 0x3;
        // Don't forget to double the divider
        ret_val = (2 * pll_clock(sel)) / clock_peri_divider(SSI1_BAUD);
        break;
    case SSI2_BAUD:
        if ((acder1 & (1 << 14)) == 0) {
            return NOT_ON_VAL;
        }

        sel = (ascsr >> 7) & 0x3;
        // Don't forget to double the divider
        ret_val = (2 *pll_clock(sel)) / clock_peri_divider(SSI2_BAUD);
        break;
    case CSI_BAUD:
        if ((acder1 & (1 << 30)) == 0) {
            return NOT_ON_VAL;
        }

        sel = (ascsr >> 11) & 0x3;
        // Don't forget to double the divider
        ret_val = (2 * pll_clock(sel)) / (clock_peri_divider(CSI_BAUD));
        break;
    case FIRI_BAUD:
        if ((acder1 & (1 << 22)) == 0) {
            return NOT_ON_VAL;
        }

        sel = (ascsr >> 9) & 0x3;
        // Don't forget to double the divider
        ret_val = pll_clock(sel) / (clock_peri_divider(FIRI_BAUD));
        break;
    }

    return ret_val;
}

RedBoot_cmd("ckoh",
            "Select clock source for CKOH (J9 on CPU daughter card)",
            " Default is 1/10 of ARM core\n\
          <0> - display current ckoh selection \n\
          <1> - ap_uncorrected_pat_ref_clk \n\
          <2> - ungated_ap_clk (ARM Core in normal case) \n\
          <3> - ungated_ap_ahb_clk (AHB) \n\
          <4> - ungated_ap_pclk (IPG) \n\
          <5> - usb_clk \n\
          <6> - ap_perclk (baud clock) \n\
          <7> - ap_ckil_clk (sync) \n\
          <8> - ap_pat_ref_clk (ungated sync)\n",
            ckoh
           );

static u8* div_str[] = {
    "1/2 of ",
    "1/3 of ",
    "1/4 of ",
    "1/5 of ",
    "1/6 of ",
    "1/8 of ",
    "1/10 of ",
    "1/12 of ",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
};

static u8* ckoh_name[] ={
    "NULL",
    "ap_uncorrected_pat_ref_clk",
    "ungated_ap_clk (ARM Core in normal case)",
    "ungated_ap_ahb_clk (AHB)",
    "ungated_ap_pclk (IPG)",
    "usb_clk",
    "ap_perclk (baud clock)",
    "ap_ckil_clk (sync)",
    "ap_pat_ref_clk (ungated sync)",
    "crm_ap_nfc_clk",
    "ap_async_pat_ref_clk for EL1T and MQSPI",
    "ap_sdhc1_perclk",
    "ap_ahb_div2_clk (for SAHARA)",
    "ipu_lpmc_hsp_clk",
};

#define CKOH_MAX_INDEX          (sizeof(ckoh_name) / sizeof(u8*))
#define CKOH_DIV                6  // default divide by 10

extern u32 system_rev;

static void ckoh(int argc,char *argv[])
{
    u32 action = 0, val, new_val, div = 0x8, i;

    if (!scan_opts(argc, argv, 1, 0, 0, (void*) &action,
                   OPTION_ARG_TYPE_NUM, "action"))
        return;

    if (action >= CKOH_MAX_INDEX || 
        (((system_rev >> 4) & 0xF) == CHIP_REV_1_0 && action > 8)) {
        diag_printf("%d is not supported\n\n", action);
        return;
    }

    val = readl(CRM_AP_BASE_ADDR + CRM_AP_ACR);

    if (action != 0) {
        // set CKOHDIV to be 6 for dividing by 10
        if (action == 2 || action == 3)
            div = CKOH_DIV;
        action--;
        // clear CKOHS-HIGH, CKOHD, CHOHS, CKOHDIV bits and
        new_val = (val & (~(1 << 18 | 0xFF00))) | (div << 8);
        if (action > 7) {
            new_val |= (1 << 18) | ((action & 7) << 12); 
        } else {
            new_val |= action << 12;
        }
        writel(new_val, CRM_AP_BASE_ADDR + CRM_AP_ACR);
        diag_printf("\nSet ckoh to ");
    }

    val = readl(CRM_AP_BASE_ADDR + CRM_AP_ACR);
    /* locate the index in the name table */
    new_val = ((val >> 15) & 8) | ((val >> 12) & 7);
    i = (val >> 8) & 0xF;
    diag_printf("%s%s\n", div_str[i], ckoh_name[new_val + 1]);
    diag_printf("ACR register[0x%x]=0x%x\n\n", 
                (CRM_AP_BASE_ADDR + CRM_AP_ACR), val);
}

RedBoot_cmd("ckol",
            "Select clock source for CKO (J10 on EVB CPU card)",
            " Default is CKIL\n\
          <0> - display current cko selection\n\
          <1> - ckil \n\
          <2> - ap_pat_ref_clk (ungated sync) \n\
          <3> - ap_ref_x2_clk \n\
          <4} - ssi1_clk \n\
          <5> - ssi2_clk \n\
          <6> - cs_clk \n\
          <7> - firi_clk \n\
          <8> - ap_uncorrected_pat_ref_clk \n",
            ckol
           );

static u8* cko_name[] ={
    "NULL",
    "ckil",
    "ap_pat_ref_clk (ungated sync)",
    "ap_ref_x2_clk",
    "ssi1_clk",
    "ssi2_clk",
    "cs_clk",
    "firi_clk",
    "ap_uncorrected_pat_ref_clk",
};

#define CKO_MAX_INDEX           (sizeof(cko_name) / sizeof(u8*))

static void ckol(int argc,char *argv[])
{
    u32 action = 0, val, new_val, t;

    if (!scan_opts(argc, argv, 1, 0, 0, (void*) &action,
                   OPTION_ARG_TYPE_NUM, "action"))
        return;

    if (action >= CKO_MAX_INDEX) {
        diag_printf("%d is not supported\n\n", action);
        return;
    }

    val = readl(CRM_AP_BASE_ADDR + CRM_AP_ACR);

    if (action != 0) {
        // turn on these clocks
        switch (action) {
        case 4: //SSI1
            t = readl(CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            writel(t | (1 << 6), CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            break;
        case 5: //SSI2
            t = readl(CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            writel(t | (1 << 14), CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            break;
        case 6: //cs_clk
            t = readl(CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            writel(t | (1 << 30), CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            break;
        case 7: //firi_clk
            t = readl(CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            writel(t | (1 << 22), CRM_AP_BASE_ADDR + CRM_AP_ACDER1);
            break;
        }
        action--;
        /* clear CKOS-HIGH, CKOD, CHOS bits and */
        new_val = val & (~((1 << 16) | 0xF0));
        if (action > 5) {
            new_val |= (1 << 16) | ((action & 7) << 4); 
        } else {
            new_val |= action << 4;
        }
        writel(new_val, CRM_AP_BASE_ADDR + CRM_AP_ACR);
        diag_printf("\nSet cko to ");
    }

    val = readl(CRM_AP_BASE_ADDR + CRM_AP_ACR);
    /* locate the index in the name table */
    new_val = ((val >> 13) & 8) | ((val >> 4) & 7);

    diag_printf("%s\nACR register[0x%x]=0x%x\n\n", cko_name[new_val + 1],
                (CRM_AP_BASE_ADDR + CRM_AP_ACR), val);
}

#ifdef L2CC_ENABLED
/*
 * This command is added for some simple testing only. It turns on/off
 * L2 cache regardless of L1 cache state. The side effect of this is
 * when doing any flash operations such as "fis init", the L2
 * will be turned back on along with L1 caches even though it is off
 * by using this command.
 */
RedBoot_cmd("L2",
            "L2 cache",
            "[ON | OFF]",
            do_L2_caches
           );

void do_L2_caches(int argc, char *argv[])
{
    u32 oldints;
    int L2cache_on=0;

    if (argc == 2) {
        if (strcasecmp(argv[1], "on") == 0) {
            HAL_DISABLE_INTERRUPTS(oldints);
            HAL_ENABLE_L2();
            HAL_RESTORE_INTERRUPTS(oldints);
        } else if (strcasecmp(argv[1], "off") == 0) {
            HAL_DISABLE_INTERRUPTS(oldints);
            HAL_CLEAN_INVALIDATE_L2();
            HAL_DISABLE_L2();
            HAL_RESTORE_INTERRUPTS(oldints);
        } else {
            diag_printf("Invalid L2 cache mode: %s\n", argv[1]);
        }
    } else {
        HAL_L2CACHE_IS_ENABLED(L2cache_on);
        diag_printf("L2 cache: %s\n", L2cache_on?"On":"Off");
    }
}
#endif //L2CC_ENABLED

#define IIM_ERR_SHIFT       8
#define POLL_FUSE_PRGD      (IIM_STAT_PRGD | (IIM_ERR_PRGE << IIM_ERR_SHIFT))
#define POLL_FUSE_SNSD      (IIM_STAT_SNSD | (IIM_ERR_SNSE << IIM_ERR_SHIFT))

static void fuse_op_start(void)
{
    /* Do not generate interrupt */
    writel(0, IIM_BASE_ADDR + IIM_STATM_OFF);
    // clear the status bits and error bits
    writel(0x3, IIM_BASE_ADDR + IIM_STAT_OFF);
    writel(0xFE, IIM_BASE_ADDR + IIM_ERR_OFF);
}

/*
 * The action should be either:
 *          POLL_FUSE_PRGD 
 * or:
 *          POLL_FUSE_SNSD
 */
static int poll_fuse_op_done(int action)
{

    u32 status, error;

    if (action != POLL_FUSE_PRGD && action != POLL_FUSE_SNSD) {
        diag_printf("%s(%d) invalid operation\n", __FUNCTION__, action);
        return -1;
    }

    /* Poll busy bit till it is NOT set */
    while ((readl(IIM_BASE_ADDR + IIM_STAT_OFF) & IIM_STAT_BUSY) != 0 ) {
    }

    /* Test for successful write */
    status = readl(IIM_BASE_ADDR + IIM_STAT_OFF);
    error = readl(IIM_BASE_ADDR + IIM_ERR_OFF);

    if ((status & action) != 0 && (error & (action >> IIM_ERR_SHIFT)) == 0) {
        if (error) {
            diag_printf("Even though the operation seems successful...\n");
            diag_printf("There are some error(s) at addr=0x%x: 0x%x\n",
                        (IIM_BASE_ADDR + IIM_ERR_OFF), error);
        }
        return 0;
    }
    diag_printf("%s(%d) failed\n", __FUNCTION__, action);
    diag_printf("status address=0x%x, value=0x%x\n",
                (IIM_BASE_ADDR + IIM_STAT_OFF), status);
    diag_printf("There are some error(s) at addr=0x%x: 0x%x\n",
                (IIM_BASE_ADDR + IIM_ERR_OFF), error);
    return -1;
}

static void sense_fuse(int bank, int row, int bit)
{
    int addr, addr_l, addr_h, reg_addr;

    fuse_op_start();
    
    addr = ((bank << 11) | (row << 3) | (bit & 0x7));
    /* Set IIM Program Upper Address */
    addr_h = (addr >> 8) & 0x000000FF;
    /* Set IIM Program Lower Address */
    addr_l = (addr & 0x000000FF);

#ifdef IIM_FUSE_DEBUG
    diag_printf("%s: addr_h=0x%x, addr_l=0x%x\n",
                __FUNCTION__, addr_h, addr_l);
#endif
    writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
    writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
    /* Start sensing */
    writel(0x8, IIM_BASE_ADDR + IIM_FCTL_OFF);
    if (poll_fuse_op_done(POLL_FUSE_SNSD) != 0) {
        diag_printf("%s(bank: %d, row: %d, bit: %d failed\n",
                    __FUNCTION__, bank, row, bit);
    }
    reg_addr = IIM_BASE_ADDR + IIM_SDAT_OFF;
    diag_printf("fuses at (bank:%d, row:%d) = 0x%x\n", bank, row, readl(reg_addr));
}

void do_fuse_read(int argc, char *argv[])
{
    int bank, row;

    if (argc == 1) {
        diag_printf("Useage: fuse_read <bank> <row>\n");
        return;
    } else if (argc == 3) {
        if (!parse_num(*(&argv[1]), (unsigned long *)&bank, &argv[1], " ")) {
                diag_printf("Error: Invalid parameter\n");
            return;
        }
        if (!parse_num(*(&argv[2]), (unsigned long *)&row, &argv[2], " ")) {
                diag_printf("Error: Invalid parameter\n");
                return;
            }

        diag_printf("Read fuse at bank:%d row:%d\n", bank, row);
        sense_fuse(bank, row, 0);

    } else {
        diag_printf("Passing in wrong arguments: %d\n", argc);
        diag_printf("Useage: fuse_read <bank> <row>\n");
    }
}

/* Blow fuses based on the bank, row and bit positions (all 0-based)
*/
static int fuse_blow(int bank,int row,int bit)
{
    int addr, addr_l, addr_h, ret = -1;

    fuse_op_start();

    /* Disable IIM Program Protect */
    writel(0xAA, IIM_BASE_ADDR + IIM_PREG_P_OFF);

    addr = ((bank << 11) | (row << 3) | (bit & 0x7));
    /* Set IIM Program Upper Address */
    addr_h = (addr >> 8) & 0x000000FF;
    /* Set IIM Program Lower Address */
    addr_l = (addr & 0x000000FF);

#ifdef IIM_FUSE_DEBUG
    diag_printf("blowing addr_h=0x%x, addr_l=0x%x\n", addr_h, addr_l);
#endif

    writel(addr_h, IIM_BASE_ADDR + IIM_UA_OFF);
    writel(addr_l, IIM_BASE_ADDR + IIM_LA_OFF);
    /* Start Programming */
    writel(0x31, IIM_BASE_ADDR + IIM_FCTL_OFF);
    if (poll_fuse_op_done(POLL_FUSE_PRGD) == 0) {
        ret = 0;
    }

    /* Enable IIM Program Protect */
    writel(0x0, IIM_BASE_ADDR + IIM_PREG_P_OFF);
    return ret;
}

/*
 * This command is added for burning IIM fuses
 */
RedBoot_cmd("fuse_read",
            "read some fuses",
            "<bank> <row>",
            do_fuse_read
           );

RedBoot_cmd("fuse_blow",
            "blow some fuses",
            "<bank> <row> <value>",
            do_fuse_blow
           );

#define         INIT_STRING              "12345678"
static char ready_to_blow[] = INIT_STRING;

void quick_itoa(u32 num, char *a) 
{
    int i, j, k;        
    for (i = 0; i <= 7; i++) {
        j = (num >> (4 * i)) & 0xF;
        k = (j < 10) ? '0' : ('a' - 0xa);
        a[i] = j + k;
    }
}

void do_fuse_blow(int argc, char *argv[])
{
    int bank, row, value, i;

    if (argc == 1) {
        diag_printf("It is too dangeous for you to use this command.\n");
        return;
    } else if (argc == 2) {
        if (strcasecmp(argv[1], "nandboot") == 0) {
            quick_itoa(readl(EPIT_BASE_ADDR + EPITCNR), ready_to_blow);
            diag_printf("%s\n", ready_to_blow);
        }
        return;
    } else if (argc == 3) {
        if (strcasecmp(argv[1], "nandboot") == 0 && 
            strcasecmp(argv[2], ready_to_blow) == 0) {
#if defined(CYGPKG_HAL_ARM_MXC91131) || defined(CYGPKG_HAL_ARM_MX21) || defined(CYGPKG_HAL_ARM_MX27) || defined(CYGPKG_HAL_ARM_MX31)
            diag_printf("No need to blow any fuses for NAND boot on this platform\n\n");
#else
            diag_printf("Ready to burn NAND boot fuses\n");
            if (fuse_blow(0, 16, 1) != 0 || fuse_blow(0, 16, 7) != 0) {
                diag_printf("NAND BOOT fuse blown failed miserably ...\n");
            } else {
                diag_printf("NAND BOOT fuse blown successfully ...\n");
            }
        } else {
            diag_printf("Not ready: %s, %s\n", argv[1], argv[2]);
#endif
        }
    } else if (argc == 4) {
        if (!parse_num(*(&argv[1]), (unsigned long *)&bank, &argv[1], " ")) {
                diag_printf("Error: Invalid parameter\n");
                return;
        }
        if (!parse_num(*(&argv[2]), (unsigned long *)&row, &argv[2], " ")) {
                diag_printf("Error: Invalid parameter\n");
                return;
        }
        if (!parse_num(*(&argv[3]), (unsigned long *)&value, &argv[3], " ")) {
                diag_printf("Error: Invalid parameter\n");
                return;
        }

        diag_printf("Blowing fuse at bank:%d row:%d value:%d\n",
                    bank, row, value);
        for (i = 0; i < 8; i++) {
            if (((value >> i) & 0x1) == 0) {
                continue;
            }
            if (fuse_blow(bank, row, i) != 0) {
                diag_printf("fuse_blow(bank: %d, row: %d, bit: %d failed\n",
                            bank, row, i);
            } else {
                diag_printf("fuse_blow(bank: %d, row: %d, bit: %d successful\n",
                            bank, row, i);
            }
        }
        sense_fuse(bank, row, 0);

    } else {
        diag_printf("Passing in wrong arguments: %d\n", argc);
    }
    /* Reset to default string */
    strcpy(ready_to_blow, INIT_STRING);;
}

/* precondition: m>0 and n>0.  Let g=gcd(m,n). */
int gcd(int m, int n)
{
    int t;
    while(m > 0) {
        if(n > m) {t = m; m = n; n = t;} /* swap */
        m -= n;
    }
    return n;
 }
