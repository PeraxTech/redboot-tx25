#ifndef CYGONCE_HAL_PLATFORM_SETUP_H
#define CYGONCE_HAL_PLATFORM_SETUP_H

//=============================================================================
//
//      hal_platform_setup.h
//
//      Platform specific support for HAL (assembly code)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <pkgconf/system.h>             // System-wide configuration info
#include CYGBLD_HAL_VARIANT_H           // Variant specific configuration
#include CYGBLD_HAL_PLATFORM_H          // Platform specific configuration
#include <cyg/hal/hal_soc.h>            // Variant specific hardware definitions
#include <cyg/hal/hal_mmu.h>            // MMU definitions
#include <cyg/hal/fsl_board.h>          // Platform specific hardware definitions

#if defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1 _platform_setup1
#define CYGHWR_HAL_ARM_HAS_MMU

#ifdef CYG_HAL_STARTUP_ROMRAM
#define CYGSEM_HAL_ROM_RESET_USES_JUMP
#endif

#define SDRAM_FULL_PAGE_BIT     0x100
#define SDRAM_FULL_PAGE_MODE    0x37
#define SDRAM_BURST_MODE        0x33

#define CYGHWR_HAL_ROM_VADDR    0x0

#if 0
#define UNALIGNED_ACCESS_ENABLE
#define SET_T_BIT_DISABLE
#define BRANCH_PREDICTION_ENABLE
#endif

//#define TURN_OFF_IMPRECISE_ABORT

// This macro represents the initial startup code for the platform
    .macro  _platform_setup1
FSL_BOARD_SETUP_START:
/*
 *       ARM1136 init
 *       - invalidate I/D cache/TLB and drain write buffer;
 *       - invalidate L2 cache
 *       - unaligned access
 *       - branch predictions
 */
    mrc 15, 0, r0, c1, c0, 0    /* r0 = system control reg */
    bic r0, r0, #(1 << 12)        /* disable ICache */
    bic r0, r0, #(1 << 2)        /* disable DCache */
    bic r0, r0, #(1 << 0)        /* disable MMU */
    mcr 15, 0, r0, c1, c0, 0    /* update system control reg */
#ifdef TURN_OFF_IMPRECISE_ABORT
    mrs r0, cpsr
    bic r0, r0, #0x100
    msr cpsr, r0
#endif

    mov r0, #0
    mcr 15, 0, r0, c7, c7, 0        /* invalidate I cache and D cache */
    mcr 15, 0, r0, c8, c7, 0        /* invalidate TLBs */
    mcr 15, 0, r0, c7, c10, 4       /* Drain the write buffer */

    /* Also setup the Peripheral Port Remap register inside the core */
    ldr r0, ARM_PPMRR        /* start from AIPS 2GB region */
    mcr p15, 0, r0, c15, c2, 4

    /*** L2 Cache setup/invalidation/disable ***/
    /* Disable L2 cache first */
    mov r0, #L2CC_BASE_ADDR
    ldr r2, [r0, #L2_CACHE_CTL_REG]
    bic r2, r2, #0x1
    str r2, [r0, #L2_CACHE_CTL_REG]
    /*
     * Configure L2 Cache:
     * - 128k size(16k way)
     * - 8-way associativity
     * - 0 ws TAG/VALID/DIRTY
     * - 4 ws DATA R/W
     */
    ldr r1, [r0, #L2_CACHE_AUX_CTL_REG]
    and r1, r1, #0xFE000000
    ldr r2, L2CACHE_PARAM
    orr r1, r1, r2
    str r1, [r0, #L2_CACHE_AUX_CTL_REG]

    /* Invalidate L2 */
    mov r1, #0x000000FF
    str r1, [r0, #L2_CACHE_INV_WAY_REG]
L2_loop:
    /* Poll Invalidate By Way register */
    ldr r2, [r0, #L2_CACHE_INV_WAY_REG]
    cmp r2, #0
    bne L2_loop
    /*** End of L2 operations ***/

    mov r0, #SDRAM_NON_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1] // for checking boot source from nand, nor or sdram
/*
 * End of ARM1136 init
 */
init_spba_start:
    init_spba
init_aips_start:
    init_aips
init_max_start:
    init_max
init_m3if_start:
    init_m3if

init_cs0_async_start:
    init_cs0_async
init_clock_start:
    init_clock
init_cs4_start:
    init_cs4

    /* If SDRAM has been setup, bypass clock/WEIM setup */
    cmp pc, #SDRAM_BASE_ADDR
    blo init_sdram_start
    cmp pc, #(SDRAM_BASE_ADDR + SDRAM_SIZE)
    blo HWInitialise_skip_SDRAM_setup

    mov r0, #NOR_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]

init_sdram_start:
    init_drive_strength
    /* Always burst mode for SDRAM*/
    ldr r10, =0
    ldr r4, =SDRAM_BURST_MODE
    /* Assuming DDR memory first */

setup_sdram_start:
#ifndef CYGHWR_HAL_ARM_MEM1
    setup_sdram ddr X32 DDR 0
    beq HWInitialise_skip_SDRAM_setup

    setup_sdram ddr X16 DDR 0
    beq HWInitialise_skip_SDRAM_setup

    setup_sdram sdr X32 SDR 0
    beq HWInitialise_skip_SDRAM_setup

    setup_sdram sdr X16 SDR 0
    beq HWInitialise_skip_SDRAM_setup
#else
    setup_sdram ddr X16 DDR 0
    beq HWInitialise_skip_SDRAM_setup
#endif

    /* Reach hear means memory setup problem. Try to
     * increase the HCLK divider */
    ldr r0, CRM_AP_BASE_ADDR_W
    ldr r1, [r0, #CRM_AP_ACDR]
    and r2, r1, #0xF0
    cmp r2, #0xF0
    beq loop_forever
    add r1, r1, #0x12
    str r1, [r0, #CRM_AP_ACDR]
    b init_sdram_start

loop_forever:
    b loop_forever  /* shouldn't get here */

HWInitialise_skip_SDRAM_setup:

    mov r0, #NFC_BASE
    add r2, r0, #0x800      // 2K window
    cmp pc, r0
    blo Normal_Boot_Continue
    cmp pc, r2
    bhi Normal_Boot_Continue
NAND_Boot_Start:
    /* Copy image from flash to SDRAM first */
    ldr r1, MXC_REDBOOT_ROM_START

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    blo 1b
    /* Jump to SDRAM */
    ldr r1, CONST_0xFFFF
    and r0, pc, r1     /* offset of pc */
    ldr r1, MXC_REDBOOT_ROM_START
    add r1, r1, #0x10
    add pc, r0, r1
    nop
    nop
    nop
    nop
NAND_Copy_Main:
    mov r0, #NAND_FLASH_BOOT
    ldr r1, AVIC_VECTOR0_ADDR_W
    str r0, [r1]
    mov r0, #MXCFIS_NAND
    ldr r1, AVIC_VECTOR1_ADDR_W
    str r0, [r1]

    mov r0, #NFC_BASE;   //r0: nfc base. Reloaded after each page copying
    mov r1, #0x800       //r1: starting flash addr to be copied. Updated constantly
    add r2, r0, #0x200   //r2: end of 1st RAM buf. Doesn't change
    add r12, r0, #0xE00  //r12: NFC register base. Doesn't change
    ldr r14, MXC_REDBOOT_ROM_START
    add r13, r14, #REDBOOT_IMAGE_SIZE //r13: end of SDRAM address for copying. Doesn't change
    add r14, r14, r1     //r14: starting SDRAM address for copying. Updated constantly

    //unlock internal buffer
    mov r3, #0x2
    strh r3, [r12, #0xA]

Nfc_Read_Page:
//  writew(FLASH_Read_Mode1, NAND_FLASH_CMD_REG);
    mov r3, #0x0;
    strh r3, [r12, #NAND_FLASH_CMD_REG_OFF]
    mov r3, #NAND_FLASH_CONFIG2_FCMD_EN;
    strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
    do_wait_op_done

//    start_nfc_addr_ops(ADDRESS_INPUT_READ_PAGE, addr, nflash_dev_info->base_mask);
    mov r3, r1
    do_addr_input       //1st addr cycle
    mov r3, r1, lsr #9
    do_addr_input       //2nd addr cycle
    mov r3, r1, lsr #17
    do_addr_input       //3rd addr cycle
#ifdef CYGHWR_HAL_ARM_MEM1
    mov r3, r1, lsr #25
    do_addr_input       //4th addr cycle
#endif

//    NFC_DATA_OUTPUT(buf, FDO_PAGE_SPARE_VAL);
//        writew(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN,
//               NAND_FLASH_CONFIG1_REG);
    mov r3, #(NAND_FLASH_CONFIG1_INT_MSK | NAND_FLASH_CONFIG1_ECC_EN)
    strh r3, [r12, #NAND_FLASH_CONFIG1_REG_OFF]

//        writew(buf_no, RAM_BUFFER_ADDRESS_REG);
    mov r3, #0
    strh r3, [r12, #RAM_BUFFER_ADDRESS_REG_OFF]
//        writew(FDO_PAGE_SPARE_VAL & 0xFF, NAND_FLASH_CONFIG2_REG);
    mov r3, #FDO_PAGE_SPARE_VAL
    strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
//        wait_op_done();
    do_wait_op_done

    // check for bad block
    mov r3, r1, lsl #(32-5-9)
    cmp r3, #(512 << (32-5-9))
    bhi Copy_Good_Blk
    add r4, r0, #0x800  //r3 -> spare area buf 0
    ldrh r4, [r4, #0x4]
    and r4, r4, #0xFF00
    cmp r4, #0xFF00
    beq Copy_Good_Blk
    // really sucks. Bad block!!!!
    cmp r3, #0x0
    beq Skip_bad_block
    // even suckier since we already read the first page!
    sub r14, r14, #512  //rewind 1 page for the sdram pointer
    sub r1, r1, #512    //rewind 1 page for the flash pointer
Skip_bad_block:
    add r1, r1, #(32*512)
    b Nfc_Read_Page
Copy_Good_Blk:
    //copying page
1:  ldmia r0!, {r3-r10}
    stmia r14!, {r3-r10}
    cmp r0, r2
    blo 1b
    cmp r14, r13
    bge NAND_Copy_Main_done
    add r1, r1, #0x200
    mov r0, #NFC_BASE
    b Nfc_Read_Page

NAND_Copy_Main_done:

Normal_Boot_Continue:
#ifdef CYG_HAL_STARTUP_ROMRAM     /* enable running from RAM */
    /* Copy image from flash to SDRAM first */
    ldr r0, =0xFFFFF000
    and r0, r0, pc
    ldr r1, MXC_REDBOOT_ROM_START
    cmp r0, r1
    beq HWInitialise_skip_SDRAM_copy

    add r2, r0, #REDBOOT_IMAGE_SIZE

1:  ldmia r0!, {r3-r10}
    stmia r1!, {r3-r10}
    cmp r0, r2
    ble 1b
    /* Jump to SDRAM */
    ldr r1, =0xFFFF
    and r0, pc, r1         /* offset of pc */
    ldr r1, =(SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000 + 0x8)
    add pc, r0, r1
    nop
    nop
    nop
    nop
#endif /* CYG_HAL_STARTUP_ROMRAM */
init_cs0_sync_start:
    init_cs0_sync

HWInitialise_skip_SDRAM_copy:

/*
 * Note:
 *     IOMUX/PBC setup is done in C function plf_hardware_init() for simplicity
 */

STACK_Setup:
    // Set up a stack [for calling C code]
    ldr r1, =__startup_stack
    ldr r2, =RAM_BANK0_BASE
    orr sp, r1, r2

    // Create MMU tables
    bl hal_mmu_init

    // Enable MMU
    ldr r2, =10f
    mrc MMU_CP, 0, r1, MMU_Control, c0      // get c1 value to r1 first
    orr r1, r1, #7                          // enable MMU bit
    mcr MMU_CP, 0, r1, MMU_Control, c0
    mov pc,r2    /* Change address spaces */
    nop
    nop
    nop
10:

    // Save shadow copy of BCR, also hardware configuration
    ldr r1, =_board_BCR
    str r2, [r1]
    ldr r1, =_board_CFG
    str r9, [r1]        // Saved far above...

    .endm               // _platform_setup1

#else // defined(CYG_HAL_STARTUP_ROM) || defined(CYG_HAL_STARTUP_ROMRAM)
#define PLATFORM_SETUP1
#endif

    /* Allow all 3 masters to have access to these shared peripherals */
    .macro  init_spba
    /* Do nothing */
    .endm  /* init_spba */

    /* AIPS setup - Only setup MPROTx registers. The PACR default values are good.*/
    .macro init_aips
        /*
         * Set all MPROTx to be non-bufferable, trusted for R/W,
         * not forced to user-mode.
         */
        ldr r0, AIPS1_CTRL_BASE_ADDR_W
        ldr r1, AIPS1_PARAM_W
        str r1, [r0, #0x00]
        str r1, [r0, #0x04]
        ldr r0, AIPS2_CTRL_BASE_ADDR_W
        str r1, [r0, #0x00]
        str r1, [r0, #0x04]

        /*
         * Clear the on and off peripheral modules Supervisor Protect bit
         * for SDMA to access them. Did not change the AIPS control registers
         * (offset 0x20) access type
         */
        ldr r0, AIPS1_CTRL_BASE_ADDR_W
        ldr r1, =0x0
        str r1, [r0, #0x40]
        str r1, [r0, #0x44]
        str r1, [r0, #0x48]
        str r1, [r0, #0x4C]
        ldr r1, [r0, #0x50]
        and r1, r1, #0x00FFFFFF
        str r1, [r0, #0x50]

        ldr r0, AIPS2_CTRL_BASE_ADDR_W
        ldr r1, =0x0
        str r1, [r0, #0x40]
        str r1, [r0, #0x44]
        str r1, [r0, #0x48]
        str r1, [r0, #0x4C]
        ldr r1, [r0, #0x50]
        and r1, r1, #0x00FFFFFF
        str r1, [r0, #0x50]
    .endm /* init_aips */

    /* MAX (Multi-Layer AHB Crossbar Switch) setup */
    .macro init_max
        ldr r0, MAX_BASE_ADDR_W
        /* MPR - priority is M4 > M2 > M3 > M5 > M0 > M1 */
        ldr r1, MAX_PARAM1
        str r1, [r0, #0x000]        /* for S0 */
        str r1, [r0, #0x100]        /* for S1 */
        str r1, [r0, #0x200]        /* for S2 */
        str r1, [r0, #0x300]        /* for S3 */
        str r1, [r0, #0x400]        /* for S4 */
        /* SGPCR - always park on last master */
        ldr r1, =0x10
        str r1, [r0, #0x010]        /* for S0 */
        str r1, [r0, #0x110]        /* for S1 */
        str r1, [r0, #0x210]        /* for S2 */
        str r1, [r0, #0x310]        /* for S3 */
        str r1, [r0, #0x410]        /* for S4 */
        /* MGPCR - restore default values */
        ldr r1, =0x0
        str r1, [r0, #0x800]        /* for M0 */
        str r1, [r0, #0x900]        /* for M1 */
        str r1, [r0, #0xA00]        /* for M2 */
        str r1, [r0, #0xB00]        /* for M3 */
        str r1, [r0, #0xC00]        /* for M4 */
        str r1, [r0, #0xD00]        /* for M5 */
    .endm /* init_max */

    /* Clock setup */
    .macro init_clock
        /* RVAL/WVAL for L2 cache memory */
        ldr r0, RVAL_WVAL_W
        ldr r1, CLKCTL_BASE_ADDR_W
        str r0, [r1, #0x10]

        ldr r0, ADPLL_BASE_ADDR_W
        ldr r2, CRM_AP_BASE_ADDR_W
        ldr r3, CRM_COM_BASE_ADDR_W

        /*
         * If PLL has already been configured via RVD script, skip
         * ADPLL clock setup.  Running ADPLL setup again can
         * cause strange behaviors and loss of communication with
         * RVD.
         */
        ldr r1, [r2, #CRM_AP_ACSR]
        ands r1, r1, #(1 << 0)
        bne udpll_setup

        // Address TLSbo60450 to default analog VREG setting to 1.6V
        ldr r1, [r3, #CRM_COM_CSCR]
        orr r1, r1, #(0x7 << 11)
        str r1, [r3, #CRM_COM_CSCR]

        /*
         * Configure ADPLL operation register (DP_OP, DP_HFS_OP):
         */
        ldr r1, ADPLL_OP_W
        str r1, [r0, #PLL_DP_OP]
        str r1, [r0, #PLL_DP_HFS_OP]

        /*
         * Configure ADPLL operation register (DP_MFN, DP_HFS_MFN):
         */
        ldr r1, ADPLL_MFN_W
        str r1, [r0, #PLL_DP_MFN]
        str r1, [r0, #PLL_DP_HFS_MFN]

        /*
         * Configure ADPLL operation register (DP_MFD, DP_HFS_MFN):
         */
        ldr r1, ADPLL_MFD_W
        str r1, [r0, #PLL_DP_MFD]
        str r1, [r0, #PLL_DP_HFS_MFD]

        /*
         * Configure CRM_AP divider register (CRM_AP_ACDR):
         */
        ldr r1, CRM_AP_DIV_W
        str r1, [r2, #CRM_AP_ACDR]

        /*
         * Configure CRM_COM system control register (CRM_COM_CSCR):
         *      PDN_CLKMON_CKIH = CLKMON on = (0 << 0)  = 0x00000000
         *      NOCK_CKIH = READ-ONLY = (0 << 1)    = 0x00000000
         *      PDN_CLKMON_DIGRF = CLKMON off = (1 << 2)= 0x00000004
         *      NOCK_DIGRF = READ-ONLY = (0 << 3)       = 0x00000000
         *      BY_AMP_CKIH = CAMP used = (0 << 4)      = 0x00000000
         *      PDN_AMP_CKIH = CAMP on = (0 << 5)       = 0x00000000
         *      REG_MOD = one regulator = (0 << 6)      = 0x00000000
         *      DIGRF_CLK_EN = DIGRF enabled = (1 << 7) = 0x00000080
         *      Reserved = (3 << 8)             = 0x00000300
         *      CNTL = 1.4 V (5 << 11)          = 0x00002800
         *      MUX_SEL = no test signals = (0 << 14)   = 0x00000000
         *      BP_PDSM_EN = no pseudo DSM = (1 << 16)  = 0x00000000
         *      CKO_SEL = AP CKO = (1 << 17)        = 0x00020000
         *      CKOH_SEL = AP CKOH = (1 << 18)      = 0x00040000
         *      NF_WIDTH = 8-bit = (0 << 19)        = 0x00000000
         *      NF_PG_SIZ = 512 bytes (0 << 20)     = 0x00000000
         *      BY_AMP_DIGRF = CAMP used = (0 << 21)    = 0x00000000
         *      PDN_AMP_DIGRF = CAMP off = (1 << 22)    = 0x00400000
         *      VREG_CTRL = VREG disabled = (1 << 23)   = 0x00800000
         *      CKIH_DBLR_OFF = doubler on = (0 << 24)  = 0x00000000
         *      DIGRF_DBLR_OFF = doubler off = (1 << 25)= 0x02000000
         *      PDD0 = CRM disables ADPLL = (0 << 26)   = 0x00000000
         *      PDD1 = CRM disables BDPLL = (0 << 27)   = 0x00000000
         *      PDD2 = CRM disables UDPLL = (0 << 28)   = 0x00000000
         *      Reserved = (0 << 29)            = 0x00000000
         *      MRCG_PWR_GT = not gated = (0 << 30)     = 0x00000000
         *      BP_PAT_REF_EN = pat_ref on = (1 << 31)  = 0x80000000
         *                          ------------
         *                        0x82C62B84
         */
        ldr r1, [r3, #CRM_COM_CSCR]
        bic r1, r1, #(1 << 24)          // enable the CKIH doubler clock
        orr r1, r1, #(3 << 17)          // AP CKO/CKOH selected
        str r1, [r3, #CRM_COM_CSCR]

        /*
         * Wait for CKIH doubler to lock
         */
    ckih_x2_lock:
        ldr r1, [r3, #CRM_COM_CCCR]
        ands r1, r1, #(1 << 14)
        beq ckih_x2_lock

        /*
         * Configure AP clock observation (CRM_AP_ACR):
         *      CKOS = AP_PAT_REF_CLK = (2 << 4)    = 0x00000020
         *      CKOD = CKO enabled = (0 << 7)       = 0x00000000
         *      CKOHDIV = /4 = (2 << 8)         = 0x00000200
         *      CKOHS = AP_CLK = (1 << 12)          = 0x00001000
         *      CKOHD = CKOH enabled = (0 << 15)    = 0x00000000
         *                          ------------
         *                        0x00001220
         */
        ldr r1, CRM_AP_ACR_VAL
        str r1, [r2, #CRM_AP_ACR]

        /*
         * Configure CRM_AP DFS control register (CRM_AP_ADCR):
         *      DIV_BYP = DFS divider used = (0 << 1)   = 0x00000000
         *      VSTAT = READ-ONLY = (0 << 3)        = 0x00000000
         *      TSTAT = READ-ONLY = (0 << 4)        = 0x00000000
         *      DFS_DIV_EN = non-integer DFS = (0 << 5) = 0x00000000
         *      CLK_ON = PAT_REF during DFS = (1 << 6)  = 0x00000040
         *      ALT_PLL = no DVS ALT PLL = (0 << 7)     = 0x00000000
         *      LFDF = /2 = (1 << 8)            = 0x00000100
         *      AP_DELAY = 976 us = (32 << 16)      = 0x00200000
         *                          ------------
         *                        0x00200140
         */
        ldr r1, CRM_AP_ADCR_VAL
        str r1, [r2, #CRM_AP_ADCR]

        /*
         * Configure CRM_AP source clock selection register (CRM_AP_ASCSR):
         *      AP_ISEL = CKIH = (0 << 0)           = 0x00000000
         *      APSEL = ADPLL = (0 << 3)        = 0x00000000
         *      SSISEL = UDPLL  = (2 << 5)          = 0x00000040
         *      SS2SEL = UDPLL  = (2 << 7)          = 0x00000100
         *      FIRISEL = UDPLL = (2 << 9)          = 0x00000400
         *      CSSEL = UDPLL = (2 << 11)           = 0x00001000
         *      USBSEL = UDPLL = (2 << 13)          = 0x00004000
         *      AP_PAT_REF_DIV = /1 = (0 << 15)     = 0x00000000
         *      CRS = uncorrected PAT_REF = (0 << 16)   = 0x00000000
         *                          ------------
         *                        0x00005540
         */
        ldr r1, CRM_AP_ASCSR_VAL
        str r1, [r2, #CRM_AP_ASCSR]

        /*
         * Configure ADPLL control register (DP_CTL):
         *      BRMO = second order = (1 << 1)      = 0x00000002
         *      PLM = freq only lock = (0 << 2)     = 0x00000000
         *      RCP = pos edge = (0 << 3)           = 0x00000000
         *      RST = no restart = (0 << 4)         = 0x00000000
         *      UPEN = PLL enable = (0 << 5)        = 0x00000020
         *      PRE = no reset = (0 << 6)           = 0x00000000
         *      HFSM = normal mode = (0 << 7)       = 0x00000000
         *      REF_CLK_SEL = ckih_camp_x2 = (2 << 8)   = 0x00000200
         *      REF_CLK_DIV = /1 = (0 << 10)        = 0x00000000
         *                          ------------
         *                        0x00000222
         */

        ldr r1, DPLL_DP_CTL_VAL
        str r1, [r0, #PLL_DP_CTL]

        /*
         * Wait for ADPLL to lock
         */
    adpll_lock:
        ldr r1, [r0, #PLL_DP_CTL]
        ands r1, r1, #(1 << 0)
        beq adpll_lock

        /*
         * Configure AP clock selection register (CRM_AP_ACSR):
         *      ACS = PLL_CLK = (1 << 0)        = 0x00000001
         *      WPS = PLL_CLK in wait mode = (0 << 1)   = 0x00000000
         *      PDS = PLL on in stop mode  = (0 << 2)   = 0x00000000
         *      SMD = use sync muxes = (1 << 3)     = 0x00000000
         *      DI = ignore dsm_int = (0 << 7)      = 0x00000000
         *      ADS = non-doubler path = (0 << 8)       = 0x00000000
         *                          ------------
         *                        0x00000001
         */
        ldr r1, =0x00000001
        str r1, [r2, #CRM_AP_ACSR]


    udpll_setup:
        /*
         * Configure UDPLL registers
         */
        ldr r0, UDPLL_BASE_ADDR_W

        /*
         * Configure UDPLL operation register (DP_OP, DP_HFS_OP):
         */
        ldr r1, UDPLL_OP_W
        str r1, [r0, #PLL_DP_OP]
        str r1, [r0, #PLL_DP_HFS_OP]

        /*
         * Configure UDPLL operation register (DP_MFN, DP_HFS_MFN):
         */
        ldr r1, UDPLL_MFN_W
        str r1, [r0, #PLL_DP_MFN]
        str r1, [r0, #PLL_DP_HFS_MFN]

        /*
         * Configure UDPLL operation register (DP_MFD, DP_HFS_MFN):
         */
        ldr r1, UDPLL_MFD_W
        str r1, [r0, #PLL_DP_MFD]
        str r1, [r0, #PLL_DP_HFS_MFD]

        /*
         * Configure UDPLL control register (DP_CTL):
         *      BRMO = second order = (1 << 1)      = 0x00000002
         *      PLM = freq only lock = (0 << 2)     = 0x00000000
         *      RCP = pos edge = (0 << 3)           = 0x00000000
         *      RST = no restart = (0 << 4)         = 0x00000000
         *      UPEN = PLL enable = (0 << 5)        = 0x00000020
         *      PRE = no reset = (0 << 6)           = 0x00000000
         *      HFSM = normal mode = (0 << 7)       = 0x00000000
         *      REF_CLK_SEL = ckih_camp = (0 << 8)      = 0x00000000
         *      REF_CLK_DIV = /1 = (0 << 10)        = 0x00000000
         *                          ------------
         *                        0x00000022
         */
        ldr r1, =0x00000022
        str r1, [r0, #PLL_DP_CTL]

        /*
         * Wait for UDPLL to lock
         */
    udpll_lock:
        ldr r1, [r0, #PLL_DP_CTL]
        ands r1, r1, #(1 << 0)
        beq udpll_lock

        /*
         * Configure AP accessory clock register (CRM_AP_ACDER1):
         *      SSI1DIV = /12.5 = (0x19 << 0)       = 0x00000019
         *      SSI1EN = off = (0 << 6)         = 0x00000000
         *      SSI2DIV = /12.5 = (0x19 << 8)       = 0x00001900
         *      SSI2EN = off = (0 << 14)        = 0x00000000
         *      FIRIEN = off = (1 << 22)        = 0x00000000
         *      CSEN = off = (0 << 20)          = 0x00000000
         *                          ------------
         *                        0x00001919
         */
        ldr r1, CRM_AP_ACDER1_W
        str r1, [r2, #CRM_AP_ACDER1]

        /*
         * Configure AP accessory clock register (CRM_AP_ACDER2):
         *      BAUD_DIV = /1 = (8 << 0)        = 0x00000008
         *      BAUD_ISEL = CKIH_X2 = (1 << 5)      = 0x00000020
         *      USBEN = USB_CLK enabled = (1 << 12)     = 0x00001000
         *      NFCEN = NFC_CLK enabled = (1 << 20)     = 0x00100000
         *                          ------------
         *                        0x00101028
         */
        ldr r1, CRM_AP_ACDER2_W
        str r1, [r2, #CRM_AP_ACDER2]

        // AP CKO/CKOH selected
        ldr r0, CRM_COM_BASE_ADDR_W
        ldr r1, [r0, #0x0C]
        orr r1, r1, #0x60000
        str r1, [r0, #0x0C]
        ldr r0, CRM_AP_BASE_ADDR_W
        /* Default CKOH as AP_CLK with div by 10 */
        //orr r1, r1, #0x5600 /* HCLK */
        ldr r1, [r0, #CRM_AP_ACR]
        bic r1, r1, #0xFF00
        bic r1, r1, #0x00FF
        orr r1, r1, #0x1600
        str r1, [r0, #CRM_AP_ACR]

        // SD clock input select - usb_clk. divider changed to 1
        mov r1, #0x00009100
        add r1, r1, #0x00000091
        str r1, [r0, #CRM_AP_APRB]
    .endm /* init_clock */

    /* M3IF setup */
    .macro init_m3if
        /* Configure M3IF registers */
        ldr r1, M3IF_BASE_W
        /*
        * M3IF Control Register (M3IFCTL)
        * MRRP[0] = TMAX not on priority list (0 << 0)        = 0x00000000
        * MRRP[1] = SMIF not on priority list (0 << 0)        = 0x00000000
        * MRRP[2] = MAX0 not on priority list (0 << 0)        = 0x00000000
        * MRRP[3] = MAX1 not on priority list (0 << 0)        = 0x00000000
        * MRRP[4] = SDMA not on priority list (0 << 0)        = 0x00000000
        * MRRP[5] = MPEG4 not on priority list (0 << 0)       = 0x00000000
        * MRRP[6] = IPU on priority list (1 << 6)             = 0x00000040
        * MRRP[7] = SMIF-L2CC not on priority list (0 << 0)   = 0x00000000
        *                                                       ------------
        *                                                       0x00000040
        */
        ldr r0, =0x00000040
        str r0, [r1]  /* M3IF control reg */
    .endm /* init_m3if */

    /* CS0 sync mode setup */
    .macro init_cs0_sync
#ifndef CYGHWR_HAL_ARM_MEM1
        /*
         * Sync mode (AHB Clk = 133MHz ; BCLK = 44.3MHz):
         */
        /* Flash reset command */
        ldr r0, =CS0_BASE_ADDR
        ldr r1, =0xF0F0
        strh r1, [r0]
        /* 1st command */
        ldr r2, =0xAAA
        add r2, r2, r0
        ldr r1, =0xAAAA
        strh r1, [r2]
        /* 2nd command */
        ldr r2, =0x554
        add r2, r2, r0
        ldr r1, =0x5555
        strh r1, [r2]
        /* 3rd command */
        ldr r2, =0xAAA
        add r2, r2, r0
        ldr r1, =0xD0D0
        strh r1, [r2]
        /* Write flash config register */
        ldr r1, =0x56CA
        strh r1, [r2]
        /* Flash reset command */
        ldr r1, =0xF0F0
        strh r1, [r0]

        ldr r0, =WEIM_BASE_ADDR
        ldr r1, =0x23524E80
        str r1, [r0, #CSCRU]
        ldr r1, =0x10000D03
        str r1, [r0, #CSCRL]
        ldr r1, =0x00720900
        str r1, [r0, #CSCRA]
#else  // CYGHWR_HAL_ARM_MEM1
        ldr r0, =WEIM_BASE_ADDR
        ldr r1, =0x23524E80
        str r1, [r0, #CSCRU]
        ldr r1, =0xC0000E03
        str r1, [r0, #CSCRL]
        ldr r1, =0x0072BD00
        str r1, [r0, #CSCRA]
        /* Flash reset command */
        ldr r0, =CS0_BASE_ADDR
        ldr r1, =0xF0F0F0F0
        str r1, [r0]
        /* Write 1st command with address multiplied by 2 */
        ldr r0, =0xA0001554
        ldr r1, = 0xAAAAAAAA
        str r1, [r0]
        /* Write 2nd command with address multiplied by 2 */
        ldr r0, =0xA0000AA8
        ldr r1, = 0x55555555
        str r1, [r0]
        /* Write 0xB80020003rd command with address multiplied by 2 */
        ldr r0, =0xA0001554
        ldr r1, = 0xD0D0D0D0
        str r1, [r0]
        /* //  Write Config register  (4 waits, wrap, Burst=8). Burst Enabled
          "FOR WHEN FCE BROKE!!!" (Sync. Mode) AHB/BCLK=133/44.3MHz   (Optimal
          configuration) */
        ldr r0, =0xA0001554
        ldr r1, = 0x56CA56CA
        str r1, [r0]

        /* Flash reset command */
        ldr r0, =CS0_BASE_ADDR
        ldr r1, =0xF0F0F0F0
        str r1, [r0]
#endif
    .endm /* init_cs0_sync */

    /* CS0 async mode setup */
    .macro init_cs0_async
init_cs0_async_start:
        /* Async flash mode */
#ifndef CYGHWR_HAL_ARM_MEM1
        ldr r0, WEIM_CTRL_CS0_W
        ldr r1, CSCRU_VAL
        str r1, [r0, #CSCRU]
        ldr r1, CSCRL_VAL
        str r1, [r0, #CSCRL]
        ldr r1, CSCRA_VAL
        str r1, [r0, #CSCRA]
#else
        ldr r0, WEIM_CTRL_CS0_W
        ldr r1, CSCRU_VAL
        str r1, [r0, #CSCRU]
        ldr r1, CSCRL_VAL
        str r1, [r0, #CSCRL]
        ldr r1, CSCRA_VAL
        str r1, [r0, #CSCRA]
#endif /* CYGHWR_HAL_ARM_MEM1 */
    .endm /* init_cs0_async */

    /* CPLD on CS4 setup */
    .macro init_cs4
        ldr r0, WEIM_CTRL_CS4_W
        ldr r1, CS4_CSCRU_VAL1
        str r1, [r0, #CSCRU]
        ldr r1, CS4_CSCRL_VAL1
        str r1, [r0, #CSCRL]
        ldr r1, CS4_CSCRA_VAL1
        str r1, [r0, #CSCRA]

        ldr r0, CS4_BASE_ADDR_W
        ldrh r1, [r0, #0x0]
        and r0, r1, #0xFF00
        cmp r0, #0x2000
        bge done_cs4_setup

        ldr r0, WEIM_CTRL_CS4_W
        ldr r1, CS4_CSCRU_VAL
        str r1, [r0, #CSCRU]
        ldr r1, CS4_CSCRL_VAL
        str r1, [r0, #CSCRL]
        ldr r1, CS4_CSCRA_VAL
        str r1, [r0, #CSCRA]

done_cs4_setup:

    .endm /* init_cs4 */

    .macro setup_sdram, name, bus_width, mode, full_page
        /* It sets the "Z" flag in the CPSR at the end of the macro */
        ldr r0, ESDCTL_BASE_W
        mov r2, #SDRAM_BASE_ADDR
        ldr r1, SDRAM_0x0075E73A
        str r1, [r0, #0x4]
        mov r1, #0x2        // reset
        str r1, [r0, #0x10]
        ldr r1, SDRAM_PARAM1_\mode
        str r1, [r0, #0x10]

        // Hold for more than 200ns
        mov r1, #0x10000
    1:
        subs r1, r1, #0x1
        bne 1b

        ldr r1, SDRAM_0x92100000
        str r1, [r0]
        mov r1, #0x0
        ldr r12, SDRAM_PARAM2_\mode
        str r1, [r12]
        ldr r1, SDRAM_0xA2100000
        str r1, [r0]
        mov r1, #0x0
        str r1, [r2]
        ldr r1, SDRAM_0xB2100000
        str r1, [r0]

        mov r1, #0x0
        .if \full_page
        strb r1, [r2, #SDRAM_FULL_PAGE_MODE]
        .else
        strb r1, [r2, #SDRAM_BURST_MODE]
        .endif
        mov r1, #0xFF
        mov r12, #0x81000000
        strb r1, [r12]
        ldr r3, SDRAM_0x82116080
        ldr r4, SDRAM_PARAM3_\mode
        add r3, r3, r4
        ldr r4, SDRAM_PARAM4_\bus_width
        add r3, r3, r4
        .if \full_page
        add r3, r3, #0x100   /* Force to full page mode */
        .endif

        str r3, [r0]
        mov r1, #0x0
        str r1, [r2]
        /* Below only for DDR */
        ldr r1, [r0, #0x10]
        ands r1, r1, #0x4
        movne r1, #0x0000000C
        strne r1, [r0, #0x10]
        /* Testing if it is truly DDR */
        ldr r1, SDRAM_COMPARE_CONST1
        mov r0, #SDRAM_BASE_ADDR
        str r1, [r0]
        ldr r2, SDRAM_COMPARE_CONST2
        str r2, [r0, #0x4]
        ldr r2, [r0]
        cmp r1, r2
    .endm // setup_sdram

    .macro do_wait_op_done
    1:
        ldrh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        ands r3, r3, #NAND_FLASH_CONFIG2_INT_DONE
        beq 1b
        mov r3, #0x0
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
    .endm   // do_wait_op_done

    .macro do_addr_input
        and r3, r3, #0xFF
        strh r3, [r12, #NAND_FLASH_ADD_REG_OFF]
        mov r3, #NAND_FLASH_CONFIG2_FADD_EN
        strh r3, [r12, #NAND_FLASH_CONFIG2_REG_OFF]
        do_wait_op_done
    .endm   // do_addr_input

    .macro  init_drive_strength
        ldr r0, IOMUX_COM_BASE_ADDR_W
        add r0, r0, #0x200
        // default is high ds
        mov r1, #0x0092
        mov r2, #0x0082
        mov r3, #0x0002
        mov r4, #CS4_BASE_ADDR
#ifdef DS_SETUP_USE_DEBUG_SWITCH
        ldrh r5, [r4, #0x2]
        and r5, r5, #0x3
        cmp r5, #0x0
        //  skip ds setup
        beq drive_strength_ret
        cmp r5, #0x2
#else
        mov r5, #0x0
        cmp r5, #0x0
#endif
        // max ds
        addeq r1, r1, #0x2
        addeq r2, r2, #0x2
        addeq r3, r3, #0x2

    setup_drive_strength:
        strh r1, [r0, #0x00]
        strh r3, [r0, #0x02]
        strh r3, [r0, #0x04]
        strh r1, [r0, #0x06]
        strh r3, [r0, #0x08]
        strh r3, [r0, #0x0A]
        strh r3, [r0, #0x0C]
        strh r3, [r0, #0x0E]
        strh r2, [r0, #0x10]
    drive_strength_ret:
    .endm

#define PLATFORM_VECTORS         _platform_vectors
    .macro  _platform_vectors
        .globl  _board_BCR, _board_CFG
_board_BCR:   .long   0       // Board Control register shadow
_board_CFG:   .long   0       // Board Configuration (read at RESET)
    .endm

ARM_PPMRR:              .word   0x40000015
L2CACHE_PARAM:          .word   0x00030024
AIPS1_CTRL_BASE_ADDR_W: .word   AIPS1_CTRL_BASE_ADDR
AIPS2_CTRL_BASE_ADDR_W: .word   AIPS2_CTRL_BASE_ADDR
AIPS1_PARAM_W:          .word   0x77777777
MAX_BASE_ADDR_W:        .word   MAX_BASE_ADDR
MAX_PARAM1:             .word   0x00302154
RVAL_WVAL_W:            .word   0x515
CLKCTL_BASE_ADDR_W:     .word   CLKCTL_BASE_ADDR
ADPLL_BASE_ADDR_W:      .word   ADPLL_BASE_ADDR
CRM_AP_BASE_ADDR_W:     .word   CRM_AP_BASE_ADDR
CRM_COM_BASE_ADDR_W:    .word   CRM_COM_BASE_ADDR
IOMUX_COM_BASE_ADDR_W:  .word   IOMUX_COM_BASE_ADDR
ADPLL_OP_W:             .word   ADPLL_OP
ADPLL_MFN_W:            .word   ADPLL_MFN
ADPLL_MFD_W:            .word   ADPLL_MFD
CRM_AP_DIV_W:           .word   CRM_AP_DIV
CRM_AP_ACR_VAL:         .word   0x00001220
CRM_AP_ADCR_VAL:        .word   0x00200140
CRM_AP_ASCSR_VAL:       .word   0x00005540
DPLL_DP_CTL_VAL:        .word   0x00000222
UDPLL_BASE_ADDR_W:      .word   UDPLL_BASE_ADDR
UDPLL_MFN_W:            .word   UDPLL_MFN
UDPLL_MFD_W:            .word   UDPLL_MFD
UDPLL_OP_W:             .word   UDPLL_OP
CRM_AP_ACDER1_W:        .word   0x00001919 | (FIRI_DIV << 16) | (CS_DIV << 24)
CRM_AP_ACDER2_W:        .word   0x00101028 | (NFC_DIV << 16) | (USB_DIV << 8)
ESDCTL_BASE_W:          .word   ESDCTL_BASE
SDRAM_PARAM1_DDR:       .word   0x4
SDRAM_PARAM1_SDR:       .word   0x0
SDRAM_PARAM2_DDR:       .word   0x80000F00
SDRAM_PARAM2_SDR:       .word   0x80000400
SDRAM_PARAM3_DDR:       .word   0x00100000
SDRAM_PARAM3_SDR:       .word   0x0
SDRAM_PARAM4_X32:       .word   0x00010000
SDRAM_PARAM4_X16:       .word   0x0
WEIM_CTRL_CS0_W:        .word   WEIM_CTRL_CS0
M3IF_BASE_W:            .word   M3IF_BASE
WEIM_CTRL_CS4_W:        .word   WEIM_CTRL_CS4
CS4_BASE_ADDR_W:        .word   CS4_BASE_ADDR
CS4_CSCRU_VAL:          .word   0x0000D843
CS4_CSCRL_VAL:          .word   0x22252521
CS4_CSCRA_VAL:          .word   0x22220A00
CS4_CSCRU_VAL1:         .word   0x0000D743
CS4_CSCRL_VAL1:         .word   0x42001521
CS4_CSCRA_VAL1:         .word   0x00430A00
SDRAM_0x0075E73A:       .word   0x0075E73A
SDRAM_0x82116080:       .word   0x82116080
SDRAM_0x92100000:       .word   0x92100000
SDRAM_0xA2100000:       .word   0xA2100000
SDRAM_0xB2100000:       .word   0xB2100000
SDRAM_COMPARE_CONST1:   .word   0x55555555
SDRAM_COMPARE_CONST2:   .word   0xAAAAAAAA
#ifndef CYGHWR_HAL_ARM_MEM1
CSCRU_VAL:              .word   0x11414C80
CSCRL_VAL:              .word   0x30000D03
CSCRA_VAL:              .word   0x00310800
#else
CSCRU_VAL:              .word   0x11414C80
CSCRL_VAL:              .word   0xC0000E03
CSCRA_VAL:              .word   0x0021BC00
#endif
MXC_REDBOOT_ROM_START:  .word   SDRAM_BASE_ADDR + SDRAM_SIZE - 0x100000
CONST_0xFFFF:           .word   0xFFFF
AVIC_VECTOR0_ADDR_W:    .word   MXCBOOT_FLAG_REG
AVIC_VECTOR1_ADDR_W:    .word   MXCFIS_FLAG_REG
/*---------------------------------------------------------------------------*/
/* end of hal_platform_setup.h                                               */
#endif /* CYGONCE_HAL_PLATFORM_SETUP_H */
