#ifndef CYGONCE_FSL_BOARD_H
#define CYGONCE_FSL_BOARD_H

//=============================================================================
//
//      Platform specific support (register layout, etc)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/hal/hal_soc.h>         // Hardware definitions

#define PBC_BASE                CS4_BASE_ADDR    /* Peripheral Bus Controller */
#define PBC_VERSION             PBC_BASE
#define PBC_BSTAT               (PBC_BASE + 0x2)
#define PBC_BCTL1_SET           (PBC_BASE + 0x4)
#define PBC_BCTL1_CLR           (PBC_BASE + 0x6)
#define PBC_BCTL2_SET           (PBC_BASE + 0x8)
#define PBC_BCTL2_CLR           (PBC_BASE + 0xA)

#define BOARD_CS_LAN_BASE       (PBC_BASE + 0x00020000 + 0x300)
#define BOARD_CS_UART_BASE      (PBC_BASE + 0x00010000)

//#define APCLK_399_133_66            /* AP_CLK: 399 MHz, AP_AHB_CLK = 133 MHz, AP_IP_CLK = 66.5 MHz */
#define APCLK_532_133_66            /* AP_CLK: 532 MHz, AP_AHB_CLK = 133 MHz, AP_IP_CLK = 66.5 MHz */
#define USB_PLL_CLK_48              /* USB_PLL_CLK: 48 MHz */

/*
 * Following definitions assume CKIH_X2 PLL reference (33.6 MHz).  PLL factor
 * values come from Example PLL settings table in IC spec.
 *
 */
#ifdef APCLK_532_133_66
#define ADPLL_PDF   (1)
#define ADPLL_MFI   (8)
#define ADPLL_MFN   (-140)
#define ADPLL_MFD   (1680 - 1)
#define ARM_DIV     8   /* 8 => /1 */
#define AHB_DIV     4
#define IP_DIV      8
#define NFC_DIV     5   /* 5 => /6 */
#endif

#ifdef APCLK_399_133_66
#define ADPLL_PDF   (1)
#define ADPLL_MFI   (6)
#define ADPLL_MFN   (-105)
#define ADPLL_MFD   (1680 - 1)
#define ARM_DIV     8   /* 8 => /1 */
#define AHB_DIV     3
#define IP_DIV      6
#define NFC_DIV     5   /* 5 => /6 */
#endif

#ifdef APCLK_266_133_66
#define ADPLL_PDF   (2)
#define ADPLL_MFI   (8)
#define ADPLL_MFN   (-140)
#define ADPLL_MFD   (1680 - 1)
#define ARM_DIV     8   /* 8 => /1 */
#define AHB_DIV     2
#define IP_DIV      4
#define NFC_DIV     5   /* 5 => /6 */
#endif

#define ADPLL_OP    ((ADPLL_MFI << 4) | (ADPLL_PDF - 1))
#define CRM_AP_DIV  ((ARM_DIV << 8) | (AHB_DIV << 4) | (IP_DIV))
/*
 * Following definitions assume CKIH PLL reference (16.8 MHz).
 */
#ifdef USB_PLL_CLK_48
#define UDPLL_PDF   (1)
#define UDPLL_MFI   (5)
#define UDPLL_MFN   (7142)
#define UDPLL_MFD   (10000 - 1)
#define UDPLL_OP    ((UDPLL_MFI << 4) | (UDPLL_PDF - 1))
#define USB_DIV     2       /* 2 => /4 */
#define FIRI_DIV    1       /* 1 => /2 */
#define CS_DIV      0x19    /* 0x19 => /12.5 */
#endif

#define REDBOOT_IMAGE_SIZE      0x40000
#define BOARD_FLASH_START       CS0_BASE_ADDR

#define SDRAM_BASE_ADDR         CSD0_BASE_ADDR
#define SDRAM_SIZE              0x04000000
#define RAM_BANK0_BASE          SDRAM_BASE_ADDR

#define LED_MAX_NUM	2
#define LED_IS_ON(n)    (readw(PBC_BCTL1_CLR) & (1 << (n+6)))
#define TURN_LED_ON(n)  writew((readw(PBC_BCTL1_CLR) | (1 << (n+6))), PBC_BCTL1_SET)
#define TURN_LED_OFF(n) writew((1<<(n+6)), PBC_BCTL1_CLR)

#define BOARD_DEBUG_LED(n) 			\
    CYG_MACRO_START				\
        if (n >= 0 && n < LED_MAX_NUM) { 	\
		if (LED_IS_ON(n)) 		\
			TURN_LED_OFF(n); 	\
		else 				\
			TURN_LED_ON(n);		\
	}					\
    CYG_MACRO_END

#endif /* CYGONCE_FSL_BOARD_H */
