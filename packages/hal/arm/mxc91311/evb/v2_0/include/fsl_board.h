#ifndef CYGONCE_FSL_BOARD_H
#define CYGONCE_FSL_BOARD_H

//=============================================================================
//
//      Platform specific support (register layout, etc)
//
//=============================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/hal/hal_soc.h>         // Hardware definitions

#define PMIC_SPI_BASE           CSPI2_BASE_ADDR

#define PBC_BASE                CS4_BASE_ADDR    /* Peripheral Bus Controller */
#define PBC_VERSION             PBC_BASE
#define PBC_BSTAT               (PBC_BASE + 0x2)
#define PBC_BCTL1_SET           (PBC_BASE + 0x4)
#define PBC_BCTL1_CLR           (PBC_BASE + 0x6)
#define PBC_BCTL2_SET           (PBC_BASE + 0x8)
#define PBC_BCTL2_CLR           (PBC_BASE + 0xA)

#define BOARD_CS_LAN_BASE       (PBC_BASE + 0x00020000 + 0x300)
#define BOARD_CS_UART_BASE      (PBC_BASE + 0x00010000)

#define REDBOOT_IMAGE_SIZE      0x40000
#define BOARD_FLASH_START       CS0_BASE_ADDR

#define SDRAM_BASE_ADDR         CSD0_BASE_ADDR
#define SDRAM_SIZE              0x04000000
#define RAM_BANK0_BASE          SDRAM_BASE_ADDR

#define EXT_UART_x16
#define LED_MAX_NUM	2
#define LED_IS_ON(n)    (readw(PBC_BCTL1_CLR) & (1 << (n+6)))
#define TURN_LED_ON(n)  writew((readw(PBC_BCTL1_CLR) | (1 << (n+6))), PBC_BCTL1_SET)
#define TURN_LED_OFF(n) writew((1<<(n+6)), PBC_BCTL1_CLR)

#define BOARD_DEBUG_LED(n) 			\
    CYG_MACRO_START				\
        if (n >= 0 && n < LED_MAX_NUM) { 	\
		if (LED_IS_ON(n)) 		\
			TURN_LED_OFF(n); 	\
		else 				\
			TURN_LED_ON(n);		\
	}					\
    CYG_MACRO_END

#endif /* CYGONCE_FSL_BOARD_H */
