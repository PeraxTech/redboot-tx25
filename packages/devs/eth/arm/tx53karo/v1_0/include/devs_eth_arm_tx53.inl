//==========================================================================
//
//      devs_eth_arm_tx53.inl
//
//      Board ethernet I/O definitions.
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/hal/hal_intr.h>			// CYGNUM_HAL_INTERRUPT_ETHR
#include <cyg/hal/hal_if.h>
#include <cyg/hal/mx53_iomux.h>

#ifdef CYGPKG_REDBOOT
#include <pkgconf/redboot.h>
#ifdef CYGSEM_REDBOOT_FLASH_CONFIG
#include <redboot.h>
#include <flash_config.h>
#endif
#endif


#ifdef __WANT_DEVS

#ifdef CYGPKG_DEVS_ETH_ARM_MXCBOARD_ETH0

#ifdef CYGPKG_DEVS_ETH_PHY

static char  mxc_fec_name[] = "mxc_fec";

#define FEC_POWER_GPIO					MX53_GPIO_NR(3, 20)
#define FEC_RESET_GPIO					MX53_GPIO_NR(7, 6)

#ifdef CYGSEM_REDBOOT_PLF_ESA_VALIDATE
//
// Verify that the given ESA is valid for this platform
//
static char oui[3] = CYGDAT_DEVS_ETH_ARM_TX53KARO_OUI;

bool
cyg_plf_redboot_esa_validate(unsigned char *val)
{
	return (val[0] == oui[0]) && (val[1] == oui[1]) && (val[2] == oui[2]);
}
#endif

extern int tx53_mac_addr_program(unsigned char mac_addr[ETHER_ADDR_LEN]);

static inline void tx53_write_reg(CYG_ADDRWORD base_addr, CYG_WORD32 offset, CYG_WORD32 val)
{
	if (net_debug) {
		diag_printf("Changing reg %08x from %08x to %08x\n",
					base_addr + offset, readl(base_addr + offset), val);
	}
	HAL_WRITE_UINT32(base_addr + offset, val);
}

static inline CYG_WORD32 tx53_read_reg(CYG_ADDRWORD base_addr, CYG_WORD32 offset)
{
	CYG_WORD32 val;

	HAL_READ_UINT32(base_addr + offset, val);
	if (net_debug) diag_printf("Read %08x from reg %08x\n", val, base_addr + offset);
	return val;
}

static inline void tx53_set_reg(CYG_ADDRWORD base_addr, CYG_WORD32 offset,
								CYG_WORD32 set_mask, CYG_WORD32 clr_mask)
{
	CYG_WORD32 val;

	HAL_READ_UINT32(base_addr + offset, val);
	if (net_debug) diag_printf("Changing reg %08x from %08x to %08x\n", base_addr + offset, val,
				   (val & ~clr_mask) | set_mask);
	val = (val & ~clr_mask) | set_mask;
	HAL_WRITE_UINT32(base_addr + offset, val);
}

static iomux_v3_cfg_t tx53_fec_gpio_pads[] = {
	/* setup FEC PHY pins for GPIO function (with SION set) */
	MX53_PAD_FEC_REF_CLK__GPIO_1_23 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_MDC__GPIO_1_31 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_MDIO__GPIO_1_22 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_RXD0__GPIO_1_27 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_RXD1__GPIO_1_26 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_RX_ER__GPIO_1_24 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_TX_EN__GPIO_1_28 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_TXD0__GPIO_1_30 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_TXD1__GPIO_1_29 | IOMUX_CONFIG_SION,
	MX53_PAD_FEC_CRS_DV__GPIO_1_25 | IOMUX_CONFIG_SION,

	/* PHY reset */
	MX53_PAD_PATA_DA_0__GPIO_7_6 | IOMUX_CONFIG_SION,
	/* PHY power */
	MX53_PAD_EIM_D20__GPIO_3_20 | IOMUX_CONFIG_SION,
};

static iomux_v3_cfg_t tx53_fec_pads[] = {
	MX53_PAD_FEC_REF_CLK__FEC_TX_CLK,
	MX53_PAD_FEC_MDC__FEC_MDC,
	MX53_PAD_FEC_MDIO__FEC_MDIO,
	MX53_PAD_FEC_RXD0__FEC_RDATA_0,
	MX53_PAD_FEC_RXD1__FEC_RDATA_1,
	MX53_PAD_FEC_RX_ER__FEC_RX_ER,
	MX53_PAD_FEC_TX_EN__FEC_TX_EN,
	MX53_PAD_FEC_TXD0__FEC_TDATA_0,
	MX53_PAD_FEC_TXD1__FEC_TDATA_1,
	MX53_PAD_FEC_CRS_DV__FEC_RX_DV,
};

static struct tx53_gpio_setup {
	unsigned gpio:9,
		dir:1,
		level:1;
} tx53_fec_gpio_data[] = {
	/*					 dir,
	 *				 gpio,	 level */
	{ FEC_RESET_GPIO,	   1, 0, }, /* PHY reset */
	{ FEC_POWER_GPIO,	   1, 1, }, /* PHY power enable */
	{ MX53_GPIO_NR(1, 23), 0, }, /* ENET_CLK */
	{ MX53_GPIO_NR(1, 31), 1, 0, }, /* MDC */
	{ MX53_GPIO_NR(1, 22), 1, 0, }, /* MDIO */
	{ MX53_GPIO_NR(1, 27), 1, 1, }, /* Mode0/RXD0 */
	{ MX53_GPIO_NR(1, 26), 1, 1, }, /* Mode1/RXD1 */
	{ MX53_GPIO_NR(1, 24), 0, }, /* RX_ER */
	{ MX53_GPIO_NR(1, 28), 1, 0, }, /* TX_EN */
	{ MX53_GPIO_NR(1, 30), 1, 0, }, /* TXD0 */
	{ MX53_GPIO_NR(1, 29), 1, 0, }, /* TXD1 */
	{ MX53_GPIO_NR(1, 25), 1, 1, }, /* Mode2/CRS_DV */
};

static inline void tx53_phy_gpio_init(void)
{
	int i;

	if (net_debug)
		diag_printf("Initializing PHY GPIOs\n");

	/* setup all pins attached to the PHY to the required level */
	for (i = 0; i < CYG_NELEM(tx53_fec_gpio_data); i++) {
		struct tx53_gpio_setup *gs = &tx53_fec_gpio_data[i];

		if (gs->dir)
			gpio_direction_output(gs->gpio, gs->level);
		else
			gpio_direction_input(gs->gpio);
	}

	/* configure all FEC pads as GPIO */
	mx53_iomux_setup_pads(tx53_fec_gpio_pads, CYG_NELEM(tx53_fec_gpio_pads));

	for (i = 0; i < CYG_NELEM(tx53_fec_gpio_data); i++) {
		struct tx53_gpio_setup *gs = &tx53_fec_gpio_data[i];

		if (gs->dir) {
			if (gs->level ^ gpio_get_value(gs->gpio)) {
				diag_printf("%s: GPIO%d_%d[%d] is not %s\n", __FUNCTION__,
							gs->gpio / GPIO_NUM_PIN, gs->gpio % GPIO_NUM_PIN,
							i, gs->level ? "HIGH" : "LOW");
			}
		}
	}
	if (net_debug)
		diag_printf("PHY GPIO init done\n");
}

static bool mxc_fec_init(struct cyg_netdevtab_entry *tab);
static bool tx53_fec_init(struct cyg_netdevtab_entry *tab)
{
	cyg_bool esa_set;
	int ok;

	/* Check, whether MAC address is enabled */
	ok = CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
									 "fec_esa", &esa_set, CONFIG_BOOL);
	if (!(ok && esa_set)) {
		diag_printf("FEC disabled; set fec_esa=true to enable networking\n");
		return false;
	}
	return mxc_fec_init(tab);
}

static void tx53_fec_phy_init(void)
{
	const int phy_reset_delay = 25000;	/* wait for 25ms for LAN8710 to power up */

	/*
	 * make sure the ETH PHY strap pins are pulled to the right voltage
	 * before deasserting the PHY reset GPIO
	 */
	/* assert FEC PHY Reset (GPIO2_14) and switch PHY power on (GPIO1_3) */

	tx53_phy_gpio_init();

	if (net_debug)
		diag_printf("Switching PHY POWER on\n");
	gpio_set_value(FEC_RESET_GPIO, 0);
	gpio_set_value(FEC_POWER_GPIO, 1);

	if (!gpio_get_value(FEC_POWER_GPIO)) {
		diag_printf("**Failed to switch PHY power on\n");
	}

	if (gpio_get_value(FEC_RESET_GPIO)) {
		diag_printf("**Failed to assert PHY reset\n");
	}

	/* wait for 100us according to LAN8710 spec. before ... */
	HAL_DELAY_US(phy_reset_delay);
	/* ... deasserting FEC PHY reset */
	if (net_debug) diag_printf("Releasing PHY RESET\n");
	gpio_set_value(FEC_RESET_GPIO, 1);
	if (!gpio_get_value(FEC_RESET_GPIO)) {
		diag_printf("**Failed to release PHY reset\n");
	}

	/* configure all FEC pins to their required functions */
	mx53_iomux_setup_pads(tx53_fec_pads, CYG_NELEM(tx53_fec_pads));
}

ETH_PHY_REG_LEVEL_ACCESS_FUNS(eth0_phy,
							  tx53_fec_phy_init,
							  mxc_fec_phy_reset,
							  mxc_fec_phy_write,
							  mxc_fec_phy_read);

cyg_bool _tx53_provide_fec_esa(unsigned char *addr)
{
	cyg_bool enabled;
	int ok;

	ok = CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
					 "fec_esa", &enabled, CONFIG_BOOL);
	if (ok && enabled) {
#ifdef CYGSEM_REDBOOT_PLF_ESA_VALIDATE
		cyg_uint8 addr2[ETHER_ADDR_LEN];

		addr[0] = readl(SOC_FEC_MAC_BASE + 0x00);
		addr[1] = readl(SOC_FEC_MAC_BASE + 0x04);
		addr[2] = readl(SOC_FEC_MAC_BASE + 0x08);
		addr[3] = readl(SOC_FEC_MAC_BASE + 0x0c);
		addr[4] = readl(SOC_FEC_MAC_BASE + 0x10);
		addr[5] = readl(SOC_FEC_MAC_BASE + 0x14);

		if (cyg_plf_redboot_esa_validate(addr)) {
			diag_printf("Ethernet FEC MAC address from fuse bank: ");
			diag_printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
						addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);
			CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
										"fec_esa_data", addr2, CONFIG_ESA);
			if (memcmp(addr, addr2, ETHER_ADDR_LEN) != 0) {
				CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_SET,
											"fec_esa_data", addr, CONFIG_ESA);
			}
#ifdef SOC_MAC_ADDR_LOCK_FUSE
			if ((readl(IIM_BASE_ADDR + 0x800 + SOC_MAC_ADDR_FUSE_BANK * 0x400 +
					   SOC_MAC_ADDR_LOCK_FUSE * 4) &
				 SOC_MAC_ADDR_LOCK_BIT) == 0) {
				tx53_mac_addr_program(addr);
			}
#endif // SOC_MAC_ADDR_LOCK_FUSE
			return true;
		}
#endif // CYGSEM_REDBOOT_PLF_ESA_VALIDATE

		CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
									"fec_esa_data", addr, CONFIG_ESA);

		diag_printf("Ethernet FEC MAC address from fconfig: ");
		diag_printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
					addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);

#ifdef CYGSEM_REDBOOT_PLF_ESA_VALIDATE
		if (cyg_plf_redboot_esa_validate(addr)) {
			tx53_mac_addr_program(addr);
			return true;
		}

		diag_printf("** Error: Invalid MAC address: ");
		diag_printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
					addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);

		writel(addr[0], SOC_FEC_MAC_BASE + 0x00);
		writel(addr[1], SOC_FEC_MAC_BASE + 0x04);
		writel(addr[2], SOC_FEC_MAC_BASE + 0x08);
		writel(addr[3], SOC_FEC_MAC_BASE + 0x0c);
		writel(addr[4], SOC_FEC_MAC_BASE + 0x10);
		writel(addr[5], SOC_FEC_MAC_BASE + 0x14);

#ifdef SOC_MAC_ADDR_LOCK_FUSE
		if ((readl(IIM_BASE_ADDR + 0x800 + SOC_MAC_ADDR_FUSE_BANK * 0x400 +
				   SOC_MAC_ADDR_LOCK_FUSE * 4) &
			 SOC_MAC_ADDR_LOCK_BIT) == 0) {
			diag_printf("Use 'fconfig fec_esa_data' to set the MAC address\n");
			return false;
		} else {
			diag_printf("Using MAC address from fconfig\n");
		}
#else
		diag_printf("Using MAC address from fconfig\n");
#endif // SOC_MAC_ADDR_LOCK_FUSE
#endif // CYGSEM_REDBOOT_PLF_ESA_VALIDATE
		return true;
	}
	return false;
}

static mxc_fec_priv_t mxc_fec_private = {
	.phy = &eth0_phy,							  // PHY access routines
	.provide_esa = _tx53_provide_fec_esa,
};

ETH_DRV_SC(mxc_fec_sc,
		   &mxc_fec_private, // Driver specific data
		   mxc_fec_name,
		   mxc_fec_start,
		   mxc_fec_stop,
		   mxc_fec_control,
		   mxc_fec_can_send,
		   mxc_fec_send,
		   mxc_fec_recv,
		   mxc_fec_deliver,		// "pseudoDSR" called from fast net thread
		   mxc_fec_poll,		// poll function, encapsulates ISR and DSR
		   mxc_fec_int_vector);

NETDEVTAB_ENTRY(mxc_fec_netdev,
				mxc_fec_name,
				tx53_fec_init,
				&mxc_fec_sc);
#endif

#if defined(CYGPKG_REDBOOT) && defined(CYGSEM_REDBOOT_FLASH_CONFIG)
RedBoot_config_option("Set FEC network hardware address [MAC]",
					  fec_esa,
					  ALWAYS_ENABLED, true,
					  CONFIG_BOOL, true
					 );
RedBoot_config_option("FEC network hardware address [MAC]",
					  fec_esa_data,
					  "fec_esa", true,
					  CONFIG_ESA, 0
					 );
#endif // CYGPKG_REDBOOT && CYGSEM_REDBOOT_FLASH_CONFIG

#ifdef CYGSEM_HAL_VIRTUAL_VECTOR_SUPPORT
// Note that this section *is* active in an application, outside RedBoot,
// where the above section is not included.

#endif // CYGSEM_HAL_VIRTUAL_VECTOR_SUPPORT
#endif // CYGPKG_DEVS_ETH_ARM_MXCBOARD_ETH0

#endif // __WANT_DEVS
