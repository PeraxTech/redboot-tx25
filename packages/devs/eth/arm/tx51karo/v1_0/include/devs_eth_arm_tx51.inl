//==========================================================================
//
//      devs_eth_arm_tx51.inl
//
//      Board ethernet I/O definitions.
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/hal/hal_intr.h>			// CYGNUM_HAL_INTERRUPT_ETHR
#include <cyg/hal/hal_if.h>
#include <cyg/hal/mx51_iomux.h>

#ifdef CYGPKG_REDBOOT
#include <pkgconf/redboot.h>
#ifdef CYGSEM_REDBOOT_FLASH_CONFIG
#include <redboot.h>
#include <flash_config.h>
#endif
#endif


#ifdef __WANT_DEVS

#ifdef CYGPKG_DEVS_ETH_ARM_MXCBOARD_ETH0

#ifdef CYGPKG_DEVS_ETH_PHY

static bool mxc_fec_init(struct cyg_netdevtab_entry *tab);

static char  mxc_fec_name[] = "mxc_fec";

#define MX51_GPIO_ADDR(bank)			(GPIO1_BASE_ADDR + (((bank) - 1) << 14))
#define FEC_POWER_GPIO					1, 3
#define FEC_RESET_GPIO					2, 14

#ifdef CYGSEM_REDBOOT_PLF_ESA_VALIDATE
//
// Verify that the given ESA is valid for this platform
//
static char oui[3] = CYGDAT_DEVS_ETH_ARM_TX51KARO_OUI;

bool cyg_plf_redboot_esa_validate(unsigned char *val)
{
	return (val[0] == oui[0]) && (val[1] == oui[1]) && (val[2] == oui[2]);
}
#endif

extern int tx51_mac_addr_program(unsigned char mac_addr[ETHER_ADDR_LEN]);

static inline void tx51_write_reg(CYG_ADDRWORD base_addr, CYG_WORD32 offset, CYG_WORD32 val)
{
	if (net_debug) {
		diag_printf("Changing reg %08x from %08x to %08x\n",
					base_addr + offset, readl(base_addr + offset), val);
	}
	HAL_WRITE_UINT32(base_addr + offset, val);
}

static inline CYG_WORD32 tx51_read_reg(CYG_ADDRWORD base_addr, CYG_WORD32 offset)
{
	CYG_WORD32 val;

	HAL_READ_UINT32(base_addr + offset, val);
	if (net_debug) diag_printf("Read %08x from reg %08x\n", val, base_addr + offset);
	return val;
}

static inline void tx51_set_reg(CYG_ADDRWORD base_addr, CYG_WORD32 offset,
								CYG_WORD32 set_mask, CYG_WORD32 clr_mask)
{
	CYG_WORD32 val;

	HAL_READ_UINT32(base_addr + offset, val);
	if (net_debug) diag_printf("Changing reg %08x from %08x to %08x\n",
							   base_addr + offset, val,
							   (val & ~clr_mask) | set_mask);
	val = (val & ~clr_mask) | set_mask;
	HAL_WRITE_UINT32(base_addr + offset, val);
}

static struct tx51_gpio_setup {
	cyg_uint32 iomux_addr;
	unsigned on_func:5,
		off_func:5,
		grp:3,
		shift:5,
		dir:1, /* 0: input; 1: output */
		level:1;
} tx51_fec_gpio_data[] = {
	/* iomux reg offset,                    func,	    gpgrp, in/out */
	/*						                      gpiofn,  gpshft,level */
	{ IOMUXC_SW_MUX_CTL_PAD_EIM_A20,		0x11, 0x11, 2, 14, 1, 0, }, /* PHY reset */
	{ IOMUXC_SW_MUX_CTL_PAD_GPIO1_3,		0x10, 0x10, 1,  3, 1, 1, }, /* PHY power enable */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_CS3,		0x02, 0x13, 3, 19, 1, 0, }, /* MDC */
	{ IOMUXC_SW_MUX_CTL_PAD_EIM_EB2,		0x03, 0x11, 2, 22, 1, 0, }, /* MDIO */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_RB3,		0x01, 0x13, 3, 11, 0, }, /* RX_CLK */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_D11,		0x02, 0x13, 3, 29, 0, }, /* RX_DV */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_D9,		0x02, 0x13, 3, 31, 1, 1, }, /* RXD0/Mode0 */
	{ IOMUXC_SW_MUX_CTL_PAD_EIM_EB3,		0x03, 0x11, 2, 23, 1, 1, }, /* RXD1/Mode1 */
	{ IOMUXC_SW_MUX_CTL_PAD_EIM_CS2,		0x03, 0x11, 2, 27, 1, 1, }, /* RXD2/Mode2 */
	{ IOMUXC_SW_MUX_CTL_PAD_EIM_CS3,		0x03, 0x11, 2, 28, 1, 1, }, /* RXD3/nINTSEL */
	{ IOMUXC_SW_MUX_CTL_PAD_EIM_CS4,		0x03, 0x11, 2, 29, 0, }, /* RX_ER/RXD4 */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_RDY_INT,	0x01, 0x13, 3, 24, 0, }, /* TX_CLK */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_CS7,		0x01, 0x13, 3, 23, 1, 0, }, /* TX_EN */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_D8,		0x02, 0x13, 4,  0, 1, 0, }, /* TXD0 */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_CS4,		0x02, 0x13, 3, 20, 1, 0, }, /* TXD1 */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_CS5,		0x02, 0x13, 3, 21, 1, 0, }, /* TXD2 */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_CS6,		0x02, 0x13, 3, 22, 1, 0, }, /* TXD3 */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_RB2,		0x01, 0x13, 3, 10, 1, 0, }, /* COL/RMII/CRSDV */
	{ IOMUXC_SW_MUX_CTL_PAD_EIM_CS5,		0x03, 0x11, 2, 30, 1, 0, }, /* CRS */
	{ IOMUXC_SW_MUX_CTL_PAD_NANDF_CS2,		0x03, 0x13, 3, 18, 0, }, /* nINT/TX_ER/TXD4 */
};

static inline void tx51_phy_gpio_init(void)
{
	int i;

	if (net_debug) diag_printf("PHY GPIO init\n");

	/* setup all pins attached to the PHY to required level */
	for (i = 0; i < NUM_ELEMS(tx51_fec_gpio_data); i++) {
		struct tx51_gpio_setup *gs = &tx51_fec_gpio_data[i];

		tx51_set_reg(MX51_GPIO_ADDR(gs->grp),
					 GPIO_DR, gs->level << gs->shift, !gs->level << gs->shift);
		tx51_set_reg(MX51_GPIO_ADDR(gs->grp),
					 GPIO_GDIR, 1 << gs->shift, 0);
		tx51_write_reg(gs->iomux_addr, 0, gs->off_func);
	}
	for (i = 0; i < NUM_ELEMS(tx51_fec_gpio_data); i++) {
		struct tx51_gpio_setup *gs = &tx51_fec_gpio_data[i];

		if (gs->dir) {
			if (gs->level ^ gpio_tst_bit(gs->grp, gs->shift)) {
				diag_printf("%s: GPIO%d_%d[%d] is not %s\n", __FUNCTION__,
							gs->grp, gs->shift, i, gs->level ? "HIGH" : "LOW");
			}
		}
	}
	if (net_debug) diag_printf("PHY GPIO init done\n");
}

static bool tx51_fec_init(struct cyg_netdevtab_entry *tab)
{
	cyg_bool esa_set;
	int ok;

	/* Check, whether MAC address is enabled */
	ok = CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
									 "fec_esa", &esa_set, CONFIG_BOOL);
	if (!(ok && esa_set)) {
		diag_printf("FEC disabled; set fec_esa=true to enable networking\n");
		return false;
	}
	return mxc_fec_init(tab);
}

static void tx51_fec_phy_init(void)
{
	int i;
	int phy_reset_delay = 100;

	/*
	 * make sure the ETH PHY strap pins are pulled to the right voltage
	 * before deasserting the PHY reset GPIO
	 */
	tx51_phy_gpio_init();

	/* LAN8700 requires 21ms to power up */
	phy_reset_delay = 22000;
	if (!gpio_tst_bit(1, 3)) {
		diag_printf("**Failed to switch PHY power on: GPIO1_PSR[%08lx]=%08x\n",
					MX51_GPIO_ADDR(1) + GPIO_PSR,
					tx51_read_reg(MX51_GPIO_ADDR(1), GPIO_PSR));
	}
	if (gpio_tst_bit(2, 14)) {
		diag_printf("**Failed to assert PHY reset: GPIO2_PSR[%08lx]=%08x\n",
					MX51_GPIO_ADDR(2) + GPIO_PSR,
					tx51_read_reg(MX51_GPIO_ADDR(2), GPIO_PSR));
	}

	/* wait the specified time according to LAN8700 spec. before ... */
	HAL_DELAY_US(phy_reset_delay);
	/* ... deasserting FEC PHY reset */
	if (net_debug) diag_printf("Releasing PHY RESET\n");
	gpio_set_bit(2, 14);
	if (!gpio_tst_bit(2, 14)) {
		diag_printf("**Failed to release PHY reset\n");
	}
	/*
	 * Due to an RC-filter in the PHY RESET line, a minimum
	 * delay of 535us is required to let the RESET line rise
	 * above the logic high threshold of the PHY input pin.
         */
	HAL_DELAY_US(550);

	/* configure all FEC pins to their required functions */
	for (i = 0; i < NUM_ELEMS(tx51_fec_gpio_data); i++) {
		struct tx51_gpio_setup *gs = &tx51_fec_gpio_data[i];

		tx51_write_reg(gs->iomux_addr, 0, gs->on_func);
	}
}

ETH_PHY_REG_LEVEL_ACCESS_FUNS(eth0_phy,
							  tx51_fec_phy_init,
							  mxc_fec_phy_reset,
							  mxc_fec_phy_write,
							  mxc_fec_phy_read);

cyg_bool _tx51_provide_fec_esa(unsigned char *addr)
{
	cyg_bool enabled;
	int ok;

	ok = CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
					 "fec_esa", &enabled, CONFIG_BOOL);
	if (ok && enabled) {
#ifdef CYGSEM_REDBOOT_PLF_ESA_VALIDATE
		cyg_uint8 addr2[ETHER_ADDR_LEN];

		addr[0] = readl(SOC_FEC_MAC_BASE + 0x14);
		addr[1] = readl(SOC_FEC_MAC_BASE + 0x10);
		addr[2] = readl(SOC_FEC_MAC_BASE + 0xC);
		addr[3] = readl(SOC_FEC_MAC_BASE + 0x8);
		addr[4] = readl(SOC_FEC_MAC_BASE + 0x4);
		addr[5] = readl(SOC_FEC_MAC_BASE + 0x0);

		if (cyg_plf_redboot_esa_validate(addr)) {
			diag_printf("Ethernet FEC MAC address from fuse bank: ");
			diag_printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
						addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);
			CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
										"fec_esa_data", addr2, CONFIG_ESA);
			if (memcmp(addr, addr2, ETHER_ADDR_LEN) != 0) {
				CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_SET,
											"fec_esa_data", addr, CONFIG_ESA);
			}
#ifdef SOC_MAC_ADDR_LOCK_FUSE
			if ((readl(IIM_BASE_ADDR + 0x800 + SOC_MAC_ADDR_FUSE_BANK * 0x400 +
					   SOC_MAC_ADDR_LOCK_FUSE * 4) &
				 SOC_MAC_ADDR_LOCK_BIT) == 0) {
				tx51_mac_addr_program(addr);
			}
#endif // SOC_MAC_ADDR_LOCK_FUSE
			return true;
		}
#endif // CYGSEM_REDBOOT_PLF_ESA_VALIDATE

		CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
									"fec_esa_data", addr, CONFIG_ESA);

		diag_printf("Ethernet FEC MAC address from fconfig: ");
		diag_printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
					addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);

#ifdef CYGSEM_REDBOOT_PLF_ESA_VALIDATE
		if (cyg_plf_redboot_esa_validate(addr)) {
			tx51_mac_addr_program(addr);
			return true;
		}

		diag_printf("** Error: Invalid MAC address: ");
		diag_printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
					addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);

		writel(addr[0], SOC_FEC_MAC_BASE + 0x14);
		writel(addr[1], SOC_FEC_MAC_BASE + 0x10);
		writel(addr[2], SOC_FEC_MAC_BASE + 0xC);
		writel(addr[3], SOC_FEC_MAC_BASE + 0x8);
		writel(addr[4], SOC_FEC_MAC_BASE + 0x4);
		writel(addr[5], SOC_FEC_MAC_BASE + 0x0);

#ifdef SOC_MAC_ADDR_LOCK_FUSE
		if ((readl(IIM_BASE_ADDR + 0x800 + SOC_MAC_ADDR_FUSE_BANK * 0x400 +
				   SOC_MAC_ADDR_LOCK_FUSE * 4) &
			 SOC_MAC_ADDR_LOCK_BIT) == 0) {
			diag_printf("Use 'fconfig fec_esa_data' to set the MAC address\n");
			return false;
		} else {
			diag_printf("Using MAC address from fconfig\n");
		}
#else
		diag_printf("Using MAC address from fconfig\n");
#endif // SOC_MAC_ADDR_LOCK_FUSE
#endif // CYGSEM_REDBOOT_PLF_ESA_VALIDATE
		return true;
	}
	return false;
}

static mxc_fec_priv_t mxc_fec_private = {
	.phy = &eth0_phy,							  // PHY access routines
	.provide_esa = _tx51_provide_fec_esa,
};

ETH_DRV_SC(mxc_fec_sc,
		   &mxc_fec_private, // Driver specific data
		   mxc_fec_name,
		   mxc_fec_start,
		   mxc_fec_stop,
		   mxc_fec_control,
		   mxc_fec_can_send,
		   mxc_fec_send,
		   mxc_fec_recv,
		   mxc_fec_deliver,		// "pseudoDSR" called from fast net thread
		   mxc_fec_poll,		// poll function, encapsulates ISR and DSR
		   mxc_fec_int_vector);

NETDEVTAB_ENTRY(mxc_fec_netdev,
				mxc_fec_name,
				tx51_fec_init,
				&mxc_fec_sc);
#endif

#if defined(CYGPKG_REDBOOT) && defined(CYGSEM_REDBOOT_FLASH_CONFIG)
RedBoot_config_option("Set FEC network hardware address [MAC]",
					  fec_esa,
					  ALWAYS_ENABLED, true,
					  CONFIG_BOOL, true
					  );
RedBoot_config_option("FEC network hardware address [MAC]",
					  fec_esa_data,
					  "fec_esa", true,
					  CONFIG_ESA, 0
					  );
#endif // CYGPKG_REDBOOT && CYGSEM_REDBOOT_FLASH_CONFIG

#ifdef CYGSEM_HAL_VIRTUAL_VECTOR_SUPPORT
// Note that this section *is* active in an application, outside RedBoot,
// where the above section is not included.

#endif // CYGSEM_HAL_VIRTUAL_VECTOR_SUPPORT
#endif // CYGPKG_DEVS_ETH_ARM_MXCBOARD_ETH0

#endif // __WANT_DEVS
