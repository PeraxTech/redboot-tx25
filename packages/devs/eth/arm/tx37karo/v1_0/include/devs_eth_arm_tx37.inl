//==========================================================================
//
//      devs_eth_arm_tx37.inl
//
//      Board ethernet I/O definitions.
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/hal/hal_intr.h>           // CYGNUM_HAL_INTERRUPT_ETHR
#include <cyg/hal/hal_if.h>

#ifdef CYGPKG_REDBOOT
#include <pkgconf/redboot.h>
#ifdef CYGSEM_REDBOOT_FLASH_CONFIG
#include <redboot.h>
#include <flash_config.h>
#endif
#endif


#ifdef __WANT_DEVS

#ifdef CYGPKG_DEVS_ETH_ARM_MXCBOARD_ETH0

#ifdef CYGPKG_DEVS_ETH_PHY

static char  mxc_fec_name[] = "mxc_fec";

#define IOMUXC_CSPI1_MISO				0x138
#define IOMUXC_AUD5_WB_FS				0x130
#define IOMUXC_EIM_CS1					0x058
#define IOMUXC_EIM_BCLK					0x064
#define IOMUXC_UART1_RI					0x174
#define IOMUXC_CSPI1_SS1				0x140
#define IOMUXC_CSPI2_MOSI				0x148
#define IOMUXC_CSPI2_MISO				0x14c
#define IOMUXC_CSPI1_MOSI				0x134
#define IOMUXC_EIM_RW					0x068
#define IOMUXC_EIM_OE					0x050
#define IOMUXC_UART1_DCD				0x178
#define IOMUXC_CSPI2_SS0				0x150
#define IOMUXC_CSPI2_SS1				0x154
#define IOMUXC_CSPI2_SCLK				0x158
#define IOMUXC_EIM_CS0					0x054
#define IOMUXC_CSPI1_SS0				0x13c
#define IOMUXC_CSPI1_SCLK				0x144

#define IOMUXC_GPIO1_7					0x22c
#define IOMUXC_NANDF_CS1				0x088

#define SW_PAD_CTL_CSPI1_MISO			0x398
#define SW_PAD_CTL_AUD5_WB_FS			0x390
#define SW_PAD_CTL_EIM_CS1				0x2b8
#define SW_PAD_CTL_EIM_BCLK				0x2c4
#define SW_PAD_CTL_UART1_RI				0x3d4
#define SW_PAD_CTL_CSPI1_SS1			0x3a0
#define SW_PAD_CTL_CSPI2_MOSI			0x3a8
#define SW_PAD_CTL_CSPI2_MISO			0x3ac
#define SW_PAD_CTL_CSPI1_MOSI			0x394
#define SW_PAD_CTL_EIM_RW				0x2c8
#define SW_PAD_CTL_EIM_OE				0x2b0
#define SW_PAD_CTL_UART1_DCD			0x3d8
#define SW_PAD_CTL_CSPI2_SS0			0x3b0
#define SW_PAD_CTL_CSPI2_SS1			0x3b4
#define SW_PAD_CTL_CSPI2_SCLK			0x3b8
#define SW_PAD_CTL_EIM_CS0				0x2b4
#define SW_PAD_CTL_CSPI1_SS0			0x39c
#define SW_PAD_CTL_CSPI1_SCLK			0x3a4

#define SW_PAD_CTL_GPIO1_7				0x484
#define SW_PAD_CTL_NANDF_CS1			0x2e8

#define SW_PAD_MUX_CSPI3_IPP_IND_MISO	0x518

#define MX37_GPIO_ADDR(bank)			(GPIO1_BASE_ADDR + (((bank) - 1) << 14))

/*
TX37 -> TX27 GPIO cross reference
                TX27    Funktion    GPIO        Pad      IOMUXC SW_PAD  SW_PAD mode   strap
                GPIO    ALT     ALT                      OFFSET  CTRL    MUX          option
FEC_MDC         PD9     5       4   GPIO3_1  CSPI1_MISO  0x138   0x398
FEC_MDIO        PD8     5       4   GPIO2_23 AUD5_WB_FS  0x130   0x390  0x5a8   0
FEC_RX_CLK      PD14    2       1   GPIO2_1  EIM_CS1	 0x058   0x2b8  0x5b0   0     REGOFF: 0
FEC_RX_DV       PD13    2       3   GPIO1_10 EIM_BCLK	 0x064   0x2c4  0x5b4   0
FEC_RXD0        PD12    5       4   GPIO2_30 UART1_RI	 0x174   0x3d4  0x5ac   1     MODE0:  1
FEC_RXD1        PD5     2       4   GPIO3_3  CSPI1_SS1	 0x140   0x3a0                MODE1:  1
FEC_RXD2        PD6     2       4   GPIO3_5  CSPI2_MOSI	 0x148   0x3a8                MODE2:  1
FEC_RXD3        PD7     2       4   GPIO3_6  CSPI2_MISO	 0x14c   0x3ac                INTSEL: 0
FEC_RX_ER       PD4     5       4   GPIO3_0  CSPI1_MOSI	 0x134   0x394  0x5b8   1
FEC_TX_CLK      PD11    2       3   GPIO1_9  EIM_RW	 0x068   0x2c8  0x5bc   0
FEC_TX_EN       PF23    2       3   GPIO1_13 EIM_OE	 0x050   0x2b0
FEC_TXD0        PD0     5       4   GPIO2_31 UART1_DCD	 0x178   0x3d8
FEC_TXD1        PD1     2       4   GPIO3_7  CSPI2_SS0	 0x150   0x3b0
FEC_TXD2        PD2     2       4   GPIO3_8  CSPI2_SS1	 0x154   0x3b4 :( reference Manual says: 0xBASE_
FEC_TXD3        PD3     2       4   GPIO3_9  CSPI2_SCLK	 0x158   0x3b8
FEC_COL         PD15    2       1   GPIO2_0  EIM_CS0	 0x054   0x2b4  0x5a0   0     RMII:   0
FEC_CRS         PD10    5       4   GPIO3_2  CSPI1_SS0	 0x13c   0x39c  0x5a4   1     PHYAD4: 0
FEC_TX_ER       PD16    5       4   GPIO3_4  CSPI1_SCLK	 0x144   0x3a4

OSC26M_ENABLE   PB22                GPIO2 vom PMIC	  ---     ---
FEC_RESET~      PB30        1       GPIO1_7  GPIO1_7	 0x22c	 0x484
FEC_ENABLE      PB27        4       GPIO2_9  NANDF_CS1	 0x088	 0x2e8
*/

#ifdef CYGSEM_REDBOOT_PLF_ESA_VALIDATE
//
// Verify that the given ESA is valid for this platform
//
static char oui[3] = CYGDAT_DEVS_ETH_ARM_TX37KARO_OUI;

bool
cyg_plf_redboot_esa_validate(unsigned char *val)
{
	return (val[0] == oui[0]) && (val[1] == oui[1]) && (val[2] == oui[2]);
}
#endif

#ifdef SOC_MAC_ADDR_BASE
extern int tx37_mac_addr_program(unsigned char mac_addr[ETHER_ADDR_LEN]);
#else
static inline int tx37_mac_addr_program(unsigned char mac_addr[ETHER_ADDR_LEN])
{
	return 0;
}
#endif // SOC_MAC_ADDR_BASE

static inline void tx37_write_reg(CYG_ADDRWORD base_addr, CYG_WORD32 offset, CYG_WORD32 val)
{
	if (net_debug) {
		diag_printf("Changing reg %08x from %08x to %08x\n",
					base_addr + offset, readl(base_addr + offset), val);
	}
	HAL_WRITE_UINT32(base_addr + offset, val);
}

static inline CYG_WORD32 tx37_read_reg(CYG_ADDRWORD base_addr, CYG_WORD32 offset)
{
	CYG_WORD32 val;
	HAL_READ_UINT32(base_addr + offset, val);
	if (net_debug) diag_printf("Read %08x from reg %08x\n", val, base_addr + offset);
	return val;
}

static inline void tx37_set_reg(CYG_ADDRWORD base_addr, CYG_WORD32 offset,
				CYG_WORD32 set_mask, CYG_WORD32 clr_mask)
{
	CYG_WORD32 val;
	HAL_READ_UINT32(base_addr + offset, val);
	if (net_debug) diag_printf("Changing reg %08x from %08x to %08x\n", base_addr + offset, val,
				   (val & ~clr_mask) | set_mask);
	val = (val & ~clr_mask) | set_mask;
	HAL_WRITE_UINT32(base_addr + offset, val);
}

static struct tx37_gpio_setup {
	cyg_uint16 iomux_addr;
	cyg_uint8 on_func;
	cyg_uint8 off_func;
	cyg_uint8 grp;
	cyg_uint8 shift;
} tx37_fec_gpio_data[] = {
	/* iomux reg offset, func,      gpgrp */
	/*                       gpiofn,  gpshift */
	{ IOMUXC_CSPI1_MISO, 0x15, 0x14,	3,	1, },
	{ IOMUXC_AUD5_WB_FS, 0x15, 0x14,	2, 23, },
	{ IOMUXC_EIM_CS1,	 0x12, 0x11,	2,	1, },
	{ IOMUXC_EIM_BCLK,	 0x12, 0x13,	1, 10, },
	{ IOMUXC_UART1_RI,	 0x15, 0x14,	2, 30, },
	{ IOMUXC_CSPI1_SS1,	 0x12, 0x14,	3,	3, },
	{ IOMUXC_CSPI2_MOSI, 0x12, 0x14,	3,	5, },
	{ IOMUXC_CSPI2_MISO, 0x12, 0x14,	3,	6, },
	{ IOMUXC_CSPI1_MOSI, 0x15, 0x14,	3,	0, },
	{ IOMUXC_EIM_RW,	 0x12, 0x13,	1,	9, },
	{ IOMUXC_EIM_OE,	 0x12, 0x13,	1, 13, },
	{ IOMUXC_UART1_DCD,	 0x15, 0x14,	2, 31, },
	{ IOMUXC_CSPI2_SS0,	 0x12, 0x14,	3,	7, },
	{ IOMUXC_CSPI2_SS1,	 0x12, 0x14,	3,	8, },
	{ IOMUXC_CSPI2_SCLK, 0x12, 0x14,	3,	9, },
	{ IOMUXC_EIM_CS0,	 0x12, 0x11,	2,	0, },
	{ IOMUXC_CSPI1_SS0,	 0x15, 0x14,	3,	2, },
	{ IOMUXC_CSPI1_SCLK, 0x15, 0x14,	3,	4, },
	{ IOMUXC_GPIO1_7,    0x11, 0x11,	1,	7, },
	{ IOMUXC_NANDF_CS1,  0x14, 0x14,	2,	9, },
};

static struct tx37_gpio_setup tx37_fec_strap_pins[] = {
	{ IOMUXC_NANDF_CS1,  0x14, 0x14,	2,	9, },
	{ IOMUXC_UART1_RI,	 0x15, 0x14,	2, 30, },
	{ IOMUXC_CSPI1_SS1,	 0x12, 0x14,	3,	3, },
	{ IOMUXC_CSPI2_MOSI, 0x12, 0x14,	3,	5, },
};

static struct tx37_gpio_setup tx37_fec_mux_table[] = {
	{ 0x5a0,		0, },	/* FEC_COL via EIM_CS0 */
	{ 0x5a4,		1, },	/* FEC_CRS via CSPI1_SS0 */
	{ 0x5a8,		0, },	/* FEC_MDIO via AUD5_WB_FS */
	{ 0x5ac,		1, },	/* FEC_RXD0 via UART1_RI */
	{ 0x5b0,		0, },	/* FEC_RX_CLK via EIM_CS1 */
	{ 0x5b4,		0, },	/* FEC_RX_DV via EIM_BCLK */
	{ 0x5b8,		1, },	/* FEC_RX_ER via CSPI1_MOSI */
	{ 0x5bc,		0, },	/* FEC_TX_CLK via EIM_RW */
};

static inline void tx37_phy_power_off(void)
{
	int i;

	if (net_debug) diag_printf("Switching PHY POWER off\n");

#if 1
	for (i = 0; i < NUM_ELEMS(tx37_fec_gpio_data); i++) {
		struct tx37_gpio_setup *gs = &tx37_fec_gpio_data[i];

		if (net_debug) diag_printf("%s: GPIO%d_%d[%d] is %d\n", __FUNCTION__,
								   gs->grp, gs->shift, i,
								   gpio_tst_bit(gs->grp, gs->shift));
	}
#endif
	/* deassert all pins attached to the PHY */
	for (i = 0; i < NUM_ELEMS(tx37_fec_gpio_data); i++) {
		struct tx37_gpio_setup *gs = &tx37_fec_gpio_data[i];

		tx37_set_reg(MX37_GPIO_ADDR(gs->grp),
					 GPIO_DR, 0, 1 << gs->shift);
		tx37_set_reg(MX37_GPIO_ADDR(gs->grp),
					 GPIO_GDIR, 1 << gs->shift, 0);
		tx37_write_reg(IOMUXC_BASE_ADDR, gs->iomux_addr,
					   gs->off_func);
	}
	/* setup pin mux */
	for (i = 0; i < NUM_ELEMS(tx37_fec_mux_table); i++) {
		struct tx37_gpio_setup *gs = &tx37_fec_mux_table[i];

		tx37_write_reg(IOMUXC_BASE_ADDR, gs->iomux_addr, gs->on_func);
	}
#ifdef DEBUG
	for (i = 0; i < NUM_ELEMS(tx37_fec_gpio_data); i++) {
		struct tx37_gpio_setup *gs = &tx37_fec_gpio_data[i];

		if (gpio_tst_bit(gs->grp, gs->shift)) {
			if (net_debug) diag_printf("%s: GPIO%d_%d[%d] is not low\n", __FUNCTION__,
									   gs->grp, gs->shift, i);
		}
	}
#endif
	if (net_debug) diag_printf("PHY POWER off done\n");
}

static bool mxc_fec_init(struct cyg_netdevtab_entry *tab);
static bool tx37_fec_init(struct cyg_netdevtab_entry *tab)
{
	cyg_bool esa_set;
	int ok;

	/* Check, whether MAC address is enabled */
    	ok = CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
					 "fec_esa", &esa_set, CONFIG_BOOL);
	if (!(ok && esa_set)) {
		diag_printf("FEC disabled; set fec_esa=true to enable networking\n");
		return false;
	}
	return mxc_fec_init(tab);
}

static void tx37_fec_phy_init(void)
{
	int i;
	int phy_reset_delay = 100;

	/*
	 * make sure the ETH PHY strap pins are pulled to the right voltage
	 * before deasserting the PHY reset GPIO
	 * REGOFF:   PD14
	 * RMII:     PD15
	 * nINTSEL:  PD7
	 * MODE0:    PD12
	 * MODE1:    PD5
	 * MODE2:    PD6
	 * PHYAD0:   -
	 * PHYAD1:   GND
	 * PHYAD2:   GND
	 * PHYAD3:   -
	 * PHYAD4:   PD10
	 */
	// assert FEC PHY Reset (PB30) and switch PHY power on
	/* PB22, PB27, PB30 => GPIO out */
#if 0
	tx37_phy_power_off();
#endif
	if (!gpio_tst_bit(2, 9)) {
		if (net_debug) diag_printf("Switching PHY POWER on\n");
		/* switch FEC power off and assert FEC_RESET~ (low) */
		tx37_read_reg(MX37_GPIO_ADDR(1), GPIO_PSR);
		gpio_clr_bit(1, 7);
		gpio_set_bit(2, 9);
		tx37_read_reg(MX37_GPIO_ADDR(1), GPIO_PSR);

		/* wait for 22ms for LAN8700 to power up */
		phy_reset_delay = 22000 * 5;
#if 1
		if (!gpio_tst_bit(2, 9)) {
			diag_printf("**Failed to switch PHY power on: GPIO2_PSR[%08lx]=%08x\n",
				    MX37_GPIO_ADDR(2) + GPIO_PSR,
				    tx37_read_reg(MX37_GPIO_ADDR(2), GPIO_PSR));
		}
#endif
#if 1
		if (gpio_tst_bit(1, 7)) {
			diag_printf("**Failed to assert PHY reset: GPIO1_PSR[%08lx]=%08x\n",
				    MX37_GPIO_ADDR(1) + GPIO_PSR,
				    tx37_read_reg(MX37_GPIO_ADDR(1), GPIO_PSR));
		}
#endif
	} else {
		if (net_debug) diag_printf("Asserting PHY RESET\n");
		tx37_read_reg(MX37_GPIO_ADDR(1), GPIO_PSR);
		gpio_clr_bit(1, 7);
		if (gpio_tst_bit(1, 7)) {
			diag_printf("**Failed to assert PHY reset: GPIO1_PSR[%08lx]=%08x\n",
				    MX37_GPIO_ADDR(1) + GPIO_PSR,
				    tx37_read_reg(MX37_GPIO_ADDR(1), GPIO_PSR));
		}
		tx37_read_reg(MX37_GPIO_ADDR(1), GPIO_PSR);
	}

	/* configure FEC strap pins to their required values */
	for (i = 0; i < NUM_ELEMS(tx37_fec_strap_pins); i++) {
		struct tx37_gpio_setup *gs = &tx37_fec_strap_pins[i];

		if (net_debug) diag_printf("Asserting GPIO%d_%d\n", gs->grp,
								   gs->shift);
		tx37_set_reg(MX37_GPIO_ADDR(gs->grp),
					 GPIO_GDIR, 1 << gs->shift, 0);
		tx37_set_reg(MX37_GPIO_ADDR(gs->grp),
					 GPIO_DR, 1 << gs->shift, 0);
		tx37_write_reg(IOMUXC_BASE_ADDR, gs->iomux_addr,
					   gs->off_func);
		gpio_set_bit(gs->grp, gs->shift);
		if (!gpio_tst_bit(gs->grp, gs->shift)) {
			diag_printf("**Failed to assert GPIO%d_%d: GPIO%d_PSR[%08lx]=%08x\n",
						gs->grp, gs->shift, gs->grp,
						MX37_GPIO_ADDR(gs->grp) + GPIO_PSR,
						tx37_read_reg(MX37_GPIO_ADDR(gs->grp), GPIO_PSR));
		}
	}
	for (i = 0; i < NUM_ELEMS(tx37_fec_gpio_data); i++) {
		struct tx37_gpio_setup *gs = &tx37_fec_gpio_data[i];
		int j;
		int strap = 0;

		for (j = 0; j < NUM_ELEMS(tx37_fec_strap_pins); j++) {
			struct tx37_gpio_setup *sp = &tx37_fec_strap_pins[j];

			if (gs->grp == sp->grp && gs->shift == sp->shift) {
				strap = 1;
				break;
			}
		}
		if (strap) {
			if (!gpio_tst_bit(gs->grp, gs->shift)) {
				if (net_debug) diag_printf("GPIO%d_%d[%d] is low instead of high\n",
										   gs->grp, gs->shift, i);
			}
		} else {
			if (gpio_tst_bit(gs->grp, gs->shift)) {
				if (net_debug) diag_printf("GPIO%d_%d[%d] is high instead of low\n",
										   gs->grp, gs->shift, i);
			}
		}
	}
	/* wait for 100us according to LAN8700 spec. before ... */
	HAL_DELAY_US(phy_reset_delay);

	/* ... deasserting FEC PHY reset */
	if (net_debug) diag_printf("Releasing PHY RESET\n");
	gpio_set_bit(1, 7);
	if (!gpio_tst_bit(1, 7)) {
		diag_printf("**Failed to release PHY reset\n");
	}

	/* configure all FEC pins to their required functions */
	for (i = 0; i < NUM_ELEMS(tx37_fec_gpio_data); i++) {
		struct tx37_gpio_setup *gs = &tx37_fec_gpio_data[i];

		tx37_write_reg(IOMUXC_BASE_ADDR, gs->iomux_addr,
					   gs->on_func);
	}
}

ETH_PHY_REG_LEVEL_ACCESS_FUNS(eth0_phy,
                              tx37_fec_phy_init,
                              mxc_fec_phy_reset,
                              mxc_fec_phy_write,
                              mxc_fec_phy_read);

cyg_bool _tx37_provide_fec_esa(unsigned char *addr)
{
	cyg_bool enabled;
	int ok;

#if 0
	addr[0] = 0x00;
	addr[1] = 0x0c;
	addr[2] = 0xc6;
	addr[3] = 0x76;
	addr[4] = 0x6e;
	addr[5] = 0x02;
	return true;
#endif

	ok = CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
					 "fec_esa", &enabled, CONFIG_BOOL);
	if (ok && enabled) {
#ifdef CYGSEM_REDBOOT_PLF_ESA_VALIDATE
#ifdef SOC_MAC_ADDR_BASE
		cyg_uint8 addr2[ETHER_ADDR_LEN];

		addr[0] = readl(SOC_FEC_MAC_BASE2 + 0x0);
		addr[1] = readl(SOC_FEC_MAC_BASE2 + 0x4);
		addr[2] = readl(SOC_FEC_MAC_BASE2 + 0x8);
		addr[3] = readl(SOC_FEC_MAC_BASE2 + 0xC);
		addr[4] = readl(SOC_FEC_MAC_BASE2 + 0x10);
		addr[5] = readl(SOC_FEC_MAC_BASE2 + 0x14);

		if (cyg_plf_redboot_esa_validate(addr)) {
			diag_printf("Ethernet FEC MAC address from fuse bank: ");
			diag_printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
				    addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);
			CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
										"fec_esa_data", addr2, CONFIG_ESA);
			if (memcmp(addr, addr2, sizeof(addr)) != 0) {
				CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_SET,
											"fec_esa_data", addr, CONFIG_ESA);
			}
#ifdef SOC_MAC_ADDR_LOCK
			if ((readl(SOC_FEC_MAC_BASE2 - 0x14) & SOC_MAC_ADDR_LOCK) == 0) {
				tx37_mac_addr_program(addr);
			}
#endif // SOC_MAC_ADDR_LOCK
			return true;
		}
#endif // SOC_MAC_ADDR_BASE
#endif // CYGSEM_REDBOOT_PLF_ESA_VALIDATE

		CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
									"fec_esa_data", addr, CONFIG_ESA);
#ifdef SOC_MAC_ADDR_BASE
		writel(addr[0], SOC_FEC_MAC_BASE2 + 0x0);
		writel(addr[1], SOC_FEC_MAC_BASE2 + 0x4);
		writel(addr[2], SOC_FEC_MAC_BASE2 + 0x8);
		writel(addr[3], SOC_FEC_MAC_BASE2 + 0xC);
		writel(addr[4], SOC_FEC_MAC_BASE2 + 0x10);
		writel(addr[5], SOC_FEC_MAC_BASE2 + 0x14);
#endif // SOC_MAC_ADDR_BASE

		diag_printf("Ethernet FEC MAC address from fconfig: ");
		diag_printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
			    addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);

#ifdef CYGSEM_REDBOOT_PLF_ESA_VALIDATE
		if (cyg_plf_redboot_esa_validate(addr)) {
			tx37_mac_addr_program(addr);
			return true;
		}
		diag_printf("** Error: Invalid MAC address: ");
		diag_printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
			    addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);

#ifdef SOC_MAC_ADDR_LOCK
		if ((readl(SOC_FEC_MAC_BASE2 - 0x14) & SOC_MAC_ADDR_LOCK) == 0) {
			diag_printf("Use 'fconfig fec_esa_data' to set the MAC address\n");
			return false;
		} else {
			diag_printf("Using MAC address from fconfig\n");
		}
#else
		diag_printf("Using MAC address from fconfig\n");
#endif // SOC_MAC_ADDR_LOCK
#endif // CYGSEM_REDBOOT_PLF_ESA_VALIDATE
		return true;
	}
	return false;
}

static mxc_fec_priv_t mxc_fec_private = {
 	.phy = &eth0_phy,                             // PHY access routines
	.provide_esa = _tx37_provide_fec_esa,
};

ETH_DRV_SC(mxc_fec_sc,
		   &mxc_fec_private, // Driver specific data
           mxc_fec_name,
           mxc_fec_start,
           mxc_fec_stop,
           mxc_fec_control,
           mxc_fec_can_send,
           mxc_fec_send,
           mxc_fec_recv,
           mxc_fec_deliver,     // "pseudoDSR" called from fast net thread
           mxc_fec_poll,        // poll function, encapsulates ISR and DSR
           mxc_fec_int_vector);

NETDEVTAB_ENTRY(mxc_fec_netdev,
                mxc_fec_name,
                tx37_fec_init,
                &mxc_fec_sc);
#endif

#if defined(CYGPKG_REDBOOT) && defined(CYGSEM_REDBOOT_FLASH_CONFIG)
RedBoot_config_option("Set FEC network hardware address [MAC]",
                      fec_esa,
                      ALWAYS_ENABLED, true,
                      CONFIG_BOOL, false
                     );
RedBoot_config_option("FEC network hardware address [MAC]",
                      fec_esa_data,
                      "fec_esa", true,
                      CONFIG_ESA, 0
                     );
#endif // CYGPKG_REDBOOT && CYGSEM_REDBOOT_FLASH_CONFIG

#ifdef CYGSEM_HAL_VIRTUAL_VECTOR_SUPPORT
// Note that this section *is* active in an application, outside RedBoot,
// where the above section is not included.

#endif // CYGSEM_HAL_VIRTUAL_VECTOR_SUPPORT
#endif // CYGPKG_DEVS_ETH_ARM_MXCBOARD_ETH0

#endif // __WANT_DEVS
