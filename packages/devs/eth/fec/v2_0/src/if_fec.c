//==========================================================================
//
//      dev/if_fec.c
//
//      Device driver for FEC
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//####BSDCOPYRIGHTBEGIN####
//
// -------------------------------------------
//
// Portions of this software may have been derived from OpenBSD or other sources,
// and are covered by the appropriate copyright disclaimers included herein.
//
// -------------------------------------------
//
//####BSDCOPYRIGHTEND####
//==========================================================================
//#####DESCRIPTIONBEGIN####
//
// Author(s):    Fred Fan
// Contributors:
// Date:         2006-08-23
// Purpose:
// Description:  Driver for FEC ethernet controller
//
// Note:
//
//####DESCRIPTIONEND####
//
//==========================================================================

#include <pkgconf/system.h>
#ifdef CYGPKG_KERNEL
#include <cyg/kernel/kapi.h>
#endif
#include <pkgconf/io_eth_drivers.h>
#include <pkgconf/devs_eth_fec.h>

#include <cyg/infra/cyg_type.h>
#include <cyg/infra/cyg_ass.h>
#include <cyg/hal/hal_arch.h>
#include <cyg/hal/hal_intr.h>
#include <cyg/hal/hal_endian.h>
#include <cyg/infra/diag.h>
#include <cyg/hal/drv_api.h>
#include <cyg/hal/hal_soc.h>
#undef __ECOS
#define __ECOS
#include <cyg/io/eth/eth_drv.h>
#include <cyg/io/eth/netdev.h>

static bool mxc_fec_init(struct cyg_netdevtab_entry *tab);

#include <cyg/io/fec.h>
#define __WANT_DEVS
#include CYGDAT_DEVS_ETH_FEC_INL
#undef __WANT_DEVS

#include <redboot.h>

#include <cyg/hal/plf_mmap.h>
#ifdef CYGSEM_REDBOOT_FLASH_CONFIG
#include <flash_config.h>
#endif


#define MII_REG_CR			0  /* Control Register						   */
#define MII_REG_SR			1  /* Status Register						   */
#define MII_REG_PHYIR1		2  /* PHY Identification Register 1			   */
#define MII_REG_PHYIR2		3  /* PHY Identification Register 2			   */

static void mxc_fec_phy_status(mxc_fec_priv_t *dev, unsigned short value, bool show);

#ifndef CYGPKG_DEVS_ETH_PHY
/*!
 * Global variable which contains the name of FEC driver and device.
 */
static char  mxc_fec_name[] = "mxc_fec";

/*!
 * Global variable which defines the private structure of FEC device.
 */
static mxc_fec_priv_t  mxc_fec_private;
#endif

/*!
 * Global variable which defines the buffer descriptors for receive frames
 *	comment:: it must aligned by 128-bits.
 */
static mxc_fec_bd_t mxc_fec_rx_bd[FEC_BD_RX_NUM] __attribute__ ((aligned(32)));

/*!
 * Global variable which defines the buffer descriptors for transmit frames
 *	comment:: it must aligned by 128-bits.
 */
static mxc_fec_bd_t mxc_fec_tx_bd[FEC_BD_TX_NUM] __attribute__ ((aligned(32)));

/*!
 * Global variable which contains the frame buffers
 */
static unsigned char mxc_fec_rx_buf[FEC_BD_RX_NUM][2048] __attribute__ ((aligned(32)));

/*!
 * Global variable which contains the frame buffers
 */
static unsigned char mxc_fec_tx_buf[FEC_BD_TX_NUM][2048] __attribute__ ((aligned(32)));

#if 1
static void dump_packet(const unsigned char *pkt, size_t len)
{
	int i;

	diag_printf("Packet dump: %u byte", len);
	for (i = 0; i < len; i++) {
		if (i % 16 == 0) {
			diag_printf("\n%04x:", i);
		} else {
			if (i % 4 == 0) {
				diag_printf(" ");
			}
			if (i % 8 == 0) {
				diag_printf(" ");
			}
		}
		diag_printf(" %02x", pkt[i]);
	}
	if (i % 16)
		diag_printf("\n");
}
#endif

static inline volatile void *fec_reg_addr(volatile void *base, unsigned int reg)
{
	return (volatile void *)((unsigned long)base + reg);
}

#define mxc_fec_reg_read(hw_reg,reg) _mxc_fec_reg_read(hw_reg, reg, #reg)
static inline unsigned long _mxc_fec_reg_read(volatile void *base, unsigned int reg,
											const char *name)
{
	unsigned long val = readl(fec_reg_addr(base, reg));

	if (net_debug) diag_printf("Read %08lx from FEC reg %s[%03x]\n",
				   val, name, reg);
	return val;
}

#define mxc_fec_reg_write(hw_reg,reg,val) _mxc_fec_reg_write(hw_reg, reg, val, #reg)
static inline void _mxc_fec_reg_write(volatile void *base, unsigned int reg,
									  unsigned long val, const char *name)
{
	if (net_debug) diag_printf("Writing %08lx to FEC reg %s[%03x]\n",
							val, name, reg);
	writel(val, fec_reg_addr(base, reg));
}

#define mxc_fec_reg_read16(hw_reg,reg) _mxc_fec_reg_read16(hw_reg, reg, #reg)
static inline unsigned short _mxc_fec_reg_read16(volatile void *base, unsigned int reg,
												const char *name)
{
	unsigned short val = readw(fec_reg_addr(base, reg));

	if (net_debug) diag_printf("Read %04x from FEC reg %s[%03x]\n",
							val, name, reg);
	return val;
}

#define mxc_fec_reg_write16(hw_reg,reg,val) _mxc_fec_reg_write16(hw_reg, reg, val, #reg)
static inline void _mxc_fec_reg_write16(volatile void *base, unsigned int reg,
										unsigned short val, const char *name)
{
	if (net_debug) diag_printf("Writing %04x to FEC reg %s[%03x]\n",
							val, name, reg);
	writew(val, fec_reg_addr(base, reg));
}

/*!
 * This function gets the value of PHY registers via MII interface
 */
static int
mxc_fec_mii_read(volatile mxc_fec_reg_t *hw_reg, unsigned char phy_addr, unsigned char reg_addr,
				 unsigned short int *value)
{
	unsigned long waiting = FEC_MII_TIMEOUT;

	if (net_debug) diag_printf("%s: Trying to read phy[%02x] reg %04x\n",
							__FUNCTION__, phy_addr, reg_addr);
	if (mxc_fec_reg_read(hw_reg, eir) & FEC_EVENT_MII) {
		if (net_debug) diag_printf("%s: Clearing EIR_EVENT_MII\n", __FUNCTION__);
		mxc_fec_reg_write(hw_reg, eir, FEC_EVENT_MII);
	}
	if (net_debug) diag_printf("%s: EIR=%08lx\n", __FUNCTION__, mxc_fec_reg_read(hw_reg, eir));
	mxc_fec_reg_write(hw_reg, mmfr, FEC_MII_READ(phy_addr, reg_addr));/* Write CMD */
	while (1) {
		if (mxc_fec_reg_read(hw_reg, eir) & FEC_EVENT_MII) {
			if (net_debug) diag_printf("%s: Got EIR_EVENT_MII: EIR=%08lx\n",
						   __FUNCTION__, mxc_fec_reg_read(hw_reg, eir));
			mxc_fec_reg_write(hw_reg, eir, FEC_EVENT_MII);
			break;
		}
		if (--waiting == 0) {
			diag_printf("%s: Read from PHY at addr %d reg 0x%02x timed out: EIR=%08lx\n",
						__FUNCTION__, phy_addr, reg_addr,
						mxc_fec_reg_read(hw_reg, eir));
			return -1;
		}
		hal_delay_us(FEC_MII_TICK);
	}
	*value = FEC_MII_GET_DATA(mxc_fec_reg_read(hw_reg, mmfr));
	if (net_debug) diag_printf("%s: Read %04x from phy[%02x] reg %04x\n", __FUNCTION__,
							*value, phy_addr, reg_addr);
	return 0;
}

/*!
 * This function set the value of  PHY registers by MII interface
 */
static int
mxc_fec_mii_write(volatile mxc_fec_reg_t *hw_reg, unsigned char phy_addr, unsigned char reg_addr,
		  unsigned short int value)
{
	unsigned long waiting = FEC_MII_TIMEOUT;

	if (net_debug) diag_printf("%s: Trying to write %04x to phy[%02x] reg %04x\n", __FUNCTION__,
				   value, phy_addr, reg_addr);
	if (mxc_fec_reg_read(hw_reg, eir) & FEC_EVENT_MII) {
		if (net_debug) diag_printf("%s: Clearing EIR_EVENT_MII\n", __FUNCTION__);
		mxc_fec_reg_write(hw_reg, eir, FEC_EVENT_MII);
	}
	if (net_debug) diag_printf("%s: EIR=%08lx\n", __FUNCTION__, mxc_fec_reg_read(hw_reg, eir));
	mxc_fec_reg_write(hw_reg, mmfr,  FEC_MII_WRITE(phy_addr, reg_addr, value));/* Write CMD */
	if (net_debug) diag_printf("%s: Wrote cmd %08x to MMFR\n", __FUNCTION__,
				   FEC_MII_WRITE(phy_addr, reg_addr, value));
	while (1) {
		if (mxc_fec_reg_read(hw_reg, eir) & FEC_EVENT_MII) {
			if (net_debug) diag_printf("%s: Got EIR_EVENT_MII: EIR=%08lx\n",
						   __FUNCTION__, mxc_fec_reg_read(hw_reg, eir));
			mxc_fec_reg_write(hw_reg, eir, FEC_EVENT_MII);
			break;
		}
		if (--waiting == 0) {
			diag_printf("%s: Write to PHY at addr %d reg 0x%02x timed out: EIR=%08lx\n",
						__FUNCTION__, phy_addr, reg_addr,
						mxc_fec_reg_read(hw_reg, eir));
			return -1;
		}
		hal_delay_us(FEC_MII_TICK);
	}
	if (net_debug) diag_printf("%s: Write to phy register succeeded\n", __FUNCTION__);
	return 0;
}

static void
mxc_fec_set_mac_address(volatile mxc_fec_reg_t *hw_reg, unsigned char *enaddr)
{
	unsigned long value;

	value = enaddr[0];
	value = (value << 8) + enaddr[1];
	value = (value << 8) + enaddr[2];
	value = (value << 8) + enaddr[3];
	mxc_fec_reg_write(hw_reg, palr, value);

	value = enaddr[4];
	value = (value << 8) + enaddr[5];
	mxc_fec_reg_write(hw_reg, paur, value << 16);
}

#ifdef CYGOPT_HAL_ARM_MXC_FEC_MIIGSK
static int mxc_fec_mii_setup(mxc_fec_priv_t *priv)
{
	volatile mxc_fec_reg_t *hw_reg = priv->hw_reg;
	/*
	 * setup the MII gasket for RMII mode
	 */

	/* disable the gasket */
	mxc_fec_reg_write16(hw_reg, miigsk_enr, 0);

	/* wait for the gasket to be disabled */
	while (mxc_fec_reg_read16(hw_reg, miigsk_enr) & MIIGSK_ENR_READY)
		hal_delay_us(FEC_COMMON_TICK);

	/* configure gasket for RMII, 50 MHz, no loopback, and no echo */
	mxc_fec_reg_write16(hw_reg, miigsk_cfgr, MIIGSK_CFGR_IF_MODE_RMII |
						((!priv || (priv->status & FEC_STATUS_100M)) ?
							0 : MIIGSK_CFGR_FRCONT));

	/* re-enable the gasket */
	mxc_fec_reg_write16(hw_reg, miigsk_enr, MIIGSK_ENR_EN);

	/* wait until MII gasket is ready */
	int max_loops = 10;
	while ((mxc_fec_reg_read16(hw_reg, miigsk_enr) & MIIGSK_ENR_READY) == 0) {
		if (--max_loops <= 0) {
			diag_printf("WAIT for MII Gasket ready timed out\n");
			return -1;
		}
	}
	return 0;
}
#else
static inline int mxc_fec_mii_setup(mxc_fec_priv_t *priv)
{
	return 0;
}
#endif

/*!
 * This function enables the FEC for reception of packets
 */
static void
mxc_fec_start(struct eth_drv_sc *sc, unsigned char *enaddr, int flags)
{
	mxc_fec_priv_t *priv = sc ? sc->driver_private : NULL;
	volatile mxc_fec_reg_t *hw_reg = priv ? priv->hw_reg : NULL;

	if (!(priv && hw_reg)) {
		diag_printf("BUG[start]: FEC driver not initialized\n");
		return;
	}
	if (enaddr == NULL) {
		diag_printf("BUG[start]: no MAC address supplied\n");
		return;
	}
	mxc_fec_set_mac_address(hw_reg, enaddr);

	priv->tx_busy = 0;
	mxc_fec_reg_write(hw_reg, rdar, FEC_RX_TX_ACTIVE);
	mxc_fec_reg_write(hw_reg, ecr, FEC_ETHER_EN);
}

/*!
 * This function pauses the FEC controller.
 */
static void
mxc_fec_stop(struct eth_drv_sc *sc)
{
	mxc_fec_priv_t *priv = sc ? sc->driver_private : NULL;
	volatile mxc_fec_reg_t *hw_reg = priv ? priv->hw_reg : NULL;

	if (!(priv && hw_reg)) {
		diag_printf("BUG[stop]: FEC driver not initialized\n");
		return;
	}
	mxc_fec_reg_write(hw_reg, ecr, mxc_fec_reg_read(hw_reg, ecr) & ~FEC_ETHER_EN);
}

static int
mxc_fec_control(struct eth_drv_sc *sc, unsigned long key, void *data, int data_length)
{
	/*TODO:: Add support */
	diag_printf("mxc_fec_control: key=0x%08lx, data=%p, data_len=0x%08x\n",
				key, data, data_length);
	return 0;
}

/*!
 * This function checks the status of FEC control.
 */
static int
mxc_fec_can_send(struct eth_drv_sc *sc)
{
	mxc_fec_priv_t *priv = sc ? sc->driver_private : NULL;
	volatile mxc_fec_reg_t *hw_reg = priv ? priv->hw_reg : NULL;

	if (!(priv && hw_reg)) {
		diag_printf("BUG[can_send]: FEC driver not initialized\n");
		return 0;
	}
	if (priv->tx_busy) {
		diag_printf("WARNING[can_send]: MXC_FEC is busy for transmission\n");
		return 0;
	}

	if (!(mxc_fec_reg_read(hw_reg, ecr) & FEC_ETHER_EN)) {
		diag_printf("WARNING[can_send]: MXC_FEC is not enabled\n");
		return 0;
	}

	if (mxc_fec_reg_read(hw_reg, tcr) & FEC_TCR_RFC_PAUSE) {
		diag_printf("WARNING[can_send]: MXC_FEC is paused\n");
		return 0;
	}

	if (!(priv->status & FEC_STATUS_LINK_ON)) {
		/* Reading the PHY status for every packet to be sent is
		 * a real performance killer.
		 * Thus, only read the PHY status when the link is down to
		 * detect a possible new connection
		 */
#ifdef CYGPKG_DEVS_ETH_PHY
		unsigned short value;
		value = _eth_phy_state(priv->phy);
		if (value & ETH_PHY_STAT_LINK) {
			mxc_fec_phy_status(priv, value, true);
		}
#else
		unsigned short value;
		mxc_fec_mii_read(hw_reg, priv->phy_addr, 1, &value);
		if (value & PHY_STATUS_LINK_ST) {
			mxc_fec_phy_status(priv, 0, true);
		}
#endif
	}

	return priv->status & FEC_STATUS_LINK_ON;
}

/*!
 * This function transmits a frame.
 */
static void
mxc_fec_send(struct eth_drv_sc *sc, struct eth_drv_sg *sg_list, int sg_len, int total,
			unsigned long key)
{
	mxc_fec_priv_t *dev = sc ? sc->driver_private : NULL;
	volatile mxc_fec_reg_t *hw_reg = dev ? dev->hw_reg : NULL;
	mxc_fec_bd_t *p;
	int i, off;

	if (dev == NULL || hw_reg == NULL) {
		diag_printf("BUG[TX]: FEC driver not initialized\n");
		return;
	}
	if (total > (FEC_FRAME_LEN - 4)) total = FEC_FRAME_LEN - 4;
	if (sg_list == NULL || total <= 14) {
		if (sc->funs->eth_drv && sc->funs->eth_drv->tx_done) {
			sc->funs->eth_drv->tx_done(sc, key, -1);
		}
		return;
	}

	for (i = 0, off = 0, p = dev->tx_cur; i < sg_len; i++) {
		unsigned long vaddr;

		if (p->status & BD_TX_ST_RDY) {
			diag_printf("BUG[TX]: trying to resend already finished buffer\n");
			break;
		}
		if (sg_list[i].buf == 0) {
			diag_printf("WARNING[TX]: sg_list->buf is NULL\n");
			break;
		}
		vaddr = hal_ioremap_nocache((unsigned long)p->data) + off;
		memcpy((void *)vaddr, (void *)sg_list[i].buf, sg_list[i].len);
		off += sg_list[i].len;
	}
	if (off < 14) {
		diag_printf("WARNING[TX]: packet size %d too small\n", off);
		return;
	}
	p->length = off;
	p->status &= ~BD_TX_ST_ABC;
	p->status |= BD_TX_ST_LAST | BD_TX_ST_RDY | BD_TX_ST_TC;
	if (p->status & BD_TX_ST_WRAP) {
		p = dev->tx_bd;
	} else {
		p++;
	}
	dev->tx_cur = p;
	dev->tx_busy = 1;
	dev->tx_key = key;
	mxc_fec_reg_write(hw_reg, tdar, FEC_RX_TX_ACTIVE);
}

/*!
 * This function receives ready Frame in DB.
 */
static void
mxc_fec_recv(struct eth_drv_sc *sc, struct eth_drv_sg *sg_list, int sg_len)
{
	mxc_fec_priv_t *priv = sc ? sc->driver_private : NULL;
	mxc_fec_bd_t *p;
	unsigned long vaddr;

	if (sg_list == NULL || priv == NULL || sg_len <= 0) {
		diag_printf("BUG[RX]: FEC driver not initialized\n");
		return;
	}

	/*TODO: I think if buf pointer is NULL, this function
	 * should not be called
	 */
	if (sg_list->buf == 0) {
		diag_printf("WARING[RX]: the sg_list is empty\n");
		return;
	}
	p = priv->rx_cur;

	if (p->status & BD_RX_ST_EMPTY) {
		diag_printf("BUG[RX]: empty buffer received; status=%04x\n", p->status);
		return;
	}

	if (!(p->status & BD_RX_ST_LAST)) {
		diag_printf("BUG[RX]: status=%0xx\n", p->status);
		return;
	}
	vaddr = hal_ioremap_nocache((unsigned long)p->data);
	/*TODO::D_CACHE invalidate this data buffer*/
	memcpy((void *)sg_list->buf, (void *)vaddr, p->length - 4);
	if (net_debug) dump_packet((void *)sg_list->buf, p->length - 4);
}

static void
mxc_fec_deliver(struct eth_drv_sc *sc)
{
	/*TODO::When redboot support thread ,
	 *	the polling function will be called at here
	 */
	return;
}

/* This funtion just called by polling funtion */
static void
mxc_fec_check_rx_bd(struct eth_drv_sc *sc)
{
	mxc_fec_priv_t *priv = sc->driver_private;
	mxc_fec_bd_t *p;
	volatile mxc_fec_reg_t *hw_reg = priv->hw_reg;
	int i;

	for (i = 0, p = priv->rx_cur; i < FEC_RX_FRAMES; i++) {
		/*
		 * TODO::D-CACHE invalidate this BD.
		 * In WRITE_BACK mode: this may destroy the next BD
		 * when the CACHE_LINE is written back.
		 */
		if (p->status & BD_RX_ST_EMPTY) {
			break;
		}
		if (!(p->status & BD_RX_ST_LAST)) {
			diag_printf("BUG[RX]: status=%04x, length=%x\n", p->status, p->length);
			goto skip_next;
		}

		if (p->status & BD_RX_ST_ERRS) {
			diag_printf("RX error: status=%08x errors=%08x\n", p->status,
						p->status & BD_RX_ST_ERRS);
		} else if (p->length > FEC_FRAME_LEN) {
			diag_printf("RX error: packet size 0x%08x larger than max frame length: 0x%08x\n",
						p->length, FEC_FRAME_LEN);
		} else {
			sc->funs->eth_drv->recv(sc, p->length - 4);
		}
	skip_next:
		p->status = (p->status & BD_RX_ST_WRAP) | BD_RX_ST_EMPTY;

		if (p->status & BD_RX_ST_WRAP) {
			p = priv->rx_bd;
		} else {
			p++;
		}
		priv->rx_cur = p;
		mxc_fec_reg_write(hw_reg, rdar, FEC_RX_TX_ACTIVE);
	}
}

/*!
 * This function checks the event of FEC controller
 */
static void
mxc_fec_poll(struct eth_drv_sc *sc)
{
	mxc_fec_priv_t *priv = sc ? sc->driver_private : NULL;
	volatile mxc_fec_reg_t *hw_reg = priv ? priv->hw_reg : NULL;
	unsigned long value;
	int dbg = net_debug;
	static unsigned long last_poll;
	int poll_intvl = (priv->status & FEC_STATUS_LINK_ON) ? 100 : 10;

	if (priv == NULL || hw_reg == NULL) {
		diag_printf("BUG[POLL]: FEC driver not initialized\n");
		return;
	}
#if 1
	net_debug = 0;
#endif
	value = mxc_fec_reg_read(hw_reg, eir);
	mxc_fec_reg_write(hw_reg, eir, value & ~FEC_EVENT_MII);
#if 1
	net_debug = dbg;
#endif
	if (value & FEC_EVENT_TX_ERR) {
		diag_printf("WARNING[POLL]: Transmit error\n");
		sc->funs->eth_drv->tx_done(sc, priv->tx_key, -1);
		priv->tx_busy = 0;
	} else {
		if (value & FEC_EVENT_TX) {
			last_poll = 0;
			sc->funs->eth_drv->tx_done(sc, priv->tx_key, 0);
			priv->tx_busy = 0;
		}
	}
	if (value & FEC_EVENT_RX) {
		last_poll = 0;
		mxc_fec_check_rx_bd(sc);
	}

	if (value & FEC_EVENT_HBERR) {
		diag_printf("WARNGING[POLL]: Heartbeat error!\n");
	}

	if (value & FEC_EVENT_EBERR) {
		diag_printf("WARNING[POLL]: Ethernet Bus Error!\n");
	}

	if (value & (FEC_EVENT_TX_ERR | FEC_EVENT_HBERR | FEC_EVENT_EBERR) ||
		last_poll++ > poll_intvl) {
#ifdef CYGPKG_DEVS_ETH_PHY
		unsigned short value;
		value = _eth_phy_state(priv->phy);
		if (!(value & ETH_PHY_STAT_LINK) ^
			!(priv->status & FEC_STATUS_LINK_ON)) {
			mxc_fec_phy_status(priv, value, true);
		}
#else
		unsigned short value;
		mxc_fec_mii_read(hw_reg, priv->phy_addr, 1, &value);
		if (!(value & PHY_STATUS_LINK_ST) ^
			!(priv->status & FEC_STATUS_LINK_ON)) {
			mxc_fec_phy_status(priv, 0, true);
		}
#endif
		last_poll = 0;
	}
}

static int
mxc_fec_int_vector(struct eth_drv_sc *sc)
{
	/*TODO::
	 *	get FEC interrupt number
	 */
	return -1;
}

/*!
 * The function initializes the description buffer for receiving or transmitting
 */
static void
mxc_fec_bd_init(mxc_fec_priv_t *dev)
{
	int i;
	mxc_fec_bd_t *p;

	p = dev->rx_bd = (void *)hal_ioremap_nocache(hal_virt_to_phy((unsigned long)mxc_fec_rx_bd));
	for (i = 0; i < FEC_BD_RX_NUM; i++, p++) {
		p->status = BD_RX_ST_EMPTY;
		p->length = 0;
		p->data = (void *)hal_virt_to_phy((unsigned long)mxc_fec_rx_buf[i]);
	}

	dev->rx_bd[i - 1].status |= BD_RX_ST_WRAP;
	dev->rx_cur = dev->rx_bd;

	p = dev->tx_bd = (void *)hal_ioremap_nocache(hal_virt_to_phy((unsigned long)mxc_fec_tx_bd));
	for (i = 0; i < FEC_BD_TX_NUM; i++, p++) {
		p->status = 0;
		p->length = 0;
		p->data = (void *)hal_virt_to_phy((unsigned long)mxc_fec_tx_buf[i]);
	}

	dev->tx_bd[i - 1].status |= BD_TX_ST_WRAP;
	dev->tx_cur = dev->tx_bd;

	/*TODO:: add the sync function for items*/
}

/*!
 *This function initializes FEC controller.
 */
static void
mxc_fec_chip_init(mxc_fec_priv_t *dev)
{
	volatile mxc_fec_reg_t *hw_reg = dev->hw_reg;
	unsigned long ipg_clk;
	unsigned long clkdiv;

	mxc_fec_reg_write(hw_reg, ecr, FEC_RESET);
	while (mxc_fec_reg_read(hw_reg, ecr) & FEC_RESET) {
		hal_delay_us(FEC_COMMON_TICK);
	}

	mxc_fec_reg_write(hw_reg, eimr, 0);
	mxc_fec_reg_write(hw_reg, eir, ~0);

	mxc_fec_reg_write(hw_reg, rcr,
					(mxc_fec_reg_read(hw_reg, rcr) & ~0x3F) |
					FEC_RCR_FCE | FEC_RCR_MII_MODE);

	mxc_fec_reg_write(hw_reg, tcr, mxc_fec_reg_read(hw_reg, tcr) | FEC_TCR_FDEN);
	mxc_fec_reg_write(hw_reg, mibc, mxc_fec_reg_read(hw_reg, mibc) | FEC_MIB_DISABLE);

	mxc_fec_reg_write(hw_reg, iaur, 0);
	mxc_fec_reg_write(hw_reg, ialr, 0);
	mxc_fec_reg_write(hw_reg, gaur, 0);
	mxc_fec_reg_write(hw_reg, galr, 0);

	ipg_clk = get_main_clock(IPG_CLK);
	clkdiv = ((ipg_clk + 499999) / 2500000 / 2) << 1;
#if 1
	mxc_fec_reg_write(hw_reg, mscr, (mxc_fec_reg_read(hw_reg, mscr) & ~0x7e) |
					clkdiv);
#endif
	if (net_debug) diag_printf("mscr set to %08lx(%08lx) for ipg_clk %ld\n",
							clkdiv, mxc_fec_reg_read(hw_reg, mscr), ipg_clk);

	mxc_fec_reg_write(hw_reg, emrbr, 2048 - 16);
	mxc_fec_reg_write(hw_reg, erdsr, hal_virt_to_phy((unsigned long)dev->rx_bd));
	mxc_fec_reg_write(hw_reg, etdsr, hal_virt_to_phy((unsigned long)dev->tx_bd));

	/* must be done before enabling the MII gasket
	 * (otherwise MIIGSK_ENR_READY will never assert)
	 */
	mxc_fec_reg_write(hw_reg, ecr, FEC_ETHER_EN);
}

static void mxc_fec_phy_status(mxc_fec_priv_t *dev, unsigned short value, bool show)
{
	int changed = 0;
#ifdef CYGPKG_DEVS_ETH_PHY
	if (value & ETH_PHY_STAT_LINK) {
		changed = !(dev->status & FEC_STATUS_LINK_ON);
		dev->status |= FEC_STATUS_LINK_ON;
		if (value & ETH_PHY_STAT_FDX) {
			dev->status |= FEC_STATUS_FULL_DPLX;
		} else {
			dev->status &= ~FEC_STATUS_FULL_DPLX;
		}
		if (value & ETH_PHY_STAT_100MB) {
			changed |= !(dev->status & ETH_PHY_STAT_100MB);
			dev->status |= FEC_STATUS_100M;
		} else {
			changed |= !!(dev->status & ETH_PHY_STAT_100MB);
			dev->status &= ~FEC_STATUS_100M;
		}
	} else {
		changed = !!(dev->status & FEC_STATUS_LINK_ON);
		dev->status &= ~FEC_STATUS_LINK_ON;
	}
#else
	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_STATUS_REG, &value);
	if (value & PHY_STATUS_LINK_ST) {
		changed |= !(dev->status & FEC_STATUS_LINK_ON);
		dev->status |= FEC_STATUS_LINK_ON;
	} else {
		changed |= dev->status & FEC_STATUS_LINK_ON;
		dev->status &= ~FEC_STATUS_LINK_ON;
	}

	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_DIAG_REG, &value);
	if (value & PHY_DIAG_DPLX) {
		dev->status |= FEC_STATUS_FULL_DPLX;
	} else {
		dev->status &= ~FEC_STATUS_FULL_DPLX;
	}
	if (value & PHY_DIAG_RATE) {
		changed |= !(dev->status & FEC_STATUS_100M);
		dev->status |= FEC_STATUS_100M;
	} else {
		changed |= dev->status & FEC_STATUS_100M;
		dev->status &= ~FEC_STATUS_100M;
	}
#endif
	if (changed) {
		if (dev->status & FEC_STATUS_FULL_DPLX) {
			mxc_fec_reg_write(dev->hw_reg, tcr,
							(mxc_fec_reg_read(dev->hw_reg, tcr) & ~FEC_TCR_HBC) |
							FEC_TCR_FDEN);
			mxc_fec_reg_write(dev->hw_reg, rcr,
							(mxc_fec_reg_read(dev->hw_reg, rcr) & ~FEC_RCR_DRT) |
							FEC_RCR_FCE);
		} else {
			mxc_fec_reg_write(dev->hw_reg, tcr,
							(mxc_fec_reg_read(dev->hw_reg, tcr) & ~FEC_TCR_FDEN) |
							FEC_TCR_HBC);
			mxc_fec_reg_write(dev->hw_reg, rcr,
							(mxc_fec_reg_read(dev->hw_reg, rcr) & ~FEC_RCR_FCE) |
							FEC_RCR_DRT);
		}
		mxc_fec_mii_setup(dev);
	}
	if (!show || !changed) {
		return;
	}
	if (dev->status & FEC_STATUS_LINK_ON) {
		diag_printf("FEC: [ %s ] [ %s ]:\n",
					(dev->status & FEC_STATUS_FULL_DPLX) ? "FULL_DUPLEX" : "HALF_DUPLEX",
					(dev->status & FEC_STATUS_100M) ? "100 Mbps" : "10 Mbps");
	} else {
		diag_printf("FEC: no cable\n");
	}
}

#ifndef CYGPKG_DEVS_ETH_PHY
/*!
 * This function initializes the PHY
 */
static bool
mxc_fec_phy_init(mxc_fec_priv_t *dev)
{
#if 1
	unsigned short value = 0;
	unsigned long timeout = FEC_COMMON_TIMEOUT;

	/* Reset PHY */
	mxc_fec_mii_write(dev->hw_reg, dev->phy_addr, PHY_CTRL_REG, PHY_CTRL_RESET);
	while (timeout--) {
		if (mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_CTRL_REG, &value)) {
			return false;
		}

		if (!(value & PHY_CTRL_RESET)) {
			if (net_debug) diag_printf("%s: FEC reset completed\n", __FUNCTION__);
			break;
		}
		hal_delay_us(FEC_MII_TICK);
	}

	if (value & PHY_CTRL_RESET) {
		diag_printf("%s: FEC PHY reset timed out\n", __FUNCTION__);
		return false;
	}

	unsigned long id;
	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_IDENTIFY_1, &value);
	id = (value & PHY_ID1_MASK) << PHY_ID1_SHIFT;
	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_IDENTIFY_2, &value);
	id |= (value & PHY_ID2_MASK) << PHY_ID2_SHIFT;
	if (id == 0 || id == 0xffffffff) {
		diag_printf("FEC could not identify PHY: ID=%08lx\n", id);
		return false;
	}

	mxc_fec_mii_write(dev->hw_reg, dev->phy_addr, PHY_CTRL_REG,
			  PHY_CTRL_AUTO_NEG | PHY_CTRL_FULL_DPLX);

	timeout = FEC_COMMON_TIMEOUT;
	while (timeout-- &&
		mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_STATUS_REG, &value) == 0) {
		if (value & PHY_STATUS_LINK_ST) {
			if (net_debug) diag_printf("PHY Status: %04x\n", value);
			break;
		}
		hal_delay_us(FEC_MII_TICK);
	}
	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_MODE_REG, &value);
	value &= ~PHY_LED_SEL;
	mxc_fec_mii_write(dev->hw_reg, dev->phy_addr, PHY_MODE_REG, value);
#else
	unsigned long value = 0;
	unsigned long id = 0, timeout = 50;

	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_IDENTIFY_1, &value);
	id = (value & PHY_ID1_MASK) << PHY_ID1_SHIFT;
	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_IDENTIFY_2, &value);
	id |= (value & PHY_ID2_MASK) << PHY_ID2_SHIFT;

	switch (id) {
	case 0x00540088:
		break;
	case 0x00007C0C:
		break;
	default:
		diag_printf("[Warning] FEC not connect right PHY: ID=%lx\n", id);
	}

	mxc_fec_mii_write(dev->hw_reg, dev->phy_addr, PHY_CTRL_REG,
					PHY_CTRL_AUTO_NEG | PHY_CTRL_FULL_DPLX);

#ifdef CYGPKG_HAL_ARM_MX27ADS
	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_MODE_REG, &value);
	value &= ~PHY_LED_SEL;
	mxc_fec_mii_write(dev->hw_reg, dev->phy_addr, PHY_MODE_REG, value);
#endif

#if defined(CYGPKG_HAL_ARM_MX51) || defined(CYGPKG_HAL_ARM_MX25_3STACK) || \
	defined(CYGPKG_HAL_ARM_MX35_3STACK) || defined(CYGPKG_HAL_ARM_MX27_3STACK)
	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_AUTO_NEG_EXP_REG, &value);
	/* Wait for packet to arrive */
	while (((value & PHY_AUTO_NEG_NEW_PAGE) == 0) && (timeout != 0)) {
		hal_delay_us(100);
		mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_AUTO_NEG_EXP_REG, &value);
		timeout--;
	}
	/* Check if link is capable of auto-negotiation */
	if ((value & PHY_AUTO_NEG_CAP) == 1) {
		mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_INT_SRC_REG, &value);
		timeout = 50;
		/* Wait for auto-negotiation to complete */
		while (((value & PHY_INT_AUTO_NEG) == 0) && (timeout != 0)) {
			hal_delay_us(100);
			mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_INT_SRC_REG, &value);
			timeout--;
		}
	}
#endif
	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_STATUS_REG, &value);
	if (value & PHY_STATUS_LINK_ST) {
		dev->status |= FEC_STATUS_LINK_ON;
	} else {
		dev->status &= ~FEC_STATUS_LINK_ON;
	}

#ifdef CYGPKG_HAL_ARM_MX27ADS
	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_DIAG_REG, &value);
	if (value & PHY_DIAG_DPLX) {
		dev->status |= FEC_STATUS_FULL_DPLX;
	} else {
		dev->status &= ~FEC_STATUS_FULL_DPLX;
	}
	if (value & PHY_DIAG_DPLX) {
		dev->status |= FEC_STATUS_100M;
	} else {
		dev->status &= ~FEC_STATUS_100M;
	}
#endif

#if defined(CYGPKG_HAL_ARM_MX51) || defined(CYGPKG_HAL_ARM_MX25_3STACK) || \
	defined(CYGPKG_HAL_ARM_MX35_3STACK)
	mxc_fec_mii_read(dev->hw_reg, dev->phy_addr, PHY_AUTO_NEG_REG, &value);
	if (value & PHY_AUTO_10BASET) {
		dev->status &= ~FEC_STATUS_100M;
		if (value & PHY_AUTO_10BASET_DPLX) {
			dev->status |= FEC_STATUS_FULL_DPLX;
		} else {
			dev->status &= ~FEC_STATUS_FULL_DPLX;
		}
	}

	if (value & PHY_AUTO_100BASET) {
		dev->status |= FEC_STATUS_100M;
		if (value & PHY_AUTO_100BASET_DPLX) {
			dev->status |= FEC_STATUS_FULL_DPLX;
		} else {
			dev->status &= ~FEC_STATUS_FULL_DPLX;
		}
	}
#endif
	diag_printf("FEC: [ %s ] [ %s ] [ %s ]:\n",
		(dev->status & FEC_STATUS_FULL_DPLX) ? "FULL_DUPLEX" : "HALF_DUPLEX",
		(dev->status & FEC_STATUS_LINK_ON) ? "connected" : "disconnected",
		(dev->status & FEC_STATUS_100M) ? "100M bps" : "10M bps");
#endif
	return true;
}

static int mxc_fec_discover_phy(mxc_fec_priv_t *fep, unsigned char def_addr)
{
	int ret = 0;
	unsigned char phy_addr = def_addr;
	unsigned long id = 0;
	int i;

	for (i = 0; i < 32; i++) {
		unsigned short mii_reg;

		ret = mxc_fec_mii_read(fep->hw_reg, phy_addr, MII_REG_PHYIR1, &mii_reg);

		if (ret != 0) {
			break;
		}
		if (mii_reg != 0xffff && mii_reg != 0) {
			/* Got first part of ID, now get remainder.
			*/
			id = mii_reg;
			ret = mxc_fec_mii_read(fep->hw_reg, phy_addr, MII_REG_PHYIR2, &mii_reg);
			if (ret != 0) {
				break;
			}
			id = (id << 16) | mii_reg;
			if (net_debug) diag_printf("%s: discovered PHY %08lx at addr %x\n",
						   __FUNCTION__, id, phy_addr);
			ret = phy_addr;
			break;
		} else {
			phy_addr = (phy_addr + 1) % 32;
			ret = mxc_fec_mii_read(fep->hw_reg, phy_addr, MII_REG_PHYIR1, &mii_reg);
			if (ret != 0) {
				break;
			}
		}
	}
	if (id == 0) {
		/* Disable MII */
		fep->mxc_fec_reg_write(hw_reg, mscr, 0);
		ret = -1;
	}

	return ret;
}
#endif

/*
 * generic PHY support functions
 */
void mxc_fec_phy_reset(void)
{
	unsigned short value = 0;
	unsigned long timeout=FEC_COMMON_TIMEOUT;
	mxc_fec_priv_t *dev = &mxc_fec_private;

	/* Reset PHY */
	if (net_debug) diag_printf("%s\n", __FUNCTION__);

	_eth_phy_write(dev->phy, PHY_CTRL_REG, dev->phy->phy_addr, PHY_CTRL_RESET);
	while (timeout--) {
		if (!_eth_phy_read(dev->phy, PHY_CTRL_REG, dev->phy->phy_addr, &value)) {
			return;
		}

		if (!(value & PHY_CTRL_RESET)) {
			if (net_debug) diag_printf("%s: FEC reset completed\n", __FUNCTION__);
			break;
		}
		hal_delay_us(FEC_MII_TICK);
	}

	if (value & PHY_CTRL_RESET) {
		diag_printf("%s: FEC PHY reset timed out\n", __FUNCTION__);
		return;
	}
}

void mxc_fec_phy_init(void)
{
	if (net_debug) diag_printf("%s\n", __FUNCTION__);
}

bool mxc_fec_phy_read(int reg, int unit, unsigned short *data)
{
	int ret;
	if (net_debug) diag_printf("%s\n", __FUNCTION__);
	ret = mxc_fec_mii_read(mxc_fec_private.hw_reg, unit, reg, data);
	return ret == 0;
}

void mxc_fec_phy_write(int reg, int unit, unsigned short data)
{
	if (net_debug) diag_printf("%s\n", __FUNCTION__);
	mxc_fec_mii_write(mxc_fec_private.hw_reg, unit, reg, data);
}

/*! This function initializes the FEC driver.
 * It is called by net_init in net module of RedBoot during RedBoot init
 */
static bool
mxc_fec_init(struct cyg_netdevtab_entry *tab)
{
	struct eth_drv_sc *sc = tab ? tab->device_instance : NULL;
	mxc_fec_priv_t *private;
	unsigned char eth_add_local[ETHER_ADDR_LEN] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	int ok = 0;
#ifdef CYGSEM_REDBOOT_FLASH_CONFIG
	cyg_bool set_esa;
#endif

	if (net_debug)  diag_printf("%s:\n", __FUNCTION__);
	if (sc == NULL) {
		diag_printf("%s: no driver attached\n", __FUNCTION__);
		return false;
	}

	private = MXC_FEC_PRIVATE(sc);
	if (private == NULL) {
		private = &mxc_fec_private;
	}
	if (private->provide_esa) {
		ok = private->provide_esa(eth_add_local);
	}
#ifdef CYGSEM_REDBOOT_FLASH_CONFIG
	if (!ok) {
		/* Get MAC address from fconfig */
		ok = CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
										 "fec_esa", &set_esa, CONFIG_BOOL);
		if (ok && set_esa) {
			CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
										"fec_esa_data", eth_add_local, CONFIG_ESA);
		}
	}
#endif
	if (!ok) {
		diag_printf("No ESA provided via fuses or RedBoot config\n");
		return false;
	}

	private->hw_reg = (volatile void *)SOC_FEC_BASE;
	private->tx_busy = 0;
	private->status = 0;

	mxc_fec_bd_init(private);

	mxc_fec_chip_init(private);
#ifdef CYGPKG_DEVS_ETH_PHY
	if (!_eth_phy_init(private->phy)) {
		diag_printf("%s: Failed to initialize PHY\n", __FUNCTION__);
		return false;
	}
	_eth_phy_state(private->phy);
#else
	ok = mxc_fec_discover_phy(private, PHY_PORT_ADDR);
	if (ok < 0) {
		diag_printf("%s: no PHY found\n", __FUNCTION__);
		return false;
	}
	private->phy_addr = ok;
	mxc_fec_phy_init(private);
#endif
	/* TODO:: initialize System Resource : irq, timer */

	sc->funs->eth_drv->init(sc, eth_add_local);
	mxc_fec_phy_status(private, _eth_phy_state(private->phy), true);

	return true;
}

#ifndef CYGPKG_DEVS_ETH_PHY
/*!
 * Global variable which defines the FEC driver,
 */
ETH_DRV_SC(mxc_fec_sc,
		&mxc_fec_private,	// Driver specific data
		mxc_fec_name,
		mxc_fec_start,
		mxc_fec_stop,
		mxc_fec_control,
		mxc_fec_can_send,
		mxc_fec_send,
		mxc_fec_recv,
		mxc_fec_deliver,	 // "pseudoDSR" called from fast net thread
		mxc_fec_poll,		 // poll function, encapsulates ISR and DSR
		mxc_fec_int_vector);

/*!
 * Global variable which defines the FEC device
 */
NETDEVTAB_ENTRY(mxc_fec_netdev,
				mxc_fec_name,
				mxc_fec_init,
				&mxc_fec_sc);

#endif // CYGPKG_DEVS_ETH_PHY
