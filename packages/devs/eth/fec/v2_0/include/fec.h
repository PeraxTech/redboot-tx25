#ifndef _CYGONCE_ETH_FEC_H_
#define _CYGONCE_ETH_FEC_H_
//==========================================================================
//
//      dev/mxc_fec.h
//
//		Fast Ethernet MAC controller in i.MXx
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//####BSDCOPYRIGHTBEGIN####
//
//
//####BSDCOPYRIGHTEND####
//==========================================================================
//#####DESCRIPTIONBEGIN####
//
// Author(s):    Fred Fan
// Contributors:
// Date:         2006-08-23
// Purpose:
// Description:
//
//####DESCRIPTIONEND####
//
//==========================================================================

#include <cyg/infra/cyg_type.h>

#include <cyg/hal/hal_io.h>

#ifdef CYGPKG_DEVS_ETH_PHY
/* generic PHY device access functions */
void mxc_fec_phy_init(void);
void mxc_fec_phy_reset(void);
bool mxc_fec_phy_read(int reg, int unit, unsigned short *data);
void mxc_fec_phy_write(int reg, int unit, unsigned short data);

#include <cyg/io/eth_phy.h>
#endif

/* The defines of event bits */
#define FEC_EVENT_HBERR		0x80000000
#define FEC_EVENT_BABR		0x40000000
#define FEC_EVENT_BABT		0x20000000
#define FEC_EVENT_GRA		0x10000000
#define FEC_EVENT_TXF		0x08000000
#define FEC_EVENT_TXB		0x04000000
#define FEC_EVENT_RXF		0x02000000
#define FEC_EVENT_RXB		0x01000000
#define FEC_EVENT_MII		0x00800000
#define FEC_EVENT_EBERR		0x00400000
#define FEC_EVENT_LC		0x00200000
#define FEC_EVENT_RL		0x00100000
#define FEC_EVENT_UN		0x00080000

#define FEC_EVENT_TX		FEC_EVENT_TXF
#define FEC_EVENT_TX_ERR	(FEC_EVENT_BABT | FEC_EVENT_LC | FEC_EVENT_RL | FEC_EVENT_UN)
#define FEC_EVENT_RX		FEC_EVENT_RXF
#define FEC_EVENT_ERR		(FEC_EVENT_HBERR | FEC_EVENT_EBERR)

#define FEC_RX_FRAMES		((CYGNUM_IO_ETH_DRIVERS_NUM_PKT / 2) + 1)
#define FEC_FRAME_LEN		(1540 + 4)

/* the defines to active transmit or receive frame */
#define FEC_RX_TX_ACTIVE	0x01000000

/* the defines of Ethernet Control register */
#define FEC_RESET			0x00000001
#define FEC_ETHER_EN		0x00000002

/* the defins of MII operation */
#define FEC_MII_ST			0x40000000
#define FEC_MII_OP_OFF		28
#define FEC_MII_OP_MASK		0x03
#define FEC_MII_OP_RD		0x02
#define FEC_MII_OP_WR		0x01
#define FEC_MII_PA_OFF		23
#define FEC_MII_PA_MASK		0xFF
#define FEC_MII_RA_OFF		18
#define FEC_MII_RA_MASK		0xFF
#define FEC_MII_TA			0x00020000
#define FEC_MII_DATA_OFF	0
#define FEC_MII_DATA_MASK	0x0000FFFF

#define FEC_MII_FRAME		(FEC_MII_ST | FEC_MII_TA)
#define FEC_MII_OP(x)		(((x) & FEC_MII_OP_MASK) << FEC_MII_OP_OFF)
#define FEC_MII_PA(pa)		(((pa)& FEC_MII_PA_MASK) << FEC_MII_PA_OFF)
#define FEC_MII_RA(ra)		(((ra)& FEC_MII_RA_MASK) << FEC_MII_RA_OFF)
#define FEC_MII_SET_DATA(v)	(((v) & FEC_MII_DATA_MASK) << FEC_MII_DATA_OFF)
#define FEC_MII_GET_DATA(v)	(((v) >> FEC_MII_DATA_OFF) & FEC_MII_DATA_MASK)
#define FEC_MII_READ(pa, ra) ((FEC_MII_FRAME | FEC_MII_OP(FEC_MII_OP_RD)) | \
								FEC_MII_PA(pa) | FEC_MII_RA(ra))
#define FEC_MII_WRITE(pa, ra, v) (FEC_MII_FRAME | FEC_MII_OP(FEC_MII_OP_WR) | \
									FEC_MII_PA(pa) | FEC_MII_RA(ra) | \
									FEC_MII_SET_DATA(v))

#define MII_SPEED_SHIFT		1
#define MII_SPEED_MASK		0x0000003F
#define MII_SPEED(x)		((((((x) + 499999) / 2500000) & MII_SPEED_MASK) >> 1) << MII_SPEED_SHIFT)

/* the defines of MIB control */
#define FEC_MIB_DISABLE		0x80000000

/* the defines of Receive Control*/
#define FEC_RCR_FCE			0x00000020
#define FEC_RCR_BC_REJ		0x00000010
#define FEC_RCR_PROM		0x00000008
#define FEC_RCR_MII_MODE	0x00000004
#define FEC_RCR_DRT			0x00000002

/* the defines of Transmit Control*/
#define FEC_TCR_RFC_PAUSE	0x00000010
#define FEC_TCR_FDEN		0x00000004
#define FEC_TCR_HBC			0x00000002

/* the defines of buffer description*/
#define FEC_BD_RX_NUM		256
#define FEC_BD_TX_NUM		2

#ifdef CYGOPT_HAL_ARM_MXC_FEC_MIIGSK
/* the defines for MIIGSK */

/* RMII frequency control: 0=50MHz, 1=5MHz */
#define MIIGSK_CFGR_FRCONT			(1 << 6)

/* loopback mode */
#define MIIGSK_CFGR_LBMODE			(1 << 4)

/* echo mode */
#define MIIGSK_CFGR_EMODE			(1 << 3)

/* MII gasket mode field */
#define MIIGSK_CFGR_IF_MODE_MASK	(3 << 0)

/* MMI/7-Wire mode */
#define MIIGSK_CFGR_IF_MODE_MII		(0 << 0)

/* RMII mode */
#define MIIGSK_CFGR_IF_MODE_RMII	(1 << 0)

/* reflects MIIGSK Enable bit (RO) */
#define MIIGSK_ENR_READY			(1 << 2)

/* enable MIGSK (set by default) */
#define MIIGSK_ENR_EN				(1 << 1)
#endif

typedef	volatile void mxc_fec_reg_t;
#define eir				0x004	/* Interrupt Event Register */
#define eimr			0x008	/* Interrupt Mask Register */
#define rdar			0x010	/* Receive Descriptor Active Register*/
#define tdar			0x014	/* Transmit Descriptor Active Register*/
#define ecr				0x024	/*Receive Descriptor Active Register*/
#define mmfr			0x040	/*MII Management Frame Register */
#define mscr			0x044	/*MII Speed Control Register */
#define mibc			0x064	/*MII Control/Status Register */
#define rcr				0x084	/*Receive Control Register */
#define tcr				0x0C4	/*Transmit Control register */
#define palr			0x0E4	/*Physical Address Low Register*/
#define paur			0x0E8	/*Physical Address High+Type Register*/
#define opd				0x0EC	/*Opcode+Pause Duration */
#define iaur			0x118	/*Upper 32bits Individual Hash Table*/
#define ialr			0x11c	/*lower 32bits Individual Hash Table*/
#define gaur			0x120	/*Upper 32bits Group Hash Table*/
#define galr			0x124	/*lower 32bits Group Hash Table*/
#define tfwr			0x144	/*Trasmit FIFO Watermark */
#define frbr			0x14c	/*FIFO Receive Bound Register*/
#define frsr			0x150	/*FIFO Receive FIFO Start Registers*/
#define erdsr			0x180	/*Pointer to Receive Descriptor Ring*/
#define etdsr			0x184	/*Pointer to Transmit Descriptor Ring*/
#define emrbr			0x188	/*Maximum Receive Buffer size*/
#ifdef CYGOPT_HAL_ARM_MXC_FEC_MIIGSK
#define miigsk_cfgr		0x300 /* MIIGSK Configuration Register */
#define miigsk_enr		0x308  /* MIIGSK Enable Register */
#endif

#define BD_RX_ST_EMPTY	0x8000
#define BD_RX_ST_WRAP	0x2000
#define BD_RX_ST_LAST	0x0800
#define BD_RX_ST_ERRS	0x0037

#define BD_TX_ST_RDY	0x8000
#define BD_TX_ST_WRAP	0x2000
#define BD_TX_ST_LAST	0x0800
#define BD_TX_ST_TC		0x0400
#define BD_TX_ST_ABC	0x0200

typedef struct mxc_fec_bd_t
{
	unsigned short int length;	/*packet size*/
	unsigned short int status;	/*control & statue of this buffer description*/
	unsigned char	   *data;	/*frame buffer address*/
} mxc_fec_bd_t;

typedef struct mxc_fec_priv_s
{
	mxc_fec_reg_t *hw_reg;		/*the register base address of FEC*/
#ifdef CYGPKG_DEVS_ETH_PHY
	eth_phy_access_t *phy;
#else
	unsigned char   phy_addr;	/*the address of PHY which associated with FEC controller*/
#endif
	unsigned char   tx_busy;	/*0:free, 1:transmitting frame*/
	unsigned char	res[2];
	unsigned long	status;		/*the status of FEC device:link-status etc.*/
	unsigned long	tx_key;		/*save the key delivered from send function*/
	mxc_fec_bd_t   *rx_bd;		/*the receive buffer description ring*/
	mxc_fec_bd_t   *rx_cur;		/*the next recveive buffer description*/
	mxc_fec_bd_t   *tx_bd;		/*the transmit buffer description rign*/
	mxc_fec_bd_t   *tx_cur;		/*the next transmit buffer description*/
	cyg_bool (*provide_esa)(unsigned char *);
} mxc_fec_priv_t;

#define MXC_FEC_PRIVATE(x)	((mxc_fec_priv_t *)(x)->driver_private)

/*The defines of the status field of mxc_fec_priv_t */
#define FEC_STATUS_LINK_ON		0x80000000
#define FEC_STATUS_FULL_DPLX	0x40000000
#define FEC_STATUS_AUTO_NEG		0x20000000
#define FEC_STATUS_100M			0x10000000

/*The defines about PHY */
#ifndef FEC_PHY_ADDR
#define PHY_PORT_ADDR			0x01
#else
#define PHY_PORT_ADDR			FEC_PHY_ADDR
#endif

#define PHY_CTRL_REG			0x00
#define PHY_CTRL_RESET			0x8000
#define PHY_CTRL_AUTO_NEG		0x1000
#define PHY_CTRL_FULL_DPLX		0x0100

#define PHY_STATUS_REG			0x01
#define PHY_STATUS_LINK_ST		0x0004

#define PHY_IDENTIFY_1			0x02
#define PHY_IDENTIFY_2			0x03
#define PHY_ID1_SHIFT			2
#define PHY_ID1_MASK			0xFFFF
#define PHY_ID2_SHIFT			8
#define PHY_ID2_MASK			0xFC00
#define PHY_MODE_NUM			0x03F0
#define PHY_REV_NUM				0x000F

#define PHY_DIAG_REG			0x12
#define PHY_DIAG_DPLX			0x0800
#define PHY_DIAG_RATE			0x0400

#define PHY_MODE_REG			0x15
#define PHY_LED_SEL				0x200

#define PHY_AUTO_NEG_REG		0x5
#define PHY_AUTO_10BASET		0x20
#define PHY_AUTO_10BASET_DPLX	0x40
#define PHY_AUTO_100BASET		0x80
#define PHY_AUTO_100BASET_DPLX	0x100

#define PHY_AUTO_NEG_EXP_REG	0x6
#define PHY_AUTO_NEG_NEW_PAGE	0x2
#define PHY_AUTO_NEG_CAP		0x1

#define PHY_INT_SRC_REG			29
#define PHY_INT_AUTO_NEG		0x40
#define FEC_COMMON_TICK			2
#define FEC_COMMON_TIMEOUT		(1000 * 1000)
#define FEC_MII_TICK			2
#define FEC_MII_TIMEOUT			(1000 * 1000)
#endif // _CYGONCE_ETH_FEC_H_
