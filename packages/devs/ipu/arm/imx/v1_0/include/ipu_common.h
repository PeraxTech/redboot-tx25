//==========================================================================
//
//      IPU_COMMON.h
//
//      common functions declaration and macro definitions for IPUv3d
//
//==========================================================================
//#####DESCRIPTIONBEGIN####
//
// Author(s):       Ray Sun <Yanfei.Sun@freescale.com>
// Create Date: 2008-07-31
//
//####DESCRIPTIONEND####
//
//==========================================================================

#ifndef IPU_COMMON_H_
#define IPU_COMMON_H_

// System-wide configuration info
#include <pkgconf/system.h>
#include <cyg/infra/cyg_type.h>
#include <cyg/infra/diag.h>
#ifdef CYGBLD_HAL_PLF_DEFS_H
#include CYGBLD_HAL_PLF_DEFS_H
#else
#include <cyg/hal/fsl_board.h>
#endif

#ifdef IMX_IPU_VER_3_EX
#include "ipuv3ex_reg_def.h"
#endif

#ifdef IMX_IPU_VER_3_D
#include "ipuv3d_reg_def.h"
#endif

#define IPU_DEBUG
#undef IPU_DEBUG
#ifdef IPU_DEBUG
#define DP(fmt,args...) diag_printf(fmt, ## args)
#else
#define DP(fmt,args...)
#endif
#define ERRDP(fmt, arg...) diag_printf("[ERR] " fmt, ## arg)
#define WARNDP(fmt, arg...) diag_printf("[WARN] " fmt, ## arg)
#define INFODP(fmt, arg...) diag_printf("[INFO] " fmt, ## arg)


#define TIMEOUT_VALUE 0x1000

#define T_VALUE 2

/* Epson LCD command definitions */
#define DISON 0x29
#define DISOFF 0x28
#define GAMSET 0x26
#define SLPIN 0x10
#define SLPOUT 0x11
#define PASET 0x2b
#define CASET 0x2a
#define MADCTL 0x36
#define COLMOD 0x3a
#define RAMWR 0x2c
#define PTLON 0x12
#define PTLAR 0x30
#define NORON 0x13

/* DI counter definitions */
#define DI_COUNTER_BASECLK		0
#define DI_COUNTER_IHSYNC		1
#define DI_COUNTER_OHSYNC		2
#define DI_COUNTER_OVSYNC		3
#define DI_COUNTER_ALINE		4
#define DI_COUNTER_ACLOCK		5

/* IDMAC defines */
#define INTERLEAVED_MODE 0
#define NON_INTERLEAVED_MODE 1

#define SHIFT_DISABLE 0
#define SHIFT_ENABLE  1

#define GET_LSB(bit, val)  (((unsigned int)(val)) & ((0x1<<(bit)) - 1))

#include CYGHWR_MEMORY_LAYOUT_H

/* Display buffer starts at the end of DDR */
#define DISPLAY_BUFFER_ADDR (void *)(SDRAM_BASE_ADDR + CYGMEM_REGION_ram_SIZE - 0x400000)

typedef struct {
	unsigned int lowmask;		// low mask inorder to find the correct masking in case of splitted data
	unsigned int ID_mask;		// ID mask of the current field
	unsigned int ID_addrs;		// ID address of the current channel
	unsigned int data_high_sh;	// High data shift if needed
} idmac_bpp_STC;

typedef struct display_buffer_info {
	CYG_ADDRESS startAddr;
	unsigned int width;
	unsigned int height;
	int dataFormat;
	int bpp;
	int channel;
} display_buffer_info_t;

typedef struct {
	unsigned int channel;
	unsigned int xv;
	unsigned int yv;
	unsigned int xb;
	unsigned int yb;
	unsigned int nsb_b;
	unsigned int cf;
	unsigned int sx;
	unsigned int sy;
	unsigned int ns;
	unsigned int sdx;
	unsigned int sm;
	unsigned int scc;
	unsigned int sce;
	unsigned int sdy;
	unsigned int sdrx;
	unsigned int sdry;
	unsigned int bpp;
	unsigned int dec_sel;
	unsigned int dim;
	unsigned int so;
	unsigned int bndm;
	unsigned int bm;
	unsigned int rot;
	unsigned int hf;
	unsigned int vf;
	unsigned int the;
	unsigned int cap;
	unsigned int cae;
	unsigned int fw;
	unsigned int fh;
	unsigned int eba0;
	unsigned int eba1;
	unsigned int ilo;
	unsigned int npb;
	unsigned int pfs;
	unsigned int alu;
	unsigned int albm;
	unsigned int id;
	unsigned int th;
	unsigned int sl;
	unsigned int wid0;
	unsigned int wid1;
	unsigned int wid2;
	unsigned int wid3;
	unsigned int ofs0;
	unsigned int ofs1;
	unsigned int ofs2;
	unsigned int ofs3;
	unsigned int cre;
	unsigned int ubo;
	unsigned int vbo;
	unsigned int sly;
	unsigned int sluv;
} ipu_channel_parameter_t;

typedef struct ipu_res_info {
	int taskType;
	unsigned int inAddr0;
	unsigned int inAddr1;
	unsigned int outAddr0;
	unsigned int outAddr1;
	int inWidth;
	int inHeight;
	int outWidth;
	int outHeight;
	int xSplitParts;
	int ySplitParts;
	int stridelineIn;
	int stridelineOut;
	int uOffsetIn;
	int uOffsetOut;
	int inDataFormat;
	int outDataFormat;
} ipu_res_info_t;

typedef struct ipu_rot_info {
	int taskType;
	unsigned int inAddr0;
	unsigned int inAddr1;
	unsigned int outAddr0;
	unsigned int outAddr1;
	int inWidth;
	int inHeight;
	int outWidth;
	int outHeight;
	int stridelineIn;
	int stridelineOut;
	int uOffsetIn;
	int uOffsetOut;
	int inDataFormat;
	int outDataFormat;
	int HorizFlip;
	int VertFlip;
	int rotation;
} ipu_rot_info_t;

typedef struct display_device {
	unsigned int type;
	int width;
	int height;
} display_device_t;

enum icTaskType {
	PrP_ENC_TASK = 0,
	PrP_VF_TASK,
	PP_TASK,
	IC_CMB,
	IC_CSC1,
	IC_CSC2,
	IC_PP,
	IC_PRPENC,
	IC_PRPVF,
};

enum colorSpace {
	RGB = 0,
	YCbCr,
	RGB565,
	RGB666,
	RGB888,
	RGBA8888,
	YUV888,
	YUVA8888,
	GRAY,
};

enum dest {
	DMA_CH0 = 0,
	DMA_CH22,
	DMA_CH23,
	DMA_CH28,
};

enum tv_display_mode {
	TVNTSC = 0,
	TVPALM,
	TVPALN,
	TVPAL,
	TV720P60,
	TV720P50,
	TV720P30,
	TV720P25,
	TV720P24,
	TV1080I60,
	TV1080I50,
	TV1035I60,
	TV1080P30,
	TV1080P25,
	TV1080P24,
	TVNONE
};

typedef struct alpha_chan_params {
	unsigned int alphaChanBaseAddr;
	int alphaWidth;
	int alphaHeight;
	int alphaStrideline;
} alpha_chan_params_t;

typedef struct ic_comb_params {
	int taskType;
	unsigned int baseAddr;
	int width;
	int height;
	int alpha;
	int inDataformat;
	alpha_chan_params_t alphaChan;
} ic_comb_params_t;

typedef struct ic_csc_params {
	int taskType;
	int inFormat;
	int outFormat;
} ic_csc_params_t;

typedef struct ipu_task_params {
	int taskType;
	int resEnable;
	int rotEnable;
	ipu_res_info_t resInfo;
	ipu_rot_info_t rotInfo;
} ipu_task_params_t;

typedef struct dp_csc_param {
	int mode;
	int **coeff;
} dp_csc_param_t;

typedef struct dp_fg_param {
	int fgEnable;
	int opaque;
	int offsetVert;
	int offsetHoriz;
	int cursorEnable;
	int colorKeyEnable;
	int graphicSelect;
	int alphaMode;
} dp_fg_param_t;

typedef struct cam_caputure_params {
	int camMode;
	int camRate;
	int camInWidth;
	int camInHeight;
	int camOutWidth;
	int camOutHeight;
} cam_capture_params_t;

typedef struct dc_microcode {
	int addr;
	int stop;
	char *opcode;
	int lf;
	int af;
	int operand;
	int mapping;
	int waveform;
	int gluelogic;
	int sync;
} dc_microcode_t;

typedef struct di_sync_wave_gen {
	int runValue;
	int runResolution;
	int offsetValue;
	int offsetResolution;
	int cntAutoReload;
	int stepRepeat;
	int cntClrSel;
	int cntPolarityGenEn;
	int cntPolarityTrigSel;
	int cntPolarityClrSel;
	int cntUp;
	int cntDown;
} di_sync_wave_gen_t;

//common API functions for IPU
void ipu_write_field(unsigned int id_addr, unsigned int id_mask, unsigned int data);
void ipu_enable_display(void);
void ipu_disable_display(void);
void ipu_csi_config(int width, int height);

//dma API functions for IPU
void ipu_idmac_params_init(ipu_channel_parameter_t * ipu_channel_params_ptr);
void ipu_idmac_cpmem_param_update(int ch_number, int int_mode, char field_name[10], int data);
void ipu_idmac_interleaved_channel_config(ipu_channel_parameter_t ipu_channel_params);
void ipu_idmac_non_interleaved_channel_config(ipu_channel_parameter_t ipu_channel_params);
void ipu_idmac_cpmem_param_set(int ch_number, unsigned int id_addr,
							unsigned int id_mask, int sh_en, idmac_bpp_STC * idmac_bpp);
void ipu_idmac_channel_buf_ready(int channel, int buf);
void ipu_idmac_channel_buf_not_ready(int channel, int buf);
void ipu_idmac_channel_mode_sel(int channel, int double_buf_en);
void ipu_idmac_channel_enable(int channel, int enable);
int ipu_idmac_channel_busy(int channel);
int ipu_idmac_chan_cur_buff(int channel);
int ipu_idamc_chan_eof_int(int channel);
int ipu_idmac_chan_till_idle(int channel, int timeout);
int ipu_dmfc_fifo_allocate(int channel, int fifo_size, int burst_size, int offset_addr);
int ipu_smfc_fifo_allocate(int channel, int map, int burst_size);

/* processing API functions for IPU */
void ipu_ic_enable(int ic_enable, int irt_enable);
void ipu_ic_task_config(ipu_task_params_t task_params);
void ipu_ic_calc_resize_coeffs(unsigned int in_size, unsigned int out_size,
							unsigned int *resize_coeff, unsigned int *downsize_coeff);
int ipu_ic_config_resize_rate(char *task_type, unsigned int res_vert,
							unsigned int down_vert, unsigned int res_horiz,
							unsigned int down_horiz);
void ipu_ic_calc_vout_size(ipu_res_info_t * info, display_device_t disp_dev, int rotation,
						int full_screen_enable);
int ipu_ic_combine_config(ic_comb_params_t comb_params);
int ipu_ic_csc_config(int csc_index, ic_csc_params_t csc_params);
int ipu_ic_task_enable(int task_type, int task, int enable);
void ipu_dp_csc_config(int dp, dp_csc_param_t dp_csc_params, bool srm_mode_update);
void ipu_dp_fg_config(dp_fg_param_t foreground_params);
void ipu_dp_fg_config(dp_fg_param_t foreground_params);
void ipu_dc_microcode_config(dc_microcode_t microcode);
void ipu_dc_microcode_event(int channel, char event[8], int priority, int address);
int ipu_dc_map(int map, int format);
int ipu_dc_display_config(int disp_port, int type, int increment, int strideline);
int ipu_dc_write_channel_config(int dma_channel, int disp_port, int link_di_index,
								int field_mode_enable);

/* display API functions for IPU */
void ipu_di_sync_config(int di, int pointer, di_sync_wave_gen_t sync_wave_gen);
void ipu_di_pointer_config(int di, int pointer, int access, int component, int cst,
						int pt0, int pt1, int pt2, int pt3, int pt4, int pt5, int pt6);
void ipu_di_waveform_config(int di, int pointer, int set, int up, int down);
int ipu_di_bsclk_gen(int di, int division, int up, int down);
int ipu_di_screen_set(int di, int screen_height);
int ipu_di_general_set(int di, int line_prediction, int vsync_sel, int hsync_sel, int clk_sel);

void fastlogo_init(display_buffer_info_t *di);
void fastlogo_dma(void);
void fastlogo_dmfc(void);
void fastlogo_dc(void);
void fastlogo_di(void);

#endif
