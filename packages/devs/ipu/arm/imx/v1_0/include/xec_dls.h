/*
 * Copyright 2008 Freescale Semiconductor, Inc.
 *
 * All modifications are confidential and proprietary information
 * of Freescale Semiconductor, Inc. ALL RIGHTS RESERVED.
 *
 */
/*!
 * @file xec_dls.h
 *
 * @brief This file contains the XEC_LCD dls part declarations.
 *
 */
#ifndef __XEC_DLS_H__
#define __XEC_DLS_H__

#define SSIM_XSTEP 4
#define SSIM_YSTEP 4

/* default configures for xec dls algorithm */
#define XECDLS_frameStep		6
#define XECDLS_xStep				4
#define XECDLS_yStep				4
#define XECDLS_yMaxPrime		235
#define XECDLS_disTh				500	/*xxxx = xx.xx */
#define XECDLS_blDeltaMax		30
#define XECDLS_alphaMax		138	/* 149 */
#define XECDLS_yRangeMax		255
#define XECDLS_yRangeMin		0
#define XECDLS_FFilter_Step	3
#define XECDLS_ALPHA_Step	109	/* gamma=2. or 108 if gamma=2.20 */
#define XECDLS_BLDelay			20
#define XECDLS_BLStep			3

enum XECDLS_FRAME_FORMAT {
	XECDLS_YV12 = 0,
	XECDLS_RGB888,
	XECDLS_YUYV,
};

struct xecDlsFrameInfo {
	unsigned char *framePointer;
	int width;
	int height;
	int rectTop;
	int rectBottom;
	int rectLeft;
	int rectRight;
	int ambientBacklight;
	enum XECDLS_FRAME_FORMAT frameFormat;
	int bytesPerPixel;
	int index;
};

struct xecDlsConfig {
	int yMaxPrime;
	int yRangeMin;
	int yRangeMax;
	int xStep;
	int yStep;
	int disThreshold;
	int alphaMax;
	int blDeltaMax;
	int frameStep;
	int blStep;
	int blDelay;
};

struct xecDlsOutput {
	unsigned short pendingAlpha;
	int pendingBLChange;
	int pending;
};
struct xec_dls_params{
	int prevCurrent;
	int curCurrent;
	int diffCurrent;
	int prevAlpha;
	int curAlpha;
	int diffAlpha;
	int stepSize;
	int stepNum;
	int stepCounter;
	int lastStep;
};

typedef struct image_block{
	unsigned int startAddr;
	int blockXSize;
	int blockYSize;
	int xStride;
	int yStride;
}image_block_t;

void xec_dls_core_init(void);
void xec_dls_core(struct xecDlsFrameInfo g_xecDlsCoreFrameInfo);
void xec_dls_stream_init(void);
int xec_dls_perform(unsigned char *frame, int xSize, int ySize,
			   int pixelformat, int index);
#endif

