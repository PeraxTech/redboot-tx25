//==========================================================================
//
//      IPU_DI.c
//
//      common functions definitions for IPU modules operation
//
//==========================================================================
//#####DESCRIPTIONBEGIN####
//
// Author(s):       Ray Sun <Yanfei.Sun@freescale.com>
// Create Date: 2008-07-31
//
//####DESCRIPTIONEND####
//
//==========================================================================

#include <cyg/io/ipu_common.h>
#include <cyg/hal/hal_soc.h>    // Hardware definitions

/*
* this function is used to config the waveform generator in the DI
*/
void ipu_di_sync_config(int di, int pointer, di_sync_wave_gen_t sync_waveform_gen)
{
	ipu_write_field(DI_SWGEN0_RUN_VALUE_M1(di, pointer), sync_waveform_gen.runValue);
	ipu_write_field(DI_SWGEN0_RUN_RESOL(di, pointer), sync_waveform_gen.runResolution);
	ipu_write_field(DI_SWGEN0_OFFSET_VALUE(di, pointer), sync_waveform_gen.offsetValue);
	ipu_write_field(DI_SWGEN0_OFFSET_RESOL(di, pointer), sync_waveform_gen.offsetResolution);
	ipu_write_field(DI_SWGEN1_CNT_POL_GEN_EN(di, pointer), sync_waveform_gen.cntPolarityGenEn);
	ipu_write_field(DI_SWGEN1_CNT_AUTOLOAD(di, pointer), sync_waveform_gen.cntAutoReload);
	ipu_write_field(DI_SWGEN1_CNT_CLR_SEL(di, pointer), sync_waveform_gen.cntClrSel);
	ipu_write_field(DI_SWGEN1_CNT_DOW(di, pointer), sync_waveform_gen.cntDown);
	ipu_write_field(DI_SWGEN1_CNT_POL_TRIG_SEL(di, pointer), sync_waveform_gen.cntPolarityTrigSel);
	ipu_write_field(DI_SWGEN1_CNT_POL_CLR_SEL(di, pointer), sync_waveform_gen.cntPolarityClrSel);
	ipu_write_field(DI_SWGEN1_CNT_CNT_UP(di, pointer), sync_waveform_gen.cntUp);
	ipu_write_field(DI_STEP_RPT(di, pointer), sync_waveform_gen.stepRepeat);

	return;
}

void ipu_di_pointer_config(int di, int pointer, int access, int component, int cst, int pt0,
						int pt1, int pt2, int pt3, int pt4, int pt5, int pt6)
{
	unsigned int regVal = 0;
	regVal =
		(access << 24) | (component << 16) | (cst << 14) | (pt6 << 12) | (pt5 << 10) | (pt4 << 8) |
		(pt3 << 6) | (pt2 << 4) | (pt1 << 2) | pt0;

	if (di == 0) {
		writel(regVal, IPU_CTRL_BASE_ADDR + IPU_DI0_DW_GEN_0__ADDR + pointer * 4);
	} else {
		writel(regVal, IPU_CTRL_BASE_ADDR + IPU_DI1_DW_GEN_0__ADDR + pointer * 4);
	}
	return;
}

void ipu_di_waveform_config(int di, int pointer, int set, int up, int down)
{
	ipu_write_field(DI_WAVESET_UP(di, pointer, set), up);
	ipu_write_field(DI_WAVESET_DOWN(di, pointer, set), down);

	return;
}

int ipu_di_bsclk_gen(int di, int division, int up, int down)
{
	switch (di) {
	case 0:
		ipu_write_field(IPU_DI0_BS_CLKGEN0__DI0_DISP_CLK_OFFSET, 0);
		ipu_write_field(IPU_DI0_BS_CLKGEN0__DI0_DISP_CLK_PERIOD, division);
		ipu_write_field(IPU_DI0_BS_CLKGEN1__DI0_DISP_CLK_DOWN, down);
		ipu_write_field(IPU_DI0_BS_CLKGEN1__DI0_DISP_CLK_UP, up);
		break;

	case 1:
		ipu_write_field(IPU_DI1_BS_CLKGEN0__DI1_DISP_CLK_OFFSET, 0);
		ipu_write_field(IPU_DI1_BS_CLKGEN0__DI1_DISP_CLK_PERIOD, division);
		ipu_write_field(IPU_DI1_BS_CLKGEN1__DI1_DISP_CLK_DOWN, down);
		ipu_write_field(IPU_DI1_BS_CLKGEN1__DI1_DISP_CLK_UP, up);
		break;

	default:
		ERRDP("Wrong di pointer!\n");
		return -1;
	}
	return 0;
}

int ipu_di_screen_set(int di, int screen_height)
{
	switch (di) {
	case 0:
		ipu_write_field(IPU_DI0_SCR_CONF__DI0_SCREEN_HEIGHT, screen_height);
		break;

	case 1:
		ipu_write_field(IPU_DI1_SCR_CONF__DI1_SCREEN_HEIGHT, screen_height);
		break;

	default:
		ERRDP("Wrong di pointer!\n");
		return -1;
	}
	return 0;
}

int ipu_di_general_set(int di, int line_prediction, int vsync_sel, int hsync_sel, int clk_sel)
{
	switch (di) {
	case 0:
		ipu_write_field(IPU_DI0_SYNC_AS_GEN__DI0_SYNC_START, line_prediction);
		ipu_write_field(IPU_DI0_SYNC_AS_GEN__DI0_VSYNC_SEL, vsync_sel);
		ipu_write_field(IPU_DI0_GENERAL__DI0_CLK_EXT, clk_sel);

		ipu_write_field(IPU_DI0_GENERAL__DI0_POLARITY_DISP_CLK, 1);
		ipu_write_field(IPU_DI0_GENERAL__DI0_POLARITY_3, 0);    //HSYNC polarity, active low
		ipu_write_field(IPU_DI0_GENERAL__DI0_POLARITY_2, 0);    //VSYNC polarity, active low
		ipu_write_field(IPU_DI0_POL__DI0_DRDY_POLARITY_15, 1);  //VIDEO_DATA_EN polarity, active hign

		/* release ipu DI0 counter */
		ipu_write_field(IPU_IPU_DISP_GEN__DI0_COUNTER_RELEASE, 1);
		break;

	case 1:
		ipu_write_field(IPU_DI1_SYNC_AS_GEN__DI1_SYNC_START, line_prediction);
		ipu_write_field(IPU_DI1_SYNC_AS_GEN__DI1_VSYNC_SEL, vsync_sel);
		ipu_write_field(IPU_DI1_GENERAL__DI1_DISP_Y_SEL, hsync_sel);
		ipu_write_field(IPU_DI1_GENERAL__DI1_CLK_EXT, clk_sel);

		ipu_write_field(IPU_DI1_GENERAL__DI1_POLARITY_DISP_CLK, 0);
		ipu_write_field(IPU_DI1_GENERAL__DI1_POLARITY_8, 1);
		ipu_write_field(IPU_DI1_GENERAL__DI1_POLARITY_5, 1);
		ipu_write_field(IPU_DI1_GENERAL__DI1_POLARITY_3, 1);    //HSYNC POLARITY
		ipu_write_field(IPU_DI1_GENERAL__DI1_POLARITY_2, 1);    //VSYNC POLARITY
		ipu_write_field(IPU_DI1_POL__DI1_DRDY_POLARITY_15, 1);
		/* release ipu DI1 counter */
		ipu_write_field(IPU_IPU_DISP_GEN__DI1_COUNTER_RELEASE, 1);
		break;

	default:
		ERRDP("Wrong di pointer!\n");
		return -1;
	}
	return 0;
}
