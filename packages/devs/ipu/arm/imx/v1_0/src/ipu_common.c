/***************************************************************************
*
*						      IPU_COMMON.C
*
* Copyright 2005-2006 by Freescale Semiconductor, Inc.
* All modifications are confidential and proprietary information
* of Freescale Semiconductor, Inc. ALL RIGHTS RESERVED.
*
***************************************************************************
*
* Author(s)		:		Ray Sun-B17777 <Yanfei.Sun@freescale.com>
* Create Date	:		2008-11-10
* Description	:		common functions definition for IPU API.
*
***************************************************************************/
#include <cyg/io/ipu_common.h>
#ifdef CYGPKG_REDBOOT
#include <redboot.h>
#endif
#include <cyg/io/flash.h>
#ifdef CYGSEM_REDBOOT_FLASH_CONFIG
#include <flash_config.h>
#endif
#ifdef CYGOPT_REDBOOT_FIS
#include <fis.h>
#endif
#include CYGBLD_HAL_PLATFORM_H

/*
* write bit fields of special IPU regs
*/
void ipu_write_field(unsigned int id_addr, unsigned int id_mask, unsigned int data)
{
	unsigned int rdata;

	id_addr += IPU_CTRL_BASE_ADDR;
	rdata = readl(id_addr);
	rdata &= ~id_mask;
	rdata |= (data * (id_mask & -id_mask)) & id_mask;
	writel(rdata, id_addr);
}

/*
*	enable ipu display
*/
void ipu_enable_display(void)
{
	//enable DI0 (display interface 1)
	ipu_write_field(IPU_IPU_CONF__DP_EN, 1);
	ipu_write_field(IPU_IPU_CONF__DC_EN, 1);
	ipu_write_field(IPU_IPU_CONF__DMFC_EN, 1);
	ipu_write_field(IPU_IPU_CONF__DI0_EN, 1);
	ipu_write_field(IPU_IPU_CONF__DI1_EN, 1);
#ifdef CYGPKG_HAL_ARM_MX51_3STACK
	ipu_write_field(IPU_IPU_CONF__CSI1_EN, 1);
#endif
}

/*
*	disable ipu display
*/
void ipu_disable_display(void)
{
	ipu_write_field(IPU_IPU_CONF__DI0_EN, 0);
	ipu_write_field(IPU_IPU_CONF__DI1_EN, 0);
	ipu_write_field(IPU_IPU_CONF__DP_EN, 0);
	ipu_write_field(IPU_IPU_CONF__DC_EN, 0);
	ipu_write_field(IPU_IPU_CONF__DMFC_EN, 0);
	ipu_write_field(IPU_IPU_CONF__IC_EN, 0);
#ifdef CYGPKG_HAL_ARM_MX51_3STACK
	ipu_write_field(IPU_IPU_CONF__CSI0_EN, 0);
	ipu_write_field(IPU_IPU_CONF__CSI1_EN, 0);
	ipu_write_field(IPU_IPU_CONF__SMFC_EN, 0);
#endif
	ipu_write_field(IPU_IC_CONF__PP_EN, 0);
	ipu_write_field(IPU_IC_CONF__PRPVF_EN, 0);
	ipu_write_field(IPU_IC_CONF__PRPENC_EN, 0);
}

#ifdef CYGHWR_MX51_LCD_LOGO
static display_buffer_info_t display_buffer;

/*!
* load the logo from nand flash to memory.
*/
static bool do_logo_load(void)
{
	void *fis_addr;
	int ret = 0xFF;
	void *err_addr;
	unsigned int logo_size;
	struct fis_image_desc *img;

	/* Read the logo from storage media */
	if ((img = fis_lookup("logo", NULL)) == NULL) {
		diag_printf("No logo partition found in the fis table, logo not loaded\n");
		return false;
	}

	fis_addr = (void *)img->flash_base;
	logo_size = img->size;
	ret = FLASH_READ(fis_addr, DISPLAY_BUFFER_ADDR, logo_size, &err_addr);
	if (ret != 0) {
		diag_printf("Loading logo from FLASH to MEMORY failed. error code: %d", ret);
	}
	return true;
}

/*!
* this function is used to reset ipu by SRC(system reset controller)
* the return value should be negative if resetting timeout
*/
#define IPU_RESET	(1 << 3)
static int ipu_sw_reset(int timeout)
{
	int tmpVal;

	tmpVal = readl(SRC_BASE_ADDR);
	writel(tmpVal | IPU_RESET, SRC_BASE_ADDR);
	while (timeout > 0) {
		if (!(readl(SRC_BASE_ADDR) & IPU_RESET))
			return 0;
		timeout--;
	}
	diag_printf("Error: ipu software reset timed out\n");
	return -1;
}

static void redboot_fastlogo_display(void)
{
	bool fastlogo_feature_enable;
	int ok;

	ok = CYGACC_CALL_IF_FLASH_CFG_OP(CYGNUM_CALL_IF_FLASH_CFG_GET,
									"fastlogo_enable", &fastlogo_feature_enable, CONFIG_BOOL);

	if (ok && fastlogo_feature_enable) {
		display_buffer.startAddr = (CYG_ADDRESS)DISPLAY_BUFFER_ADDR;
		display_buffer.width = 640;
		display_buffer.height = 480;
		display_buffer.dataFormat = RGB565;
		display_buffer.bpp = 16; // bit per pixel
		display_buffer.channel = 28;

		ipu_sw_reset(0x10000);
		if (!do_logo_load()) {
			return;
		}
		mxc_ipu_iomux_config();
		//lcd_backlit_on();
		//lcd_config();
		fastlogo_init(&display_buffer);
		fastlogo_dma();
		fastlogo_dmfc();
		fastlogo_dc();
		fastlogo_di();
		ipu_enable_display();
		ipu_idmac_channel_buf_ready(display_buffer.channel, 0);
	}
}

#ifdef CYGPKG_REDBOOT
RedBoot_init(redboot_fastlogo_display, RedBoot_INIT_SECOND);
#endif

RedBoot_config_option("Enable fast logo display at boot",
					fastlogo_enable,
					ALWAYS_ENABLED, true,
					CONFIG_BOOL,
					false
	);
#endif
