//==========================================================================
//
//      IPU_COMMON.c
//
//      common functions definitions for IPU modules operation
//
//==========================================================================
//#####DESCRIPTIONBEGIN####
//
// Author(s):       Ray Sun <Yanfei.Sun@freescale.com>
// Create Date: 2008-07-31
//
//####DESCRIPTIONEND####
//
//==========================================================================

#include <string.h>
#include <cyg/io/ipu_common.h>

void ipu_idmac_params_init(ipu_channel_parameter_t *ipu_channel_params_ptr)
{
	memset(ipu_channel_params_ptr, 0, sizeof(ipu_channel_parameter_t));
}

/*
* config the dma channel to be interleaved mode
*/
void ipu_idmac_interleaved_channel_config(ipu_channel_parameter_t ipu_channel_params)
{
	int w0_d0 = 0, w0_d1 = 0, w0_d2 = 0, w0_d3 = 0, w0_d4 = 0, w1_d0 = 0, w1_d1 = 0, w1_d2 =
		0, w1_d3 = 0, w1_d4 = 0;

	w0_d0 = ipu_channel_params.xb << 19 | ipu_channel_params.yv << 10 | ipu_channel_params.xv;
	w0_d1 =
		ipu_channel_params.sy << 26 | ipu_channel_params.sx << 14 | ipu_channel_params.
		cf << 13 | ipu_channel_params.nsb_b << 12 | ipu_channel_params.yb;
	w0_d2 =
		ipu_channel_params.sm << 22 | ipu_channel_params.sdx << 15 | ipu_channel_params.
		ns << 5 | ipu_channel_params.sy >> 6;
	w0_d3 =
		ipu_channel_params.fw << 29 | ipu_channel_params.cae << 28 | ipu_channel_params.
		cap << 27 | ipu_channel_params.the << 26 | ipu_channel_params.vf << 25 | ipu_channel_params.
		hf << 24 | ipu_channel_params.rot << 23 | ipu_channel_params.bm << 21 | ipu_channel_params.
		bndm << 18 | ipu_channel_params.so << 17 | ipu_channel_params.
		dim << 16 | ipu_channel_params.dec_sel << 14 | ipu_channel_params.
		bpp << 11 | ipu_channel_params.sdry << 10 | ipu_channel_params.
		sdrx << 9 | ipu_channel_params.sdy << 2 | ipu_channel_params.sce << 1 | ipu_channel_params.
		scc;
	w0_d4 = ipu_channel_params.fh << 10 | ipu_channel_params.fw >> 3;

	w1_d0 = ipu_channel_params.eba1 << 29 | ipu_channel_params.eba0;
	w1_d1 = ipu_channel_params.ilo << 26 | ipu_channel_params.eba1 >> 3;
	w1_d2 =
		ipu_channel_params.th << 31 | ipu_channel_params.id << 29 | ipu_channel_params.
		albm << 26 | ipu_channel_params.alu << 25 | ipu_channel_params.
		pfs << 21 | ipu_channel_params.npb << 14 | ipu_channel_params.ilo >> 6;
	w1_d3 =
		ipu_channel_params.wid3 << 29 | ipu_channel_params.wid2 << 26 | ipu_channel_params.
		wid1 << 23 | ipu_channel_params.wid0 << 20 | ipu_channel_params.
		sl << 6 | ipu_channel_params.th >> 1;
	w1_d4 =
		ipu_channel_params.ofs3 << 15 | ipu_channel_params.ofs2 << 10 | ipu_channel_params.
		ofs1 << 5 | ipu_channel_params.ofs0;

	/* config the cpmem */
	writel(w0_d0,
		IPU_CTRL_BASE_ADDR + CPMEM_WORD0_DATA0_INT__ADDR + (ipu_channel_params.channel << 6));
	writel(w0_d1,
		IPU_CTRL_BASE_ADDR + CPMEM_WORD0_DATA1_INT__ADDR + (ipu_channel_params.channel << 6));
	writel(w0_d2,
		IPU_CTRL_BASE_ADDR + CPMEM_WORD0_DATA2_INT__ADDR + (ipu_channel_params.channel << 6));
	writel(w0_d3,
		IPU_CTRL_BASE_ADDR + CPMEM_WORD0_DATA3_INT__ADDR + (ipu_channel_params.channel << 6));
	writel(w0_d4,
		IPU_CTRL_BASE_ADDR + CPMEM_WORD0_DATA4_INT__ADDR + (ipu_channel_params.channel << 6));

	writel(w1_d0,
		IPU_CTRL_BASE_ADDR + CPMEM_WORD1_DATA0_INT__ADDR + (ipu_channel_params.channel << 6));
	writel(w1_d1,
		IPU_CTRL_BASE_ADDR + CPMEM_WORD1_DATA1_INT__ADDR + (ipu_channel_params.channel << 6));
	writel(w1_d2,
		IPU_CTRL_BASE_ADDR + CPMEM_WORD1_DATA2_INT__ADDR + (ipu_channel_params.channel << 6));
	writel(w1_d3,
		IPU_CTRL_BASE_ADDR + CPMEM_WORD1_DATA3_INT__ADDR + (ipu_channel_params.channel << 6));
	writel(w1_d4,
		IPU_CTRL_BASE_ADDR + CPMEM_WORD1_DATA4_INT__ADDR + (ipu_channel_params.channel << 6));
}

void ipu_idmac_channel_buf_ready(int channel, int buf)
{
	int idx = channel / 32;
	int offset = channel % 32;
	if (idx) {
		if (buf) {
			ipu_write_field(IPU_IPU_GPR__IPU_CH_BUF1_RDY1_CLR, 0);
			ipu_write_field(IPU_IPU_CH_BUF1_RDY1__ADDR, 1 << offset, 1);
		} else {
			ipu_write_field(IPU_IPU_GPR__IPU_CH_BUF0_RDY1_CLR, 0);
			ipu_write_field(IPU_IPU_CH_BUF0_RDY1__ADDR, 1 << offset, 1);
		}
	} else {
		if (buf) {
			ipu_write_field(IPU_IPU_GPR__IPU_CH_BUF1_RDY0_CLR, 0);
			ipu_write_field(IPU_IPU_CH_BUF1_RDY0__ADDR, 1 << offset, 1);
		} else {
			ipu_write_field(IPU_IPU_GPR__IPU_CH_BUF0_RDY0_CLR, 0);
			ipu_write_field(IPU_IPU_CH_BUF0_RDY0__ADDR, 1 << offset, 1);
		}
	}
}

void ipu_idmac_channel_mode_sel(int channel, int double_buf_en)
{
	int idx = channel / 32;
	int offset = channel % 32;
	ipu_write_field(IPU_IPU_CH_DB_MODE_SEL_0__ADDR + idx * 4, 1 << offset, double_buf_en);
}

void ipu_idmac_channel_enable(int channel, int enable)
{
	int idx = channel / 32;
	int offset = channel % 32;
	ipu_write_field(IPU_IDMAC_CH_EN_1__ADDR + idx * 4, 1 << offset, enable);
}

int ipu_idmac_channel_busy(int channel)
{
	int idx, offset;
	idx = channel / 32;
	offset = channel % 32;
	return ((readl(IPU_CTRL_BASE_ADDR + IPU_IDMAC_CH_BUSY_1__ADDR + 4 * idx) & (1 << offset)) >>
			offset);
}

int ipu_idmac_chan_cur_buff(int channel)
{
	int idx, offset;
	idx = channel / 32;
	offset = channel % 32;
	return ((readl(IPU_CTRL_BASE_ADDR + IPU_IPU_CUR_BUF_0__ADDR + 4 * idx) & (1 << offset)) >>
			offset);
}

int ipu_idamc_chan_eof_int(int channel)
{
	int idx, offset;
	idx = channel / 32;
	offset = channel % 32;
	return ((readl(IPU_CTRL_BASE_ADDR + IPU_IPU_INT_STAT_1__ADDR + 4 * idx) & (1 << offset)) >>
			offset);
}

int ipu_idmac_chan_till_idle(int channel, int timeout)
{
	int i = 0;
	unsigned int chanBusy;

	while (i < timeout) {
		chanBusy = ipu_idmac_channel_busy(channel);
		if (!chanBusy) {
//          diag_printf("\ncount cycles:%d\n",i);
			__asm("nop");
			return 0;
		}
		i++;
	}
	ERRDP("can not get channel %d idle state\n", channel);
	return -1;
}

/*
* allocate dmfc fifo for ipu display channel
*/
int ipu_dmfc_fifo_allocate(int channel, int fifo_size, int burst_size, int offset_addr)
{

	if (fifo_size > 7 || fifo_size < 0) {
		ERRDP("FIFO size is wrong! range from 0 to 7.\n");
		return -1;
	}
	if (burst_size > 3 || burst_size < 0) {
		ERRDP("Burst size is wrong! range from 0 to 3.\n");
		return -1;
	}
	if (offset_addr < 0 || offset_addr > 7) {
		ERRDP("Start addr of FIFO is wrong! range from 0 to 7.\n");
		return -1;
	}
	switch (channel) {
	case 28:
		ipu_write_field(IPU_DMFC_WR_CHAN__DMFC_FIFO_SIZE_1, fifo_size);
		ipu_write_field(IPU_DMFC_WR_CHAN__DMFC_BURST_SIZE_1, burst_size);
		ipu_write_field(IPU_DMFC_WR_CHAN__DMFC_ST_ADDR_1, offset_addr);
		ipu_write_field(IPU_DMFC_WR_CHAN_DEF__DMFC_WM_CLR_1, 0);
		ipu_write_field(IPU_DMFC_WR_CHAN_DEF__DMFC_WM_SET_1, 0);
		ipu_write_field(IPU_DMFC_WR_CHAN_DEF__DMFC_WM_EN_1, 0);
		ipu_write_field(IPU_DMFC_GENERAL1__WAIT4EOT_1, 0);
		break;

	case 23:
		ipu_write_field(IPU_DMFC_DP_CHAN__DMFC_FIFO_SIZE_5B, fifo_size);
		ipu_write_field(IPU_DMFC_DP_CHAN__DMFC_BURST_SIZE_5B, burst_size);
		ipu_write_field(IPU_DMFC_DP_CHAN__DMFC_ST_ADDR_5B, offset_addr);
		ipu_write_field(IPU_DMFC_DP_CHAN_DEF__DMFC_WM_CLR_5B, 0);
		ipu_write_field(IPU_DMFC_DP_CHAN_DEF__DMFC_WM_SET_5B, 0);
		ipu_write_field(IPU_DMFC_DP_CHAN_DEF__DMFC_WM_EN_5B, 0);
		ipu_write_field(IPU_DMFC_GENERAL1__WAIT4EOT_5B, 0);
		break;

	case 27:
		ipu_write_field(IPU_DMFC_DP_CHAN__DMFC_FIFO_SIZE_5F, fifo_size);
		ipu_write_field(IPU_DMFC_DP_CHAN__DMFC_BURST_SIZE_5F, burst_size);
		ipu_write_field(IPU_DMFC_DP_CHAN__DMFC_ST_ADDR_5F, offset_addr);
		ipu_write_field(IPU_DMFC_DP_CHAN_DEF__DMFC_WM_CLR_5F, 0);
		ipu_write_field(IPU_DMFC_DP_CHAN_DEF__DMFC_WM_SET_5F, 0);
		ipu_write_field(IPU_DMFC_DP_CHAN_DEF__DMFC_WM_EN_5F, 0);
		ipu_write_field(IPU_DMFC_GENERAL1__WAIT4EOT_5F, 0);
		break;
	default:
		ERRDP("Channel selection error!!\n");
		return -1;
	}
	return 0;
}
