//==========================================================================
//
//      IPU_COMMON.c
//
//      common functions definitions for IPU modules operation
//
//==========================================================================
//#####DESCRIPTIONBEGIN####
//
// Author(s):       Ray Sun <Yanfei.Sun@freescale.com>
// Create Date: 2008-07-31
//
//####DESCRIPTIONEND####
//
//==========================================================================

#include <string.h>
#include <cyg/io/ipu_common.h>
#include <cyg/io/xec_dls.h>

extern struct xec_dls_params xecDlsParams;

void ipu_ic_enable(int ic_enable, int irt_enable)
{
	ipu_write_field(IPU_IPU_CONF__IC_EN, ic_enable);
	ipu_write_field(IPU_IPU_CONF__IRT_EN, irt_enable);
}

/*
* this function is used to config the rotation/resizing task perform by the Image Converter
*/
void ipu_ic_task_config(ipu_task_params_t task_params)
{
	int resCoff, downsCoff;

	switch (task_params.taskType) {
	case PrP_ENC_TASK:
		ipu_write_field(IPU_IC_CONF__PRPENC_EN, 0);
		ipu_write_field(IPU_IC_CONF__RWS_EN, 1);
		ipu_write_field(IPU_IPU_FS_PROC_FLOW1__ENC_IN_VALID, 1);
		ipu_write_field(IPU_IC_IDMAC_1__CB0_BURST_16, 1);   // set to 16bps
		ipu_write_field(IPU_IC_IDMAC_1__CB6_BURST_16, 1);
		ipu_write_field(IPU_IC_CONF__PRPENC_EN, 1);
		ipu_write_field(IPU_IC_CONF__PRPENC_ROT_EN, task_params.rotEnable);
		/* set rotation task */
		if (task_params.rotEnable) {
			ipu_write_field(IPU_IC_IDMAC_1__T1_FLIP_LR, task_params.rotInfo.HorizFlip);
			ipu_write_field(IPU_IC_IDMAC_1__T1_FLIP_UD, task_params.rotInfo.VertFlip);
			ipu_write_field(IPU_IC_IDMAC_1__T1_ROT, task_params.rotInfo.rotation);
		}
		/* set resizing task */
		if (task_params.resEnable) {
			ipu_write_field(IPU_IC_IDMAC_2__T1_FR_HEIGHT, task_params.resInfo.outHeight - 1);
			ipu_write_field(IPU_IC_IDMAC_3__T1_FR_WIDTH, task_params.resInfo.outWidth - 1);

			ipu_ic_calc_resize_coeffs(task_params.resInfo.inWidth, task_params.resInfo.outWidth,
									&resCoff, &downsCoff);
			ipu_write_field(IPU_IC_PRP_ENC_RSC__PRPENC_DS_R_H, downsCoff);
			ipu_write_field(IPU_IC_PRP_ENC_RSC__PRPENC_RS_R_H, resCoff);
			ipu_ic_calc_resize_coeffs(task_params.resInfo.inHeight, task_params.resInfo.outHeight,
									&resCoff, &downsCoff);

			ipu_write_field(IPU_IC_PRP_ENC_RSC__PRPENC_DS_R_V, downsCoff);
			ipu_write_field(IPU_IC_PRP_ENC_RSC__PRPENC_RS_R_V, resCoff);
		}
		ipu_write_field(IPU_IC_CONF__PRPENC_EN, 1);
		break;

	case PrP_VF_TASK:
		ipu_write_field(IPU_IC_CONF__PRPVF_EN, 0);
		ipu_write_field(IPU_IC_CONF__RWS_EN, 1);
		ipu_write_field(IPU_IPU_FS_PROC_FLOW1__VF_IN_VALID, 1);
		ipu_write_field(IPU_IC_IDMAC_1__CB1_BURST_16, 1);   // set to 16bps
		ipu_write_field(IPU_IC_IDMAC_1__CB6_BURST_16, 1);
		ipu_write_field(IPU_IC_CONF__PRPVF_EN, 1);
		ipu_write_field(IPU_IC_CONF__PRPVF_ROT_EN, task_params.rotEnable);
		// set rotation
		if (task_params.rotEnable) {
			ipu_write_field(IPU_IC_IDMAC_1__T2_FLIP_LR, task_params.rotInfo.HorizFlip);
			ipu_write_field(IPU_IC_IDMAC_1__T2_FLIP_UD, task_params.rotInfo.VertFlip);
			ipu_write_field(IPU_IC_IDMAC_1__T2_ROT, task_params.rotInfo.rotation);
		}
		// set resizing
		if (task_params.resEnable) {
			ipu_write_field(IPU_IC_IDMAC_2__T2_FR_HEIGHT, task_params.resInfo.outHeight - 1);
			ipu_write_field(IPU_IC_IDMAC_3__T2_FR_WIDTH, task_params.resInfo.outWidth - 1);

			ipu_ic_calc_resize_coeffs(task_params.resInfo.inWidth, task_params.resInfo.outWidth,
									&resCoff, &downsCoff);
			ipu_write_field(IPU_IC_PRP_VF_RSC__PRPVF_DS_R_H, downsCoff);
			ipu_write_field(IPU_IC_PRP_VF_RSC__PRPVF_RS_R_H, resCoff);
			ipu_ic_calc_resize_coeffs(task_params.resInfo.inHeight, task_params.resInfo.outHeight,
									&resCoff, &downsCoff);
			ipu_write_field(IPU_IC_PRP_VF_RSC__PRPVF_DS_R_V, downsCoff);
			ipu_write_field(IPU_IC_PRP_VF_RSC__PRPVF_RS_R_V, resCoff);
		}
		ipu_write_field(IPU_IC_CONF__PRPVF_EN, 1);
		break;

	case PP_TASK:
		ipu_write_field(IPU_IC_CONF__PP_EN, 0);
		ipu_write_field(IPU_IC_IDMAC_1__CB2_BURST_16, 1);   // set to 16bps
		ipu_write_field(IPU_IC_IDMAC_1__CB5_BURST_16, 1);
		ipu_write_field(IPU_IC_CONF__PP_EN, 1);
		ipu_write_field(IPU_IC_CONF__PP_ROT_EN, task_params.rotEnable);
		// set rotation
		if (task_params.rotEnable) {
			ipu_write_field(IPU_IC_IDMAC_1__T3_FLIP_LR, task_params.rotInfo.HorizFlip);
			ipu_write_field(IPU_IC_IDMAC_1__T3_FLIP_UD, task_params.rotInfo.VertFlip);
			ipu_write_field(IPU_IC_IDMAC_1__T3_ROT, task_params.rotInfo.rotation);
		}
		// set resizing
		if (task_params.resEnable) {
			ipu_write_field(IPU_IC_IDMAC_2__T3_FR_HEIGHT, task_params.resInfo.outHeight - 1);
			ipu_write_field(IPU_IC_IDMAC_3__T3_FR_WIDTH, task_params.resInfo.outWidth - 1);

			ipu_ic_calc_resize_coeffs(task_params.resInfo.inWidth, task_params.resInfo.outWidth,
									&resCoff, &downsCoff);

			ipu_write_field(IPU_IC_PP_RSC__PP_DS_R_H, downsCoff);
			ipu_write_field(IPU_IC_PP_RSC__PP_RS_R_H, resCoff);
			ipu_ic_calc_resize_coeffs(task_params.resInfo.inHeight, task_params.resInfo.outHeight,
									&resCoff, &downsCoff);

			ipu_write_field(IPU_IC_PP_RSC__PP_DS_R_V, downsCoff);
			ipu_write_field(IPU_IC_PP_RSC__PP_RS_R_V, resCoff); // FROM (1536/2 -1)->479 *8192 = 13117
		}
		ipu_write_field(IPU_IC_CONF__PP_EN, 1);
		break;
	default:
		ERRDP("Task type is wrong, IC task configuration failed\n");
	}
}

/*
* this function is used to calculate the params for resizing
*/
void ipu_ic_calc_resize_coeffs(unsigned int in_size, unsigned int out_size,
							unsigned int *resize_coeff, unsigned int *downsize_coeff)
{
	unsigned int tempSize;
	unsigned int tempDownsize;

	/* Cannot downsize more than 8:1 */
	if ((out_size << 3) < in_size)
		return;

	/* compute downsizing coefficient */
	tempDownsize = 0;
	tempSize = in_size;
	while ((tempSize >= out_size * 2) && (tempDownsize < 2)) {
		tempSize >>= 1;
		tempDownsize++;
	}
	*downsize_coeff = tempDownsize;

	/* compute resizing coefficient using the following equation:
	   resizeCoeff = M*(SI -1)/(SO - 1)
	   where M = 2^13, SI - input size, SO - output size    */
	*resize_coeff = (8192L * (tempSize - 1)) / (out_size - 1);
	if (*resize_coeff >= 16384L) {
		ERRDP("Overflow on resize coeff.\n");
		*resize_coeff = 0x3FFF;
	}
}

/*
* this function is used to set the resizing parameters
*/
int ipu_ic_config_resize_rate(char *task_type, unsigned int res_vert, unsigned int down_vert,
							unsigned int res_horiz, unsigned int down_horiz)
{
	unsigned int val;
	val = (down_vert << 30) | (res_vert << 16) | (down_horiz << 14) | (res_horiz);

	if (!strcmp(task_type, "PPTASK")) {
		DP("Post Processing Task!\n");
		writel(val, IPU_CTRL_BASE_ADDR + IPU_IC_PP_RSC__ADDR);
	} else if (!strcmp(task_type, "VFTASK")) {
		DP("View Finder Task!\n");
		writel(val, IPU_CTRL_BASE_ADDR + IPU_IC_PRP_VF_RSC__ADDR);
	} else if (!strcmp(task_type, "PrPTASK")) {
		DP("Pre Processing Task!\n");
		writel(val, IPU_CTRL_BASE_ADDR + IPU_IC_PRP_ENC_RSC__ADDR);
	} else {
		ERRDP("Task type is not defined!\n");
		return -1;
	}
	return 0;
}

/*
* this function is used to calculate the output size for IC resizing task
*/
void ipu_ic_calc_vout_size(ipu_res_info_t * info, display_device_t disp_device, int rotation,
						int full_screen_enable)
{
	float coffHeight, coffWidth;

	/* if rotation is enabled, swap the width and height */
	if (rotation) {
		coffWidth = (float)(disp_device.height) / info->inWidth;
		coffHeight = (float)(disp_device.width) / info->inHeight;
	} else {
		coffWidth = (float)(disp_device.width) / info->inWidth;
		coffHeight = (float)(disp_device.height) / info->inHeight;
	}
	/* the resizing ratio should be the same in both width and height */
	if (coffWidth >= coffHeight) {
		info->outWidth = info->inWidth * coffHeight;
		info->outHeight = info->inHeight * coffHeight;
	} else {
		info->outWidth = info->inWidth * coffWidth;
		info->outHeight = info->inHeight * coffWidth;
	}

	if (full_screen_enable) {
		if(rotation) {
			info->outWidth = disp_device.height;
			info->outHeight = disp_device.width;
		}
		else {
			info->outWidth = disp_device.width;
			info->outHeight = disp_device.height;
		}
	}

	/* the output of IPU resizing is up to 1024*1024 */
	info->xSplitParts = info->outWidth / 1024 + 1;
	info->ySplitParts = info->outHeight / 1024 + 1;

	/* the image in block mode, which is 8*8 size */
	info->outHeight -= info->outHeight % 8;
	info->outWidth -= info->outWidth % 8;

}

/*
* this function is used to config the combination task in the IC
* local alpha with per-pixel or from separate buffer can be used
* global alpha can be used also.
*/
int ipu_ic_combine_config(ic_comb_params_t comb_params)
{
	switch (comb_params.taskType) {
	case PrP_VF_TASK:
		ipu_write_field(IPU_IC_IDMAC_1__CB3_BURST_16, 1);   // set to 16bps
		if (comb_params.alpha < 0) {
			ipu_write_field(IPU_IC_CONF__IC_GLB_LOC_A, 0);  // local alpha with per-pixel
		} else if (comb_params.alpha < 0x100) {
			ipu_write_field(IPU_IC_CONF__IC_GLB_LOC_A, 1);  // global alpha enabled
			ipu_write_field(IPU_IC_CMBP_1__IC_PRPVF_ALPHA_V, comb_params.alpha);    // global alpha value
		} else {
			ipu_write_field(IPU_IC_CONF__IC_GLB_LOC_A, 0);  // local alpha from separate buffer
		}
		ipu_write_field(IPU_IC_CONF__PRPVF_CMB,
						(comb_params.alpha == 0) ? 0 : 1);
		ipu_write_field(IPU_IC_CONF__PRPVF_EN, 1);
		break;
	case PP_TASK:
		ipu_write_field(IPU_IC_IDMAC_1__CB4_BURST_16, 1);   // set to 16bps
		if (comb_params.alpha < 0) {
			ipu_write_field(IPU_IC_CONF__IC_GLB_LOC_A, 0);  // local alpha with per-pixel
		} else if (comb_params.alpha < 0x100) {
			ipu_write_field(IPU_IC_CONF__IC_GLB_LOC_A, 1);  // global alpha enabled
			ipu_write_field(IPU_IC_CMBP_1__IC_PP_ALPHA_V, comb_params.alpha);   // global alpha
		} else {
			ipu_write_field(IPU_IC_CONF__IC_GLB_LOC_A, 0);  // local alpha in sepatate buffer
		}
		ipu_write_field(IPU_IC_CONF__PP_CMB,
						(comb_params.alpha == 0) ? 0 : 1);
		ipu_write_field(IPU_IC_CONF__PP_EN, 1);
		break;
	default:
		ERRDP("Task Type is wrong!!\n");
		return -1;
	}
	return 0;
}

/*
* this function is used to config the color space conversion task in the IC
*/
extern int xecDlsEnable;
int ipu_ic_csc_config(int csc_set_index, ic_csc_params_t csc_params)
{
	unsigned int param;
	CYG_ADDRESS tpmBaseAddr = IPU_CTRL_BASE_ADDR + 0x1F060000;
	CYG_ADDRESS base;

	if (csc_set_index != 1 && csc_set_index != 2) {
		ERRDP("Wrong index input for IC CSC!!\n");
		return -1;
	}
	/*Y = R *  .299 + G *  .587 + B *  .114;
	  U = R * -.169 + G * -.332 + B *  .500 + 128.;
	  V = R *  .500 + G * -.419 + B * -.0813 + 128.; */
	unsigned int rgb2ycbcr_coeff[4][3] = {
		{ 0x004D, 0x0096, 0x001D },
		{ 0x01D5, 0x01AB, 0x0080 },
		{ 0x0080, 0x0195, 0x01EB },
		{ 0x0000, 0x0200, 0x0200 },   /* A0, A1, A2 */
	};

	/* transparent RGB->RGB matrix for combining
	 */
	unsigned int rgb2rgb_coeff[4][3] = {
		{ 0x0080, 0x0000, 0x0000 },
		{ 0x0000, 0x0080, 0x0000 },
		{ 0x0000, 0x0000, 0x0080 },
		{ 0x0000, 0x0000, 0x0000 },   /* A0, A1, A2 */
	};

	/*
	  R = (1.164 * (Y - 16)) + (1.596 * (Cr - 128));
	  G = (1.164 * (Y - 16)) - (0.392 * (Cb - 128)) - (0.813 * (Cr - 128));
	  B = (1.164 * (Y - 16)) + (2.017 * (Cb - 128);
	*/
	unsigned int ycbcr2rgb_coeff[4][3] = {
		{ 0x95, 0x0, 0xCC },
		{ 0x95, 0x1CE, 0x198 },
		{ 0x95, 0xFF, 0x0 },
		{ 0x1E42, 0x10A, 0x1DD6 },    // A0, A1, A2
	};

	/*
	  R = (1.164 * alpha *  (Y - 16)) + (1.596 * (Cr - 128));
	  G = (1.164 * alpha * (Y - 16)) - (0.392 * (Cb - 128)) - (0.813 * (Cr - 128));
	  B = (1.164 * alpha * (Y - 16)) + (2.017 * (Cb - 128);
	*/
	if (xecDlsEnable) {
		/* compensation of y_coeff */
		ycbcr2rgb_coeff[0][0] = ycbcr2rgb_coeff[0][0] * xecDlsParams.curAlpha/100;
		ycbcr2rgb_coeff[1][0] = ycbcr2rgb_coeff[1][0] * xecDlsParams.curAlpha/100;
		ycbcr2rgb_coeff[2][0] = ycbcr2rgb_coeff[2][0] * xecDlsParams.curAlpha/100;
#if 0
		/* compensation of constant coeff */
		ycbcr2rgb_coeff[3][0] -= (1.164 * 16 * 0x2 * (xecDlsParams.curAlpha - 100)/100);
		ycbcr2rgb_coeff[3][1] -= (1.164 * 16 * 0x2 * (xecDlsParams.curAlpha - 100)/100);
		ycbcr2rgb_coeff[3][2] -= (1.164 * 16 * 0x2 * (xecDlsParams.curAlpha - 100)/100);
#endif
		DP("alpha %d \n",	xecDlsParams.curAlpha);
	}

	if (csc_set_index == 1) {
		if (csc_params.taskType == PrP_ENC_TASK) {
			base = tpmBaseAddr + 0x2008;
		} else if (csc_params.taskType == PrP_VF_TASK) {
			base = tpmBaseAddr + 0x4028;
		} else if (csc_params.taskType == PP_TASK) {
			base = tpmBaseAddr + 0x6060;
		} else {
			ERRDP("Wrong task type for IC CSC1 input!!\n");
			return -1;
		}
	} else {
		if (csc_params.taskType == PrP_VF_TASK) {
			base = tpmBaseAddr + 0x4040;
		} else if (csc_params.taskType == PP_TASK) {
			base = tpmBaseAddr + 0x6078;
		} else {
			ERRDP("Wrong task type for IC CSC2 input!!\n");
			return -1;
		}
	}

	if ((csc_params.inFormat == YCbCr) && (csc_params.outFormat == RGB)) {
		/* Init CSC (YCbCr->RGB) */
		param = (ycbcr2rgb_coeff[3][0] << 27) |
			(ycbcr2rgb_coeff[0][0] << 18) | (ycbcr2rgb_coeff[1][1] << 9) | ycbcr2rgb_coeff[2][2];
		writel(param, base++);
		/* scale = 2, sat = 0 */
		param = (ycbcr2rgb_coeff[3][0] >> 5) | (2 << (40 - 32));
		writel(param, base++);

		param = (ycbcr2rgb_coeff[3][1] << 27) |
			(ycbcr2rgb_coeff[0][1] << 18) | (ycbcr2rgb_coeff[1][0] << 9) | ycbcr2rgb_coeff[2][0];
		writel(param, base++);
		param = (ycbcr2rgb_coeff[3][1] >> 5);
		writel(param, base++);

		param = (ycbcr2rgb_coeff[3][2] << 27) |
			(ycbcr2rgb_coeff[0][2] << 18) | (ycbcr2rgb_coeff[1][2] << 9) | ycbcr2rgb_coeff[2][1];
		writel(param, base++);
		param = (ycbcr2rgb_coeff[3][2] >> 5);
		writel(param, base++);
	} else if ((csc_params.inFormat == RGB) && (csc_params.outFormat == YCbCr)) {
		/* Init CSC1 (RGB->YCbCr) */
		param = (rgb2ycbcr_coeff[3][0] << 27) |
			(rgb2ycbcr_coeff[0][0] << 18) | (rgb2ycbcr_coeff[1][1] << 9) | rgb2ycbcr_coeff[2][2];
		writel(param, base++);
		/* scale = 1, sat = 0 */
		param = (rgb2ycbcr_coeff[3][0] >> 5) | (1UL << 8);
		writel(param, base++);

		param = (rgb2ycbcr_coeff[3][1] << 27) |
			(rgb2ycbcr_coeff[0][1] << 18) | (rgb2ycbcr_coeff[1][0] << 9) | rgb2ycbcr_coeff[2][0];
		writel(param, base++);
		param = (rgb2ycbcr_coeff[3][1] >> 5);
		writel(param, base++);

		param = (rgb2ycbcr_coeff[3][2] << 27) |
			(rgb2ycbcr_coeff[0][2] << 18) | (rgb2ycbcr_coeff[1][2] << 9) | rgb2ycbcr_coeff[2][1];
		writel(param, base++);
		param = (rgb2ycbcr_coeff[3][2] >> 5);
		writel(param, base++);
	} else if ((csc_params.inFormat == RGB) && (csc_params.outFormat == RGB)) {
		/* Init CSC1 */
		param =
			(rgb2rgb_coeff[3][0] << 27) | (rgb2rgb_coeff[0][0] << 18) |
			(rgb2rgb_coeff[1][1] << 9) | rgb2rgb_coeff[2][2];
		writel(param, base++);
		/* scale = 2, sat = 0 */
		param = (rgb2rgb_coeff[3][0] >> 5) | (2UL << 8);
		writel(param, base++);

		param =
			(rgb2rgb_coeff[3][1] << 27) | (rgb2rgb_coeff[0][1] << 18) |
			(rgb2rgb_coeff[1][0] << 9) | rgb2rgb_coeff[2][0];
		writel(param, base++);
		param = (rgb2rgb_coeff[3][1] >> 5);
		writel(param, base++);

		param =
			(rgb2rgb_coeff[3][2] << 27) | (rgb2rgb_coeff[0][2] << 18) |
			(rgb2rgb_coeff[1][2] << 9) | rgb2rgb_coeff[2][1];
		writel(param, base++);
		param = (rgb2rgb_coeff[3][2] >> 5);
		writel(param, base++);
	} else {
		ERRDP("Unkown color space conversion!!\n");
		return -1;
	}
	if (csc_set_index == 1) {
		if (csc_params.taskType == PrP_ENC_TASK) {
			ipu_write_field(IPU_IC_CONF__PRPENC_CSC1, 1);
		} else if (csc_params.taskType == PrP_VF_TASK) {
			ipu_write_field(IPU_IC_CONF__PRPVF_CSC1, 1);
		} else if (csc_params.taskType == PP_TASK) {
			ipu_write_field(IPU_IC_CONF__PP_CSC1, 1);
		} else {
			ERRDP("Wrong Task input!!\n");
			return -1;
		}
	} else {
		if (csc_params.taskType == PrP_VF_TASK) {
			ipu_write_field(IPU_IC_CONF__PRPVF_CSC2, 1);
		} else if (csc_params.taskType == PP_TASK) {
			ipu_write_field(IPU_IC_CONF__PP_CSC2, 1);
		} else {
			ERRDP("Wrong Task input!!\n");
			return -1;
		}
	}
	return 0;
}

/*
* enable ipu tasks, such as preprocessing/post-processing task
*/
int ipu_ic_task_enable(int task_type, int task, int enable)
{
	switch (task_type) {
	case PrP_ENC_TASK:
		if (task == IC_CSC1)
			ipu_write_field(IPU_IC_CONF__PRPENC_CSC1, enable);
		else if (IC_PRPENC)
			ipu_write_field(IPU_IC_CONF__PRPENC_EN, enable);
		else
			ERRDP("Task Type is wrong!!\n");
		break;
	case PrP_VF_TASK:
		if (task == IC_CMB)
			ipu_write_field(IPU_IC_CONF__PRPVF_CMB, enable);
		else if (task == IC_CSC1)
			ipu_write_field(IPU_IC_CONF__PRPVF_CSC1, enable);
		else if (task == IC_CSC2)
			ipu_write_field(IPU_IC_CONF__PRPVF_CSC2, enable);
		else if (task == IC_PRPVF)
			ipu_write_field(IPU_IC_CONF__PRPVF_EN, enable);
		else
			ERRDP("Task Type is wrong!!\n");
		break;
	case PP_TASK:
		if (task == IC_CMB)
			ipu_write_field(IPU_IC_CONF__PP_CMB, enable);
		else if (task == IC_CSC1)
			ipu_write_field(IPU_IC_CONF__PP_CSC1, enable);
		else if (task == IC_CSC2)
			ipu_write_field(IPU_IC_CONF__PP_CSC2, enable);
		else if (task == IC_PP)
			ipu_write_field(IPU_IC_CONF__PP_EN, enable);
		else
			ERRDP("Task Type is wrong!!\n");
		break;
	default:
		ERRDP("Task Type is wrong!!\n");
		return -1;
	}
	return 0;
}

/*
* this function is used to config the color space conversion task in the DP
*/
void ipu_dp_csc_config(int dp, dp_csc_param_t dp_csc_params, bool srm_mode_update)
{
	int **coeff;

	ipu_write_field(SRM_DP_COM_CONF_SYNC__DP_CSC_YUV_SAT_MODE_SYNC, 0); //SAT mode is zero
	ipu_write_field(SRM_DP_COM_CONF_SYNC__DP_CSC_GAMUT_SAT_EN_SYNC, 0); //GAMUT en (RGB...)

	if (dp_csc_params.mode >= 0) {
		ipu_write_field(SRM_DP_COM_CONF_SYNC__DP_CSC_DEF_SYNC, dp_csc_params.mode); //disable CSC
	}

	coeff = dp_csc_params.coeff;

	if (coeff) {
		writel(GET_LSB(10, coeff[0][0]) | (GET_LSB(10, coeff[0][1]) << 16),
			IPU_CTRL_BASE_ADDR + SRM_DP_CSCA_SYNC_0__ADDR + dp * 4);
		writel(GET_LSB(10, coeff[0][2]) | (GET_LSB(10, coeff[1][0]) << 16),
			IPU_CTRL_BASE_ADDR + SRM_DP_CSCA_SYNC_1__ADDR + dp * 4);
		writel(GET_LSB(10, coeff[1][1]) | (GET_LSB(10, coeff[1][2]) << 16),
			IPU_CTRL_BASE_ADDR + SRM_DP_CSCA_SYNC_2__ADDR + dp * 4);
		writel(GET_LSB(10, coeff[2][0]) | (GET_LSB(10, coeff[2][1]) << 16),
			IPU_CTRL_BASE_ADDR + SRM_DP_CSCA_SYNC_3__ADDR + dp * 4);
		writel(GET_LSB(10, coeff[2][2]) | (GET_LSB(14, coeff[3][0]) << 16) |
			(coeff[4][0] << 30), IPU_CTRL_BASE_ADDR + SRM_DP_CSC_SYNC_0__ADDR + dp * 4);
		writel(GET_LSB(14, coeff[3][1]) | (coeff[4][1] << 14) |
			(GET_LSB(14, coeff[3][2]) << 16) | (coeff[4][2] << 30),
			IPU_CTRL_BASE_ADDR + SRM_DP_CSC_SYNC_1__ADDR + dp * 4);
	}
	if (srm_mode_update) {
		ipu_write_field(IPU_IPU_SRM_PRI2__DP_S_SRM_MODE, 3);
		ipu_write_field(IPU_IPU_SRM_PRI2__DP_SRM_PRI, 0x0);
	}
}

/*
* this function is used to config the foreground plane for combination in the DP
*/
void ipu_dp_fg_config(dp_fg_param_t foreground_params)
{
	ipu_write_field(SRM_DP_COM_CONF_SYNC__DP_GAMMA_EN_SYNC, 0);
	ipu_write_field(SRM_DP_COM_CONF_SYNC__DP_GAMMA_YUV_EN_SYNC, 0);

	ipu_write_field(SRM_DP_COM_CONF_SYNC__DP_COC_SYNC, foreground_params.cursorEnable);
	ipu_write_field(SRM_DP_COM_CONF_SYNC__DP_GWCKE_SYNC, foreground_params.colorKeyEnable); //color key
	ipu_write_field(SRM_DP_COM_CONF_SYNC__DP_GWAM_SYNC, foreground_params.alphaMode);   //1=global alpha,0=local alpha
	ipu_write_field(SRM_DP_COM_CONF_SYNC__DP_GWSEL_SYNC, foreground_params.graphicSelect);  //1=graphic is FG,0=graphic is BG
	ipu_write_field(SRM_DP_COM_CONF_SYNC__DP_FG_EN_SYNC, foreground_params.fgEnable);   //1=FG channel enabled,0=FG channel disabled
	ipu_write_field(SRM_DP_FG_POS_SYNC__DP_FGXP_SYNC, foreground_params.offsetHoriz);
	ipu_write_field(SRM_DP_FG_POS_SYNC__DP_FGYP_SYNC, foreground_params.offsetVert);
	ipu_write_field(SRM_DP_GRAPH_WIND_CTRL_SYNC__DP_GWAV_SYNC, foreground_params.opaque);   // set the FG opaque
	ipu_write_field(SRM_DP_GRAPH_WIND_CTRL_SYNC__DP_GWCKR_SYNC, 0xFF);
	ipu_write_field(SRM_DP_GRAPH_WIND_CTRL_SYNC__DP_GWCKG_SYNC, 0xFF);
	ipu_write_field(SRM_DP_GRAPH_WIND_CTRL_SYNC__DP_GWCKB_SYNC, 0xFF);
}

/*
* microcode configuration, refer to ipuv3 spec
*/
void ipu_dc_microcode_config(dc_microcode_t microcode)
{
	unsigned int LowWord = 0;
	unsigned int HighWord = 0;
	unsigned int opcode_fixed;

	if (!strcmp(microcode.opcode, "WROD")) {
		LowWord = LowWord | microcode.sync;
		LowWord = LowWord | (microcode.gluelogic << 4);
		LowWord = LowWord | (microcode.waveform << 11);
		LowWord = LowWord | (microcode.mapping << 15);
		LowWord = LowWord | (microcode.operand << 20);

		HighWord = HighWord | (microcode.operand >> 12);
		opcode_fixed = 0x18 | (microcode.lf << 1);
		HighWord = HighWord | (opcode_fixed << 4);
		HighWord = HighWord | (microcode.stop << 9);
	} else {
		ERRDP("Microcode type not supported yet!!\n");
	}
	writel(LowWord, IPU_CTRL_BASE_ADDR + IPU_MEM_DC_MICROCODE_BASE_ADDR + microcode.addr * 8);
	writel(HighWord, IPU_CTRL_BASE_ADDR + IPU_MEM_DC_MICROCODE_BASE_ADDR + microcode.addr * 8 + 4);
}

/*
* microcode event configuration, to handle different event
*/
void ipu_dc_microcode_event(int channel, char event[8], int priority, int address)
{
	int channel_offset = (channel >= 5) ? (0x5C + (channel - 5) * 0x1C) : channel * 0x1C;

	if (!strcmp(event, "NL")) {
		ipu_write_field(channel_offset + IPU_DC_RL0_CH_0__COD_NL_START_CHAN_0, address);
		ipu_write_field(channel_offset + IPU_DC_RL0_CH_0__COD_NL_PRIORITY_CHAN_0, priority);
	} else if (!strcmp(event, "NF")) {
		ipu_write_field(channel_offset + IPU_DC_RL0_CH_0__COD_NF_START_CHAN_0, address);
		ipu_write_field(channel_offset + IPU_DC_RL0_CH_0__COD_NF_PRIORITY_CHAN_0, priority);
	} else if (!strcmp(event, "NFIELD")) {
		ipu_write_field(channel_offset + IPU_DC_RL1_CH_0__COD_NFIELD_START_CHAN_0, address);
		ipu_write_field(channel_offset + IPU_DC_RL1_CH_0__COD_NFIELD_PRIORITY_CHAN_0, priority);
	} else if (!strcmp(event, "EOF")) {
		ipu_write_field(channel_offset + IPU_DC_RL1_CH_0__COD_EOF_START_CHAN_0, address);
		ipu_write_field(channel_offset + IPU_DC_RL1_CH_0__COD_EOF_PRIORITY_CHAN_0, priority);
	} else if (!strcmp(event, "EOFIELD")) {
		ipu_write_field(channel_offset + IPU_DC_RL2_CH_0__COD_EOFIELD_START_CHAN_0, address);
		ipu_write_field(channel_offset + IPU_DC_RL2_CH_0__COD_EOFIELD_PRIORITY_CHAN_0, priority);
	} else if (!strcmp(event, "EOL")) {
		ipu_write_field(channel_offset + IPU_DC_RL2_CH_0__COD_EOL_START_CHAN_0, address);
		ipu_write_field(channel_offset + IPU_DC_RL2_CH_0__COD_EOL_PRIORITY_CHAN_0, priority);
	} else if (!strcmp(event, "NEW_CHAN")) {
		ipu_write_field(channel_offset + IPU_DC_RL3_CH_0__COD_NEW_CHAN_START_CHAN_0, address);
		ipu_write_field(channel_offset + IPU_DC_RL3_CH_0__COD_NEW_CHAN_PRIORITY_CHAN_0, priority);
	} else if (!strcmp(event, "NEW_ADDR")) {
		ipu_write_field(channel_offset + IPU_DC_RL3_CH_0__COD_NEW_ADDR_START_CHAN_0, address);
		ipu_write_field(channel_offset + IPU_DC_RL3_CH_0__COD_NEW_ADDR_PRIORITY_CHAN_0, priority);
	} else if (!strcmp(event, "NEW_DATA")) {
		ipu_write_field(channel_offset + IPU_DC_RL4_CH_0__COD_NEW_DATA_START_CHAN_0, address);
		ipu_write_field(channel_offset + IPU_DC_RL4_CH_0__COD_NEW_DATA_PRIORITY_CHAN_0, priority);
	} else {
		ERRDP("Wrong DC microcode\n");
	}
}

/*
* this function is used to perform pack/unpacking for yuv/rgb data
*/
int ipu_dc_map(int map, int format)
{
	int offset[3], mask[3];

	if (format == RGB565) {
		offset[0] = 15;
		mask[0] = 0xF8;
		offset[1] = 10;
		mask[1] = 0xFC;
		offset[2] = 4;
		mask[2] = 0xF8;
	} else if (format == RGB666) {
		offset[0] = 17;
		mask[0] = 0xFC;
		offset[1] = 11;
		mask[1] = 0xFC;
		offset[2] = 5;
		mask[2] = 0xFC;
	} else if (format == RGB888 || format == YUV888) {
		offset[0] = 23;
		mask[0] = 0xFF;
		offset[1] = 15;
		mask[1] = 0xFF;
		offset[2] = 7;
		mask[2] = 0xFF;
	} else {
		ERRDP("Invalid pixel format %d\n", format);
		return -1;
	}

	switch (map) {
	case 0:
		/* DC_MAP, should be RGB666 mode */
		ipu_write_field(IPU_DC_MAP_CONF_16__MD_OFFSET_2, offset[0]);
		ipu_write_field(IPU_DC_MAP_CONF_16__MD_MASK_2, mask[0]);
		ipu_write_field(IPU_DC_MAP_CONF_15__MD_OFFSET_1, offset[1]);
		ipu_write_field(IPU_DC_MAP_CONF_15__MD_MASK_1, mask[1]);
		ipu_write_field(IPU_DC_MAP_CONF_15__MD_OFFSET_0, offset[2]);
		ipu_write_field(IPU_DC_MAP_CONF_15__MD_MASK_0, mask[2]);

		ipu_write_field(IPU_DC_MAP_CONF_0__MAPPING_PNTR_BYTE2_0, 0);
		ipu_write_field(IPU_DC_MAP_CONF_0__MAPPING_PNTR_BYTE1_0, 2);
		ipu_write_field(IPU_DC_MAP_CONF_0__MAPPING_PNTR_BYTE0_0, 1);
		break;

	case 1:
		/* DC_MAP */
		ipu_write_field(IPU_DC_MAP_CONF_18__MD_OFFSET_6, offset[0]);
		ipu_write_field(IPU_DC_MAP_CONF_18__MD_MASK_6, mask[0]);
		ipu_write_field(IPU_DC_MAP_CONF_17__MD_OFFSET_5, offset[1]);
		ipu_write_field(IPU_DC_MAP_CONF_17__MD_MASK_5, mask[1]);
		ipu_write_field(IPU_DC_MAP_CONF_17__MD_OFFSET_4, offset[2]);
		ipu_write_field(IPU_DC_MAP_CONF_17__MD_MASK_4, mask[2]);
		ipu_write_field(IPU_DC_MAP_CONF_0__MAPPING_PNTR_BYTE2_1, 6);
		ipu_write_field(IPU_DC_MAP_CONF_0__MAPPING_PNTR_BYTE1_1, 5);
		ipu_write_field(IPU_DC_MAP_CONF_0__MAPPING_PNTR_BYTE0_1, 4);
		break;

	default:
		ERRDP("Invalid map value: %d\n", map);
		return -1;
	}
	return 0;
}

/*
*  config the display port in the DC
*/
int ipu_dc_display_config(int display_port, int type, int increment, int strideline)
{
	switch (display_port) {
	case 0:
		ipu_write_field(IPU_DC_DISP_CONF1_0__DISP_TYP_0, type); /* parallel display without byte enable */
		ipu_write_field(IPU_DC_DISP_CONF1_0__ADDR_INCREMENT_0, increment);
		ipu_write_field(IPU_DC_DISP_CONF2_0__SL_0, strideline); //stride line
		break;
	case 1:
		ipu_write_field(IPU_DC_DISP_CONF1_1__DISP_TYP_1, type); /* parallel display without byte enable */
		ipu_write_field(IPU_DC_DISP_CONF1_1__ADDR_INCREMENT_1, increment);
		ipu_write_field(IPU_DC_DISP_CONF2_1__SL_1, strideline); //stride line
		break;
	case 2:
		ipu_write_field(IPU_DC_DISP_CONF1_2__DISP_TYP_2, type); /* parallel display without byte enable */
		ipu_write_field(IPU_DC_DISP_CONF1_2__ADDR_INCREMENT_2, increment);
		ipu_write_field(IPU_DC_DISP_CONF2_2__SL_2, strideline); //stride line
		break;
	case 3:
		ipu_write_field(IPU_DC_DISP_CONF1_3__DISP_TYP_3, type); /* parallel display without byte enable */
		ipu_write_field(IPU_DC_DISP_CONF1_3__ADDR_INCREMENT_3, increment);
		ipu_write_field(IPU_DC_DISP_CONF2_3__SL_3, strideline); //stride line
		break;
	default:
		ERRDP("Invalid display port: %d\n", display_port);
		return -1;
	}
	return 0;
}

/*
* config the write channel for display.
* different channels linked to different display port
*/
int ipu_dc_write_channel_config(int dma_channel, int disp_port, int link_di_index,
								int field_mode_enable)
{
	switch (dma_channel) {
	case 23:
		ipu_write_field(IPU_DC_WR_CH_CONF_5__PROG_START_TIME_5, 0);
		ipu_write_field(IPU_DC_WR_CH_CONF_5__CHAN_MASK_DEFAULT_5, 0);
		ipu_write_field(IPU_DC_WR_CH_CONF_5__PROG_CHAN_TYP_5, 4);   // Normal mode without anti-tearing
		ipu_write_field(IPU_DC_WR_CH_CONF_5__PROG_DISP_ID_5, disp_port);
		ipu_write_field(IPU_DC_WR_CH_CONF_5__PROG_DI_ID_5, link_di_index);
		ipu_write_field(IPU_DC_WR_CH_CONF_5__W_SIZE_5, 2);  // Component size access to DC set to 24bit
		ipu_write_field(IPU_DC_WR_CH_ADDR_5__ST_ADDR_5, 0);
		ipu_write_field(IPU_DC_WR_CH_CONF_5__FIELD_MODE_5, field_mode_enable);

		ipu_write_field(IPU_DC_GEN__SYNC_PRIORITY_5, 1);    // sets the priority of channel #5 to high.
		ipu_write_field(IPU_DC_GEN__MASK4CHAN_5, 0);    // mask channel is associated to the sync flow via DC (without DP)
		ipu_write_field(IPU_DC_GEN__MASK_EN, 0);    // mask channel is disabled
		ipu_write_field(IPU_DC_GEN__DC_CH5_TYPE, 0);    // alternate sync or asyn flow
		break;
	case 28:
		ipu_write_field(IPU_DC_WR_CH_CONF_1__PROG_START_TIME_1, 0);
		ipu_write_field(IPU_DC_WR_CH_CONF_1__CHAN_MASK_DEFAULT_1, 0);
		ipu_write_field(IPU_DC_WR_CH_CONF_1__PROG_CHAN_TYP_1, 4);   // Normal mode without anti-tearing
		ipu_write_field(IPU_DC_WR_CH_CONF_1__PROG_DISP_ID_1, disp_port);
		ipu_write_field(IPU_DC_WR_CH_CONF_1__PROG_DI_ID_1, link_di_index);
		/* if CH28 is connected to DI0, CH23 must connect to DI1 even if it is not used. */
		if (link_di_index == 0)
			ipu_write_field(IPU_DC_WR_CH_CONF_5__PROG_DI_ID_5, 1);

		ipu_write_field(IPU_DC_WR_CH_CONF_1__W_SIZE_1, 2);  // Component size access to DC set to 24bit
		ipu_write_field(IPU_DC_WR_CH_ADDR_1__ST_ADDR_1, 0); // START ADDRESS OF CHANNEL
		ipu_write_field(IPU_DC_WR_CH_CONF_1__FIELD_MODE_1, field_mode_enable);

		ipu_write_field(IPU_DC_GEN__SYNC_PRIORITY_1, 1);    //sets the priority of channel #5 to high.
		ipu_write_field(IPU_DC_GEN__SYNC_1_6, 2);   // Channel 1 of the DC handles sync flow
		break;
	default:
		ERRDP("Invalid display channel: %d\n", dma_channel);
		return -1;
	}
	return 0;
}

