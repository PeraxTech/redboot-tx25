#ifndef CYGONCE_DEVS_FLASH_BOARD_STRATAFLASH_INL
#define CYGONCE_DEVS_FLASH_BOARD_STRATAFLASH_INL
//==========================================================================
//
//      board_strataflash.inl
//
//      Flash programming - device constants, etc.
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
//#####DESCRIPTIONBEGIN####
//
// Author(s):    gthomas, hmt
// Contributors: gthomas
// Date:         2001-02-24
// Purpose:      
// Description:  
//              
//####DESCRIPTIONEND####
//
//==========================================================================

// Intel Flash configuration:         Buffered  Read     Block
//                                     write    query    locking
// 28FxxxB3 - Bootblock               - no      no       no
// 28FxxxC3 - StrataFlash             - no      yes      yes
// 28FxxxJ3 - Advanced StrataFlash    - yes     yes      yes
// 28FxxxK3 - Synchronous StrataFlash - yes     yes      yes
// 
// These options are controlled by defining or not, in that include file,
// these symbols (not CDL options, just symbols - though they could be CDL
// in future)
//         CYGOPT_FLASH_IS_BOOTBLOCK     - for xxxB3 devices.
//         CYGOPT_FLASH_IS_NOT_ADVANCED  - for xxxC3 devices.
//         CYGOPT_FLASH_IS_SYNCHRONOUS   - for xxxK3 devices.
//         none of the above             - for xxxJ3 devices.  
// (Advanced seems to be usual these days hence the sense of that opt)
//
// Other properties are controlled by these symbols:
//         CYGNUM_FLASH_DEVICES 	number of devices across the databus
//         CYGNUM_FLASH_WIDTH 	        number of bits in each device
//         CYGNUM_FLASH_BLANK           1 if blank is allones, 0 if 0
//         CYGNUM_FLASH_BASE 	        base address
//         CYGNUM_FLASH_BASE_MASK       a mask to get base address from any
// 
// for example, a 32-bit memory could be made from 1x32bit, 2x16bit or
// 4x8bit devices; usually 16bit ones are chosen in practice, so we would
// have CYGNUM_FLASH_DEVICES = 2, and CYGNUM_FLASH_WIDTH = 16.  Both
// devices would be handled simulataneously, via 32bit bus operations.
// Some CPUs can handle a single 16bit device as 32bit memory "by magic".
// In that case, CYGNUM_FLASH_DEVICES = 1 and CYGNUM_FLASH_WIDTH = 16, and
// the device is managed using only 16bit bus operations.


// The NOR flash type of the i.300-30 ADS board is a StrataFlash 28F256L18B.
// The 256 means 256Mbit, so 16M x 16bit. 16bits width.
// but There are 2 chips and there conf depends on Three resistors:

#define I30030ADS_FLASH_DEF_CONF

#ifdef I30030ADS_FLASH_DEF_CONF
//by default the two 28F256L18B are configured in parallel so 32 bits width: 16Mx32bits

#define CYGNUM_FLASH_DEVICES 	(1)
#define CYGNUM_FLASH_BASE 	(0xA0000000u)
#define CYGNUM_FLASH_BASE_MASK  (0xFF000000u)     // 16MB devices (size=0x01000000 -> mask=0xFF000000)
#define CYGNUM_FLASH_WIDTH 	(16)              // width of one device
#define CYGNUM_FLASH_BLANK      (1)

#define CYGOPT_FLASH_IS_SYNCHRONOUS
#define CYGOPT_FLASH_IS_BOOTBLOCK 1

#endif  //I30030ADS_FLASH_DEF_CONF

#endif  // CYGONCE_DEVS_FLASH_BOARD_STRATAFLASH_INL
// ------------------------------------------------------------------------

