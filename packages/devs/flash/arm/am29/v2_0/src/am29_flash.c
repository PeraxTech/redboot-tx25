//==========================================================================
//
//      sa1100mm_flash.c
//
//      Flash programming
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <pkgconf/hal.h>
#include <cyg/hal/hal_arch.h>
#include <cyg/hal/hal_cache.h>

#define  _FLASH_PRIVATE_
#include <cyg/io/flash.h>

#include "am29.h"

extern void diag_printf(char* str, ...);

int
#ifndef MXCFLASH_SELECT_MULTI
flash_hwr_init(void)
#else
norflash_hwr_init(void)
#endif
{
    flash_info.block_size = FLASH_SECTOR_SIZE;
    flash_info.blocks = FLASH_SECTOR_NUM;
    flash_info.start = (void *)(FLASH_BASE);
    flash_info.end = (void *)(FLASH_BASE+(FLASH_SECTOR_NUM*FLASH_SECTOR_SIZE));
    return FLASH_ERR_OK;
}

// Map a hardware status to a package error
int
#ifndef MXCFLASH_SELECT_MULTI
flash_hwr_map_error(int e)
#else
norflash_hwr_map_error(int e)
#endif
{
    return e;     // do straing mapping for now
}

// See if a range of FLASH addresses overlaps currently running code
bool
#ifndef MXCFLASH_SELECT_MULTI
flash_code_overlaps(void *start, void *end)
#else
norflash_code_overlaps(void *start, void *end)
#endif
{
    extern char _stext[], _etext[];

    return ((((unsigned long)&_stext >= (unsigned long)start) &&
             ((unsigned long)&_stext < (unsigned long)end)) ||
            (((unsigned long)&_etext >= (unsigned long)start) &&
             ((unsigned long)&_etext < (unsigned long)end)));
}


