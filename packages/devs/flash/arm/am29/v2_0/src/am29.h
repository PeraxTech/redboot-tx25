//==========================================================================
//
//      am29.h
//
//      Flash programming for Freescale MX21 ADS board
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/infra/cyg_type.h>

#define CYGNUM_FLASH_INTERLEAVE     (2)
#define FLASH_SECTOR_SIZE           (0x10000*CYGNUM_FLASH_INTERLEAVE) //128KB with 64KB each chip
#define FLASH_SMALL_SECTOR_SIZE     (0x2000*CYGNUM_FLASH_INTERLEAVE)  //16KB with 8KB each chip
#define FLASH_SECTOR_NUM            (256)

#if defined(CYGPKG_HAL_ARM_MDBMX1)
    #define FLASH_BASE                  (0x10000000)     /* CS0 for MX1 */
#elif defined(CYGPKG_HAL_ARM_MX21)
    #define FLASH_BASE                  (0xC8000000)     /* CS0 for MX2 */
#else
    #error MX1 or MX2?
#endif


# if 1 == CYGNUM_FLASH_INTERLEAVE
#  define FLASHWORD( k ) (k)
typedef cyg_uint16 flash_data_t;
# elif 2 == CYGNUM_FLASH_INTERLEAVE
// 2 devices to make 32-bit
#  define FLASHWORD( k ) ((k)+((k)<<16))
typedef cyg_uint32 flash_data_t;
# else
#  error How many 16-bit flash devices?
# endif


# define FLASH_P2V( _a_ )           ((volatile flash_data_t *)((CYG_ADDRWORD)(_a_)))
# define FLASH_P2V_MAPPED(base, offset )  \
      ((volatile flash_data_t *)((CYG_ADDRWORD)(offset<<CYGNUM_FLASH_INTERLEAVE)+(CYG_ADDRWORD)(base)))


#define KBIT0       FLASHWORD(1<<0)
#define KBIT1       FLASHWORD(1<<1)
#define KBIT2       FLASHWORD(1<<2)
#define KBIT3       FLASHWORD(1<<3)
#define KBIT4       FLASHWORD(1<<4)
#define KBIT5       FLASHWORD(1<<5)
#define KBIT6       FLASHWORD(1<<6)
#define KBIT7       FLASHWORD(1<<7)


#define DRAIN_WRITE_BUFFER()     \
{                                                      \
      asm volatile (                                                      \
        "mov    r0, #0x0;"                                                    \
        "mcr    p15,0,r0,c7,c10,4;" /* drain the write buffer */        \
        :                                                               \
        :                                                               \
        : "r0" /* Clobber list */                                       \
  );                        \
}                           

#define FLASH_RESET()                                   \
{                                                       \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0) = FLASHWORD(0xF0);             \
}

/* 
 * This function does the "2 unlock cycle" commands.
 */
#define FLASH_UNLOCK()                                  \
{                                                       \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0x555) = FLASHWORD(0xAA);         \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0x2AA) = FLASHWORD(0x55);         \
}

/*
 * Setup the 3 cycle command for the autoselect commands based on 
 * the address
 */
#define FLASH_AUTO_SEL_SETUP(aSelAddr)                  \
{                                                       \
    FLASH_RESET();                                      \
    FLASH_UNLOCK();                                     \
    *FLASH_P2V_MAPPED(aSelAddr, 0x555) = FLASHWORD(0x90);       \
}

/*
 * The manufacturer's ID is in the passed in variable upon return
 */
#define FLASH_GET_MANU_ID(aManuId)                   \
{                                                        \
    FLASH_AUTO_SEL_SETUP(FLASH_BASE);                    \
    aManuId = *FLASH_P2V_MAPPED(FLASH_BASE, 0);    \
    FLASH_RESET();                                      \
}

/*
 * The device IDs are in the variables upon return
 */
#define FLASH_GET_DEV_ID(aDevId1, aDevId2, aDevId3)     \
{                                                                   \
    FLASH_AUTO_SEL_SETUP(FLASH_BASE);                               \
    aDevId1 = *FLASH_P2V_MAPPED(FLASH_BASE, 0x01);              \
    aDevId2 = *FLASH_P2V_MAPPED(FLASH_BASE, 0x0E);              \
    aDevId3 = *FLASH_P2V_MAPPED(FLASH_BASE, 0x0F);              \
    FLASH_RESET();                                      \
}

/*
 * The sector lock status is in the variable upon return
 */
#define FLASH_GET_SECTOR_LOCK_VERIFY(aSecAddr, aRtnLock)  \
{                                                             \
    FLASH_AUTO_SEL_SETUP(aSecAddr);                           \
    aRtnLock = *FLASH_P2V_MAPPED(aSecAddr, 0x02);     \
}

/*
 * Write aProgData to address aProgAbsAddr as the absolute address 
 * (not address offset)
 * May require to call reset fucntion before start programming
 */
#define FLASH_PROGRAM(aProgAddr, aProgData)             \
{                                                       \
    FLASH_UNLOCK();                                     \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0x555) = FLASHWORD(0xA0);         \
    *FLASH_P2V(aProgAddr) = aProgData;                  \
     DRAIN_WRITE_BUFFER();                               \
}

#define FLASH_CHIP_ERASE()                              \
{                                                       \
    FLASH_RESET();                                      \
    DRAIN_WRITE_BUFFER();                               \
    FLASH_UNLOCK();                                     \
    DRAIN_WRITE_BUFFER();                               \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0x555) = FLASHWORD(0x80);         \
    DRAIN_WRITE_BUFFER();                               \
    FLASH_UNLOCK();                                     \
    DRAIN_WRITE_BUFFER();                               \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0x555) = FLASHWORD(0x10);         \
    DRAIN_WRITE_BUFFER();                               \
    FLASH_RESET();                                      \
    DRAIN_WRITE_BUFFER();                               \
}

#define FLASH_SECTOR_ERASE(aEraseSecAddr, aEraseStatus)     \
    {   									\
    aEraseStatus = FLASH_ERR_OK;                            \
    FLASH_RESET();     					\
    DRAIN_WRITE_BUFFER();                               \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0x555) = FLASHWORD(0xAA);         \
    DRAIN_WRITE_BUFFER();                                \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0x2AA) = FLASHWORD(0x55);         \
    DRAIN_WRITE_BUFFER();                       \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0x555) = FLASHWORD(0x80); \
    DRAIN_WRITE_BUFFER();                                \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0x555) = FLASHWORD(0xAA);         \
    DRAIN_WRITE_BUFFER();                         \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0x2AA) = FLASHWORD(0x55);         \
    DRAIN_WRITE_BUFFER();                           \
    *FLASH_P2V_MAPPED(aEraseSecAddr, 0x555) = FLASHWORD(0x30);            \
    DRAIN_WRITE_BUFFER();                           \
    while(((*FLASH_P2V_MAPPED(aEraseSecAddr, 0x555)) & KBIT7) != KBIT7)   \
        {                                                   \
        if(((*FLASH_P2V_MAPPED(aEraseSecAddr, 0x555)) & KBIT5) == KBIT5)  \
            {                                               \
            if(((*FLASH_P2V_MAPPED(aEraseSecAddr, 0x555)) & KBIT7) != KBIT7) \
                {                                               \
                aEraseStatus = FLASH_ERR_ERASE;                 \
                break;                                          \
                }                                               \
            }                                               \
        }                                                   \
    FLASH_RESET();                                          \
    DRAIN_WRITE_BUFFER();                      \
    }

#define FLASH_ERASE_SUSPEND(aBankAddrSus)               \
{                                                       \
    *FLASH_P2V(aBankAddrSus) = FLASHWORD(0xB0);         \
    DRAIN_WRITE_BUFFER();                               \
    FLASH_RESET();                                          \
}

#define FLASH_ERASE_RESUME(aBankAddrRes)                \
{                                                       \
    *FLASH_P2V(aBankAddrRes) = FLASHWORD(0x30);         \
    DRAIN_WRITE_BUFFER();                               \
}


/* 
 * Call reset before doing this. When exiting the unlock bypass mode, 
 * FLASH_UB_RESET() has to be called.
 */
#define FLASH_UB_ENTRY()                                \
{                                                       \
    FLASH_RESET();                                      \
    DRAIN_WRITE_BUFFER();                               \
    FLASH_UNLOCK();                                     \
    DRAIN_WRITE_BUFFER();                               \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0x555) = FLASHWORD(0x20);         \
     DRAIN_WRITE_BUFFER();                               \
}


#define FLASH_UB_PROGRAM(aUBProgAddr, aUBProgData)      \
{                                                       \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0) = FLASHWORD(0xA0); \
     DRAIN_WRITE_BUFFER();                               \
    *FLASH_P2V(aUBProgAddr) = aUBProgData;              \
     DRAIN_WRITE_BUFFER();                               \
}

#define FLASH_UB_SECTOR_ERASE(aUBSecEraseAddr)          \
{                                                       \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0) = FLASHWORD(0x80);             \
     DRAIN_WRITE_BUFFER();                               \
    *FLASH_P2V(aUBSecEraseAddr) = FLASHWORD(0x30);      \
     DRAIN_WRITE_BUFFER();                               \
}


#define FLASH_UB_ERASE()                                \
{                                                       \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0) = FLASHWORD(0x80);             \
    DRAIN_WRITE_BUFFER();                               \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0) = FLASHWORD(0x10);             \
     DRAIN_WRITE_BUFFER();                               \
}

#define FLASH_UB_RESET()                                \
{                                                       \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0) = FLASHWORD(0x90);             \
    *FLASH_P2V_MAPPED(FLASH_BASE, 0) = FLASHWORD(0x00);             \
}


#define Get_PC(pcVal)                                   \
CYG_MACRO_START                                         \
    register cyg_uint32 reg;                            \
    asm volatile ("mov %0,pc"                           \
                  : "=r"(reg)                           \
                  :                                     \
        );                                              \
    pcVal = reg;                                        \
CYG_MACRO_END
