//==========================================================================
//
//      flash_program_buf.c
//
//      Flash programming
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//===========================================================================

#include <cyg/io/flash.h>
#include <pkgconf/hal.h>
#include <cyg/hal/hal_arch.h>

#if defined(CYGPKG_HAL_ARM_MDBMX1)
	#include <cyg/hal/hal_mdbmx1.h>         // Hardware definitions
#elif defined(CYGPKG_HAL_ARM_MX21)
	#include <cyg/hal/hal_soc.h>            // Hardware definitions
#else
    #error MX1 or MX2?
#endif

#include "am29.h"

#ifndef MXCFLASH_SELECT_MULTI
int 
flash_program_buf(volatile unsigned long *addr, unsigned long *data, int byteLen)
	__attribute__ ((section (".2ram.flash_program_buf")));
#else
int  norflash_program_buf(volatile unsigned long *addr, unsigned long *data, int byteLen);
#endif

int
#ifndef MXCFLASH_SELECT_MULTI
flash_program_buf(volatile unsigned long *addr, unsigned long *data, int byteLen)
#else
norflash_program_buf(volatile unsigned long *addr, unsigned long *data, int byteLen)
#endif
{
    unsigned long stat = FLASH_ERR_OK;
    int i, j=0;
    volatile flash_data_t* tempWriteData = (volatile flash_data_t *)data;
    volatile flash_data_t tempReadData = 0;

    FLASH_RESET();
    FLASH_UB_ENTRY();
    for(i=0, j=0; i<byteLen; i+=2*CYGNUM_FLASH_INTERLEAVE, j++)
        {
        FLASH_UB_PROGRAM(((unsigned long)addr)+i, tempWriteData[j]);

        while(((tempReadData = (*FLASH_P2V(((unsigned long)addr)+i))) & KBIT7) != (tempWriteData[j] & KBIT7))
            {
            if((tempReadData & KBIT5) == KBIT5)
                {
                if(((*FLASH_P2V(((unsigned long)addr)+i)) & KBIT7) == (tempWriteData[j] & KBIT7))
                    break;
                else
                    {
                    FLASH_UB_RESET();
                    return FLASH_ERR_PROGRAM;
                    }
            
                }
            }
        }
        
    FLASH_UB_RESET();
    FLASH_RESET();
    
    return stat;
}

