static cyg_int8 bbt_pattern[] = {'B', 'b', 't', '0' };
static cyg_int8 mirror_pattern[] = {'1', 't', 'b', 'B' };

static struct nand_bbt_descr tx25_bbt_main_descr = {
	.options = (NAND_BBT_LASTBLOCK | NAND_BBT_CREATE | NAND_BBT_WRITE |
		    NAND_BBT_2BIT | NAND_BBT_VERSION),
	.offs =	0,
	.len = 4,
	.veroffs = 4,
	.maxblocks = 4,
	.pattern = bbt_pattern,
};

static struct nand_bbt_descr tx25_bbt_mirror_descr = {
	.options = (NAND_BBT_LASTBLOCK | NAND_BBT_CREATE | NAND_BBT_WRITE |
		    NAND_BBT_2BIT | NAND_BBT_VERSION),
	.offs =	0,
	.len = 4,
	.veroffs = 4,
	.maxblocks = 4,
	.pattern = mirror_pattern,
};

static struct nand_bbt_descr *g_mxc_nfc_bbt_main_descr = &tx25_bbt_main_descr;
static struct nand_bbt_descr *g_mxc_nfc_bbt_mirror_descr = &tx25_bbt_mirror_descr;
