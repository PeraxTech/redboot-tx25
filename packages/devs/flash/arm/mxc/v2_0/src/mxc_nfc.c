//==========================================================================
//
//		mxc_nfc.c
//
//		Flash programming to support NAND flash on Freescale MXC platforms
//
//==========================================================================
//####ECOSGPLCOPYRIGHTBEGIN####
// -------------------------------------------
// This file is part of eCos, the Embedded Configurable Operating System.
// Copyright (C) 1998, 1999, 2000, 2001, 2002 Red Hat, Inc.
//
// eCos is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 or (at your option) any later version.
//
// eCos is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with eCos; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
//
// As a special exception, if other files instantiate templates or use macros
// or inline functions from this file, or you compile this file and link it
// with other works to produce a work based on this file, this file does not
// by itself cause the resulting work to be covered by the GNU General Public
// License. However the source code for this file must still be made available
// in accordance with section (3) of the GNU General Public License.
//
// This exception does not invalidate any other reasons why a work based on
// this file might be covered by the GNU General Public License.
//
// Alternative licenses for eCos may be arranged by contacting Red Hat, Inc.
// at http://sources.redhat.com/ecos/ecos-license/
// -------------------------------------------
//####ECOSGPLCOPYRIGHTEND####
//==========================================================================
//#####DESCRIPTIONBEGIN####
//
// Author(s):	 Kevin Zhang <k.zhang@freescale.com>
// Contributors: Kevin Zhang <k.zhang@freescale.com>
// Date:		 2006-01-23 Initial version
// Date:		 2007-12-20 Update to support 4K page and bbt management.
// Purpose:
// Description:
//	 -- Add bad block management according to Linux NAND MTD implementation.
//		Reference linux/drivers/mtd/nand/nand_bbt.c by Thomas Gleixner
//		Summary:
//		   1. Last 4 blocks are reserved for one main BBT and one
//			  mirror BBT (2 spare ones just in case a block turns bad.)
//		   2. The main BBT block's spare area starts with "Bbt0" followed
//			  by a version number starting from 1.
//		   3. The mirror BBT block's spare area starts with "1tbB" followed
//			  by a version number also starting from 1.
//		   4. The actual main area, starting from first page in the BBT block,
//			  is used to indicate if a block is bad or not through 2bit/block:
//				* The table uses 2 bits per block
//				* 11b:	block is good
//				* 00b:	block is factory marked bad
//				* 01b:	block is marked bad due to wear
//				* 10b:	block is marked reserved (for BBT)
//		Redboot operations: During boot, it searches for the marker for
//							either main BBT or mirror BBT based on the marker:
//		   case 1: Neither table is found:
//				   Do the bad block scan of the whole flash with ECC off. Use
//				   manufactor marked BI field to decide if a block is bad and
//				   then build the BBT in RAM. Then write this table to both
//				   main BBT block and mirror BBT block.
//		   case 2: Only one table is found:
//				   Load the BBT from the flash and stored in the RAM.
//				   Then build the 2nd BBT in the flash.
//		   case 3: If both tables found, load the one with higher version in the
//				   RAM and then update the block with older BBT info with the
//				   newer one. If same version, just then read out the table in
//				   RAM.
//
//####DESCRIPTIONEND####
//
//==========================================================================

#include <pkgconf/hal.h>
#include <cyg/hal/hal_arch.h>
#include <cyg/hal/hal_cache.h>
#include <cyg/hal/hal_misc.h>
#include <cyg/io/nand_bbt.h>
#include <redboot.h>
#include <stdlib.h>

#include CYGHWR_MEMORY_LAYOUT_H

#include <cyg/hal/hal_io.h>
#define	 _FLASH_PRIVATE_
#include <cyg/io/flash.h>

#ifdef CYGHWR_FLASH_NAND_BBT_HEADER
#include CYGHWR_FLASH_NAND_BBT_HEADER
#endif

#include <cyg/io/imx_nfc.h>

#define ECC_FORCE_ON	1
#define ECC_FORCE_OFF	2

typedef u64 flash_addr_t;

enum blk_bad_type
{
	BLK_GOOD = 0,
	BLK_BAD_RUNTIME = 1,
	BLK_RESERVED = 2,
	BLK_BAD_FACTORY = 3,
};

#define diag_printf1(fmt...) CYG_MACRO_START						\
		if (g_nfc_debug_level >= NFC_DEBUG_MIN) diag_printf(fmt);	\
CYG_MACRO_END

#define MXC_UNLOCK_BLK_END		0xFFFF

extern unsigned int hal_timer_count(void);
static int nfc_program_region(flash_addr_t addr, u8 *buf, u32 len);
static int nfc_erase_region(flash_addr_t addr, u32 len, bool skip_bad, bool verbose);

static int nfc_write_pg_random(u32 pg_no, u32 pg_off, u8 *buf, u32 ecc_force);
static int nfc_read_pg_random(u32 pg_no, u32 pg_off, u32 ecc_force, u32 cs_line,
							  u32 num_of_nand_chips);
static int nfc_erase_blk(u32 ra);
static void print_page(u32 addr, bool spare_only);
static int nfc_read_page(u32 cs_line, u32 pg_no, u32 pg_off);
static int mxc_nfc_scan(bool lowlevel);
static void read_nflash_id(u32 *id, u32 cs_line);
static int nfc_program_blk(u32 ra, u8 *buf, u32 len);

static void print_pkt_16(u16 *pkt, u32 len);

// globals
static int g_ecc_enable = true;
static int g_spare_only_read_ok = true;
static int g_nfc_debug_level = NFC_DEBUG_DEF;
static bool g_nfc_debug_measure = false;
static bool g_is_2k_page = false;
static unsigned int g_block_offset;
static bool g_is_4k_page = false;
static unsigned int g_nfc_version = MXC_NFC_V1; // default to version 1.0
static int	num_of_nand_chips = 1;
static int num_of_nand_chips_for_nandsize = 1;
static int scale_block_cnt = 1;

#define nfc_printf(level, args...) CYG_MACRO_START	\
		if (g_nfc_debug_level >= level)				\
			diag_printf(args);						\
CYG_MACRO_END

#if defined(NFC_V2_0) || defined(NFC_V2_1)
#include <cyg/io/mxc_nfc_v2.h>
#elif defined(NFC_V3_0)
#include <cyg/io/mxc_nfc_v3.h>
#else
#include <cyg/io/mxc_nfc.h>
#endif

#ifndef NAND_LAUNCH_REG
#define NAND_LAUNCH_REG				0xDEADEEEE
#define NAND_CONFIGURATION1_REG		0xDEADEEEE
#define NFC_FLASH_CONFIG2_REG		0xDEADEEEE
#define NFC_FLASH_CONFIG2_ECC_EN	0xDEADEEEE
#define write_nfc_ip_reg(a, b)		CYG_EMPTY_STATEMENT
#endif

#ifndef MXCFLASH_SELECT_MULTI
void flash_query(void *data)
#else
void nandflash_query(void *data)
#endif
{
	u32 id[2];

	nfc_printf(NFC_DEBUG_MIN, "%s@%d data=%p\n", __FUNCTION__, __LINE__, data);

	read_nflash_id(&id[0], 0);
	nfc_printf(NFC_DEBUG_MIN, "%s(ID=0x%02x: 0x%02x, 0x%02x, 0x%02x)\n", __FUNCTION__,
			   id[0] & 0xff, (id[0] >> 8) & 0xff, (id[0] >> 16) & 0xff, id[0] >> 24);
	if (data != NULL) {
		nfc_printf(NFC_DEBUG_MAX, "%s@%d copy flash ID from %p to %p\n",
				__FUNCTION__, __LINE__, &id[0], data);
		memcpy(data, id, sizeof(id));
	}
	nfc_printf(NFC_DEBUG_MAX, "%s@%d called from %p\n", __FUNCTION__, __LINE__,
			__builtin_return_address(0));
}

#ifndef MXCFLASH_SELECT_MULTI
int flash_program_buf(void *addr, void *data, int len)
#else
int nandflash_program_buf(void *addr, void *data, int len)
#endif
{
	nfc_printf(NFC_DEBUG_MAX, "%s(addr=%p, data=%p, len=0x%08x)\n",
			   __FUNCTION__, addr, data, len);
	return nfc_program_region((u32)addr, data, len);
}

#ifndef MXCFLASH_SELECT_MULTI
int flash_erase_block(void *block, unsigned int size)
#else
int nandflash_erase_block(void *block, unsigned int size)
#endif
{
	nfc_printf(NFC_DEBUG_MAX, "%s(block=%p, size=0x%08x)\n",
			   __FUNCTION__, block, size);
	return nfc_erase_region((u32)block, size, 1, 0);
}

#ifndef MXCFLASH_SELECT_MULTI
bool flash_code_overlaps(void *start, void *end)
#else
bool nandflash_code_overlaps(void *start, void *end)
#endif
{
	extern unsigned char _stext[], _etext[];

	return ((((unsigned long)&_stext >= (unsigned long)start) &&
			 ((unsigned long)&_stext < (unsigned long)end)) ||
			(((unsigned long)&_etext >= (unsigned long)start) &&
			 ((unsigned long)&_etext < (unsigned long)end)));
}

#ifndef MXCFLASH_SELECT_MULTI
int flash_hwr_map_error(int e)
#else
int nandflash_hwr_map_error(int e)
#endif
{
	return e;
}

#ifndef MXCFLASH_SELECT_MULTI
int flash_lock_block(void *block)
#else
int nandflash_lock_block(void *block)
#endif
{
	// Not supported yet
	return 0;
}

#ifndef MXCFLASH_SELECT_MULTI
int flash_unlock_block(void *block, int block_size, int blocks)
#else
int nandflash_unlock_block(void *block, int block_size, int blocks)
#endif
{
	// Not supported yet
	return 0;
}

//----------------------------------------------------------------------------
// Now that device properties are defined, include magic for defining
// accessor type and constants.
#include <cyg/io/flash_dev.h>

// Information about supported devices
typedef struct flash_dev_info {
	cyg_uint16	 device_id;
	cyg_uint16	 device_id2;
	cyg_uint16	 device_id3;
	cyg_uint16	 device_id4;
	cyg_uint16	 page_size;
	cyg_uint16	 spare_size;
	cyg_uint32	 pages_per_block;
	cyg_uint32	 block_size;
	cyg_int32	 block_count;
	cyg_uint32	 device_size;
	cyg_uint32	 port_size;		// x8 or x16 IO
	cyg_uint32	 type;			// SLC vs MLC
	cyg_uint32	 options;
	cyg_uint32	 fis_start_addr;
	cyg_uint32	 bi_off;
	cyg_uint32	 bbt_blk_max_nr;
	cyg_uint8	 vendor_info[96];
	cyg_uint32	 col_cycle;		   // number of column address cycles
	cyg_uint32	 row_cycle;		   // number of row address cycles
	cyg_uint32	 max_bad_blk;
} flash_dev_info_t;

static const flash_dev_info_t *flash_dev_info;
static const flash_dev_info_t supported_devices[] = {
#include <cyg/io/mxc_nand_parts.inl>
};
#define NUM_DEVICES						NUM_ELEMS(supported_devices)

#define COL_CYCLE						flash_dev_info->col_cycle
#define ROW_CYCLE						flash_dev_info->row_cycle
#define NF_PG_SZ						(flash_dev_info->page_size * num_of_nand_chips)
#define NF_SPARE_SZ						(flash_dev_info->spare_size * num_of_nand_chips)
#define NF_PG_PER_BLK					flash_dev_info->pages_per_block
#define NF_DEV_SZ						(flash_dev_info->device_size * num_of_nand_chips_for_nandsize)
#define NF_BLK_SZ						(flash_dev_info->block_size * num_of_nand_chips)
#define NF_BLK_CNT						(flash_dev_info->block_count / scale_block_cnt)
#define NF_VEND_INFO					flash_dev_info->vendor_info
#define NF_OPTIONS						flash_dev_info->options
#define NF_BBT_MAX_NR					flash_dev_info->bbt_blk_max_nr
#define NF_OPTIONS						flash_dev_info->options
#define NF_BI_OFF						flash_dev_info->bi_off

#define MXC_NAND_ADDR_MASK				(NF_DEV_SZ - 1)
#define BLOCK_TO_OFFSET(blk)			((blk) * NF_PG_PER_BLK * NF_PG_SZ)
#define BLOCK_TO_PAGE(blk)				((blk) * NF_PG_PER_BLK)
#define BLOCK_PAGE_TO_OFFSET(blk, pge)	(((blk) * NF_PG_PER_BLK + (pge)) * NF_PG_SZ)
#define OFFSET_TO_BLOCK(offset)			((u32)((offset) / (NF_PG_SZ * NF_PG_PER_BLK)))
#define OFFSET_TO_PAGE(offset)			((u32)((offset) / NF_PG_SZ) % NF_PG_PER_BLK)

static u8 *g_bbt, *g_page_buf;
static u32 g_bbt_sz;
static bool mxcnfc_init_ok;
static bool mxc_nfc_scan_done;

// this callback allows the platform specific function to be called right
// after flash_dev_query()
nfc_setup_func_t *nfc_setup = NULL;

// this callback allows the platform specific iomux setup
nfc_iomuxsetup_func_t *nfc_iomux_setup = NULL;

static flash_addr_t flash_region_start;
static flash_addr_t flash_region_end;
static int flash_enable;

/* This assumes reading the flash with monotonically increasing flash addresses */
static flash_addr_t nfc_l_to_p(flash_addr_t addr)
{
	if (g_block_offset == 0) {
		return addr & MXC_NAND_ADDR_MASK;
	} else {
		flash_addr_t ra;
		u32 block = (addr & MXC_NAND_ADDR_MASK) / NF_BLK_SZ;
		u32 offset = addr % NF_BLK_SZ;

		ra = (block + g_block_offset) * NF_BLK_SZ + offset;
		if (offset == 0) {
			nfc_printf(NFC_DEBUG_MIN,
					   "Remapping block %u at addr 0x%08llx to block %u at addr 0x%08llx\n",
					   block, (u64)addr, block + g_block_offset, (u64)ra);
		}
		return ra;
	}
}

static int flash_addr_valid(flash_addr_t addr)
{
	if (!flash_enable) {
		nfc_printf(NFC_DEBUG_MIN, "No flash area enabled\n");
		return 1;
	}
	if (addr < flash_region_start || addr >= flash_region_end) {
		diag_printf("Flash address 0x%08llx is outside valid region 0x%08llx..0x%08llx\n",
					(u64)addr, (u64)flash_region_start, (u64)flash_region_end);
		return 0;
	}
	return 1;
}

/* FIXME: we should pass flash_addr_t as arguments */
void mxc_flash_enable(void *start, void *end)
{
	flash_addr_t s = (unsigned long)start & MXC_NAND_ADDR_MASK;
	flash_addr_t e = ((unsigned long)end - 1) & MXC_NAND_ADDR_MASK;

	if (start == end)
		return;
	if (flash_enable++ == 0) {
		flash_region_start = s;
		flash_region_end = e;
		diag_printf1("Enabling flash region 0x%08llx..0x%08llx\n",
					 (u64)s, (u64)e);
		g_block_offset = 0;
	} else {
		if (s < flash_region_start ||
			e > flash_region_end) {
			diag_printf("** WARNING: Enable 0x%08llx..0x%08llx outside enabled flash region 0x%08llx..0x%08llx\n",
						(u64)s, (u64)e, (u64)flash_region_start, (u64)flash_region_end);
		}
	}
}

void mxc_flash_disable(void *start, void *end)
{
	flash_addr_t s = (unsigned long)start & MXC_NAND_ADDR_MASK;
	flash_addr_t e = ((unsigned long)end - 1) & MXC_NAND_ADDR_MASK;

	if (start == end)
		return;
	if (flash_enable) {
		if (--flash_enable == 0) {
			diag_printf1("Disabling flash region 0x%08llx..0x%08llx\n",
						 (u64)s, (u64)e);
			if (s != flash_region_start ||
				e != flash_region_end) {
				diag_printf("** Error: Disable 0x%08llx..0x%08llx not equal to enabled flash region 0x%08llx..0x%08llx\n",
						(u64)s, (u64)e, (u64)flash_region_start, (u64)flash_region_end);
			}
		}
	} else {
		diag_printf("** Error: unbalanced call to flash_disable()\n");
	}
}

int
#ifndef MXCFLASH_SELECT_MULTI
flash_hwr_init(void)
#else
nandflash_hwr_init(void)
#endif
{
	u32 id[2];
	int i;

	nfc_printf(NFC_DEBUG_MAX, "%s()\n", __FUNCTION__);

	if (nfc_iomux_setup)
		nfc_iomux_setup();

	NFC_SET_NFC_ACTIVE_CS(0);
	NFC_CMD_INPUT(FLASH_Reset);

	// Look through table for device data
	flash_dev_query(&id[0]);

	flash_dev_info = supported_devices;
	for (i = 0; i < NUM_DEVICES; i++) {
		if ((flash_dev_info->device_id == (id[0] & 0xffff)) &&
			(flash_dev_info->device_id2 == 0xFFFF ||
			 flash_dev_info->device_id2 == (id[0] >> 16)))
			break;
		flash_dev_info++;
	}

	// Did we find the device? If not, return error.
	if (NUM_DEVICES == i) {
		diag_printf("Unrecognized NAND part: 0x%02x, 0x%02x, 0x%02x, 0x%02x\n",
					id[0] & 0xff, (id[0] >> 8) & 0xff, (id[0] >> 16) & 0xff, id[0] >> 24);
		return FLASH_ERR_DRV_WRONG_PART;
	}

	mxcnfc_init_ok = true;

	if (NF_PG_SZ == 2048) {
		g_is_2k_page = true;
		g_spare_only_read_ok = false;
	}
	if (NF_PG_SZ == 4096) {
		g_is_4k_page = true;
		g_spare_only_read_ok = false;
	}

	nfc_printf(NFC_DEBUG_MED, "%s(): %d out of NUM_DEVICES=%d, id=0x%02x\n",
			   __FUNCTION__, i, NUM_DEVICES, flash_dev_info->device_id);

	if (nfc_setup)
		g_nfc_version = nfc_setup(NF_PG_SZ / num_of_nand_chips, flash_dev_info->port_size,
								flash_dev_info->type, num_of_nand_chips);

	diag_printf1("NFC version: %02x\n", g_nfc_version);
	if (g_nfc_version >= MXC_NFC_V3) {
		for (i = 2; i <= NUM_OF_CS_LINES; i++) {
			u32 id_tmp[2];
			read_nflash_id(&id_tmp[0], i - 1);
			if (id[0] != id_tmp[0]) {
				break;
			}
			/* Support interleave with 1, 2, 4, 8 chips */
			if (i == (num_of_nand_chips * 2)) {
				num_of_nand_chips = i;
			}
			NFC_CMD_INPUT(FLASH_Reset);
		}

		if (nfc_setup && (num_of_nand_chips > 1)) {
			nfc_setup(NF_PG_SZ / num_of_nand_chips, flash_dev_info->port_size,
						   flash_dev_info->type, num_of_nand_chips);
		}
	}

	NFC_ARCH_INIT();

	g_bbt_sz = NF_BLK_CNT / 4;
	g_bbt = malloc(g_bbt_sz); // two bit for each block
	if (g_bbt == NULL) {
		diag_printf("%s(): failed to allocate %d byte for bbt\n", __FUNCTION__, g_bbt_sz);
		return FLASH_ERR_PROTOCOL;
	}

	g_page_buf = malloc(NF_PG_SZ); // for programming less than one page size buffer
	if (g_page_buf == NULL) {
		diag_printf("%s(): failed to allocate %d byte page buffer\n", __FUNCTION__,
					NF_PG_SZ);
		return FLASH_ERR_PROTOCOL;
	}
	memset(g_bbt, 0, g_bbt_sz);

	/* For now cap off the Device size to 2GB */
	i = 1;
	while ((i <= num_of_nand_chips) && ((NF_DEV_SZ * i) < 0x80000000)) {
		num_of_nand_chips_for_nandsize = i;
		i *= 2;
	}

	scale_block_cnt = num_of_nand_chips / num_of_nand_chips_for_nandsize;
	// Hard wired for now
	flash_info.block_size = NF_BLK_SZ;
	flash_info.blocks = NF_BLK_CNT - CYGNUM_FLASH_NAND_BBT_BLOCKS;
	flash_info.start = (void *)MXC_NAND_BASE_DUMMY;
	flash_info.end = (void *)(MXC_NAND_BASE_DUMMY + NF_DEV_SZ -
							  CYGNUM_FLASH_NAND_BBT_BLOCKS * NF_BLK_SZ);

	mxc_nfc_scan(false); // look for table

	diag_printf1("%s(): block_size=0x%08x, blocks=0x%08x, start=%p, end=%p\n",
				 __FUNCTION__, flash_info.block_size, flash_info.blocks,
				 flash_info.start, flash_info.end);

	return FLASH_ERR_OK;
}

// used by redboot/current/src/flash.c
int mxc_nand_fis_start(void)
{
	return flash_dev_info->fis_start_addr * num_of_nand_chips;
}

static inline u8 get_byte(cyg_uint16 *buf, int offs)
{
	cyg_uint16 word = buf[offs >> 1];
	if (offs & 1) {
		return word >> 8;
	}
	return word & 0xff;
}

static inline void store_byte(cyg_uint16 *buf, int offs, u8 val)
{
	cyg_uint16 word = buf[offs >> 1];

	if (offs & 1) {
		word = (word & 0x00ff) | ((u16)val << 8);
	} else {
		word = (word & 0xff00) | val;
	}
	buf[offs >> 1] = word;
}

static inline bool nfc_verify_addr(unsigned long dst, unsigned long len)
{
	if (dst < NAND_MAIN_BUF0 || dst + len >= NAND_SPAR_BUF3 + NFC_SPARE_BUF_SZ) {
		diag_printf("%s: Bad NFC Buffer address 0x%08lx\n", __FUNCTION__, dst);
		return false;
	}
	return true;
}

static void nfc_buf_read(void *dst, unsigned long src, u32 len)
{
	u16 *s = (u16 *)(src & ~1);
	u8 *bp = dst;

	if (len == 0) {
		return;
	}
	if (src + len < src) {
		diag_printf("%s: Bad address range 0x%08lx .. 0x%08lx\n", __FUNCTION__,
					src, src + len);
	}
	if ((unsigned long)dst + len < (unsigned long)dst) {
		diag_printf("%s: Bad address range 0x%08lx .. 0x%08lx\n", __FUNCTION__,
					(unsigned long)dst, (unsigned long)dst + len);
	}
	if (src < NAND_MAIN_BUF0 || src + len >= NAND_SPAR_BUF3 + NF_PG_SZ) {
		diag_printf("%s: Bad NFC Buffer address 0x%08lx\n", __FUNCTION__, src);
		return;
	}
	if ((unsigned long)dst >= NAND_MAIN_BUF0 &&
		(unsigned long)dst < NAND_SPAR_BUF3 + NF_PG_SZ) {
		diag_printf("%s: Bad memory address 0x%08lx\n", __FUNCTION__,
					(unsigned long)dst);
		return;
	}
	if (src & 1) {
		*bp++ = get_byte(s, 1);
		s++;
		len--;
	}
	if ((unsigned long)bp & 1) {
		while (len > 1) {
			u16 word = *s++;
			*bp++ = word & 0xff;
			*bp++ = word >> 8;
			len -= 2;
		}
	} else {
		u16 *wp = (u16 *)bp;

		while (len > 1) {
			*wp++ = *s++;
			len -= 2;
		}
		bp = (u8*)wp;
	}
	if (len != 0) {
		u16 word = *s;
		*bp = word & 0xff;
	}
}

static void nfc_buf_write(unsigned long dst, void *src, u32 len)
{
	u8 *bp = src;
	u16 *d = (u16 *)(dst & ~1);

	if (len == 0) {
		return;
	}
	if (!nfc_verify_addr(dst, len)) {
		return;
	}
	diag_printf1("Copying %u byte from %p..%p to flash buffer %08lx..%08lx\n",
				len, bp, bp + len - 1, dst, dst + len - 1);
	if (dst & 1) {
		store_byte(d, 1, *bp);
		d++;
		bp++;
		len--;
	}
	if ((unsigned long)bp & 1) {
		while (len > 1) {
			u16 word;
			word = *bp++;
			word |= (u16)(*bp++) << 8;
			*d++ = word;
			len -= 2;
		}
	} else {
		u16 *wp = (u16 *)bp;
		while (len > 1) {
			*d++ = *wp++;
			len -= 2;
		}
		bp = (u8 *)wp;
	}
	if (len != 0) {
		store_byte(d, 1, *bp);
	}
}

#ifndef NFC_V3_0
/*!
 * Starts the address input cycles for different operations as defined in ops.
 *
 * @param ops			operations as defined in enum nfc_addr_ops
 * @param pg_no			page number offset from 0
 * @param pg_off		byte offset within the page
 * @param is_erase		don't care for earlier NFC
 * @param cs_line		 don't care for earlier NFC
 */
static void start_nfc_addr_ops(u32 ops, u32 pg_no, u32 pg_off, u32 is_erase,
				   u32 cs_line, u32 num_of_chips)
{
	int i;

	switch (ops) {
	case FLASH_Read_ID:
		/* Only supports one NAND chip (CS0) */
		if (cs_line != 0)
			return;
		NFC_ADDR_INPUT(0);
		return;
	case FLASH_Read_Mode1:
	case FLASH_Program:
		for (i = 0; i < COL_CYCLE; i++, pg_off >>= 8) {
			NFC_ADDR_INPUT(pg_off & 0xFF);
		}
		// don't break on purpose
	case FLASH_Block_Erase:
		for (i = 0; i < ROW_CYCLE; i++, pg_no >>= 8) {
			NFC_ADDR_INPUT(pg_no & 0xFF);
		}
		break;
	default:
		diag_printf("!!!!!! %s(): wrong ops: %d !!!!!\n", __FUNCTION__, ops);
		return;
	}
}
#endif					// #ifndef NFC_V3_0

static void read_nflash_id(u32 *id, u32 cs_line)
{
	volatile u32 *ptr = (volatile u32*)NAND_MAIN_BUF0;

	nfc_printf(NFC_DEBUG_MIN, "%s: read flash id from chip %d @ %p\n",
			   __FUNCTION__, cs_line, ptr);

	NFC_PRESET(MXC_UNLOCK_BLK_END);
	NFC_SET_NFC_ACTIVE_CS(cs_line);
	NFC_CMD_INPUT(FLASH_Read_ID);

	start_nfc_addr_ops(FLASH_Read_ID, 0, 0, 0, cs_line, num_of_nand_chips);
	NFC_DATA_OUTPUT(RAM_BUF_0, FDO_FLASH_ID, g_ecc_enable);

	*id++ = *ptr++;
	*id++ = *ptr++;
}

static void mark_blk_bad(unsigned int block, unsigned char *buf,
						 enum blk_bad_type bad_type)
{
	unsigned int off = block >> 2;			// byte offset - each byte can hold status for 4 blocks
	unsigned int sft = (block & 3) << 1;	// bit shift 0, 2, 4, 6
	unsigned char val = buf[off];

	if (block > NF_BLK_CNT) {
		diag_printf("%s: Block number %u out of range: 0..%u\n", __FUNCTION__,
					block, NF_BLK_CNT - 1);
		return;
	}
	val = (val & ~(3 << sft)) | (bad_type << sft);
	buf[off] = val;
}

/*!
 * Checks to see if a block is bad. If buf is not NULL, it indicates a valid
 * BBT in the RAM. In this case, it assumes to have 2-bit to represent each
 * block for good or bad
 *				* 11b:	block is good
 *				* 00b:	block is factory marked bad
 *				* 01b:	block is marked bad due to wear
 *				* 10b:	block is marked reserved (for BBT)
 * If buf is NULL, then it indicates a low level scan based on the certain
 * offset value in certain pages and certain offset to be non-0xFF. In this
 * case, the HW ECC will be turned off.
 *
 * @param block		0-based block number
 * @param buf		BBT buffer. Could be NULL (see above explanation)
 *
 * @return			1 if bad block; 0 otherwise
 */
static int nfc_is_badblock(u32 block, u8 *buf)
{
	u32 off;	   // byte offset
	u32 sft;	   // bit shift 0, 2, 4, 6
	flash_addr_t addr;
	u16 temp, i;
	int res;
	u32 pg_no;

	if (buf) {
		// use BBT
		off = block >> 2;		// byte offset
		sft = (block & 3) << 1;	 // bit shift 0, 2, 4, 6
		res = (buf[off] >> sft) & 0x3;
		if (res) {
			addr = BLOCK_TO_OFFSET(block);
			diag_printf1("Block %u at 0x%08llx is marked %s (%d) in BBT@%p[%02x] mask %02x\n",
						 block, (u64)addr, res == BLK_RESERVED ? "reserved" :
						 res == BLK_BAD_FACTORY ? "factory bad" : "runtime bad",
						 res, buf, off, 3 << sft);
		}
		return res;
	}

	// need to do low level scan with ECC off
	if (NF_OPTIONS & NAND_BBT_SCANLSTPAGE) {
		if (g_is_4k_page || g_is_2k_page) {
			addr = (block + 1) * NF_BLK_SZ - NF_PG_SZ;
			pg_no = addr / NF_PG_SZ;
			for (i = 0; i < num_of_nand_chips; i++) {
				// we don't do partial page read here. No ecc either
				nfc_read_pg_random(pg_no, 0, ECC_FORCE_OFF, i, num_of_nand_chips);
				temp = readw((u32)NAND_MAIN_BUF0 + NF_BI_OFF);
				if ((temp & 0xFF) != 0xFF) {
					return BLK_BAD_FACTORY;
				}
			}
		} else {
			diag_printf("only 2K/4K page is supported\n");
			// die here -- need to fix the SW
			while (1);
		}
		return 0;
	}
	addr = block * NF_BLK_SZ;
	pg_no = addr / NF_PG_SZ;
	for (i = 0; i < num_of_nand_chips; i++) {
		nfc_read_pg_random(pg_no, 0, ECC_FORCE_OFF, i, num_of_nand_chips); // no ecc
		if (g_is_2k_page || g_is_4k_page) {
			temp = readw(NAND_MAIN_BUF0 + NF_BI_OFF);
		} else {
			temp = readw(NAND_SPAR_BUF0 + 4) >> 8; // BI is at 5th byte in spare area
		}
		if ((temp & 0xFF) != 0xFF) {
			return BLK_BAD_FACTORY;
		}
	}
	if (NF_OPTIONS & NAND_BBT_SCAN2NDPAGE) {
		addr += NF_PG_SZ;
		pg_no++;
		for (i = 0; i < num_of_nand_chips; i++) {
			nfc_read_pg_random(pg_no, 0, ECC_FORCE_OFF, i, num_of_nand_chips); // no ecc
			if (g_is_2k_page || g_is_4k_page) {
				temp = readw(NAND_MAIN_BUF0 + NF_BI_OFF);
			} else {
				temp = readw(NAND_SPAR_BUF0 + 4) >> 8; // BI is at 5th byte in spare area
			}
			if ((temp & 0xFF) != 0xFF) {
				return BLK_BAD_FACTORY;
			}
		}
	}
	return 0;
}

static inline void mxc_nfc_buf_clear(unsigned long buf, u8 pattern, int size)
{
	int i;
	u16 *p = (u16 *)buf;
	u16 fill = pattern;

	fill = (fill << 8) | pattern;
	for (i = 0; i < size >> 1; i++) {
		p[i] = fill;
	}
}

#ifdef CYGHWR_FLASH_NAND_BBT_HEADER
/*
 * check_short_pattern - [GENERIC] check if a pattern is in the buffer
 * @buf:	the buffer to search
 * @td:		search pattern descriptor
 *
 * Check for a pattern at the given place. Used to search bad block
 * tables and good / bad block identifiers.
*/
static int check_short_pattern(void *buf, struct nand_bbt_descr *td)
{
	int i;

	for (i = 0; i < td->len; i++) {
		if (get_byte(buf, td->offs + i) != td->pattern[i]) {
			return -1;
		}
	}
	return 0;
}

static int nfc_write_page(u32 pg_no, u32 pg_off, u32 ecc_force);
/*
 * Program g_bbt into the NAND block with offset at g_main_bbt_addr.
 * This assumes that the g_bbt has been built already.
 *
 * If g_main_bbt_addr is 0, search for a free block from the bottom 4 blocks (but make
 * sure not re-using the mirror block). If g_mirror_bbt_page is 0, do the same thing.
 * Otherwise, just use g_main_bbt_addr, g_mirror_bbt_page numbers to prgram the
 * g_bbt into those two blocks.
 * todo: need to do the version to see which one is newer.
 *
 * @return	0 if successful; -1 otherwise.
 */
static int mxc_nfc_write_bbt_page(struct nand_bbt_descr *td)
{
	int ret;
	u32 block = td->pages / NF_PG_PER_BLK;
	flash_addr_t addr = td->pages * NF_PG_SZ;

	ret = nfc_erase_blk(addr);
	if (ret != 0) {
		diag_printf("Failed to erase bbt block %u\n", block);
		return ret;
	}
	ret = nfc_write_page(td->pages, 0, 0);
	if (ret != 0) {
		diag_printf("Failed to write bbt block %u\n", block);
		return ret;
	}
	mark_blk_bad(block, g_bbt, BLK_RESERVED);
	return 0;
}

static int mxc_nfc_write_bbt(struct nand_bbt_descr *td, struct nand_bbt_descr *md)
{
	int ret = -1;
	int block;
	int pg_offs = 0;
	int page = 0;
	u16 *buf = (u16 *)NAND_MAIN_BUF0;

	for (block = NF_BLK_CNT - 1; block >= NF_BLK_CNT - td->maxblocks - 1; block--) {
		int pg = block * NF_PG_PER_BLK;

		if ((nfc_is_badblock(block, g_bbt) & 1) == 0) {
			if (md != NULL && md->pages == pg) {
				continue;
			}
			td->pages = pg;
			break;
		}
	}
	if (td->pages < 0) {
		return -1;
	}
	mxc_nfc_buf_clear(NAND_SPAR_BUF0, 0xff, NF_SPARE_SZ);
	mxc_nfc_buf_clear(NAND_MAIN_BUF0, 0xff, NF_PG_SZ);
	diag_printf1("%s: Updating bbt %c%c%c%c version %d\n", __FUNCTION__,
				 td->pattern[0], td->pattern[1], td->pattern[2], td->pattern[3], td->version);
	nfc_buf_write(NAND_SPAR_BUF0 + td->offs, td->pattern, td->len);
	store_byte((u16 *)NAND_SPAR_BUF0, td->veroffs, td->version);

	for (block = 0, pg_offs = 0; block < NF_BLK_CNT; pg_offs++) {
		u16 tmp = 0xffff;
		int i;

		if (pg_offs << 1 >= NF_PG_SZ) {
			ret = mxc_nfc_write_bbt_page(td);
			if (ret != 0) {
				return ret;
			}
			page++;
			mxc_nfc_buf_clear(NAND_SPAR_BUF0, 0xff, NF_SPARE_SZ);
			mxc_nfc_buf_clear(NAND_MAIN_BUF0, 0xff, NF_PG_SZ);
			pg_offs = 0;
		}
		for (i = 0; i < 16 && block < NF_BLK_CNT; i += 2, block++) {
			u8 code = nfc_is_badblock(block, g_bbt);
			if ((code & 1) != 0) {
				tmp &= ~(code << i);
				diag_printf1("%s: bad block %u pattern[%p] 0x%04x mask 0x%04x\n", __FUNCTION__,
							 block, &buf[pg_offs], tmp, 0x03 << i);
			}
		}
		buf[pg_offs] = tmp;
	}
	if (pg_offs > 0) {
		diag_printf1("%s: Writing final bbt block %d page %d\n", __FUNCTION__,
					 td->pages / NF_PG_PER_BLK, page);
		ret = mxc_nfc_write_bbt_page(td);
	}
	return ret;
}

static int mxc_nfc_update_bbt(struct nand_bbt_descr *td, struct nand_bbt_descr *md)
{
	int ret;

	if (td == NULL) {
		return -1;
	}
	if (td->pages < 0 && (md == NULL || md->pages == -1)) {
		td->version = 1;
	} else {
		if (md != NULL && md->pages >= 0) {
			if (md->version >= td->version) {
				td->version = ++md->version;
			} else {
				md->version = ++td->version;
			}
		} else {
			td->version++;
		}
	}
	ret = mxc_nfc_write_bbt(td, md);
	if (ret) {
		diag_printf("** Error: Failed to update main BBT\n");
	}
	if (md) {
		ret = mxc_nfc_write_bbt(md, td);
		if (ret) {
			diag_printf("** Error: Failed to update mirror BBT\n");
		}
	}
	return ret;
}

static int program_bbt_to_flash(void)
{
	return mxc_nfc_update_bbt(g_mxc_nfc_bbt_main_descr, g_mxc_nfc_bbt_mirror_descr);
}
#else
static int program_bbt_to_flash(void)
{
	return 0;
}
#endif

/*!
 * Unconditionally erase a block without checking the BI field.
 * Note that there is NO error checking for passed-in ra.
 *
 * @param ra		starting address in the raw address space (offset)
 *					Must be block-aligned
 * @return			0 if successful; -1 otherwise
 */
static int nfc_erase_blk(u32 ra)
{
	u16 flash_status, i;
	u32 pg_no, pg_off;

	if (g_nfc_version >= MXC_NFC_V3) {
		// combine the two commands for erase
		nfc_reg_write((FLASH_Start_Erase << 8) | FLASH_Block_Erase, NAND_CMD_REG);
		pg_no = ra / NF_PG_SZ;
		pg_off = ra % NF_PG_SZ;
		for (i = 0; i < num_of_nand_chips; i++) {
			start_nfc_addr_ops(FLASH_Block_Erase, pg_no, pg_off, 1, i, num_of_nand_chips);
			// start auto-erase
			nfc_reg_write(NAND_LAUNCH_AUTO_ERASE, NAND_LAUNCH_REG);
			wait_op_done();
			pg_off = 0;
		}
		flash_status = NFC_STATUS_READ();
		// check I/O bit 0 to see if it is 0 for success
		if ((flash_status & ((0x1 << num_of_nand_chips) - 1)) != 0) {
			return -1;
		}
	} else {
		NFC_CMD_INPUT(FLASH_Block_Erase);
		start_nfc_addr_ops(FLASH_Block_Erase, ra / NF_PG_SZ, ra % NF_PG_SZ,
						   1, 0, num_of_nand_chips);
		NFC_CMD_INPUT(FLASH_Start_Erase);

		flash_status = NFC_STATUS_READ();

		// check I/O bit 0 to see if it is 0 for success
		if ((flash_status & 0x1) != 0) {
			return -1;
		}
	}
	return 0;
}

/*!
 * Program a block of data in the flash. This function doesn't do
 * bad block checking. But if program fails, it return error.
 * Note: If "len" is less than a block it will program up to a page's
 *		 boundary. If not within a page boundary, then it fills the
 *		 rest of the page with 0xFF.
 *
 * @param ra		destination raw flash address
 * @param buf		source address in the RAM
 * @param len		len to be programmed
 *
 * @return			0 if successful; -1 otherwise
 */
static int nfc_program_blk(u32 ra, u8 *buf, u32 len)
{
	u32 temp = num_of_nand_chips;

	/* Needed when romupdate is called */
	if (ra == 0)
		num_of_nand_chips = 1;

	for (; len >= NF_PG_SZ; len -= NF_PG_SZ) {
		if (nfc_write_pg_random(ra / NF_PG_SZ, ra % NF_PG_SZ, buf, 0) != 0) {
			return -1;
		}
		ra += NF_PG_SZ;
		buf += NF_PG_SZ;
	}
	if (len != 0) {
		diag_printf1("Clearing flash buffer from %p..%p\n", g_page_buf + len - 1,
					g_page_buf + NF_PG_SZ - 1);
		memset(g_page_buf + len, 0xFF, NF_PG_SZ - len);
		diag_printf1("Copying partial page from %p..%p to %p..%p\n",
					buf, buf + len - 1, g_page_buf, g_page_buf + len);
		memcpy(g_page_buf, buf, len);
		if (nfc_write_pg_random(ra / NF_PG_SZ, ra % NF_PG_SZ, g_page_buf, 0) != 0) {
			num_of_nand_chips = temp;
			return -1;
		}
	}
	num_of_nand_chips = temp;
	return 0;
}

/*!
 * Erase a range of NAND flash good blocks only.
 * It skips bad blocks and update the BBT once it sees new bad block due to erase.
 * @param addr			raw NAND flash address. it has to be block size aligned
 * @param len			number of bytes
 * @param skip_bad		if 1, don't erase bad block; otherwise, always erase
 * @param verbose		use true to print more messages
 *
 * @return				FLASH_ERR_OK (0) if successful; non-zero otherwise
 */
static int nfc_erase_region(flash_addr_t addr, u32 len, bool skip_bad, bool verbose)
{
	u32 sz, blk, update = 0, j = 0;

	nfc_printf(NFC_DEBUG_MED, "%s: addr=0x%08llx len=0x%08x\n",
			   __FUNCTION__, (u64)addr, len);

	if ((addr % NF_BLK_SZ) != 0) {
		diag_printf("Error: flash address 0x%08llx not block aligned\n", addr);
		return FLASH_ERR_INVALID;
	}
	if ((len % NF_BLK_SZ) != 0 || len == 0) {
		diag_printf("Error: invalid length %u (must be > 0 and block aligned)\n", len);
		return FLASH_ERR_INVALID;
	}
	addr &= MXC_NAND_ADDR_MASK;
	// now addr has to be block aligned
	for (sz = 0; sz < len; addr += NF_BLK_SZ, j++, sz += NF_BLK_SZ) {
		if (!flash_addr_valid(addr)) {
			return 0;
		}
		blk = OFFSET_TO_BLOCK(addr);
		if (skip_bad && nfc_is_badblock(blk, g_bbt)) {
			diag_printf("\nSkipping bad block %u at addr 0x%08llx\n",
						blk, (u64)addr);
			continue;
		}
		if (nfc_erase_blk(addr) != 0) {
			diag_printf("\n** Error: Failed to erase block %u at addr 0x%08llx\n",
						blk, (u64)addr);
			mark_blk_bad(blk, g_bbt, BLK_BAD_RUNTIME);
			// we don't need to update the table immediately here since even
			// with power loss now, we should see the same erase error again.
			update++;
			continue;
		}
		if (verbose) {
			if ((j % 0x20) == 0)
				diag_printf("\n%s 0x%08llx: ", skip_bad ? "Erase" : "FORCE erase", (u64)addr);
			diag_printf(".");
		}
	}
	if (update) {
		if (program_bbt_to_flash() != 0) {
			diag_printf("\nError: Failed to update bad block table\n");
			return FLASH_ERR_PROGRAM;
		}
		diag_printf("\nnew bad blocks=%d\n", update);
	}
	return FLASH_ERR_OK;
}

/*!
 * Program a range of NAND flash in blocks only.
 * It skips bad blocks and update the BBT once it sees new bad block due to program.
 * @param addr			raw NAND flash address. it has to be block size aligned
 * @param len			number of bytes
 * @return				FLASH_ERR_OK (0) if successful; non-zero otherwise
 */
static int nfc_program_region(flash_addr_t addr, u8 *buf, u32 len)
{
	u32 sz, blk, update = 0, partial_block_size;

	nfc_printf(NFC_DEBUG_MED, "%s: addr=0x%08llx, len=0x%08x\n",
			   __FUNCTION__, (u64)addr, len);

	if ((addr % (NF_PG_SZ / num_of_nand_chips)) != 0) {
		diag_printf("Error: flash address 0x%08llx not page aligned\n", (u64)addr);
		return FLASH_ERR_INVALID;
	}
	if (len == 0) {
		diag_printf("Error: invalid length\n");
		return FLASH_ERR_INVALID;
	}

	partial_block_size = NF_BLK_SZ - (addr % NF_BLK_SZ);

	mxc_nfc_buf_clear(NAND_SPAR_BUF0, 0xff, NF_SPARE_SZ);
	addr = nfc_l_to_p(addr);
	while (1) {
		if (!flash_addr_valid(addr)) {
			diag_printf("\nToo many bad blocks in flash region 0x%08llx..0x%08llx\n",
						(u64)flash_region_start, (u64)flash_region_end);
			return FLASH_ERR_INVALID;
		}
		blk = OFFSET_TO_BLOCK(addr);
		if (nfc_is_badblock(blk, g_bbt)) {
			diag_printf("\nSkipping bad block %u at addr 0x%08llx\n", blk, addr);
			g_block_offset++;
			goto incr_address;
		}

		sz = (len >= partial_block_size) ? partial_block_size : len;

		if (nfc_program_blk(addr, buf, sz) != 0) {
			update++;
			diag_printf("\nError: Failed to program flash block %u at addr 0x%08llx\n",
						blk, (u64)addr);
			mark_blk_bad(blk, g_bbt, BLK_BAD_RUNTIME);
			// we don't need to update the table immediately here since even
			// with power loss now, we should see the same program error again.
			g_block_offset++;
			goto incr_address;
		}
		diag_printf(".");

		len -= sz;
		buf += sz;
		if (len == 0)
			break;

incr_address:
		addr += partial_block_size;
		partial_block_size = NF_BLK_SZ;
	}
	if (update) {
		if (program_bbt_to_flash() != 0) {
			diag_printf("\nError: Failed to update bad block table\n");
			return -1;
		}
	}
	return FLASH_ERR_OK;
}

/*!
 * Read data from raw NAND flash address to memory. The MSB of the passed-
 * in flash address will be masked off inside the function.
 * It skips bad blocks and read good blocks of data for "len" bytes.
 *
 * @param addr			NAND flash address.
 * @param buf			memory buf where data will be copied to
 * @param len			number of bytes
 * @return				FLASH_ERR_OK (0) if successful; non-zero otherwise
 */
int nfc_read_region(flash_addr_t addr, u8 *buf, u32 len)
{
	u32 start_point = 0, pg_no;
	unsigned int offset = addr % NF_PG_SZ;
	int chk_bad = 1;

	nfc_printf(NFC_DEBUG_MED, "%s: addr=0x%08llx, offset=%03x buf=0x%p, len=0x%08x\n",
			   __FUNCTION__, addr, offset, buf, len);

	if (addr < (u32)flash_info.start || (addr + len) > (u32)flash_info.end || len == 0) {
		diag_printf("** Error: flash address 0x%08llx..0x%08llx outside valid range %p..%p\n",
					(u64)addr, (u64)addr + len - 1, flash_info.start, flash_info.end);
		return FLASH_ERR_INVALID;
	}

	addr = nfc_l_to_p(addr);
	while (len > 0) {
		int i;

		if (!flash_addr_valid(addr)) {
			diag_printf("Too many bad blocks in flash region 0x%08llx..0x%08llx\n",
						(u64)flash_region_start, (u64)flash_region_end);
			return FLASH_ERR_INVALID;
		}
		if (chk_bad) {
			int blk = OFFSET_TO_BLOCK(addr);

			if (nfc_is_badblock(blk, g_bbt)) {
				diag_printf("Skipping bad block %u at addr 0x%08llx\n", blk, (u64)addr);
				addr += NF_BLK_SZ;
				g_block_offset++;
				continue;
			}
			chk_bad = 0;
		}

		pg_no = addr / NF_PG_SZ;
		if (offset != 0) {
			/* Find which interleaved NAND device */
			start_point = offset / (NF_PG_SZ / num_of_nand_chips);
		} else {
			start_point = 0;
		}
		for (i = start_point; i < num_of_nand_chips; i++) {
			int chunk_size = (NF_PG_SZ - offset) / num_of_nand_chips;

			if (chunk_size > len)
				chunk_size = len;
			nfc_printf(NFC_DEBUG_MED, "Reading page %d addr 0x%08llx chip %d len 0x%03x\n",
					   pg_no, (u64)addr, i, chunk_size);
			if (nfc_read_page(i, pg_no, 0) != 0) {
				diag_printf("** Error: Failed to read flash block %u at addr 0x%08llx\n",
							OFFSET_TO_BLOCK(addr), (u64)addr);
				return FLASH_ERR_INVALID;
			}
			// now do the copying
			nfc_buf_read(buf, NAND_MAIN_BUF0 + offset, chunk_size);

			buf += chunk_size;
			len -= chunk_size;
			addr += NF_PG_SZ / num_of_nand_chips - offset;
			offset = 0;
		}
		chk_bad = (addr % NF_BLK_SZ) == 0;
	}

	return FLASH_ERR_OK;
}

/*
 * Support only either program for main area only. Or spare-area only for 512B.
 * If one wants to write to the spare-area, then before calling this function,
 * the spare area NFC RAM buffer has to be setup already. This function doesn't touch
 * the spare area NFC RAM buffer.
 *
 * @param pg_no			page number offset from 0
 * @param pg_off		byte offset within the page
 * @param buf			data buffer in the RAM to be written to NAND flash
 * @param ecc_force		can force ecc to be off. Otherwise, by default it is on
 *						unless the page offset is non-zero
 *
 * @return	0 if successful; non-zero otherwise
 */
// SP-only opearation is not supported anymore !!!
static int nfc_write_pg_random(u32 pg_no, u32 pg_off, u8 *buf, u32 ecc_force)
{
	u16 flash_status;
	u32 ecc = NFC_FLASH_CONFIG2_ECC_EN, i;
	u32 v __attribute__((unused));
	u32 write_count = NF_PG_SZ, start_point = 0, rba, rba_count = 0;

	// the 2nd condition is to test for unaligned page address -- ecc has to be off.
	if (ecc_force == ECC_FORCE_OFF || pg_off != 0) {
		ecc = 0;
	}

	diag_printf1("%s(0x%x, 0x%x, %d)\n", __FUNCTION__, pg_no, pg_off, ecc_force);
	if (g_nfc_version != MXC_NFC_V1) {
		int i;

		for (i = 1; i < NFC_SPARE_BUF_SZ / 16; i++) {
			memcpy((void *)(NAND_SPAR_BUF0 + i * NFC_SPARE_BUF_SZ),
				(void *)(NAND_SPAR_BUF0 + i * 16), 16);
		}
	}
	if (g_nfc_version >= MXC_NFC_V3) {
		/* Check if Page size is greater than NFC buffer */
		do {
			rba = nfc_reg_read(NAND_CONFIGURATION1_REG);
			if ((rba >> 4) & 0x7) {
				nfc_reg_write(rba & ~0x70, NAND_CONFIGURATION1_REG);
			}
			if (write_count <= NFC_BUFSIZE) {
				// No need to worry about the spare area
				nfc_buf_write(NAND_MAIN_BUF0, buf, write_count);
				write_count = 0;
			} else {
				// No need to worry about the spare area
				nfc_buf_write(NAND_MAIN_BUF0, buf, NFC_BUFSIZE);
				write_count -= NFC_BUFSIZE;
				buf += NFC_BUFSIZE;
			}
			// combine the two commands for program
			nfc_reg_write((FLASH_Program << 8) | FLASH_Send_Data, NAND_CMD_REG);

			for (i = start_point; i < num_of_nand_chips; i++) {
				rba = rba_count * ((NF_PG_SZ / num_of_nand_chips) / 512);
				/* Completely wrote out the NFC buffer, break and copy more to the NFC buffer */
				if (rba > 7) {
					rba_count = 0;
					break;
				}

				// For ECC
				v = nfc_reg_read(NFC_FLASH_CONFIG2_REG) & ~NFC_FLASH_CONFIG2_ECC_EN;
				// setup config2 register for ECC enable or not
				write_nfc_ip_reg(v | ecc, NFC_FLASH_CONFIG2_REG);

				start_nfc_addr_ops(FLASH_Program, pg_no, pg_off, 0, i, num_of_nand_chips);

				// start auto-program
				nfc_reg_write(NAND_LAUNCH_AUTO_PROG, NAND_LAUNCH_REG);
				if (i < (num_of_nand_chips - i))
					wait_for_auto_prog_done();
				else
					wait_op_done();
				pg_off = 0;
				rba_count++;
				rba = nfc_reg_read(NAND_CONFIGURATION1_REG);
			}
			flash_status = NFC_STATUS_READ();
			// check I/O bit 0 to see if it is 0 for success
			if ((flash_status & ((0x1 << num_of_nand_chips) - 1)) != 0) {
				return -1;
			}
			start_point = i;
		} while (write_count > 0);
	} else {
		nfc_buf_write(NAND_MAIN_BUF0, buf, NF_PG_SZ);
#ifdef BARKER_CODE_SWAP_LOC
		// To replace the data at offset MXC_NAND_BOOT_LOAD_BARKER with
		// the address of the NFC base. This is needed for certain platforms.
		if (pg_no == 0) {
			diag_printf("\n[INFO]: copy data at 0x%x to spare area and set it to 0x%x\n",
						BARKER_CODE_SWAP_LOC, BARKER_CODE_VAL);
			nfc_reg_write(nfc_reg_read(NFC_BASE + BARKER_CODE_SWAP_LOC), NAND_SPAR_BUF0);
			// todo: set BARKER_CODE_VAL and BARKER_CODE_SWAP_LOC for skye, etc.
			nfc_reg_write(BARKER_CODE_VAL, NFC_BASE + BARKER_CODE_SWAP_LOC);
		}
#endif
		NFC_CMD_INPUT(FLASH_Send_Data);
		start_nfc_addr_ops(FLASH_Program, pg_no, pg_off, 0, 0, num_of_nand_chips);

		NFC_DATA_INPUT(RAM_BUF_0, NFC_MAIN_ONLY, ecc);
		if (g_is_4k_page && PG_2K_DATA_OP_MULTI_CYCLES()) {
			diag_printf("4K page with multi cycle write is not supported\n");
			return -1;
		}
		if (g_is_2k_page && PG_2K_DATA_OP_MULTI_CYCLES()) {
			NFC_DATA_INPUT_2k(RAM_BUF_1);
			NFC_DATA_INPUT_2k(RAM_BUF_2);
			NFC_DATA_INPUT_2k(RAM_BUF_3);
		}
		NFC_CMD_INPUT(FLASH_Program);

		flash_status = NFC_STATUS_READ();
		// check I/O bit 0 to see if it is 0 for success
		if ((flash_status & 0x1) != 0) {
			diag_printf("** Error: failed to program page %u at 0x%08x status=0x%02x\n",
						pg_no, pg_no * NF_PG_SZ + pg_off, flash_status);
			return -1;
		}
	}
	return 0;
}

#ifdef NFC_V3_0
/*
 * Do a page read at random address
 *
 * @param pg_no             page number offset from 0
 * @param pg_off             byte offset within the page
 * @param ecc_force        can force ecc to be off. Otherwise, by default it is on
 *                                    unless the page offset is non-zero
 * @param cs_line            indicates which NAND of interleaved NAND devices is used
 *
 * @return  0 if successful; non-zero otherwise
 */
static int nfc_read_pg_random(u32 pg_no, u32 pg_off, u32 ecc_force, u32 cs_line, u32 num_of_chips)
{
	u32 ecc = NFC_FLASH_CONFIG2_ECC_EN;
	u32 v, res = 0;
	int i;

	// clear the NAND_STATUS_SUM_REG register
	nfc_reg_write(0, NAND_STATUS_SUM_REG);

	// the 2nd condition is to test for unaligned page address -- ecc has to be off.
	if (ecc_force == ECC_FORCE_OFF || pg_off != 0 ) {
		ecc = 0;
	}

	// Take care of config1 for RBA and SP_EN
	v = nfc_reg_read(NAND_CONFIGURATION1_REG) & ~0x71;
	nfc_reg_write(v, NAND_CONFIGURATION1_REG);

	// For ECC
	v = nfc_reg_read(NFC_FLASH_CONFIG2_REG) & ~NFC_FLASH_CONFIG2_ECC_EN;
	// setup config2 register for ECC enable or not
	write_nfc_ip_reg(v | ecc, NFC_FLASH_CONFIG2_REG);

	start_nfc_addr_ops(FLASH_Read_Mode1, pg_no, pg_off, 0, cs_line, num_of_chips);

	if (g_is_2k_page || g_is_4k_page) {
		// combine the two commands for 2k/4k page read
		nfc_reg_write((FLASH_Read_Mode1_LG << 8) | FLASH_Read_Mode1, NAND_CMD_REG);
	} else {
		// just one command is enough for 512 page
		nfc_reg_write(FLASH_Read_Mode1, NAND_CMD_REG);
	}

	// start auto-read
	nfc_reg_write(NAND_LAUNCH_AUTO_READ, NAND_LAUNCH_REG);
	wait_op_done();

	for (i = 1; i < NFC_SPARE_BUF_SZ / 16; i++) {
		memcpy((void *)(NAND_SPAR_BUF0 + i * 16),
			(void *)(NAND_SPAR_BUF0 + i * NFC_SPARE_BUF_SZ), 16);
	}
	v = nfc_reg_read(NAND_STATUS_SUM_REG);
	// test for CS0 ECC error from the STATUS_SUM register
	if ((v & (0x0100 << cs_line)) != 0) {
		// clear the status
		nfc_reg_write(v & ~(0x0100 << cs_line), NAND_STATUS_SUM_REG);
		diag_printf("ECC error from NAND_STATUS_SUM_REG(0x%08lx) = 0x%08x\n",
					NAND_STATUS_SUM_REG, v);
		diag_printf("NAND_ECC_STATUS_RESULT_REG(0x%08lx) = 0x%08x\n", NAND_ECC_STATUS_RESULT_REG,
					nfc_reg_read(NAND_ECC_STATUS_RESULT_REG));
		res = -1;
	}
	return res;
}
#else
// for version V1 and V2 of NFC
static int nfc_read_pg_random(u32 pg_no, u32 pg_off, u32 ecc_force, u32 cs_line,
							  u32 num_of_nand_chips)
{
	u32 t1, ecc = 1;
	u8 t2 = 0, t3 = 0, t4 = 0, t5 = 0, t6 = 0, t7 = 0, t8 = 0;
	int res = 0;

	nfc_printf(NFC_DEBUG_MAX, "%s: reading page %u offset 0x%03x (addr 0x%08llx)\n",
			   __FUNCTION__, pg_no, pg_off, (flash_addr_t)pg_no * NF_PG_SZ + pg_off);

	if (ecc_force == ECC_FORCE_OFF || pg_off != 0 )
		ecc = 0;

	NFC_CMD_INPUT(FLASH_Read_Mode1);
	start_nfc_addr_ops(FLASH_Read_Mode1, pg_no, pg_off, 0, 0, num_of_nand_chips);

	if (g_is_2k_page || g_is_4k_page) {
		NFC_CMD_INPUT(FLASH_Read_Mode1_LG);
	}

	NFC_DATA_OUTPUT(RAM_BUF_0, FDO_PAGE_SPARE, ecc);
	switch (g_nfc_version) {
	case MXC_NFC_V1:
		t1 = readw(ECC_STATUS_RESULT_REG);
		if (g_is_2k_page && PG_2K_DATA_OP_MULTI_CYCLES()) {
			NFC_DATA_OUTPUT(RAM_BUF_1, FDO_PAGE_SPARE, ecc);
			t2 = readw(ECC_STATUS_RESULT_REG);
			NFC_DATA_OUTPUT(RAM_BUF_2, FDO_PAGE_SPARE, ecc);
			t3 = readw(ECC_STATUS_RESULT_REG);
			NFC_DATA_OUTPUT(RAM_BUF_3, FDO_PAGE_SPARE, ecc);
			t4 = readw(ECC_STATUS_RESULT_REG);
		}

		if (ecc && ((t1 & 0xA) != 0x0 || (t2 & 0xA) != 0x0 ||
					(t3 & 0xA) != 0x0 || (t4 & 0xA) != 0x0)) {
			diag_printf("\n** Error: ECC error page %u, col %u: ECC status=0x%x:0x%x:0x%x:0x%x\n",
						pg_no, pg_off, t1, t2, t3, t4);
			res = -1;
			goto out;
		}
		break;
	case MXC_NFC_V1_1:
	case MXC_NFC_V2:
		if (g_is_2k_page && PG_2K_DATA_OP_MULTI_CYCLES()) {
			NFC_DATA_OUTPUT(RAM_BUF_1, FDO_PAGE_SPARE, ecc);
			NFC_DATA_OUTPUT(RAM_BUF_2, FDO_PAGE_SPARE, ecc);
			NFC_DATA_OUTPUT(RAM_BUF_3, FDO_PAGE_SPARE, ecc);
		}
		if (ecc) {
			t1 = nfc_reg_read(ECC_STATUS_RESULT_REG);
			if (g_is_2k_page || g_is_4k_page) {
				t2 = (t1 >> 4) & 0xF;
				t3 = (t1 >> 8) & 0xF;
				t4 = (t1 >> 12) & 0xF;
				if (g_is_4k_page) {
					t5 = (t1 >> 16) & 0xF;
					t6 = (t1 >> 20) & 0xF;
					t7 = (t1 >> 24) & 0xF;
					t8 = (t1 >> 28) & 0xF;
				}
			}
			if ((t1 = (t1 & 0xF)) > 4 || t2 > 4 || t3 > 4 || t4 > 4 ||
				t5 > 4 || t6 > 4 || t7 > 4 || t8 > 4) {
				diag_printf("\n** Error: ECC error reading block %u page %u\n",
							pg_no / NF_PG_PER_BLK, pg_no % NF_PG_PER_BLK);
				diag_printf("   ECC status=%x:%x:%x:%x:%x:%x:%x:%x\n",
							t1, t2, t3, t4, t5, t6, t7, t8);
				res = -1;
				goto out;
			}
		}
		break;
	default:
		diag_printf("Unknown NFC version: %d\n", g_nfc_version);
		return -1;
	}
	if (g_nfc_version != MXC_NFC_V1) {
		int i;

		for (i = 1; i < NFC_SPARE_BUF_SZ / 16; i++) {
			memcpy((void *)(NAND_SPAR_BUF0 + i * 16),
				   (void *)(NAND_SPAR_BUF0 + i * NFC_SPARE_BUF_SZ), 16);
		}
	}
#ifdef BARKER_CODE_SWAP_LOC
	// To replace the data at offset BARKER_CODE_SWAP_LOC with the address of the NFC base
	// This is needed for certain platforms
	if (pg_no == 0) {
		diag_printf("\n[INFO]: copy back data from spare to 0x%x\n", BARKER_CODE_SWAP_LOC);
		nfc_reg_write(nfc_reg_read(NAND_SPAR_BUF0), NFC_BASE + BARKER_CODE_SWAP_LOC);
	}
#endif

out:
	return res;
}
#endif			// ifndef NFC_V3_0

/*!
 * Read a page's both main and spare area from NAND flash to the internal RAM buffer.
 * It always reads data to the internal buffer 0.
 *
 * @param cs_line	which NAND device is used
 * @param pg_no	   page number of the device
 * @param pg_off	offset within a page
 *
 * @return				0 if no error or 1-bit error; -1 otherwise
 */
static int nfc_read_page(u32 cs_line, u32 pg_no, u32 pg_off)
{
	return nfc_read_pg_random(pg_no, pg_off, ECC_FORCE_ON, cs_line, num_of_nand_chips);
}

static int nfc_write_page(u32 pg_no, u32 pg_off, u32 ecc_force)
{
	u16 flash_status;
	u32 ecc = NFC_FLASH_CONFIG2_ECC_EN;

	diag_printf1("Writing page %u addr 0x%08llx\n",
				 pg_no, (u64)pg_no * NF_PG_SZ + pg_off);
	if (ecc_force == ECC_FORCE_OFF || pg_off != 0) {
		ecc = 0;
	}

	if (g_nfc_version != MXC_NFC_V1) {
		int i;

		for (i = NFC_SPARE_BUF_SZ / 16 - 1; i >= 0; i--) {
			memcpy((void *)(NAND_SPAR_BUF0 + i * NFC_SPARE_BUF_SZ),
				(void *)(NAND_SPAR_BUF0 + i * 16), 16);
		}
	}
	if (g_nfc_version == MXC_NFC_V3) {
		int i;
		u32 v __attribute__((unused));
		u32 start_point = 0, rba, rba_count = 0;

		rba = nfc_reg_read(NAND_CONFIGURATION1_REG);
		if ((rba >> 4) & 0x7) {
			nfc_reg_write(rba & ~0x70, NAND_CONFIGURATION1_REG);
		}
		// combine the two commands for program
		nfc_reg_write((FLASH_Program << 8) | FLASH_Send_Data, NAND_CMD_REG);

		for (i = start_point; i < num_of_nand_chips; i++) {
			rba = rba_count * ((NF_PG_SZ / num_of_nand_chips) / 512);
			/* Completely wrote out the NFC buffer, break and copy more to the NFC buffer */
			if (rba > 7) {
				rba_count = 0;
				break;
			}

			// For ECC
			v = nfc_reg_read(NFC_FLASH_CONFIG2_REG) & ~NFC_FLASH_CONFIG2_ECC_EN;
			// setup config2 register for ECC enable or not
			write_nfc_ip_reg(v | ecc, NFC_FLASH_CONFIG2_REG);

			start_nfc_addr_ops(FLASH_Program, pg_no, pg_off, 0, i, num_of_nand_chips);

			// start auto-program
			nfc_reg_write(NAND_LAUNCH_AUTO_PROG, NAND_LAUNCH_REG);
			if (i < (num_of_nand_chips - i))
				wait_for_auto_prog_done();
			else
				wait_op_done();
			pg_off = 0;
			rba_count++;
		}
		flash_status = NFC_STATUS_READ();
		// check I/O bit 0 to see if it is 0 for success
		if ((flash_status & ((0x1 << num_of_nand_chips) - 1)) != 0) {
			return -1;
		}
		rba = nfc_reg_read(NAND_CONFIGURATION1_REG);
		start_point = i;
	} else {
		NFC_CMD_INPUT(FLASH_Send_Data);
		start_nfc_addr_ops(FLASH_Program, pg_no, pg_off, 0, 0, num_of_nand_chips);

		NFC_DATA_INPUT(RAM_BUF_0, NFC_MAIN_ONLY, ecc);
		if (g_is_4k_page && PG_2K_DATA_OP_MULTI_CYCLES()) {
			diag_printf("4K page with multi cycle write is not supported\n");
			return -1;
		}
		if (g_is_2k_page && PG_2K_DATA_OP_MULTI_CYCLES()) {
			NFC_DATA_INPUT_2k(RAM_BUF_1);
			NFC_DATA_INPUT_2k(RAM_BUF_2);
			NFC_DATA_INPUT_2k(RAM_BUF_3);
		}
		NFC_CMD_INPUT(FLASH_Program);

		flash_status = NFC_STATUS_READ();
		if ((flash_status & 0x1) != 0) {
			diag_printf("** Error: failed to program page %u at addr 0x%08llx\n",
						pg_no, (u64)pg_no * NF_PG_SZ + pg_off);
			return -1;
		}
	}
	return 0;
}

// Read data into buffer
#ifndef MXCFLASH_SELECT_MULTI
int flash_read_buf(void *addr, void *data, int len)
#else
int nandflash_read_buf(void *addr, void *data, int len)
#endif
{
	flash_addr_t flash_addr = (unsigned long)addr;
	return nfc_read_region(flash_addr, data, len);
}

void mxc_nfc_print_info(void)
{
	diag_printf("[0x%08x bytes]: %u blocks of %u pages of %u bytes each.\n",
				NF_DEV_SZ, NF_BLK_CNT,
				NF_PG_PER_BLK, NF_PG_SZ);
}

static int mxc_nfc_isbad_bbt(u16 *bbt, int block)
{
	cyg_uint8 res;

	block <<= 1;
	res = (get_byte(bbt, block >> 3) >> (block & 0x06)) & 0x03;
	res ^= 0x03;
	return res;
}

static int mxc_nfc_search_bbt(struct nand_bbt_descr *td)
{
	int i;

	td->pages = -1;
	for (i = 0; i < NF_BBT_MAX_NR; i++) {
		u32 blk = NF_BLK_CNT - i - 1;
		flash_addr_t addr = blk * NF_BLK_SZ;

		if (nfc_read_pg_random(addr / NF_PG_SZ, addr % NF_PG_SZ,
							   ECC_FORCE_ON, 0, num_of_nand_chips) != 0) {
			diag_printf("Failed to read bbt page %u at 0x%08llx\n",
						(u32)(addr / NF_PG_SZ), addr);
			continue;
		}
		if (check_short_pattern((void *)NAND_SPAR_BUF0, td) == 0) {
			diag_printf1("found BBT at block %u addr %08llx\n", blk, (u64)addr);
			td->pages = blk * NF_PG_PER_BLK;
			td->version = get_byte((void *)NAND_SPAR_BUF0, td->veroffs);
			mark_blk_bad(blk, g_bbt, BLK_RESERVED);
			diag_printf1("Found version %d BBT at block %d (0x%08llx)\n",
						 td->version, td->pages / NF_PG_PER_BLK,
						 (u64)td->pages * NF_PG_SZ);
			return 0;
		}
	}
	return 1;
}

/*
 * Look for the BBT depending on the passed-in lowlevel value.
 * @param	lowlevel	If true, then it does a low level scan based on factory
 *						marked BI(block info) field with ECC off to decide if a
 *						block is bad.
 *						If false, then it checks to see if an existing BBT in the
 *						flash or not. If not, then it returns -1. If yes, it will
 *						prints out the number of bad blocks.
 *
 * @return	number of bad blocks for the whole nand flash
 *
 * Note: For a brand new flash, this function has to be called with
 *		 lowlevel=true.
 *
 *
 */
static int mxc_nfc_scan(bool lowlevel)
{
	u32 bad = 0, i;
	u32 count1 = 0, count2 = 0;
	u8 *buf = NULL;
	struct nand_bbt_descr *td = g_mxc_nfc_bbt_main_descr;
	struct nand_bbt_descr *md = g_mxc_nfc_bbt_mirror_descr;

	nfc_printf(NFC_DEBUG_MAX, "%s()\n", __FUNCTION__);
	mxc_nfc_scan_done = 0;

	if (g_nfc_debug_measure) {
		count1 = hal_timer_count();
	}
	// read out the last 4 blocks for marker
	// need to keep where is the td and md block number
	if (!lowlevel) {
		struct nand_bbt_descr *bd;

		diag_printf1("Searching for BBT in the flash ...\n");
		if (mxc_nfc_search_bbt(td) != 0) {
			diag_printf("No main BBT found in flash\n");
		}
		if (md && mxc_nfc_search_bbt(md) != 0) {
			diag_printf("No mirror BBT found in flash\n");
		}
		if (td->pages == -1 && (!md || md->pages == -1)) {
			diag_printf("No BBT found. Need to do \"nand scan\" first\n");
			return -1;
		}
		if (td->pages >= 0 && (md == NULL || md->version <= td->version)) {
			bd = td;
			nfc_printf(NFC_DEBUG_MIN, "Using normal bbt at page %d\n", bd->pages);
		} else if (md != NULL && md->pages >= 0) {
			bd = md;
			nfc_printf(NFC_DEBUG_MIN, "Using mirror bbt at page %d\n", bd->pages);
		} else {
			diag_printf("** Error: Failed to read bbt from flash\n");
			return -1;
		}
		nfc_read_page(0, bd->pages, 0);
		for (i = 0; i < NF_BLK_CNT; i++) {
			int res = mxc_nfc_isbad_bbt((u16 *)NAND_MAIN_BUF0, i);
			if (res) {
				// construct the bad block table
				mark_blk_bad(i, g_bbt, res);
				bad++;
			}
		}
		buf = g_bbt;
	} else {
		diag_printf("Doing low level scan to construct BBT\n");
		for (i = 0; i < NF_BLK_CNT; i++) {
			int res = nfc_is_badblock(i, buf);
			if (res) {
				// construct the bad block table
				if (!buf)
					mark_blk_bad(i, g_bbt, res);
				bad++;
			}
		}
	}
	diag_printf1("Total bad blocks: %d\n", bad);
	if (g_nfc_debug_measure) {
		count2 = hal_timer_count();
		diag_printf("counter1=0x%x, counter2=0x%x, diff=0x%x (%u usec)\n",
					count1, count2, count2 - count1,
					(count2 - count1) * 1000000 / 32768);
	}
	mxc_nfc_scan_done = 1;
	return bad;
}

////////////////////////// "nand" commands support /////////////////////////
// Image management functions
local_cmd_entry("info",
				"Show nand flash info (number of good/bad blocks)",
				"",
				nand_info,
				NAND_cmds
		   );

local_cmd_entry("show",
				"Show a page main/spare areas or spare area only (-s)",
				"-f <raw page address> | -b <block> [-s]",
				nand_show,
				NAND_cmds
		   );

local_cmd_entry("read",
				"Read data from nand flash into RAM",
				"-f <raw addr> -b <mem_load_addr> -l <byte len> [-c <col>]\n"
				"      Note -c is only for 2K-page for value <0, 2048+64-1>",
				nand_read,
				NAND_cmds
		   );

local_cmd_entry("write",
				"Write data from RAM into nand flash",
				"-f <raw address> -b <memory_address> -l <image_length> [-c <col_addr>]",
				nand_write,
				NAND_cmds
		   );

local_cmd_entry("erase",
				"Erase nand flash contents",
				"-f <raw address> -l <length> [-o]\n"
				"             -o: force erase (even for bad blocks)",
				nand_erase,
				NAND_cmds
		   );

local_cmd_entry("scan",
				"Scan bad blocks and may also save bad block table into the NAND flash.",
				"[-o] [-r]\n"
				"No argument: save existing bad block table (BBT)\n"
				"            -r: re-scan with ECC off and save BBT -- for brand NEW flash\n"
				"            -o: force erase all, reconstruct BBT (no ECC) and save BBT -- for development.",
				nand_scan,
				NAND_cmds
		   );

local_cmd_entry("debug",
				"Various NAND debug features ",
				"<0> no debug messages <default>\n"
				"             <1> min debug messages\n"
				"             <2> med debug messages\n"
				"             <3> max debug messages\n"
				"             <4> enable(default)/disable h/w ECC for both r/w\n"
				"             <5> disable(default)/enalbe spare-only read\n"
				"             <9> enable/disable measurement\n"
				"             no parameter - display current debug setup",
				nand_debug_fun,
				NAND_cmds
				);

local_cmd_entry("bad",
				"Mark bad block in BBT",
				"[-f <raw address>] [-b <block number>] [-c]\n"
				"           -c: clear bad block mark\n"
				"           -f and -b are mutually exclusive",
				nand_bad,
				NAND_cmds
				);

// Define table boundaries
CYG_HAL_TABLE_BEGIN( __NAND_cmds_TAB__, NAND_cmds);
CYG_HAL_TABLE_END( __NAND_cmds_TAB_END__, NAND_cmds);

extern struct cmd __NAND_cmds_TAB__[], __NAND_cmds_TAB_END__;

// CLI function
static cmd_fun do_nand_cmds;
RedBoot_nested_cmd("nand",
		   "Utility function to NAND flash using raw address",
		   "{cmds}",
		   do_nand_cmds,
		   __NAND_cmds_TAB__, &__NAND_cmds_TAB_END__
		  );

static void nand_usage(char *why)
{
	diag_printf("*** invalid 'nand' command: %s\n", why);
	cmd_usage(__NAND_cmds_TAB__, &__NAND_cmds_TAB_END__, "nand ");
}

static u32 curr_addr;
static void nand_show(int argc, char *argv[])
{
	u32 ra, block;
	bool flash_addr_set = false;
	bool block_set = false;
	bool spar_only = false;
	struct option_info opts[3];

	init_opts(&opts[0], 'f', true, OPTION_ARG_TYPE_NUM,
			  &ra, &flash_addr_set, "NAND FLASH memory byte address");
	init_opts(&opts[1], 'b', true, OPTION_ARG_TYPE_NUM,
			  &block, &block_set, "NAND FLASH memory block number");
	init_opts(&opts[2], 's', false, OPTION_ARG_TYPE_FLG,
			  &spar_only, NULL, "Spare only");

	if (!scan_opts(argc, argv, 2, opts, NUM_ELEMS(opts), NULL, 0, NULL)) {
		return;
	}
	if (flash_addr_set && block_set) {
		nand_usage("options -f and -b are mutually exclusive");
		return;
	} else if (flash_addr_set) {
		curr_addr = ra;
	} else if (block_set) {
		ra = BLOCK_TO_OFFSET(block) + (unsigned long)flash_info.start;
		curr_addr = ra;
	} else {
		ra = curr_addr;
		curr_addr += NF_PG_SZ;
	}

	if (ra % NF_PG_SZ) {
		diag_printf("** Error: flash address must be page aligned\n");
		return;
	}

	ra &= MXC_NAND_ADDR_MASK;
	if (nfc_is_badblock(OFFSET_TO_BLOCK(ra), g_bbt)) {
		diag_printf("This is a bad block\n");
	}

	print_page(ra, spar_only);
}

/*!
 * For low level nand read command. It doesn't check for bad block or not
 */
static void nand_read(int argc, char *argv[])
{
	int len;
	u32 mem_addr, ra, col, i, pg_no, pg_off;
	bool mem_addr_set = false;
	bool flash_addr_set = false;
	bool length_set = false;
	bool col_set = false;
	struct option_info opts[4];
	int j = 0;
	bool ecc_status = g_ecc_enable;

	init_opts(&opts[0], 'b', true, OPTION_ARG_TYPE_NUM,
			  &mem_addr, &mem_addr_set, "memory base address");
	init_opts(&opts[1], 'f', true, OPTION_ARG_TYPE_NUM,
			  &ra, &flash_addr_set, "FLASH memory base address");
	init_opts(&opts[2], 'l', true, OPTION_ARG_TYPE_NUM,
			  &len, &length_set, "image length [in FLASH]");
	init_opts(&opts[3], 'c', true, OPTION_ARG_TYPE_NUM,
			  &col, &col_set, "column addr");

	if (!scan_opts(argc, argv, 2, opts, NUM_ELEMS(opts), NULL, 0, NULL)) {
		nand_usage("invalid arguments");
		return;
	}

	if (ra % NF_PG_SZ) {
		diag_printf("** Error: flash address must be page aligned\n");
		return;
	}

	if (!mem_addr_set || !flash_addr_set || !length_set) {
		nand_usage("** Error: required parameter missing");
		return;
	}
	if ((mem_addr < (CYG_ADDRESS)ram_start) ||
		((mem_addr+len) >= (CYG_ADDRESS)ram_end)) {
		diag_printf("** WARNING: RAM address: 0x%08x may be invalid\n", mem_addr);
		diag_printf("   valid range is 0x%p-0x%p\n", ram_start, ram_end);
	}

	if (col_set) {
		diag_printf("Random read at page %u, column 0x%04x\n",
					ra / NF_PG_SZ, col);

		if (g_is_2k_page || g_is_4k_page) {
			g_ecc_enable = false;
		}
		nfc_read_pg_random(ra / NF_PG_SZ, col, ECC_FORCE_OFF, 0, num_of_nand_chips);
		if (g_is_2k_page || g_is_4k_page) {
			g_ecc_enable = ecc_status;
		}
		nfc_buf_read((void *)mem_addr, NAND_MAIN_BUF0, NF_PG_SZ);
		return;
	}

	// ensure integer multiple of page size
	len = (len + NF_PG_SZ - 1) & ~(NF_PG_SZ - 1);
	ra &= MXC_NAND_ADDR_MASK;
	do {
		if (OFFSET_TO_BLOCK(ra) > (NF_BLK_CNT - 1)) {
			diag_printf("\n** Error: flash address: 0x%08x out of range\n", ra);
			return;
		}
		if (nfc_is_badblock(OFFSET_TO_BLOCK(ra), g_bbt)) {
			diag_printf("\nSkipping bad block %u at addr=0x%08llx\n",
						OFFSET_TO_BLOCK(ra), (u64)ra);
			ra = (OFFSET_TO_BLOCK(ra) + 1) *  NF_BLK_SZ;
			continue;
		}
		pg_no = ra / NF_PG_SZ;
		pg_off = ra % NF_PG_SZ;
		for (i = 0; i < num_of_nand_chips; i++) {
			if (nfc_read_page(i, pg_no, pg_off) != 0) {
				diag_printf("\n** Error: uncorrectable ECC at addr 0x%08x\n", ra);
				diag_printf("use 'nand bad -b %u' to mark this block in BBT\n",
							pg_no / NF_PG_PER_BLK);
			}
			if ((j++ % 0x20) == 0)
				diag_printf("\n%s 0x%08x: ", __FUNCTION__, ra);
			diag_printf(".");

			nfc_buf_read((void *)mem_addr, NAND_MAIN_BUF0, NF_PG_SZ / num_of_nand_chips);

			ra += NF_PG_SZ / num_of_nand_chips;
			mem_addr += NF_PG_SZ / num_of_nand_chips;
			len -= NF_PG_SZ / num_of_nand_chips;
			pg_off = 0;
		}
	} while (len > 0);
	diag_printf("\n");
}

static void nand_write(int argc, char *argv[])
{
	int len, len_st, j = 0;
	u32 mem_addr, mem_addr_st, ra, col;
	bool mem_addr_set = false;
	bool flash_addr_set = false;
	bool length_set = false;
	bool col_set = false;
	struct option_info opts[4];
	bool ecc_status = g_ecc_enable;

	init_opts(&opts[0], 'b', true, OPTION_ARG_TYPE_NUM,
			  &mem_addr, &mem_addr_set, "memory base address");
	init_opts(&opts[1], 'f', true, OPTION_ARG_TYPE_NUM,
			  &ra, &flash_addr_set, "FLASH memory base address");
	init_opts(&opts[2], 'l', true, OPTION_ARG_TYPE_NUM,
			  &len, &length_set, "image length [in FLASH]");
	init_opts(&opts[3], 'c', true, OPTION_ARG_TYPE_NUM,
			  &col, &col_set, "column addr");
	if (!scan_opts(argc, argv, 2, opts, NUM_ELEMS(opts), NULL, 0, NULL)) {
		nand_usage("invalid arguments");
		return;
	}

	if (!mem_addr_set || !flash_addr_set || !length_set) {
		nand_usage("required parameter missing");
		return;
	}

	if ((mem_addr < (CYG_ADDRESS)ram_start) ||
		((mem_addr + len) >= (CYG_ADDRESS)ram_end)) {
		diag_printf("** WARNING: RAM address range: %p..%p may be invalid\n",
					(void *)mem_addr, (void *)(mem_addr + len));
		diag_printf("   valid range is %p-%p\n", (void *)ram_start, (void *)ram_end);
	}

	if (col_set) {
		diag_printf("Random write at page %u, column %u\n", ra / NF_PG_SZ, col);

		if (g_is_2k_page || g_is_4k_page) {
			g_ecc_enable = false;
		}
		nfc_write_pg_random(ra / NF_PG_SZ, col, (u8 *)mem_addr, 0);
		if (g_is_2k_page || g_is_4k_page) {
			g_ecc_enable = ecc_status;
		}
		return;
	}

	if ((ra % NF_PG_SZ) != 0) {
		diag_printf("** Error: flash address must be page aligned\n");
		return;
	}

	mem_addr_st = mem_addr;
	len_st = len;
	ra &= MXC_NAND_ADDR_MASK;

	mxc_nfc_buf_clear(NAND_SPAR_BUF0, 0xff, NF_SPARE_SZ);
	do {
		if (OFFSET_TO_BLOCK(ra) > (NF_BLK_CNT - 1)) {
			diag_printf("\nFlash address 0x%08x out of range\n", ra);
			return;
		}
		if (nfc_is_badblock(OFFSET_TO_BLOCK(ra), g_bbt)) {
			diag_printf("\nSkipping bad block %u at addr=0x%08llx\n",
						OFFSET_TO_BLOCK(ra), (u64)ra);
			ra = (OFFSET_TO_BLOCK(ra) + 1) *  NF_BLK_SZ;
			continue;
		}

		if ((ra % NF_BLK_SZ) == 0) {
			 mem_addr_st = mem_addr;
			 len_st = len;
		}
		if (nfc_write_pg_random(ra / NF_PG_SZ, ra % NF_PG_SZ, (u8 *)mem_addr, 0) != 0) {
			if (g_nfc_debug_level >= NFC_DEBUG_DEF) {
				diag_printf("\nWarning %d: program error at addr 0x%x\n", __LINE__, ra);
			}
			mark_blk_bad(OFFSET_TO_BLOCK(ra), g_bbt, BLK_BAD_RUNTIME);
			ra = (OFFSET_TO_BLOCK(ra) + 1) *  NF_BLK_SZ; //make sure block size aligned
			mem_addr = mem_addr_st; // rewind to block boundary
			len = len_st;
			continue;
		}
		if ((j++ % 0x20) == 0)
			diag_printf("\nProgramming 0x%08x: ", ra);
		diag_printf(".");

		len -= NF_PG_SZ;
		ra += NF_PG_SZ;
		mem_addr += NF_PG_SZ;
	} while (len > 0);
	diag_printf("\n");
}

void nand_debug_fun(int argc, char *argv[])
{
	int opt;
	const char *dbg_lvl_str;

	if (argc == 3) {
		opt = argv[2][0] - '0';
		switch (opt) {
		case 0:
			g_nfc_debug_level = NFC_DEBUG_NONE;
			break;
		case 1:
			g_nfc_debug_level = NFC_DEBUG_MIN;
			break;
		case 2:
			g_nfc_debug_level = NFC_DEBUG_MED;
			break;
		case 3:
			g_nfc_debug_level = NFC_DEBUG_MAX;
			break;
		case 4:
			g_ecc_enable = g_ecc_enable? false: true;
			break;
		case 5:
			// toggle g_spare_only_read_ok
			g_spare_only_read_ok = g_spare_only_read_ok? false: true;
			break;
		case 9:
			g_nfc_debug_measure = g_nfc_debug_measure? false: true;
			break;

		default:
			diag_printf("%s(%s) not supported\n", __FUNCTION__, argv[2]);
		}
	}
	switch (g_nfc_debug_level) {
	case NFC_DEBUG_NONE:
		dbg_lvl_str = "none";
		break;
	case NFC_DEBUG_MIN:
		dbg_lvl_str = "min";
		break;
	case NFC_DEBUG_MED:
		dbg_lvl_str = "med";
		break;
	case NFC_DEBUG_MAX:
		dbg_lvl_str = "max";
		break;
	default:
		dbg_lvl_str = "invalid";
	}
	diag_printf("Current debug options are:\n");
	diag_printf("    h/w ECC: %s\n", g_ecc_enable ? "on" : "off");
	diag_printf("    sp-only read: %s\n", g_spare_only_read_ok ? "on" : "off");
	diag_printf("    measurement: %s\n", g_nfc_debug_measure ? "on" : "off");
	diag_printf("    message level: %s\n", dbg_lvl_str);
}

static void nand_erase(int argc, char *argv[])
{
	u32 len, ra;
	bool faddr_set = false;
	bool force_erase_set = false;
	bool length_set = false;
	struct option_info opts[4];

	init_opts(&opts[0], 'f', true, OPTION_ARG_TYPE_NUM,
		  &ra, &faddr_set, "FLASH memory base address");
	init_opts(&opts[1], 'l', true, OPTION_ARG_TYPE_NUM,
		  &len, &length_set, "length in bytes");
	init_opts(&opts[2], 'o', false, OPTION_ARG_TYPE_FLG,
		  &force_erase_set, &force_erase_set, "force erases block");

	if (!scan_opts(argc, argv, 2, opts, NUM_ELEMS(opts), NULL, 0, NULL)) {
		nand_usage("invalid arguments");
		return;
	}

	if (!faddr_set || !length_set) {
		nand_usage("missing argument");
		return;
	}
	if ((ra % NF_BLK_SZ) != 0) {
		diag_printf("Address must be block aligned!\n");
		diag_printf("Block size is 0x%x\n", NF_BLK_SZ);
		return;
	}
	if ((len % NF_BLK_SZ) != 0) {
		diag_printf("length must be block aligned!\n");
		diag_printf("Block size is 0x%x\n", NF_BLK_SZ);
		return;
	}
	if (len == 0) {
		diag_printf("length must be > 0!\n");
		return;
	}

	if (!verify_action("About to erase 0x%08x bytes from nand offset 0x%08x", len, ra)) {
		diag_printf("** Aborted\n");
		return;
	}

	diag_printf1("Enabling flash from %p..%p\n", (u8 *)ra, (u8 *)ra + len - 1);
	FLASH_Enable((u8 *)ra, (u8 *)ra + len);
	if (force_erase_set == true) {
		diag_printf("Force erase ...");
		nfc_erase_region(ra, len, 0, 1);
		diag_printf("\n");
	} else {
		nfc_erase_region(ra, len, 1, 1);
	}
	FLASH_Disable((u8 *)ra, (u8 *)ra + len);
	diag_printf("\n");
}

extern void romupdate(int argc, char *argv[]);
static void nand_scan(int argc, char *argv[])
{
	bool force_erase = false;
	bool force_rescan = false;
	struct option_info opts[2];

	init_opts(&opts[0], 'o', false, OPTION_ARG_TYPE_FLG,
		  &force_erase, NULL, "force erases block first");

	init_opts(&opts[1], 'r', false, OPTION_ARG_TYPE_FLG,
		  &force_rescan, NULL, "force low level re-scan");

	if (!scan_opts(argc, argv, 2, opts, NUM_ELEMS(opts), NULL, 0, NULL)) {
		nand_usage("invalid arguments");
		return;
	}

	if (!force_erase && !force_rescan && !mxc_nfc_scan_done) {
		diag_printf("Need to build BBT first with \"nand scan [-o|-r]\"\n");
		return;
	}
	if (force_erase) {
		void *bbt = g_bbt;

		diag_printf("Force erase first ...\n");
		g_bbt = NULL;
		// do force erase, skipping bad blocks. After this call, g_bbt should be re-built
		// for the whole NAND flash.
		if (nfc_erase_region(0, NF_DEV_SZ, true, false) != 0) {
			g_bbt = bbt;
			return;
		}
		g_bbt = bbt;
		mxc_nfc_scan_done = 0;
		diag_printf("\n");
	}
	if (force_rescan) {
		diag_printf("Force re-scan ...\n");
		memset(g_bbt, 0, g_bbt_sz);
		mxc_nfc_scan(true);
	}
	// program g_bbt into the flash
	diag_printf("Writing BBT to flash\n");
	if (program_bbt_to_flash() != 0) {
		diag_printf("Error: Failed to write BBT to flash\n");
	}
	if (force_erase) {
		romupdate(0, NULL);
	}
}

static void nand_info(int argc, char *argv[])
{
	u32 i, j = 0;

	diag_printf("\nType:\t\t %s\n", NF_VEND_INFO);
	diag_printf("Total size:\t 0x%08x bytes (%d MiB)\n", NF_DEV_SZ, NF_DEV_SZ / SZ_1M);
	diag_printf("Total blocks:\t 0x%x (%d)\n", NF_BLK_CNT, NF_BLK_CNT);
	diag_printf("Block size:\t 0x%x (%d)\n", NF_BLK_SZ, NF_BLK_SZ);
	diag_printf("Page size:\t 0x%x (%d)\n", NF_PG_SZ, NF_PG_SZ);
	diag_printf("Spare size:\t 0x%x (%d)\n", NF_SPARE_SZ, NF_SPARE_SZ);
	diag_printf("Pages per block: 0x%x (%d)\n", NF_PG_PER_BLK, NF_PG_PER_BLK);

	if (mxc_nfc_scan(false) == -1) {
		return;
	}
	diag_printf("\n");
	for (i = 0; i < NF_BLK_CNT; i++) {
		int res = nfc_is_badblock(i, g_bbt);
		if (res & ~BLK_RESERVED) {
			diag_printf("block %d at offset 0x%08x is a %s bad block\n",
						i, i * NF_BLK_SZ, res == BLK_BAD_FACTORY ? "factory" : "runtime");
			j++;
		}
	}
	diag_printf("==================================\n");
	diag_printf("Found %d bad block(s) out of %d\n", j, i);
}

static void nand_bad(int argc, char *argv[])
{
	u32 ra;
	u32 block;
	bool ra_set = false;
	bool block_set = false;
	bool clear = false;
	struct option_info opts[3];
	int bad;

	init_opts(&opts[0], 'f', true, OPTION_ARG_TYPE_NUM,
			  &ra, &ra_set, "FLASH memory base address");
	init_opts(&opts[1], 'b', true, OPTION_ARG_TYPE_NUM,
			  &block, &block_set, "block number");
	init_opts(&opts[2], 'c', false, OPTION_ARG_TYPE_FLG,
			  &clear, NULL, "clear bad block marker");

	if (!scan_opts(argc, argv, 2, opts, NUM_ELEMS(opts), NULL, 0, NULL)) {
		nand_usage("invalid arguments");
		return;
	}

	if (!ra_set && !block_set) {
		nand_usage("missing argument");
		return;
	}
	if (ra_set && block_set) {
		nand_usage("options -f and -b are mutually exclusive");
		return;
	} else if (ra_set) {
		block = OFFSET_TO_BLOCK(ra & MXC_NAND_ADDR_MASK);
	} else {
		ra = BLOCK_TO_OFFSET(block) + (unsigned long)flash_info.start;
	}
	if ((ra % NF_BLK_SZ) != 0) {
		diag_printf("Address is not block aligned!\n");
		diag_printf("Block size is 0x%08x\n", NF_BLK_SZ);
		return;
	}

	bad = nfc_is_badblock(block, g_bbt);
	if ((bad && !clear) || (!bad && clear)) {
		diag_printf("block %5u at address 0x%08x is already %s\n",
					block, ra, bad ? "bad" : "good");
		return;
	}
	if (clear && bad != BLK_BAD_RUNTIME) {
		diag_printf("Refusing to mark a factory bad block as good!\n");
		return;
	}
	if (!verify_action("Mark block %u at address 0x%08x %s in BBT",
					   block, ra, clear ? "good" : "bad")) {
		diag_printf("** Aborted\n");
		return;
	}

	nfc_printf(NFC_DEBUG_MIN, "Marking block %5u at 0x%08x %s\n",
			   block, ra, clear ? "good" : "bad");
	mark_blk_bad(block, g_bbt, clear ? 0 : BLK_BAD_RUNTIME);
	mxc_nfc_update_bbt(g_mxc_nfc_bbt_main_descr,
					   g_mxc_nfc_bbt_mirror_descr);
}

static void do_nand_cmds(int argc, char *argv[])
{
	struct cmd *cmd;

	if (!mxcnfc_init_ok) {
		flash_hwr_init();
		if (!mxcnfc_init_ok) {
#ifdef CYGHWR_DEVS_FLASH_MXC_MULTI
			diag_printf("Warning: NAND flash hasn't been initialized. Try \"factive nand\" first\n\n");
#else
			diag_printf("Error: NAND flash hasn't been initialized\n");
#endif
			return;
		}
	}

	if (argc < 2) {
		nand_usage("too few arguments");
		return;
	}

	if ((cmd = cmd_search(__NAND_cmds_TAB__, &__NAND_cmds_TAB_END__,
						  argv[1])) != NULL) {
		cmd->fun(argc, argv);
		return;
	}
	nand_usage("unrecognized command");
}

/*!
 * Display a memory region by 16-bit words
 * @param pkt	pointer to the starting address of the memory
 * @param len	byte length of the buffer to be displayed
 */
static void print_pkt_16(u16 *pkt, u32 len)
{
	diag_printf("******************** %d bytes********************\n", len);
	u32 i = 0, tempLen = (len + 1) / 2;

	while (tempLen != 0) {
		if (tempLen >= 8) {
			diag_printf("[%03x-%03x] ", i * 2, (i * 2) + 14);
			diag_printf("%04x %04x %04x %04x %04x %04x %04x %04x\n",
						pkt[i], pkt[i + 1], pkt[i + 2], pkt[i + 3],
						pkt[i + 4], pkt[i + 5], pkt[i + 6], pkt[i + 7]);
			tempLen -= 8;
			i += 8;
		} else {
			if (tempLen != 0) {
				diag_printf("[%03x-%03x]", i * 2, (i + tempLen) * 2);
				while (tempLen-- != 0) {
					diag_printf(" %04x", pkt[i++]);
				}
				diag_printf("\n");
			}
			diag_printf("*************************************************\n");
			return;
		}
	}
}

// addr = starting byte address within NAND flash
static void print_page(u32 addr, bool spare_only)
{
	u32 i, pg_no, pg_off;
	u32 blk_num = OFFSET_TO_BLOCK(addr), pg_num = OFFSET_TO_PAGE(addr);

	if (addr % NF_PG_SZ) {
		diag_printf("Non page-aligned read not supported here: 0x%x\n", addr);
		return;
	}
	pg_no = addr / NF_PG_SZ;
	pg_off = addr % NF_PG_SZ;
	for (i = 0; i < num_of_nand_chips; i++) {
		if (nfc_read_page(i, pg_no, pg_off) != 0) {
			diag_printf("Error %d: uncorrectable. But still printing ...\n", __LINE__);
		}
		pg_off = 0;
		diag_printf("\n============ Printing block(%d) page(%d)	 ==============\n",
					blk_num, pg_num);

		diag_printf("<<<<<<<<< spare area >>>>>>>>>\n");
		print_pkt_16((u16*)NAND_SPAR_BUF0, NF_SPARE_SZ);

		if (!spare_only) {
			diag_printf("<<<<<<<<< main area >>>>>>>>>\n");
			print_pkt_16((u16*)NAND_MAIN_BUF0, NF_PG_SZ / num_of_nand_chips);
		}

		diag_printf("\n");
	}
}
