#ifndef __WINCE_H__
#define __WINCE_H__

// Bin image parse results

#define CE_PR_EOF				0
#define CE_PR_MORE				1
#define CE_PR_ERROR				2


bool ce_bin_load(void* image, int imglen);
bool ce_is_bin_image(void* image, int imglen);
void ce_bin_init_parser(void);
int ce_bin_parse_next(void* parseBuffer, int len);

#endif





