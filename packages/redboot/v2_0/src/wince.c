#include <redboot.h>
#include <cyg/io/flash.h>
#include <net/net.h>
#include CYGHWR_MEMORY_LAYOUT_H
#include <wince.h>
#include <winceinc.h>

cmd_fun do_go;

///////////////////////////////////////////////////////////////////////////////////////////////
// Local macro

// Memory macro
#define CE_RAM_BASE				CYGMEM_REGION_ram
#define CE_RAM_SIZE				CYGMEM_REGION_ram_SIZE
#define CE_RAM_END				(CE_RAM_BASE + CE_RAM_SIZE)
#define CE_WINCE_VRAM_BASE		0x80000000
#define CE_FIX_ADDRESS(a)		(((a) - CE_WINCE_VRAM_BASE) + CE_RAM_BASE)

#ifdef CYGHWR_REDBOOT_FLASH_CONFIG_MEDIA_FLASH
extern void *flash_start, *flash_end;

static inline int is_rom_addr(void *addr)
{
	return addr >= flash_start && addr <= flash_end;
}
#else
#define is_rom_addr(p)			0
#endif

static inline int is_ram_addr(unsigned long addr)
{
	return addr >= CE_RAM_BASE && addr < CE_RAM_END;
}

// Bin image parse states
#define CE_PS_RTI_ADDR			0
#define CE_PS_RTI_LEN			1
#define CE_PS_E_ADDR			2
#define CE_PS_E_LEN				3
#define CE_PS_E_CHKSUM			4
#define CE_PS_E_DATA			5

// Min/max
#define CE_MIN(a, b)			(((a) < (b)) ? (a) : (b))
#define CE_MAX(a, b)			(((a) > (b)) ? (a) : (b))

// Macro string
#define _STRMAC(s)				#s
#define STRMAC(s)				_STRMAC(s)

///////////////////////////////////////////////////////////////////////////////////////////////
// Local types
typedef struct {
	unsigned long rtiPhysAddr;
	unsigned long rtiPhysLen;
	unsigned long ePhysAddr;
	unsigned long ePhysLen;
	unsigned long eChkSum;

	unsigned long eEntryPoint;
	unsigned long eRamStart;
	unsigned long eRamLen;
	unsigned long eDrvGlb;

	unsigned char parseState;
	unsigned long parseChkSum;
	int parseLen;
	unsigned char *parsePtr;
	int section;

	int dataLen;
	unsigned char *data;

	int binLen;
	int endOfBin;

	edbg_os_config_data edbgConfig;
} ce_bin;

typedef struct {
	bool verbose;
	bool link;
	struct sockaddr_in locAddr;
	struct sockaddr_in srvAddrSend;
	struct sockaddr_in srvAddrRecv;
	bool gotJumpingRequest;
	unsigned char secNum;
	unsigned short blockNum;
	int dataLen;
	unsigned char data[516];
} ce_net;

///////////////////////////////////////////////////////////////////////////////////////////////
// Global data
#ifdef CYGPKG_REDBOOT_NETWORKING
static ce_net g_net;
#endif
static ce_bin g_bin;

// Equotip3 specific
#ifdef CYGPKG_HAL_ARM_XSCALE_TRITON270_EQT32
#include <cyg/hal/hal_triton270.h>
extern EEDAT eedat;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////
// Local proto

void ce_init_bin(ce_bin *bin, unsigned char *dataBuffer);
int ce_parse_bin(ce_bin *bin);
bool ce_lookup_ep_bin(ce_bin *bin);
void ce_prepare_run_bin(ce_bin *bin);
void ce_run_bin(ce_bin *bin);

#ifdef CYGPKG_REDBOOT_NETWORKING
// Redboot network based routines
void ce_shell(int argc, char *argv[]);
void ce_init_download_link(ce_net *net, ce_bin *bin, struct sockaddr_in *host_addr, bool verbose);
void ce_init_edbg_link(ce_net *net);
int ce_send_frame(ce_net *net);
int ce_recv_frame(ce_net *net, int timeout);
int ce_send_bootme(ce_net *net);
int ce_send_write_ack(ce_net *net);
int ce_process_download(ce_net *net, ce_bin *bin);
void ce_process_edbg(ce_net *net, ce_bin *bin);

#endif

///////////////////////////////////////////////////////////////////////////////////////////////
// RedBoot commands

#ifdef CYGPKG_REDBOOT_NETWORKING
// Redboot network based commands
RedBoot_cmd(
	"ceconnect",
	"Set up a connection to the CE host PC over TCP/IP and download the run-time image",
	"[-v] [-t <timeout>] [-h <host>]",
	ce_load
	);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////
// Implementation

bool ce_bin_load(void *image, int imglen)
{
	ce_init_bin(&g_bin, image);

	g_bin.dataLen = imglen;

	if (ce_parse_bin(&g_bin) == CE_PR_EOF) {
		ce_prepare_run_bin(&g_bin);
		return true;
	}

	return false;
}

bool ce_is_bin_image(void *image, int imglen)
{
	if (imglen < CE_BIN_SIGN_LEN) {
		diag_printf("Not a valid CE image: image size %u shorter than minimum %u\n",
					imglen, CE_BIN_SIGN_LEN);
		return 0;
	}
	if (is_rom_addr(image)) {
		unsigned char sign_buf[CE_BIN_SIGN_LEN];
		void *err_addr;

		if (flash_read(image, sign_buf,
						CE_BIN_SIGN_LEN, &err_addr) != FLASH_ERR_OK) {
			return 0;
		}
		return memcmp(sign_buf, CE_BIN_SIGN, CE_BIN_SIGN_LEN) == 0;
	}
	return memcmp(image, CE_BIN_SIGN, CE_BIN_SIGN_LEN) == 0;
}

void ce_bin_init_parser()
{
	// No buffer address by now, will be specified
	// later by the ce_bin_parse_next routine
	ce_init_bin(&g_bin, NULL);
}

int ce_bin_parse_next(void *parseBuffer, int len)
{
	int rc;

	g_bin.data = (unsigned char*)parseBuffer;
	g_bin.dataLen = len;
	rc = ce_parse_bin(&g_bin);

	if (rc == CE_PR_EOF) {
		ce_prepare_run_bin(&g_bin);
	}

	return rc;
}

void ce_init_bin(ce_bin *bin, unsigned char *dataBuffer)
{
	memset(bin, 0, sizeof(ce_bin));

	bin->data = dataBuffer;
	bin->parseState = CE_PS_RTI_ADDR;
	bin->parsePtr = (unsigned char*)&bin->rtiPhysAddr;
}

int ce_parse_bin(ce_bin *bin)
{
	unsigned char *pbData = bin->data;
	int pbLen = bin->dataLen;
	int copyLen;

	if (pbLen) {
		if (bin->binLen == 0) {
			// Check for the .BIN signature first
			if (!ce_is_bin_image(pbData, pbLen)) {
				diag_printf("** Error: Invalid or corrupted .BIN image!\n");

				return CE_PR_ERROR;
			}

			diag_printf("Loading Windows CE .BIN image ...\n");

			// Skip signature
			pbLen -= CE_BIN_SIGN_LEN;
			pbData += CE_BIN_SIGN_LEN;
		}

		while (pbLen) {
			switch (bin->parseState) {
			case CE_PS_RTI_ADDR:
			case CE_PS_RTI_LEN:
			case CE_PS_E_ADDR:
			case CE_PS_E_LEN:
			case CE_PS_E_CHKSUM:

				copyLen = CE_MIN(sizeof(unsigned int) - bin->parseLen, pbLen);

				if (is_rom_addr(pbData)) {
					void *err_addr;

					if (flash_read(pbData, &bin->parsePtr[bin->parseLen],
									copyLen, &err_addr) != FLASH_ERR_OK) {
						return CE_PR_ERROR;
					}
				} else {
					memcpy(&bin->parsePtr[bin->parseLen], pbData, copyLen);
				}
				bin->parseLen += copyLen;
				pbLen -= copyLen;
				pbData += copyLen;

				if (bin->parseLen == sizeof(unsigned int)) {
					if (bin->parseState == CE_PS_RTI_ADDR) {
						bin->rtiPhysAddr = CE_FIX_ADDRESS(bin->rtiPhysAddr);
						if (!is_ram_addr(bin->rtiPhysAddr)) {
							diag_printf("Invalid address %08lx in CE_PS_RTI_ADDR section\n",
										bin->rtiPhysAddr);
							return CE_PR_ERROR;
						}
					} else if (bin->parseState == CE_PS_E_ADDR) {
						if (bin->ePhysAddr) {
							bin->ePhysAddr = CE_FIX_ADDRESS(bin->ePhysAddr);
							if (!is_ram_addr(bin->ePhysAddr)) {
								diag_printf("Invalid address %08lx in CE_PS_E_ADDR section\n",
											bin->ePhysAddr);
								return CE_PR_ERROR;
							}
						}
					}

					bin->parseState++;
					bin->parseLen = 0;
					bin->parsePtr += sizeof(unsigned int);
					if (bin->parseState == CE_PS_E_DATA) {
						if (bin->ePhysAddr) {
							bin->parsePtr = (unsigned char*)(bin->ePhysAddr);
							bin->parseChkSum = 0;
						} else {
							// EOF
							pbLen = 0;
							bin->endOfBin = 1;
						}
					}
				}

				break;

			case CE_PS_E_DATA:
				if (bin->ePhysAddr) {
					copyLen = CE_MIN(bin->ePhysLen - bin->parseLen, pbLen);
					bin->parseLen += copyLen;
					pbLen -= copyLen;
					if (is_rom_addr(pbData)) {
						void *err_addr;

						if (flash_read(pbData, bin->parsePtr,
										copyLen, &err_addr) != FLASH_ERR_OK) {
							return CE_PR_ERROR;
						}
						pbData += copyLen;
						while (copyLen--) {
							bin->parseChkSum += *bin->parsePtr++;
						}
					} else {
						while (copyLen--) {
							bin->parseChkSum += *pbData;
							*bin->parsePtr++ = *pbData++;
						}
					}
					if (bin->parseLen == bin->ePhysLen) {
						diag_printf("Section [%02d]: address 0x%08lX, size 0x%08lX, checksum %s\n",
							bin->section,
							bin->ePhysAddr,
							bin->ePhysLen,
							(bin->eChkSum == bin->parseChkSum) ? "ok" : "fail");

						if (bin->eChkSum != bin->parseChkSum) {
							// Checksum error!
							diag_printf("Error: Checksum error, corrupted .BIN file!\n");
							diag_printf("checksum calculated: 0x%08lx from file: 0x%08lx\n",
										bin->parseChkSum, bin->eChkSum);
							bin->binLen = 0;
							return CE_PR_ERROR;
						}

						bin->section++;
						bin->parseState = CE_PS_E_ADDR;
						bin->parseLen = 0;
						bin->parsePtr = (unsigned char*)(&bin->ePhysAddr);
					}
				} else {
					bin->parseLen = 0;
					bin->endOfBin = 1;
					pbLen = 0;
				}

				break;
			}
		}
	}

	if (bin->endOfBin) {
		// Find entry point

		if (!ce_lookup_ep_bin(bin)) {
			diag_printf("** Error: entry point not found!\n");

			bin->binLen = 0;

			return CE_PR_ERROR;
		}

		diag_printf("Entry point: 0x%08lX, address range: 0x%08lX-0x%08lX\n",
			bin->eEntryPoint,
			bin->rtiPhysAddr,
			bin->rtiPhysAddr + bin->rtiPhysLen);

		return CE_PR_EOF;
	}

	// Need more data

	bin->binLen += bin->dataLen;

	return CE_PR_MORE;
}

bool ce_lookup_ep_bin(ce_bin *bin)
{
	ce_rom_hdr *header;
	ce_toc_entry *tentry;
	e32_rom *e32;
	unsigned int i;

	// Check image Table Of Contents (TOC) signature
	if (*(unsigned int*)(bin->rtiPhysAddr + ROM_SIGNATURE_OFFSET) != ROM_SIGNATURE) {
		// Error: Did not find image TOC signature!

		return 0;
	}


	// Lookup entry point
	header = (ce_rom_hdr*)CE_FIX_ADDRESS(*(unsigned int*)(bin->rtiPhysAddr +
														  ROM_SIGNATURE_OFFSET +
														  sizeof(unsigned int)));
	tentry = (ce_toc_entry*)(header + 1);

	for (i = 0; i < header->nummods; i++) {
		// Look for 'nk.exe' module
		if (strcmp((char*)CE_FIX_ADDRESS(tentry[ i ].fileName), "nk.exe") == 0) {
			// Save entry point and RAM addresses

			e32 = (e32_rom*)CE_FIX_ADDRESS(tentry[ i ].e32Offset);

			bin->eEntryPoint = CE_FIX_ADDRESS(tentry[ i ].loadOffset) +
				e32->e32_entryrva;
			bin->eRamStart = CE_FIX_ADDRESS(header->ramStart);
			bin->eRamLen = header->ramEnd - header->ramStart;

			// Save driver_globals address
			// Must follow RAM section in CE config.bib file
			//
			// eg.
			//
			// RAM		80900000	03200000	RAM
			// DRV_GLB	83B00000	00001000	RESERVED
			//

			bin->eDrvGlb = CE_FIX_ADDRESS(header->ramEnd) -
				sizeof(ce_driver_globals);
			return 1;
		}
	}

	// Error: Did not find 'nk.exe' module

	return 0;
}

void setup_drv_globals(ce_driver_globals *drv_glb)
{
	// Fill out driver globals
	memset(drv_glb, 0, sizeof(ce_driver_globals));

	// Signature
	drv_glb->signature = DRV_GLB_SIGNATURE;

	// No flags by now
	drv_glb->flags = 0;

#ifdef CYGPKG_REDBOOT_NETWORKING
	// Local ethernet MAC address
	memcpy(drv_glb->macAddr, __local_enet_addr, sizeof(__local_enet_addr));

	// Local IP address
	memcpy(&drv_glb->ipAddr, __local_ip_addr, sizeof(__local_ip_addr));

#ifdef CYGSEM_REDBOOT_NETWORKING_USE_GATEWAY
	// Subnet mask
	memcpy(&drv_glb->ipMask, __local_ip_mask, sizeof(__local_ip_mask));

	// Gateway config
	// Getway IP address
	memcpy(&drv_glb->ipGate, __local_ip_gate, sizeof(__local_ip_gate));
#endif
#endif
}

void setup_std_drv_globals(ce_std_driver_globals *std_drv_glb)
{
	// Fill out driver globals
	memset(std_drv_glb, 0, sizeof(ce_std_driver_globals));

	std_drv_glb->header.signature = STD_DRV_GLB_SIGNATURE;
	std_drv_glb->header.oalVersion = 1;
	std_drv_glb->header.bspVersion = 1;

	std_drv_glb->kitl.flags = 0;
	diag_sprintf(std_drv_glb->deviceId, "Triton");

#ifdef CYGPKG_REDBOOT_NETWORKING
	memcpy(&std_drv_glb->kitl.mac[0], __local_enet_addr, sizeof(__local_enet_addr));
	diag_sprintf(std_drv_glb->deviceId, "Triton%02X", __local_enet_addr[5]);

	// Local IP address
	memcpy(&std_drv_glb->kitl.ipAddress, __local_ip_addr, sizeof(__local_ip_addr));

#ifdef CYGSEM_REDBOOT_NETWORKING_USE_GATEWAY
	// Subnet mask
	memcpy(&std_drv_glb->kitl.ipMask, __local_ip_mask, sizeof(__local_ip_mask));

	// Gateway config
	// Getway IP address
	memcpy(&std_drv_glb->kitl.ipRoute, __local_ip_gate, sizeof(__local_ip_gate));
#endif
#endif
}

void ce_prepare_run_bin(ce_bin *bin)
{
	ce_driver_globals *drv_glb;
	ce_std_driver_globals *std_drv_glb = &_KARO_CECFG_START;
	char *karo_magic = &_KARO_MAGIC[0];
	unsigned long *karo_structure_size = &_KARO_STRUCT_SIZE;

	memcpy(karo_magic, "KARO_CE6", sizeof(_KARO_MAGIC));
	*karo_structure_size = sizeof(ce_std_driver_globals);

	// Clear os RAM area (if needed)
	if (bin->edbgConfig.flags & EDBG_FL_CLEANBOOT) {
		diag_printf("Preparing clean boot ... ");
		memset((void *)bin->eRamStart, 0, bin->eRamLen);
		diag_printf("ok\n");
	}

	// Prepare driver globals (if needed)
	if (bin->eDrvGlb) {
		drv_glb = (ce_driver_globals *)bin->eDrvGlb;

		setup_drv_globals(drv_glb);
		setup_std_drv_globals(std_drv_glb);

		// EDBG services config
		memcpy(&drv_glb->edbgConfig, &bin->edbgConfig, sizeof(bin->edbgConfig));
	}

	// Update global RedBoot entry point address
	// to use with go/run commands
	entry_address = bin->eEntryPoint;
}

void ce_run_bin(ce_bin *bin)
{
	char *argv[] = { "go" };

	diag_printf("Launching Windows CE ...\n");

	// Call GO command direcly
	do_go(1, argv);
}

// Extract version from CYGDAT_REDBOOT_CUSTOM_VERSION macro
// Works with versions like 'v2', 'v2.1', '2', '2.1', '2a', '2.1a'

void ce_redboot_version(unsigned int *vhigh, unsigned int *vlow)
{
	char *pver = ""STRMAC(CYGDAT_REDBOOT_CUSTOM_VERSION);
	char *p;
	unsigned int *ver;
	int len, pow;

	*vhigh = *vlow = 0;
	len = strlen(pver);
	ver = vhigh;
	pow = 1;
	p = pver + strlen(pver) - 1;

	while (len--) {
		if (*p >= '0' && *p <= '9') {
			*ver += ((*p - '0') * pow);
			pow *= 10;
		} else if (*p == '.') {
			if (ver == vhigh) {
				*vlow = *vhigh;
				*ver = 0;
				pow = 1;
			} else {
				break;
			}
		}
		p--;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Redboot network based routines

#ifdef CYGPKG_REDBOOT_NETWORKING
void ce_load(int argc, char *argv[])
{
	struct option_info opts[3];
	bool verbose, use_timeout;
	int timeout, recv_timeout, ret;
	char *host_name;
	char ctemp;
	struct sockaddr_in host_ip_addr;

	// Prepare for command line scan

	verbose = false;
	init_opts(&opts[0], 'v', false, OPTION_ARG_TYPE_FLG, (void *)&verbose, NULL,
		  "verbose operation");

	timeout = 0;
	init_opts(&opts[1], 't', true, OPTION_ARG_TYPE_NUM, (void *)&timeout, &use_timeout,
		  "<timeout> - max wait time (#sec) for the connection");

	host_name = NULL;
	init_opts(&opts[2], 'h', true, OPTION_ARG_TYPE_STR, (void *)&host_name, NULL,
		  "<host> - host name or IP address");

	if (!scan_opts(argc, argv, 1, opts, 3, NULL, 0, "")) {
		diag_printf("CELOAD - Invalid option specified\n");
		return;
	}

	// Check host IP address (if specified)
	memset(&host_ip_addr, 0, sizeof(host_ip_addr));
	if (host_name) {
		if (!_gethostbyname(host_name, (in_addr_t*)&host_ip_addr)) {
			diag_printf("CELOAD - Invalid host name: %s\n", host_name);
			return;
		}
	}

	// Initialize download link

	ce_init_download_link(&g_net, &g_bin, &host_ip_addr, verbose);

	// Download loop

	while (1) {
		if (g_net.link) {
			recv_timeout = 3;
		} else {
			recv_timeout = 1;

			if (use_timeout) {
				if (timeout <= 0) {
					diag_printf("CELOAD - Canceled, timeout\n");
					return;
				}
			} else {
				// Try to catch ^C
				if (_rb_gets(&ctemp, 1, 1) == _GETS_CTRLC) {
					diag_printf("CELOAD - canceled by user\n");
					return;
				}
			}

			if (ce_send_bootme(&g_net)) {
				diag_printf("CELOAD - error while sending BOOTME request\n");
				return;
			}

			if (verbose) {
				if (use_timeout) {
					diag_printf("Waiting for connection, timeout %d sec\n",
								timeout);
				} else {
					diag_printf("Waiting for connection, enter ^C to abort\n");
				}
			}
		}

		// Try to receive frame

		if (ce_recv_frame(&g_net, recv_timeout)) {
			// Process received data

			ret = ce_process_download(&g_net, &g_bin);

			if (ret != CE_PR_MORE) {
				break;
			}
		} else if (use_timeout) {
			timeout -= recv_timeout;
		}
	}

	if (g_bin.binLen) {
		// Try to receive edbg commands from host
		ce_init_edbg_link(&g_net);

		if (verbose) {
			diag_printf("Waiting for EDBG commands ...\n");
		}

		while (ce_recv_frame(&g_net, 3)) {
			ce_process_edbg(&g_net, &g_bin);
		}

		// Prepare WinCE image for execution
		ce_prepare_run_bin(&g_bin);

		// Launch WinCE, if necessary
		if (g_net.gotJumpingRequest) {
			ce_run_bin(&g_bin);
		}
	}
}

void ce_init_download_link(ce_net *net, ce_bin *bin, struct sockaddr_in *host_addr, bool verbose)
{
	// Initialize EDBG link for download

	memset(net, 0, sizeof(ce_net));

	net->locAddr.sin_family = AF_INET;
	memcpy(&net->locAddr.sin_addr, __local_ip_addr, sizeof(__local_ip_addr));
	net->locAddr.sin_port = htons(EDBG_DOWNLOAD_PORT);

	net->srvAddrSend.sin_family = AF_INET;
	net->srvAddrSend.sin_port = htons(EDBG_DOWNLOAD_PORT);

	net->srvAddrRecv.sin_family = AF_INET;
	net->srvAddrRecv.sin_port = 0;

	if (host_addr->sin_addr.s_addr) {
		// Use specified host address ...

		net->srvAddrSend.sin_addr = host_addr->sin_addr;
		net->srvAddrRecv.sin_addr = host_addr->sin_addr;
	} else {
		// ... or use default server address

		net->srvAddrSend.sin_addr = my_bootp_info.bp_siaddr;
		net->srvAddrRecv.sin_addr = my_bootp_info.bp_siaddr;
	}

	net->verbose = verbose;

	// Initialize .BIN parser

	// net->data + 0 -> Command
	// net->data + 2 -> Block number
	// net->data + 4 -> Block data

	ce_init_bin(bin, net->data + 4);
}

void ce_init_edbg_link(ce_net *net)
{
	// Initialize EDBG link for commands

	net->locAddr.sin_port = htons(EDBG_DOWNLOAD_PORT);
	net->srvAddrSend.sin_port = htons(EDBG_DOWNLOAD_PORT);
	net->srvAddrRecv.sin_port = 0;
	net->link = false;
}

int ce_send_frame(ce_net *net)
{
	// Send UDP packet

	return __udp_sendto(net->data, net->dataLen, &net->srvAddrSend, &net->locAddr);
}

int ce_recv_frame(ce_net *net, int timeout)
{
	struct timeval timeo;

	// Setup timeout

	timeo.tv_sec = timeout;
	timeo.tv_usec = 0;

	// Receive UDP packet

	net->dataLen = __udp_recvfrom(net->data, sizeof(net->data), &net->srvAddrRecv,
								  &net->locAddr, &timeo);

	if (net->dataLen < 0) {
		// Error! No data available

		net->dataLen = 0;
	}
	return net->dataLen;
}

int ce_send_bootme(ce_net *net)
{
	eth_dbg_hdr *header;
	edbg_bootme_data *data;
	unsigned int verHigh, verLow;

	// Fill out BOOTME packet
	memset(net->data, 0, BOOTME_PKT_SIZE);

	header = (eth_dbg_hdr*)net->data;
	data = (edbg_bootme_data*)header->data;

	header->id = EDBG_ID;
	header->service = EDBG_SVC_ADMIN;
	header->flags = EDBG_FL_FROM_DEV;
	header->seqNum = net->secNum++;
	header->cmd = EDBG_CMD_BOOTME;

	// Get RedBoot version
	ce_redboot_version(&verHigh, &verLow);

	data->versionMajor = verHigh;
	data->versionMinor = verLow;
	data->cpuId = EDBG_CPU_TYPE_ARM;
	data->bootmeVer = EDBG_CURRENT_BOOTME_VERSION;
	data->bootFlags = 0;
	data->downloadPort = 0;
	data->svcPort = 0;

	memcpy(data->macAddr, __local_enet_addr, sizeof(__local_enet_addr));
	memcpy(&data->ipAddr, __local_ip_addr, sizeof(__local_ip_addr));

	// Device name string (NULL terminated). Should include
	// platform and number based on Ether address (e.g. Odo42, CEPCLS2346, etc)

	// We will use lower MAC address segment to create device name
	// eg. MAC '00-0C-C6-69-09-05', device name 'Triton05'

	strcpy(data->platformId, "Triton");
	diag_sprintf(data->deviceName, "%s%02X", data->platformId, __local_enet_addr[5]);
#if 0
	diag_printf("header->id: %08X\r\n", header->id);
	diag_printf("header->service: %08X\r\n", header->service);
	diag_printf("header->flags: %08X\r\n", header->flags);
	diag_printf("header->seqNum: %08X\r\n", header->seqNum);
	diag_printf("header->cmd: %08X\r\n\r\n", header->cmd);

	diag_printf("data->versionMajor: %08X\r\n", data->versionMajor);
	diag_printf("data->versionMinor: %08X\r\n", data->versionMinor);
	diag_printf("data->cpuId: %08X\r\n", data->cpuId);
	diag_printf("data->bootmeVer: %08X\r\n", data->bootmeVer);
	diag_printf("data->bootFlags: %08X\r\n", data->bootFlags);
	diag_printf("data->svcPort: %08X\r\n\r\n", data->svcPort);

	diag_printf("data->macAddr: %02X-%02X-%02X-%02X-%02X-%02X-%02X\r\n",
		(data->macAddr[0] >> 0) & 0xFF,
		(data->macAddr[0] >> 8) & 0xFF,
		(data->macAddr[1] >> 0) & 0xFF,
		(data->macAddr[1] >> 8) & 0xFF,
		(data->macAddr[2] >> 0) & 0xFF,
		(data->macAddr[2] >> 8) & 0xFF);

	diag_printf("data->ipAddr: %d.%d.%d.%d\r\n",
		(data->ipAddr >> 0) & 0xFF,
		(data->ipAddr >> 8) & 0xFF,
		(data->ipAddr >> 16) & 0xFF,
		(data->ipAddr >> 24) & 0xFF);

	diag_printf("data->platformId: %s\r\n", data->platformId);
	diag_printf("data->deviceName: %s\r\n", data->deviceName);
#endif
	// Some diag output ...
	if (net->verbose) {
		diag_printf("Sending BOOTME request [%d] to %s\n",
					net->secNum,
					inet_ntoa((in_addr_t *)&net->srvAddrSend));
	}

	// Send packet
	net->dataLen = BOOTME_PKT_SIZE;

	return ce_send_frame(net);
}

int ce_send_write_ack(ce_net *net)
{
	unsigned short *wdata = (unsigned short*)net->data;

	wdata[ 0 ] = htons(EDBG_CMD_WRITE_ACK);
	wdata[ 1 ] = htons(net->blockNum);

	net->dataLen = 4;

	return ce_send_frame(net);
}

int ce_process_download(ce_net *net, ce_bin *bin)
{
	int ret = CE_PR_MORE;

	if (net->dataLen >= 2) {
		unsigned short *wdata = (unsigned short*)net->data;
		switch (ntohs(*wdata)) {
		case EDBG_CMD_WRITE_REQ:
			if (!net->link) {
				// Check file name for WRITE request
				// CE EShell uses "boot.bin" file name

				/*diag_printf(">>>>>>>> First Frame, IP: %s, port: %d\n",
							inet_ntoa((in_addr_t *)&net->srvAddrRecv),
							net->srvAddrRecv.sin_port);*/
				if (strncmp((char*)(net->data + 2), "boot.bin", 8) == 0) {
					// Some diag output
					if (net->verbose) {
						diag_printf("Locked Down download link, IP: %s, port: %d\n",
									inet_ntoa((in_addr_t *)&net->srvAddrRecv),
									net->srvAddrRecv.sin_port);
					}

					// Lock down EShell download link
					net->locAddr.sin_port = htons(EDBG_DOWNLOAD_PORT + 1);
					net->srvAddrSend.sin_port = net->srvAddrRecv.sin_port;
					net->srvAddrSend.sin_addr = net->srvAddrRecv.sin_addr;
					net->link = true;
				} else {
					// Unknown link
					net->srvAddrRecv.sin_port = 0;
				}

				// Return write ack
				if (net->link) {
					ce_send_write_ack(net);
				}
				break;
			}
			// LW: is it really intended to fall thru in case of net->link != 0 ?
		case EDBG_CMD_WRITE:
			// Fix data len
			bin->dataLen = net->dataLen - 4;

			// Parse next block of .bin file
			ret = ce_parse_bin(bin);

			// Request next block
			if (ret != CE_PR_ERROR) {
				net->blockNum++;

				ce_send_write_ack(net);
			}
			break;
		case EDBG_CMD_READ_REQ:
			// Read requests are not supported
			// Do nothing ...
			break;
		case EDBG_CMD_ERROR:
			// Error condition on the host side
			diag_printf("Error: unknown error on the host side\n");

			bin->binLen = 0;
			ret = CE_PR_ERROR;
			break;
		}
	}

	return ret;
}

void ce_process_edbg(ce_net *net, ce_bin *bin)
{
	eth_dbg_hdr *header;

	if (net->dataLen < sizeof(eth_dbg_hdr)) {
		// Bad packet

		net->srvAddrRecv.sin_port = 0;
		return;
	}

	header = (eth_dbg_hdr*)net->data;

	if (header->id != EDBG_ID) {
		// Bad packet

		net->srvAddrRecv.sin_port = 0;
		return;
	}

	if (header->service != EDBG_SVC_ADMIN) {
		// Unknown service

		return;
	}

	if (!net->link) {
		// Some diag output

		if (net->verbose) {
			diag_printf("Locked Down EDBG service link, IP: %s, port: %d\n",
				inet_ntoa((in_addr_t *)&net->srvAddrRecv),
				net->srvAddrRecv.sin_port);
		}

		// Lock down EDBG link

		net->srvAddrSend.sin_port = net->srvAddrRecv.sin_port;
		net->link = true;
	}

	switch (header->cmd) {
	case EDBG_CMD_JUMPIMG:
		net->gotJumpingRequest = true;

		if (net->verbose) {
			diag_printf("Received JUMPING command\n");
		}
		// Just pass through and copy CONFIG structure
	case EDBG_CMD_OS_CONFIG:
		// Copy config structure
		memcpy(&bin->edbgConfig, header->data, sizeof(edbg_os_config_data));
		if (net->verbose) {
			diag_printf("Received CONFIG command\n");
			if (bin->edbgConfig.flags & EDBG_FL_DBGMSG) {
				diag_printf("--> Enabling DBGMSG service, IP: %d.%d.%d.%d, port: %d\n",
					(bin->edbgConfig.dbgMsgIPAddr >> 0) & 0xFF,
					(bin->edbgConfig.dbgMsgIPAddr >> 8) & 0xFF,
					(bin->edbgConfig.dbgMsgIPAddr >> 16) & 0xFF,
					(bin->edbgConfig.dbgMsgIPAddr >> 24) & 0xFF,
					(int)bin->edbgConfig.dbgMsgPort);
			}
			if (bin->edbgConfig.flags & EDBG_FL_PPSH) {
				diag_printf("--> Enabling PPSH service, IP: %d.%d.%d.%d, port: %d\n",
					(bin->edbgConfig.ppshIPAddr >> 0) & 0xFF,
					(bin->edbgConfig.ppshIPAddr >> 8) & 0xFF,
					(bin->edbgConfig.ppshIPAddr >> 16) & 0xFF,
					(bin->edbgConfig.ppshIPAddr >> 24) & 0xFF,
					(int)bin->edbgConfig.ppshPort);
			}
			if (bin->edbgConfig.flags & EDBG_FL_KDBG) {
				diag_printf("--> Enabling KDBG service, IP: %d.%d.%d.%d, port: %d\n",
					(bin->edbgConfig.kdbgIPAddr >> 0) & 0xFF,
					(bin->edbgConfig.kdbgIPAddr >> 8) & 0xFF,
					(bin->edbgConfig.kdbgIPAddr >> 16) & 0xFF,
					(bin->edbgConfig.kdbgIPAddr >> 24) & 0xFF,
					(int)bin->edbgConfig.kdbgPort);
			}
			if (bin->edbgConfig.flags & EDBG_FL_CLEANBOOT) {
				diag_printf("--> Force clean boot\n");
			}
		}
		break;
	default:
		if (net->verbose) {
			diag_printf("Received unknown command: %08X\n", header->cmd);
		}
		return;
	}

	// Respond with ack
	header->flags = EDBG_FL_FROM_DEV | EDBG_FL_ACK;
	net->dataLen = EDBG_DATA_OFFSET;

	ce_send_frame(net);
}
#endif
